ALTER TABLE `servicios` ADD `codigo` VARCHAR(100) NOT NULL AFTER `id`;
ALTER TABLE `cotizaciones_productos` ADD `cantidad` INT(11) NOT NULL AFTER `p_unitario`;
ALTER TABLE `cotizaciones_servicios` ADD `cantidad` INT(11) NOT NULL AFTER `p_unitario`;

ALTER TABLE `cotizaciones_productos` ADD `total` FLOAT NOT NULL AFTER `cantidad`;
ALTER TABLE `cotizaciones_servicios` ADD `total` FLOAT NOT NULL AFTER `cantidad`;
ALTER TABLE `cotizaciones` ADD `check_descuento_e` VARCHAR(2) NOT NULL AFTER `estatus`, ADD `descuento_e` FLOAT NOT NULL AFTER `check_descuento_e`, ADD `check_descuento_p` VARCHAR(2) NOT NULL AFTER `descuento_e`, ADD `descuento_p` FLOAT NOT NULL AFTER `check_descuento_p`;

ALTER TABLE `cotizaciones_productos` CHANGE `codigo` `codigo` VARCHAR(250) CHARACTER SET utf8 COLLATE utf8_general_ci NULL;
ALTER TABLE `cotizaciones_productos` CHANGE `producto` `producto` VARCHAR(250) CHARACTER SET utf8 COLLATE utf8_general_ci NULL;
ALTER TABLE `cotizaciones_servicios` CHANGE `codigo` `codigo` VARCHAR(250) CHARACTER SET utf8 COLLATE utf8_general_ci NULL;
ALTER TABLE `cotizaciones_servicios` CHANGE `servicio` `servicio` VARCHAR(250) CHARACTER SET utf8 COLLATE utf8_general_ci NULL;

/*----------------------------*/

ALTER TABLE `venta_detalle` ADD `id_servicio` INT(11) NOT NULL AFTER `id_producto`;