ALTER TABLE `ventas` ADD `check_servicio` VARCHAR(2) NOT NULL AFTER `monto_total`, ADD `fecha_servicio` DATE NOT NULL AFTER `check_servicio`, ADD `hora_servicio` TIME NOT NULL AFTER `fecha_servicio`, ADD `unidad_servicio` INT(11) NOT NULL AFTER `hora_servicio`;

ALTER TABLE `ventas` ADD `id_cotizacion` INT(11) NOT NULL AFTER `id_cliente`;

INSERT INTO `menu_sub` (`MenusubId`, `MenuId`, `Nombre`, `Pagina`, `Icon`) VALUES (NULL, '10', 'Servicios', 'Servicio', 'fa fa-cogs');
INSERT INTO `perfiles_detalles` (`Perfil_detalleId`, `perfilId`, `MenusubId`) VALUES (NULL, '1', '27');


-----------------------------17_01_2023---------------------------

INSERT INTO `perfiles` (`perfilId`, `nombre`) VALUES (NULL, 'Clientes');
ALTER TABLE `clientes` ADD `check_usuario` VARCHAR(2) NOT NULL AFTER `check_unidades`;
ALTER TABLE `usuarios` ADD `clienteId` INT(11) NOT NULL AFTER `personalId`;
ALTER TABLE `venta_detalle` ADD `estatus` TINYINT(1) NOT NULL DEFAULT '0' AFTER `precio`;
ALTER TABLE `clientes` ADD `quejas` TEXT NOT NULL AFTER `check_usuario`;