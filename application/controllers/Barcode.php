<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Barcode extends CI_Controller {
	function __construct(){
        parent::__construct();
        $this->load->model('ModeloProductos');
    }
	public function index(){
		$id=$this->input->get('id');
		$data['getproductosbarcode']=$this->ModeloProductos->getproducto($id);
		$this->load->view('Reportes/barcode',$data);
	        
	}
}