<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Clientes extends CI_Controller {
    function __construct(){
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('Personal/ModeloPersonal');
        $this->load->model('ModeloCatalogos');
        $this->load->model('ModelGeneral');
        $this->load->model('Usuarios/ModeloUsuarios');
    }
    
	public function index()
    {
        $this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('clientes/listado');
        $this->load->view('templates/footer');
        $this->load->view('clientes/listadojs');
    }

    public function registrar($id=0)
    {   
        $data['uso_cfdi_row']=$this->ModeloCatalogos->getselectwheren('uso_cfdi',array());
        $data['metodo_pago_row']=$this->ModeloCatalogos->getselectwheren('metodo_pago',array());
        $data['forma_pago_row']=$this->ModeloCatalogos->getselectwheren('forma_pago',array());
        $data['regimen_fiscal_row']=$this->ModeloCatalogos->getselectwheren('regimen_fiscal',array());

        $data['perfilesrow']=$this->ModeloUsuarios->get_a_perfil(array('perfilId' => 5));

        if($id==0){
            $data['clienteId']=0;
            $data['nombre']='';
            $data['correo']='';
            $data['celular']='';
            $data['tel_oficina']='';
            $data['contacto']='';
            $data['direccion']='';
            $data['check_fiscales']='';
            $data['razon_social']='';
            $data['calle']='';
            $data['no_ext']='';
            $data['no_int']='';
            $data['colonia']='';
            $data['localidad']='';
            $data['municipio']='';
            $data['estado']='';
            $data['pais']='MÉXICO';
            $data['codigo_postal']='';
            $data['rfc']='';

            $data['uso_cfdi']='';
            $data['metodo_pago']='';
            $data['forma_pago']='';
            $data['condicion_pago']='';
            $data['regimen_fiscal']='';

            $data['check_bancarios']='';
            $data['check_unidades']='';

            $data['check_usuario']='';
            $data['UsuarioID'] = 0;
            $data['perfilId'] = 0;
            $data['Usuario'] = '';
            $data['contrasena'] = '';

        }else{
            $resul = $this->ModeloCatalogos->getselectwhere('clientes','clienteId',$id);
            foreach ($resul as $item){
                $data['clienteId']=$item->clienteId;
                $data['nombre']=$item->nombre;
                $data['correo']=$item->correo;
                $data['celular']=$item->celular;
                $data['tel_oficina']=$item->tel_oficina;
                $data['contacto']=$item->contacto;
                $data['direccion']=$item->direccion;
                $data['check_fiscales']=$item->check_fiscales;
                $data['razon_social']=$item->razon_social;
                $data['calle']=$item->calle;
                $data['no_ext']=$item->no_ext;
                $data['no_int']=$item->no_int;
                $data['colonia']=$item->colonia;
                $data['localidad']=$item->localidad;
                $data['municipio']=$item->municipio;
                $data['estado']=$item->estado;
                $data['pais']=$item->pais;
                $data['codigo_postal']=$item->codigo_postal;
                $data['rfc']=$item->rfc;

                $data['uso_cfdi']=$item->uso_cfdi;
                $data['metodo_pago']=$item->metodo_pago;
                $data['forma_pago']=$item->forma_pago;
                $data['condicion_pago']=$item->condicion_pago;
                $data['regimen_fiscal']=$item->regimen_fiscal;

                $data['check_bancarios']=$item->check_bancarios;
                $data['check_unidades']=$item->check_unidades;
                
                $data['check_usuario'] = $item->check_usuario;
            }
            
            $resultUsu = $this->ModeloCatalogos->getselectwhere('usuarios','clienteId',$id);
            if (sizeof($resultUsu) > 0 ){
                foreach ($resultUsu as $itemUsu) {
                    $data['UsuarioID'] = $itemUsu->UsuarioID;
                    $data['perfilId'] = $itemUsu->perfilId;
                    $data['Usuario'] = $itemUsu->Usuario;
                    $data['contrasena'] = 'xxxxxx';
                }
            }else{
                $data['UsuarioID'] = 0;
                $data['perfilId'] = 0;
                $data['Usuario'] = '';
                $data['contrasena'] = '';
            }            
        } 
        
        $this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('clientes/vista',$data);
        $this->load->view('templates/footer');
        $this->load->view('clientes/vistajs');
    }

    public function registro(){
        $data=$this->input->post();

        $id = $data['clienteId'];
        unset($data['clienteId']);

        if(isset($data['check_fiscales'])){
            $data['check_fiscales']='on';
        }else{
            $data['check_fiscales']='';
        }

        if(isset($data['check_bancarios'])){
            $data['check_bancarios']='on';
            unset($data['bancox']);
            unset($data['titularx']);
            unset($data['cuentax']);
            unset($data['clavex']);
            unset($data['tarjetax']);
        }else{
            $data['check_bancarios']='';
        }

        if(isset($data['check_unidades'])){
            $data['check_unidades']='on';
        }else{
            $data['check_unidades']='';
        }


        if(isset($data['check_usuario'])){
            $data['check_usuario']='on';

            $usuarioID = $data['usuarioid'];
            $usuario = $data['usuario'];
            $contrasena = $data['contrasena'];
            $perfilId = $data['perfiles'];

            unset($data['usuarioid']);
            unset($data['usuario']);
            unset($data['contrasena']);
            unset($data['perfiles']);
        }else{
            $data['check_usuario']='';
            unset($data['usuarioid']);
        }

        if($id == 0){
            $id = $this->ModeloCatalogos->Insert('clientes',$data);
        }else{
            $this->ModeloCatalogos->updateCatalogo('clientes',$data,array('clienteId'=>$id));
        }

        if($data['check_usuario'] === 'on'){
            if ($usuarioID  > 0) {
                $this->ModeloUsuarios->usuariosupdateC($usuarioID,$usuario,$contrasena,$perfilId,$id);
            }else{
                $this->ModeloUsuarios->usuariosinsertC($usuarioID,$usuario,$contrasena,$perfilId,$id);
            }
        }
        

        echo $id;
    }

    public function getlistado(){
        $params = $this->input->post();
        $getdata = $this->ModelGeneral->get_clientes($params);
        $totaldata= $this->ModelGeneral->total_clientes($params); 
        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($totaldata),  
            "recordsFiltered" => intval($totaldata),
            "data"            => $getdata->result(),
            "query"           =>$this->db->last_query()   
        );
        echo json_encode($json_data);
    }
    public function deleteregistro(){
        $id=$this->input->post('id');
        $this->ModeloCatalogos->updateCatalogo('clientes',array('activo'=>0),array('clienteId'=>$id));
    }

    function registro_bancarios(){
        $datos = $this->input->post('data');
        $DATA = json_decode($datos);
        for ($i=0;$i<count($DATA);$i++) {   
            $data['clienteId']=$DATA[$i]->clienteId;
            $data['banco']=$DATA[$i]->banco;
            $data['titular']=$DATA[$i]->titular;
            $data['cuenta']=$DATA[$i]->cuenta;
            $data['clave']=$DATA[$i]->clave;
            $data['tarjeta']=$DATA[$i]->tarjeta;
            if($DATA[$i]->id==0){
                $this->ModeloCatalogos->Insert('clientes_bancarios',$data);
            }else{
                $this->ModeloCatalogos->updateCatalogo('clientes_bancarios',$data,array('id'=>$DATA[$i]->id));
            }
        }   
    }

    function get_tabla_bancarios_all()
    {
        $id = $this->input->post('id');
        $arrayban = array('clienteId'=>$id,'activo'=>1);
        $results=$this->ModeloCatalogos->getselectwhere_n_consulta('clientes_bancarios',$arrayban);
        echo json_encode($results);
    }

    public function deleteregistro_bancaria(){
        $id=$this->input->post('id');
        $data = array('activo'=>0);
        $this->ModeloCatalogos->updateCatalogo('clientes_bancarios',$data,array('id'=>$id));
    }

    function get_tabla_unidades_all()
    {
        $id = $this->input->post('id');
        $arrayban = array('clienteId'=>$id,'activo'=>1);
        $results=$this->ModeloCatalogos->getselectwhere_n_consulta('unidades',$arrayban);
        echo json_encode($results);
    }

    function registro_unidades(){
        $datos = $this->input->post('data');
        $DATA = json_decode($datos);
        for ($i=0;$i<count($DATA);$i++) {   
            $data['clienteId']=$DATA[$i]->clienteId;
            $data['placas']=$DATA[$i]->placas;
            $data['modelo']=$DATA[$i]->modelo;
            $data['ano']=$DATA[$i]->ano;
            if($DATA[$i]->id==0){
                $this->ModeloCatalogos->Insert('unidades',$data);
            }else{
                $this->ModeloCatalogos->updateCatalogo('unidades',$data,array('unidadId'=>$DATA[$i]->id));
            }
        }   
    }

    public function deleteregistro_unidad(){
        $id=$this->input->post('id');
        $data = array('activo'=>0);
        $this->ModeloCatalogos->updateCatalogo('unidades',$data,array('unidadId'=>$id));
    }


    public function get_quejas(){
        $id = $this->input->post('id');
        $quejas = $this->ModeloCatalogos->getQuejasClientes($id);
        echo json_encode($quejas);
    }
}