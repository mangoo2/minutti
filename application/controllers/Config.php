<?php 

defined('BASEPATH') OR exit('No direct script access allowed');

class Config extends CI_Controller {
    function __construct(){
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('ModeloCatalogos');
        $this->load->model('ModeloConfig');
        if($this->session->userdata('logeado')==true){
            $this->perfilid =$this->session->userdata('perfilid');
            $this->idpersonal = $this->session->userdata('idpersonal');
            $this->sucursal = $this->session->userdata('sucursal');
            
        }else{
            redirect('Sistema'); 
        }
    }
    public function index(){
        $this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('config/vista');
        $this->load->view('templates/footer');
        $this->load->view('config/vistajs');
    }

    public function addtipo(){
        $params = $this->input->post();
        $id = $params['id'];
        unset($params['id']);
        if ($id>0) {
            $this->ModeloCatalogos->updateCatalogo('tipo_camioneta',$params,array('id'=>$id));
            $id=$id;
        }else{
            $id=$this->ModeloCatalogos->Insert('tipo_camioneta',$params);
        }
        echo $id;
    }

    public function getlistado_tipo(){
        $params = $this->input->post();
        $getdata = $this->ModeloConfig->get_tipo_camioneta($params);
        $totaldata= $this->ModeloConfig->total_tipo_camioneta($params); 
        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($totaldata),  
            "recordsFiltered" => intval($totaldata),
            "data"            => $getdata->result(),
            "query"           =>$this->db->last_query()   
        );
        echo json_encode($json_data);
    }

    function delete_tipo(){
        $params = $this->input->post();
        $id=$params['id'];
        $this->ModeloCatalogos->updateCatalogo('tipo_camioneta',array('activo'=>0),array('id'=>$id));
    } 
}