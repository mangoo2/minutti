<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Configuracion_transporte extends CI_Controller
{
  function __construct()
  {
    parent::__construct();
    $this->load->helper('url');
    $this->load->model('ModeloTransporte');
    $this->load->model('ModeloCatalogos');
    $this->load->model('ModeloConfig');
    $this->idpersonal = $this->session->userdata('idpersonal');
    date_default_timezone_set('America/Mexico_City');
    $this->fechahoy = date('Y-m-d G:i:s');
  }

  public function index()
  {
    $this->load->view('templates/header');
    $this->load->view('templates/navbar');
    $this->load->view('ventas/config_transportes');
    $this->load->view('templates/footer');
    $this->load->view('ventas/config_transportesjs');
  }

  public function add($id = 0)
  {
    $data["id"] = $id;
    $data['title_heades'] = 'Configuración de transporte';
    $data['title_save'] = 'Guardar';
    if ($id > 0) {
      $data['title_heades'] = 'Editar configuración de transporte';
      $data['title_save'] = 'Actualizar';
    }
    $data['productosrow'] = $this->ModeloTransporte->getselect_where('producto', array('unidadId' => 1));

    $this->load->view('templates/header');
    $this->load->view('templates/navbar');
    $this->load->view('ventas/config_transporte_add', $data);
    $this->load->view('templates/footer');
    $this->load->view('ventas/config_transporte_addjs');
  }

  function getTabla()
  {
    $results = $this->ModeloCatalogos->getselectwhere_n_consulta('config_transporte', array("status" => 1));
    $json_data = array("data" => $results);
    echo json_encode($json_data);
  }

  function getDetalleTabla()
  {
    $id = $this->input->post('id');
    $datodetalle=$this->ModeloConfig->getDetalleConfig($id);
    $html="";
    foreach ($datodetalle as $d) {
      $html.="<tr>
              <td>".$d->cantidad."</td>
              <td>".$d->codigo."</td>
              <td>".$d->nombre."</td>
            </tr>";
    }
    echo $html;
  }

  function getDetalles(){
    $id = $this->input->post('id');
    $dato=$this->ModeloCatalogos->getselectwhere_n_consulta('config_transporte',array('id'=>$id));
    
    $array = array();
    //$datodetalle=$this->ModeloCatalogos->getselectwhere_n_consulta('config_transporte_detalle',array('id_config'=>$id,'status'=>1));
    $datodetalle=$this->ModeloConfig->getDetalleConfig($id);
    if($dato){
        $dato=$dato[0];
        $array = array(
            'id'=>$dato->id,
            'nombre'=>$dato->nombre,
            'detalle'=>$datodetalle
        );
    }
    echo json_encode($array);
  }

  /*function insertupdate()
  {
    $params = $this->input->post();
    $configId = $params['configId'];
    $arrayconfiguracion = $params['arrayconfiguracion'];
    unset($params['configId']);
    unset($params['arrayconfiguracion']);

    if ($configId > 0) {
      $this->ModeloCatalogos->updateCatalogo('config_transporte', $params, array('id' => $configId));
    } else {
      $configId = $this->ModeloCatalogos->Insert('config_transporte', $params);
    }

    $DATAc = json_decode($arrayconfiguracion);

    for ($i = 0; $i < count($DATAc); $i++) {
      $configdId = $DATAc[$i]->configdId;
      if ($configdId > 0) {
        $this->ModeloCatalogos->updateCatalogo('config_transporte_detalle', array('nombre' => $DATAc[$i]->nombresub), array('id_config' => $configdId));
      } else {
        $configdId = $this->ModeloCatalogos->Insert('config_transporte_detalle', array('id_config' => $configId, 'nombre' => $DATAc[$i]->nombresub));
      }

      $DATAcd = $DATAc[$i]->productos;

      for ($j = 0; $j < count($DATAcd); $j++) {
        $configddId = $DATAcd[$j]->configddId;
        if ($configddId > 0) {
          //$this->ModeloCatalogos->updateCatalogo('configuracion_detalle_d',array('piezas'=>$DATAcd[$j]->piezas),array('configddId'=>$configddId));
        } else {
          $this->ModeloCatalogos->Insert('config_transporte_detalle', array('id_config' => $configdId, 'cantidad' => $DATAcd[$j]->piezas, 'id_producto' => $DATAcd[$j]->productoId));
        }
      }
    }
  }*/

  function deleteg()
  {
    $params = $this->input->post();
    $id = $params['id'];
    $this->ModeloCatalogos->updateCatalogo('config_transporte', array('status' => 0), array('id' => $id));
  }

  function deleteDetalle()
  {
    $id = $this->input->post("id");
    $this->ModeloCatalogos->updateCatalogo('config_transporte_detalle', array('status' => 0), array('id' => $id));
  }

  public function insertupdate()
  {
    $data = $this->input->post('data');
    $DATA = json_decode($data);
    $cont_conf = 0;
    for ($i = 0; $i < count($DATA); $i++) {
      $cont_conf++;
      if ($DATA[$i]->configId > 0 && $cont_conf == 1) {
        $configId = $DATA[$i]->configId;
        $this->ModeloCatalogos->updateCatalogo('config_transporte', array("nombre" => $DATA[$i]->nombre, "id_usuario" => $this->idpersonal, "fecha_reg" => $this->fechahoy), array('id' => $DATA[$i]->configId));
      } else if ($DATA[$i]->configId == 0 && $cont_conf == 1) {
        $configId = $this->ModeloCatalogos->Insert('config_transporte', array("nombre" => $DATA[$i]->nombre, "id_usuario" => $this->idpersonal, "fecha_reg" => $this->fechahoy));
      }
      if ($DATA[$i]->configddId > 0) {
        $this->ModeloCatalogos->updateCatalogo('config_transporte_detalle', array("id_config" => $configId, "cantidad" => $DATA[$i]->piezas, "id_producto" => $DATA[$i]->productoId), array('id' => $DATA[$i]->configddId));
      } else {
        $configdId = $this->ModeloCatalogos->Insert('config_transporte_detalle', array('id_config' => $configId, 'cantidad' => $DATA[$i]->piezas, "id_producto" => $DATA[$i]->productoId));
      }
    }
  }
}

