<?php 

defined('BASEPATH') OR exit('No direct script access allowed');

class Configuraciones extends CI_Controller {
    function __construct(){
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('ModeloCatalogos');
        $this->load->model('ModeloConfig');
        if($this->session->userdata('logeado')==true){
            $this->perfilid =$this->session->userdata('perfilid');
            $this->idpersonal = $this->session->userdata('idpersonal');
            $this->sucursal = $this->session->userdata('sucursal');
            
        }else{
            redirect('Sistema'); 
        }
    }
    public function index(){
        $this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('configuraciones/configuracioneslis');
        $this->load->view('templates/footer');
        $this->load->view('configuraciones/configuracioneslisjs');
    }
    function add($id=0,$view=0){
        $data['title_heades']='Agregar Configuración';
        $data['title_save']='Guardar';
        if($id>0){
            $data['title_heades']='Editar Configuración';
            $data['title_save']='Actualizar';
        }
        $data['productoId']=$id;
        $data['viewstatus']=$view;
        
        
        $data['productosinsumos']=$this->ModeloCatalogos->getselectwheren('producto',array('activo'=>1));
        $this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('configuraciones/configuracionesadd',$data);
        $this->load->view('templates/footer');
        $this->load->view('configuraciones/configuracionesaddjs');
    }
    function insertupdate(){
        $params = $this->input->post();
        $configId = $params['configId'];
        $arrayconfiguracion = $params['arrayconfiguracion'];
        unset($params['configId']);
        unset($params['arrayconfiguracion']);
        if($configId>0){
            $this->ModeloCatalogos->updateCatalogo('configuracion',$params,array('configId'=>$configId));
        }else{
            $configId = $this->ModeloCatalogos->Insert('configuracion',$params);
        }
        $DATAc = json_decode($arrayconfiguracion); 
        for ($i=0;$i<count($DATAc);$i++) {
            $configdId=$DATAc[$i]->configdId;
            if($configdId>0){
                $this->ModeloCatalogos->updateCatalogo('configuracion_detalle',array('nombresub'=>$DATAc[$i]->nombresub),array('configdId'=>$configdId));
            }else{
                $configdId= $this->ModeloCatalogos->Insert('configuracion_detalle',array('configId'=>$configId,'nombresub'=>$DATAc[$i]->nombresub));
            }
            $DATAcd=$DATAc[$i]->productos;
            for ($j=0;$j<count($DATAcd);$j++) {
                $configddId=$DATAcd[$j]->configddId;
                if($configddId>0){
                    //$this->ModeloCatalogos->updateCatalogo('configuracion_detalle_d',array('piezas'=>$DATAcd[$j]->piezas),array('configddId'=>$configddId));
                }else{
                    $this->ModeloCatalogos->Insert('configuracion_detalle_d',array('configdId'=>$configdId,'piezas'=>$DATAcd[$j]->piezas,'productoId'=>$DATAcd[$j]->productoId));
                }
            }
        }
    }
    function obtenerdatosproducto(){
        $params = $this->input->post();
        $id = $params['id'];
        $configId=0;
        $codigo='';
        $nombre='';
        $descripcion='';
        $configuracion_result = $this->ModeloCatalogos->getselectwheren('configuracion',array('configId'=>$id));
        $config_d_result = $this->ModeloCatalogos->getselectwheren('configuracion_detalle',array('configId'=>$id));
        foreach ($configuracion_result->result() as $item) {
            $configId       = $item->configId;
            $codigo         = $item->codigo;
            $nombre         = $item->nombre;
            $descripcion    = $item->descripcion;
        }

        $arrayconfiguracion = array(
                                    'configId'=>$configId,
                                    'codigo'=>$codigo,
                                    'nombre'=>$nombre,
                                    'descripcion'=>$descripcion,
                                    'config_d'=>$config_d_result->result()
                                    );
        echo json_encode($arrayconfiguracion);
    }
    function obtenerdatospro(){
        $params = $this->input->post();
        $id = $params['id'];
        $producto_result = $this->ModeloCatalogos->getselectwheren('producto',array('productoId'=>$id));
        $productoId=0;
        $codigo='';
        $nombre='';
        foreach ($producto_result->result() as $item) {
            $productoId=$item->productoId;
            $codigo=$item->codigo;
            $nombre=$item->nombre;
        }
        $arrayp=array(
                    'productoId'=>$productoId,
                    'codigo'=>$codigo,
                    'nombre'=>$nombre
            );
        echo json_encode($arrayp);
    }
    function config_d_d(){
        $params = $this->input->post();
        $id = $params['id'];
        $producto_result = $this->ModeloCatalogos->configproducto($id);
        echo json_encode($producto_result->result());
    }
    function deleteproduto(){
        $params = $this->input->post();
        $id = $params['producto'];
 
        $this->ModeloCatalogos->updateCatalogo('configuracion_detalle_d',array('activo'=>0),array('configddId'=>$id));
    }
    public function getlistconfig() {
        $params = $this->input->post();

        $getdata = $this->ModeloConfig->getlistconfig($params);
        $totaldata= $this->ModeloConfig->getlistconfigt($params); 
        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($totaldata),  
            "recordsFiltered" => intval($totaldata),
            "data"            => $getdata->result(),
            "query"           =>$this->db->last_query()   
        );
        echo json_encode($json_data);
    }
    function deleteg(){
        $params = $this->input->post();
        $id = $params['id'];
 
        $this->ModeloCatalogos->updateCatalogo('configuracion',array('activo'=>0),array('configId'=>$id));
    }

    function search_producto(){
        $usu = $this->input->get('search');
        $results=$this->ModeloCatalogos->productoallsearch2($usu);
        echo json_encode($results->result());
    }

}