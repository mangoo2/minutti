<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Corte_caja extends CI_Controller {
    function __construct(){
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('ModeloCatalogos');
        $this->load->model('ModeloProductos');
        $this->load->model('ModelGeneral');
        $this->load->model('ModeloVentas');
        if($this->session->userdata('logeado')==true){
            $this->perfilid =$this->session->userdata('perfilid');
            $this->idpersonal = $this->session->userdata('idpersonal');
            $this->sucursal = $this->session->userdata('sucursal');
            
        }else{
            redirect('Sistema'); 
        }
    }
	public function index(){
        $this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('cortecaja/form');
        $this->load->view('templates/footer');
        $this->load->view('cortecaja/formjs');

	}
    function corte(){
        $inicio = $this->input->post('fecha1');
        $fin = $this->input->post('fecha2');
        $tipo_venta = $this->input->post('tipo_venta');
        $pro_ven = $this->input->post('pro_ven');

        $resultadoc=$this->ModeloVentas->corte($inicio,$fin,$tipo_venta,$pro_ven);
        $resultadocs=$this->ModeloVentas->cortesum($inicio,$fin,$tipo_venta,$pro_ven);

        $table="<table class='tabla table table-striped table-bordered table-hover' id='sample_2'>
                    <thead>
                        <tr>
                            <th>Número de venta/Orden</th>
                            <th>Descripción</th>
                            <th>Categoría</th>
                            <th>Precio de venta</th>
                            <th>Costo</th>
                            <th>Utilidad</th>
                            <th>Tipo de venta</th>
                            <th>Cliente</th>
                            <th>Fecha</th>
                            <th>Cajero</th>
                        </tr>
                    </thead>
                    <tbody>";
        $rowventas=0;
        $obedt=0;
        foreach ($resultadoc->result() as $fila) {
            $totalventas=$fila->monto_total; 
            $totalpreciocompra=$this->ModeloVentas->consultartotal_venta2($fila->id_venta);
            $obed =$totalventas-$totalpreciocompra; 
            $tipo_venta_text='';
            if($fila->metodo==1){
                $tipo_venta_text='Efectivo';
            }else if($fila->metodo==2){
                $tipo_venta_text='Tarjeta de crédito';
            }else if($fila->metodo==3){
                $tipo_venta_text='Tarjeta de débito';
            }else if($fila->metodo==4){
                $tipo_venta_text='Crédito';
            }
            $utilidad=$fila->precio-$fila->costo_compra;
            $table .= "<tr>
                            <td>".$fila->id_venta."</td>
                            <td>".$fila->producto."</td>
                            <td>".$fila->categoria."</td>
                            <td>$".number_format($fila->precio,2,'.',',')."</td>
                            <td>$".number_format($fila->costo_compra,2,'.',',')."</td>
                            <td>$".number_format($utilidad,2,'.',',')."</td>
                            <td>".$tipo_venta_text."</td>
                            <td>".$fila->nombre."</td>
                            <td>".$fila->reg."</td>
                            <td>".$fila->vendedor."</td>
                        </tr>";
            $obedt=$obedt+$obed;
            $rowventas++;
            /*
                <td>$".number_format($fila->subtotal,2,'.',',')."</td>
                <td>$".number_format($fila->monto_total,2,'.',',')."</td>
                <td>$".number_format($fila->descuentocant,2,'.',',')."</td>  
                <td>$".number_format($fila->efectivo,2,'.',',')."</td>
            */
        }
        $table.="</tbody> </table>";
        
        $mas_vendindos='<br><h5><b>Productos mas vendidos</b><h5><hr>';
            $mas_vendindos.='<table class="tabla table table-striped table-bordered table-hover">
                    <thead>
                        <tr>
                            <th style="font-size: 16px;">Número</th>
                            <th style="font-size: 16px;">Producto</th>
                            <th style="font-size: 16px;">Cantidad</th>
                        </tr>
                    </thead>
                    <tbody>';
                $resulta_ven_pro=$this->ModeloVentas->pro_venta($inicio,$fin);
                $contador=1;
                foreach ($resulta_ven_pro->result() as $x){
                        $mas_vendindos .= "<tr>
                            <td>".$contador."</td>
                            <td>".$x->producto."</td>
                            <td>".$x->total_cantidad."</td>
                        </tr>";
                    $contador++;         
                }
        $mas_vendindos.='</tbody>
                </table>';

        $total=0;
        $subtotal=0;
        foreach ($resultadocs->result() as $fila) {
            $total = $fila->total;
            $subtotal = $fila->subtotal;
            $descuento = $fila->descuento;
        }

        $total = round($total,2);
        $subtotal = round($subtotal,2);

        
        $subtotal=number_format($subtotal,2,'.',',');

        $obedt=number_format($obedt,2,'.',',');

        $ttotal=0;
        $gasto=0;
        $resultgastos=$this->ModeloVentas->get_gastos($inicio,$fin);
        foreach ($resultgastos->result() as $x){
            $gasto=$x->gasto;
        }

        $ttotal=$total-$gasto;


        $array = array("tabla"=>$table,
                        "tabla2"=>'',
                        "dgastos"=>number_format($gasto,2,'.',','),
                        "dTotal"=>number_format($ttotal,2,'.',','),
                        "totalventas"=>$rowventas,
                        "dImpuestos"=>0,
                        "totalutilidad"=>$obedt,
                        "descuento"=>$descuento,
                        "dSubtotal"=>"".$subtotal."",
                        "mas_vendindos"=>$mas_vendindos 
                    );
            echo json_encode($array);
    }

    function corte_credito(){
        $inicio = $this->input->post('fecha1');
        $fin = $this->input->post('fecha2');
        $tipo_venta = $this->input->post('tipo_venta');
        $pro_ven = $this->input->post('pro_ven');

        $resultadoc=$this->ModeloVentas->corte_credito($inicio,$fin,$tipo_venta,$pro_ven);
        $resultadocs=$this->ModeloVentas->cortesum_credito($inicio,$fin,$tipo_venta,$pro_ven);

        $table="<table class='tabla table table-striped table-bordered table-hover' id='sample_2'>
                    <thead>
                        <tr>
                            <th>No. venta</th>
                            <th>Cajero</th>
                            <th>Cliente</th>
                            <th>Fecha</th>
                            <th>Subtotal</th>
                            <th>Total</th>
                            <th>Descuento</th>
                            <th>Tipo de venta</th>
                            <th>Efectivo</th>
                        </tr>
                    </thead>
                    <tbody>";
        $rowventas=0;
        $obedt=0;
        foreach ($resultadoc->result() as $fila) {
            $totalventas=$fila->monto_total; 
            $totalpreciocompra=$this->ModeloVentas->consultartotal_venta2($fila->id_venta,$fila->sucursal);
            $obed =$totalventas-$totalpreciocompra; 
            $tipo_venta_text='';
            if($fila->metodo==1){
                $tipo_venta_text='Efectivo';
            }else if($fila->metodo==2){
                $tipo_venta_text='Tarjeta de crédito';
            }else if($fila->metodo==3){
                $tipo_venta_text='Tarjeta de débito';
            }else if($fila->metodo==4){
                $tipo_venta_text='Crédito';
            }
            $abono = $this->ModeloVentas->getall_total_abono($fila->id_venta);
            $restar=$fila->monto_total-$abono;
            $table .= "<tr>
                            <td rowspan='2'>".$fila->id_venta."</td>
                            <td>".$fila->vendedor."</td>
                            <td>".$fila->nombre."</td>
                            <td>".$fila->reg."</td>
                            <td>$".number_format($fila->subtotal,2,'.',',')."</td>
                            <td>$".number_format($fila->monto_total,2,'.',',')."</td>
                            <td>$".number_format($fila->descuentocant,2,'.',',')."</td>
                            <td>".$tipo_venta_text."</td>
                            <td>$".number_format($fila->efectivo,2,'.',',')."</td>
                        </tr>
                        <tr>
                            <td colspan='3' align='center'><h4><b> Abono </b></h4></td>
                            <td>$".number_format($abono,2,'.',',')."</td>
                            <td colspan='3' align='center'><h4><b> Restante </b> </h4></td>
                            <td colspan='2'>$".number_format($restar,2,'.',',')."</td>
                        </tr>";
            $obedt=$obedt+$obed;
            $rowventas++;
            
        }
        $table.="</tbody> </table>";
        
        $mas_vendindos='<br><h5><b>Productos mas vendidos</b><h5><hr>';
        $mas_vendindos.='<table class="tabla table table-striped table-bordered table-hover">
                        <thead>
                            <tr>
                                <th style="font-size: 16px;">Producto</th>
                                <th style="font-size: 16px;">Cantidad</th>
                            </tr>
                        </thead>
                        <tbody>';
                    $resulta_ven_pro=$this->ModeloVentas->pro_venta($inicio,$fin);
                    foreach ($resulta_ven_pro->result() as $x){
                            $mas_vendindos .= "<tr>
                                <td>".$x->producto."</td>
                                <td>".$x->total_cantidad."</td>
                            </tr>";         
                    }
        $mas_vendindos.='</tbody>
                    </table>';


        $total=0;
        $subtotal=0;
        foreach ($resultadocs->result() as $fila) {
            $total = $fila->total;
            $subtotal = $fila->subtotal;
            $descuento = $fila->descuento;
        }

        $total = round($total,2);
        $subtotal = round($subtotal,2);

        
        $subtotal=number_format($subtotal,2,'.',',');

        $obedt=number_format($obedt,2,'.',',');
        
        $ttotal=0;
        $gasto=0;
        $resultgastos=$this->ModeloVentas->get_gastos($inicio,$fin);
        foreach ($resultgastos->result() as $x){
            $gasto=$x->gasto;
        }

        $ttotal=$total-$gasto;

        $array = array("tabla"=>$table,
                        "tabla2"=>'',
                        "dgastos"=>number_format($gasto,2,'.',','),
                        "dTotal"=>number_format($ttotal,2,'.',','),
                        "totalventas"=>$rowventas,
                        "dImpuestos"=>0,
                        "totalutilidad"=>$obedt,
                        "descuento"=>$descuento,
                        "dSubtotal"=>"".$subtotal."",
                        "mas_vendindos"=>$mas_vendindos 
                    );
            echo json_encode($array);
    }


    ////Gastos
    function corte_gastos(){
        $inicio = $this->input->post('fecha1');
        $fin = $this->input->post('fecha2');
        $result=$this->ModeloVentas->corte_gastos($inicio,$fin);
        $table="<h5><b>GASTOS Y PAGOS</b></h5><table class='tabla table table-striped table-bordered table-hover' id='sample_2'>
                    <thead>
                        <tr>
                            <th>ID pago o gastos</th>
                            <th>Descripción</th>
                            <th>Tipo</th>
                            <th>Categoría</th>
                            <th>Monto</th>
                        </tr>
                    </thead>
                    <tbody>";
        $total_pagos=0;
        $total_gastos=0;            
        foreach ($result->result() as $x) {

            if($x->tipocobro==1){
                $total_gastos+=$x->monto;
                $tipo_gasto='Gasto';
            }else if($x->tipocobro==2){
                $total_pagos+=$x->monto;
                $tipo_gasto='Pago';
            }else{
                $tipo_gasto='';
            }
            $table .= "<tr>
                            <td>".$x->id."</td>
                            <td>".$x->nombre."</td>
                            <td>".$tipo_gasto."</td>
                            <td>".$x->categoria."</td>
                            <td>$".number_format($x->monto,2,'.',',')."</td>
                        </tr>";
        }
        $suma_total=$total_gastos+$total_pagos;
        $table.="</tbody> </table>
        <h5><b>SUB TOTAL GASTOS: $".number_format($total_gastos,2,'.',',')."</b></h5><h5><b>SUB TOTAL PAGOS: $".number_format($total_pagos,2,'.',',')."</b></h5><h5><b>TOTAL: $".number_format($suma_total,2,'.',',')."</b></h5>";
        $array = array("tabla"=>$table);
        echo json_encode($array);
    }
    
}
