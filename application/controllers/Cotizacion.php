<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Cotizacion extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('ModelGeneral');
        $this->load->model('ModeloCatalogos');

        date_default_timezone_set('America/Mexico_City');
        $this->currentFecha = date('Y-m-d');
    }


    public function index()
    {
        $data['fecha']=$this->currentFecha;

        $this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('cotizacion/index',$data);
        $this->load->view('templates/footer');
        $this->load->view('cotizacion/index_js');
    }


    public function add($id = 0)
    {
        $noCotizacion = $this->ModelGeneral->get_id('cotizaciones', 'idCotizaciones');

        $data['fecha'] = date('Y-m-d');
        $data['noCotizacion'] = $noCotizacion + 1;
        $data['check_productos'] = '';
        $data['check_servicios'] = '';

        $data['check_descuento_p'] = '';
        $data['check_descuento_e'] = '';

        $data['usuarioId'] = $_SESSION['usuarioid'];


        $data['id'] = $id;
        $data['title_heades'] = 'Nueva cotización';
        $data['title_save'] = 'Guardar';

        if ($id > 0) {
            $data['title_heades'] = 'Editar cotización';
            $data['title_save'] = 'Actualizar';

            $result = $this->ModeloCatalogos->getselectwhere('cotizaciones', 'idCotizaciones', $id);
            $data['data']  = $result;

            foreach ($result as $item) {
                $data['noCotizacion'] = $item->idCotizaciones;

                $data['clienteId'] = $item->clienteId;

                $data['fecha'] = $item->fecha;
                $data['correo'] = $item->correo;
                $data['telefono'] = $item->telefono;
                $data['check_productos'] = $item->check_producto;
                $data['total_productos'] = $item->total_producto;
                $data['check_servicios'] = $item->check_servicio;
                $data['total_servicios'] = $item->total_servicio;
                $data['observaciones'] = $item->observaciones;
                //$data['estatus'] = $item->estatus;

                $data['check_descuento_p'] = $item->check_descuento_p;
                $data['descuento_p'] = $item->descuento_p;

                $data['check_descuento_e'] = $item->check_descuento_e;
                $data['descuento_e'] = $item->descuento_e;

                $data['subtotal'] = $item->subtotal;
                $data['iva'] = $item->iva;
                $data['total'] = $item->total;
                $data['usuarioId'] = $item->usuarioId;
                //$data['activo'] = $item->activo;
            }

            /*
            $result = $this->ModeloCatalogos->getselectwhere('personal', 'personalId', $data["userId"]);
            foreach ($result as $key) {
                $data["user"] = $key->nombre;
            }
            */
        }

        $this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('cotizacion/form', $data);
        $this->load->view('templates/footer');
        $this->load->view('cotizacion/form_js');
    }


    public function insert()
    {
        $params = $this->input->post();
        $idCotizaciones = $params['IDcotizaciones'];
        unset($params['IDcotizaciones']);

        $noCotizaciones = $params['noCotizacion'];
        unset($params['noCotizacion']);


        $checkProductos =  false;
        if (isset($params['check_productos'])) {
            $checkProductos =  true;
            unset($params['check_productos']);
        }
        $cantP = $params['cantP'];
        unset($params['cantP']);


        $checkServicios =  false;
        if (isset($params['check_servicios'])) {
            $checkServicios =  true;
            unset($params['check_servicios']);
        }
        $cantS = $params['cantS'];
        unset($params['cantS']);


        $check_descuento_e =  false;
        if (isset($params['check_descuento_e'])) {
            $check_descuento_e =  true;
            unset($params['check_descuento_e']);
        }

        $check_descuento_p =  false;
        if (isset($params['check_descuento_p'])) {
            $check_descuento_p =  true;
            unset($params['check_descuento_p']);
        }

        //Productos
        $arrayProductos = array();
        for ($i = 0; $i < $cantP; $i++) {
            $productos = array(
                'cotizacionesId' => "",
                'codigo' => isset($params['codigoP' . $i]) ? $params['codigoP' . $i] : '',
                'producto' => isset($params['productoP' . $i]) ? $params['productoP' . $i] : '',
                'descripcion' => isset($params['descripcionP' . $i]) ? $params['descripcionP' . $i] : '',
                'cantidad' => isset($params['cantidadP' . $i]) ? $params['cantidadP' . $i] : '',
                'p_unitario' => isset($params['pUnitarioP' . $i]) ? $params['pUnitarioP' . $i] : '',
                'total' => isset($params['totalP' . $i]) ? $params['totalP' . $i] : '',
            );

            unset($params['codigoP' . $i]);
            unset($params['productoP' . $i]);
            unset($params['descripcionP' . $i]);
            unset($params['cantidadP' . $i]);
            unset($params['pUnitarioP' . $i]);

            if(isset($params['totalP' . $i]) && $params['totalP' . $i] != ''){
                array_push($arrayProductos, $productos);
                $cantP += 1;
            }

            unset($params['totalP' . $i]);
        }


        //Servicios
        $arrayServicios = array();
        for ($i = 0; $i < $cantS; $i++) {
            $servicios = array(
                'cotizacionesId' => "",
                'codigo' => isset($params['codigoS' . $i]) ? $params['codigoS' . $i] : '',
                'servicio' => isset($params['servicioS' . $i]) ? $params['servicioS' . $i] : '',
                'descripcion' => isset($params['descripcionS' . $i]) ? $params['descripcionS' . $i] : '',
                'cantidad' => isset($params['cantidadS' . $i]) ? $params['cantidadS' . $i] : '',
                'p_unitario' => isset($params['pUnitarioS' . $i]) ? $params['pUnitarioS' . $i] : '',
                'total' => isset($params['totalS' . $i]) ? $params['totalS' . $i] : '',
            );

            unset($params['codigoS' . $i]);
            unset($params['servicioS' . $i]);
            unset($params['descripcionS' . $i]);
            unset($params['cantidadS' . $i]);
            unset($params['pUnitarioS' . $i]);

            if(isset($params['totalS' . $i]) && $params['totalS' . $i] != ''){
                array_push($arrayServicios, $servicios);
                $cantS += 1;
            }

            unset($params['totalS' . $i]);
        }

        if ($idCotizaciones > 0) {
            $data = array('clienteId' => $params['clienteId'], 'correo' => $params['correo'], 'telefono' => $params['telefono'], 'check_producto' => $checkProductos ? "on" : "", 'total_producto' => $checkProductos ? $params['total_producto'] : 0, 'check_servicio' => $checkServicios ? "on" : "", 'total_servicio' => $checkServicios ? $params['total_servicio'] : 0, 'check_descuento_e' => $check_descuento_e ? "on" : "", 'descuento_e' => $check_descuento_e ? $params['descuento_e'] : 0, 'check_descuento_p' => $check_descuento_p ? "on" : "", 'descuento_p' => $check_descuento_p ? $params['descuento_p'] : 0, 'subtotal' => $params['subtotal'], 'iva' => $params['iva'], 'total' => $params['total']);
            $this->ModeloCatalogos->updateCatalogo('cotizaciones', $data, array('idCotizaciones' => $idCotizaciones));

            //Productos
            if ($checkProductos) {
                //$this->ModeloCatalogos->replaceCatalogo('cotizaciones_productos', array('cotizacionesId' => $noCotizaciones));
                foreach ($arrayProductos as $i => $key) {
                    $key['cotizacionesId'] = $noCotizaciones;

                    if($params['idP' . $i] > 0){
                        $this->ModeloCatalogos->updateCatalogo('cotizaciones_productos', $key, array('idProductos' => $params['idP' . $i]));
                    }else{
                        $this->ModeloCatalogos->Insert('cotizaciones_productos', $key);
                    }
                }
            }

            //Servicios
            if ($checkServicios) {
                //$this->ModeloCatalogos->replaceCatalogo('cotizaciones_servicios', array('cotizacionesId' => $noCotizaciones));
                foreach ($arrayServicios as $i => $key) {
                    $key['cotizacionesId'] = $noCotizaciones;

                    if($params['idS' . $i] > 0){
                        $this->ModeloCatalogos->updateCatalogo('cotizaciones_servicios', $key, array('idServicios' => $params['idS' . $i]));
                    }else{
                        $this->ModeloCatalogos->Insert('cotizaciones_servicios', $key);
                    }
                }
            }


        } else {
            $data = array('clienteId' => $params['clienteId'], 'fecha' => $params['fecha'], 'correo' => $params['correo'], 'telefono' => $params['telefono'], 'check_producto' => $checkProductos ? "on" : "", 'total_producto' => $checkProductos ? $params['total_producto'] : 0, 'check_servicio' => $checkServicios ? "on" : "", 'total_servicio' => $checkServicios ? $params['total_servicio'] : 0, 'check_descuento_e' => $check_descuento_e ? "on" : "", 'descuento_e' => $check_descuento_e ? $params['descuento_e'] : 0, 'check_descuento_p' => $check_descuento_p ? "on" : "", 'descuento_p' => $check_descuento_p ? $params['descuento_p'] : 0, 'subtotal' => $params['subtotal'], 'iva' => $params['iva'], 'total' => $params['total'], 'usuarioId' => $params['usuarioId']);
            $id = $this->ModeloCatalogos->Insert('cotizaciones', $data);
            $idCotizaciones=$id;
            //Productos
            if ($checkProductos) {
                foreach ($arrayProductos as $key) {
                    $key['cotizacionesId'] = $id;
                    $this->ModeloCatalogos->Insert('cotizaciones_productos', $key);
                }
            }

            //Servicios
            if ($checkServicios) {
                foreach ($arrayServicios as $key) {
                    $key['cotizacionesId'] = $id;
                    $this->ModeloCatalogos->Insert('cotizaciones_servicios', $key);
                }
            }
        }
        
        $arrayinfo = array('idCotizaciones' => $idCotizaciones);
        echo json_encode($arrayinfo);
    }


    public function get_list()
    {
        $params = $this->input->post();
        $getdata = $this->ModelGeneral->get_cotizacion($params);
        $totaldata = $this->ModelGeneral->get_cotizacion_total($params);
        $json_data = array(
            "draw"            => intval($params['draw']),  
            "recordsTotal"    => intval($totaldata),
            "recordsFiltered" => intval($totaldata),
            "data"            => $getdata->result(),
            "query"           => $this->db->last_query()
        );
        echo json_encode($json_data);
    }


    public function delete()
    {
        $id = $this->input->post('id');
        $this->ModeloCatalogos->updateCatalogo('cotizaciones', array('activo' => 0), array('idCotizaciones' => $id));
    }

    function get_data_productos()
    {
        $id = $this->input->post('id');
        $arrayData = array('cotizacionesId' => $id, 'activo' => 1);
        $results = $this->ModeloCatalogos->getselectwhere_n_consulta('cotizaciones_productos', $arrayData);
        echo json_encode($results);
    }

    function get_data_servicios()
    {
        $id = $this->input->post('id');
        $arrayData = array('cotizacionesId' => $id, 'activo' => 1);
        $results = $this->ModeloCatalogos->getselectwhere_n_consulta('cotizaciones_servicios', $arrayData);
        echo json_encode($results);
    }

    function get_data_clientes()
    {
        $id = $this->input->post('id');
        $arrayData = array('clienteId' => $id, 'activo' => 1);
        $results = $this->ModeloCatalogos->getselectwhere_n_consulta('clientes', $arrayData);
        echo json_encode($results);
    }

    public function getDataProductoServicio()
    {
        $html = "";
        $id = $this->input->post("id");
        $dataProducto = $this->ModeloCatalogos->getProductoDataModal($id);

        $dataServicio = $this->ModeloCatalogos->getServicioDataModal($id);

        foreach ($dataProducto->result() as $key) {
            $html .= "<tr>
                        <td>" . $key->cod . "</td>
                        <td>" . $key->nombre . "</td>
                        <td>" . $key->p_unitario . "</td>
                        <td>" . $key->cantidad . "</td>
                        <td>" . $key->total . "</td>
                    </tr>";
        }

        foreach ($dataServicio->result() as $key) {
            $html .= "<tr>
                        <td>" . $key->cod . "</td>
                        <td>" . $key->nombre . "</td>
                        <td>" . $key->p_unitario . "</td>
                        <td>" . $key->cantidad . "</td>
                        <td>" . $key->total . "</td>
                    </tr>";
        }

        echo $html;
    }

    public function delete_registro_producto(){
        $id = $this->input->post('id');
        $data = array('activo' => 0);
        $this->ModeloCatalogos->updateCatalogo('cotizaciones_productos',$data,array('idProductos'=>$id));
    }

    public function delete_registro_servicio(){
        $id = $this->input->post('id');
        $data = array('activo' => 0);
        $this->ModeloCatalogos->updateCatalogo('cotizaciones_servicios',$data,array('idServicios'=>$id));
    }

    function get_cotizacion_productos_cod(){
        $usu = $this->input->get('search');
        $results=$this->ModeloCatalogos->cotizacion_producto_cod_search($usu);
        echo json_encode($results->result());
    }

    function get_cotizacion_productos_nom(){
        $usu = $this->input->get('search');
        $results=$this->ModeloCatalogos->cotizacion_producto_nom_search($usu);
        echo json_encode($results->result());
    }

    function get_data_cotizacion_producto(){
        $id = $this->input->post('id');
        $arrayData = array('productoId' => $id, 'activo' => 1);
        $results = $this->ModeloCatalogos->getselectwhere_n_consulta('producto', $arrayData);
        echo json_encode($results);
    }

    //-------------

    function get_cotizacion_servicios_cod(){
        $usu = $this->input->get('search');
        $results=$this->ModeloCatalogos->cotizacion_servicio_cod_search($usu);
        echo json_encode($results->result());
    }

    function get_cotizacion_servicios_nom(){
        $usu = $this->input->get('search');
        $results=$this->ModeloCatalogos->cotizacion_servicio_nom_search($usu);
        echo json_encode($results->result());
    }

    function get_data_cotizacion_servicio(){
        $id = $this->input->post('id');
        $arrayData = array('id' => $id, 'activo' => 1);
        $results = $this->ModeloCatalogos->getselectwhere_n_consulta('servicios', $arrayData);
        echo json_encode($results);
    }

    //----

    public function formato_cotizacion($id = 0)
    {
        $resultCot = $this->ModeloCatalogos->getselectwhere('cotizaciones', 'idCotizaciones', $id);
        $data['cotizacion']  = $resultCot;

        foreach ($resultCot as $item) {
            $data['noCotizacion'] = $item->idCotizaciones;
            $data['usuarioId'] = $item->usuarioId;

            $data['fecha'] = $item->fecha;
            $data['correo'] = $item->correo;
            $data['telefono'] = $item->telefono;
            $data['observaciones'] = $item->observaciones;

            $data['subtotal'] = $item->subtotal;
            $data['iva'] = $item->iva;
            $data['total'] = $item->total;
        }

        $resultUsu = $this->ModeloCatalogos->getselectwhere('personal', 'personalId', $data['usuarioId']);
        foreach ($resultUsu as $item) {
            $data['usuario'] = $item->nombre.' '.$item->apellido_paterno.' '.$item->apellido_materno;
        }

        $data['productos'] = $this->ModeloCatalogos->getProductoDataModal($id);

        $data['servicios'] = $this->ModeloCatalogos->getServicioDataModal($id);

        $this->load->view('Reportes/documento_productos_servicios', $data);
    }

    public function updateEstatus(){
        $id = $this->input->post('id');
        $this->ModeloCatalogos->updateCatalogo('cotizaciones', array('estatus' => 1), array('idCotizaciones' => $id));
    }


    public function tabla_prod_serv()
    {

        $html = "";
        $id = $this->input->post("id");
        $check = "";
        $data = $this->ModeloCatalogos->getselectwhere('cotizaciones', 'idCotizaciones', $id);
        $check_producto = false;
        $dataProducto = $this->ModeloCatalogos->getProductoDataModal($id);
        $check_servicio = false;
        $dataServicio = $this->ModeloCatalogos->getServicioDataModal($id);

        foreach ($data as $key) {
            foreach ($key as $value => $val) {

                if($value === 'clienteId'){
                    $html .= "<input type='hidden' id='id_cliente_acep' value='".$val."' readonly>";
                }

                if($value === 'check_producto' && $val === "on"){
                    $check_producto = true;
                }

                if($value === 'check_servicio' && $val === "on"){
                    $check_servicio = true;
                }
            }
        }

        if($check_producto){
            foreach ($dataProducto->result() as $key) {
                //<td>" . $key->cod . "</td>
                $html .= "<tr>
                    <td>" . $key->nombre . "</td>
                    <td style='color:red; text-align: center;'>" . $key->stock . "</td>
                    <td style='text-align: center;'>" . $key->cantidad . "</td>
                    <td style='text-align: center;'>" . $key->p_unitario . "</td>
                    <td style='text-align: center;'>" . $key->total . "</td>
                </tr>";
            }
        }
        

        if($check_servicio){
            foreach ($dataServicio->result() as $key) {
                //<td>" . $key->cod . "</td>
                $html .= "<tr>
                    <td>" . $key->nombre . "</td>
                    <td style='color:green; text-align: center;'> N/A </td>
                    <td style='text-align: center;'>" . $key->cantidad . "</td>
                    <td style='text-align: center;'>" . $key->p_unitario . "</td>
                    <td style='text-align: center;'>" . $key->total . "</td>
                </tr>";
            }
        }
        

        echo $html;
    }

    public function registrar_cotizacion(){
        $id = $this->input->post('id');
        $metopago = $this->input->post('mpago');
        $total_p_total = $this->input->post('total_p_total');
        $descuento = $this->input->post('descuento');
        $cantdescuento = $this->input->post('cantdescuento');
        
        $check_servicio = $this->input->post('check_servicio');
        $fecha_servicio = $this->input->post('fecha_servicio');
        $hora_servicio = $this->input->post('hora_servicio');
        $unidad_servicio = $this->input->post('unidad_servicio');
        $factura = $this->input->post('factura');

        $data = $this->ModeloCatalogos->getselectwhere('cotizaciones', 'idCotizaciones', $id);
        foreach ($data as $key){
            $cliente = $key->clienteId;
            //$uncluir=$z->uncluir;
            $subtotal_p = $key->subtotal;
        }


        $validar = 0;
        $dataProducto = $this->ModeloCatalogos->getProductoDataModal($id);
        foreach ($dataProducto->result() as $key){
            //$idproducto = $key->idProductos;
            $cantidad = $key->cantidad;
            $stock = $key->stock;
            if($cantidad >= $stock){
                $validar = 1 ;
            }
        }


        $id_venta=0;
        if($validar == 0){
            $data = array('estatus'=>1);

            $this->ModeloCatalogos->updateCatalogo('cotizaciones', $data, array('idCotizaciones' => $id));

            $usuario = $_SESSION['usuarioid'];
            $mpago = $metopago;
            $desc = $descuento;
            $descu = $cantdescuento;
            

                $sbtotal = $subtotal_p;
                $total = $total_p_total;


            $data_info = array(
                'id_personal' => $usuario,
                'id_cliente' => $cliente,
                'metodo' => $mpago,
                'subtotal' => $sbtotal,
                'descuento' => $desc,
                'descuentocant' => $descu,
                'monto_total' => $total,
                'id_cotizacion' => $id,
                'factura' => $factura,
            );


            if($check_servicio === 'true'){
                $data_info = array_merge($data_info, 
                array('check_servicio' => 'on', 'fecha_servicio' => $fecha_servicio, 'hora_servicio' => $hora_servicio, 'unidad_servicio' => $unidad_servicio));
            }

            $id_venta = $this->ModeloCatalogos->Insert('ventas', $data_info);
        }

        $array_venta = array('validar'=>$validar,'id_venta'=>$id_venta);
        echo json_encode($array_venta);
    }


    function ingresarventapro(){
        $id = $this->input->post('id');
        $idcot = $this->input->post('idcot');

        
        $dataProducto = $this->ModeloCatalogos->getProductoDataModal($idcot);
        foreach ($dataProducto->result() as $key) {
            $idventa = $id;

            $producto = $key->codigo;
            $servicio  = 0;
            $cantidad = $key->cantidad;
            $precio = $key->p_unitario;

            $this->ModeloCatalogos->ingresarventad($idventa,$producto,$servicio,$cantidad,$precio);
            $this->ModeloCatalogos->stock_descontar_producto($cantidad,$producto);
        }

//-------------------

        $dataServicio = $this->ModeloCatalogos->getServicioDataModal($idcot);
        foreach ($dataServicio->result() as $key) {
            $idventa = $id;

            $producto  = 0;
            $servicio = $key->codigo;
            $cantidad = $key->cantidad;
            $precio = $key->p_unitario;

            $this->ModeloCatalogos->ingresarventad($idventa,$producto,$servicio,$cantidad,$precio);
        }

    }

    function get_unidades(){
        $id = $this->input->post('id');

        $data = $this->ModeloCatalogos->getselectwhere_n_consulta('unidades', array('clienteId'=> $id, 'activo'=>1));

        echo json_encode($data);
    }

    public function get_listado_unidades(){
        $params = $this->input->post();
        $id = $this->input->post('cliente');
        $getdata = $this->ModeloCatalogos->get_unidades($params, $id);
        $totaldata= $this->ModeloCatalogos->total_unidades($params, $id); 
        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($totaldata),  
            "recordsFiltered" => intval($totaldata),
            "data"            => $getdata->result(),
            "query"           =>$this->db->last_query()   
        );
        echo json_encode($json_data);
    }

    function delete_unidad(){
        $params = $this->input->post();
        $unidadId=$params['unidadId'];
        $this->ModeloCatalogos->updateCatalogo('unidades',array('activo'=>0),array('unidadId'=>$unidadId));
    }  

    public function add_unidad(){
        $params = $this->input->post();
        $unidadId = $params['unidadId'];
        unset($params['unidadId']);
        if ($unidadId>0) {
            $this->ModeloCatalogos->updateCatalogo('unidades',$params,array('unidadId'=>$unidadId));
            $id=$unidadId;
        }else{
            $id=$this->ModeloCatalogos->Insert('unidades',$params);
        }
        echo $id;
    }

    function registro_clausulas(){
        $datos = $this->input->post('data');
        $DATA = json_decode($datos);
        for ($i=0;$i<count($DATA);$i++) {   
            $data['idCotizaciones']=$DATA[$i]->idCotizaciones;
            $data['clausulas']=$DATA[$i]->clausulas;
            if($DATA[$i]->id==0){
                $this->ModeloCatalogos->Insert('cotizaciones_clausulas',$data);
            }else{
                $this->ModeloCatalogos->updateCatalogo('cotizaciones_clausulas',$data,array('id'=>$DATA[$i]->id));
            }
        }   
    }

}