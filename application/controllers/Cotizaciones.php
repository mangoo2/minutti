<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cotizaciones extends CI_Controller {
	public function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('ModelGeneral');
        $this->load->model('ModeloCatalogos');
    }
	public function index(){
		$this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('cotizaciones/cotizaciones');
        $this->load->view('templates/footer');
        $this->load->view('cotizaciones/cotizacionesjs');
	}
    
    public function getlist(){
        $params = $this->input->post();
        $getdata = $this->ModelGeneral->get_cotizaciones($params);
        $totaldata= $this->ModelGeneral->total_get_cotizaciones($params); 
        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($totaldata),  
            "recordsFiltered" => intval($totaldata),
            "data"            => $getdata->result(),
            "query"           =>$this->db->last_query()   
        );
        echo json_encode($json_data);
    }

    function detallesCotizacion(){
        $id = $this->input->post("id");
        $result = $this->ModeloCatalogos->getselectwheren('config_interiores_web',array('id_datos'=>$id));
        $html='<table class="table thead-inverse" id="table_detalles">
                    <thead>
                        <tr>
                            <th>TIPO</th>
                            <th>SELECCIONADO</th>
                        </tr>
                    </thead>
                    <tbody>
                ';
        $tipo=""; $valor="";
        foreach ($result->result() as $i) {
            if($i->nombre_config=="tipou"){
                $tipo='UNIDAD';
                if($i->valor=="1"){
                    $valor='SPRINTER';
                }else{
                    $valor='CRAFTER';
                }
            }
            if($i->nombre_config=="tipoasi"){
                $tipo='TIPO DE ASIENTO';
                $valor=strtoupper($i->valor);
            }
            if($i->nombre_config=="num_asientos"){
                $tipo='NÚMERO DE ASIENTO(S)';
                $valor=$i->valor;
            }
            if($i->nombre_config=="color_asi"){
                $tipo='COLOR DE ASIENTO';
                $valor='<div class="col-sm-3 anim-hover-translate-top-10px transition-3ms">
                            <img id="c1" class="col_asi img-responsive img-thumbnail" src="'.base_url().'public/img/colores/'.$i->valor.'.jpg" />
                        </div>';
            }
            if($i->nombre_config=="color_pi"){
                $tipo='COLOR DE PISO';
                $valor='<div class="col-sm-3 anim-hover-translate-top-10px transition-3ms">
                            <img id="c1" class="col_asi img-responsive img-thumbnail" src="'.base_url().'public/img/colores/'.$i->valor.'.jpg" />
                        </div>';
            }
            for($j=1; $j<8; $j++){
                if($i->nombre_config=="conf_av".$j.""){
                    $nombre="";
                    $tipo='CONF. INTERIORES - AUDIO Y VIDEO';
                    $checked="";
                    if($i->valor=="1") { $checked="checked"; } else { $checked=""; }
                    if($i->nombre_config=="conf_av1") $nombre="<label>Sistema de entretenimiento 1</label>";
                    else if($i->nombre_config=="conf_av2") $nombre="<label>Sistema de entretenimiento 2</label>";
                    else if($i->nombre_config=="conf_av3") $nombre="<label>Cámara de reversa</label>";
                    else if($i->nombre_config=="conf_av4") $nombre="<label>Cámara de reversa básica</label>";
                    else if($i->nombre_config=="conf_av5") $nombre="<label>Cámaras internas</label>";
                    else if($i->nombre_config=="conf_av6") $nombre="<label>Audio básico</label>";
                    else if($i->nombre_config=="conf_av7") $nombre="<label>Audio básico</label>";
                    $valor='<div class="col-md-6">'.$nombre.'</div>
                            <label class="switch">
                                <input disabled name="conf_av" id="conf_av'.$j.'" type="checkbox" '.$checked.' >
                                <span class="sliderN round"></span>
                            </label>';
                }
            }
            for($k=1; $k<4; $k++){
                if($i->nombre_config=="conf_usb".$k.""){
                    $tipo='CONF. INTERIORES - USB Y ELECTRÓNICOS';
                    $checked=""; $nombre="";
                    if($i->valor=="1") { $checked="checked"; } else { $checked=""; }
                    if($i->nombre_config=="conf_usb1") $nombre="<label>Puntos de carga USB</label>";
                    else if($i->nombre_config=="conf_usb2") $nombre="<label>Puntos de carga USB Importados</label>";
                    else if($i->nombre_config=="conf_usb3") $nombre="<label>Contactos 110</label>";
                    $valor='<div class="col-md-6">'.$nombre.'</div>
                            <label class="switch">
                                <input disabled name="conf_av" id="conf_usb'.$k.'" type="checkbox" '.$checked.' >
                                <span class="sliderN round"></span>
                            </label>';
                }
            }

            $html.='<tr>
                        <td>'.$tipo.'</td>
                        <td>'.$valor.'</td>
                    </tr>';
        }
        $html.='</tbody>
                </table>';
        echo $html;
    }
 
}