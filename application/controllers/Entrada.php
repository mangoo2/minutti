<?php 

defined('BASEPATH') OR exit('No direct script access allowed');

class Entrada extends CI_Controller {
    function __construct(){
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('ModeloCatalogos');
        $this->load->model('ModeloProductos');
        $this->load->model('ModeloRetornoMaterial');
        $this->load->model('ModeloRequisicion');
        $this->load->model('ModelGeneral');
        if($this->session->userdata('logeado')==true){
            $this->perfilid =$this->session->userdata('perfilid');
            $this->idpersonal = $this->session->userdata('idpersonal');
            $this->sucursal = $this->session->userdata('sucursal');
            
        }else{
            redirect('Sistema'); 
        }
        date_default_timezone_set('America/Mexico_City');
        $this->fechahoy = date('Y-m-d G:i:s');
        $this->fechaactual = date('Y-m-d');
    }

    public function index(){
            $this->load->view('templates/header');
            $this->load->view('templates/navbar');
            $this->load->view('entrada/entrada');
            $this->load->view('templates/footer');
    }
    public function ingresa_orden($id=0){
        $get_nooc=$this->ModelGeneral->get_nooc();
        $get_nooc_aux=$get_nooc+1;    
        if($id==0){
            $data['fecha_compra']=$this->fechaactual;
            $data['idcompra']=0;
            $data['no_oc']=$get_nooc_aux;
            $data['idproveedor']=0;
            $data['personalId']=0;
            $data['fecha_surtimiento']='';
            $data['comentarios']='';
            $data['supplir']='';
            $data['employee']='';
            $data['idbancario']='';
            $data['tipo_compra']='';
            $data['metodo_pago']='';
            $data['total']=0;
        }else{
            $resul=$this->ModeloCatalogos->getselectwhere('orden_compra','id',$id);
            foreach ($resul as $item){
                $data['idcompra']=$item->id;
                $data['no_oc']=$item->no_oc;
                $data['idproveedor']=$item->idproveedor;
                $get_supplir=$this->ModeloCatalogos->getselectwhere_row('proveedor',array('idproveedor'=>$item->idproveedor));
                $data['supplir']=$get_supplir->nombre;
                $data['personalId']=$item->personalId;
                $get_employee=$this->ModeloCatalogos->getselectwhere_row('personal',array('personalId'=>$item->personalId));
                if($get_employee==null){
                    $data['employee']='';
                }else{
                    $data['employee']=$get_employee->nombre.' '.$get_employee->apellido_paterno.' '.$get_employee->apellido_materno;
                }
                $data['fecha_compra']=$item->fecha_compra;
                $data['fecha_surtimiento']=$item->fecha_surtimiento;
                $data['comentarios']=$item->comentarios;
                $data['idbancario']=$item->idbancario;
                $data['tipo_compra']=$item->tipo_compra;
                $data['metodo_pago']=$item->metodo_pago;
                $data['total']=$item->total;
            }
        }    
        $this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('entrada/ingresa_orden',$data);
        $this->load->view('templates/footer');
        $this->load->view('entrada/ingresa_ordenjs');
    }
    public function retorno_material(){
            $this->load->view('templates/header');
            $this->load->view('templates/navbar');
            $this->load->view('entrada/retorno_material_list');
            $this->load->view('templates/footer');
            $this->load->view('entrada/retorno_material_listjs');
    }
    public function getlist_rm_row() {
        $params = $this->input->post();
        $getdata = $this->ModeloRetornoMaterial->getlist_row($params);
        $totaldata= $this->ModeloRetornoMaterial->getlist_row_t($params); 
        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($totaldata),  
            "recordsFiltered" => intval($totaldata),
            "data"            => $getdata->result(),
            "query"           =>$this->db->last_query()   
        );
        echo json_encode($json_data);
    }
    public function retorno_material_add(){
            $this->load->view('templates/header');
            $this->load->view('templates/navbar');
            $this->load->view('entrada/retorno_material');
            $this->load->view('templates/footer');
            $this->load->view('entrada/retorno_materialjs');
    }
    function retorno_material_insertupdate(){
        $params = $this->input->post();
        $retornoId = $params['retornoId'];
        $arrayproductos = $params['arrayproductos'];
        unset($params['retornoId']);
        unset($params['arrayproductos']);
        if($retornoId>0){
        }else{
            $retornoId = $this->ModeloCatalogos->Insert('retorno_material',$params);
        }

        $DATAc = json_decode($arrayproductos); 
        for ($i=0;$i<count($DATAc);$i++) {
            $sotrosdId=$DATAc[$i]->sotrosdId;
            if($sotrosdId>0){
            }else{
                $ordenIdd= $this->ModeloCatalogos->Insert('retorno_material_d',array('retornoId'=>$retornoId,'cantidad'=>$DATAc[$i]->cantidad,'productoId'=>$DATAc[$i]->productoId));
            }
            
        }
    }


    public function listadocompras(){
        $this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('entrada/listadocompra');
        $this->load->view('templates/footer');
        $this->load->view('entrada/listadocomprajs');
    }
    
    public function completar($id){
        $data['sucursalrow']=$this->ModeloCatalogos->getselectwheren('sucursal',array('activo'=>1));
        $resul=$this->ModeloCatalogos->getselectwhere('orden_compra','id',$id);
        foreach ($resul as $item){
            $data['idcompra']=$item->id;
            $data['no_oc']=$item->no_oc;
            $data['idproveedor']=$item->idproveedor;
            $get_supplir=$this->ModeloCatalogos->getselectwhere_row('proveedor',array('idproveedor'=>$item->idproveedor));
            $data['supplir']=$get_supplir->nombre;
            $data['personalId']=$item->personalId;
            $get_employee=$this->ModeloCatalogos->getselectwhere_row('personal',array('personalId'=>$item->personalId));
            //var_dump($get_employee);die;
            if($get_employee==null){
                $data['employee']='';
            }else{
                $data['employee']=$get_employee->nombre.' '.$get_employee->apellido_paterno.' '.$get_employee->apellido_materno;
            }
            $data['fecha_compra']=$item->fecha_compra;
            $data['fecha_surtimiento']=$item->fecha_surtimiento;
            $data['comentarios']=$item->comentarios;
            $data['idbancario']=$item->idbancario;
            $data['tipo_compra']=$item->tipo_compra;
            $data['metodo_pago']=$item->metodo_pago;
            $data['total']=$item->total;
            $data['requsicionId']=$item->requsicionId;
            $data['destino']=0;
            $resul_req=$this->ModeloCatalogos->getselectwhere('requisicion_material','requsicionId',$item->requsicionId);
            foreach ($resul_req as $x){
                $data['destino']=$x->destino;
            }
        } 
        $this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('entrada/ingresa_orden2',$data);
        $this->load->view('templates/footer');
        $this->load->view('entrada/ingresa_orden2js');
    }

    public function search_supplier(){
       $search = $this->input->get('search');
        $results=$this->ModelGeneral->getselect_where_like_supplir($search);
        echo json_encode($results);    
    }

    public function search_employee(){
       $search = $this->input->get('search');
        $results=$this->ModelGeneral->getselect_where_like_emplpyee($search);
        echo json_encode($results);    
    }

    public function add_orden_compra(){
        $data=$this->input->post();
        $id=$data['idcompra'];
        unset($data['idcompra']);
        if(!isset($data['tipo_compra'])){
            $data['tipo_compra']='';
        }
        if($id==0){
            $get_nooc=$this->ModelGeneral->get_nooc();
            $get_nooc_aux=$get_nooc+1;   
            $data['no_oc']=$get_nooc_aux;
            $data['personalId_realizo']=$this->idpersonal;
            $id=$this->ModeloCatalogos->Insert('orden_compra',$data);
        }else{
            $this->ModeloCatalogos->updateCatalogo('orden_compra',$data,array('id'=>$id));
        }
        echo $id;
    }

    public function add_orden_compra_aceptar(){
        $data=$this->input->post();
        $id=$data['idcompra'];
        unset($data['idcompra']);  
        $data['estatus']=2;
        $this->ModeloCatalogos->updateCatalogo('orden_compra',$data,array('id'=>$id));
        echo $id;
    }

    function add_detalle_product(){
        $datos = $this->input->post('data');
        $DATA = json_decode($datos);
        for ($i=0;$i<count($DATA);$i++) {   
            $data['idorden']=$DATA[$i]->idorden;    
            $data['cantidad']=$DATA[$i]->cantidad;        
            $data['idproducto']=$DATA[$i]->idproducto;    
            $data['unidad']=$DATA[$i]->unidad;
            if(isset($DATA[$i]->requsiciondId)){
                $data['requsiciondId']=$DATA[$i]->requsiciondId;    
            }
            if(isset($DATA[$i]->total)){
                $data['total']=$DATA[$i]->total;    
            }
            $data['costo']=$DATA[$i]->costo;    
            if($DATA[$i]->id==0){
                $this->ModeloCatalogos->Insert('orden_compra_detalle',$data);
            }else{
                $this->ModeloCatalogos->updateCatalogo('orden_compra_detalle',$data,array('id'=>$DATA[$i]->id));
            }
            $rv=$this->ModelGeneral->get_ultimo_costo_proveedor($DATA[$i]->idproducto,$DATA[$i]->idproveedor);
            if($rv==1){
                $this->ModelGeneral->update_ultimo_costo_proveedor($DATA[$i]->costo,$DATA[$i]->idproducto,$DATA[$i]->idproveedor);
            }else{
                $arrayultimo = array('costo'=>$DATA[$i]->costo,'productoId'=>$DATA[$i]->idproducto,'idproveedor'=>$DATA[$i]->idproveedor);
                $this->ModeloCatalogos->Insert('ultimo_costo_proveedor',$arrayultimo);
            }
        }
    }

    function add_detalle_product_update(){
        $datos = $this->input->post('data');
        $DATA = json_decode($datos);
        for ($i=0;$i<count($DATA);$i++) {     
            $data['cantidad_recibida']=$DATA[$i]->cantidad_recibida;  
            $data['diferencia']=$DATA[$i]->diferencia;    
            $this->ModeloCatalogos->updateCatalogo('orden_compra_detalle',$data,array('id'=>$DATA[$i]->id));
        }
    }

    function add_detalle_product_nave(){
        $datos = $this->input->post('data');
        $DATA = json_decode($datos);
        for ($i=0;$i<count($DATA);$i++) {   
            $DATAcd=$DATA[$i]->productos_stock;
            for ($j=0;$j<count($DATAcd);$j++) {
                $data['idorden']=$DATAcd[$j]->idorden;    
                $data['idordendetalle']=$DATAcd[$j]->idordendetalle;    
                $data['idproducto']=$DATAcd[$j]->idproducto;    
                $data['sucursalId']=$DATAcd[$j]->sucursalId; 
                $data['stock']=intval($DATAcd[$j]->stock);
                $idproducto=$DATAcd[$j]->idproducto;    
                $sucursalId=$DATAcd[$j]->sucursalId; 
                $stock=intval($DATAcd[$j]->stock);   
                if($DATAcd[$j]->idstock==0){
                    $this->ModeloCatalogos->Insert('orden_compra_detalle_stock',$data);
                    /// incremertar el stock de la nave
                    $this->ModelGeneral->incrementar_stock_producto_sucursal($idproducto,$sucursalId,$stock);
                }else{
                    $result_detalle_compra=$this->ModeloCatalogos->getselectwhere_n_consulta('orden_compra_detalle_stock',array('id'=>$DATAcd[$j]->idstock,'idproducto'=>$idproducto,'sucursalId'=>$sucursalId));
                    foreach ($result_detalle_compra as $x){
                        $this->ModelGeneral->descontar_stock_producto_sucursal($idproducto,$sucursalId,$x->stock); 
                    }
                    $this->ModelGeneral->incrementar_stock_producto_sucursal($idproducto,$sucursalId,$stock);
                    //
                    $this->ModeloCatalogos->updateCatalogo('orden_compra_detalle_stock',$data,array('id'=>$DATAcd[$j]->idstock));
                } 

            }
        }
    }

    public function search_product(){
       $search = $this->input->get('search');
        $results=$this->ModelGeneral->getselect_where_like_product($search);
        echo json_encode($results);    
    }
    
    public function getlistado_compras(){
        $params = $this->input->post();
        $getdata = $this->ModelGeneral->get_orden_compra($params);
        $totaldata= $this->ModelGeneral->total_get_orden_compra($params); 
        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($totaldata),  
            "recordsFiltered" => intval($totaldata),
            "data"            => $getdata->result(),
            "query"           =>$this->db->last_query()   
        );
        echo json_encode($json_data);
    }
    
    public function get_detalle_product()
    {   
        $id = $this->input->post('id');
        $html='<table class="table thead-inverse">
                    <thead>
                        <tr>
                            <th>CÓDIGO</th>
                            <th>PRODUCTO</th>
                            <th>UNIDAD</th>
                            <th>SUBTOTAL</th>
                        </tr>
                    </thead>
                    <tbody>';
                    $result=$this->ModelGeneral->get_detalle_producto($id);
                    foreach ($result as $x){
                        $html.='<tr>
                            <td>'.$x->codigo.'</td>
                            <td>'.$x->nombre.'</td>
                            <td>'.$x->unidad.'</td>
                            <td>'.$x->total.'</td>
                        </tr>';
                    }
            $html.='</tbody>
            </table>';
        echo $html;    
    }

    function get_tabla_detalles_product()
    {
        $id = $this->input->post('id');
        $results=$this->ModelGeneral->get_detalle_producto($id,0);
        echo json_encode($results);
    }
    

    public function get_detalle_nave()
    {   
        $id = $this->input->post('id');
        $cant = $this->input->post('cant');
        $idprod = $this->input->post('idprod');
        $requsicionId = $this->input->post('requsicionId');
        $destino = $this->input->post('destino');
        $cantidad = $this->input->post('cantidad');
        $idcompra_orden = $this->input->post('idcompra_orden');
        $result=$this->ModeloCatalogos->getselectwhere('sucursal','activo',1);
        $html='';
        foreach ($result as $x){
            $get_pro=$this->ModeloCatalogos->getselectwhere_row('orden_compra_detalle_stock',array('idorden'=>$idcompra_orden,'idordendetalle'=>$id,'idproducto'=>$idprod,'sucursalId'=>$x->sucursalId));
            $stock=0;
            $idstock=0;
            if(isset($get_pro->stock)){
                $stock=$get_pro->stock;
                $idstock=$get_pro->id;;
            }else{
                if($requsicionId>0){
                    if($x->sucursalId==$destino){
                        $stock=$cantidad;
                    }else{
                        $stock='';
                    }   
                }else{    
                    $stock='';
                    
                }
                $idstock=0;
            }
            $html.='<span class="row_d_'.$cant.'">
                        <input type="hidden" id="idstockx" value="'.$idstock.'">
                        <input type="hidden" id="idordendetallex" value="'.$id.'">
                        <input type="hidden" id="idproductodx" value="'.$idprod.'">
                        <input type="hidden" id="sucursalIdx" value="'.$x->sucursalId.'">
                        <input type="number" id="stockxv" class="form-control idsucu_'.$x->sucursalId.'_'.$idprod.'" value="'.$stock.'" placeholder="'.$x->sucursal.'" style="width: 95px;height: 32px;" oninput="validar_suma('.$x->sucursalId.','.$idprod.','.$cant.')">
                    </span>';

        }
        echo $html;    
    }

    public function delete_compras(){
        $id = $this->input->post('id');
        $this->ModeloProductos->comprasdelete($id); 
    } 


    public function requisicion()
    {
        $this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('entrada/requisicionlist');
        $this->load->view('templates/footer');
        $this->load->view('entrada/requisicionlistjs');
    }

    public function getlist_requision() {
        $params = $this->input->post();
        $getdata = $this->ModeloRequisicion->getlist_row($params);
        $totaldata= $this->ModeloRequisicion->getlist_row_t($params); 
        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($totaldata),  
            "recordsFiltered" => intval($totaldata),
            "data"            => $getdata->result(),
            "query"           =>$this->db->last_query()   
        );
        echo json_encode($json_data);
    }

    public function compra_requisicion($id=0)
    {   
        $get_nooc=$this->ModelGeneral->get_nooc();
        $get_nooc_aux=$get_nooc+1;    
        $data['fecha_compra']=$this->fechaactual;
        $data['idcompra']=0;
        $data['requsicionId']=$id;
        $data['no_oc']=$get_nooc_aux;
        $data['idproveedor']=0;
        $data['personalId']=0;
        $data['fecha_surtimiento']='';
        $data['comentarios']='';
        $data['supplir']='';
        $data['employee']='';
        $data['idbancario']='';
        $data['tipo_compra']='';
        $data['metodo_pago']='';
        $data['total']='';
        $data['sucursal']='';
        $resul_req=$this->ModeloCatalogos->getselectwhere('requisicion_material','requsicionId',$id);
        foreach ($resul_req as $x){
            $resul_suc=$this->ModeloCatalogos->getselectwhere('sucursal','sucursalId',$x->destino);
            foreach ($resul_suc as $y){
                $data['sucursal']=$y->sucursal;
            }
        }
        $this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('entrada/compra_requisicion',$data);
        $this->load->view('templates/footer');
        $this->load->view('entrada/compra_requisicionjs');
    }

    public function add_requisicion_compra(){
        $data=$this->input->post();
        unset($data['idcompra']);
        if(!isset($data['tipo_compra'])){
            $data['tipo_compra']='';
        }
        $get_nooc=$this->ModelGeneral->get_nooc();
        $get_nooc_aux=$get_nooc+1;   
        $data['no_oc']=$get_nooc_aux;
        $id=$this->ModeloCatalogos->Insert('orden_compra',$data);
        $requsicionId=$data['requsicionId'];
        $this->ModeloCatalogos->updateCatalogo('requisicion_material',array('estatus' =>1),array('requsicionId'=>$requsicionId));
        echo $id;
    }

    function get_tabla_requisicion_compra()
    {
        $id = $this->input->post('id');
        $results=$this->ModelGeneral->get_requisicion_material_detalle($id);
        echo json_encode($results);
    }

    function add_detalle_requisicion_compra(){
        $datos = $this->input->post('data');
        $DATA = json_decode($datos);
        for ($i=0;$i<count($DATA);$i++) {   
            $data['idorden']=$DATA[$i]->idorden;    
            $data['cantidad']=$DATA[$i]->cantidad;        
            $data['idproducto']=$DATA[$i]->idproducto;    
            $data['unidad']=$DATA[$i]->unidad;    
            if($DATA[$i]->id==0){
                $this->ModeloCatalogos->Insert('orden_compra_detalle',$data);
            }else{
                $this->ModeloCatalogos->updateCatalogo('orden_compra_detalle',$data,array('id'=>$DATA[$i]->id));
            }
        }
    }
    /// datos bancarios
    public function get_bacarios_proveedor(){
        $id=$this->input->post('id');
        $result=$this->ModeloCatalogos->getselectwhere_n_consulta('proveedor_bancarios',array('idproveedor' =>$id,'activo'=>1));
        $html='<select class="form-control" name="idbancario" id="idbancario">';
        foreach ($result as $x){
            $html.='<option value="'.$x->id.'">'.$x->banco.' - '.$x->titular.' - '.$x->cuenta.'</option>';         
        }
        $html.='</select>';
        echo $html;
    }

    public function get_detalle_nave_stock()
    {   
        $cant = $this->input->post('cant');
        $idprod = $this->input->post('idprod');

        $html='';
        $get_pro=$this->ModeloProductos->getProductosSucursales($idprod);
        foreach ($get_pro as $x){
            $stock=0;
            if(isset($x->stock)){
               $stock=$x->stock;
            }else{
               $stock=0;
            }
            //log_message('error', 'stock'.$stock);
            /// cambiar nombre de nave number_format($obed,2,'.',',')
            $nom_sucu='';
            if($x->sucursalId==1){
                $nom_sucu='N1'; 
            }else if($x->sucursalId==2){
                $nom_sucu='N2'; 
            }else if($x->sucursalId==3){
                $nom_sucu='N3'; 
            }else if($x->sucursalId==4){
                $nom_sucu='N4'; 
            }
            //log_message('error', 'nom_sucu'.$nom_sucu);

            $html.='<span class="row_st_'.$cant.'">
                        '.$nom_sucu.'-('.$stock.')<br>
                    </span>';
        }
        echo $html;    
    }
    
    public function documento_pdf_compra($id){
        $resul=$this->ModeloCatalogos->getselectwhere('orden_compra','id',$id);
        foreach ($resul as $x){
            $data['idcompra']=$x->id;
            $data['requsicionId']=$x->requsicionId;
            $data['idproveedor']=$x->idproveedor;
            $get_pro=$this->ModeloCatalogos->getselectwhere_row('proveedor',array('idproveedor'=>$x->idproveedor));
            if($get_pro==null){
                $data['proveedor']='';
                $data['celular']='';
            }else{
                $data['proveedor']=$get_pro->nombre;
                if($get_pro->celular!=''){
                    $data['celular']=$get_pro->celular;
                }else{
                    $data['celular']=$get_pro->tel_oficina;
                }
            }
            $data['metodo_pago']='';
            if($x->metodo_pago==1){
                $data['metodo_pago']='Efectivo';
            }else if($x->metodo_pago==2){
                $data['metodo_pago']='Tarjeta de crédito';
            }else if($x->metodo_pago==3){
                $data['metodo_pago']='Tarjeta de débito';
            }else if($x->metodo_pago==4){
                $data['metodo_pago']='Transferencia';
            }else if($x->metodo_pago==5){
                $data['metodo_pago']='Cheque';
            }
            $idnave=0; 
            if($x->requsicionId!=0){
               $get_re=$this->ModeloCatalogos->getselectwhere_row('requisicion_material',array('requsicionId'=>$x->requsicionId));
               $get_per=$this->ModeloCatalogos->getselectwhere_row('personal',array('personalId'=>$get_re->personalId));
               $idnave=$get_per->sucursalId;

            }
            $data['fecha_compra']=date('d/m/Y',strtotime($x->fecha_compra));
            $data['get_productos']=$this->ModelGeneral->get_detalle_producto($id,$idnave);
            $get_pro_ban=$this->ModeloCatalogos->getselectwhere_row('proveedor_bancarios',array('idproveedor'=>$x->idproveedor,'id'=>$x->idbancario));
            if($get_pro_ban==null){
                $data['banco']='';
                $data['clave']='';
            }else{
                $data['banco']=$get_pro_ban->banco;
                $data['clave']=$get_pro_ban->clave;
            }
            $data['comentarios']=$x->comentarios;

        }
        
        $this->load->view('Reportes/documento_compra',$data);
    }
}
?>