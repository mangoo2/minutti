<?php 

defined('BASEPATH') OR exit('No direct script access allowed');

class GastosPagos extends CI_Controller {
    function __construct(){
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('ModeloCatalogos');
        $this->load->model('ModeloGastosPagos');
        $this->load->model('ModelGeneral');
        if($this->session->userdata('logeado')==true){
            $this->perfilid =$this->session->userdata('perfilid');
            $this->idpersonal = $this->session->userdata('idpersonal');
            $this->sucursal = $this->session->userdata('sucursal');
            
        }else{
            redirect('Sistema'); 
        }
    }

    public function index(){
            $data['categoriarow']=$this->ModeloCatalogos->getselectwheren('categoria',array('activo'=>1));
            $data['unidadrow']=$this->ModeloCatalogos->getselectwheren('unidad',array('activo'=>1));
            $data['sucursalrow']=$this->ModeloCatalogos->getselectwheren('sucursal',array('activo'=>1));

            if($this->perfilid==1){
                $selectedsucursal=0;
                $selectedsucursal_view='block';
            }else{
                $selectedsucursal=$this->sucursal;
                $selectedsucursal_view='None';
            }
            $data['selectedsucursal']=$selectedsucursal;
            $data['selectedsucursal_view']=$selectedsucursal_view;

            $this->load->view('templates/header');
            $this->load->view('templates/navbar');
            $this->load->view('gastospagos/listado',$data);
            $this->load->view('templates/footer');
            $this->load->view('gastospagos/listadojs');
    }

    public function getlistado(){
        $params = $this->input->post();
        $getdata = $this->ModeloGastosPagos->get_gastos($params);
        $totaldata= $this->ModeloGastosPagos->total_gastos($params); 
        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($totaldata),  
            "recordsFiltered" => intval($totaldata),
            "data"            => $getdata->result(),
            "query"           =>$this->db->last_query()   
        );
        echo json_encode($json_data);
    }  

    public function deleteregistro(){
        $id = $this->input->post('id');
        $data = array('activo'=>0);
        $this->ModeloCatalogos->updateCatalogo('gastospagos',$data,array('id'=>$id));
    }

    public function registro_gasto(){
        $data=$this->input->post();
        $idgasto=$data['id'];
        unset($data['id']);
        if($idgasto==0){
            $data['usuario']=$this->idpersonal;
            $id=$this->ModeloCatalogos->Insert('gastospagos',$data);
        }else{
            $data['usuario']=$this->idpersonal;
            $this->ModeloCatalogos->updateCatalogo('gastospagos',$data,array('id'=>$idgasto));
            $id=$idgasto;
        }
        echo $id;
    }

    public function searchcategoria(){
        $usu = $this->input->get('search');
        $results=$this->ModeloGastosPagos->categoriasallsearch($usu);
        echo json_encode($results->result());
    }

    public function addcategoria(){
        $params = $this->input->post();
        $categoriaId = $params['categoriaId'];
        unset($params['categoriaId']);
        if ($categoriaId>0) {
            $this->ModeloCatalogos->updateCatalogo('categoria_cobro',$params,array('id'=>$categoriaId));
            $id=$categoriaId;
        }else{
            $id=$this->ModeloCatalogos->Insert('categoria_cobro',$params);
        }
        echo $id;
    }

    public function getlistado_categoria(){
        $params = $this->input->post();
        $getdata = $this->ModeloGastosPagos->get_categoria($params);
        $totaldata= $this->ModeloGastosPagos->total_categoria($params); 
        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($totaldata),  
            "recordsFiltered" => intval($totaldata),
            "data"            => $getdata->result(),
            "query"           =>$this->db->last_query()   
        );
        echo json_encode($json_data);
    }

    function delete_categoria(){
        $params = $this->input->post();
        $categoriaId=$params['categoriaId'];
        $this->ModeloCatalogos->updateCatalogo('categoria_cobro',array('activo'=>0),array('id'=>$categoriaId));
    } 

    function get_categoria_tipo()
    {
        $tipo=$this->input->post('tipo');
        $html='<select name="categoria" id="categoria" class="form-control"><option value="0" selected="" disabled="">Seleccionar una opción</option>';
            $result=$this->ModeloCatalogos->getselectwheren('categoria_cobro',array('activo'=>1,'tipo'=>$tipo));
            foreach ($result->result() as $x){
                $html.='<option value="'.$x->id.'">'.$x->nombre.'</option>';
            }   
            $html.='</select>';
        echo $html;
    }

}