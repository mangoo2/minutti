<?php 

defined('BASEPATH') OR exit('No direct script access allowed');

class General extends CI_Controller {
    function __construct(){
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('ModeloCatalogos');
        $this->load->model('ModeloProductos');

        date_default_timezone_set('America/Mexico_City');
        $this->fechahoy = date('Y-m-d');
        if($this->session->userdata('logeado')==true){
            $this->perfilid =$this->session->userdata('perfilid');
            $this->idpersonal = $this->session->userdata('idpersonal');
            $this->sucursal = $this->session->userdata('sucursal');
            
        }else{
            redirect('Sistema'); 
        }
    }

    public function index(){
            
    }
    function search_cliente(){
        $usu = $this->input->get('search');
        $results=$this->ModeloCatalogos->clientesallsearch($usu);
        echo json_encode($results->result());
    }
    function search_personal(){
        $usu = $this->input->get('search');
        $results=$this->ModeloCatalogos->personalallsearch($usu);
        echo json_encode($results->result());
    }
    function search_producto(){
        $usu = $this->input->get('search');
        $results=$this->ModeloCatalogos->productoallsearch($usu,$this->perfilid,$this->sucursal);
        echo json_encode($results->result());
    }
    function eliminarnotificaciontemporal(){
        $idpros = $this->input->post('idpros');

        $fechahoy = strtotime ( '+ 3 days' , strtotime ( $this->fechahoy ) ) ;
        $fechahoy = date ( 'Y-m-d' , $fechahoy );

        $this->ModeloCatalogos->updateCatalogo('producto_sucursal',array('fechanotificacion'=>$fechahoy),array('productosId'=>$idpros));
    }
}
?>