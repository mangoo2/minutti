<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Herramientas extends CI_Controller
{
  function __construct()
  {
    parent::__construct();
    $this->load->helper('url');
    $this->load->model('ModelGeneral');
    $this->load->model('ModeloHerramientas');
    date_default_timezone_set('America/Mexico_City');
  }

  public function index()
  {
    $this->load->view('templates/header');
    $this->load->view('templates/navbar');
    $this->load->view('herramientas/index');
    $this->load->view('templates/footer');
    $this->load->view('herramientas/index_js');
  }


  public function add($id = 0)
  {
    $data['id'] = $id;
    $data['title_heades'] = 'Nueva herramienta';
    $data['title_save'] = 'Guardar';

    if ($id > 0) {
      $data['title_heades'] = 'Editar herramienta';
      $data['title_save'] = 'Actualizar';

      $result = $this->ModeloHerramientas->getSelectWhere('herramientas', 'id_herramienta', $id);
      foreach ($result as $tool) {
        $data['medida'] = $tool->medida;
        $data['nombre'] = $tool->nombre;
        $data['ubicacion'] = $tool->ubicacion;
        $data['stock'] = $tool->stock;
      }
    }

    $this->load->view('templates/header');
    $this->load->view('templates/navbar');
    $this->load->view('herramientas/form', $data);
    $this->load->view('templates/footer');
    $this->load->view('herramientas/form_js');
  }

  public function insert()
  {
    $params = $this->input->post();
    $idHerramienta = $params['IDherramienta'];
    unset($params['IDherramienta']);

    if ($idHerramienta > 0) {
      $this->ModeloHerramientas->updateEntrance('herramientas', $params, array('id_herramienta' => $idHerramienta));
      $id = $idHerramienta;
    } else {
      $id = $this->ModeloHerramientas->insertEntrance('herramientas', $params);
    }
    echo $id;
  }


  public function getlist()
  {
    $params = $this->input->post();
    $getdata = $this->ModeloHerramientas->get_herramientas($params);
    $totaldata = $this->ModeloHerramientas->get_total_herramientas($params);
    $json_data = array(
      "recordsTotal"    => intval($totaldata),
      "recordsFiltered" => intval($totaldata),
      "data"            => $getdata->result(),
      "query"           => $this->db->last_query()
    );
    echo json_encode($json_data);
  }

  public function delete()
  {
    $id = $this->input->post('id');
    $this->ModeloHerramientas->updateEntrance('herramientas', array('activo' => 0), array('id_herramienta' => $id));
  }
}
