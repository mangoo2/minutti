<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Inicio extends CI_Controller {
	public function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('ModeloCatalogos');

        date_default_timezone_set('America/Mexico_City');
        $this->currentFecha = date('Y-m-d');

        if($this->session->userdata('logeado')==true){
            $this->perfilid =$this->session->userdata('perfilid');
            $this->idpersonal = $this->session->userdata('idpersonal');
            $this->sucursal = $this->session->userdata('sucursal');
        }else{
            redirect('Sistema'); 
        }

    }
	public function index(){
        if($this->perfilid == 5){
            $data['usuario'] = $_SESSION['usuarioname'];
            $data['fecha'] = $this->currentFecha;
            $data['idCliente'] = $_SESSION['idpersonal'];
            $quejas = $this->ModeloCatalogos->getQuejasClientes($_SESSION['idpersonal']);
            foreach ($quejas as $key) {
                $data['quejas'] = $key;
            }
            $this->load->view('templates/header');
            $this->load->view('templates/navbar');
            $this->load->view('inicio_cliente/index',$data);
            $this->load->view('templates/footer');
            $this->load->view('inicio_cliente/index_js');
        }else{
            $this->load->view('templates/header');
            $this->load->view('templates/navbar');
            $this->load->view('Catalogos/catalogos');
            $this->load->view('templates/footer');
        }
		
	}
    

    //Clientes-------------
    public function get_servicios_fecha()
    {
        $params = $this->input->post();
        $id = $this->input->post('id');
        $fecha = $this->currentFecha;
        $getdata = $this->ModeloCatalogos->get_agenda_servicios_fecha($params, $fecha, $id);
        $totaldata = $this->ModeloCatalogos->get_agenda_servicios_fecha_total($params, $fecha, $id);
        $json_data = array(
            "recordsTotal"    => intval($totaldata),
            "recordsFiltered" => intval($totaldata),
            "data"            => $getdata->result(),
            "query"           => $this->db->last_query()
        );
        echo json_encode($json_data);
    }

    public function get_servicios_historico()
    {
        $params = $this->input->post();
        $id = $this->input->post('id');
        $getdata = $this->ModeloCatalogos->get_agenda_servicios_historico($params, $id);
        $totaldata = $this->ModeloCatalogos->get_agenda_servicios_historico_total($params, $id);
        $json_data = array(
            "recordsTotal"    => intval($totaldata),
            "recordsFiltered" => intval($totaldata),
            "data"            => $getdata->result(),
            "query"           => $this->db->last_query()
        );
        echo json_encode($json_data);
    }

    public function get_servicios_activos()
    {
        $params = $this->input->post();
        $id = $this->input->post('id');
        $getdata = $this->ModeloCatalogos->get_agenda_servicios_activos($params, $id);
        $totaldata = $this->ModeloCatalogos->get_agenda_servicios_activos_total($params, $id);
        $json_data = array(
            "recordsTotal"    => intval($totaldata),
            "recordsFiltered" => intval($totaldata),
            "data"            => $getdata->result(),
            "query"           => $this->db->last_query()
        );
        echo json_encode($json_data);
    }


    public function getDataVehiculo()
    {
        $html = "";
        $id_cliente = $this->input->post("id_cliente");
        $id_venta = $this->input->post("id_venta");
        $data = $this->ModeloCatalogos->getDataVehiculo($id_cliente, $id_venta);

        foreach ($data->result() as $key) {
            $html .= "<tr>
                        <td>" . $key->placas . "</td>
                        <td>" . $key->modelo . "</td>
                        <td>" . $key->ano . "</td>
                    </tr>";
        }

        echo $html;
    }

    public function insertQueja(){
        $id = $this->input->post('clienteId');
        $data = $this->input->post();
        $this->ModeloCatalogos->updateCatalogo('clientes', $data, array('clienteId' => $id));
    }

}