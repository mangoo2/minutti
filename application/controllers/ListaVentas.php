<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ListaVentas extends CI_Controller {
    function __construct(){
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('ModeloVentas');
        $this->load->model('ModeloCatalogos');
        $this->load->model('ModelGeneral');
        if($this->session->userdata('logeado')==true){
            $this->perfilid =$this->session->userdata('perfilid');
            $this->idpersonal = $this->session->userdata('idpersonal');            
        }else{
            redirect('Sistema'); 
        }
        date_default_timezone_set('America/Mexico_City');
        $this->fechahoy = date('Y-m-d G:i:s');
        $this->fechaactual = date('Y-m-d');
    }
    public function index(){
        $get_personal=$this->ModeloCatalogos->get_records_condition('personalId='.$this->idpersonal,'personal');
        foreach ($get_personal as $item){
            $data['personalId']=$item->personalId;
            $data['nombre_persona']=$item->nombre;
        }
        $data['fecha']=$this->fechahoy;
        $this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('ventas_ope/listado',$data);
        $this->load->view('templates/footer');
        $this->load->view('ventas_ope/listadojs');
    }
    ////////////////////////////////////////////
    public function getlistado(){
        $params = $this->input->post();
        $getdata = $this->ModeloVentas->get_ventas($params);
        $totaldata= $this->ModeloVentas->total_ventas($params); 
        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($totaldata),  
            "recordsFiltered" => intval($totaldata),
            "data"            => $getdata->result(),
            "query"           =>$this->db->last_query()   
        );
        echo json_encode($json_data);
    }

    public function registrar_abono(){
        $data=$this->input->post();
        $arrayinfo = array('id_venta'=>$data['id_venta'],
                           'idpersona'=>$data['idpersona'],
                           'cantidad'=>$data['cantidad'],
                           'fecha'=>$data['fecha']);
        $this->General_model->add_record('venta_abono',$arrayinfo);
    }
    public function get_abono_venta(){
        $id_venta = $this->input->post('id_venta');
        $abono = $this->ModelCatalogos->get_total_abono($id_venta);
        $venta = $this->General_model->get_record('id_venta',$id_venta,'ventas');
        $monto_total=number_format($venta->monto_total, 2);
        $monto_total2=$venta->monto_total;
        $resta=$venta->monto_total-$abono->total;
        $resta2=number_format($resta, 2);
        $arrayabono = array('total' =>$monto_total,'resta'=>$resta2);
        echo json_encode($arrayabono);
    }

    public function get_tabla_abono(){
        $id_venta = $this->input->post('id_venta');
        $abono = $this->ModelCatalogos->getall_total_abono($id_venta);
        $get_personal=$this->General_model->get_records_condition('personalId='.$this->personalId,'personal');
        foreach ($get_personal as $item){
            $nombre_persona=$item->nombre;
        }

        $html1='<tr>
                    <th>Fecha<span style="color: #ff000000">_</span>y<span style="color: #ff000000">_</span>hora</th>
                    <th>Usuario</th>
                    <th>Cantidad a abonar</th>
                    <th></th>
                </tr>';
        
        $suma_cantidad=0;      
        $html2='';
        foreach ($abono as $x){
            $suma_cantidad+=$x->cantidad;
            $html2.='<tr>
                        <td>'.date('d/m/Y g:i:a',strtotime($x->fecha)).'</td>
                        <td>'.$x->nombre.' '.$x->apellidos.'</td>
                        <td>'.$x->cantidad.'</td>
                        <td><button type="button" class="btn btn-raised btn_orange round btn-min-width mr-1 mb-1"  onclick="cancelar_abono('.$x->id.')"><i class="ft-trash-2"></i></button>
                        </td>
                  </tr>';
        } 

        //////////////////////////////  Venta
        $venta = $this->General_model->get_record('id_venta',$id_venta,'ventas');
        $monto_total2=$venta->monto_total;

        if($monto_total2>$suma_cantidad){

        $html1.='<tr>
                    <td>'.date('d/m/Y g:i:a',strtotime($this->fechahoy)).'</td>
                    <td>'.$nombre_persona.'</td>
                    <td><input type="number" id="cantidad_abono" class="form-control"></td>
                    <td><button type="button" class="btn btn-raised btn_amarillo round btn-min-width mr-1 mb-1"  onclick="guardar_abono()"><i class="ft-plus"></i></button></td>
                </tr>';
            $this->General_model->edit_record('id_venta',$id_venta,array('pagado_credito'=>0),'ventas');
        }else{
            $this->General_model->edit_record('id_venta',$id_venta,array('pagado_credito'=>1),'ventas');
        }  

        //////////////////////////////////////

        $suma_cantidad_aux=number_format($suma_cantidad, 2); 
        $arrayabono = array('get_abono1'=>$html1,'get_abono2'=>$html2,'suma_cantidad'=>$suma_cantidad_aux,);
        echo json_encode($arrayabono);
    }

    public function deleteregistro_abono(){
        $id = $this->input->post('id');
        $data = array('activo'=>0);
        $this->General_model->edit_record('id',$id,$data,'venta_abono');
    }

    function cancalarventa(){
        $id = $this->input->post('id');
        $this->ModeloVentas->cancalarventa($id);
        $resultado=$this->ModeloVentas->ventadetalles($id);
        $venta = $this->ModeloCatalogos->get_record('id_venta',$id,'ventas');
        foreach ($resultado->result() as $item){
            $this->ModeloVentas->stock_aumentar_producto($item->cantidad,$item->id_producto);
        }
    }

    public function registrar_agenda(){
        $id = $this->input->post('idventa');        
        $fecha_servicio = $this->input->post('fecha_servicio');
        $hora_servicio = $this->input->post('hora_servicio');
        $unidad_servicio = $this->input->post('unidad_servicio');
        $data=array('fecha_servicio' => $fecha_servicio, 'hora_servicio' => $hora_servicio, 'unidad_servicio' => $unidad_servicio);
        $this->ModeloCatalogos->updateCatalogo('ventas', $data, array('id_venta' => $id));
    }
       
}
