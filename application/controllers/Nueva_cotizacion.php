<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Nueva_cotizacion extends CI_Controller
{
  function __construct()
  {
    parent::__construct();
    $this->load->helper('url');
    $this->load->model('ModeloCatalogos');
    $this->load->model('ModeloTransporte');
    $this->load->model('ModelGeneral');
    $this->load->model('ModeloCotizaciones');
    date_default_timezone_set('America/Mexico_City');
  }

  public function index()
  {
    $this->load->view('templates/header');
    $this->load->view('templates/navbar');
    $this->load->view('cotizaciones/nueva_cotizacion/lista_cotizacion');
    $this->load->view('templates/footer');
    $this->load->view('cotizaciones/nueva_cotizacion/lista_cotizacionjs');
  }

  public function getlist()
  {
    $params = $this->input->post();
    $getdata = $this->ModelGeneral->get_cotizaciones_sistema($params);
    $totaldata = $this->ModelGeneral->total_get_cotizaciones_sistema($params);
    $json_data = array(
      "recordsTotal"    => intval($totaldata),
      "recordsFiltered" => intval($totaldata),
      "data"            => $getdata->result(),
      "query"           => $this->db->last_query()
    );
    echo json_encode($json_data);
  }


  public function add($id = 0)
  {
    $idd = $this->ModelGeneral->get_id('cotizacion_sistema', 'id_ncotizacion');
    $length = 3;
    $format = substr(str_repeat(0, $length) . ($idd + 1), -$length);
    $folio = date('Y') . $format;

    $data["reg"] = date('Y/m/d - h:i a');
    $data["folio"] = $folio;
    $data["id"] = $id;
    $data["user"] = $_SESSION['usuario'];
    $data["userId"] = $_SESSION['usuarioid'];
    $data['title_heades'] = 'Nueva cotización';
    $data['title_save'] = 'Guardar';
    //$data["productosrow"] = $this->ModeloCotizaciones->get_all_equipment('datos_equipamiento');

    if ($id > 0) {
      $data = [];
      $data['title_heades'] = 'Editar cotización';
      $data['title_save'] = 'Actualizar';
      $current_data = $this->ModelGeneral->getselectid1($id);

      foreach ($current_data as $item) {
        $data["id_n"] = $item->id_ncotizacion;
        $data["reg"] = $item->registro;
        $data["user"] = $_SESSION['usuario'];
        $data["userId"] = $_SESSION['usuarioid'];
        $data["folio"] = $item->folio;
        $data["vehiculo"] = $item->vehiculo;
        $data["equipamiento"] = $item->equipamiento;
        $data["encabezado"] = $item->encabezado;
      }
    }

    $this->load->view('templates/header');
    $this->load->view('templates/navbar');
    $this->load->view('cotizaciones/nueva_cotizacion/nueva_cotizacion', $data);
    $this->load->view('templates/footer');
    $this->load->view('cotizaciones/nueva_cotizacion/nueva_cotizacionjs');
  }


  public function insert()
  {
    $data = $this->input->post('array');
    $DATA = json_decode($data);
    if ($DATA->id_n > 0) {
      $this->ModeloCatalogos->updateCatalogo('cotizacion_sistema', array('equipamiento' => $DATA->equipamiento, 'vehiculo' => $DATA->vehiculo, 'encabezado' => $DATA->encabezado), array('id_ncotizacion' => $DATA->id_n));
      $id = $DATA->id_n;
    } else {
      $id = $this->ModeloCotizaciones->insert_n_cotizacion('cotizacion_sistema', array('user' => $DATA->user, 'equipamiento' => $DATA->equipamiento, 'folio' => $DATA->folio, 'vehiculo' => $DATA->vehiculo, 'encabezado' => $DATA->encabezado, 'registro' => $DATA->reg));
    }
    echo $id;
  }

  public function insert2()
  {
    $data = $this->input->post('data');
    $TABLE = json_decode($data);

    $this->ModeloCotizaciones->check_n_cotizacion('equipamiento', $TABLE[0]->id);

    for ($i = 0; $i < count($TABLE); $i++) {
      $this->ModeloCotizaciones->insert_n_cotizacion('equipamiento', array('id_cotizacion' => $TABLE[$i]->id, 'id_equipamiento' => $TABLE[$i]->id_equipo));
    }
  }


  public function get_equip()
  {
    $type = $this->input->post('type');
    $vehicle = $this->input->post('vehicle');
    if ($vehicle == 1) {

      switch ($type) {
        case '1':
          $type = "sprinter_alto";
          break;
        case '2':
          $type = "sprinter_medio";
          break;
        case '3':
          $type = "sprinter_personal";
          break;
        case '4':
          $type = "sprinter_publico";
          break;
        default:
          break;
      }
    } elseif ($vehicle == 2) {
      switch ($type) {
        case '1':
          $type = "crafter_alto";
          break;
        case '2':
          $type = "crafter_medio";
          break;
        case '3':
          $type = "crafter_urbana";
          break;
        default:
          break;
      }
    }
    $where = array($type => 1);
    $getdata = $this->ModeloCotizaciones->get_equipment($where);
    $json_data = $getdata->result();
    echo json_encode($json_data);
  }

  public function get_equip_up()
  {
    $id_up = $this->input->post('id');
    $where = array('id_cotizacion' => $id_up);
    $get_id = $this->ModeloCotizaciones->get_idequipment_up($where);


    $getdata = $this->ModeloCotizaciones->get_equipment_up($get_id);
    $json_data = $getdata->result();
    echo json_encode($json_data);
  }

  public function delete()
  {
    $params = $this->input->post();
    $id = $params['id'];
    $this->ModeloCatalogos->updateCatalogo('cotizacion_sistema', array('status' => 0), array('id_ncotizacion' => $id));
  }

  public function formatocotizacion($id = 0)
  {
    $data['cotizacion'] = $this->ModelGeneral->getselectid1($id);


    $where = array('id_cotizacion' => $id);
    $get_id = $this->ModeloCotizaciones->get_idequipment_up($where);
    $getdata = $this->ModeloCotizaciones->get_equipment_up($get_id);
    $json_data = $getdata->result();
    $data['datos'] = $json_data;

    $this->load->view('Reportes/documento_nuevacotizacion', $data);
  }

  public function search_equip()
  {
    $input = $this->input->get('search');
    $results = $this->ModeloCotizaciones->search_equip($input);
    echo json_encode($results->result());
  }
}
