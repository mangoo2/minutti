<?php 

defined('BASEPATH') OR exit('No direct script access allowed');

class Nuevacompra extends CI_Controller {
    function __construct(){
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('ModeloCatalogos');
        $this->load->model('ModeloProductos');
        if($this->session->userdata('logeado')==true){
            $this->perfilid =$this->session->userdata('perfilid');
            $this->idpersonal = $this->session->userdata('idpersonal');
            $this->sucursal = $this->session->userdata('sucursal');
            
        }else{
            redirect('Sistema'); 
        }
    }

    public function index(){
	    $this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('compras/listado');
        $this->load->view('templates/footer');
        $this->load->view('compras/listadojs');
    }
}