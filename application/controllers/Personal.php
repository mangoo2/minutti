<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Personal extends CI_Controller {
    function __construct(){
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('Personal/ModeloPersonal');
        $this->load->model('ModeloCatalogos');
        $this->load->model('ModelGeneral');
        $this->load->model('Usuarios/ModeloUsuarios');
    }
	public function index(){
            //$data['personal']=$this->ModeloPersonal->getpersonal();
            //carga de vistas
            $this->load->view('templates/header');
            $this->load->view('templates/navbar');
            //$this->load->view('Personal/Personal',$data);
            //$data['personal']=$this->ModeloPersonal->getpersonal();
            $this->load->view('Personal/Personal');
            $this->load->view('templates/footer');
            $this->load->view('Personal/personallist');

	}
    /**
     * Retorna vista para agregar personal 
     */
    public function Personaladd(){
            //carga de vistas
            $data['sucursalrow']=$this->ModeloCatalogos->getselectwheren('sucursal',array('activo'=>1));
            $data['estadorow']=$this->ModeloCatalogos->getselectwheren('estado',array('activo'=>1));
            $data['perfilesrow']=$this->ModeloUsuarios->getperfiles();
            $this->load->view('templates/header');
            $this->load->view('templates/navbar');
            $this->load->view('Personal/Personaladd',$data);
            $this->load->view('templates/footer');
            $this->load->view('Personal/jspersonal');
    }    
    public function addpersonal(){
        $params = $this->input->post();

        $personalId = $params['personalId'];
        unset($params['personalId']);

        $ideu=$params['ideu'];
        $usuario=$params['usuario'];
        $contrasena=$params['contrasena'];
        $perfiles=$params['perfiles'];

        unset($params['ideu']);
        unset($params['usuario']);
        unset($params['contrasena']);
        unset($params['perfiles']);
        
        if ($personalId>0) {
            $this->ModeloCatalogos->updateCatalogo('personal',$params,array('personalId'=>$personalId));
        }else{
            $personalId=$this->ModeloCatalogos->Insert('personal',$params);
        }
        if ($ideu>0) {
            $this->ModeloUsuarios->usuariosupdate($ideu,$usuario,$contrasena,$perfiles,$personalId);
        }else{
            $this->ModeloUsuarios->usuariosinsert($ideu,$usuario,$contrasena,$perfiles,$personalId);
        }
    }
    
    public function deletepersonal(){
        $id = $this->input->post('id');
        $this->ModeloPersonal->personaldelete($id); 
    } 


    public function getlistado(){
        $params = $this->input->post();
        $getdata = $this->ModelGeneral->get_personal($params);
        $totaldata= $this->ModelGeneral->total_get_personal($params); 
        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($totaldata),  
            "recordsFiltered" => intval($totaldata),
            "data"            => $getdata->result(),
            "query"           =>$this->db->last_query()   
        );
        echo json_encode($json_data);
    }

    
}
