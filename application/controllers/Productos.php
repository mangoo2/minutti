<?php 

defined('BASEPATH') OR exit('No direct script access allowed');

class Productos extends CI_Controller {
    function __construct(){
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('ModeloCatalogos');
        $this->load->model('ModeloProductos');
        $this->load->model('ModelGeneral');
        if($this->session->userdata('logeado')==true){
            $this->perfilid =$this->session->userdata('perfilid');
            $this->idpersonal = $this->session->userdata('idpersonal');
            $this->sucursal = $this->session->userdata('sucursal');
            
        }else{
            redirect('Sistema'); 
        }
    }

    public function index(){
            $data['categoriarow']=$this->ModeloCatalogos->getselectwheren('categoria',array('activo'=>1));
            $data['unidadrow']=$this->ModeloCatalogos->getselectwheren('unidad',array('activo'=>1));
            $data['sucursalrow']=$this->ModeloCatalogos->getselectwheren('sucursal',array('activo'=>1));

            if($this->perfilid==1){
                $selectedsucursal=0;
                $selectedsucursal_view='block';
            }else{
                $selectedsucursal=$this->sucursal;
                $selectedsucursal_view='None';
            }
            $data['selectedsucursal']=$selectedsucursal;
            $data['selectedsucursal_view']=$selectedsucursal_view;

    	    $this->load->view('templates/header');
            $this->load->view('templates/navbar');
            $this->load->view('productos/productoslista',$data);
            $this->load->view('templates/footer');
            $this->load->view('productos/productoslistajs');
    }

    public function add($id=0){
            $data['title_heades']='Agregar Producto';
            $data['title_save']='Guardar';
            if($id>0){
                $data['title_heades']='Editar Producto';
                $data['title_save']='Actualizar';
            }
            $data['productoId']=$id;
            $data['unidadrow']=$this->ModeloCatalogos->getselectwheren('unidad',array('activo'=>1));
            $data['categoriarow']=$this->ModeloCatalogos->getselectwheren('categoria',array('activo'=>1));
            $data['marcarow']=$this->ModeloCatalogos->getselectwheren('marca',array('activo'=>1));
            $data['config_productorow']=$this->ModeloCatalogos->getselectwheren('config_producto',array('activo'=>1));
            $data['sucursalrow']=$this->ModeloCatalogos->getselectwheren('sucursal',array('activo'=>1));
    	    $this->load->view('templates/header');
            $this->load->view('templates/navbar');
            $this->load->view('productos/productosadd',$data);
            $this->load->view('templates/footer');
            $this->load->view('productos/productosaddjs');
    }
    function insertupdate(){
        $params=$this->input->post();
        $productoId=$params['productoId'];
        unset($params['productoId']);
        //$arraystock=$params['arraystock'];
        //unset($params['arraystock']);
        if($productoId>0){
            $this->ModeloCatalogos->updateCatalogo('producto',$params,array('productoId'=>$productoId));
        }else{
            $productoId=$this->ModeloCatalogos->Insert('producto',$params);
        }
        
        /*
        $DATAc = json_decode($arraystock);       
        for ($i=0;$i<count($DATAc);$i++) {
            // insertar 
            if ($DATAc[$i]->productosId>0) {
                $arraydatos=array(
                                    'stock'=>$DATAc[$i]->stock,
                                    'fechanotificacion'=>null,
                                    'pertenece'=>$DATAc[$i]->pertenece
                            );
                $this->ModeloCatalogos->updateCatalogo('producto_sucursal',$arraydatos,array('productosId'=>$DATAc[$i]->productosId));
                
            }else{
                $arraydatos=array(
                                    'productoId'=>$productoId,
                                    'sucursalId'=>$DATAc[$i]->sucursalId,
                                    'stock'=>$DATAc[$i]->stock,
                                    'pertenece'=>$DATAc[$i]->pertenece
                            );
                $this->ModeloCatalogos->Insert('producto_sucursal',$arraydatos);

            }
        }
        */
    }
    function obtenerdatosproducto(){
        $params = $this->input->post();
        $id=$params['id'];
        $productoresult=$this->ModeloCatalogos->getselectwheren('producto',array('productoId'=>$id));
            $productoId=0;
        foreach ($productoresult->result() as $item) {
            $productoId = $item->productoId;
            $codigo = $item->codigo;
            $nombre = $item->nombre;
            $descripcion = $item->descripcion;
            $unidadId = $item->unidadId;
            $categoriaId = $item->categoriaId;
            $marcaId = $item->marcaId;
            $configuracion = $item->configuracion;
            $preciocompra = $item->preciocompra;
            $cantidad_minima_existencia = $item->cantidad_minima_existencia;
            $num_serie = $item->num_serie;
            $cantidad_maxima_existencia = $item->cantidad_maxima_existencia;
            $stock_disponible = $item->stock_disponible; 
            $precio_venta = $item->precio_venta; 
            $costo_compra = $item->costo_compra; 
        }
        $productosucursal=$this->ModeloCatalogos->getselectwheren('producto_sucursal',array('productoId'=>$id));
        $infoarray = array(
                            'productoId' =>$productoId,
                            'codigo' =>$codigo,
                            'nombre' =>$nombre,
                            'descripcion' =>$descripcion,
                            'unidadId' =>$unidadId,
                            'categoriaId' =>$categoriaId,
                            'marcaId' =>$marcaId,
                            'configuracion' =>$configuracion,
                            'preciocompra' =>$preciocompra,
                            'cantidad_minima_existencia' =>$cantidad_minima_existencia,
                            'cantidad_maxima_existencia' =>$cantidad_maxima_existencia,
                            'num_serie' =>$num_serie,
                            'stock_disponible' =>$stock_disponible,
                            'precio_venta' => $precio_venta,
                            'costo_compra'=> $costo_compra, 
                            'productosucursal'=>$productosucursal->result()
                            );
        echo json_encode($infoarray);
    }
    public function getlistproductos() {
        $params = $this->input->post();
        //$params['sucursal']=$this->sucursal;
        $getdata = $this->ModeloProductos->getlistproductos($params);
        $totaldata= $this->ModeloProductos->getlistproductost($params); 
        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($totaldata),  
            "recordsFiltered" => intval($totaldata),
            "data"            => $getdata->result(),
            "query"           =>$this->db->last_query()   
        );
        echo json_encode($json_data);
    }
    function traspasos(){
        $params = $this->input->post();
        $idproducto=$params['id'];
        $suc_salida=$params['suc_salida'];
        $suc_entrada=$params['suc_entrada'];
        $cantidad=$params['cantidad'];

        $this->ModeloCatalogos->updatestock2('producto_sucursal','stock','-',$cantidad,'productoId',$idproducto,'sucursalId',$suc_salida);


        $stockresult_s=$this->ModeloCatalogos->getselectwheren('producto_sucursal',array('productoId'=>$idproducto,'sucursalId'=>$suc_salida));
        $stock_salida=0;
        foreach ($stockresult_s->result() as $item) {
            $stock_salida=$item->stock;
        }

        $this->ModeloCatalogos->Insert('producto_bitacora',array('productoId'=>$idproducto,'sucursalId'=>$suc_salida,'movimiento'=>0,'stock_mov'=>-$cantidad,'stock_res'=>$stock_salida));

        //=======================================================================================================================
        $this->ModeloCatalogos->updatestock2('producto_sucursal','stock','+',$cantidad,'productoId',$idproducto,'sucursalId',$suc_entrada);

        $stockresult_e=$this->ModeloCatalogos->getselectwheren('producto_sucursal',array('productoId'=>$idproducto,'sucursalId'=>$suc_entrada));
        $stock_entrada=0;
        foreach ($stockresult_e->result() as $item) {
            $stock_entrada=$item->stock;
        }

        $this->ModeloCatalogos->Insert('producto_bitacora',array('productoId'=>$idproducto,'sucursalId'=>$suc_salida,'movimiento'=>1,'stock_mov'=>$cantidad,'stock_res'=>$stock_entrada));



    }
    function obtenerdatosproductobitacora(){
        $params = $this->input->post();
        $idproducto=$params['id'];
        $sucursal=$params['sucursal'];

        $bitacoraresult = $this->ModeloCatalogos->getselectwheren('producto_bitacora',array('productoId'=>$idproducto,'sucursalId'=>$sucursal));
        $html='<table class="table thead-inverse" id="table_bitacora">
                    <thead>
                        <tr>
                            <th>FECHA</th>
                            <th>MOVIMIENTO</th>
                            <th>STOCK DESPLAZADO</th>
                            <th>STOCK</th>
                        </tr>
                    </thead>
                    <tbody>
                ';
        foreach ($bitacoraresult->result() as $item) {
            if($item->movimiento==0){
                $movimiento='SALIDA';
            }else{
                $movimiento='ENTRADA';
            }
            $html.='<tr>
                        <td>'.$item->reg.'</td>
                        <td>'.$movimiento.'</td>
                        <td>'.$item->stock_mov.'</td>
                        <td>'.$item->stock_res.'</td>
                    </tr>';
        }
        $html.='</tbody>
                </table>';
        echo $html;
    }
    function delete(){
        $params = $this->input->post();
        $idproducto=$params['producto'];

        $this->ModeloCatalogos->updateCatalogo('producto',array('activo'=>0),array('productoId'=>$idproducto));
    }

    public function addunidad(){
        $params = $this->input->post();
        $unidadId = $params['unidadId'];
        unset($params['unidadId']);
        if ($unidadId>0) {
            $this->ModeloCatalogos->updateCatalogo('unidad',$params,array('unidadId'=>$unidadId));
            $id=$unidadId;
        }else{
            $id=$this->ModeloCatalogos->Insert('unidad',$params);
        }
        echo $id;
    }


    public function getlistado_unidad(){
        $params = $this->input->post();
        $getdata = $this->ModeloProductos->get_unidads($params);
        $totaldata= $this->ModeloProductos->total_unidads($params); 
        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($totaldata),  
            "recordsFiltered" => intval($totaldata),
            "data"            => $getdata->result(),
            "query"           =>$this->db->last_query()   
        );
        echo json_encode($json_data);
    }

    function delete_unidad(){
        $params = $this->input->post();
        $unidadId=$params['unidadId'];
        $this->ModeloCatalogos->updateCatalogo('unidad',array('activo'=>0),array('unidadId'=>$unidadId));
    }   

    public function addcategoria(){
        $params = $this->input->post();
        $categoriaId = $params['categoriaId'];
        unset($params['categoriaId']);
        if ($categoriaId>0) {
            $this->ModeloCatalogos->updateCatalogo('categoria',$params,array('categoriaId'=>$categoriaId));
            $id=$categoriaId;
        }else{
            $id=$this->ModeloCatalogos->Insert('categoria',$params);
        }
        echo $id;
    }


    public function getlistado_categoria(){
        $params = $this->input->post();
        $getdata = $this->ModeloProductos->get_categoria($params);
        $totaldata= $this->ModeloProductos->total_categoria($params); 
        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($totaldata),  
            "recordsFiltered" => intval($totaldata),
            "data"            => $getdata->result(),
            "query"           =>$this->db->last_query()   
        );
        echo json_encode($json_data);
    }

    function delete_categoria(){
        $params = $this->input->post();
        $categoriaId=$params['categoriaId'];
        $this->ModeloCatalogos->updateCatalogo('categoria',array('activo'=>0),array('categoriaId'=>$categoriaId));
    }  
    /// Configuración
    public function addconfig(){
        $params = $this->input->post();
        $id = $params['id'];
        unset($params['id']);
        if ($id>0) {
            $this->ModeloCatalogos->updateCatalogo('config_producto',$params,array('id'=>$id));
            $idx=$id;
        }else{
            $idx=$this->ModeloCatalogos->Insert('config_producto',$params);
        }
        echo $idx;
    }


    public function getlistado_config(){
        $params = $this->input->post();
        $getdata = $this->ModeloProductos->get_config($params);
        $totaldata= $this->ModeloProductos->total_config($params); 
        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($totaldata),  
            "recordsFiltered" => intval($totaldata),
            "data"            => $getdata->result(),
            "query"           =>$this->db->last_query()   
        );
        echo json_encode($json_data);
    }

    function delete_config(){
        $params = $this->input->post();
        $id=$params['id'];
        $this->ModeloCatalogos->updateCatalogo('config_producto',array('activo'=>0),array('id'=>$id));
    }
    
    public function excel_reporte($categoria,$nave)
    {   
        $data['nave']=$nave;
        $data['productos_get']=$this->ModeloProductos->productos_all($categoria,$nave);
        $this->load->view('productos/excel',$data);
    }

    /*public function get_detalle_ultimo_costo()
    {   
        $id = $this->input->post('id');
        $html='<table class="table thead-inverse" style="width="100%>
                    <thead>
                        <tr>
                            <th>Proveedor</th>
                            <th>Último precio de compra</th>
                        </tr>
                    </thead>
                    <tbody>';
                    $result=$this->ModelGeneral->get_datos_ultimo_costo($id);
                    foreach ($result as $x){
                        $html.='<tr>
                            <td>'.$x->proveedor.'</td>
                            <td>'.$x->costo.'</td>
                        </tr>';
                    }
            $html.='</tbody>
            </table>';
        echo $html;    
    }*/

    function validar_codigo_numero_serie()
    {
        $codigo=$this->input->post('codigo');
        $num_serie=$this->input->post('num_serie');
        $id_producto=$this->input->post('id_producto');
        $validar=0;
        if($codigo!=''){
            $result_cod=$this->ModeloCatalogos->getselectwhere_n_consulta('producto',array('codigo'=>$codigo));
            foreach ($result_cod as $x){
                $validar=1;
            }
        }
        if($num_serie!=''){
            $result_num=$this->ModeloCatalogos->getselectwhere_n_consulta('producto',array('num_serie'=>$num_serie));
            foreach ($result_num as $c){
                $validar=1;
            }
        }

        if($id_producto!=0){
            $validar=0;
        }
        echo $validar;
    } 


    public function addmarca(){
        $params = $this->input->post();
        $marcaId = $params['marcaId'];
        unset($params['marcaId']);
        if ($marcaId>0) {
            $this->ModeloCatalogos->updateCatalogo('marca',$params,array('marcaId'=>$marcaId));
            $id=$marcaId;
        }else{
            $id=$this->ModeloCatalogos->Insert('marca',$params);
        }
        echo $id;
    }

    public function getlistado_marca(){
        $params = $this->input->post();
        $getdata = $this->ModeloProductos->get_marca($params);
        $totaldata= $this->ModeloProductos->total_marca($params); 
        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($totaldata),  
            "recordsFiltered" => intval($totaldata),
            "data"            => $getdata->result(),
            "query"           =>$this->db->last_query()   
        );
        echo json_encode($json_data);
    }

    function delete_marca(){
        $params = $this->input->post();
        $marcaId=$params['marcaId'];
        $this->ModeloCatalogos->updateCatalogo('marca',array('activo'=>0),array('marcaId'=>$marcaId));
    }  
}
?>