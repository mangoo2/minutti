<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Proveedores extends CI_Controller {
    function __construct(){
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('Personal/ModeloPersonal');
        $this->load->model('ModeloCatalogos');
        $this->load->model('ModelGeneral');
    }
    
	public function index()
    {
        $this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('proveedor/vista');
        $this->load->view('templates/footer');
        $this->load->view('proveedor/vistajs');
    }

    public function registrar($id=0)
    {   
        if($id==0){
            $data['idproveedor']=0;
            $data['nombre']='';
            $data['correo']='';
            $data['celular']='';
            $data['tel_oficina']='';
            $data['contacto']='';
            $data['direccion']='';
            $data['check_fiscales']='';
            $data['razon_social']='';
            $data['calle']='';
            $data['no_ext']='';
            $data['no_int']='';
            $data['colonia']='';
            $data['localidad']='';
            $data['municipio']='';
            $data['estado']='';
            $data['pais']='';
            $data['codigo_postal']='';
            $data['rfc']='';
            $data['check_bancarios']='';
        }else{
            $resul=$this->ModeloCatalogos->getselectwhere('proveedor','idproveedor',$id);
            foreach ($resul as $item){
                $data['idproveedor']=$item->idproveedor;
                $data['nombre']=$item->nombre;
                $data['correo']=$item->correo;
                $data['celular']=$item->celular;
                $data['tel_oficina']=$item->tel_oficina;
                $data['contacto']=$item->contacto;
                $data['direccion']=$item->direccion;
                $data['check_fiscales']=$item->check_fiscales;
                $data['razon_social']=$item->razon_social;
                $data['calle']=$item->calle;
                $data['no_ext']=$item->no_ext;
                $data['no_int']=$item->no_int;
                $data['colonia']=$item->colonia;
                $data['localidad']=$item->localidad;
                $data['municipio']=$item->municipio;
                $data['estado']=$item->estado;
                $data['pais']=$item->pais;
                $data['codigo_postal']=$item->codigo_postal;
                $data['rfc']=$item->rfc;
                $data['check_bancarios']=$item->check_bancarios; 
            }
        }    
        $this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('proveedor/vistaadd',$data);
        $this->load->view('templates/footer');
        $this->load->view('proveedor/vistaaddjs');
    }

    public function registro(){
        $data=$this->input->post();
        $id=$data['idproveedor'];
        unset($data['idproveedor']);
        if(isset($data['check_fiscales'])){
            $data['check_fiscales']='on';
        }else{
            $data['check_fiscales']='';
        }
        if(isset($data['check_bancarios'])){
            $data['check_bancarios']='on';
        }else{
            $data['check_bancarios']='';
        }
        if($id==0){
            $id=$this->ModeloCatalogos->Insert('proveedor',$data);
        }else{
            $this->ModeloCatalogos->updateCatalogo('proveedor',$data,array('idproveedor'=>$id));
        }
        echo $id;
    }
    public function getlistado(){
        $params = $this->input->post();
        $getdata = $this->ModelGeneral->get_proveedor($params);
        $totaldata= $this->ModelGeneral->total_proveedor($params); 
        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($totaldata),  
            "recordsFiltered" => intval($totaldata),
            "data"            => $getdata->result(),
            "query"           =>$this->db->last_query()   
        );
        echo json_encode($json_data);
    }
    public function deleteregistro(){
        $id=$this->input->post('id');
        $this->ModeloCatalogos->updateCatalogo('proveedor',array('activo'=>0),array('idproveedor'=>$id));
    }

    function registro_bancarios(){
        $datos = $this->input->post('data');
        $DATA = json_decode($datos);
        for ($i=0;$i<count($DATA);$i++) {   
            $data['idproveedor']=$DATA[$i]->idproveedor;
            $data['banco']=$DATA[$i]->banco;
            $data['titular']=$DATA[$i]->titular;
            $data['cuenta']=$DATA[$i]->cuenta;
            $data['clave']=$DATA[$i]->clave;
            $data['tarjeta']=$DATA[$i]->tarjeta;
            if($DATA[$i]->id==0){
                $this->ModeloCatalogos->Insert('proveedor_bancarios',$data);
            }else{
                $this->ModeloCatalogos->updateCatalogo('proveedor_bancarios',$data,array('id'=>$DATA[$i]->id));
            }
        }   
    }

    function get_tabla_bancarios_all()
    {
        $id = $this->input->post('id');
        $arrayban = array('idproveedor'=>$id,'activo'=>1);
        $results=$this->ModeloCatalogos->getselectwhere_n_consulta('proveedor_bancarios',$arrayban);
        echo json_encode($results);
    }

    public function deleteregistro_bancaria(){
        $id=$this->input->post('id');
        $data = array('activo'=>0);
        $this->ModeloCatalogos->updateCatalogo('proveedor_bancarios',$data,array('id'=>$id));
    }

}