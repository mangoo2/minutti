<?php 

defined('BASEPATH') OR exit('No direct script access allowed');

class Reportes extends CI_Controller {
    function __construct(){
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('ModeloCatalogos');
        $this->load->model('ModeloReportes');
        if($this->session->userdata('logeado')==true){
            $this->perfilid =$this->session->userdata('perfilid');
            $this->idpersonal = $this->session->userdata('idpersonal');
            $this->sucursal = $this->session->userdata('sucursal');
            
        }else{
            redirect('Sistema'); 
        }
    }
    public function productos(){
        $data["naves"]=$this->ModeloCatalogos->getselectwhere('sucursal','activo',1);
        $data["provee"]=$this->ModeloCatalogos->getselectwherNOrder('proveedor',array("activo"=>1),'nombre',"asc");
        $this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('Reportes/reporte_productos',$data);
        $this->load->view('templates/footer');
        $this->load->view('Reportes/reporte_productosjs');
    }

    public function PDFProds($id,$tipo){
        $data["rep"]=$this->ModeloReportes->getReportePros($id,$tipo);
        $this->load->view('Reportes/pdfProductos',$data);
    }

    public function ExcelProds($id,$tipo){
        $data["rep"]=$this->ModeloReportes->getReportePros($id,$tipo);
        $this->load->view('Reportes/excelProductos',$data);
    }
    ////////////////////////////////////////
    public function PDFProvees($id){
        $data["rep"]=$this->ModeloReportes->getReporteProvees($id);
        $this->load->view('Reportes/pdfProvees',$data);
    }

    public function ExcelProvees($id){
        $data["rep"]=$this->ModeloReportes->getReporteProvees($id);
        $this->load->view('Reportes/excelProvees',$data);
    }

    ////////////////////////////////////////
    public function PDFCompras($id,$fi,$ff,$metodo){
        $data["rep"]=$this->ModeloReportes->getReporteCompras($id,$fi,$ff,$metodo);
        $this->load->view('Reportes/pdfCompras',$data);
    }

    public function ExcelCompras($id,$fi,$ff,$metodo){
        $data["rep"]=$this->ModeloReportes->getReporteCompras($id,$fi,$ff,$metodo);
        $this->load->view('Reportes/excelCompras',$data);
    }
}