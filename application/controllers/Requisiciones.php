<?php 

defined('BASEPATH') OR exit('No direct script access allowed');

class Requisiciones extends CI_Controller {
    function __construct(){
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('ModeloCatalogos');
        $this->load->model('ModeloProductos');
        $this->load->model('ModeloRequisicion');
        if($this->session->userdata('logeado')==true){
            $this->perfilid =$this->session->userdata('perfilid');
            $this->idpersonal = $this->session->userdata('idpersonal');
            $this->sucursal = $this->session->userdata('sucursal');
            
        }else{
            redirect('Sistema'); 
        }
    }

    public function index(){
        $data['sucursalrow']=$this->ModeloCatalogos->getselectwheren('sucursal',array('activo'=>1));
        $this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('requisicion/requisicion_lis',$data);
        $this->load->view('templates/footer');
        $this->load->view('requisicion/requisicion_lisjs');
    }
    public function add($id=0){
        $data['sucursalrow']=$this->ModeloCatalogos->getselectwheren('sucursal',array('activo'=>1));
        if($id==0){
            $data['requsicionId']=0;
            $data['personalId']=0;
            $data['fecha_solicitud']='';
            $data['destino']='';
            $data['personal_txt']='';
        }else{
            $resul=$this->ModeloCatalogos->getselectwhere('requisicion_material','requsicionId',$id);
            foreach ($resul as $item){
                $data['requsicionId']=$item->requsicionId;
                $data['personalId']=$item->personalId;
                $data['fecha_solicitud']=$item->fecha_solicitud;
                $data['destino']=$item->destino;
                $data['personal_txt']=$item->personal;
                /*
                $resul_p=$this->ModeloCatalogos->getselectwhere('personal','personalId',$item->personalId);
                foreach ($resul_p as $item){
                    $data['personal_txt']=$item->nombre.' '.$item->apellido_paterno.' '.$item->apellido_materno;
                }
                */
            }
        }
            
        $this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('requisicion/requisicion',$data);
        $this->load->view('templates/footer');
        $this->load->view('requisicion/requisicionjs',$data);
    }
    function insertupdate(){
        $params = $this->input->post();
        $requsicionId = $params['requsicionId'];
        intval($params['requsicionId']);
        $arrayproductos = $params['arrayproductos'];
        unset($params['retornoId']);
        unset($params['arrayproductos']);
        if($requsicionId>0){
            $this->ModeloCatalogos->updateCatalogo('requisicion_material',$params,array('requsicionId'=>$requsicionId));
        }else{
            $params['personalId_realizo']=$this->idpersonal;
            $requsicionId = $this->ModeloCatalogos->Insert('requisicion_material',$params);
        }
        $DATAc = json_decode($arrayproductos); 
        for ($i=0;$i<count($DATAc);$i++) {
            $requsiciondId=$DATAc[$i]->requsiciondId;
            if($requsiciondId>0){
                //$this->ModeloCatalogos->updateCatalogo('configuracion_detalle',array('nombresub'=>$DATAc[$i]->nombresub),array('configdId'=>$configdId));
            }else{
                $ordenIdd= $this->ModeloCatalogos->Insert('requisicion_material_d',array('requsicionId'=>$requsicionId,'cantidad'=>$DATAc[$i]->cantidad,'productoId'=>$DATAc[$i]->productoId));
            }
            
        }
    }
    public function getlist_row() {
        $params = $this->input->post();
        $getdata = $this->ModeloRequisicion->getlist_row($params);
        $totaldata= $this->ModeloRequisicion->getlist_row_t($params); 
        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($totaldata),  
            "recordsFiltered" => intval($totaldata),
            "data"            => $getdata->result(),
            "query"           =>$this->db->last_query()   
        );
        echo json_encode($json_data);
    }

    public function cancelar_requisicion_material()
    {   
        $id = $this->input->post('id');
        $this->ModeloCatalogos->updateCatalogo('requisicion_material',array('activo'=>0),array('requsicionId'=>$id));
    }

    public function cancelar_requisicion_material_detalle()
    {   
        $id = $this->input->post('id');
        $this->ModeloCatalogos->updateCatalogo('requisicion_material_d',array('activo'=>0),array('requsiciondId'=>$id));
    }
    

    function get_tabla_requisicion_material_detalle()
    {
        $id = $this->input->post('id');
        $results=$this->ModeloRequisicion->get_requisicion_material_detalle($id);
        echo json_encode($results);
    }
}
?>