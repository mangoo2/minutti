<?php 

defined('BASEPATH') OR exit('No direct script access allowed');

class Salida extends CI_Controller {
    function __construct(){
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('ModeloCatalogos');
        $this->load->model('ModeloProductos');
        $this->load->model('ModeloOrdenm');
        $this->load->model('ModeloValeMaterial');
        $this->load->model('ModelGeneral');
        if($this->session->userdata('logeado')==true){
            $this->perfilid =$this->session->userdata('perfilid');
            $this->idpersonal = $this->session->userdata('idpersonal');
            $this->sucursal = $this->session->userdata('sucursal');
            
        }else{
            redirect('Sistema'); 
        }

        date_default_timezone_set('America/Mexico_City');
        $this->fechahoy = date('Y-m-d G:i:s');
        $this->fechaactual = date('Y-m-d');

    }

    public function index(){
            $this->load->view('templates/header');
            $this->load->view('templates/navbar');
            $this->load->view('salida/salida');
            $this->load->view('templates/footer');
    }
    function orden_m($id=0,$view=0){
        $data['title_heades']='Agregar Configuración';
        $data['title_save']='Guardar';
        if($id>0){
            $data['title_heades']='Editar Configuración';
            $data['title_save']='Actualizar';
        }
        $data['ordenId']=$id;
        $data['viewstatus']=$view;
        $data['configuracionrow']=$this->ModeloCatalogos->getselectwheren('configuracion',array('activo'=>1));
        $data['unidadrow']=$this->ModeloCatalogos->getselectwheren('unidad',array('activo'=>1));
        $this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('salida/orden_m_list',$data);
        $this->load->view('templates/footer');
        $this->load->view('salida/orden_m_listjs');
    }
    public function getlist_orden_m_row() {
        $params = $this->input->post();
        $getdata = $this->ModeloOrdenm->getlist_row($params);
        $totaldata= $this->ModeloOrdenm->getlist_row_t($params); 
        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($totaldata),  
            "recordsFiltered" => intval($totaldata),
            "data"            => $getdata->result(),
            "query"           =>$this->db->last_query()   
        );
        echo json_encode($json_data);
    }
    function orden_m_add($id=0,$view=0){
        $data['user_session'] = $_SESSION['usuarioname'];
        $data['title_heades']='Agregar Configuración';
        $data['title_save']='Guardar';
        if($id>0){
            $data['title_heades']='Editar Configuración';
            $data['title_save']='Actualizar';
            $resul=$this->ModeloCatalogos->getselectwhere('orden_de_material','ordenId',$id);
            foreach ($resul as $item){
                $data['ordenId']=$item->ordenId;
                $data['serie']=$item->serie;
                $data['fecha_solicitud']=$item->fecha_solicitud;
                $data['fecha_entrega']=$item->fecha_entrega;
                $data['unidad']=$item->unidad;
                $resulcl=$this->ModeloCatalogos->getselectwhere('clientes','clienteId',$item->clienteId);
                $data['clienteId']=0;
                $data['cliente']='';
                $data['modelo_asiento']=$item->modelo_asiento;
                $data['viewstatus']=$view;
                $data['lider']=$item->lider;
                foreach ($resulcl as $xc){
                    $data['clienteId']=$xc->clienteId;
                    $data['cliente']=$xc->nombre;
                }
                $resulli=$this->ModeloCatalogos->getselectwhere('personal','personalId',$item->lider);
                $data['personalId']=0;
                $data['persona']='';
                foreach ($resulli as $li){
                    $data['personalId']=$li->personalId;
                    $data['persona']=$li->nombre.' '.$li->apellido_paterno.' '.$li->apellido_materno;
                }
            }
        }else{
            $data['fecha_solicitud']=$this->fechaactual;
            $data['ordenId']=$id;
            $data['viewstatus']=$view;
            $data['fecha_entrega']='';
            $data['serie']='';
            $data['unidad']='';
            $data['clienteId']=0;
            $data['cliente']='';
            $data['personalId']=0;
            $data['persona']='';
            $data['modelo_asiento']='';
            $data['lider']='';
        }
        $data['configuracionrow']=$this->ModeloCatalogos->getselectwheren('configuracion',array('activo'=>1));
        $data['unidadrow']=$this->ModeloCatalogos->getselectwheren('tipo_camioneta',array('activo'=>1));
        $this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('salida/orden_m',$data);
        $this->load->view('templates/footer');
        $this->load->view('salida/orden_mjs');
    }
    function orden_m_insertupdate(){
        $params = $this->input->post();
        $ordenId = $params['ordenId'];
        $arrayconfiguracion = $params['arrayconfiguracion'];
        unset($params['ordenId']);
        unset($params['arrayconfiguracion']);
        if($ordenId>0){
            $this->ModeloCatalogos->updateCatalogo('orden_de_material',$params,array('ordenId'=>$ordenId));
        }else{
            $ordenId = $this->ModeloCatalogos->Insert('orden_de_material',$params);
        }
        $DATAc = json_decode($arrayconfiguracion); 
        for ($i=0;$i<count($DATAc);$i++) {
            $ordenIdd=$DATAc[$i]->ordenIdd;
            if($ordenIdd>0){
                //$this->ModeloCatalogos->updateCatalogo('configuracion_detalle',array('nombresub'=>$DATAc[$i]->nombresub),array('configdId'=>$configdId));
            }else{
                log_message('error', 'configId: '.$DATAc[$i]->configId);
                $ordenIdd= $this->ModeloCatalogos->Insert('orden_de_material_d',array('ordenId'=>$ordenId,'configId'=>$DATAc[$i]->configId));
            }
            $DATAcd=$DATAc[$i]->productos;
            for ($j=0;$j<count($DATAcd);$j++) {
                $ordenIddd=$DATAcd[$j]->ordenIddd;
                if($ordenIddd>0){
                    //$this->ModeloCatalogos->updateCatalogo('configuracion_detalle_d',array('piezas'=>$DATAcd[$j]->piezas),array('configddId'=>$configddId));
                }else{
                    $this->ModeloCatalogos->Insert('orden_de_material_d_d',array('ordenIdd'=>$ordenIdd,'pieza'=>$DATAcd[$j]->pieza,'configdId'=>$DATAcd[$j]->configdId,'sucursalId'=>$DATAcd[$j]->sucursalId));
                    $result_c=$this->ModeloCatalogos->getselectwheren('configuracion_detalle_d',array('configdId'=>$DATAcd[$j]->configdId));
                    foreach ($result_c->result() as $x){
                        $productoId=$x->productoId;
                        $piezas=$x->piezas*$DATAcd[$j]->pieza;
                        $this->ModelGeneral->descontar_stock_producto_sucursal($productoId,$DATAcd[$j]->sucursalId,$piezas);
                    }
                }
            }
        }
    }
    function surtir_vale_material(){
        $data['sucursalrow']=$this->ModeloCatalogos->getselectwheren('sucursal',array('activo'=>1));
        $this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('salida/surtir_vale_material_lis',$data);
        $this->load->view('templates/footer');
        $this->load->view('salida/surtir_vale_material_lisjs');
    }
    function surtir_vale_material_add($id=0){
        $data['user_session'] = $_SESSION['usuarioname'];
        $data['sucursalrow']=$this->ModeloCatalogos->getselectwheren('sucursal',array('activo'=>1));
        if($id>0){
            $resul=$this->ModeloCatalogos->getselectwhere('vale_material','valeId',$id);
            foreach ($resul as $item){
                $data['valeId']=$item->valeId;
                $data['fecha_solicitud']=$item->fecha_solicitud;
                $data['destino']=$item->destino;
                $data['origen']=$item->origen;
                $resulli=$this->ModeloCatalogos->getselectwhere('personal','personalId',$item->personal_solicitante);
                $data['personal_solicitante']=0;
                $data['persona']='';
                foreach ($resulli as $li){
                    $data['personal_solicitante']=$li->personalId;
                    $data['persona']=$li->nombre.' '.$li->apellido_paterno.' '.$li->apellido_materno;
                }
                $resulsu=$this->ModeloCatalogos->getselectwhere('sucursal','sucursalId',$item->origen);
                $data['sucursaltext']='';
                foreach ($resulsu as $su){
                    $data['sucursaltext']=$su->sucursal;
                }
                $data['solicita']=$item->solicita;
            }
        }else{
            $data['valeId']=0;
            $data['personal_solicitante']=0;
            $data['fecha_solicitud']=$this->fechaactual;
            $data['destino']=0;
            $data['personalId']=0;
            $data['persona']='';
            $data['origen']='';
            $data['sucursaltext']='';
            $data['solicita']='';
        }
        $this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('salida/surtir_vale_material',$data);
        $this->load->view('templates/footer');
        $this->load->view('salida/surtir_vale_materialjs');
    }

    public function getlist_vale_m_row() {
        $params = $this->input->post();
        $getdata = $this->ModeloValeMaterial->getlist_row($params);
        $totaldata= $this->ModeloValeMaterial->getlist_row_t($params); 
        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($totaldata),  
            "recordsFiltered" => intval($totaldata),
            "data"            => $getdata->result(),
            "query"           =>$this->db->last_query()   
        );
        echo json_encode($json_data);
    }

    function vale_m_insertupdate(){
        $params = $this->input->post();
        $valeId = $params['valeId'];
        $origen = $params['origen'];
        $arrayproductos = $params['arrayproductos'];
        unset($params['valeId']);
        unset($params['arrayproductos']);
        if($valeId>0){
            $this->ModeloCatalogos->updateCatalogo('vale_material',$params,array('valeId'=>$valeId));
        }else{
            $valeId = $this->ModeloCatalogos->Insert('vale_material',$params);
        }

        $DATAc = json_decode($arrayproductos); 
        for ($i=0;$i<count($DATAc);$i++) {
            $valedId=$DATAc[$i]->valedId;
            if($valedId>0){
                //$this->ModeloCatalogos->updateCatalogo('configuracion_detalle',array('nombresub'=>$DATAc[$i]->nombresub),array('configdId'=>$configdId));
            }else{
                $ordenIdd= $this->ModeloCatalogos->Insert('vale_material_d',array('valeId'=>$valeId,'cantidad'=>$DATAc[$i]->cantidad,'productoId'=>$DATAc[$i]->productoId));
                $this->ModelGeneral->descontar_stock_producto_sucursal($DATAc[$i]->productoId,$origen,$DATAc[$i]->cantidad);
            }
            
        }
    }

    function obtenersubconfig(){
        $params = $this->input->post();
        $configId= $params['config'];
        $subconfigrow=$this->ModeloCatalogos->getselectwheren('configuracion_detalle',array('activo'=>1,'configId'=>$configId));
        $html='<option value="0" disabled selected>Seleccione una opción</option>';
        foreach ($subconfigrow->result() as $item) {
            $html.='<option value="'.$item->configdId.'">'.$item->nombresub.'</option>';
        }
        echo $html;
    }

    function obtener_nave(){
        $naverow=$this->ModeloCatalogos->getselectwheren('sucursal',array('activo'=>1));
        $html='<option value="0" disabled selected>Seleccione una opción</option>';
        foreach ($naverow->result() as $item) {
            if($item->sucursalId==$this->sucursal){
                $html.='<option value="'.$item->sucursalId.'">'.$item->sucursal.'</option>';
            }else{
                if($this->perfilid==1){
                    $html.='<option value="'.$item->sucursalId.'">'.$item->sucursal.'</option>'; 
                }
            }
            

        }
        echo $html;
    }
    
    public function get_verificacion_existencia(){
        $configdId=$this->input->post('id');
        $piezas=$this->input->post('piezas');
        $sucursalId=$this->input->post('sucursalId');
        $piezas_aux=0; 
        $validar=0;
        $result_c=$this->ModeloCatalogos->getselectwheren('configuracion_detalle_d',array('configdId'=>$configdId));
        foreach ($result_c->result() as $x){
            $productoId=$x->productoId;
            $piezas_aux=$x->piezas*$piezas;
            $get_pro=$this->ModeloCatalogos->getselectwhere_row('producto_sucursal',array('productoId'=>$productoId,'sucursalId'=>$sucursalId));
            if($piezas_aux>$get_pro->stock){
                $validar=1; 
            }
        }
        echo $validar;
    }

    public function get_producto_subconfiguracion(){
        $configdId=$this->input->post('id');
        $id_nave=$this->input->post('id_nave');
        $result_c=$this->ModelGeneral->producto_config($configdId,$id_nave);
        $style="";
        $html='<table class="table thead-inverse" style="width: 100%">
                <thead>
                    <tr>
                        <th>PRODUCTO</th>
                        <th>CANTIDAD DE CONFIG</th>
                        <th>STOCK DISPONIBLE</th>
                        <th>CÓDIGO</th>
                    </tr>
                </thead>';
        foreach ($result_c->result() as $x){
            if($x->piezas>$x->stock){
                $style="background-color:red";
            }else{
                $style="";
            }
            $html.='<tbody>
                <tr style="'.$style.'">
                    <td>'.$x->producto.'</td>
                    <td>'.$x->piezas.'</td>
                    <td>'.$x->stock.'</td>
                    <td>'.$x->codigo.'</td>
                </tr>
            </tbody>';
        }
        $html.='</table>';
        echo $html;
    }

    public function cancelar_orden()
    {   
        $id = $this->input->post('id');
        $this->ModeloCatalogos->updateCatalogo('orden_de_material',array('activo'=>0,'personalId_cancela'=>$this->idpersonal),array('ordenId'=>$id));
        $result_om=$this->ModeloCatalogos->getselectwheren('orden_de_material_d',array('ordenId'=>$id));
        foreach ($result_om->result() as $om){
            $result_omdd=$this->ModeloCatalogos->getselectwheren('orden_de_material_d_d',array('ordenIdd'=>$om->ordenIdd));
            foreach ($result_omdd->result() as $om){
                $result_c=$this->ModeloCatalogos->getselectwheren('configuracion_detalle_d',array('configdId'=>$om->configdId));
                foreach ($result_c->result() as $x){
                    $productoId=$x->productoId;
                    $piezas=$x->piezas*$om->pieza;
                    $this->ModelGeneral->incrementar_stock_producto_sucursal($productoId,$om->sucursalId,$piezas);
                }
            }
        }


    }
    
    function obtenerdatosproducto(){
        $data = $this->input->post();
        $id = $data['id'];
        $config_d_result = $this->ModelGeneral->get_obtenerdatosproducto($id);
        $arrayconfiguracion = array('config_d'=>$config_d_result);
        echo json_encode($arrayconfiguracion);
    }

    function config_d_d(){
        $params = $this->input->post();
        $id = $params['id'];
        $producto_result = $this->ModelGeneral->salida_config($id);
        echo json_encode($producto_result->result());
    }

    public function cancelar_orden_detalle()
    {   
        $id = $this->input->post('id');
        $this->ModeloCatalogos->updateCatalogo('orden_de_material_d_d',array('activo'=>0),array('ordenIddd'=>$id));
        $result_omdd=$this->ModeloCatalogos->getselectwheren('orden_de_material_d_d',array('ordenIddd'=>$id));
        foreach ($result_omdd->result() as $om){
            $result_c=$this->ModeloCatalogos->getselectwheren('configuracion_detalle_d',array('configdId'=>$om->configdId));
            foreach ($result_c->result() as $x){
                $productoId=$x->productoId;
                $piezas=$x->piezas*$om->pieza;
                $this->ModelGeneral->incrementar_stock_producto_sucursal($productoId,$om->sucursalId,$piezas);
            }
        }
    }

    public function documento_pdf_basico($id,$view=0){
        $resul=$this->ModeloCatalogos->getselectwhere('orden_de_material','ordenId',$id);
        foreach ($resul as $item){
            $data['ordenId']=$item->ordenId;
            $data['serie']=$item->serie;
            $data['fecha_solicitud']=date('d/m/Y',strtotime($item->fecha_solicitud));
            $data['fecha_entrega']=date('d/m/Y',strtotime($item->fecha_entrega));
            $data['modelo_asiento']=$item->modelo_asiento;
            $resulcl=$this->ModeloCatalogos->getselectwhere('clientes','clienteId',$item->clienteId);
            $data['cliente']='';
            $data['viewstatus']=$view;
            foreach ($resulcl as $xc){
                $data['cliente']=$xc->nombre;
            }
            $data['unidad']='';
            $resuluni=$this->ModeloCatalogos->getselectwhere('tipo_camioneta','id',$item->unidad);
            foreach ($resuluni as $un){
                $data['unidad']=$un->nombre;
            }
            $data['persona']=$item->lider;
            /*
            $resulli=$this->ModeloCatalogos->getselectwhere('personal','personalId',$item->lider);
            $data['persona']='';
            foreach ($resulli as $li){
                $data['persona']=$li->nombre.' '.$li->apellido_paterno.' '.$li->apellido_materno;
            }
            */
            $data['config_d_result'] = $this->ModelGeneral->get_obtenerdatosproducto($id);
        }
        $this->load->view('Reportes/documento1',$data);
    }

    public function documento_pdf_completo($id,$view=0){
        $resul=$this->ModeloCatalogos->getselectwhere('orden_de_material','ordenId',$id);
        foreach ($resul as $item){
            $data['ordenId']=$item->ordenId;
            $data['serie']=$item->serie;
            $data['fecha_solicitud']=date('d/m/Y',strtotime($item->fecha_solicitud));
            $data['fecha_entrega']=date('d/m/Y',strtotime($item->fecha_entrega));
            $data['modelo_asiento']=$item->modelo_asiento;
            $resulcl=$this->ModeloCatalogos->getselectwhere('clientes','clienteId',$item->clienteId);
            $data['cliente']='';
            $data['viewstatus']=$view;
            //$data['viewstatus']="";
            foreach ($resulcl as $xc){
                $data['cliente']=$xc->nombre;
            }
            $data['unidad']='';
            $resuluni=$this->ModeloCatalogos->getselectwhere('tipo_camioneta','id',$item->unidad);
            foreach ($resuluni as $un){
                $data['unidad']=$un->nombre;
            }
            $resulli=$this->ModeloCatalogos->getselectwhere('personal','personalId',$item->lider);
            $data['persona']='';
            foreach ($resulli as $li){
                $data['persona']=$li->nombre.' '.$li->apellido_paterno.' '.$li->apellido_materno;
            }
            $data['config_d_result'] = $this->ModelGeneral->get_obtenerdatosproducto($id);
        }
        $this->load->view('Reportes/documento2',$data);
    }

    function obtenerdatosproducto_vale(){
        $data = $this->input->post();
        $id = $data['id'];
        $result = $this->ModelGeneral->get_obtenerdatosproducto_vale($id);
        $array_result= array('get_datos'=>$result);
        echo json_encode($array_result);
    }

    function search_personal_sucu(){
        $usu = $this->input->get('search');
        $results=$this->ModeloValeMaterial->personalall_sucursal_search($usu);
        echo json_encode($results->result());
    }

    public function get_verificacion_existencia_producto(){
        $idorigen=$this->input->post('idorigen');
        $productoId=$this->input->post('idproducto');
        $cantidad=$this->input->post('cantidad');
        $validar=0;
        $get_pro=$this->ModeloCatalogos->getselectwhere_row('producto_sucursal',array('productoId'=>$productoId,'sucursalId'=>$idorigen));
        if($cantidad>$get_pro->stock){
            $validar=1; 
        }
        echo $validar;
    }

    public function cancelar_orden_detalle_vale_material()
    {   
        $id = $this->input->post('valedId');
        $this->ModeloCatalogos->updateCatalogo('vale_material_d',array('activo'=>0),array('valedId'=>$id));
        $result=$this->ModeloCatalogos->getselectwheren('vale_material_d',array('valedId'=>$id));
        foreach ($result->result() as $om){
            $result_c=$this->ModeloCatalogos->getselectwheren('vale_material',array('valeId'=>$om->valeId));
            foreach ($result_c->result() as $x){
                $this->ModelGeneral->incrementar_stock_producto_sucursal($om->productoId,$x->origen,$om->cantidad);
            }
        }
    }

    public function cancelar_orden_vale_material()
    {   
        $id = $this->input->post('id');
        $this->ModeloCatalogos->updateCatalogo('vale_material',array('activo'=>0,'personalId_cancela'=>$this->idpersonal),array('valeId'=>$id));
        $result=$this->ModeloCatalogos->getselectwheren('vale_material',array('valeId'=>$id));
        foreach ($result->result() as $v){
            $result_d=$this->ModeloCatalogos->getselectwheren('vale_material_d',array('valeId'=>$v->valeId,'activo'=>1));
            foreach ($result_d->result() as $vd){
                $this->ModelGeneral->incrementar_stock_producto_sucursal($vd->productoId,$v->origen,$vd->cantidad);
                $this->ModeloCatalogos->updateCatalogo('vale_material_d',array('activo'=>0),array('valedId'=>$vd->valedId));
            }
        }
    }

    public function documento_pdf_surtir($id){
        $resul=$this->ModeloCatalogos->getselectwhere('vale_material','valeId',$id);
        foreach ($resul as $item){
            $data['valeId']=$item->valeId;
            $data['personal_solicitante']=$item->personal_solicitante;
            $data['fecha_solicitud']=date('d/m/Y',strtotime($item->fecha_solicitud));
            $data['persona']=$item->solicita;;
            /*
            $resulli=$this->ModeloCatalogos->getselectwhere('personal','personalId',$item->personal_solicitante);
            $data['persona']='';
            foreach ($resulli as $li){
                $data['persona']=$li->nombre.' '.$li->apellido_paterno.' '.$li->apellido_materno;
            }
            */
            $resulor=$this->ModeloCatalogos->getselectwhere('sucursal','sucursalId',$item->origen);
            $data['origen']='';
            foreach ($resulor as $li){
                $data['origen']=$li->sucursal;
            }
            $resulde=$this->ModeloCatalogos->getselectwhere('sucursal','sucursalId',$item->destino);
            $data['destino']='';
            foreach ($resulde as $li){
                $data['destino']=$li->sucursal;
            }
            $data['result_pro'] = $this->ModeloValeMaterial->salida_prodcuto_vale($id);
        }
        $this->load->view('Reportes/documento_surtir1',$data);
    }

    function surtir_consumible_listado(){
        $this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('salida/surtir_consumible_listado');
        $this->load->view('templates/footer');
        $this->load->view('salida/surtir_consumible_listadojs');
    }

    function surtir_consumible_add($id=0){
        if($id>0){
            $resul=$this->ModeloCatalogos->getselectwhere('surtir_consumibles','Id',$id);
            foreach ($resul as $item){
                $data['Id']=$item->Id;
                $data['fecha_solicitud']=$item->fecha_solicitud;
                $resulli=$this->ModeloCatalogos->getselectwhere('personal','personalId',$item->personalId);
                $data['personalId']=0;
                $data['personal']='';
                foreach ($resulli as $li){
                    $data['personalId']=$li->personalId;
                    $data['personal']=$li->nombre.' '.$li->apellido_paterno.' '.$li->apellido_materno;
                }
                //$resulsu=$this->ModeloCatalogos->getselectwhere('sucursal','sucursalId',$item->area);
                $data['area']=$item->area;
                /*
                $data['areatext']='';
                foreach ($resulsu as $su){
                    $data['area']=$su->sucursalId;
                    $data['areatext']=$su->sucursal;
                }
                */
                $data['solicita']=$item->solicita;
            }
        }else{
            $data['Id']=0;
            $data['fecha_solicitud']=$this->fechaactual;
            $data['personalId']='';
            $data['personal']='';
            $data['area']=0;
            //$data['areatext']='';
            $data['solicita']='';
        }
        $data['sucursalrow']=$this->ModeloCatalogos->getselectwheren('sucursal',array('activo'=>1));
        $data['user_session'] = $_SESSION['usuarioname'];
        $this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('salida/surtir_consumible_add',$data);
        $this->load->view('templates/footer');
        $this->load->view('salida/surtir_consumible_addjs');
    }

    function surtir_consumible_insertupdate(){
        $params = $this->input->post();
        $Id = $params['Id'];
        $area = $params['area'];
        $arrayproductos = $params['arrayproductos'];
        unset($params['Id']);
        unset($params['arrayproductos']);
        if($Id>0){
            $this->ModeloCatalogos->updateCatalogo('surtir_consumibles',$params,array('Id'=>$Id));
        }else{
            $Id = $this->ModeloCatalogos->Insert('surtir_consumibles',$params);
        }
        $DATAc = json_decode($arrayproductos); 
        for ($i=0;$i<count($DATAc);$i++) {
            $Iddconsumible=$DATAc[$i]->Iddconsumible;
            if($Iddconsumible>0){
            }else{
                $this->ModeloCatalogos->Insert('surtir_consumibles_detalle',array('Idconsumible'=>$Id,'cantidad'=>$DATAc[$i]->cantidad,'producto'=>$DATAc[$i]->producto,'motivo'=>$DATAc[$i]->motivo,'motivo_otro'=>$DATAc[$i]->motivo_otro));
                $this->ModelGeneral->descontar_stock_producto_sucursal($DATAc[$i]->producto,$area,$DATAc[$i]->cantidad);
            }
        }
    }
    
    public function getlist_surtir_consumible(){
        $params = $this->input->post();
        $getdata = $this->ModeloValeMaterial->getlist_surtir_consumible($params);
        $totaldata= $this->ModeloValeMaterial->getlist_surtir_consumible_t($params); 
        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($totaldata),  
            "recordsFiltered" => intval($totaldata),
            "data"            => $getdata->result(),
            "query"           =>$this->db->last_query()   
        );
        echo json_encode($json_data);
    }

    function obtenerdatosproducto_surtir_consumible(){
        $data = $this->input->post();
        $id = $data['id'];
        $result = $this->ModeloValeMaterial->get_producto_surtir_consumible($id);
        $array_result= array('get_datos'=>$result);
        echo json_encode($array_result);
    }

    public function cancelar_orden_detalle_surtir_consumibles()
    {   
        $id = $this->input->post('iddconsimuble');
        $this->ModeloCatalogos->updateCatalogo('surtir_consumibles_detalle',array('activo'=>0),array('Iddconsumible'=>$id));
        $result=$this->ModeloCatalogos->getselectwheren('surtir_consumibles_detalle',array('Iddconsumible'=>$id));
        foreach ($result->result() as $om){
            $result_c=$this->ModeloCatalogos->getselectwheren('surtir_consumibles',array('Id'=>$om->Idconsumible));
            foreach ($result_c->result() as $x){
                $this->ModelGeneral->incrementar_stock_producto_sucursal($om->producto,$x->area,$om->cantidad);
            }
        }
    }

    public function cancelar_surtir_consumibles()
    {   
        $id = $this->input->post('id');
        $this->ModeloCatalogos->updateCatalogo('surtir_consumibles',array('activo'=>0,'personalId_cancela'=>$this->idpersonal),array('Id'=>$id));
        $result=$this->ModeloCatalogos->getselectwheren('surtir_consumibles',array('Id'=>$id));
        foreach ($result->result() as $v){
            $result_d=$this->ModeloCatalogos->getselectwheren('surtir_consumibles_detalle',array('Idconsumible'=>$v->Id,'activo'=>1));
            foreach ($result_d->result() as $vd){
                $this->ModelGeneral->incrementar_stock_producto_sucursal($vd->producto,$v->area,$vd->cantidad);
                $this->ModeloCatalogos->updateCatalogo('surtir_consumibles_detalle',array('activo'=>0),array('Iddconsumible'=>$vd->Iddconsumible));
            }
        }
    }

    public function documento_pdf_surtir_consumibles($id){
        $resul=$this->ModeloCatalogos->getselectwhere('surtir_consumibles','Id',$id);
        foreach ($resul as $item){
            $data['Id']=$item->Id;
            $data['personalId']=$item->personalId;
            $data['fecha_solicitud']=date('d/m/Y',strtotime($item->fecha_solicitud));
            $data['persona']=$item->solicita;
            /*
            $resulli=$this->ModeloCatalogos->getselectwhere('personal','personalId',$item->personalId);
            $data['persona']='';
            foreach ($resulli as $li){
                $data['persona']=$li->nombre.' '.$li->apellido_paterno.' '.$li->apellido_materno;
            }
            */
            $resulor=$this->ModeloCatalogos->getselectwhere('sucursal','sucursalId',$item->area);
            $data['area']='';
            foreach ($resulor as $li){
                $data['area']=$li->sucursal;
            }
            $data['result_pro'] = $this->ModeloValeMaterial->get_surtir_consumible($id);
        }
        $this->load->view('Reportes/documento_vale_consumibles1',$data);
    }

    //// Papeleria
    function surtir_papeleria_listado(){
        $this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('salida/surtir_papeleria_list');
        $this->load->view('templates/footer');
        $this->load->view('salida/surtir_papeleria_listjs');
    }

    function surtir_papeleria_add($id=0){
        if($id>0){
            $resul=$this->ModeloCatalogos->getselectwhere('surtir_papeleria','Id',$id);
            foreach ($resul as $item){
                $data['Id']=$item->Id;
                $data['fecha_solicitud']=$item->fecha_solicitud;
                $resulli=$this->ModeloCatalogos->getselectwhere('personal','personalId',$item->personalId);
                $data['personalId']=0;
                $data['personal']='';
                foreach ($resulli as $li){
                    $data['personalId']=$li->personalId;
                    $data['personal']=$li->nombre.' '.$li->apellido_paterno.' '.$li->apellido_materno;
                }
                //$resulsu=$this->ModeloCatalogos->getselectwhere('sucursal','sucursalId',$item->area);
                $data['area']=$item->area;
                /*
                $data['areatext']='';
                foreach ($resulsu as $su){
                    $data['area']=$su->sucursalId;
                    $data['areatext']=$su->sucursal;
                }
                */
                $data['solicita']=$item->solicita;
            }
        }else{
            $data['Id']=0;
            $data['fecha_solicitud']=$this->fechaactual;
            $data['personalId']='';
            $data['personal']='';
            $data['area']='';
            //$data['areatext']='';
            $data['solicita']='';
        }
        $data['sucursalrow']=$this->ModeloCatalogos->getselectwheren('sucursal',array('activo'=>1));
        $data['user_session'] = $_SESSION['usuarioname'];
        $this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('salida/surtir_papeleria_add',$data);
        $this->load->view('templates/footer');
        $this->load->view('salida/surtir_papeleria_addjs');
    }

    function surtir_papeleria_insertupdate(){
        $params = $this->input->post();
        $Id = $params['Id'];
        $area = $params['area'];
        $arrayproductos = $params['arrayproductos'];
        unset($params['Id']);
        unset($params['arrayproductos']);
        if($Id>0){
            $this->ModeloCatalogos->updateCatalogo('surtir_papeleria',$params,array('Id'=>$Id));
        }else{
            $Id = $this->ModeloCatalogos->Insert('surtir_papeleria',$params);
        }
        $DATAc = json_decode($arrayproductos); 
        for ($i=0;$i<count($DATAc);$i++) {
            $Iddpapeleria=$DATAc[$i]->Iddpapeleria;
            if($Iddpapeleria>0){
            }else{
                $this->ModeloCatalogos->Insert('surtir_papeleria_detalle',array('Idpapeleria'=>$Id,'cantidad'=>$DATAc[$i]->cantidad,'producto'=>$DATAc[$i]->producto,'motivo'=>$DATAc[$i]->motivo,'motivo_otro'=>$DATAc[$i]->motivo_otro));
                $this->ModelGeneral->descontar_stock_producto_sucursal($DATAc[$i]->producto,$area,$DATAc[$i]->cantidad);
            }
        }
    }

    public function getlist_surtir_papeleria(){
        $params = $this->input->post();
        $getdata = $this->ModeloValeMaterial->getlist_surtir_papeleria($params);
        $totaldata= $this->ModeloValeMaterial->getlist_surtir_papeleria_t($params); 
        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($totaldata),  
            "recordsFiltered" => intval($totaldata),
            "data"            => $getdata->result(),
            "query"           =>$this->db->last_query()   
        );
        echo json_encode($json_data);
    }

    function obtenerdatosproducto_surtir_papeleria(){
        $data = $this->input->post();
        $id = $data['id'];
        $result = $this->ModeloValeMaterial->get_producto_surtir_papeleria($id);
        $array_result= array('get_datos'=>$result);
        echo json_encode($array_result);
    }

    public function cancelar_orden_detalle_surtir_papeleria()
    {   
        $id = $this->input->post('iddpapeleria');
        $this->ModeloCatalogos->updateCatalogo('surtir_papeleria_detalle',array('activo'=>0),array('Iddpapeleria'=>$id));
        $result=$this->ModeloCatalogos->getselectwheren('surtir_papeleria_detalle',array('Iddpapeleria'=>$id));
        foreach ($result->result() as $om){
            $result_c=$this->ModeloCatalogos->getselectwheren('surtir_papeleria',array('Id'=>$om->Idpapeleria));
            foreach ($result_c->result() as $x){
                $this->ModelGeneral->incrementar_stock_producto_sucursal($om->producto,$x->area,$om->cantidad);
            }
        }
    }

    public function cancelar_surtir_papeleria()
    {   
        $id = $this->input->post('id');
        $this->ModeloCatalogos->updateCatalogo('surtir_papeleria',array('activo'=>0,'personalId_cancela'=>$this->idpersonal),array('Id'=>$id));
        $result=$this->ModeloCatalogos->getselectwheren('surtir_papeleria',array('Id'=>$id));
        foreach ($result->result() as $v){
            $result_d=$this->ModeloCatalogos->getselectwheren('surtir_papeleria_detalle',array('Idpapeleria'=>$v->Id,'activo'=>1));
            foreach ($result_d->result() as $vd){
                $this->ModelGeneral->incrementar_stock_producto_sucursal($vd->producto,$v->area,$vd->cantidad);
                $this->ModeloCatalogos->updateCatalogo('surtir_papeleria_detalle',array('activo'=>0),array('Iddpapeleria'=>$vd->Iddpapeleria));
            }
        }
    }

    public function documento_pdf_surtir_papeleria($id){
        $resul=$this->ModeloCatalogos->getselectwhere('surtir_papeleria','Id',$id);
        foreach ($resul as $item){
            $data['Id']=$item->Id;
            $data['personalId']=$item->personalId;
            $data['fecha_solicitud']=date('d/m/Y',strtotime($item->fecha_solicitud));
            $data['persona']=$item->solicita;    
            /*
            $resulli=$this->ModeloCatalogos->getselectwhere('personal','personalId',$item->personalId);
            $data['persona']='';
            foreach ($resulli as $li){
                $data['persona']=$li->nombre.' '.$li->apellido_paterno.' '.$li->apellido_materno;
            }
            */
            $resulor=$this->ModeloCatalogos->getselectwhere('sucursal','sucursalId',$item->area);
            $data['area']='';
            foreach ($resulor as $li){
                $data['area']=$li->sucursal;
            }
            $data['result_pro'] = $this->ModeloValeMaterial->get_surtir_papeleria($id);
        }
        $this->load->view('Reportes/documento_vale_papeleria1',$data);
    }
    
    /// Surtir otro
    function surtir_otro_listado(){
        $this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('salida/surtir_otro_list');
        $this->load->view('templates/footer');
        $this->load->view('salida/surtir_otro_listjs');
    }

    function surtir_otro($id=0){
        if($id>0){
            $resul=$this->ModeloCatalogos->getselectwhere('surtir_otros','Id',$id);
            foreach ($resul as $item){
                $data['Id']=$item->Id;
                $data['fecha_solicitud']=$item->fecha_solicitud;
                $resulli=$this->ModeloCatalogos->getselectwhere('personal','personalId',$item->personalId);
                $data['personalId']=0;
                $data['personal']='';
                foreach ($resulli as $li){
                    $data['personalId']=$li->personalId;
                    $data['personal']=$li->nombre.' '.$li->apellido_paterno.' '.$li->apellido_materno;
                }
                //$resulsu=$this->ModeloCatalogos->getselectwhere('sucursal','sucursalId',$item->area);
                $data['area']=$item->area;
                /*
                $data['areatext']='';
                foreach ($resulsu as $su){
                    $data['area']=$su->sucursalId;
                    $data['areatext']=$su->sucursal;
                }
                */
                $data['solicita']=$item->solicita;
            }
        }else{
            $data['Id']=0;
            $data['fecha_solicitud']=$this->fechaactual;
            $data['personalId']='';
            $data['personal']='';
            $data['area']='';
            //$data['areatext']='';
            $data['solicita']='';
        }
        $data['sucursalrow']=$this->ModeloCatalogos->getselectwheren('sucursal',array('activo'=>1));
        $data['user_session'] = $_SESSION['usuarioname'];
        $this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('salida/surtir_otro',$data);
        $this->load->view('templates/footer');
        $this->load->view('salida/surtir_otrojs');
    }

    public function getlist_surtir_otro(){
        $params = $this->input->post();
        $getdata = $this->ModeloValeMaterial->getlist_surtir_otro($params);
        $totaldata= $this->ModeloValeMaterial->getlist_surtir_otro_t($params); 
        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($totaldata),  
            "recordsFiltered" => intval($totaldata),
            "data"            => $getdata->result(),
            "query"           =>$this->db->last_query()   
        );
        echo json_encode($json_data);
    }

    function surtir_otro_insertupdate(){
        $params = $this->input->post();
        $Id = $params['Id'];
        $area = $params['area'];
        $arrayproductos = $params['arrayproductos'];
        unset($params['Id']);
        unset($params['arrayproductos']);
        if($Id>0){
            $this->ModeloCatalogos->updateCatalogo('surtir_otros',$params,array('Id'=>$Id));
        }else{
            $Id = $this->ModeloCatalogos->Insert('surtir_otros',$params);
        }
        $DATAc = json_decode($arrayproductos); 
        for ($i=0;$i<count($DATAc);$i++) {
            $sotrosdId=$DATAc[$i]->sotrosdId;
            if($sotrosdId>0){
            }else{
                $this->ModeloCatalogos->Insert('surtir_otros_d',array('sotrosId'=>$Id,'cantidad'=>$DATAc[$i]->cantidad,'producto'=>$DATAc[$i]->producto,'motivo'=>$DATAc[$i]->motivo,'motivo_otro'=>$DATAc[$i]->motivo_otro));
                $this->ModelGeneral->descontar_stock_producto_sucursal($DATAc[$i]->producto,$area,$DATAc[$i]->cantidad);
            }
        }
    }

    function obtenerdatosproducto_surtir_otro(){
        $data = $this->input->post();
        $id = $data['id'];
        $result = $this->ModeloValeMaterial->get_producto_surtir_otro($id);
        $array_result= array('get_datos'=>$result);
        echo json_encode($array_result);
    }

    public function documento_pdf_surtir_otro($id){
        $resul=$this->ModeloCatalogos->getselectwhere('surtir_otros','Id',$id);
        foreach ($resul as $item){
            $data['Id']=$item->Id;
            $data['personalId']=$item->personalId;
            $data['fecha_solicitud']=date('d/m/Y',strtotime($item->fecha_solicitud));
            $data['persona']=$item->solicita;
            /*
            $resulli=$this->ModeloCatalogos->getselectwhere('personal','personalId',$item->personalId);
            $data['persona']='';
            foreach ($resulli as $li){
                $data['persona']=$li->nombre.' '.$li->apellido_paterno.' '.$li->apellido_materno;
            }
            */
            $resulor=$this->ModeloCatalogos->getselectwhere('sucursal','sucursalId',$item->area);
            $data['area']='';
            foreach ($resulor as $li){
                $data['area']=$li->sucursal;
            }
            $data['result_pro'] = $this->ModeloValeMaterial->get_surtir_otro($id);
        }
        $this->load->view('Reportes/documento_surtir_otro1',$data);
    }

    public function cancelar_orden_detalle_surtir_otro()
    {   
        $id = $this->input->post('sotrosdId');
        $this->ModeloCatalogos->updateCatalogo('surtir_otros_d',array('activo'=>0),array('sotrosdId'=>$id));
        $result=$this->ModeloCatalogos->getselectwheren('surtir_otros_d',array('sotrosdId'=>$id));
        foreach ($result->result() as $om){
            $result_c=$this->ModeloCatalogos->getselectwheren('surtir_otros',array('Id'=>$om->sotrosId));
            foreach ($result_c->result() as $x){
                $this->ModelGeneral->incrementar_stock_producto_sucursal($om->producto,$x->area,$om->cantidad);
            }
        }
    }

    public function cancelar_surtir_otro()
    {   
        $id = $this->input->post('id');
        $this->ModeloCatalogos->updateCatalogo('surtir_otros',array('activo'=>0,'personalId_cancela'=>$this->idpersonal),array('Id'=>$id));
        $result=$this->ModeloCatalogos->getselectwheren('surtir_otros',array('Id'=>$id));
        foreach ($result->result() as $v){
            $result_d=$this->ModeloCatalogos->getselectwheren('surtir_otros_d',array('sotrosId'=>$v->Id,'activo'=>1));
            foreach ($result_d->result() as $vd){
                $this->ModelGeneral->incrementar_stock_producto_sucursal($vd->producto,$v->area,$vd->cantidad);
                $this->ModeloCatalogos->updateCatalogo('surtir_otros_d',array('activo'=>0),array('sotrosdId'=>$vd->sotrosdId));
            }
        }
    }
    
    /// Surtir herramienta
    function surtir_herramienta_listado(){
        $this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('salida/surtir_herramienta_list');
        $this->load->view('templates/footer');
        $this->load->view('salida/surtir_herramienta_listjs');
    }

    function surtir_herramienta($id=0){
        if($id>0){
            $resul=$this->ModeloCatalogos->getselectwhere('surtir_herramienta','Id',$id);
            foreach ($resul as $item){
                $data['Id']=$item->Id;
                $data['fecha_solicitud']=$item->fecha_solicitud;
                $resulli=$this->ModeloCatalogos->getselectwhere('personal','personalId',$item->personalId);
                $data['personalId']=0;
                $data['personal']='';
                foreach ($resulli as $li){
                    $data['personalId']=$li->personalId;
                    $data['personal']=$li->nombre.' '.$li->apellido_paterno.' '.$li->apellido_materno;
                }
                //$resulsu=$this->ModeloCatalogos->getselectwhere('sucursal','sucursalId',$item->area);
                $data['area']=$item->area;
                /*
                $data['areatext']='';
                foreach ($resulsu as $su){
                    $data['area']=$su->sucursalId;
                    $data['areatext']=$su->sucursal;
                }
                */
                $data['solicita']=$item->solicita;
            }
        }else{
            $data['Id']=0;
            $data['fecha_solicitud']=$this->fechaactual;
            $data['personalId']='';
            $data['personal']='';
            $data['area']='';
            //$data['areatext']='';
            $data['solicita']='';
        }
        $data['sucursalrow']=$this->ModeloCatalogos->getselectwheren('sucursal',array('activo'=>1));
        $data['user_session'] = $_SESSION['usuarioname'];
        $this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('salida/surtir_herramienta_add',$data);
        $this->load->view('templates/footer');
        $this->load->view('salida/surtir_herramienta_addjs');
    }

    public function getlist_surtir_herramienta(){
        $params = $this->input->post();
        $getdata = $this->ModeloValeMaterial->getlist_surtir_herramienta($params);
        $totaldata= $this->ModeloValeMaterial->getlist_surtir_herramienta_t($params); 
        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($totaldata),  
            "recordsFiltered" => intval($totaldata),
            "data"            => $getdata->result(),
            "query"           =>$this->db->last_query()   
        );
        echo json_encode($json_data);
    }

    function surtir_herramienta_insertupdate(){
        $params = $this->input->post();
        $Id = $params['Id'];
        $area = $params['area'];
        $arrayproductos = $params['arrayproductos'];
        unset($params['Id']);
        unset($params['arrayproductos']);
        if($Id>0){
            $this->ModeloCatalogos->updateCatalogo('surtir_herramienta',$params,array('Id'=>$Id));
        }else{
            $Id = $this->ModeloCatalogos->Insert('surtir_herramienta',$params);
        }
        $DATAc = json_decode($arrayproductos); 
        for ($i=0;$i<count($DATAc);$i++) {
            $iddherramienta=$DATAc[$i]->iddherramienta;
            if($iddherramienta>0){
            }else{
                $this->ModeloCatalogos->Insert('surtir_herramienta_detalle',array('Idherramienta'=>$Id,'cantidad'=>$DATAc[$i]->cantidad,'producto'=>$DATAc[$i]->producto,'motivo'=>$DATAc[$i]->motivo,'motivo_otro'=>$DATAc[$i]->motivo_otro));
                $this->ModelGeneral->descontar_stock_producto_sucursal($DATAc[$i]->producto,$area,$DATAc[$i]->cantidad);
            }
        }
    }

    function obtenerdatosproducto_herramienta(){
        $data = $this->input->post();
        $id = $data['id'];
        $result = $this->ModeloValeMaterial->get_producto_surtir_herramienta($id);
        $array_result= array('get_datos'=>$result);
        echo json_encode($array_result);
    }
    
    public function cancelar_orden_detalle_surtir_herramienta()
    {   
        $id = $this->input->post('Iddherramienta');
        $this->ModeloCatalogos->updateCatalogo('surtir_herramienta_detalle',array('activo'=>0),array('Iddherramienta'=>$id));
        $result=$this->ModeloCatalogos->getselectwheren('surtir_herramienta_detalle',array('Iddherramienta'=>$id));
        foreach ($result->result() as $om){
            $result_c=$this->ModeloCatalogos->getselectwheren('surtir_herramienta',array('Id'=>$om->Idherramienta));
            foreach ($result_c->result() as $x){
                $this->ModelGeneral->incrementar_stock_producto_sucursal($om->producto,$x->area,$om->cantidad);
            }
        }
    }

    function obtenerdatosproducto_surtir_herramienta(){
        $data = $this->input->post();
        $id = $data['id'];
        $result = $this->ModeloValeMaterial->get_producto_surtir_herramienta($id);
        $array_result= array('get_datos'=>$result);
        echo json_encode($array_result);
    }

    public function documento_pdf_surtir_herramienta($id){
        $resul=$this->ModeloCatalogos->getselectwhere('surtir_herramienta','Id',$id);
        foreach ($resul as $item){
            $data['Id']=$item->Id;
            $data['personalId']=$item->personalId;
            $data['fecha_solicitud']=date('d/m/Y',strtotime($item->fecha_solicitud));
            $data['persona']=$item->solicita;
            /*
            $resulli=$this->ModeloCatalogos->getselectwhere('personal','personalId',$item->personalId);
            $data['persona']='';
            foreach ($resulli as $li){
                $data['persona']=$li->nombre.' '.$li->apellido_paterno.' '.$li->apellido_materno;
            }
            */
            $resulor=$this->ModeloCatalogos->getselectwhere('sucursal','sucursalId',$item->area);
            $data['area']='';
            foreach ($resulor as $li){
                $data['area']=$li->sucursal;
            }
            $data['result_pro'] = $this->ModeloValeMaterial->get_surtir_herramienta($id);
        }
        $this->load->view('Reportes/documento_vale_herramienta1',$data);
    }

    public function cancelar_surtir_herramienta()
    {   
        $id = $this->input->post('id');
        $this->ModeloCatalogos->updateCatalogo('surtir_herramienta',array('activo'=>0,'personalId_cancela'=>$this->idpersonal),array('Id'=>$id));
        $result=$this->ModeloCatalogos->getselectwheren('surtir_herramienta',array('Id'=>$id));
        foreach ($result->result() as $v){
            $result_d=$this->ModeloCatalogos->getselectwheren('surtir_herramienta_detalle',array('Idherramienta'=>$v->Id,'activo'=>1));
            foreach ($result_d->result() as $vd){
                $this->ModelGeneral->incrementar_stock_producto_sucursal($vd->producto,$v->area,$vd->cantidad);
                $this->ModeloCatalogos->updateCatalogo('surtir_herramienta_detalle',array('activo'=>0),array('Iddherramienta'=>$vd->Iddpapeleria));
            }
        }
    }

}
?>