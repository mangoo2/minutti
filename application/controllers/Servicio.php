<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Servicio extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('ModelGeneral');
        $this->load->model('ModeloCatalogos');
        $this->load->model('ModeloServicios');
        $this->load->model('ModeloVentas');
        if($this->session->userdata('logeado')==true){
            $this->perfilid =$this->session->userdata('perfilid');
            $this->idpersonal = $this->session->userdata('idpersonal');
            $this->sucursal = $this->session->userdata('sucursal');
            
        }else{
            redirect('Sistema'); 
        }
        date_default_timezone_set('America/Mexico_City');
        $this->fechahoy = date('Y-m-d G:i:s');
        $this->hora_actual = date('G:i');
        $this->fecha_actual = date('Y-m-d');
    }


    public function index()
    {
        //$data['fecha']=$this->currentFecha;
        $this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('servicio/index');
        $this->load->view('templates/footer');
        $this->load->view('servicio/index_js');
    }

    public function agenda_servicios()
    {
        $data = $this->input->post();
        $inicio = date('Y-m-d', $data['start']);
        $fin = date('Y-m-d', $data['end']);
        $result = $this->ModeloCatalogos->get_agenda_servicios($inicio,$fin);
        echo json_encode($result);
    }

    public function searchcli(){
        $usu = $this->input->get('search');
        $results=$this->ModeloCatalogos->clientesallsearch($usu);
        echo json_encode($results->result());
    }

    public function searchserv(){
        $usu = $this->input->get('search');
        $results=$this->ModeloServicios->serviciosallsearch($usu);
        echo json_encode($results->result());
    }

    function unidades_cliente()
    {    
        $idcliente=$this->input->post('idcliente');
        $where = array('activo' =>1,'clienteId'=>$idcliente);
        $result=$this->ModeloCatalogos->getselectwheren('unidades',$where);
        $html='<select class="form-control" name="unidad_servicio" id="placas_unidad_x" style="width: 100%" onchange="get_unidad()"><option value="0" disabled selected>Selecciona una opción</option>';
            foreach ($result->result() as $x) {
                $html.='<option value="'.$x->unidadId.'" data-modelo="'.$x->modelo.'" data-ano="'.$x->ano.'">'.$x->placas.'-'.$x->modelo.'</option>';
            } 
        $html.='</select>';
        echo $html;
    }

    public function registro_cita(){
        $data=$this->input->post();
        $id=$data['id'];
        $precio_venta=$data['precio'];
        $arrayproductos=$data['arrayproductos'];
        ///
        $data['subtotal']=$precio_venta;
        $data['monto_total']=$precio_venta;
        /// 
        unset($data['id']);
        unset($data['precio']);
        unset($data['arrayproductos']);
        if($id==0){
            $data['reg']=$this->fechahoy;
            $data['id_personal']=$this->idpersonal;
            $id=$this->ModeloCatalogos->Insert('ventas',$data);
        }else{
            $this->ModeloCatalogos->updateCatalogo('ventas',$data,array('id_venta'=>$id));
        }

        $DATAc = json_decode($arrayproductos); 
        for ($i=0;$i<count($DATAc);$i++) {
            if($DATAc[$i]->idreg==0){
                $this->ModeloVentas->ingresarventad($id,$DATAc[$i]->servicioS,$DATAc[$i]->cantidadS,$DATAc[$i]->pUnitarioS,2);
            }else{
                $arraydv=array('id_servicio'=>$DATAc[$i]->servicioS,'cantidad'=>$DATAc[$i]->cantidadS,'precio'=>$DATAc[$i]->pUnitarioS);
                $this->ModeloCatalogos->updateCatalogo('venta_detalle',$arraydv,array('id_detalle_venta'=>$DATAc[$i]->idreg));
            }
        }

        echo $id;
    }

    function get_cita()
    {
        $id=$this->input->post('id');
        $result = $this->ModeloServicios->get_agenda_servicio_cita($id);
        $fecha_servicio='';
        $hora_servicio='';
        $id_cliente=0;
        $celular='';
        $unidad_servicio='';
        $modelo='';
        $ano='';
        $metodo='';
        foreach ($result as $x){
            $fecha_servicio=$x->fecha_servicio;
            $hora_servicio=$x->hora_servicio;
            $id_cliente=$x->id_cliente;
            $nombre=$x->nombre;
            $celular=$x->celular;
            $unidad_servicio=$x->unidad_servicio;
            $modelo=$x->modelo;
            $ano=$x->ano;
            $metodo=$x->metodo;
        }
        $array = array('fecha_servicio'=>$fecha_servicio,'hora_servicio'=>$hora_servicio,'id_cliente'=>$id_cliente,'nombre'=>$nombre,'celular'=>$celular,'unidad_servicio'=>$unidad_servicio,'modelo'=>$modelo,'ano'=>$ano,'metodo'=>$metodo);
        echo json_encode($array);
    }


    function get_servicios()
    {
        $id=$this->input->post('id_venta');
        $html='<div class="row">
                <div class="col-md-12">
                    <table class="table thead-inverse">
                        <thead>
                            <tr>
                                <th>Productos / Servicio</th>
                                <th>Descripción</th>
                                <th>Duración servicio</th>
                                <th>Costo Servicio</th>
                                <th>Cantidad</th>
                                <th>Total</th>
                            </tr>
                        </thead>
                        <tbody>';
                            $sumat=0;
                            $result = $this->ModeloServicios->get_servicio_cita($id);
                            foreach ($result as $x){
                                $total=$x->cantidad*$x->precio;
                                $sumat+=$total;
                                $html.='<tr>
                                    <td>'.$x->servicio.'</td>
                                    <td>'.$x->descripcion.'</td>
                                    <td>'.$x->duracion.'</td>
                                    <td>$'.number_format($x->precio,2,'.',',').'</td>
                                    <td>'.$x->cantidad.'</td>
                                    <td>$'.number_format($total,2,'.',',').'</td>
                                </tr>';   
                            }
                        $html.='</tbody>
                    </table>
                    <h5>Monto total: $'.number_format($sumat,2,'.',',').'</h5>
                </div>
            </div>';
        echo $html;

    }

    function get_tabla_servicios_venta()
    {
        $id = $this->input->post('id');
        $result = $this->ModeloServicios->get_servicio_cita($id);
        echo json_encode($result);
    }

    public function deleteregistro_servicio(){
        $id=$this->input->post('id');
        $this->ModeloCatalogos->updateCatalogo('ventas',array('activo'=>0),array('id_venta'=>$id));
    }

    function checket_servicio(){
        $id=$this->input->post('id');
        $valor=$this->input->post('valor');
        $this->ModeloCatalogos->updateCatalogo('ventas',array('check_servicio_cita'=>$valor),array('id_venta'=>$id));
    }

    public function registro_hoja_servicio(){
        $id=$this->input->post('id');
        $where = array('id_venta'=>$id);
        $result=$this->ModeloCatalogos->getselectwheren('hoja_servicio',$where);
        $idreg=0;
        foreach ($result->result() as $x){
            $idreg=$x->id;
        }
        if($idreg==0){
            $data['id_venta']=$id;
            $idreg=$this->ModeloCatalogos->Insert('hoja_servicio',$data);
        }        
        echo $idreg;
    }

    function hoja_servicio($id)
    {   
        $data['id_venta']=$id;
        $result = $this->ModeloServicios->get_agenda_servicio_cita($id);
        foreach ($result as $x){
            $data['fecha_servicio']=$x->fecha_servicio;
            $data['hora_servicio']=$x->hora_servicio;
            $data['id_cliente']=$x->id_cliente;
            $data['nombre']=$x->nombre;
            $data['celular']=$x->celular;
            $data['unidad_servicio']=$x->unidad_servicio;
            $data['modelo']=$x->modelo;
            $data['ano']=$x->ano;
            $data['rfc']=$x->rfc;
            $data['direccion']=$x->direccion;
            $data['estado']=$x->estado;
            $data['codigo_postal']=$x->codigo_postal;
            $data['correo']=$x->correo;
            $data['placas']=$x->placas;
            $data['placas2']=$x->placas;
        }
        
        $where = array('id_venta'=>$id);
        $resulth=$this->ModeloCatalogos->getselectwheren('hoja_servicio',$where);
        foreach ($resulth->result() as $i) {
            if($i->estatus==0){
                $data['hora_entrada']=$this->hora_actual;
                $data['hora_salida']='';
                $data['fecha']=$this->fecha_actual;
            }else{
                $data['hora_entrada']=$i->hora_entrada;
                $data['hora_salida']=$i->hora_salida;
                $data['fecha']=$i->fecha;
            }    
            
            $data['recibe']=$i->recibe;
            
            $data['tipo']=$i->tipo;
            //$data['placas']=$i->placas;
            $data['iva_desglosado']=$i->iva_desglosado;
            $data['aliniacion']=$i->aliniacion;
            $data['valvulas']=$i->valvulas;
            $data['balance']=$i->balance;
            $data['montajes']=$i->montajes;
            $data['afinacion']=$i->afinacion;
            $data['rotacion']=$i->rotacion;
            $data['parches']=$i->parches;
            $data['l_cantida']=$i->l_cantida;
            $data['l_media']=$i->l_media;
            $data['a_cantidad']=$i->a_cantidad;
            $data['a_media']=$i->a_media;
            $data['c_cadidad']=$i->c_cadidad;
            $data['c_media']=$i->c_media;
            $data['c_amortiguador']=$i->c_amortiguador;
            $data['c_d']=$i->c_d;
            $data['c_t']=$i->c_t;
            $data['f_cantidad']=$i->f_cantidad;
            $data['f_tipo']=$i->f_tipo;
            $data['co_cantidad']=$i->co_cantidad;
            $data['co_tipo']=$i->co_tipo;
            $data['co_ins_amortiguador']=$i->co_ins_amortiguador;
            $data['co_n']=$i->co_n;
            $data['co_e']=$i->co_e;
            $data['ba_d']=$i->ba_d;
            $data['ba_t']=$i->ba_t;
            $data['mano_obra']=$i->mano_obra;
            $data['otros']=$i->otros;
            $data['km']=$i->km;

            $data['b1']=$i->b1;
            $data['b2']=$i->b2;
            $data['b3']=$i->b3;
            $data['b4']=$i->b4;
            $data['b5']=$i->b5;
            $data['b6']=$i->b6;
            $data['b7']=$i->b7;
            $data['b8']=$i->b8;
            $data['b9']=$i->b9;
            $data['b10']=$i->b10;
            $data['r1']=$i->r1;
            $data['r2']=$i->r2;
            $data['r3']=$i->r3;
            $data['r4']=$i->r4;
            $data['r5']=$i->r5;
            $data['r6']=$i->r6;
            $data['r7']=$i->r7;
            $data['r8']=$i->r8;
            $data['r9']=$i->r9;
            $data['r10']=$i->r10;
            $data['m1']=$i->m1;
            $data['m2']=$i->m2;
            $data['m3']=$i->m3;
            $data['m4']=$i->m4;
            $data['m5']=$i->m5;
            $data['m6']=$i->m6;
            $data['m7']=$i->m7;
            $data['m8']=$i->m8;
            $data['m9']=$i->m9;
            $data['m10']=$i->m10;
            //$data['auto']=$i->auto;
            //$data['placas2']=$i->placas2;
            $data['file_carro']=$i->file_carro;
            $data['file_firma1']=$i->file_firma1;
            $data['file_firma2']=$i->file_firma2;
        }

        $this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('servicio/hoja_servicio',$data);
        $this->load->view('templates/footer');
        $this->load->view('servicio/hoja_serviciojs');
    }

    public function update_registro_hojaservicio(){
        $data=$this->input->post();
        $id=$data['id_venta'];
        if(isset($data['iva_desglosado'])){
            $data['iva_desglosado']=1;
        }else{
            $data['iva_desglosado']=0;
        }
        if(isset($data['aliniacion'])){
            $data['aliniacion']=1;
        }else{
            $data['aliniacion']=0;
        }
        if(isset($data['valvulas'])){
            $data['valvulas']=1;
        }else{
            $data['valvulas']=0;
        }
        if(isset($data['balance'])){
            $data['balance']=1;
        }else{
            $data['balance']=0;
        }
        if(isset($data['montajes'])){
            $data['montajes']=1;
        }else{
            $data['montajes']=0;
        }
        if(isset($data['afinacion'])){
            $data['afinacion']=1;
        }else{
            $data['afinacion']=0;
        }
        if(isset($data['rotacion'])){
            $data['rotacion']=1;
        }else{
            $data['rotacion']=0;
        }
        if(isset($data['parches'])){
            $data['parches']=1;
        }else{
            $data['parches']=0;
        }
        //////b
        if(isset($data['b1'])){
            $data['b1']=1;
        }else{
            $data['b1']=0;
        }
        if(isset($data['b2'])){
            $data['b2']=1;
        }else{
            $data['b2']=0;
        }
        if(isset($data['b3'])){
            $data['b3']=1;
        }else{
            $data['b3']=0;
        }
        if(isset($data['b4'])){
            $data['b4']=1;
        }else{
            $data['b4']=0;
        }
        if(isset($data['b5'])){
            $data['b5']=1;
        }else{
            $data['b5']=0;
        }
        if(isset($data['b6'])){
            $data['b6']=1;
        }else{
            $data['b6']=0;
        }
        if(isset($data['b7'])){
            $data['b7']=1;
        }else{
            $data['b7']=0;
        }
        if(isset($data['b8'])){
            $data['b8']=1;
        }else{
            $data['b8']=0;
        }
        if(isset($data['b9'])){
            $data['b9']=1;
        }else{
            $data['b9']=0;
        }
        if(isset($data['b10'])){
            $data['b10']=1;
        }else{
            $data['b10']=0;
        }
        //////r
        if(isset($data['r1'])){
            $data['r1']=1;
        }else{
            $data['r1']=0;
        }
        if(isset($data['r2'])){
            $data['r2']=1;
        }else{
            $data['r2']=0;
        }
        if(isset($data['r3'])){
            $data['r3']=1;
        }else{
            $data['r3']=0;
        }
        if(isset($data['r4'])){
            $data['r4']=1;
        }else{
            $data['r4']=0;
        }
        if(isset($data['r5'])){
            $data['r5']=1;
        }else{
            $data['r5']=0;
        }
        if(isset($data['r6'])){
            $data['r6']=1;
        }else{
            $data['r6']=0;
        }
        if(isset($data['r7'])){
            $data['r7']=1;
        }else{
            $data['r7']=0;
        }
        if(isset($data['r8'])){
            $data['r8']=1;
        }else{
            $data['r8']=0;
        }
        if(isset($data['r9'])){
            $data['r9']=1;
        }else{
            $data['r9']=0;
        }
        if(isset($data['r10'])){
            $data['r10']=1;
        }else{
            $data['r10']=0;
        }
        //////m
        if(isset($data['m1'])){
            $data['m1']=1;
        }else{
            $data['m1']=0;
        }
        if(isset($data['m2'])){
            $data['m2']=1;
        }else{
            $data['m2']=0;
        }
        if(isset($data['m3'])){
            $data['m3']=1;
        }else{
            $data['m3']=0;
        }
        if(isset($data['m4'])){
            $data['m4']=1;
        }else{
            $data['m4']=0;
        }
        if(isset($data['m5'])){
            $data['m5']=1;
        }else{
            $data['m5']=0;
        }
        if(isset($data['m6'])){
            $data['m6']=1;
        }else{
            $data['m6']=0;
        }
        if(isset($data['m7'])){
            $data['m7']=1;
        }else{
            $data['m7']=0;
        }
        if(isset($data['m8'])){
            $data['m8']=1;
        }else{
            $data['m8']=0;
        }
        if(isset($data['m9'])){
            $data['m9']=1;
        }else{
            $data['m9']=0;
        }
        if(isset($data['m10'])){
            $data['m10']=1;
        }else{
            $data['m10']=0;
        }
        $this->ModeloCatalogos->updateCatalogo('hoja_servicio',$data,array('id_venta'=>$id));
        echo $id;
    }

    public function update_registro_cliente(){
        $id_cliente=$this->input->post('id_cliente');
        $nombre=$this->input->post('nombre');
        $rfc=$this->input->post('rfc');
        $direccion=$this->input->post('direccion');
        $estado=$this->input->post('estado');
        $codigo_postal=$this->input->post('codigo_postal');
        $celular=$this->input->post('celular');
        $correo=$this->input->post('correo');
        $data = array('nombre'=>$nombre,
                'rfc'=>$rfc,
                'direccion'=>$direccion,
                'estado'=>$estado,
                'codigo_postal'=>$codigo_postal,
                'celular'=>$celular,
                'correo'=>$correo,);
        $this->ModeloCatalogos->updateCatalogo('clientes',$data,array('clienteId'=>$id_cliente));
    }
    
    /*function add_carro(){
      $id = $this->input->post('id');
      $datax = $this->input->post();
      $base64Image=$datax['carro'];
      $namefile=date('Y_m_d-h_i_s');
      file_put_contents('uploads/servicio_carro/'.$namefile.'.png',$base64Image);
      $datos = array('file_carro'=>$namefile.'.png');
      $this->ModeloCatalogos->updateCatalogo('hoja_servicio',$datos,array('id_venta'=>$id));
      //echo $base64Image;
    }*/

    function add_carro(){
      $id = $this->input->post('id');
      $datax = $this->input->post();
      $base64Image=$datax['carro'];
      $namefile=date('Y_m_d-h_i_s').'.png';
      $imagenBinaria = base64_decode(str_replace('data:image/png;base64,', '', $base64Image));
      file_put_contents('uploads/servicio_carro/'.$namefile, $imagenBinaria);
      //file_put_contents('uploads/graficas_nutricion/'.$namefile.'.txt',$base64Image);
      $datos = array('file_carro'=>$namefile);
      $this->ModeloCatalogos->updateCatalogo('hoja_servicio',$datos,array('id_venta'=>$id));
      //echo $base64Image;
    }

    function add_firma1(){
      $id = $this->input->post('id');
      $datax = $this->input->post();
      $base64Image=$datax['firma'];
      $namefile=date('Y_m_d-h_i_s').'.png';
      $imagenBinaria = base64_decode(str_replace('data:image/png;base64,', '', $base64Image));
      file_put_contents('uploads/servicio_firma1/'.$namefile, $imagenBinaria);
      //file_put_contents('uploads/graficas_nutricion/'.$namefile.'.txt',$base64Image);
      $datos = array('file_firma1'=>$namefile);
      $this->ModeloCatalogos->updateCatalogo('hoja_servicio',$datos,array('id_venta'=>$id));
      //echo $base64Image;
    }

    function add_firma2(){
      $id = $this->input->post('id');
      $datax = $this->input->post();
      $base64Image=$datax['firma'];
      $namefile=date('Y_m_d-h_i_s').'.png';
      $imagenBinaria = base64_decode(str_replace('data:image/png;base64,', '', $base64Image));
      file_put_contents('uploads/servicio_firma2/'.$namefile, $imagenBinaria);
      //file_put_contents('uploads/graficas_nutricion/'.$namefile.'.txt',$base64Image);
      $datos = array('file_firma2'=>$namefile);
      $this->ModeloCatalogos->updateCatalogo('hoja_servicio',$datos,array('id_venta'=>$id));
      //echo $base64Image;
    }

    public function pdf_hoja_servicio($id)
    {   
        $data['id_venta'] = $id;
        $result = $this->ModeloServicios->get_agenda_servicio_cita($id);
        foreach ($result as $x){
            $data['fecha_servicio']=$x->fecha_servicio;
            $data['hora_servicio']=$x->hora_servicio;
            $data['id_cliente']=$x->id_cliente;
            $data['nombre']=$x->nombre;
            $data['celular']=$x->celular;
            $data['unidad_servicio']=$x->unidad_servicio;
            $data['modelo']=$x->modelo;
            $data['ano']=$x->ano;
            $data['rfc']=$x->rfc;
            $data['direccion']=$x->direccion;
            $data['estado']=$x->estado;
            $data['codigo_postal']=$x->codigo_postal;
            $data['correo']=$x->correo;
        }
        
        $where = array('id_venta'=>$id);
        $resulth=$this->ModeloCatalogos->getselectwheren('hoja_servicio',$where);
        foreach ($resulth->result() as $i) {
            $data['hora_entrada']=$i->hora_entrada;
            $data['hora_salida']=$i->hora_salida;
            $data['recibe']=$i->recibe;
            $data['fecha']=$i->fecha;
            $data['tipo']=$i->tipo;
            $data['placas']=$i->placas;
            $data['iva_desglosado']=$i->iva_desglosado;
            $data['aliniacion']=$i->aliniacion;
            $data['valvulas']=$i->valvulas;
            $data['balance']=$i->balance;
            $data['montajes']=$i->montajes;
            $data['afinacion']=$i->afinacion;
            $data['rotacion']=$i->rotacion;
            $data['parches']=$i->parches;
            $data['l_cantida']=$i->l_cantida;
            $data['l_media']=$i->l_media;
            $data['a_cantidad']=$i->a_cantidad;
            $data['a_media']=$i->a_media;
            $data['c_cadidad']=$i->c_cadidad;
            $data['c_media']=$i->c_media;
            $data['c_amortiguador']=$i->c_amortiguador;
            $data['c_d']=$i->c_d;
            $data['c_t']=$i->c_t;
            $data['f_cantidad']=$i->f_cantidad;
            $data['f_tipo']=$i->f_tipo;
            $data['co_cantidad']=$i->co_cantidad;
            $data['co_tipo']=$i->co_tipo;
            $data['co_ins_amortiguador']=$i->co_ins_amortiguador;
            $data['co_n']=$i->co_n;
            $data['co_e']=$i->co_e;
            $data['ba_d']=$i->ba_d;
            $data['ba_t']=$i->ba_t;
            $data['mano_obra']=$i->mano_obra;
            $data['otros']=$i->otros;
            $data['km']=$i->km;

            $data['b1']=$i->b1;
            $data['b2']=$i->b2;
            $data['b3']=$i->b3;
            $data['b4']=$i->b4;
            $data['b5']=$i->b5;
            $data['b6']=$i->b6;
            $data['b7']=$i->b7;
            $data['b8']=$i->b8;
            $data['b9']=$i->b9;
            $data['b10']=$i->b10;
            $data['r1']=$i->r1;
            $data['r2']=$i->r2;
            $data['r3']=$i->r3;
            $data['r4']=$i->r4;
            $data['r5']=$i->r5;
            $data['r6']=$i->r6;
            $data['r7']=$i->r7;
            $data['r8']=$i->r8;
            $data['r9']=$i->r9;
            $data['r10']=$i->r10;
            $data['m1']=$i->m1;
            $data['m2']=$i->m2;
            $data['m3']=$i->m3;
            $data['m4']=$i->m4;
            $data['m5']=$i->m5;
            $data['m6']=$i->m6;
            $data['m7']=$i->m7;
            $data['m8']=$i->m8;
            $data['m9']=$i->m9;
            $data['m10']=$i->m10;
            $data['auto']=$i->auto;
            $data['placas2']=$i->placas2;
            $data['file_carro']=$i->file_carro;
            $data['file_firma1']=$i->file_firma1;
            $data['file_firma2']=$i->file_firma2;
        }

        $this->load->view('Reportes/pdf_hoja_servicio', $data);
    }

    public function deleteregistro_servicio_venta(){
        $id=$this->input->post('id');
        $this->ModeloCatalogos->updateCatalogo('venta_detalle',array('activo'=>0),array('id_detalle_venta'=>$id));
    }


}