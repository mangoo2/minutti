<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Servicios extends CI_Controller {
    function __construct(){
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('Personal/ModeloPersonal');
        $this->load->model('ModeloCatalogos');
        $this->load->model('ModelGeneral');
        $this->load->model('ModeloServicios');
        date_default_timezone_set('America/Mexico_City');
        $this->fechahoy = date('Y-m-d G:i:s');
    }
    
	public function index()
    {
        $this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('servicios/index');
        $this->load->view('templates/footer');
        $this->load->view('servicios/indexjs');
    }

    public function registrar($id=0)
    {   
        if($id==0){
            $data['tittle']='Nuevo';
            $data['id']=0;
            $data['codigo']='';
            $data['servico']='';
            $data['descripcion']='';
            $data['costo_servicio']='';
            $data['costo_especial']='';
            $data['duracion']='';
            $data['servicio_cuesta']='';
        }else{
            $data['tittle']='Edición de';
            $resul=$this->ModeloCatalogos->getselectwhere('servicios','id',$id);
            foreach ($resul as $item){
                $data['id']=$item->id;
                $data['codigo']=$item->codigo;
                $data['servico']=$item->servico;
                $data['descripcion']=$item->descripcion;
                $data['costo_servicio']=$item->costo_servicio;
                $data['costo_especial']=$item->costo_especial;
                $data['duracion']=$item->duracion;
                $data['servicio_cuesta']=$item->servicio_cuesta;
            }
        }    
        $this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('servicios/form',$data);
        $this->load->view('templates/footer');
        $this->load->view('servicios/formjs');
    }

    public function registro(){
        $data=$this->input->post();
        $id=$data['id'];
        unset($data['id']);
        if($id==0){
            $data['reg']=$this->fechahoy;
            $id=$this->ModeloCatalogos->Insert('servicios',$data);
        }else{
            $this->ModeloCatalogos->updateCatalogo('servicios',$data,array('id'=>$id));
        }
        echo $id;
    }

    public function getlist_row() {
        $params = $this->input->post();
        $getdata = $this->ModeloServicios->getlist_row($params);
        $totaldata= $this->ModeloServicios->getlist_row_t($params); 
        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($totaldata),  
            "recordsFiltered" => intval($totaldata),
            "data"            => $getdata->result(),
            "query"           =>$this->db->last_query()   
        );
        echo json_encode($json_data);
    }
    
    public function deleteregistro(){
        $id=$this->input->post('id');
        $this->ModeloCatalogos->updateCatalogo('servicios',array('activo'=>0),array('id'=>$id));
    }

    public function getDescripcionServicio()
    {
        $html = "";
        $id = $this->input->post("id");
        $dataServicio = $this->ModeloCatalogos->getDescripcionServicio(array('id' => $id));
        
        foreach ($dataServicio as $data) {
            $html .= "<tr>
                <td>" . $data->descripcion . "</td>
            </tr>";
        }

        echo $html;
    }

}