<?php 

defined('BASEPATH') OR exit('No direct script access allowed');

class Ticket extends CI_Controller {
    function __construct(){
        parent::__construct();
        $this->load->library('Pdf');
        $this->load->helper('url');
        $this->load->model('ModeloCatalogos');
        $this->load->model('ModeloVentas');
        $this->load->model('ModelGeneral');
        if($this->session->userdata('logeado')==true){
            $this->perfilid =$this->session->userdata('perfilid');
            $this->idpersonal = $this->session->userdata('idpersonal');
            $this->sucursal = $this->session->userdata('sucursal');
            
        }else{
            redirect('Sistema'); 
        }
        date_default_timezone_set('America/Mexico_City');
        $this->fechahoy = date('Y-m-d G:i:s');
        $this->fechaactual = date('Y-m-d');
    }
    
    /// ticket venta
    function ticket_venta($id){
        
        $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, array(80, 250), true, 'UTF-8', false);
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('Mangoo');
        $pdf->SetTitle('Ticket');
        $pdf->SetSubject('Ticket por venta');
        $pdf->SetKeywords('Ticket, Pago, Comprobante');
        $pdf->setPrintHeader(false);
        $pdf->setPrintFooter(false);

        // set margins
        $pdf->SetMargins('8','9','8');
        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

        $pdf->SetFont('courier', '', '10');

        $pdf->AddPage();
        
        $imglogo=base_url().'public/img/logo4.png';
        $getventas=$this->ModeloVentas->getventas($id);
        foreach ($getventas->result() as $item){
          $idticket = $item->id_venta;
          $id_personal = $item->id_personal;
          $id_cliente = $item->id_cliente;
          //$idsucursal = $item->sucursal;
          $personalId = $item->id_personal;
          $cliente=$this->ModeloCatalogos->get_record('clienteId',$id_cliente,'clientes');
          //$sucursal=$this->ModeloCatalogos->get_record('id',$idsucursal,'sucursales');
          $personal=$this->ModeloCatalogos->get_record('personalId',$personalId,'personal');
          $metodo = $item->metodo;
          $subtotal = $item->subtotal;
          $descuento = $item->descuentocant;
          $monto_total = $item->monto_total;
          $cambio = $item->cambio;
          $fecha = date("d/m/Y",strtotime($item->reg));
          $hora = date("g:i:a",strtotime($item->reg));
        }
        $getventasd=$this->ModeloCatalogos->getventasd($id);
        $html = '';
        /*
                           <tr>
                        <td align="center">Rejuvenecimiento Integral</td>
                    </tr>
                    <tr>
                        <td align="center">27 A Norte 1019 Colonia San Alejandro CP.72090
                           <hr>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" style="font-style: italic;">"La pasión cuando llega, nos hace más humildes y nos da un sentimiento de éxito diario"<br></td>
                        <hr>
                    </tr>
        */
        $html .= '<table border="0">
                    <tr>
                        <td align="center">
                            <img src="'.$imglogo.'" width="100px">
                            <br><br>
                        </td>
                    </tr>
                    <tr>
                        <td align="center">Minutti</td>
                    </tr>
                    <tr>
                        <td align="center">Información de venta</td>
                    </tr>
                    <tr>
                        <td align="center">Fecha de venta: '.$fecha.'</td>
                    </tr>
                    <tr>
                        <td align="center">Hora de venta: '.$hora.'</td>
                    </tr>
                    <tr> 
                        <td align="center">Cliente: '.$cliente->nombre.'</td>
                    </tr>
                    <tr>
                        <td align="center">Cajero: '.$personal->nombre.' '.$personal->apellido_paterno.' '.$personal->apellido_materno.'</td>
                    </tr>
                    <tr>
                        <hr> 
                        <td align="center">Productos</td>
                        <hr>
                    </tr>
                    <tr width="100%">
                        <td width="30%">Cant</td>
                        <td width="40%">Articulo</td>
                        <td width="30%">Precio</td>
                    </tr>';
                    foreach ($getventasd->result() as $rowEmp){
          $html .= '<tr width="100%">
                        <td width="30%">'.$rowEmp->cantidad.'</td>
                        <td width="40%">'.$rowEmp->nombre.'</td>
                        <td width="30%">$'.number_format($rowEmp->precio, 2).'</td>
                    </tr>';
                    }
          $html .= '<tr width="100%">
                        <hr>
                        <td width="30%"></td>
                        <td width="40%">Subtotal</td>
                        <td width="30%">$'.number_format($subtotal, 2).'</td>
                    </tr>
                    <tr width="100%">
                        <td width="30%"></td>
                        <td width="40%">Descuento</td>
                        <td width="30%">$'.number_format($descuento, 2).'</td>
                    </tr>
                    <tr width="100%">
                        <td width="30%"></td>
                        <td width="40%">Total</td>
                        <td width="30%">$'.number_format($monto_total, 2).'</td>
                    </tr>
                    <tr width="100%">
                        <td width="30%"></td>
                        <td width="40%">Cambio</td>
                        <td width="30%">$'.number_format($cambio, 2).'</td>
                    </tr>
                    <tr width="100%">
                        <hr>
                        <td width="100%" align="center">
                            ¡GRACIAS POR TU PREFERENCIA!
                        </td>
                        <hr>
                    </tr>
                    <tr width="100%">
                        <td align="center">
                            Este ticket no tiene ninguna validéz o representación fiscal
                        </td>
                    </tr>

                    ';
  
        $pdf->writeHTML($html, true, false, true, false, '');
        $pdf->IncludeJS('print(true);');
        ob_end_clean();
        $pdf->Output('ticket.pdf', 'I');

    }

}