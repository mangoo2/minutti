<?php 

defined('BASEPATH') OR exit('No direct script access allowed');

class Traspasos extends CI_Controller {
    function __construct(){
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('ModeloCatalogos');
        $this->load->model('ModeloProductos');
        $this->load->model('ModeloTraspasos');
        $this->load->model('ModelGeneral');
        if($this->session->userdata('logeado')==true){
            $this->perfilid =$this->session->userdata('perfilid');
            $this->idpersonal = $this->session->userdata('idpersonal');
            $this->sucursal = $this->session->userdata('sucursal');
            
        }else{
            redirect('Sistema'); 
        }
        date_default_timezone_set('America/Mexico_City');
        $this->fechahoy = date('Y-m-d G:i:s');
        $this->fechaactual = date('Y-m-d');
    }

    public function index(){
        $data['sucursalrow']=$this->ModeloCatalogos->getselectwheren('sucursal',array('activo'=>1));
            $this->load->view('templates/header');
            $this->load->view('templates/navbar');
            $this->load->view('traspasos/traspasos_lis',$data);
            $this->load->view('templates/footer');
            $this->load->view('traspasos/traspasos_lisjs',$data);
    }
    function add(){
        $data['sucursalrow']=$this->ModeloCatalogos->getselectwheren('sucursal',array('activo'=>1));
        $data['fechaactual']=$this->fechaactual;
        $data['sucursal']=$this->sucursal;
        $this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('traspasos/traspasos',$data);
        $this->load->view('templates/footer');
        $this->load->view('traspasos/traspasosjs');
    }
    function insertupdate(){
        $params = $this->input->post();
        $traspasoId = $params['traspasoId'];
        $arrayproductos = $params['arrayproductos'];
        unset($params['traspasoId']);
        unset($params['arrayproductos']);
        if($traspasoId>0){
            //$this->ModeloCatalogos->updateCatalogo('orden_de_material',$params,array('ordenId'=>$ordenId));
        }else{
            $params['personalId_realizo']=$this->idpersonal;
            $traspasoId = $this->ModeloCatalogos->Insert('traspasos',$params);
        }
        $DATAc = json_decode($arrayproductos); 
        for ($i=0;$i<count($DATAc);$i++) {
            $traspasodId=$DATAc[$i]->traspasodId;
            $cantidad=$DATAc[$i]->cantidad;
            $productoId=$DATAc[$i]->productoId;
            $sucursal_origen=$DATAc[$i]->sucursal_origen;
            $sucursal_destino=$DATAc[$i]->sucursal_destino;

            $this->ModeloCatalogos->Insert('traspasos_d',array('traspasoId'=>$traspasoId,'cantidad'=>$DATAc[$i]->cantidad,'productoId'=>$DATAc[$i]->productoId,'sucursal_origen'=>$DATAc[$i]->sucursal_origen,'sucursal_destino'=>$DATAc[$i]->sucursal_destino));
            $this->ModelGeneral->incrementar_stock_producto_sucursal($productoId,$sucursal_destino,$cantidad);
            $this->ModelGeneral->descontar_stock_producto_sucursal($productoId,$sucursal_origen,$cantidad);
            
        }
    }
    public function getlist_row() {
        $params = $this->input->post();
        $getdata = $this->ModeloTraspasos->getlist_row($params);
        $totaldata= $this->ModeloTraspasos->getlist_row_t($params); 
        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($totaldata),  
            "recordsFiltered" => intval($totaldata),
            "data"            => $getdata->result(),
            "query"           =>$this->db->last_query()   
        );
        echo json_encode($json_data);
    }

    public function update_traspaso()
    {   
        $id = $this->input->post('id');
        $this->ModeloCatalogos->updateCatalogo('traspasos',array('activo'=>0,'personalId_cancela'=>$this->idpersonal),array('traspasoId'=>$id));
        $result=$this->ModeloCatalogos->getselectwhere_n_consulta('traspasos_d',array('traspasoId' => $id));
        foreach ($result as $x){
            $this->ModelGeneral->incrementar_stock_producto_sucursal($x->productoId,$x->sucursal_origen,$x->cantidad);
            $this->ModelGeneral->descontar_stock_producto_sucursal($x->productoId,$x->sucursal_destino,$x->cantidad);
        }

    }
    
    public function cantidad_nave_prodcuto()
    {
        $id_nave=$this->input->post('id_nave');
        $idproducto=$this->input->post('idproducto');
        $get_pro=$this->ModeloCatalogos->getselectwhere_row('producto_sucursal',array('productoId' => $idproducto,'sucursalId' => $id_nave));
        echo $get_pro->stock;
    }

    public function generarPDF($id){
        $data["det"]=$this->ModeloTraspasos->getTraspasoPdf($id);
        $data["result"]=$this->ModeloTraspasos->getTraspasoPdfProd2($id);
        $this->load->view('Reportes/documento_traspaso',$data);
    }
}
?>