<?php 

defined('BASEPATH') OR exit('No direct script access allowed');

class Ventas extends CI_Controller {
    function __construct(){
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('ModeloCatalogos');
        $this->load->model('ModeloProductos');
        $this->load->model('ModelGeneral');
        $this->load->model('ModeloVentas');
        if($this->session->userdata('logeado')==true){
            $this->perfilid =$this->session->userdata('perfilid');
            $this->idpersonal = $this->session->userdata('idpersonal');
            $this->sucursal = $this->session->userdata('sucursal');
            
        }else{
            redirect('Sistema'); 
        }
    }

    public function index(){
        $data['clientedefault']=$this->ModeloVentas->clientepordefecto();
        $this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('ventas_ope/form',$data);
        $this->load->view('templates/footer');
        $this->load->view('ventas_ope/formjs');
    }

    public function buscar_producto(){
        $nom=$this->input->post('producto');
        $html='';
        $result=$this->ModeloVentas->get_producto($nom);
        echo json_encode($result);
    }
    
    function productoclear(){
        unset($_SESSION['pro']);
        $_SESSION['pro']=array();
        unset($_SESSION['can']);
        $_SESSION['can']=array();
    }

    public function addproducto_tabla(){
        $cant = $this->input->post('cant');
        $prod = $this->input->post('prod');    
        $tipo = $this->input->post('tipo');   
        $personalv=$this->ModeloVentas->get_producto_x($prod,$tipo);
        //$oProducto = new Producto();
        if($prod!=0){
            foreach ($personalv->result() as $item){
                $id = $item->productoId;
                $codigo = $item->codigo;
                $nombre = $item->nombre;
                $precioventa = $item->precio_venta;
                $oProducto=array(
                    "id"=>$id,
                    "codigo"=>$codigo,
                    "nombre"=>$nombre,
                    'precioventa'=>$precioventa,
                    'tipo'=>$tipo,
                );

            }

            if(in_array($oProducto, $_SESSION['pro'])){  // si el producto ya se encuentra en la lista de compra suma las cantidades
                $idx = array_search($oProducto, $_SESSION['pro']);
                $_SESSION['can'][$idx]+=$cant;
            }
            else{ //sino lo agrega
                array_push($_SESSION['pro'],$oProducto);
                array_push($_SESSION['can'],$cant);
            }
        }

        //var_dump($_SESSION['can']);
        //======================================================================
        $count = 0;
        $n =array_sum($_SESSION['can']);
        foreach ($_SESSION['pro'] as $fila){
            $Cantidad=$_SESSION['can'][$count]; 
            $precio=$fila['precioventa'];
            $cantotal=$Cantidad*$precio;

            //var_dump($Cantidad);
        ?>
            <tr class="producto_<?php echo $count;?>">                                        
                <td>
                    <input type="hidden" name="vsproid" id="vsproid" value="<?php echo $fila['id'];?>">
                    <input type="hidden" name="vstipo" id="vstipo" value="<?php echo $fila['tipo'];?>">
                    <?php echo $fila['codigo'];?>
                </td>                                        
                <td>
                    <input type="number" name="vscanti" id="vscanti" class="vscanti_<?php echo $count; ?>" oninput="calcular_cantidad_total(<?php echo $count; ?>)" value="<?php echo $Cantidad;?>" style="background: transparent;border: 0px; width: 80px;">
                </td>                                        
                <td><?php echo $fila['nombre'];?></td>                                        
                <td>
                    <div class="btn-group">
                        $<input type="text" class="precio_<?php echo $count; ?>"
                        data-idproducto="<?php echo $fila['id']; ?>" 
                        data-cantidad="<?php echo $_SESSION['can'][$count]; ?>" 
                        data-precioventa="<?php echo $fila['precioventa']; ?>" 
                        name="vsprecio" id="vsprecio" value="<?php echo $precio ?>" readonly style="background: transparent;border: 0px;width: 100px;">
                    </div>
                </td>                                        
                <td><div class="btn-group">
                        $<input type="text" class="vstotal vstotal_<?php echo $count; ?>" name="vstotal" id="vstotal" value="<?php echo $cantotal ?>" readonly style="background: transparent;border: 0px;    width: 100px;">
                    </div>    
                </td>                                        
                <td>                                            
                    <a data-original-title="Eliminar" title="Eliminar" style="font-size: 20px; color: #f2b327;" onclick="deletepro(<?php echo $count;?>)">
                        <i class="fa fa-trash"></i>
                    </a>
                </td>
            </tr>
        <?php
        $count++;
        }
        $contadormenos=$count-1;
        echo '<input type="hidden" id="borrar_producto" value="'.$contadormenos.'">';
    }

    function ingresarventa(){
        //$uss = $this->input->post('uss');
        $uss = $this->idpersonal;
        $cli = $this->input->post('cli');
        $mpago = $this->input->post('mpago');
        $desc = $this->input->post('desc');
        $descu = $this->input->post('descu');
        $sbtotal = $this->input->post('sbtotal');
        $total = $this->input->post('total');
        $efectivo = $this->input->post('efectivo');
        $cambio = $this->input->post('cambio');
        $factura = $this->input->post('factura');
        //$tarjeta = $this->input->post('tarjeta');
        $id=$this->ModeloVentas->ingresarventa($uss,$cli,$mpago,$sbtotal,$desc,$descu,$total,$efectivo,$cambio,$factura);
        echo $id;
    }
    function ingresarventapro(){
        $datos = $this->input->post('data');
        $DATA = json_decode($datos);
        for ($i=0;$i<count($DATA);$i++) { 
            $idventa = $DATA[$i]->idventa;
            $producto = $DATA[$i]->producto;
            $cantidad = $DATA[$i]->cantidad;
            $precio = $DATA[$i]->precio;
            $tipo = $DATA[$i]->tipo;
            $this->ModeloVentas->ingresarventad($idventa,$producto,$cantidad,$precio,$tipo);
            $this->ModeloVentas->stock_descontar_producto(intval($cantidad),intval($producto));
        }
    }

    public function searchcli(){
        $usu = $this->input->get('search');
        $results=$this->ModeloVentas->clientesallsearch($usu);
        echo json_encode($results->result());
    }
}
