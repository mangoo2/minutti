
<?php
$a=session_id();
if(empty($a)) session_start();
defined('BASEPATH') OR exit ('No direct script access allowed');

class ModelGeneral extends CI_Model {
    public function __construct() {
        parent::__construct();
        if (isset($_SESSION['idpersonal'])) {
            $this->idpersonal=$_SESSION['idpersonal'];
        }else{
            $this->idpersonal=0;
        }
    }

    function get_personal($params){
        $columns = array( 
            0=>'p.personalId',
            1=>'p.nombre',
            2=>'p.apellido_paterno',
            3=>'p.apellido_materno',
            4=>'p.puesto',
            5=>'us.Usuario',
            6=>'pe.nombre AS perfil',
        );

        $columnsx = array( 
            0=>'p.personalId',
            1=>'p.nombre',
            2=>'p.apellido_paterno',
            3=>'p.apellido_materno',
            4=>'p.puesto',
            5=>'us.Usuario',
            6=>'pe.nombre',
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('personal p');
        $this->db->join('usuarios us', 'us.personalId = p.personalId','left');
        $this->db->join('perfiles pe', 'pe.perfilId = us.perfilId','left');
        $this->db->where(array('p.estatus'=>1,'p.tipo'=>1));
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columnsx as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        $this->db->order_by($columnsx[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        $query=$this->db->get();
        return $query;
    }
    
    public function total_get_personal($params){
        $columns = array( 
            0=>'p.personalId',
            1=>'p.nombre',
            2=>'p.apellido_paterno',
            3=>'p.apellido_materno',
            4=>'p.puesto',
            5=>'s.sucursal'
        );

        $this->db->select('COUNT(1) as total');
        $this->db->from('personal p');
        $this->db->join('usuarios us', 'us.UsuarioID = p.personalId','left');
        $this->db->join('perfiles pe', 'pe.perfilId = us.perfilId','left');
        $this->db->where(array('p.estatus'=>1,'p.tipo'=>1));
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        $query=$this->db->get();
        return $query->row()->total;
    }

    function get_proveedor($params){
        $columns = array( 
            0=>'*',
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $columns2 = array( 
            0=>'idproveedor',
            1=>'nombre',
            2=>'correo',
            3=>'celular',
        );
        $this->db->select($select);
        $this->db->from('proveedor');
        $where = array('activo'=>1);
        $this->db->where($where);
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns2 as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        $this->db->order_by($columns2[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        $query=$this->db->get();
        return $query;
    }
    public function total_proveedor($params){
        $columns = array( 
            0=>'*',
        );
        $this->db->select('COUNT(1) as total');
        $this->db->from('proveedor');
        $where = array('activo'=>1);
        $this->db->where($where);
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }  
        $query=$this->db->get();
        return $query->row()->total;
    }

    public function getselect_where_like_supplir($search) {

        $strq = "SELECT idproveedor,nombre
                    FROM proveedor
                    where activo = 1 AND nombre like '%$search%'";
        $query = $this->db->query($strq);
        return $query->result();
    }

    public function getselect_where_like_emplpyee($search) {

        $strq = "SELECT personalId,nombre,apellido_paterno,apellido_materno
                    FROM personal
                    where estatus = 1 AND tipo=1 AND nombre like '%$search%'";
        $query = $this->db->query($strq);
        return $query->result();
    }

    function get_nooc(){
        $strq = "SELECT * FROM orden_compra ORDER BY id DESC LIMIT 1";
        $query = $this->db->query($strq);
        $no_oc=0;
        foreach ($query->result() as $x) {
            $no_oc =$x->no_oc;
        } 
        return $no_oc;
    }

    public function getselect_where_like_product($search) {
        $strq = "SELECT p.productoId,p.nombre,p.codigo,u.unidad,p.unidadId,p.preciocompra
                    FROM producto AS p
                    LEFT JOIN unidad AS u ON u.unidadId = p.unidadId
                    where p.activo = 1 AND p.nombre like '%$search%'";
        $query = $this->db->query($strq);
        return $query->result();
    }
     
    function get_orden_compra($params){
        $columns = array( 
            0=>'o.id',
            1=>'o.no_oc',
            2=>'DATE_FORMAT(o.fecha_compra,  "%d / %m / %Y" ) AS fecha_compra',
            3=>'DATE_FORMAT(o.fecha_surtimiento,  "%d / %m / %Y" ) AS fecha_surtimiento',
            4=>'pr.nombre AS proveedor',
            5=>'CONCAT(pe.nombre," ",pe.apellido_paterno," ",pe.apellido_materno) AS recibe ',
            6=>'o.estatus',
            7=>'o.requsicionId',
            5=>'CONCAT(pez.nombre," ",pez.apellido_paterno," ",pez.apellido_materno) AS realizo ',

        );
        $columns2 = array( 
            0=>'o.id',
            1=>'o.no_oc',
            2=>'o.fecha_compra',
            3=>'o.fecha_surtimiento',
            4=>'pr.nombre',
            5=>'pe.nombre',
            6=>'pe.apellido_paterno',
            7=>'pe.apellido_materno',
            8=>'o.estatus',
            9=>'o.requsicionId',
            10=>'o.total',
            11=>'pez.nombre',
            12=>'pez.apellido_paterno',
            13=>'pez.apellido_materno',
            
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('orden_compra o');
        $this->db->join('proveedor pr', 'pr.idproveedor = o.idproveedor','left');
        $this->db->join('personal pe', 'pe.personalId = o.personalId','left');
        $this->db->join('personal pez', 'pez.personalId = o.personalId_realizo','left');
        $this->db->where(array('o.activo'=>1));
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns2 as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        $this->db->order_by($columns2[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        $query=$this->db->get();
        return $query;
    }
    
    public function total_get_orden_compra($params){
        $columns = array( 
            0=>'o.id',
            1=>'o.no_oc',
            2=>'o.fecha_compra',
            3=>'o.fecha_surtimiento',
            4=>'pr.nombre',
            5=>'pe.nombre',
            6=>'pe.apellido_paterno',
            7=>'pe.apellido_materno',
            8=>'o.estatus',
            9=>'o.requsicionId',
            11=>'pez.nombre',
            12=>'pez.apellido_paterno',
            13=>'pez.apellido_materno',
        );

        $this->db->select('COUNT(1) as total');
        $this->db->from('orden_compra o');
        $this->db->join('proveedor pr', 'pr.idproveedor = o.idproveedor','left');
        $this->db->join('personal pe', 'pe.personalId = o.personalId','left');
        $this->db->join('personal pez', 'pez.personalId = o.personalId_realizo','left');
        $this->db->where(array('o.activo'=>1));
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        $query=$this->db->get();
        return $query->row()->total;
    }

    function get_detalle_producto($id,$nave){
        if($nave!=0){
        $strq="SELECT oc.id,oc.cantidad,oc.cantidad_recibida,oc.diferencia,oc.idproducto,pro.codigo,pro.nombre,u.unidad,pro.unidadId,oc.costo,oc.total,ps.stock
                FROM orden_compra_detalle AS oc 
                INNER JOIN producto AS pro ON pro.productoId=oc.idproducto
                LEFT JOIN producto_sucursal AS ps ON ps.productoId=pro.productoId
                INNER JOIN unidad AS u ON u.unidadId=pro.unidadId 
                WHERE oc.activo=1 AND oc.idorden=$id AND ps.sucursalId=$nave";
        }else{
            /*$strq="SELECT oc.id,oc.cantidad,oc.cantidad_recibida,oc.diferencia,oc.idproducto,pro.codigo,pro.nombre,u.unidad,pro.unidadId,oc.costo,oc.total, '0' AS stock
                FROM orden_compra_detalle AS oc 
                INNER JOIN producto AS pro ON pro.productoId=oc.idproducto
                INNER JOIN unidad AS u ON u.unidadId=pro.unidadId 
                WHERE oc.activo=1 AND oc.idorden=$id";*/
            $strq="SELECT oc.id,oc.cantidad,oc.cantidad_recibida,oc.diferencia,oc.idproducto,pro.codigo,pro.nombre,u.unidad,pro.unidadId,oc.costo,oc.total, '0' AS stock,
                (select GROUP_CONCAT(CONCAT('N',ps.sucursalId,'-(',ps.stock,')<br>') SEPARATOR '') as stock_suc from producto_sucursal ps where ps.productoId=pro.productoId) as stock_suc
                FROM orden_compra_detalle AS oc 
                INNER JOIN producto AS pro ON pro.productoId=oc.idproducto
                INNER JOIN unidad AS u ON u.unidadId=pro.unidadId 
                WHERE oc.activo=1 AND oc.idorden=$id";
        }        
        $query = $this->db->query($strq);

        return $query->result();
    }

    function incrementar_stock_producto_sucursal($id,$idsucu,$can){
        $strq = "UPDATE producto_sucursal set stock=stock+$can, pertenece=1 where productoId=$id AND sucursalId=$idsucu";
        $query = $this->db->query($strq);
    }

    function descontar_stock_producto_sucursal($id,$idsucu,$can){
        $strq = "UPDATE producto_sucursal set stock=stock-$can where productoId=$id AND sucursalId=$idsucu";
        $query = $this->db->query($strq);
    }

    function get_clientes($params){
        $columns = array( 
            0=>'*',
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $columns2 = array( 
            0=>'clienteId',
            1=>'nombre',
            2=>'correo',
            3=>'celular',
        );
        $this->db->select($select);
        $this->db->from('clientes');
        $where = array('activo'=>1);
        $this->db->where($where);
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns2 as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        $this->db->order_by($columns2[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        $query=$this->db->get();
        return $query;
    }
    public function total_clientes($params){
        $columns = array( 
            0=>'*',
        );
        $this->db->select('COUNT(1) as total');
        $this->db->from('clientes');
        $where = array('activo'=>1);
        $this->db->where($where);
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }  
        $query=$this->db->get();
        return $query->row()->total;
    }

    function get_obtenerdatosproducto($id){
        $strq="SELECT om.ordenIdd,om.configId,om.ordenId,c.nombre AS nombresub
                FROM orden_de_material_d AS om
                INNER JOIN configuracion AS c ON c.configId=om.configId
                WHERE om.activo=1 AND om.ordenId=$id";
        $query = $this->db->query($strq);
        return $query->result();
    }

    function salida_config($id){
        $strq="SELECT ord.ordenIddd,ord.ordenIdd,ord.configdId,ord.pieza,ord.sucursalId,cd.nombresub,su.sucursal
                FROM orden_de_material_d_d as ord 
                INNER JOIN configuracion_detalle as cd on cd.configdId=ord.configdId 
                INNER JOIN sucursal as su on su.sucursalId=ord.sucursalId 
                WHERE ord.activo=1 AND ord.ordenIdd=$id";
        $query = $this->db->query($strq);
        return $query;
    }

    function producto_config($id,$idnav){
        $w2=""; $gb="";
        if($idnav>0){
            $w2=" and ps.sucursalId=".$idnav."";
        }else{
            $gb=" group by pro.productoId";
        }

        $strq="SELECT pro.nombre AS producto,cd.piezas,pro.codigo, ps.stock
                FROM configuracion_detalle_d as cd
                INNER JOIN producto as pro on pro.productoId=cd.productoId
                INNER JOIN producto_sucursal as ps on ps.productoId=cd.productoId
                WHERE cd.activo=1 AND cd.configdId=$id $w2 $gb";
        $query = $this->db->query($strq);
        return $query;
    }

    function get_obtenerdatosproducto_vale($id){
        $strq="SELECT vm.valedId,vm.cantidad,pro.codigo,pro.nombre AS producto
                FROM vale_material_d AS vm
                INNER JOIN producto AS pro ON pro.productoId=vm.productoId
                WHERE vm.activo=1 AND vm.valeId=$id";
        $query = $this->db->query($strq);
        return $query->result();
    }
    
    public function get_requisicion_material_detalle($id){
        $strq = "SELECT r.requsiciondId,r.requsicionId,r.cantidad,r.productoId,p.codigo,p.nombre,u.unidad,u.unidadId,p.preciocompra,r.requsiciondId
                FROM requisicion_material_d AS r
                INNER JOIN producto AS p ON p.productoId=r.productoId
                INNER JOIN unidad AS u ON u.unidadId=p.unidadId 
                where r.activo = 1 AND r.requsicionId=$id";
        $query = $this->db->query($strq);
        return $query->result();
    }
    
    function get_ultimo_costo_proveedor($idprod,$idprovee){
        $strq = "SELECT * FROM ultimo_costo_proveedor where productoId=$idprod AND idproveedor=$idprovee";
        $query = $this->db->query($strq);
        $no_oc=0;
        foreach ($query->result() as $x) {
            $no_oc=1;
        } 
        return $no_oc;
    }

    function update_ultimo_costo_proveedor($costo,$idprod,$idprov){
        $strq = "UPDATE ultimo_costo_proveedor set costo=$costo where productoId=$idprod AND idproveedor=$idprov";
        $query = $this->db->query($strq);
    }

    function get_datos_ultimo_costo($id){
        $strq="SELECT uc.costo,pro.nombre AS proveedor
                FROM ultimo_costo_proveedor AS uc
                LEFT JOIN proveedor AS pro ON pro.idproveedor=uc.idproveedor
                WHERE uc.productoId=$id";
        $query = $this->db->query($strq);
        return $query->result();
    }

    function get_cotizaciones($params){
        $columns = array( 
            0=>'id',
            1=>'nombre',
            2=>'direccion',
            3=>'telefono',
            4=>'email',
            5=>'DATE_FORMAT(fecha_reg,  "%d / %m / %Y" ) AS fecha_reg'
        );
        $columns2 = array( 
            0=>'id',
            1=>'nombre',
            2=>'direccion',
            3=>'telefono',
            4=>'email',
            5=>'fecha_reg'    
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('datos_contacto_config_web dc');
        //$this->db->where(array('activo'=>1));

        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns2 as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        $this->db->order_by($columns2[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        $query=$this->db->get();
        return $query;
    }
    
    public function total_get_cotizaciones($params){
        $columns = array( 
            0=>'id',
            1=>'nombre',
            2=>'direccion',
            3=>'telefono',
            4=>'email',
            5=>'fecha_reg'
        );

        $this->db->select('COUNT(1) as total');
        $this->db->from('datos_contacto_config_web dc');

        //$this->db->where(array('o.activo'=>1));
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        $query=$this->db->get();
        return $query->row()->total;
    }

    function get_cotizaciones_sistema($params){
        $columns = array( 
            0=>'id_ncotizacion',
            1=>'folio',
            2=>'vehiculo',
            3=>'equipamiento',
            4=>'registro',
            5=>'estatus',
            6=>'usuario',
        );
        $columns2 = array( 
            0=>'id_ncotizacion',
            1=>'folio',
            2=>'vehiculo',
            3=>'equipamiento',
            4=>'registro',
            5=>'estatus',
            6=>'usuario',
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('cotizacion_sistema cs');
        $this->db->join('usuarios u', 'u.UsuarioID = cs.user');
        $this->db->where(array('status'=>1));

        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns2 as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        $query=$this->db->get();
        return $query;
    }
    
    public function total_get_cotizaciones_sistema($params){
        $columns = array( 
            0=>'id_ncotizacion',
            1=>'folio',
            2=>'vehiculo',
            3=>'equipamiento',
            4=>'registro',
            5=>'estatus',
            6=>'usuario',
        );

        $this->db->select('COUNT(1) as total');
        $this->db->from('cotizacion_sistema cs');

        //$this->db->where(array('o.activo'=>1));
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        $query=$this->db->get();
        return $query->row()->total;
    }

    public function get_id($table,$id){
        $strq="SELECT MAX($id) AS id 
                FROM $table";
        $query = $this->db->query($strq);
        foreach ($query->result() as $item) {
            $query = $item->id;
        }
        return $query;
    }

    public function getselectid1($id)
	{
		$strq="SELECT * 
                FROM cotizacion_sistema
                WHERE id_ncotizacion = $id";
        $query = $this->db->query($strq);
        return $query->result();
	}

////-------------------

    function get_cotizacion($params){
        $columns = array( 
            0=>'c.idCotizaciones',
            1=>'c.fecha',
            2=>'c.usuarioId',
            3=>'p.nombre as usuario',
            4=>'c.estatus',
            5=>'c.subtotal',
            6=>'c.iva',
            7=>'c.total',
            8=>'c.check_servicio',
            9=>'c.clienteId',
            10=>'c.descuento_e',
            11=>'c.descuento_p'
        );

        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('cotizaciones c');
        $this->db->join('personal p', 'p.personalId = c.usuarioId');
        $this->db->where(array('c.activo'=>1));

        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }              
        $this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        $query=$this->db->get();
        return $query;
    }

    public function get_cotizacion_total($params){
        $columns = array( 
            0=>'c.idCotizaciones',
            1=>'c.fecha',
            2=>'c.usuarioId',
            3=>'p.nombre as usuario',
            4=>'c.estatus',
            5=>'c.subtotal',
            6=>'c.iva',
            7=>'c.total',
            8=>'c.check_servicio',
            9=>'c.clienteId'
        );

        $this->db->select('COUNT(1) as total');
        $this->db->from('cotizaciones c');
        $this->db->join('personal p', 'p.personalId = c.usuarioId');
        $this->db->where(array('c.activo'=>1));

        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        $query=$this->db->get();
        return $query->row()->total;
    }

}