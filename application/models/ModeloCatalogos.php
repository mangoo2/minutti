<?php
$a = session_id();
if (empty($a)) session_start();
defined('BASEPATH') or exit('No direct script access allowed');

class ModeloCatalogos extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }
    function getadminsu()
    {
        $strq = "CALL SP_GET_PERSONAL_ADMINSU";
        $query = $this->db->query($strq);
        $this->db->close();
        return $query;
    }
    function Insert($Tabla, $data)
    {
        $this->db->insert($Tabla, $data);
        $id = $this->db->insert_id();
        return $id;
    }
    function updateCatalogo($Tabla, $data, $where)
    {
        $this->db->set($data);
        $this->db->where($where);
        $this->db->update($Tabla);
        //return $id;
    }
    function getselectwheren($table, $where)
    {
        $this->db->select('*');
        $this->db->from($table);
        $this->db->where($where);
        $query = $this->db->get();
        return $query;
    }

    public function get_record($col, $id, $table)
    {
        $sql = "SELECT * FROM $table WHERE $col=$id";
        $query = $this->db->query($sql);
        return $query->row();
    }


    function getProductoDataModal($id)
    {
        $this->db->select('cot.*, prod.codigo as cod, prod.nombre, prod.productoId, prod.stock_disponible as stock');
        $this->db->from('cotizaciones_productos cot');
        $this->db->join('producto prod', 'cot.codigo = prod.productoId', 'left');
        $this->db->where('cot.cotizacionesId', $id);
        $this->db->where('cot.activo', 1);

        $query = $this->db->get();
        return $query;
    }

    function getServicioDataModal($id)
    {
        $this->db->select('cot.*, serv.codigo as cod, serv.servico as nombre, serv.id');
        $this->db->from('cotizaciones_servicios cot');
        $this->db->join('servicios serv', 'cot.codigo = serv.id', 'left');
        $this->db->where('cot.cotizacionesId', $id);
        $this->db->where('cot.activo', 1);

        $query = $this->db->get();
        return $query;
    }

    function getDescripcionServicio($where)
    {
        $this->db->select('*');
        $this->db->from('servicios');
        $this->db->where($where);
        $query = $this->db->get();
        return $query->result();
    }

    function updatestock3($Tabla, $value, $masmeno, $value2, $idname1, $id1, $idname2, $id2, $idname3, $id3)
    {
        $strq = "UPDATE $Tabla SET $value = $value $masmeno $value2 where $idname1=$id1 and $idname2=$id2 and $idname3=$id3 ";
        $query = $this->db->query($strq);
        //return $id;
    }
    function updatestock2($Tabla, $value, $masmeno, $value2, $idname1, $id1, $idname2, $id2)
    {
        $strq = "UPDATE $Tabla SET $value = $value $masmeno $value2 where $idname1=$id1 and $idname2=$id2";
        $query = $this->db->query($strq);
        //return $id;
    }

    function configproducto($config)
    {
        $strq = "SELECT conf_d.configddId,conf_d.configdId,conf_d.piezas,conf_d.productoId,pro.codigo,pro.nombre 
                FROM configuracion_detalle_d as conf_d 
                INNER JOIN producto as pro on pro.productoId=conf_d.productoId 
                WHERE conf_d.configdId=$config";
        $query = $this->db->query($strq);
        return $query;
    }

    function cotizacion_producto_cod_search($usu)
    {
        $strq = "SELECT * FROM producto where activo=1 and codigo like '%" . $usu . "%' ORDER BY nombre ASC";
        $query = $this->db->query($strq);

        return $query;
    }

    function cotizacion_producto_nom_search($usu)
    {
        $strq = "SELECT * FROM producto where activo=1 and nombre like '%" . $usu . "%' ORDER BY nombre ASC";
        $query = $this->db->query($strq);

        return $query;
    }

    function cotizacion_codigo_search($id, $usu)
    {
        $strq = "SELECT * FROM cotizaciones_productos where cotizacionesId = $id and activo = 1 and codigo like '%" . $usu . "%' ORDER BY nombre ASC";
        $query = $this->db->query($strq);

        return $query;
    }


    function cotizacion_servicio_cod_search($usu)
    {
        $strq = "SELECT * FROM servicios where activo=1 and codigo like '%" . $usu . "%' ORDER BY servico ASC";
        $query = $this->db->query($strq);

        return $query;
    }

    function cotizacion_servicio_nom_search($usu)
    {
        $strq = "SELECT * FROM servicios where activo=1 and servico like '%" . $usu . "%' ORDER BY servico ASC";
        $query = $this->db->query($strq);

        return $query;
    }

    function clientesallsearch($usu)
    {
        $strq = "SELECT * FROM clientes where activo=1 and nombre like '%" . $usu . "%' ORDER BY nombre ASC";
        $query = $this->db->query($strq);
        return $query;
    }
    
    function personalallsearch($usu)
    {
        $strq = "SELECT * FROM personal where estatus=1 and nombre like '%$usu%' or apellido_paterno like '%$usu%' or apellido_materno like '%$usu%'  ORDER BY nombre ASC";
        $query = $this->db->query($strq);

        return $query;
    }
    function productoallsearch($usu, $perfil, $sucursal)
    {
        if ($perfil == 1) {
            $wheres = '';
        } else {
            $wheres = "pros.sucursalId=$sucursal and pros.pertenece=1 and ";
        }
        $strq = "SELECT pro.* 
                FROM producto as pro 
                INNER JOIN producto_sucursal as pros on pros.productoId = pro.productoId
                where 
                    $wheres
                    pro.activo=1 and 
                    ( pro.nombre like '%$usu%' or 
                      pro.codigo like '%$usu%')
                group by pro.productoId 
                ORDER BY pro.nombre ASC";
        $query = $this->db->query($strq);
        //group by pro.productoId 
        return $query;
    }

    function productoallsearch2($usu)
    {
        $strq = "SELECT * FROM producto where activo=1 and nombre like '%$usu%' or codigo like '%$usu%'  ORDER BY nombre ASC";
        $query = $this->db->query($strq);

        return $query;
    }

    public function getselectwhere($tables, $cols, $values)
    {
        $this->db->select("*");
        $this->db->from($tables);
        $this->db->where($cols, $values); /// Se puede ocupar un array para n condiciones
        $query = $this->db->get();
        //$this->db->close();
        return $query->result();
    }

    public function getselectwhere_n_consulta($tables, $values)
    {
        $this->db->select("*");
        $this->db->from($tables);
        $this->db->where($values); /// Se puede ocupar un array para n condiciones
        $query = $this->db->get();
        //$this->db->close();
        return $query->result();
    }

    public function getselectwherNOrder($tables, $where, $nord, $ord)
    {
        $this->db->select("*");
        $this->db->from($tables);
        $this->db->where($where);
        $this->db->order_by($nord, $ord);
        $query = $this->db->get();
        return $query->result();
    }

    function getselectwhere_row($table, $where)
    {
        $this->db->select('*');
        $this->db->from($table);
        $this->db->where($where);
        $query = $this->db->get();
        return $query->row();
    }

    function replaceCatalogo($table, $where)
    {
        $this->db->where($where);
        $this->db->delete($table);
    }

    function ingresarventad($idventa, $producto, $servicio, $cantidad, $precio)
    {
        $strq = "INSERT INTO venta_detalle(id_venta, id_producto, id_servicio, cantidad, precio) VALUES ($idventa,$producto,$servicio,$cantidad,$precio)";
        $query = $this->db->query($strq);
        $this->db->close();
    }

    function stock_descontar_producto($cantidad, $id)
    {
        $strq = "UPDATE producto SET stock_disponible = stock_disponible-$cantidad WHERE productoId = $id";
        $this->db->query($strq);
    }

    function getventasd($id)
    {
        $strq = "SELECT vendell.cantidad, pro.nombre, vendell.precio
        FROM venta_detalle as vendell
        inner join producto as pro on pro.productoId=vendell.id_producto
        where vendell.id_venta=$id
        UNION
        SELECT vendell.cantidad, ser.servico AS nombre, vendell.precio
        FROM venta_detalle as vendell
        inner join servicios as ser on ser.id=vendell.id_servicio
        where vendell.id_venta=$id";
        $query = $this->db->query($strq);
        $this->db->close();
        return $query;
    }

    public function get_records_condition($condition, $table)
    {
        $sql = "SELECT * FROM $table WHERE $condition";
        $query = $this->db->query($sql);
        return $query->result();
    }


    ///====== Unidades =========
    function get_unidades($params, $idCliente)
    {
        $columns = array(
            0 => '*',
        );
        $columns2 = array(
            0 => 'unidadId',
            1 => 'modelo'
        );
        $select = "";
        foreach ($columns as $c) {
            $select .= "$c, ";
        }
        $this->db->select($select);
        $this->db->from('unidades');
        $where = array('activo' => 1, 'clienteId' => $idCliente);
        $this->db->where($where);
        if (!empty($params['search']['value'])) {
            $search = $params['search']['value'];
            $this->db->group_start();
            foreach ($columns2 as $c) {
                $this->db->or_like($c, $search);
            }
            $this->db->group_end();
        }
        $this->db->order_by($columns2[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'], $params['start']);
        $query = $this->db->get();
        return $query;
    }

    public function total_unidades($params, $idCliente)
    {
        $columns = array(
            0 => '*',
        );
        $this->db->select('COUNT(1) as total');
        $this->db->from('unidades');
        $where = array('activo' => 1, 'clienteId' => $idCliente);
        $this->db->where($where);
        if (!empty($params['search']['value'])) {
            $search = $params['search']['value'];
            $this->db->group_start();
            foreach ($columns as $c) {
                $this->db->or_like($c, $search);
            }
            $this->db->group_end();
        }
        $query = $this->db->get();
        return $query->row()->total;
    }

    //-----agenda servicio---------------------------

   /* public function get_agenda_servicios($inicio, $fin){
        $strq = "SELECT v.id_venta,v.id_cliente,v.id_cotizacion,v.monto_total, v.fecha_servicio, v.hora_servicio, c.nombre, c.celular, d.id_servicio, s.servico as servicio, s.descripcion, s.duracion, s.costo_servicio, u.placas, u.modelo, u.ano
                    FROM ventas as v 
                    LEFT JOIN clientes AS c ON v.id_cliente = c.clienteId
                    LEFT JOIN venta_detalle AS d ON v.id_venta = d.id_venta
                    LEFT JOIN servicios AS s ON d.id_servicio = s.id
                    LEFT JOIN unidades AS u ON v.unidad_servicio = u.unidadId

                    WHERE d.id_servicio != 0 AND v.fecha_servicio BETWEEN '$inicio' AND '$fin'";

        $query = $this->db->query($strq);
        $this->db->close();
        return $query->result();
    }*/

    public function get_agenda_servicios($inicio,$fin){ 
        $strq = "SELECT v.id_venta,v.id_cliente,v.id_cotizacion,v.monto_total, v.fecha_servicio, v.hora_servicio, c.nombre, c.celular, d.id_servicio,s.servico as servicio, s.duracion, s.costo_servicio, u.placas, u.modelo, u.ano,ADDTIME(CONCAT(v.fecha_servicio,' ',v.hora_servicio),SEC_TO_TIME(SUM(TIME_TO_SEC(s.duracion)))) AS hsalida2,v.check_servicio_cita,hs.estatus,(select group_concat(s2.servico) as serviciott from venta_detalle AS vd2 LEFT JOIN servicios AS s2 ON vd2.id_servicio = s2.id  where vd2.activo=1 AND vd2.id_venta=v.id_venta) as serviciott 
        FROM ventas as v  
        LEFT JOIN clientes AS c ON v.id_cliente = c.clienteId 
        LEFT JOIN venta_detalle AS d ON v.id_venta = d.id_venta 
        LEFT JOIN servicios AS s ON d.id_servicio = s.id 
        LEFT JOIN unidades AS u ON v.unidad_servicio = u.unidadId 
        LEFT JOIN hoja_servicio AS hs ON hs.id_venta = v.id_venta 
        WHERE d.id_servicio != 0 AND v.activo=1 AND v.fecha_servicio BETWEEN '$inicio' AND '$fin' GROUP BY v.id_venta"; 
        $query = $this->db->query($strq);
        return $query->result();
    }
    
    function getQuejasClientes($id){ 
        $this->db->select('quejas'); 
        $this->db->from('clientes'); 
        $this->db->where('clienteId', $id); 
        $this->db->where('activo', 1); 
 
        $query = $this->db->get(); 
        return $query->row(); 
    }

    function get_agenda_servicios_fecha($params, $fecha, $cliente) 
    { 
        $columns = array( 
            0 => 'v.id_venta', 
            1 => 'v.id_cliente', 
            2 => 'v.id_cotizacion', 
            3 => 'd.id_servicio', 
            4 => 'v.fecha_servicio', 
            5 => 'v.hora_servicio', 
            6 => 's.descripcion', 
            7 => 's.servico as servicio', 
            8 => 's.costo_servicio', 
            9 => 'd.estatus', 
            10 => 'v.check_servicio_cita', 
            //11 => 'u.placas', 
            //12 => 'u.modelo', 
            //13 => 'u.ano', 
            //14 => 'v.monto_total', 
        ); 
        $columns2 = array( 
            0 => 'v.id_venta', 
            1 => 'v.id_cliente', 
            2 => 'v.id_cotizacion', 
            3 => 'd.id_servicio', 
            4 => 'v.fecha_servicio', 
            5 => 'v.hora_servicio', 
            6 => 's.descripcion', 
            7 => 's.servico as servicio', 
            8 => 's.costo_servicio', 
            9 => 'd.estatus', 
            //10 => ' c.nombre', 
            //11 => 'u.placas', 
            //12 => 'u.modelo', 
            //13 => 'u.ano', 
            //14 => 'v.monto_total', 
        ); 
        $select = ""; 
        foreach ($columns as $c) { 
            $select .= "$c, "; 
        } 
        $this->db->select($select); 
        $this->db->from('ventas v'); 
        $this->db->join('clientes c', 'v.id_cliente = c.clienteId', 'left'); 
        $this->db->join('venta_detalle d', 'v.id_venta = d.id_venta', 'left'); 
        $this->db->join('servicios s', 'd.id_servicio = s.id', 'left'); 
        $this->db->join('unidades u', 'v.unidad_servicio = u.unidadId', 'left'); 
        $this->db->where('d.id_servicio !=', 0); 
        $this->db->where('v.fecha_servicio >=', $fecha); 
        $this->db->where('v.fecha_servicio <=', $fecha); 
        $this->db->where('v.id_cliente =', $cliente); 
 
        if (!empty($params['search']['value'])) { 
            $search = $params['search']['value']; 
            $this->db->group_start(); 
            foreach ($columns2 as $c) { 
                $this->db->or_like($c, $search); 
            } 
            $this->db->group_end(); 
        } 
        $this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']); 
        $this->db->limit($params['length'], $params['start']); 
        $query = $this->db->get(); 
        return $query; 
    } 
 
        public function get_agenda_servicios_fecha_total($params, $fecha, $cliente) 
    { 
        $columns = array( 
            0 => 'v.id_venta', 
            1 => 'v.id_cliente', 
            2 => 'v.id_cotizacion', 
            3 => 'd.id_servicio', 
            4 => 'v.fecha_servicio', 
            5 => 'v.hora_servicio', 
            6 => 's.descripcion', 
            7 => 's.servico as servicio', 
            8 => 's.costo_servicio', 
            9 => 'd.estatus', 
            //10 => ' c.nombre', 
            //11 => 'u.placas', 
            //12 => 'u.modelo', 
            //13 => 'u.ano', 
            //14 => 'v.monto_total', 
        ); 
 
        $this->db->select('COUNT(1) as total'); 
        $this->db->from('ventas v'); 
        $this->db->join('clientes c', 'v.id_cliente = c.clienteId', 'left'); 
        $this->db->join('venta_detalle d', 'v.id_venta = d.id_venta', 'left'); 
        $this->db->join('servicios s', 'd.id_servicio = s.id', 'left'); 
        $this->db->join('unidades u', 'v.unidad_servicio = u.unidadId', 'left'); 
        $this->db->where('d.id_servicio !=', 0); 
        $this->db->where('v.fecha_servicio >=', $fecha); 
        $this->db->where('v.fecha_servicio <=', $fecha); 
        $this->db->where('v.id_cliente =', $cliente); 
 
        if (!empty($params['search']['value'])) { 
        $search = $params['search']['value']; 
        $this->db->group_start(); 
        foreach ($columns as $c) { 
            $this->db->or_like($c, $search); 
        } 
        $this->db->group_end(); 
        } 
        $query = $this->db->get(); 
        return $query->row()->total; 
    } 
 
 
    function get_agenda_servicios_historico($params, $cliente) 
    { 
        $columns = array( 
            0 => 'v.id_venta', 
            1 => 'v.id_cliente', 
            2 => 'v.id_cotizacion', 
            3 => 'd.id_servicio', 
            4 => 'v.fecha_servicio', 
            5 => 'v.hora_servicio', 
            6 => 's.descripcion', 
            7 => 's.servico as servicio', 
            8 => 's.costo_servicio', 
            9 => 'd.estatus', 
            //10 => ' c.nombre', 
            //11 => 'u.placas', 
            //12 => 'u.modelo', 
            //13 => 'u.ano', 
            //14 => 'v.monto_total', 
        ); 
        $columns2 = array( 
            0 => 'v.id_venta', 
            1 => 'v.id_cliente', 
            2 => 'v.id_cotizacion', 
            3 => 'd.id_servicio', 
            4 => 'v.fecha_servicio', 
            5 => 'v.hora_servicio', 
            6 => 's.descripcion', 
            7 => 's.servico as servicio', 
            8 => 's.costo_servicio', 
            9 => 'd.estatus', 
            //10 => ' c.nombre', 
            //11 => 'u.placas', 
            //12 => 'u.modelo', 
            //13 => 'u.ano', 
            //14 => 'v.monto_total', 
        ); 
        $select = ""; 
        foreach ($columns as $c) { 
            $select .= "$c, "; 
        } 
        $this->db->select($select); 
        $this->db->from('ventas v'); 
        $this->db->join('clientes c', 'v.id_cliente = c.clienteId', 'left'); 
        $this->db->join('venta_detalle d', 'v.id_venta = d.id_venta', 'left'); 
        $this->db->join('servicios s', 'd.id_servicio = s.id', 'left'); 
        $this->db->join('unidades u', 'v.unidad_servicio = u.unidadId', 'left'); 
        $this->db->where('d.id_servicio !=', 0); 
        $this->db->where('v.id_cliente =', $cliente); 
 
        if (!empty($params['search']['value'])) { 
            $search = $params['search']['value']; 
            $this->db->group_start(); 
            foreach ($columns2 as $c) { 
                $this->db->or_like($c, $search); 
            } 
            $this->db->group_end(); 
        } 
        $this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']); 
        $this->db->limit($params['length'], $params['start']); 
        $query = $this->db->get(); 
        return $query; 
    } 
 
        public function get_agenda_servicios_historico_total($params, $cliente) 
    { 
        $columns = array( 
            0 => 'v.id_venta', 
            1 => 'v.id_cliente', 
            2 => 'v.id_cotizacion', 
            3 => 'd.id_servicio', 
            4 => 'v.fecha_servicio', 
            5 => 'v.hora_servicio', 
            6 => 's.descripcion', 
            7 => 's.servico as servicio', 
            8 => 's.costo_servicio', 
            9 => 'd.estatus', 
            //10 => ' c.nombre', 
            //11 => 'u.placas', 
            //12 => 'u.modelo', 
            //13 => 'u.ano', 
            //14 => 'v.monto_total', 
        ); 
 
        $this->db->select('COUNT(1) as total'); 
        $this->db->from('ventas v'); 
        $this->db->join('clientes c', 'v.id_cliente = c.clienteId', 'left'); 
        $this->db->join('venta_detalle d', 'v.id_venta = d.id_venta', 'left'); 
        $this->db->join('servicios s', 'd.id_servicio = s.id', 'left'); 
        $this->db->join('unidades u', 'v.unidad_servicio = u.unidadId', 'left'); 
        $this->db->where('d.id_servicio !=', 0); 
        $this->db->where('v.id_cliente =', $cliente); 
 
        if (!empty($params['search']['value'])) { 
        $search = $params['search']['value']; 
        $this->db->group_start(); 
        foreach ($columns as $c) { 
            $this->db->or_like($c, $search); 
        } 
        $this->db->group_end(); 
        } 
        $query = $this->db->get(); 
        return $query->row()->total; 
    } 
 
    function get_agenda_servicios_activos($params, $cliente) 
    { 
        $columns = array( 
            0 => 'v.id_venta', 
            1 => 'v.id_cliente', 
            2 => 'v.id_cotizacion', 
            3 => 'd.id_servicio', 
            4 => 'v.fecha_servicio', 
            5 => 'v.hora_servicio', 
            6 => 's.descripcion', 
            7 => 's.servico as servicio', 
            8 => 's.costo_servicio', 
            9 => 'd.estatus', 
            //10 => ' c.nombre', 
            //11 => 'u.placas', 
            //12 => 'u.modelo', 
            //13 => 'u.ano', 
            //14 => 'v.monto_total', 
        ); 
        $columns2 = array( 
            0 => 'v.id_venta', 
            1 => 'v.id_cliente', 
            2 => 'v.id_cotizacion', 
            3 => 'd.id_servicio', 
            4 => 'v.fecha_servicio', 
            5 => 'v.hora_servicio', 
            6 => 's.descripcion', 
            7 => 's.servico as servicio', 
            8 => 's.costo_servicio', 
            9 => 'd.estatus', 
            //10 => ' c.nombre', 
            //11 => 'u.placas', 
            //12 => 'u.modelo', 
            //13 => 'u.ano', 
            //14 => 'v.monto_total', 
        ); 
        $select = ""; 
        foreach ($columns as $c) { 
            $select .= "$c, "; 
        } 
        $this->db->select($select); 
        $this->db->from('ventas v'); 
        $this->db->join('clientes c', 'v.id_cliente = c.clienteId', 'left'); 
        $this->db->join('venta_detalle d', 'v.id_venta = d.id_venta', 'left'); 
        $this->db->join('servicios s', 'd.id_servicio = s.id', 'left'); 
        $this->db->join('unidades u', 'v.unidad_servicio = u.unidadId', 'left'); 
        $this->db->where('d.id_servicio !=', 0); 
        $this->db->where('d.estatus !=', 2); 
        $this->db->where('v.id_cliente =', $cliente); 
 
        if (!empty($params['search']['value'])) { 
            $search = $params['search']['value']; 
            $this->db->group_start(); 
            foreach ($columns2 as $c) { 
                $this->db->or_like($c, $search); 
            } 
            $this->db->group_end(); 
        } 
        $this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']); 
        $this->db->limit($params['length'], $params['start']); 
        $query = $this->db->get(); 
        return $query; 
    } 
 
        public function get_agenda_servicios_activos_total($params, $cliente) 
    { 
        $columns = array( 
            0 => 'v.id_venta', 
            1 => 'v.id_cliente', 
            2 => 'v.id_cotizacion', 
            3 => 'd.id_servicio', 
            4 => 'v.fecha_servicio', 
            5 => 'v.hora_servicio', 
            6 => 's.descripcion', 
            7 => 's.servico as servicio', 
            8 => 's.costo_servicio', 
            9 => 'd.estatus', 
            //10 => ' c.nombre', 
            //11 => 'u.placas', 
            //12 => 'u.modelo', 
            //13 => 'u.ano', 
            //14 => 'v.monto_total', 
        ); 
 
        $this->db->select('COUNT(1) as total'); 
        $this->db->from('ventas v'); 
        $this->db->join('clientes c', 'v.id_cliente = c.clienteId', 'left'); 
        $this->db->join('venta_detalle d', 'v.id_venta = d.id_venta', 'left'); 
        $this->db->join('servicios s', 'd.id_servicio = s.id', 'left'); 
        $this->db->join('unidades u', 'v.unidad_servicio = u.unidadId', 'left'); 
        $this->db->where('d.id_servicio !=', 0); 
        $this->db->where('d.estatus !=', 2); 
        $this->db->where('v.id_cliente =', $cliente); 
 
        if (!empty($params['search']['value'])) { 
        $search = $params['search']['value']; 
        $this->db->group_start(); 
        foreach ($columns as $c) { 
            $this->db->or_like($c, $search); 
        } 
        $this->db->group_end(); 
        } 
        $query = $this->db->get(); 
        return $query->row()->total; 
    }

    function getDataVehiculo($id_cliente, $id_venta) 
    { 
        $this->db->select('u.placas, u.modelo, u.ano, v.unidad_servicio'); 
        $this->db->from('unidades u'); 
        $this->db->join('ventas v', 'u.unidadId = v.unidad_servicio'); 
        $this->db->where('v.id_venta', $id_venta); 
        $this->db->where('u.clienteId', $id_cliente); 
        $this->db->where('u.activo', 1); 
        $query = $this->db->get(); 
        return $query; 
    }

}
