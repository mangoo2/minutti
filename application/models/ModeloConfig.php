<?php

defined('BASEPATH') OR exit ('No direct script access allowed');

class ModeloConfig extends CI_Model {
    public function __construct() {
        parent::__construct();
    }
    function getlistconfig($params){
        $columns = array( 
            0=>'con.configId',
            1=>'con.codigo',
            2=>'con.nombre',
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('configuracion con');
        
        $this->db->where(array('con.activo'=>1));

        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        $this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        //echo $this->db->get_compiled_select();
        $query=$this->db->get();
        // print_r($query); die;
        return $query;
    }
    public function getlistconfigt($params){
        $columns = array( 
            0=>'con.configId',
            1=>'con.codigo',
            2=>'con.nombre',
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select('COUNT(*) as total');
        $this->db->from('configuracion con');
        
        $this->db->where(array('con.activo'=>1));

        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        //$this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        //$this->db->limit($params['length'],$params['start']);
        //echo $this->db->get_compiled_select();
        $query=$this->db->get();
        // print_r($query); die;
        return $query->row()->total;
    }

    ///====== Tipo camioneta =========!!
    function get_tipo_camioneta($params){
        $columns = array( 
            0=>'id',
            1=>'nombre'
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('tipo_camioneta');
        $where = array('activo'=>1);
        $this->db->where($where);
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        $this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        $query=$this->db->get();
        return $query;
    }
    public function total_tipo_camioneta($params){
        $columns = array( 
            0=>'id',
            1=>'nombre'
        );
        $this->db->select('COUNT(1) as total');
        $this->db->from('tipo_camioneta');
        $where = array('activo'=>1);
        $this->db->where($where);
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }  
        $query=$this->db->get();
        return $query->row()->total;
    }

    function getDetalleConfig($id){
        $this->db->select('ct.*, p.productoId, p.codigo, p.nombre');
        $this->db->from("config_transporte_detalle ct");
        $this->db->join("producto p","p.productoId=ct.id_producto");
        $this->db->where("ct.id_config",$id);
        $this->db->where("ct.status",1);

        $query=$this->db->get(); 
        return $query->result();
    }
}
?>