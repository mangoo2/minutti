<?php
defined('BASEPATH') or exit('No direct script access allowed');

class ModeloCotizaciones extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
	}

	function get_equipment($where)
	{
		$columns = array(
			0 => 'id',
			1 => 'title',
			2 => 'text',
			3 => 'img',
		);
		$select = "";
		foreach ($columns as $c) {
			$select .= "$c, ";
		}
		$this->db->select($select);
		$this->db->from('datos_equipamiento de');
		//$this->db->join('Usuarios u', 'u.UsuarioID = cs.user');
		$this->db->where($where);
		$query = $this->db->get();
		return $query;
	}

	function get_idequipment_up($where)
	{
		$this->db->select('id_equipamiento');
		$this->db->from('equipamiento eq');
		$this->db->where($where);
		return $this->db->get();
	}

	function get_equipment_up($arrayid)
	{
		$columns = array(
			0 => 'id',
			1 => 'title',
			2 => 'text',
			3 => 'img',
		);
		$select = "";
		foreach ($columns as $c) {
			$select .= "$c, ";
		}
		$this->db->select($select);
		$this->db->from('datos_equipamiento de');

		foreach ($arrayid->result() as $id) {
			$this->db->or_where('id', $id->id_equipamiento);
		}
		return $this->db->get();
	}

	function get_all_equipment($table)
	{
		$this->db->select('*');
		$this->db->from($table);
		//$this->db->where($where);
		$query = $this->db->get();
		return $query;
	}

	function insert_n_cotizacion($Tabla, $data)
	{
		$this->db->insert($Tabla, $data);
		return $this->db->insert_id();
	}

	function check_n_cotizacion($Tabla, $id)
	{

		$this->db->select('id_cotizacion');
		$this->db->from($Tabla);
		$this->db->where('id_cotizacion', $id);
		$query = $this->db->get();

		if ($query->num_rows() > 0) {
			$this->db->delete($Tabla, array('id_cotizacion' => $id));
		}
	}

	function updateCatalogo($Tabla, $data, $where)
	{
		$this->db->set($data);
		$this->db->where($where);
		$this->db->update($Tabla);
		//return $id;
	}

	function search_equip($input)
	{
		$strq = "SELECT * FROM datos_equipamiento where title like '%$input%' ORDER BY title ASC";
		$query = $this->db->query($strq);
		return $query;
	}
}
