
<?php
$a=session_id();
if(empty($a)) session_start();
defined('BASEPATH') OR exit ('No direct script access allowed');

class ModeloGastosPagos extends CI_Model {
    public function __construct() {
        parent::__construct();
        if (isset($_SESSION['idpersonal'])) {
            $this->idpersonal=$_SESSION['idpersonal'];
        }else{
            $this->idpersonal=0;
        }
    }

    /// == Gastos == ///
    function get_gastos($params){
        $columns = array( 
            0=>'gas.id',
            1=>'gas.nombre',
            2=>'gas.monto',
            3=>'gas.fecha',
            4=>'p.nombre AS empleado',
            5=>'DATE_FORMAT(gas.fecha,  "%d / %m / %Y" ) AS fecha_gas',
            6=>'p.apellido_paterno',
            7=>'p.apellido_materno',
            8=>'gas.tipocobro',
            9=>'c.nombre AS categoria',
            10=>'gas.categoria AS idcategoria'
        );
        $columns2 = array( 
            0=>'gas.id',
            1=>'gas.nombre',
            2=>'gas.monto',
            3=>'gas.fecha',
            4=>'p.nombre',
            5=>'p.apellido_paterno',
            6=>'p.apellido_materno',
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('gastospagos gas');
        $this->db->join('personal p','p.personalId=gas.usuario','left'); 
        $this->db->join('categoria_cobro c','c.id=gas.categoria','left'); 
        
        if($params['tipo_gasto']==0){
            $where = array('gas.activo'=>1);
        }else{
            $where = array('gas.activo'=>1,'tipocobro'=>$params['tipo_gasto']); 
        }
        
        $this->db->where($where);
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns2 as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        $this->db->order_by($columns2[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        $query=$this->db->get();
        return $query;
    }
    public function total_gastos($params){
        $columns = array( 
            0=>'gas.id',
            1=>'gas.nombre',
            2=>'gas.monto',
            3=>'gas.fecha',
            4=>'p.nombre',
            5=>'p.apellido_paterno',
            6=>'p.apellido_materno',
        );
        $this->db->select('COUNT(1) as total');
        $this->db->from('gastospagos gas');
        $this->db->join('personal p','p.personalId=gas.usuario','left'); 
        if($params['tipo_gasto']==0){
            $where = array('gas.activo'=>1);
        }else{
            $where = array('gas.activo'=>1,'tipocobro'=>$params['tipo_gasto']); 
        }
        $this->db->where($where);
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }  
        $query=$this->db->get();
        return $query->row()->total;
    }

    function categoriasallsearch($usu,$tipo){
        $strq = "SELECT * FROM categoria_cobro WHERE activo=1 AND tipo=$tipo AND nombre like '%".$usu."%' ORDER BY nombre ASC";
        $query = $this->db->query($strq);
        $this->db->close();
        return $query;
    }

    ///====== Categoria =========!!
    function get_categoria($params){
        $columns = array( 
            0=>'id',
            1=>'nombre'
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('categoria_cobro');
        $where = array('activo'=>1,'tipo'=>$params['tipo']);
        $this->db->where($where);
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        $this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        $query=$this->db->get();
        return $query;
    }
    public function total_categoria($params){
        $columns = array( 
            0=>'id',
            1=>'nombre'
        );
        $this->db->select('COUNT(1) as total');
        $this->db->from('categoria_cobro');
        $where = array('activo'=>1,'tipo'=>$params['tipo']);
        $this->db->where($where);
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }  
        $query=$this->db->get();
        return $query->row()->total;
    }

}