
<?php
$a = session_id();
if (empty($a)) session_start();
defined('BASEPATH') or exit('No direct script access allowed');

class ModeloHerramientas extends CI_Model
{
  public function __construct()
  {
    parent::__construct();
    if (isset($_SESSION['idpersonal'])) {
      $this->idpersonal = $_SESSION['idpersonal'];
    } else {
      $this->idpersonal = 0;
    }
  }

  function insertEntrance($tabla, $data)
  {
    $this->db->insert($tabla, $data);
    return $this->db->insert_id();
  }

  function updateEntrance($tabla, $data, $where)
  {
    $this->db->set($data);
    $this->db->where($where);
    $this->db->update($tabla);
    //return $id;
  }

  function getSelectWhere($tables, $cols, $values)
  {
    $this->db->select("*");
    $this->db->from($tables);
    $this->db->where($cols, $values); /// Se puede ocupar un array para n condiciones
    $query = $this->db->get();
    //$this->db->close();
    return $query->result();
  }

  function get_herramientas($params)
  {
    $columns = array(
      0 => 'id_herramienta',
      1 => 'medida',
      2 => 'nombre',
      3 => 'ubicacion',
      4 => 'stock',
    );
    $columns2 = array(
      0 => 'id_herramienta',
      1 => 'medida',
      2 => 'nombre',
      3 => 'ubicacion',
      4 => 'stock',
    );
    $select = "";
    foreach ($columns as $c) {
      $select .= "$c, ";
    }
    $this->db->select($select);
    $this->db->from('herramientas');
    $this->db->where(array('activo' => 1));

    if (!empty($params['search']['value'])) {
      $search = $params['search']['value'];
      $this->db->group_start();
      foreach ($columns2 as $c) {
        $this->db->or_like($c, $search);
      }
      $this->db->group_end();
    }            
    $this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
    $this->db->limit($params['length'],$params['start']);
    $query=$this->db->get();
    return $query;
  }

  public function get_total_herramientas($params)
  {
    $columns = array(
      0 => 'id_herramienta',
      1 => 'medida',
      2 => 'nombre',
      3 => 'ubicacion',
      4 => 'stock',
    );

    $this->db->select('COUNT(1) as total');
    $this->db->from('herramientas');
    $this->db->where(array('activo'=>1));

    if (!empty($params['search']['value'])) {
      $search = $params['search']['value'];
      $this->db->group_start();
      foreach ($columns as $c) {
        $this->db->or_like($c, $search);
      }
      $this->db->group_end();
    }
    $query = $this->db->get();
    return $query->row()->total;
  }
}
