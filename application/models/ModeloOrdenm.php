<?php

defined('BASEPATH') OR exit ('No direct script access allowed');

class ModeloOrdenm extends CI_Model {
    public function __construct() {
        parent::__construct();
    }
    function getlist_row($params){
        $columns = array( 
            0=>'om.ordenId',
            1=>'cli.nombre cliente',
            2=>'om.serie',
            3=>'om.fecha_solicitud',
            4=>'om.fecha_entrega',
            5=>'om.lider',
            6=>'om.activo'
        );
        $columns_s = array( 
            0=>'om.ordenId',
            1=>'cli.nombre',
            2=>'om.serie',
            3=>'om.fecha_solicitud',
            4=>'om.fecha_entrega',
            5=>'om.lider',
            6=>'om.activo'
        );
        
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('orden_de_material om');
        $this->db->join('clientes cli', 'cli.clienteId = om.clienteId');
        //$this->db->join('personal per', 'per.personalId = om.lider');

        
        //$this->db->where(array('om.activo'=>1));
        //$this->db->where(array('facturaabierta'=>1));
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns_s as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        $this->db->order_by($columns_s[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        //echo $this->db->get_compiled_select();
        $query=$this->db->get();
        // print_r($query); die;
        return $query;
    }
    
    public function getlist_row_t($params){
        $columns = array( 
            0=>'om.ordenId',
            1=>'cli.nombre cliente',
            2=>'om.serie',
            3=>'om.fecha_solicitud',
            4=>'om.fecha_entrega',
            5=>'om.lider',
            6=>'om.activo'
        );
        $columns_s = array( 
            0=>'om.ordenId',
            1=>'cli.nombre',
            2=>'om.serie',
            3=>'om.fecha_solicitud',
            4=>'om.fecha_entrega',
            5=>'om.lider',
            6=>'om.activo'
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select('COUNT(*) as total');
        $this->db->from('orden_de_material om');
        $this->db->join('clientes cli', 'cli.clienteId = om.clienteId');
        //$this->db->join('personal per', 'per.personalId = om.lider');

        
        //$this->db->where(array('om.activo'=>1));
        //$where = ;
        //$this->db->where(array('facturaabierta'=>1));
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns_s as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
   
        $query=$this->db->get();

        return $query->row()->total;
    }

}