<?php

defined('BASEPATH') OR exit ('No direct script access allowed');

class ModeloProductos extends CI_Model {
    public function __construct() {
        parent::__construct();
    }
    function getlistproductos($params){
        //$sucursal=$params['sucursal'];
        $categoria=$params['categoria'];

        $columns = array( 
            0=>'pro.productoId',
            1=>'pro.codigo',
            2=>'pro.nombre',
            3=>'cat.categoria',
            4=>'pro.stock_disponible as stock',
            5=>'pro.cantidad_minima_existencia'
        );
        $columnsss = array( 
            0=>'pro.productoId',
            1=>'pro.codigo',
            2=>'pro.nombre',
            3=>'cat.categoria',
            4=>'pro.stock_disponible',
            5=>'pro.cantidad_minima_existencia'
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('producto pro');
        $this->db->join('categoria cat', 'cat.categoriaId = pro.categoriaId','LEFT');
        //$this->db->join('producto_sucursal pros', 'pros.productoId = pro.productoId');
        /*
        if($sucursal>0){
            $this->db->where(array('pros.sucursalId'=>$sucursal,'pros.pertenece'=>1));
        }
        */
        if($categoria>0){
            $this->db->where(array('pro.categoriaId'=>$categoria));
        }
        
        $this->db->where(array('pro.activo'=>1));

        //$this->db->group_by("pro.productoId");
        
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columnsss as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        $this->db->order_by($columnsss[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        //echo $this->db->get_compiled_select();
        $query=$this->db->get();
        // print_r($query); die;
        return $query;
    }
    public function getlistproductost($params){
        //$sucursal=$params['sucursal'];
        $categoria=$params['categoria'];

        $columns = array( 
            0=>'pro.productoId',
            1=>'pro.codigo',
            2=>'pro.nombre',
            3=>'uni.categoria',
            4=>'pro.stock_disponible',
            5=>'pro.cantidad_minima_existencia'
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select('COUNT(*) as total');
        $this->db->from('producto pro');
        $this->db->join('categoria cat', 'cat.categoriaId = pro.categoriaId','LEFT');
        //$this->db->join('producto_sucursal pros', 'pros.productoId = pro.productoId');
        /*
        if($sucursal>0){
            $this->db->where(array('pros.sucursalId'=>$sucursal,'pros.pertenece'=>1));
        }
        */
        if($categoria>0){
            $this->db->where(array('pro.categoriaId'=>$categoria));
        }
        
        $this->db->where(array('pro.activo'=>1));
        
        //$this->db->group_by("pro.productoId");

        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        //$this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        //$this->db->limit($params['length'],$params['start']);
        //echo $this->db->get_compiled_select();
        $query=$this->db->get();
        // print_r($query); die;
        return $query->row()->total;
    }

    ///====== Unidad =========!!
    function get_unidads($params){
        $columns = array( 
            0=>'*',
        );
        $columns2 = array( 
            0=>'unidadId',
            1=>'unidad'
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('unidad');
        $where = array('activo'=>1);
        $this->db->where($where);
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns2 as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        $this->db->order_by($columns2[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        $query=$this->db->get();
        return $query;
    }
    public function total_unidads($params){
        $columns = array( 
            0=>'*',
        );
        $this->db->select('COUNT(1) as total');
        $this->db->from('unidad');
        $where = array('activo'=>1);
        $this->db->where($where);
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }  
        $query=$this->db->get();
        return $query->row()->total;
    }

    ///====== Categoria =========!!
    function get_categoria($params){
        $columns = array( 
            0=>'*',
        );
        $columns2 = array( 
            0=>'categoriaId',
            1=>'categoria'
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('categoria');
        $where = array('activo'=>1);
        $this->db->where($where);
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns2 as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        $this->db->order_by($columns2[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        $query=$this->db->get();
        return $query;
    }
    public function total_categoria($params){
        $columns = array( 
            0=>'*',
        );
        $this->db->select('COUNT(1) as total');
        $this->db->from('categoria');
        $where = array('activo'=>1);
        $this->db->where($where);
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }  
        $query=$this->db->get();
        return $query->row()->total;
    }

    public function comprasdelete($id){
        $strq = "UPDATE orden_compra SET activo=0 where id=$id";
        $this->db->query($strq);
    }

    ///====== Configuracion =========!!
    function get_config($params){
        $columns = array( 
            0=>'id',
            1=>'nombre'
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('config_producto');
        $where = array('activo'=>1);
        $this->db->where($where);
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        $this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        $query=$this->db->get();
        return $query;
    }

    public function total_config($params){
        $columns = array( 
            0=>'id',
            1=>'nombre'
        );
        $this->db->select('COUNT(1) as total');
        $this->db->from('config_producto');
        $where = array('activo'=>1);
        $this->db->where($where);
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }  
        $query=$this->db->get();
        return $query->row()->total;
    }

    function getproducto($id){
        $strq = "SELECT * FROM producto where productoId=$id";
        $query = $this->db->query($strq);
        $this->db->close();
        return $query;
    }

    function productos_all($categoria,$nave){
        $where='';
        if($categoria==0){
            $where=''; 
        }else{
            $where=' AND p.categoriaId='.$categoria; 
        }

        $wheren='';
        $wheren2='';
        if($nave==0){
            $wheren=''; 
            $wheren2='';
        }else{
            $wheren='INNER JOIN producto_sucursal AS pros ON pros.productoId = p.productoId'; 
            $wheren2=' AND pros.sucursalId='.$nave.' AND pros.pertenece=1'; 
        }

        $strq="SELECT p.productoId,p.codigo,p.nombre AS producto,p.descripcion,p.preciocompra,p.cantidad_minima_existencia AS existencia_min,c.categoria,u.unidad,co.nombre AS config
                FROM producto as p 
                INNER JOIN categoria as c on c.categoriaId=p.categoriaId
                INNER JOIN unidad as u on u.unidadId=p.unidadId
                LEFT JOIN config_producto as co on co.id=p.configuracion
                $wheren 
                WHERE p.activo=1".$where.$wheren2;
        $query = $this->db->query($strq);
        return $query->result();
    }

    function productos_sucu($id,$nave){
        $where='';
        if($nave==0){
            $where=''; 
        }else{
            $where=' AND p.sucursalId='.$nave; 
        }
        $strq="SELECT s.sucursal,p.stock
                FROM producto_sucursal as p 
                LEFT JOIN sucursal as s on s.sucursalId=p.sucursalId
                WHERE p.productoId=$id".$where;
        $query = $this->db->query($strq);
        return $query->result();
    }


    function getProductosSucursales($id){
        $this->db->select('ps.stock,ps.sucursalId');
        $this->db->from("producto_sucursal ps");
        $this->db->where("activo",1); 
        $this->db->where("productoId",$id); 
        $query=$this->db->get(); 
        return $query->result();
    }


    
    ///====== Marca =========!!
    function get_marca($params){
        $columns = array( 
            0=>'*',
        );
        $columns2 = array( 
            0=>'marcaId',
            1=>'marca'
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('marca');
        $where = array('activo'=>1);
        $this->db->where($where);
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns2 as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        $this->db->order_by($columns2[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        $query=$this->db->get();
        return $query;
    }

    public function total_marca($params){
        $columns = array( 
            0=>'*',
        );
        $this->db->select('COUNT(1) as total');
        $this->db->from('marca');
        $where = array('activo'=>1);
        $this->db->where($where);
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }  
        $query=$this->db->get();
        return $query->row()->total;
    }

}
?>