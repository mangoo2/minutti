
<?php
$a=session_id();
if(empty($a)) session_start();
defined('BASEPATH') OR exit ('No direct script access allowed');

class ModeloReportes extends CI_Model {
    public function __construct() {
        parent::__construct();
        if (isset($_SESSION['idpersonal'])) {
            $this->idpersonal=$_SESSION['idpersonal'];
        }else{
            $this->idpersonal=0;
        }
    }

    function getReportePros($id,$tipo){
        $this->db->select('ps.*, p.codigo, p.nombre, s.sucursal');
        $this->db->from("producto_sucursal ps");
        $this->db->join("producto p","p.productoId=ps.productoId");
        $this->db->join("sucursal s","s.sucursalId=ps.sucursalId");
        $this->db->where("ps.sucursalId",$id);
        if($tipo==1){ //productos con existencia
            $this->db->where("ps.stock>",1);
        }
        if($tipo==2){ //Productos con existencia minima
            $this->db->where("ps.stock=p.cantidad_minima_existencia");
        }
        if($tipo==3){ //Productos sin stock
            $this->db->where("ps.stock<=0");
        }

        $this->db->where("ps.activo",1);
        $this->db->where("p.activo",1);
        $query=$this->db->get(); 
        return $query->result();
    }

    function getReporteProvees($id){
        $this->db->select('p.idproveedor, p.nombre, SUM(oc.total) as total_compra, oc.fecha_compra, oc.fecha_surtimiento, db.banco, db.titular, db.cuenta, db.clave, db.tarjeta');
        $this->db->from("proveedor p");
        $this->db->join("orden_compra oc","oc.idproveedor=p.idproveedor");
        $this->db->join("proveedor_bancarios db","db.idproveedor=p.idproveedor");
        $this->db->where("oc.activo",1); 
        $this->db->where("oc.estatus",2); 
        $this->db->where("db.activo",1); 
        if($id!="t"){
            $this->db->where("p.idproveedor",$id); 
        }else{
            $this->db->group_by("oc.idproveedor");
        }

        $query=$this->db->get(); 
        return $query->result();
    }

    function getReporteCompras($id,$fi,$ff,$metodo){
        $this->db->select("p.idproveedor, p.nombre, SUM(oc.total) as total_compra, oc.fecha_compra, oc.fecha_surtimiento, IF(oc.tipo_compra='on','A crédito','Contado') as tipo_compra, oc.metodo_pago, oc.no_oc,
            GROUP_CONCAT(pd.nombre,' - $',ocd.costo,' * ',ocd.cantidad SEPARATOR '<br>') as producto_precio, oc.total ");
        $this->db->from("orden_compra oc");
        $this->db->join("proveedor p","p.idproveedor=oc.idproveedor");
        $this->db->join("orden_compra_detalle ocd","ocd.idorden=oc.id");
        $this->db->join("producto pd","pd.productoId=ocd.idproducto");
        $this->db->where("oc.activo",1); 
        $this->db->where("oc.estatus",2); 
        $this->db->where("ocd.activo",1); 
        if($id!="t")
            $this->db->where("oc.idproveedor",$id); 

        if($metodo!=6)
            $this->db->where("oc.metodo_pago",$metodo); 

        $this->db->where("oc.fecha_compra between '{$fi}' AND '{$ff}'");
        $this->db->group_by("oc.id");
        $query=$this->db->get(); 
        return $query->result();
    }

}