<?php

defined('BASEPATH') OR exit ('No direct script access allowed');

class ModeloRequisicion extends CI_Model {
    public function __construct() {
        parent::__construct();
    }
    function getlist_row($params){
        $columns = array( 
            0=>'rem.requsicionId',
            1=>'rem.personalId',
            2=>'suc.sucursal',
            3=>'rem.fecha_solicitud',
            4=>'rem.personal',
            5=>'rem.estatus',
            6=>'CONCAT(pez.nombre," ",pez.apellido_paterno," ",pez.apellido_materno) AS realizo',
        );

        $columns2 = array( 
            0=>'rem.requsicionId',
            1=>'rem.personalId',
            2=>'suc.sucursal',
            3=>'rem.fecha_solicitud',
            4=>'rem.personal',
            5=>'rem.estatus',
            6=>'perz.nombre',
            7=>'perz.apellido_paterno',
            8=>'perz.apellido_materno',
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('requisicion_material rem');
        //$this->db->join('personal per', 'per.personalId = rem.personalId');
        $this->db->join('sucursal suc', 'suc.sucursalId = rem.destino');
        $this->db->join('personal pez', 'pez.personalId = rem.personalId_realizo','left');
        
        $this->db->where(array('rem.activo'=>1));
        //$this->db->where(array('facturaabierta'=>1));
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns2 as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        $this->db->order_by($columns2[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        //echo $this->db->get_compiled_select();
        $query=$this->db->get();
        // print_r($query); die;
        return $query;
    }
    public function getlist_row_t($params){
        $columns = array( 
            0=>'rem.requsicionId',
            1=>'rem.personalId',
            2=>'suc.sucursal',
            3=>'rem.fecha_solicitud',
            4=>'rem.personal',
            5=>'rem.estatus',
            6=>'perz.nombre',
            7=>'perz.apellido_paterno',
            8=>'perz.apellido_materno',
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select('COUNT(*) as total');
        $this->db->from('requisicion_material rem');
        //$this->db->join('personal per', 'per.personalId = rem.personalId');
        $this->db->join('sucursal suc', 'suc.sucursalId = rem.destino');
        $this->db->join('personal pez', 'pez.personalId = rem.personalId_realizo','left');
        $this->db->where(array('rem.activo'=>1));
        //$where = ;
        //$this->db->where(array('facturaabierta'=>1));
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
   
        $query=$this->db->get();

        return $query->row()->total;
    }

    public function get_requisicion_material_detalle($id){
        $strq = "SELECT r.requsiciondId,r.requsicionId,r.cantidad,r.productoId,p.codigo,p.nombre AS producto
                    FROM requisicion_material_d AS r
                    INNER JOIN producto AS p ON p.productoId=r.productoId
                    where r.activo = 1 AND r.requsicionId=$id";
        $query = $this->db->query($strq);
        return $query->result();
    }


}