<?php

defined('BASEPATH') OR exit ('No direct script access allowed');

class ModeloRetornoMaterial extends CI_Model {
    public function __construct() {
        parent::__construct();
    }
    function getlist_row($params){
        $columns = array( 
            0=>'tra.requsicionId',
            1=>'tra.personalId',
            4=>'tra.fecha_solicitud',
            5=>'per.nombre',
            6=>'per.apellido_paterno',
            7=>'per.apellido_materno',
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('requisicion_material tra');
        $this->db->join('personal per', 'per.personalId = tra.personalId');

        
        $this->db->where(array('tra.activo'=>1));
        //$this->db->where(array('facturaabierta'=>1));
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        $this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        //echo $this->db->get_compiled_select();
        $query=$this->db->get();
        // print_r($query); die;
        return $query;
    }
    public function getlist_row_t($params){
        $columns = array( 
            0=>'tra.requsicionId',
            1=>'tra.personalId',
            4=>'tra.fecha_solicitud',
            5=>'per.nombre',
            6=>'per.apellido_paterno',
            7=>'per.apellido_materno',
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select('COUNT(*) as total');
        $this->db->from('requisicion_material tra');
        $this->db->join('personal per', 'per.personalId = tra.personalId');
        
        $this->db->where(array('tra.activo'=>1));
        //$where = ;
        //$this->db->where(array('facturaabierta'=>1));
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
   
        $query=$this->db->get();

        return $query->row()->total;
    }




}