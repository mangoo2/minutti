<?php
defined('BASEPATH') OR exit ('No direct script access allowed');
class ModeloServicios extends CI_Model {
    public function __construct() {
        parent::__construct();
    }
    function getlist_row($params){
        $columns = array( 
            0=>'s.id',
            1=>'s.servico',
            2=>'s.descripcion',
            3=>'s.costo_servicio',
            4=>'s.costo_especial',
            5=>'s.duracion',
            6=>'s.codigo',
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('servicios s');
        $this->db->where(array('s.activo'=>1));
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        $this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        //echo $this->db->get_compiled_select();
        $query=$this->db->get();
        // print_r($query); die;
        return $query;
    }
    public function getlist_row_t($params){
        $columns = array( 
            0=>'s.id',
            1=>'s.servico',
            2=>'s.descripcion',
            3=>'s.costo_servicio',
            4=>'s.costo_especial',
            5=>'s.duracion',
            6=>'s.codigo',
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select('COUNT(*) as total');
        $this->db->from('servicios s');
        $this->db->where(array('s.activo'=>1));
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
   
        $query=$this->db->get();

        return $query->row()->total;
    }

    function serviciosallsearch($usu){
        $strq = "SELECT * FROM servicios where activo=1 and servico like '%".$usu."%' OR activo=1 and codigo like '%".$usu."%'";
        $query = $this->db->query($strq);
        return $query;
    }
    
    public function get_agenda_servicio_cita($id){
        $strq = "SELECT v.id_venta,v.id_cliente,v.id_cotizacion,v.monto_total, v.fecha_servicio, v.hora_servicio, c.nombre, c.celular, u.placas, u.modelo, u.ano,v.unidad_servicio,c.rfc,c.direccion,c.estado,c.codigo_postal,c.correo,v.metodo
                    FROM ventas as v 
                    LEFT JOIN clientes AS c ON v.id_cliente = c.clienteId
                    LEFT JOIN venta_detalle AS d ON v.id_venta = d.id_venta
                    LEFT JOIN unidades AS u ON v.unidad_servicio = u.unidadId
                    WHERE v.id_venta=$id";
        $query = $this->db->query($strq);
        return $query->result();
    }

    public function get_servicio_cita($id){
        $strq = "SELECT d.id_detalle_venta,v.id_venta, s.servico as servicio, s.descripcion,s.duracion,d.precio,d.cantidad,d.id_servicio
                    FROM ventas as v 
                    LEFT JOIN venta_detalle AS d ON v.id_venta = d.id_venta
                    INNER JOIN servicios AS s ON d.id_servicio = s.id
                    WHERE d.activo=1 AND v.id_venta=$id";
        $query = $this->db->query($strq);
        return $query->result();
    }

}
