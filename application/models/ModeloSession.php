<?php
$a=session_id();
if(empty($a)) session_start();
defined('BASEPATH') OR exit ('No direct script access allowed');

class ModeloSession extends CI_Model {
    public function __construct() {
        parent::__construct();
        date_default_timezone_set('America/Mexico_City');
        $this->fechahoy = date('Y-m-d');
    }
    function login($usu,$pass) {
        $strq = "SELECT usu.UsuarioID,per.personalId,per.nombre, per.apellido_paterno, per.apellido_materno, usu.perfilId, usu.contrasena,per.sucursalId
                FROM usuarios as usu 
                INNER JOIN personal as per on per.personalId=usu.personalId
                where usu.Usuario ='$usu' and per.estatus=1";
        $count = 0;
        $passwo =0;
        $query = $this->db->query($strq);
        $this->db->close();
        foreach ($query->result() as $row) {
            $passwo =$row->contrasena;
            $id = $row->UsuarioID;
            $nom =$row->nombre;
            $name =$row->nombre. ' ' .$row->apellido_paterno. ' ' .$row->apellido_materno;
            $perfil = $row->perfilId; 
            $idpersonal = $row->personalId; 
            $verificar = password_verify($pass,$passwo);
            if ($verificar) {
                $data = array(
                        'logeado' => true,
                        'usuarioid' => $id,
                        'usuario' => $nom,
                        'usuarioname' => $name,
                        'perfilid'=>$perfil,
                        'idpersonal'=>$idpersonal,
                        'sucursal'=>$row->sucursalId
                    );
                $this->session->set_userdata($data);
                
                $count=1;
                //$count=$passwo.'/'.$id.'/'.$nom.'/'.$perfil.'/'.$idpersonal;
            }
            /*
                 si es cero aqui se agregara los mismos parametros pero para el aseso de los residentes si es que lo piden
            */
        } 
        /*
        if ($count==0) {
            $residentes = $this->db->query("CALL SP_GET_SESSIONRESIDENTES('$usu','$pass')");
            $this->db->close();

            foreach ($residentes->result() as $row) {

                $id = $row->AspiranteId;
                $nom =$row->Nombre;
            
            
                $_SESSION['usuarioid']=$id;
                $_SESSION['usuario']=$nom;
                $_SESSION['perfilid']=3;
                $_SESSION['idpersonal']=0;
                $count=1;
            
           
            }
        }
        */
        
        echo $count;
        //echo $strq;
    }
    public function menus($perfil){
        $strq ="SELECT distinct men.MenuId,men.Nombre,men.Icon from menu as men, menu_sub as mens, perfiles_detalles as perfd where men.MenuId=mens.MenuId and perfd.MenusubId=mens.MenusubId and perfd.PerfilId='$perfil' ORDER BY men.MenuId ASC";
        //$strq="CALL SP_GET_MENUS($perfil)";
        $query = $this->db->query($strq);
        return $query;

    }
    public function submenus($perfil,$menu){
        $strq ="SELECT menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon from menu_sub as menus, perfiles_detalles as perfd WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='$perfil' and menus.MenuId='$menu' ORDER BY menus.MenusubId ASC";
        $query = $this->db->query($strq);
        return $query;
    }

    function sucursales()
    {   
        $sql='SELECT * FROM sucursal WHERE activo=1';
        $query = $this->db->query($sql);
        return $query->result();        
    }  

    function suma_producto_sucursal($id)
    {   
        $sql='SELECT COUNT(*) AS total
            FROM producto AS p
            LEFT JOIN producto_sucursal AS ps ON ps.productoId = p.productoId
            WHERE p.activo=1 AND p.cantidad_minima_existencia>=ps.stock AND ps.sucursalId='.$id;
        $query = $this->db->query($sql);
        return $query->result();        
    } 

    function suma_producto_sucursal_datos($id){   
        $sql="SELECT ps.productosId,p.nombre AS producto,ps.stock
            FROM producto AS p
            LEFT JOIN producto_sucursal AS ps ON ps.productoId = p.productoId
            WHERE 
            p.activo=1 AND 
            p.cantidad_minima_existencia>=ps.stock AND 
            ps.sucursalId=$id and
            ps.pertenece=1 and
            (fechanotificacion IS NULL or fechanotificacion<'$this->fechahoy')";
        $query = $this->db->query($sql);
        return $query;        
    } 
    function totalporsucursal(){   
        $sql="SELECT sum(p.costo_compra*p.stock_disponible) as total
            FROM producto AS p
            WHERE p.activo=1";
        $query = $this->db->query($sql);
        /*
        $sql="SELECT sum(p.preciocompra*ps.stock) as total
            FROM producto AS p
            LEFT JOIN producto_sucursal AS ps ON ps.productoId = p.productoId
            WHERE 
            p.activo=1 and  
            ps.sucursalId=$id";
        $query = $this->db->query($sql);
        */
        $total = 0;
        foreach ($query->result() as $itemsp) {
            $total = $itemsp->total;
        }
        return $total;        
    } 

    function is_cliente($usu){
        $this->db->select('clienteId');
        $this->db->from('usuarios');
        $this->db->where('Usuario', $usu);
        $this->db->where('clienteId !=', 0);
        $query = $this->db->get();
        return $query->row();
    }

    function loginCliente($usu,$pass) {
        $strq = "SELECT usu.UsuarioID,cli.clienteId,cli.nombre, usu.perfilId, usu.contrasena
                FROM usuarios as usu 
                INNER JOIN clientes as cli on cli.clienteId = usu.clienteId
                where usu.Usuario ='$usu' and cli.activo = 1";
        $count = 0;
        $passwo =0;
        $query = $this->db->query($strq);
        $this->db->close();
        foreach ($query->result() as $row) {
            $passwo =$row->contrasena;
            $id = $row->UsuarioID;
            $nom =$row->nombre;
            $name =$row->nombre;
            $perfil = $row->perfilId; 
            $idpersonal = $row->clienteId; 
            $verificar = password_verify($pass,$passwo);
            if ($verificar) {
                $data = array(
                        'logeado' => true,
                        'usuarioid' => $id,
                        'usuario' => $nom,
                        'usuarioname' => $name,
                        'perfilid'=>$perfil,
                        'idpersonal'=>$idpersonal,
                        'sucursal'=>0
                    );
                $this->session->set_userdata($data);
                
                $count=1;
                //$count=$passwo.'/'.$id.'/'.$nom.'/'.$perfil.'/'.$idpersonal;
            }
            /*
                 si es cero aqui se agregara los mismos parametros pero para el aseso de los residentes si es que lo piden
            */
        } 
        /*
        if ($count==0) {
            $residentes = $this->db->query("CALL SP_GET_SESSIONRESIDENTES('$usu','$pass')");
            $this->db->close();

            foreach ($residentes->result() as $row) {

                $id = $row->AspiranteId;
                $nom =$row->Nombre;
            
            
                $_SESSION['usuarioid']=$id;
                $_SESSION['usuario']=$nom;
                $_SESSION['perfilid']=3;
                $_SESSION['idpersonal']=0;
                $count=1;
            
           
            }
        }
        */
        
        echo $count;
        //echo $strq;
        //echo json_encode($verificar );
    }

}
