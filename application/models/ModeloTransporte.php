<?php
$a = session_id();
if (empty($a)) session_start();
defined('BASEPATH') or exit('No direct script access allowed');

class ModeloTransporte extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
	}

	function Insert($Tabla, $data)
	{
		$this->db->insert($Tabla, $data);
		$id = $this->db->insert_id();
		return $id;
	}

	function getselect_where($table, $where)
	{
		$this->db->select('*');
		$this->db->from($table);
		$this->db->where($where);
		$query = $this->db->get();
		return $query;
	}

	function getselect_product()
	{
		$strq = "SELECT *
                    FROM producto AS p ON c.id_producto = p.productoId
                    where c.status = 1";
		$query = $this->db->query($strq);
		return $query->result();
	}

	function updateCatalogo($Tabla, $data, $where)
	{
		$this->db->set($data);
		$this->db->where($where);
		$this->db->update($Tabla);
		//return $id;
	}
}
