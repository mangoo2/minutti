<?php

defined('BASEPATH') OR exit ('No direct script access allowed');

class ModeloTraspasos extends CI_Model {
    public function __construct() {
        parent::__construct();
    }
    function getlist_row($params){
        $columns = array( 
            0=>'tra.traspasoId',
            1=>'tra.personalId',
            2=>'suco.sucursal sucursalo',
            3=>'sucd.sucursal sucursald',
            4=>'tra.fecha_solicitud',
            5=>'tra.personal',
            6=>'tra.activo'
        );
        $columns_s = array( 
            0=>'tra.traspasoId',
            1=>'tra.personalId',
            2=>'suco.sucursal',
            3=>'sucd.sucursal',
            4=>'tra.fecha_solicitud',
            5=>'tra.personal',
            6=>'tra.activo'
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('traspasos tra');
        //$this->db->join('personal per', 'per.personalId = tra.personalId');
        $this->db->join('sucursal suco', 'suco.sucursalId = tra.sucursal_origen');
        $this->db->join('sucursal sucd', 'sucd.sucursalId = tra.sucursal_destino');

        
        //$this->db->where(array('tra.activo'=>1));
        //$this->db->where(array('facturaabierta'=>1));
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns_s as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        $this->db->order_by($columns_s[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        //echo $this->db->get_compiled_select();
        $query=$this->db->get();
        // print_r($query); die;
        return $query;
    }
    public function getlist_row_t($params){
        $columns = array( 
            0=>'tra.traspasoId',
            1=>'tra.personalId',
            2=>'suco.sucursal',
            3=>'sucd.sucursal',
            4=>'tra.fecha_solicitud',
            5=>'tra.personal',
            7=>'tra.activo'
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select('COUNT(*) as total');
        $this->db->from('traspasos tra');
        //$this->db->join('personal per', 'per.personalId = tra.personalId');
        $this->db->join('sucursal suco', 'suco.sucursalId = tra.sucursal_origen');
        $this->db->join('sucursal sucd', 'sucd.sucursalId = tra.sucursal_destino');
        
        //$this->db->where(array('tra.activo'=>1));
        //$where = ;
        //$this->db->where(array('facturaabierta'=>1));
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
   
        $query=$this->db->get();

        return $query->row()->total;
    }

    function getTraspasoPdf($id){
        $this->db->select('t.personal, t.fecha_solicitud, t.reg ');
        $this->db->from("traspasos t");
        //$this->db->join("traspasos_d td","td.traspasoId=t.traspasoId");
        $this->db->where("t.traspasoId",$id);
        //$this->db->where("td.activo",1);
        $query=$this->db->get(); 
        return $query->result();
    }
    function getTraspasoPdfProd($id){
        $this->db->select('p.codigo, p.nombre, td.cantidad, so.sucursal as suc_org, sd.sucursal as suc_dest ');
        $this->db->from("traspasos t");
        $this->db->join("traspasos_d td","td.traspasoId=t.traspasoId");
        $this->db->join("producto p","p.productoId=td.productoId");
        $this->db->join("sucursal so","so.sucursalId=td.sucursal_origen");
        $this->db->join("sucursal sd","sd.sucursalId=td.sucursal_destino");

        $this->db->where("t.traspasoId",$id);
        $this->db->where("td.activo",1);
        $query=$this->db->get(); 
        return $query->result();
    }
    function getTraspasoPdfProd2($id){
        $this->db->select('p.codigo, p.nombre, p.descripcion, td.cantidad, so.sucursal as suc_org, sd.sucursal as suc_dest ');
        $this->db->from("traspasos t");
        $this->db->join("traspasos_d td","td.traspasoId=t.traspasoId");
        $this->db->join("producto p","p.productoId=td.productoId");
        $this->db->join("sucursal so","so.sucursalId=td.sucursal_origen");
        $this->db->join("sucursal sd","sd.sucursalId=td.sucursal_destino");

        $this->db->where("t.traspasoId",$id);
        $this->db->where("td.activo",1);
        $query=$this->db->get(); 
        return $query;
    }
    /*
    function getTraspasoPdfProd2($id){
        $strq = "SELECT `p`.`codigo`, `p`.`nombre`, `p`.`descripcion`, `td`.`cantidad`, `so`.`sucursal` as `suc_org`, `sd`.`sucursal` as `suc_dest`
            FROM `traspasos` `t`
            JOIN `traspasos_d` `td` ON `td`.`traspasoId`=`t`.`traspasoId`
            JOIN `producto` `p` ON `p`.`productoId`=`td`.`productoId`
            JOIN `sucursal` `so` ON `so`.`sucursalId`=`td`.`sucursal_origen`
            JOIN `sucursal` `sd` ON `sd`.`sucursalId`=`td`.`sucursal_destino`
            WHERE `t`.`traspasoId` = $id
            AND `td`.`activo` = 1 "; 
        $query = $this->db->query($strq);
        return $query;
    }
    */ 

}