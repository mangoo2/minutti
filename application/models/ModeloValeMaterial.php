<?php

defined('BASEPATH') OR exit ('No direct script access allowed');

class ModeloValeMaterial extends CI_Model {
    public function __construct() {
        parent::__construct();
    }
    function getlist_row($params){
        $columns = array( 
            0=>'vm.valeId',
            1=>'vm.personal_solicitante',
            2=>'suc.sucursal',
            3=>'vm.fecha_solicitud',
            4=>'per.nombre',
            5=>'per.apellido_paterno',
            6=>'per.apellido_materno',
            7=>'vm.activo',
            8=>'vm.solicita',
        );      
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('vale_material vm');
        $this->db->join('personal per', 'per.personalId = vm.personal_solicitante','left');
        $this->db->join('sucursal suc', 'suc.sucursalId = vm.destino','left');

        
        $this->db->where(array('vm.activo'=>1));
        //$this->db->where(array('facturaabierta'=>1));
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        $this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        //echo $this->db->get_compiled_select();
        $query=$this->db->get();
        // print_r($query); die;
        return $query;
    }
    public function getlist_row_t($params){
        $columns = array( 
            0=>'vm.valeId',
            1=>'vm.personal_solicitante',
            2=>'suc.sucursal',
            3=>'vm.fecha_solicitud',
            4=>'per.nombre',
            5=>'per.apellido_paterno',
            6=>'per.apellido_materno',
            7=>'vm.activo',
            8=>'vm.solicita',
        );      
        
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select('COUNT(*) as total');
        $this->db->from('vale_material vm');
        $this->db->join('personal per', 'per.personalId = vm.personal_solicitante','left');
        $this->db->join('sucursal suc', 'suc.sucursalId = vm.destino','left');

        
        $this->db->where(array('vm.activo'=>1));
        //$where = ;
        //$this->db->where(array('facturaabierta'=>1));
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
   
        $query=$this->db->get();

        return $query->row()->total;
    }

    function personalall_sucursal_search($usu){
        $strq = "SELECT p.*,s.sucursal FROM personal AS p
                LEFT JOIN sucursal as s on s.sucursalId=p.sucursalId 
                where p.estatus=1 and p.nombre like '%$usu%' or p.apellido_paterno like '%$usu%' or p.apellido_materno like '%$usu%'  ORDER BY p.nombre ASC";
        $query = $this->db->query($strq);

        return $query;
    }

    function salida_prodcuto_vale($id){
        $strq="SELECT vm.cantidad,pro.nombre as producto
                FROM vale_material_d as vm 
                INNER JOIN producto as pro on pro.productoId=vm.productoId 
                WHERE vm.activo=1 AND vm.valeId=$id";
        $query = $this->db->query($strq);
        return $query;
    }

    function getlist_surtir_consumible($params){
        $columns = array( 
            0=>'sc.Id',
            1=>'sc.solicita',
            2=>'suc.sucursal',
            3=>'DATE_FORMAT(sc.fecha_solicitud, "%d / %m / %Y" ) AS fecha_solicitud',
            4=>'per.nombre',
            5=>'per.apellido_paterno',
            6=>'per.apellido_materno',
            7=>'sc.activo'
        );  
        $columns2 = array( 
            0=>'sc.Id',
            1=>'suc.sucursal',
            2=>'sc.fecha_solicitud',
            3=>'per.nombre',
            4=>'per.apellido_paterno',
            5=>'per.apellido_materno',
            6=>'sc.activo'
        );      
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('surtir_consumibles sc');
        $this->db->join('personal per', 'per.personalId = sc.personalId','left');
        $this->db->join('sucursal suc', 'suc.sucursalId = sc.area','left');
        $this->db->where(array('sc.activo'=>1));
        //$this->db->where(array('facturaabierta'=>1));
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns2 as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        $this->db->order_by($columns2[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        //echo $this->db->get_compiled_select();
        $query=$this->db->get();
        // print_r($query); die;
        return $query;
    }
    public function getlist_surtir_consumible_t($params){
        $columns = array( 
            0=>'sc.Id',
            1=>'suc.sucursal',
            2=>'sc.fecha_solicitud',
            3=>'per.nombre',
            4=>'per.apellido_paterno',
            5=>'per.apellido_materno',
            6=>'sc.activo',
            7=>'sc.solicita',
        );      
        
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select('COUNT(*) as total');
        $this->db->from('surtir_consumibles sc');
        $this->db->join('personal per', 'per.personalId = sc.personalId','left');
        $this->db->join('sucursal suc', 'suc.sucursalId = sc.area','left');

        
        $this->db->where(array('sc.activo'=>1));
        //$where = ;
        //$this->db->where(array('facturaabierta'=>1));
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
   
        $query=$this->db->get();

        return $query->row()->total;
    }
    
    function get_producto_surtir_consumible($id){
        $strq="SELECT sc.Iddconsumible,sc.Idconsumible,sc.cantidad,pro.codigo,sc.producto,sc.motivo,sc.motivo_otro,pro.nombre AS productotext
                FROM surtir_consumibles_detalle AS sc
                INNER JOIN producto AS pro ON pro.productoId=sc.producto
                WHERE sc.activo=1 AND sc.Idconsumible=$id";
        $query = $this->db->query($strq);
        return $query->result();
    }

    function get_surtir_consumible($id){
        $strq="SELECT vm.cantidad,pro.nombre as producto
                FROM surtir_consumibles_detalle as vm 
                INNER JOIN producto as pro on pro.productoId=vm.producto 
                WHERE vm.activo=1 AND vm.Idconsumible=$id";
        $query = $this->db->query($strq);
        return $query;
    }
    
    function getlist_surtir_papeleria($params){
        $columns = array( 
            0=>'sc.Id',
            1=>'CONCAT(per.nombre," ",per.apellido_paterno," ",per.apellido_materno) AS solita',
            2=>'suc.sucursal',
            3=>'DATE_FORMAT(sc.fecha_solicitud, "%d / %m / %Y" ) AS fecha_solicitud',
            4=>'per.nombre',
            5=>'per.apellido_paterno',
            6=>'per.apellido_materno',
            7=>'sc.activo',
            8=>'sc.solicita',
        );  
        $columns2 = array( 
            0=>'sc.Id',
            1=>'suc.sucursal',
            2=>'sc.fecha_solicitud',
            3=>'per.nombre',
            4=>'per.apellido_paterno',
            5=>'per.apellido_materno',
            6=>'sc.activo',
            7=>'sc.solicita',
        );      
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('surtir_papeleria sc');
        $this->db->join('personal per', 'per.personalId = sc.personalId','left');
        $this->db->join('sucursal suc', 'suc.sucursalId = sc.area','left');
        $this->db->where(array('sc.activo'=>1));
        //$this->db->where(array('facturaabierta'=>1));
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns2 as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        $this->db->order_by($columns2[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        //echo $this->db->get_compiled_select();
        $query=$this->db->get();
        // print_r($query); die;
        return $query;
    }

    public function getlist_surtir_papeleria_t($params){
        $columns = array( 
            0=>'sc.Id',
            1=>'suc.sucursal',
            2=>'sc.fecha_solicitud',
            3=>'per.nombre',
            4=>'per.apellido_paterno',
            5=>'per.apellido_materno',
            6=>'sc.activo',
            7=>'sc.solicita',
        );      
        
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select('COUNT(*) as total');
        $this->db->from('surtir_papeleria sc');
        $this->db->join('personal per', 'per.personalId = sc.personalId','left');
        $this->db->join('sucursal suc', 'suc.sucursalId = sc.area','left');
        
        $this->db->where(array('sc.activo'=>1));
        //$where = ;
        //$this->db->where(array('facturaabierta'=>1));
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
   
        $query=$this->db->get();

        return $query->row()->total;
    }
    
    function get_producto_surtir_papeleria($id){
        $strq="SELECT sc.Iddpapeleria,sc.Idpapeleria,sc.cantidad,pro.codigo,sc.producto,sc.motivo,sc.motivo_otro,pro.nombre AS productotext
                FROM surtir_papeleria_detalle AS sc
                INNER JOIN producto AS pro ON pro.productoId=sc.producto
                WHERE sc.activo=1 AND sc.Idpapeleria=$id";
        $query = $this->db->query($strq);
        return $query->result();
    }
    
    function get_surtir_papeleria($id){
        $strq="SELECT vm.cantidad,pro.nombre as producto
                FROM surtir_papeleria_detalle as vm 
                INNER JOIN producto as pro on pro.productoId=vm.producto 
                WHERE vm.activo=1 AND vm.Idpapeleria=$id";
        $query = $this->db->query($strq);
        return $query;
    }

    /// surtir otro
    function getlist_surtir_otro($params){
        $columns = array( 
            0=>'sc.Id',
            1=>'CONCAT(per.nombre," ",per.apellido_paterno," ",per.apellido_materno) AS solita',
            2=>'suc.sucursal',
            3=>'DATE_FORMAT(sc.fecha_solicitud, "%d / %m / %Y" ) AS fecha_solicitud',
            4=>'per.nombre',
            5=>'per.apellido_paterno',
            6=>'per.apellido_materno',
            7=>'sc.activo',
            8=>'sc.solicita'
        );  
        $columns2 = array( 
            0=>'sc.Id',
            1=>'suc.sucursal',
            2=>'sc.fecha_solicitud',
            3=>'per.nombre',
            4=>'per.apellido_paterno',
            5=>'per.apellido_materno',
            6=>'sc.activo',
            7=>'sc.solicita'
        );      
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('surtir_otros sc');
        $this->db->join('personal per', 'per.personalId = sc.personalId','left');
        $this->db->join('sucursal suc', 'suc.sucursalId = sc.area','left');
        $this->db->where(array('sc.activo'=>1));
        //$this->db->where(array('facturaabierta'=>1));
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns2 as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        $this->db->order_by($columns2[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        //echo $this->db->get_compiled_select();
        $query=$this->db->get();
        // print_r($query); die;
        return $query;
    }

    public function getlist_surtir_otro_t($params){
        $columns = array( 
            0=>'sc.Id',
            1=>'suc.sucursal',
            2=>'sc.fecha_solicitud',
            3=>'per.nombre',
            4=>'per.apellido_paterno',
            5=>'per.apellido_materno',
            6=>'sc.activo',
            7=>'sc.solicita'
        );      
        
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select('COUNT(*) as total');
        $this->db->from('surtir_otros sc');
        $this->db->join('personal per', 'per.personalId = sc.personalId','left');
        $this->db->join('sucursal suc', 'suc.sucursalId = sc.area','left');

        
        $this->db->where(array('sc.activo'=>1));
        //$where = ;
        //$this->db->where(array('facturaabierta'=>1));
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
   
        $query=$this->db->get();

        return $query->row()->total;
    }

    function get_producto_surtir_otro($id){
        $strq="SELECT sc.sotrosdId,sc.sotrosId,sc.cantidad,pro.codigo,sc.producto,sc.motivo,sc.motivo_otro,pro.nombre AS productotext
                FROM surtir_otros_d AS sc
                INNER JOIN producto AS pro ON pro.productoId=sc.producto
                WHERE sc.activo=1 AND sc.sotrosId=$id";
        $query = $this->db->query($strq);
        return $query->result();
    }

    function get_surtir_otro($id){
        $strq="SELECT vm.cantidad,pro.nombre as producto
                FROM surtir_otros_d as vm 
                INNER JOIN producto as pro on pro.productoId=vm.producto 
                WHERE vm.activo=1 AND vm.sotrosId=$id";
        $query = $this->db->query($strq);
        return $query;
    }

    /// surtir herramienta
    function getlist_surtir_herramienta($params){
        $columns = array( 
            0=>'sc.Id',
            1=>'CONCAT(per.nombre," ",per.apellido_paterno," ",per.apellido_materno) AS solita',
            2=>'suc.sucursal',
            3=>'DATE_FORMAT(sc.fecha_solicitud, "%d / %m / %Y" ) AS fecha_solicitud',
            4=>'per.nombre',
            5=>'per.apellido_paterno',
            6=>'per.apellido_materno',
            7=>'sc.activo',
            8=>'sc.solicita',
        );  
        $columns2 = array( 
            0=>'sc.Id',
            1=>'suc.sucursal',
            2=>'sc.fecha_solicitud',
            3=>'per.nombre',
            4=>'per.apellido_paterno',
            5=>'per.apellido_materno',
            6=>'sc.activo',
            7=>'sc.solicita',
        );      
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('surtir_herramienta sc');
        $this->db->join('personal per', 'per.personalId = sc.personalId','left');
        $this->db->join('sucursal suc', 'suc.sucursalId = sc.area','left');
        $this->db->where(array('sc.activo'=>1));
        //$this->db->where(array('facturaabierta'=>1));
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns2 as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        $this->db->order_by($columns2[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        //echo $this->db->get_compiled_select();
        $query=$this->db->get();
        // print_r($query); die;
        return $query;
    }

    public function getlist_surtir_herramienta_t($params){
        $columns = array( 
            0=>'sc.Id',
            1=>'suc.sucursal',
            2=>'sc.fecha_solicitud',
            3=>'per.nombre',
            4=>'per.apellido_paterno',
            5=>'per.apellido_materno',
            6=>'sc.activo',
            7=>'sc.solicita',
        );      
        
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select('COUNT(*) as total');
        $this->db->from('surtir_herramienta sc');
        $this->db->join('personal per', 'per.personalId = sc.personalId','left');
        $this->db->join('sucursal suc', 'suc.sucursalId = sc.area','left');
        $this->db->where(array('sc.activo'=>1));
        //$where = ;
        //$this->db->where(array('facturaabierta'=>1));
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
   
        $query=$this->db->get();

        return $query->row()->total;
    }

    function get_producto_surtir_herramienta($id){
        $strq="SELECT sc.Iddherramienta,sc.Idherramienta,sc.cantidad,pro.codigo,sc.producto,sc.motivo,sc.motivo_otro,pro.nombre AS productotext
                FROM surtir_herramienta_detalle AS sc
                INNER JOIN producto AS pro ON pro.productoId=sc.producto
                WHERE sc.activo=1 AND sc.Idherramienta=$id";
        $query = $this->db->query($strq);
        return $query->result();
    }

    function get_surtir_herramienta($id){
        $strq="SELECT vm.cantidad,pro.nombre as producto
                FROM surtir_herramienta_detalle as vm 
                INNER JOIN producto as pro on pro.productoId=vm.producto 
                WHERE vm.activo=1 AND vm.Idherramienta=$id";
        $query = $this->db->query($strq);
        return $query;
    }

}