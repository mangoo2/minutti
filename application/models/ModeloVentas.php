<?php

defined('BASEPATH') OR exit ('No direct script access allowed');

class ModeloVentas extends CI_Model {
    public function __construct() {
        parent::__construct();
    }

    function clientepordefecto(){
        $strq = "SELECT * FROM  clientes LIMIT 1";
        $query = $this->db->query($strq);
        $this->db->close();
        return $query;
    }

    public function get_producto($buscar){
        $strq="SELECT p.productoId,p.nombre,p.codigo,p.precio_venta,1 As tipo
                FROM producto as p
                WHERE p.activo = 1 AND p.nombre like '%$buscar%' OR p.activo = 1 AND p.codigo like '%$buscar%'
                UNION
                SELECT s.id AS productoId,s.servico AS nombre,s.codigo,s.costo_servicio AS precio_venta,2 As tipo
                FROM servicios as s
                WHERE s.activo = 1 AND s.servico like '%$buscar%' OR s.activo = 1 AND s.codigo like '%$buscar%'"; 
        $query = $this->db->query($strq);
        return $query->result();
    }

    public function get_producto_x($productoid,$tipo){
        if($tipo==1){
            $strq = "SELECT p.productoId,p.nombre,p.codigo,p.precio_venta
                FROM producto as p
                WHERE p.productoid=$productoid";
        }else{
            $strq = "SELECT s.id AS productoId,s.servico AS nombre,s.codigo,s.costo_servicio AS precio_venta,2 As tipo
                FROM servicios as s
                WHERE s.id=$productoid";
        }        
        $query = $this->db->query($strq);
        return $query;
    }

    function ingresarventa($uss,$cli,$mpago,$sbtotal,$desc,$descu,$total,$efectivo,$cambio,$factura){
        $strq = "INSERT INTO ventas(id_personal, id_cliente, metodo, subtotal, descuento,descuentocant, monto_total, efectivo, cambio,factura) 
                VALUES ($uss,$cli,$mpago,$sbtotal,$desc,$descu,$total,$efectivo,$cambio,$factura)";
        $query = $this->db->query($strq);
        $id=$this->db->insert_id();
        $this->db->close();
        return $id;
    }
    function ingresarventad($idventa,$producto,$cantidad,$precio,$tipo){
        if($tipo==1){
            $strq = "INSERT INTO venta_detalle(id_venta,id_producto, cantidad, precio) VALUES ($idventa,$producto,$cantidad,$precio)";
        }else{
            $strq = "INSERT INTO venta_detalle(id_venta,id_servicio, cantidad, precio) VALUES ($idventa,$producto,$cantidad,$precio)";
        }
        $query = $this->db->query($strq);
        $this->db->close();
    }

    function stock_descontar_producto($cantidad,$id){
        $strq="UPDATE producto SET stock_disponible = stock_disponible-$cantidad WHERE productoId = $id";
        $this->db->query($strq);
    }
    
    function getventas($id){
        $strq = "SELECT * FROM ventas where id_venta=$id";
        $query = $this->db->query($strq);
        $this->db->close();
        return $query;
    }

    function get_ventas($params){
        $columns = array( 
            0=>'ven.id_venta', 
            1=>'DATE_FORMAT(ven.reg,  "%d / %m / %Y %r") AS reg',
            2=>'cli.nombre as cliente', 
            3=>'concat(per.nombre," ",per.apellido_paterno) as vendedor',
            4=>'ven.metodo',
            5=>'ven.subtotal', 
            6=>'ven.monto_total',
            7=>'ven.cancelado',
            8=>'ven.tipo_venta',
            9=>'ven.descuentocant',
            10=>'ven.factura',
            11=>'ven.pagado_credito',
            12=>'ven.fecha_servicio',
            13=>'ven.id_cliente',
            14=>'(select COUNT(*) from venta_detalle where id_servicio!=0 AND activo=1 AND id_venta = ven.id_venta) as serviciott'
            
        );
        $columns2 = array( 
            0=>'ven.id_venta',
            1=>'ven.reg', 
            2=>'cli.Nom', 
            3=>'per.nombre',
            3=>'per.apellidos',
            4=>'ven.metodo',
            5=>'ven.subtotal', 
            6=>'ven.monto_total',
            7=>'ven.cancelado',
            8=>'ven.descuentocant'
        );    
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('ventas ven');
        $this->db->join('personal per','per.personalId=ven.id_personal'); 
        $this->db->join('clientes cli','cli.clienteId=ven.id_cliente'); 

        if($params['tipo_venta']!=0){
            $wheret = array('ven.metodo'=>$params['tipo_venta']);
            $this->db->where($wheret);
        }
        if($params['tipo_factura']==1){
            $wheref = array('ven.factura'=>1);
            $this->db->where($wheref);
        }else if($params['tipo_factura']==2){
            $wheref = array('ven.factura'=>0);
            $this->db->where($wheref);
        }
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns2 as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        $this->db->order_by($columns2[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        $query=$this->db->get();
        return $query;
    }
    function total_ventas($params){
        $columns = array( 
            0=>'ven.id_venta',
            1=>'ven.reg', 
            2=>'cli.nombre', 
            3=>'per.nombre',
            3=>'per.apellido_paterno',
            4=>'ven.metodo',
            5=>'ven.subtotal', 
            6=>'ven.monto_total',
            7=>'ven.cancelado'
        );
        $this->db->select('COUNT(1) as total');
        $this->db->from('ventas ven');
        $this->db->join('personal per','per.personalId=ven.id_personal'); 
        $this->db->join('clientes cli','cli.clienteId=ven.id_cliente'); 

        if($params['tipo_venta']!=0){
            $wheret = array('ven.metodo'=>$params['tipo_venta']);
            $this->db->where($wheret);
        }
        if($params['tipo_factura']==1){
            $wheref = array('ven.factura'=>1);
            $this->db->where($wheref);
        }else if($params['tipo_factura']==2){
            $wheref = array('ven.factura'=>0);
            $this->db->where($wheref);
        }
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }  
        $query=$this->db->get();
        return $query->row()->total;
    }

    function cancalarventa($id){
        $fecha = date('Y-m-d');
        $strq = "UPDATE ventas SET cancelado=1,hcancelacion='$fecha' WHERE `id_venta`=$id";
        $query = $this->db->query($strq);
        $this->db->close();
    }

    function ventadetalles($id){
        $strq = "SELECT * FROM venta_detalle where id_venta=$id";
        $query = $this->db->query($strq);
        $this->db->close();
        return $query;
    }

    function stock_aumentar_producto($cantidad,$id){
        $strq="UPDATE producto SET stock_disponible = stock_disponible+$cantidad WHERE productoid=$id";
        $this->db->query($strq);
    }

    function clientesallsearch($usu){
        $strq = "SELECT * FROM clientes where activo=1 and nombre like '%".$usu."%' ORDER BY nombre ASC";
        $query = $this->db->query($strq);
        $this->db->close();
        return $query;
    }

    function corte($inicio,$fin,$tipo_venta,$pro_ven){
        if($tipo_venta==0){
            $whereven=" ";
        }else{
            $whereven=" v.metodo=".$tipo_venta."  AND ";
        }
        $strq = "SELECT cl.clienteId,cl.nombre, v.reg, v.id_venta, v.id_cliente, v.id_personal, v.monto_total,v.efectivo, v.subtotal, p.personalId, concat(p.nombre,p.apellido_paterno,p.apellido_materno) AS vendedor,v.descuentocant,v.metodo,ps.precio_venta,ps. nombre AS producto,dt.precio,ps.costo_compra,ca.categoria
                FROM ventas AS v 
                INNER JOIN venta_detalle AS dt ON dt.id_venta=v.id_venta
                INNER JOIN producto AS ps ON ps.productoId=dt.id_producto
                LEFT JOIN categoria AS ca ON ca.categoriaId=ps.categoriaId 
                LEFT JOIN personal AS p ON v.id_personal=p.personalId 
                INNER JOIN clientes AS cl ON cl.clienteId=v.id_cliente
                WHERE $whereven dt.activo=1 AND v.activo=1 AND v.reg BETWEEN '$inicio 00:00:00' AND '$fin 23:59:59' AND v.cancelado=0
                UNION
                SELECT cl.clienteId,cl.nombre, v.reg, v.id_venta, v.id_cliente, v.id_personal, v.monto_total,v.efectivo, v.subtotal, p.personalId, concat(p.nombre,p.apellido_paterno,p.apellido_materno) AS vendedor,v.descuentocant,v.metodo,ps.costo_servicio AS precio_venta,ps.servico AS producto,dt.precio,ps.servicio_cuesta AS costo_compra,'' AS categoria
                FROM ventas AS v 
                INNER JOIN venta_detalle AS dt ON dt.id_venta=v.id_venta
                INNER JOIN servicios AS ps ON ps.id=dt.id_servicio 
                LEFT JOIN personal AS p ON v.id_personal=p.personalId 
                INNER JOIN clientes AS cl ON cl.clienteId=v.id_cliente
                WHERE $whereven dt.activo=1 AND v.activo=1 AND v.reg BETWEEN '$inicio 00:00:00' AND '$fin 23:59:59' AND v.cancelado=0";
        $query = $this->db->query($strq);
        $this->db->close();
        return $query;
    }

    function corte_gastos($inicio,$fin){
        $strq = "SELECT gp.id,gp.nombre,gp.monto,gp.fecha,gp.tipocobro,c.nombre AS categoria
                FROM gastospagos AS gp
                INNER JOIN categoria_cobro AS c ON c.id=gp.categoria
                WHERE gp.activo=1 aND gp.fecha BETWEEN '$inicio' AND '$fin'";
        $query = $this->db->query($strq);
        $this->db->close();
        return $query;
    }

    function cortesum($inicio,$fin,$tipo_venta,$pro_ven){
        if($tipo_venta==0){
            $whereven=" ";
        }else{
            $whereven=" metodo=".$tipo_venta."  AND ";
        }
        $strq = "SELECT sum(monto_total) as total , sum(subtotal) as subtotal, sum(descuentocant) as descuento 
                FROM ventas 
                where $whereven activo=1 AND reg between '$inicio 00:00:00' and '$fin 23:59:59' and cancelado=0";
        $query = $this->db->query($strq);
        $this->db->close();
        return $query;
    }

    function consultartotal_venta2($id_venta){
        $strq = "SELECT ps.costo_compra*vd.cantidad AS preciocompra 
                FROM venta_detalle AS vd
                INNER JOIN producto AS ps ON ps.productoId=vd.id_producto
                INNER JOIN ventas AS v ON v.id_venta=vd.id_venta
                WHERE vd.activo AND v.id_venta=$id_venta GROUP BY vd.id_detalle_venta";
        $query = $this->db->query($strq);
        $this->db->close();
        $total_aux=0;
        foreach ($query->result() as $row) {
            $total_aux = $total_aux+$row->preciocompra;
        }
        return $total_aux;
    }

    function pro_venta($inicio,$fin){
        $strq = "SELECT v.id_venta, p.nombre as producto, sum(vd.cantidad) AS total_cantidad  FROM ventas AS v 
                INNER JOIN venta_detalle AS vd ON vd.id_venta=v.id_venta
                INNER JOIN producto AS p ON p.productoId=vd.id_producto
                WHERE vd.activo=1 AND v.activo=1 AND v.reg BETWEEN '$inicio 00:00:00' AND '$fin 23:59:59' AND v.cancelado=0 GROUP BY vd.id_producto ORDER BY sum(vd.cantidad) DESC LIMIT 5";
        $query = $this->db->query($strq);
        return $query;
    }

    function get_gastos($inicio,$fin){
        $strq = "SELECT SUM(monto) AS gasto
                FROM gastospagos
                WHERE fecha BETWEEN '$inicio' AND '$fin' AND activo=1";
        $query = $this->db->query($strq);
        $this->db->close();
        return $query;
    }

    /////////////////////Corte a credito////////////////////////
    function corte_credito($inicio,$fin,$tipo_venta,$pro_ven){
        if($tipo_venta==0){
            $whereven=" ";
        }else{
            $whereven=" v.metodo=".$tipo_venta."  AND ";
        }
        $strq = "SELECT cl.clienteId,cl.nombre, v.reg, v.id_venta, v.id_cliente, v.id_personal, v.monto_total,v.efectivo, v.subtotal, p.personalId, concat(p.nombre,p.apellidos) AS vendedor,v.descuentocant,v.metodo
                FROM ventas AS v 
                LEFT JOIN personal AS p ON v.id_personal=p.personalId 
                INNER JOIN clientes AS cl ON cl.clienteId=v.id_cliente
                WHERE $whereven v.activo=1 AND v.metodo=4 AND v.reg BETWEEN '$inicio 00:00:00' AND '$fin 23:59:59' AND v.cancelado=0";
        $query = $this->db->query($strq);
        $this->db->close();
        return $query;
    }

    function cortesum_credito($inicio,$fin,$tipo_venta,$pro_ven){
        if($tipo_venta==0){
            $whereven=" ";
        }else{
            $whereven=" metodo=".$tipo_venta."  AND ";
        }
        $strq = "SELECT sum(monto_total) as total , sum(subtotal) as subtotal, sum(descuentocant) as descuento 
                FROM ventas 
                where $whereven activo=1 AND metodo=4 AND reg between '$inicio 00:00:00' and '$fin 23:59:59' and cancelado=0";
        $query = $this->db->query($strq);
        $this->db->close();
        return $query;
    }

    //// Abono
    public function getall_total_abono($id){
        $sql = "SELECT SUM(cantidad) AS abono FROM venta_abono AS v 
                INNER JOIN personal AS p ON p.personalId = v.idpersona
                WHERE v.id_venta = $id AND v.activo=1";
        $query = $this->db->query($sql);
        $total_aux=0;
        foreach ($query->result() as $row) {
            $total_aux = $row->abono;
        }
        return $total_aux;
    }

}