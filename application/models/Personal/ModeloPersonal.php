<?php
$a=session_id();
if(empty($a)) session_start();
defined('BASEPATH') OR exit ('No direct script access allowed');

class ModeloPersonal extends CI_Model {
    public function __construct() {
        parent::__construct();
    }
    
    
    function getpersonal() {
        $strq = "SELECT * FROM personal WHERE tipo=1 AND estatus=1";
        $query = $this->db->query($strq);
        return $query;
    }
    
    function personalview($id) {
        $strq = "SELECT *  FROM personal where personalId='$id'";
        $query = $this->db->query($strq);
        return $query;
    }

    public function personaldelete($id){
            $strq = "UPDATE personal SET estatus=0 where personalId=$id";
            $this->db->query($strq);
    }

    function notificarasignacion() {
        $strq = "SELECT * FROM config";
        $query = $this->db->query($strq);
        $this->db->close();
        $notf =0;
        foreach($query->result() as $valor){
            $notf=$valor->notfig_tutor;
        }
        return $notf;
    }

}
