<style type="text/css">
    .dataTables_wrapper{
        padding: 0px;
    }
    #table-lis td{
        font-size: 14px;
    }
</style>
<section class="basic-elements">
    <div class="row">
        <div class="col-sm-12">
            <div class="content-header">Personal</div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header" style="padding:10px;">
                    <h4 class="card-title mb-0"></h4>
                </div>
                <div class="card-body">
                    <div class="px-3">
                      <div class="row form-group">
                            <div class="col-md-3">
                                <a type="button" class="btn btn-raised btn-danger btn-min-width mr-1 mb-1 button_save" href="Personal/Personaladd"><i class="fa fa-save"></i> Agregar nuevo</a>
                            </div>
                      </div>
                        <div class="row form-group">
                            <div class="col-md-12">
                                <table class="table thead-inverse" id="data_tables" style="width: 100%">
                                    <thead>
                                        <tr>
                                            <th>NOMBRE</th>
                                            <th>USUARIO</th>
                                            <th>PERFIL</th>
                                            <th>PUESTO</th>  
                                            <th>ACCIONES</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                      <?php /* foreach ($personal->result() as $item){ ?>
                                         <tr id="trper_<?php echo $item->personalId; ?>">
                                                  <td><?php echo $item->nombre.' '.$item->apellido_paterno.' '.$item->apellido_materno; ?></td>
                                                  <td><?php echo $item->puesto; ?></td>
                                                  <td>
                                                    <a href="personal/Personaladd?id=<?php echo $item->personalId; ?>" class="btn btn-flat btn-danger"><i class="fa fa-edit icon_font"></i></a>
                                                    <a onclick="personaldelete(<?php echo $item->personalId; ?>)" class="btn btn-flat btn-danger"><i class="fa fa-trash-o icon_font"></i></a>
                                                    <!-- 
                                                    <div class="btn-group mr-1 mb-1">
                                                      <button type="button" class="btn btn-raised btn-icon btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="icon-settings"></i></button>
                                                      <div class="dropdown-menu" x-placement="bottom-start" style="position: absolute; transform: translate3d(0px, 38px, 0px); top: 0px; left: 0px; will-change: transform;">
                                                        <a class="dropdown-item" href="personal/Personaladd?id=<?php echo $item->personalId; ?>">Editar</a>
                                                        <a class="dropdown-item" href="#" onclick="personaldelete(<?php echo $item->personalId; ?>);">Eliminar</a>
                                        
                                                      </div>
                                                    --> 
                                                  </td>
                                             
                                                </tr>
                                          
                                        <?php } */ ?>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>