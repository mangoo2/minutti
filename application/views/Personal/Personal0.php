<div class="container">
	<div class="page-header">
		<h1><small>Personal</small></h1>
	</div>
	<div class="vd_content-section clearfix">
		<div class="row">
			<div class="col-md-12">
				<div class="panel widget">
					<div class="panel-heading vd_bg-grey">
						<h3 class="panel-title"> <span class="menu-icon"> <i class="fa fa-dot-circle-o"></i> </span> Personal </h3>
						<div class="vd_panel-menu">
								<div data-action="minimize" data-original-title="Minimize" data-toggle="tooltip" data-placement="bottom" class="menu entypo-icon">
									<i class="icon-minus3"></i>
								</div>
								<div class="tooltip fade bottom" role="tooltip" id="tooltip108722" style="display: block; top: 24px; left: -22.5px;">
									<div class="tooltip-arrow"></div><div class="tooltip-inner">Minimize</div>
								</div>
								<div data-action="close" data-original-title="Close" data-toggle="tooltip" data-placement="bottom" class=" menu entypo-icon">
									<i class="icon-cross"></i>
								</div>
						</div>
					</div>
					<div class="panel-body table-responsive">
						<a href="personal/Personaladd" data-toggle="tooltip" data-original-title="Nuevo evento"><button data-target='#nuevaagenda' data-toggle="modal"  data-placement="bottom" data-original-title="Venta" title="Venta" class="btn vd_btn vd_bg-blue" type="button"><i class="icon-user"></i> Agregar nuevo</button></a>

						<div class="panel widget light-widget">
	                 		<div class="panel-body">
	                    		<div class="col-md-12">
	                    			<table class="table table-striped" id="data-tables">
				                      <thead>
				                        <tr>
				                          <th>Nombre</th>
				                          <th>Puesto</th>
				                          <th></th>
				                        </tr>
				                      </thead>
				                      <tbody>
                                               
				                      	<?php  /* foreach ($personal->result() as $item){ ?>
                  						 <tr id="trper_<?php echo $item->personalId; ?>">
						                          <td><?php echo $item->personal; ?></td>
						                          <td>
						                          	<div class="btn-group">
													 <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
													  <i class="fa fa-cog"></i> 
													  <i class="fa fa-caret-down prepend-icon"></i> 
													 </button>
													    <ul class="dropdown-menu" role="menu">
													        <li><a href="personal/Personaladd?id=<?php echo $item->personalId; ?>"><i class="fa fa-pencil"></i> Editar</a></li>
													        <li><a href="#" onclick="personaldelete(<?php echo $item->personalId; ?>);"><i class="fa fa-times-circle-o"></i> Eliminar</a></li>
													    </ul>
													</div>
						                          </td>
						                     
						                        </tr>
				                          
                						<?php } */ 
                						?>
				                      	 	
				                      </tbody>
				                    </table>
	                    		</div>
	                    		
   							</div>
   						</div>	
   					</div>
   				</div>
   			</div>
   		</div>
   	</div>
	<script type="text/javascript">
		$(document).ready(function() {
				$('#data-tables').dataTable();
		} );
		
</script>
<script type="text/javascript" src="<?php echo base_url(); ?>public/js/personal/personal.js"></script> 