<?php 
$UsuarioID=0;
$perfilId=0;
$Usuario='';
$contrasena='';
if (isset($_GET['id'])) {
    $id=$_GET['id'];
    $personalv=$this->ModeloPersonal->personalview($id);
    foreach ($personalv->result() as $item){
      $id=$item->personalId;
      $nom=$item->nombre;
      $apellido_paterno=$item->apellido_paterno;
      $apellido_materno=$item->apellido_materno;
      $sucu=$item->sucursalId;
      $puesto=$item->puesto;
      $celular=$item->celular;
      $nss=$item->nss;
      $calle=$item->calle;
      $no_ext=$item->no_ext;
      $no_int=$item->no_int;
      $colonia=$item->colonia;
      $municipio=$item->municipio;
      $estado=$item->estado;
      $cp=$item->cp;
      $pais=$item->pais;
      $municipio = $item->municipio;
      $estado = $item->estado;
      $cp = $item->cp;
      $pais = $item->pais;
    }
    $resultusu=$this->ModeloCatalogos->getselectwheren('usuarios',array('personalId'=>$id));
    foreach ($resultusu->result() as $itemu) {
      $UsuarioID=$itemu->UsuarioID;
      $perfilId=$itemu->perfilId;
      $Usuario=$itemu->Usuario;
      $contrasena='xxxxxx';
    }
}else{
  $id='';
  $nom='';
  $apellido_paterno='';
  $apellido_materno='';
  $puesto='';
  $celular='';
  $nss='';
  $sucu='';     
  $calle='';
    $no_ext='';
    $no_int='';
    $colonia='';
    $municipio='';
    $estado='';
    $cp='';
    $pais='';

    $municipio='';
    $estado='';
    $cp='';
    $pais='';
} 
?>
<div class="row">
                <div class="col-md-12">
                  <h2>Personal</h2>
                </div>
              </div>
              <!--Statistics cards Ends-->
              <!--Line with Area Chart 1 Starts-->
              <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <br>
                        <div class="card-body">
                            <div class="card-block form-horizontal">
                                <!--------//////////////-------->
                                <form method="post" id="frmPersonal" role="form" action="../Personal">
                                  <input type="text" id="personalId" name="personalId" value="<?php echo $id; ?>" hidden>
                                  <!------------------------------------------------------>
                                  <div class="row form-group">
                                    <div class="col-md-2">
                                      <label class="control-label">Nombre</label>
                                    </div>
                                    <div class="col-md-10">
                                      <input type="text" id="nombre" name="nombre" value="<?php echo $nom; ?>" class="form-control">
                                    </div>
                                  </div>

                                  <div class="row form-group">
                                    <div class="col-md-2">
                                      <label class="control-label">Apellido Paterno</label>
                                    </div>
                                    <div class="col-md-4">
                                      <input type="text" id="apellido_paterno" name="apellido_paterno" value="<?php echo $apellido_paterno; ?>" class="form-control">
                                    </div>
                                    <div class="col-md-2">
                                      <label class="control-label">Apellido Materno</label>
                                    </div>
                                    <div class="col-md-4">
                                      <input type="text" id="apellido_materno" name="apellido_materno" value="<?php echo $apellido_materno; ?>" class="form-control">
                                    </div>
                                  </div>
                                  <div class="row form-group">
                                    <div class="col-md-2">
                                      <label class="control-label">Función / Puesto</label>
                                    </div>
                                    <div class="col-md-4">
                                      <input type="text" id="puesto" name="puesto" value="<?php echo $puesto; ?>" class="form-control">
                                    </div>

                                    <div class="col-md-2">
                                      <label class="control-label">Número celular</label>
                                    </div>
                                    <div class="col-md-4">
                                      <input type="text" maxlength="10" minlength="10" id="celular" name="celular" value="<?php echo $celular; ?>" class="form-control">
                                    </div>
                                    <?php /*
                                    <div class="col-md-2 sucursalIdclass" style="display:none;">
                                      <label class="control-label">Nave</label>
                                    </div>
                                    <div class="col-md-4 sucursalIdclass" style="display:none;">
                                      <select name="sucursalId" id="sucursalId" class="form-control">
                                                <?php foreach ($sucursalrow->result() as $item) { ?>
                                                  <option value="<?php echo $item->sucursalId;?>" <?php if($sucu==$item->sucursalId){ echo 'selected';}?> ><?php echo $item->sucursal;?></option>
                                                <?php } ?> 
                                                
                                              </select> 
                                      </div>
                                    
                                    */ ?>
                                    </div>
                                    
                                    <div class="row form-group">
                                      <div class="col-md-2">
                                        <label class="control-label">Seguro social</label>
                                      </div>
                                      <div class="col-md-4">
                                        <input type="text" id="nss" name="nss" value="<?php echo $nss; ?>" class="form-control">
                                      </div>
                                    </div>

                                    <div class="row form-group">
                                      <div class="col-md-12">
                                        <hr/>
                                      </div>
                                      <div class="col-md-12">
                                        Dirección
                                      </div>
                                    </div>
                                    
                                    <div class="row form-group">
                                      <div class="col-md-4">
                                        <label>Calle</label>
                                        <input type="text" name="calle" id="calle" class="form-control" value="<?php echo $calle; ?>">
                                      </div>
                                      <div class="col-md-2">
                                        <label>No. Ext</label>
                                        <input type="text" name="no_ext" id="no_ext" class="form-control" value="<?php echo $no_ext; ?>">
                                      </div>
                                      <div class="col-md-2">
                                        <label>No. Int</label>
                                        <input type="text" name="no_int" id="no_int" class="form-control" value="<?php echo $no_int; ?>">
                                      </div>
                                      <div class="col-md-4">
                                        <label>Colonia</label>
                                        <input type="text" name="colonia" id="colonia" class="form-control" value="<?php echo $colonia; ?>">
                                      </div>
                                    </div>


                                    <div class="row form-group">
                                      <div class="col-md-3">
                                        <label>Municipio o delegación</label>
                                        <input type="text" name="municipio" id="municipio" class="form-control" value="<?php echo $municipio; ?>">
                                      </div>
                                      <div class="col-md-3">
                                        <label>Estado</label>
                                        <select name="estado" id="estado" class="form-control">
                                          <?php foreach ($estadorow->result() as $item) { ?>
                                                  <option value="<?php echo $item->EstadoId;?>" <?php if($estado==$item->EstadoId){ echo 'selected';}?> ><?php echo $item->Nombre;?></option>
                                                <?php } ?> 
                                        </select>
                                      </div>
                                      <div class="col-md-3">
                                        <label>C.P</label>
                                        <input type="text" name="cp" id="cp" class="form-control" value="<?php echo $cp; ?>">
                                      </div>
                                      <div class="col-md-3">
                                        <label>País</label>
                                        <select name="pais" id="pais" class="form-control">
                                          <option value="MEX">MEX - México</option>
                                        </select>

                                      </div>
                                    </div>
                                </form>   
                                <div class="row">
                                  <div class="col-md-12">
                                    <hr/>
                                  </div>
                                  <div class="col-md-12">
                                    Permisos
                                  </div>
                                </div>
                                
                                <form method="post" id="frmusuario" role="form" style="margin-bottom: 10px;">
                                  <div class="row">
                                    <input type="hidden" name="ideu" id="ideu" value="<?php echo $UsuarioID;?>">
                                    <div class="col-md-3">
                                      <label>Usuario</label>
                                      <input type="text" name="usuario" id="usuario" class="form-control" value="<?php echo $Usuario;?>" pattern="[a-z]{1,15}"  required>
                                    </div>
                                    <div class="col-md-3">
                                      <label>Contraseña</label>
                                      <input type="password" name="contrasena" min id="contrasena" class="form-control" value="<?php echo $contrasena;?>" minlength="6" required>
                                    </div>
                                    <div class="col-md-3">
                                      <label>Perfil</label>
                                      <select name="perfiles" id="perfiles" class="form-control" required onchange="sucursalIdclass()">
                                        <?php foreach ($perfilesrow->result() as $itemp) { ?>
                                          <option value="<?php echo $itemp->perfilId;?>" <?php if($itemp->perfilId==$perfilId){ echo 'selected';}?>><?php echo $itemp->nombre;?></option>
                                        <?php } ?>
                                      </select>
                                    </div>
                                  </div>    
                                </form>
                                 
                                <button class="btn btn-raised btn-dark btn-min-width btn-lg mr-1 mb-1 btn_registro"  type="button" onclick="guardar()"><i class="fa fa-save"></i> Guardar</button>
                                  <!------------------------------------------------------>
                                
   					</div>
   				</div>
   			</div>
   		</div>
   	</div>
	