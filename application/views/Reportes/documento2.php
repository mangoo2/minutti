<?php

    require_once('TCPDF3/examples/tcpdf_include.php');
    require_once('TCPDF3/tcpdf.php');
    $this->load->helper('url');

//=======================================================================================
class MYPDF extends TCPDF {
    //Page header
    public function Header() {
    
        $img_file = base_url().'public/img/formatos/header.PNG'; 
        $this->Image($img_file, 0, 3, 210, 20, '', '', '', false, 330, '', false, false, 0); 

        $html = ''; 

        $this->writeHTML($html, true, false, true, false, ''); 
    }
    // Page footer
    public function Footer() {
        
        $img_file = base_url().'public/img/formatos/footer.PNG'; 
        $this->Image($img_file, 0, 250, 212, 45, '', '', '', false, 330, '', false, false, 0); 
        
        $html='';   
        $this->writeHTML($html, true, false, true, false, ''); 
    }
} 
$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Mapgroup');
$pdf->SetTitle('Solicitud de material');
$pdf->SetSubject('Solicitud de material');
$pdf->SetKeywords('Solicitud de material');

// set default header data
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins('10', '15', '10'); 
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER); 
//$pdf->SetFooterMargin(PDF_MARGIN_FOOTER); 
$pdf->SetFooterMargin('145'); 
// set auto page breaks
$pdf->SetAutoPageBreak(true, 50);
// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

$pdf->SetFont('dejavusans',13);
// add a page
$pdf->AddPage('P', 'A4');
  $html='
        <table width="100%" border="0" RULES="rows"> 
            <tr style="border:10px"> 
                <td width="0.8%">
                </td> 
                <td width="98.5%">
                    <div style="border-bottom-color: #bd1620; border-bottom: 5px"></div>
                </td> 
            </tr>
            <tr> 
                <td HEIGHT="30" width="68%" style="color:black; background-color: #f6f6f6;"> 
                    <b style="font-size: 11px;">
                    CLIENTE: '.$cliente.'
                    </b>
                    <span style="font-size: 20px; color:#f6f6f6;">l</span>
                </td> 
                <td width="2%"></td>
                <td  HEIGHT="30" width="30%" style="color:black; font-size: 11px; background-color: #f6f6f6;"> 
                    <b>  
                    NO.S: '.$serie.'
                    </b>
                    <span style="font-size: 20px; color:#f6f6f6;">l</span>
                </td>
            </tr>
            <small  HEIGHT="1" style="height: 1px; font-size: 7px;"><br></small>
            <tr> 
                <td  HEIGHT="30" width="100%" style="color:black; font-size: 11px; background-color: #f6f6f6;"> 
                    <b style="color:black; font-size: 11px; background-color: #f6f6f6;">
                    TIPO DE UNIDAD: '.$unidad.'
                    </b>
                    <span style="font-size: 20px; color:#f6f6f6;">l</span>
                </td> 
            </tr>
            <small  HEIGHT="1" style="height: 1px; font-size: 7px;"><br></small>
            <tr> 
                <td  HEIGHT="30" width="100%" style="color:black; font-size: 11px; background-color: #f6f6f6;"> 
                    <b style="color:black; font-size: 11px; background-color: #f6f6f6;">
                    MODELO DE ASIENTO: '.$modelo_asiento.'
                    </b>
                    <span style="font-size: 20px; color:#f6f6f6;">l</span>
                </td> 
            </tr>
            <small  HEIGHT="1" style="height: 1px; font-size: 7px;"><br></small>
            <tr> 
                <td  HEIGHT="30" width="100%" style="color:black; font-size: 11px; background-color: #f6f6f6;"> 
                    <b style="color:black; font-size: 11px; background-color: #f6f6f6;">
                    LIDER: '.$persona.'
                    </b>
                    <span style="font-size: 20px; color:#f6f6f6;">l</span>
                </td> 
            </tr>
            <small  HEIGHT="1" style="height: 1px; font-size: 7px;"><br></small>
            <tr> 
                <td HEIGHT="30" width="49%" style="color:black; font-size: 11px; background-color: #f6f6f6;"> 
                    <b>
                    FECHA DE SOLICITUD: '.$fecha_solicitud.'
                    </b>
                    <span style="font-size: 20px; color:#f6f6f6;">l</span>
                </td> 
                <td width="2%"></td>
                <td  HEIGHT="30" width="49%" style="color:black; font-size: 11px; background-color: #f6f6f6;"> 
                    <b>  
                    FECHA DE ENTREGA: '.$fecha_entrega.'
                    </b>
                    <span style="font-size: 20px; color:#f6f6f6;">l</span>
                </td>
            </tr>
            <tr> 
                <td width="0.8%">
                </td> 
                <td width="98.5%">
                    <div style="border-bottom-color: #bd1620; border-bottom: 5px"></div>
                </td> 
            </tr>';
            $cont=0;
            foreach ($config_d_result as $x){    
                if($cont>=1){
                    $html.='<small  HEIGHT="1" style="height: 1px; font-size: 7px;"><br></small>';
                }
                $html.='<tr> 
                    <td  HEIGHT="30" width="100%" style="color:black; background-color: #f6f6f6;"> 
                        <b style="color:black; font-size: 12px; background-color: #f6f6f6;">
                        CONFIGURACIÓN: '.$x->nombresub.'
                        </b>
                        <span style="font-size: 20px; color:#f6f6f6;">l</span>
                    </td> 
                </tr>'; 
                $producto_result = $this->ModelGeneral->salida_config($x->ordenIdd);
                foreach ($producto_result->result() as $pro){
                    $html.='<small  HEIGHT="1" style="height: 1px; font-size: 7px;"><br></small>
                    <tr> 
                        <td HEIGHT="30" width="30%" style="color:black; font-size: 12px; background-color: #f6f6f6;"> 
                            <b>
                            NO. '.$pro->pieza.'
                            </b>
                            <span style="font-size: 20px; color:#f6f6f6;">l</span>
                        </td> 
                        <td width="2%"></td>
                        <td  HEIGHT="30" width="68%" style="color:black; font-size: 12px; background-color: #f6f6f6;"> 
                            <b>  
                            SUBCONFIGURACIÓN: '.$pro->nombresub.'
                            </b>
                            <span style="font-size: 20px; color:#f6f6f6;">l</span>
                        </td>
                    </tr>';
                    $pro_result = $this->ModelGeneral->producto_config($pro->configdId,0);
                    foreach ($pro_result->result() as $pro_x){
                        $mult=$pro->pieza*$pro_x->piezas;
                    $html.='<small  HEIGHT="1" style="height: 1px; font-size: 7px;"><br></small>
                        <tr> 
                            <td width="5%"></td>
                            <td HEIGHT="30" width="30%" style="color:black; font-size: 12px; background-color: #f6f6f6;"> 
                                <b>
                                CANTIDAD: '.$mult.'
                                </b>
                                <span style="font-size: 20px; color:#f6f6f6;">l</span>
                            </td> 
                            <td width="2%"></td>
                            <td  HEIGHT="30" width="63%" style="color:black; font-size: 12px; background-color: #f6f6f6;"> 
                                <b>  
                                PRODUCTO: '.$pro_x->producto.'
                                </b>
                                <span style="font-size: 20px; color:#f6f6f6;">l</span>
                            </td>
                        </tr>';
                    }
                }
                $cont++; 
            }
  $html.='</table>';

$pdf->writeHTML($html, true, false, true, false, '');
$pdf->Output('documento.pdf', 'I');
//$pdf->Output('files/'.$GLOBALS["carpeta"].'/facturas/'.$GLOBALS["rrfc"].'_'.$GLOBALS["Folio"].'.pdf', 'F');
?>
