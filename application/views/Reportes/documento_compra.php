<?php
    require_once('TCPDF3/examples/tcpdf_include.php');
    require_once('TCPDF3/tcpdf.php');
    $this->load->helper('url');

//=======================================================================================
class MYPDF extends TCPDF {
    //Page header
    public function Header() {
    
        $img_file = base_url().'public/img/formatos/header3.PNG'; 
        $this->Image($img_file, 40, 3, 130, 20, '', '', '', false, 330, '', false, false, 0); 

        $html = ''; 

        $this->writeHTML($html, true, false, true, false, ''); 
    }
    // Page footer
    public function Footer() {
        /*
        $img_file = base_url().'public/img/formatos/footer.PNG'; 
        $this->Image($img_file, 0, 250, 212, 45, '', '', '', false, 330, '', false, false, 0); 
        */
        $html='';   
        $this->writeHTML($html, true, false, true, false, ''); 
    }
} 
$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Mapgroup');
$pdf->SetTitle('Solicitud de material');
$pdf->SetSubject('Solicitud de material');
$pdf->SetKeywords('Solicitud de material');

// set default header data
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins('8', '22', '8'); 
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER); 
//$pdf->SetFooterMargin(PDF_MARGIN_FOOTER); 
$pdf->SetFooterMargin('8'); 
// set auto page breaks
$pdf->SetAutoPageBreak(true, 8);
// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

$pdf->SetFont('dejavusans',13);
// add a page
$pdf->AddPage('P', 'A4');
  $html='<style>
  .table-striped tbody tr:nth-of-type(odd) {
    background-color: rgb(255 18 18 / 5%);
}
        </style>
        <br>
        <table width="100%" border="1" RULES="rows" style="padding: 5px;" class="table table-striped"> 
            <tr> 
                <td width="100%" style="color:black; background-color: #f6f6f6; font-size: 10px; text-align: center;">FRABRICACIÓN DE ASIENTOS PARA AUTOBUSES Y EQUIPAMIENTO DE VANES<br>
                    CARRETERA VIA CORTA KM 23, #23 COLONIA CALHUACA SANTA ISABEL XILOXOXOTLA, TLAXCALA CP. 90794<br>map_autotransformaciones@yahoo.com.mx              
                </td>
            </tr>
            <tr> 
                <td width="25%" style="font-size: 8px;">NÚMERO DE PROVEEDOR: '.$idproveedor.'</td>
                <td width="15%" style="font-size: 8px;"></td> 
                <td width="9%" style="font-size: 8px;"></td>
                <td width="11%" style="font-size: 8px;"></td> 
                <td width="28%" style="color:black; font-size: 8px; text-align: center;">No. de requición </td> 
                <td width="12%" style="color:black; font-size: 8px; text-align: center;">'.$requsicionId.'</td> 
            </tr>
            <tr> 
                <td width="11%" style="color:black; font-size: 8px; text-align: center;">PROVEEDOR:</td> 
                <td width="38%" style="color:black; font-size: 8px; text-align: center;">'.$proveedor.'</td> 
                <td width="11%" style="font-size: 8px;"></td>
                <td width="28%" style="color:black; font-size: 8px; text-align: center;">NO. ORDEN DE COMPRA B2:</td> 
                <td width="12%" style="color:black; font-size: 8px; text-align: center;">'.$idcompra.'</td> 
            </tr>
            <tr> 
                <td width="11%" style="color:black; font-size: 8px; text-align: center;">TELEFONO:</td> 
                <td width="38%" style="color:black; font-size: 8px; text-align: center;">'.$celular.'</td> 
                <td width="11%" style="font-size: 8px;"></td>
                <td width="14%" style="color:black; font-size: 8px;">Forma de pago</td>
                <td width="14%" style="color:black; font-size: 8px;">FECHA:</td> 
                <td width="12%" style="color:black; font-size: 8px;">'.$fecha_compra.'</td>
            </tr>
            <tr> 
                <td width="49%" style="font-size: 8px;"></td> 
                <td width="11%" style="font-size: 8px;"></td>
                <td width="14%" style="color:black; font-size: 8px;">'.$metodo_pago.'</td>
                <td width="14%" style="color:black; font-size: 8px;"></td> 
                <td width="12%" style="color:black; font-size: 8px;"></td>
            </tr>
            <tr> 
                <td width="11%" style="color:black; font-size: 8px; text-align: center; background-color: #BFBFBF;">CODIGO</td> 
                <td width="29%" style="color:black; font-size: 8px; text-align: center; background-color: #BFBFBF;">DESCRIPCIÒN</td> 
                <td width="9%" style="color:black; font-size: 8px; text-align: center; background-color: #BFBFBF;">STOCK</td> 
                <td width="11%" style="color:black; font-size: 8px; text-align: center; background-color: #BFBFBF;">CANTIDAD</td>
                <td width="14%" style="color:black; font-size: 8px; text-align: center; background-color: #BFBFBF;">U/M</td>
                <td width="14%" style="color:black; font-size: 8px; text-align: center; background-color: #BFBFBF;">PRECIO U.</td>
                <td width="12%" style="color:black; font-size: 8px; text-align: center; background-color: #BFBFBF;">IMPORTE TOTAL</td>
            </tr>';
            $cont=1;
            $subtotal=0;
            $color=0;
            foreach ($get_productos as $i){
                $c1='';
                if($color==0){
                    $color=1;
                    $c1='FCD5B4';
                }else{
                    $color=0;
                    $c1='FDE9D9';
                }
                $html.='<tr> 
                    <td width="11%" style="color:black; font-size: 8px; text-align: center; background-color: #'.$c1.';">'.$cont.'</td> 
                    <td width="29%" style="color:black; font-size: 8px; background-color: #'.$c1.';">'.$i->nombre.'</td> 
                    <td width="9%" style="color:black; font-size: 8px; text-align: center; background-color: #'.$c1.';">'.$i->stock.'</td> 
                    <td width="11%" style="color:black; font-size: 8px; text-align: center; background-color: #'.$c1.';">'.$i->cantidad.'</td>
                    <td width="14%" style="color:black; font-size: 8px; text-align: center; background-color: #'.$c1.';">'.$i->unidad.'</td>
                    <td width="14%" style="color:black; font-size: 8px; text-align: center; background-color: #'.$c1.';">'.$i->costo.'</td>
                    <td width="12%" style="color:black; font-size: 8px; text-align: center; background-color: #'.$c1.';">$'.number_format($i->total,2).'</td>
                </tr>';
                $subtotal+=$i->total;
                $cont++;
            } 
            $impuesto=$subtotal*0.16;
            $suma=$subtotal+$impuesto;
            $html.='<tr> 
                    <td width="60%" style="color:black; font-size: 8px;" rowspan="3">OBSERVACIONES: '.$comentarios.'</td> 
                    <td width="14%" style="color:black; font-size: 8px; text-align: center;"></td>
                    <td width="14%" style="color:black; font-size: 8px; text-align: center;">SUBTOTAL</td>
                    <td width="12%" style="color:black; font-size: 8px; text-align: center;">$'.number_format($subtotal,2).'</td>
                </tr>
                <tr> 
                    <td width="14%" style="color:black; font-size: 8px; text-align: center;"></td>
                    <td width="14%" style="color:black; font-size: 8px; text-align: center;">IMPUESTO</td>
                    <td width="12%" style="color:black; font-size: 8px; text-align: center;">$'.number_format($impuesto,2).'</td>
                </tr>
                <tr> 
                    <td width="14%" style="color:black; font-size: 8px; text-align: center;"></td>
                    <td width="14%" style="color:black; font-size: 8px; text-align: center;">TOTAL</td>
                    <td width="12%" style="color:black; font-size: 8px; text-align: center;">$'.number_format($suma,2).'</td>
                </tr>';
            $html.='<tr> 
                    <td width="100%" style="color:black; font-size: 8px; text-align: center;"></td>
                </tr>    
                <tr> 
                    <td width="60%" style="color:black; font-size: 18px; text-align: center;"></td> 
                    <td width="40%" style="color:#002060; font-size: 18px; text-align: center;"><b>'.$banco.'<br><br>CLABE:<br>'.$clave.'</b></td>
                </tr>';
        $html.='</table>';

$pdf->writeHTML($html, true, false, true, false, '');
$pdf->Output('documento.pdf', 'I');
//$pdf->Output('files/'.$GLOBALS["carpeta"].'/facturas/'.$GLOBALS["rrfc"].'_'.$GLOBALS["Folio"].'.pdf', 'F');
?>
