<?php
require_once('TCPDF3/examples/tcpdf_include.php');
require_once('TCPDF3/tcpdf.php');
$this->load->helper('url');

foreach ($cotizacion as $data) {
	$id = $data->id_ncotizacion;
	$equipamiento = $data->equipamiento;
	$folio = $data->folio;
	$vehiculo = $data->vehiculo;
	$registro = $data->registro;
}

$equip_list = $datos;

switch ($vehiculo) {
	case "1":
		$vehiculo = "Sprinter";
		break;
	case "2":
		$vehiculo = "Crafter";
		break;
	default:
		break;
}

if ($vehiculo == "Sprinter") {
	switch ($equipamiento) {
		case "1":
			$equipamiento = "Turismo alto";
			break;
		case "2":
			$equipamiento = "Turismo medio";
			break;
		case "3":
			$equipamiento = "Transporte de personal";
			break;
		case "4":
			$equipamiento = "Transporte publico";
			break;
		default:
			$equipamiento = " ";
			break;
	}
} else if ($vehiculo == "Crafter") {
	switch ($equipamiento) {
		case "1":
			$equipamiento = "Turismo alto";
			break;
		case "2":
			$equipamiento = "Turismo medio";
			break;
		case "3":
			$equipamiento = "Urbana";
			break;
		default:
			$equipamiento = " ";
			break;
	}
}

$logo = base_url() . "public/img/formatos/header3.jpg";
//=======================================================================================
class MYPDF extends TCPDF
{
	//Page header
	public function Header()
	{
		$html = '
          <table width="100%" border="1" cellpadding="4px" class="info_fac">
            <tr>
              <td width="100%"></td>
            </tr>
          ';
		$this->writeHTML($html, true, false, true, false, '');
	}
	// Page footer
	public function Footer()
	{
		$html = '';
		$this->writeHTML($html, true, false, true, false, '');
	}
}
$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Mapgroup');
$pdf->SetTitle('Cotizacion vehiculo');
$pdf->SetSubject('Cotizacion vehiculo');
$pdf->SetKeywords('Cotizacion vehiculo');

// set default header data
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

// set header and footer fonts
$pdf->setHeaderFont(array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins('8', '15', '8', '15');
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
//$pdf->SetFooterMargin(PDF_MARGIN_FOOTER); 
$pdf->SetFooterMargin('8');
// set auto page breaks
$pdf->SetAutoPageBreak(true, 8);
// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

$pdf->SetFont('dejavusans', '', 13);
// add a page
$pdf->AddPage('P', 'LETTER');
$html = '
			<table width="100%" border="1" RULES="rows" style="padding: 5px;" class="table table-striped"> 
				<tr> 
					<td width="50%" style="color:black; text-align: center;">
						<img src="' . $logo . '">
					</td>

					<td width="50%" style="color:black; font-size: 9px; text-align: right;">
						<span style="font-size: 16px;"><b>COTIZACION: ' . $folio . '</b></span><br>
						<span style="font-size: 12px;">Equipamiento de vanes</span><br>
						Carretera vía corta km 23, # 24 colonia Calhuaca Santa Isabel Xiloxoxtla,<br>
						Tlaxcala cp. 90194
					</td>
				</tr>
				<tr>
					<td width="50%" style="font-size: 12px; text-align: center;"><b>EQUIPAMIENTO:</b></td>
					<td width="50%" style="font-size: 14px; text-align: center;"><b>' . $equipamiento . '</b></td>
				</tr>
				<tr> 
					<td width="25%" style="font-size: 11px;"><b>MOD.UNIDAD:</b></td>
          <td width="25%" style="font-size: 11px;">' . $vehiculo . '</td>
					<td width="25%" style="font-size: 11px;"><b>FECHA:</b></td>
          <td width="25%" style="font-size: 11px;">' . $registro . '</td> 
        </tr>
        <tr> 
          <td width="100%" style="color:#002060; font-size: 10px; text-align: center;">
						<i>
							Atendiendo a su solicitud aprovechamos la oportunidad para enviarle un 
							cordial saludo y al mismo tiempo manifestarle nuestro interés en servirle, 
							por lo que ponemos a su consideración la siguiente cotización.
						</i>
					</td> 
        </tr>';
foreach ($equip_list as $key => $dato) {
	$html .= '
						<tr>
							<td width="5%" style="color:black; font-size: 11px; text-align: center;">' . ($key + 1) . '</td> 
							<td width="75%" style="color:black; font-size: 10px;"><b>' . $dato->title . '</b>' . $dato->text . '</td> 
							<td width="20%" style="margin:auto">
								<img src="' . base_url() . 'public/img/equipamiento/' . $dato->img . '">
							</td>
						</tr>
					';
}
$html .= '
      </table>';

$pdf->writeHTML($html, true, false, true, false, '');
$pdf->IncludeJS('print(true);');
$pdf->Output('documento.pdf', 'I');
//$pdf->Output('files/'.$GLOBALS["carpeta"].'/facturas/'.$GLOBALS["rrfc"].'_'.$GLOBALS["Folio"].'.pdf', 'F');
