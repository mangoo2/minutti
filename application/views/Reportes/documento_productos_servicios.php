<?php
require_once('TCPDF/examples/tcpdf_include.php');
require_once('TCPDF/tcpdf.php');
$this->load->helper('url');

$Cotizacion = $noCotizacion;

//=======================================================================================
class MYPDF extends TCPDF
{
	//Page header
	public function Header() {
    
		$img_file = base_url() . "public/img/formatos/headerm.png";
		$this->Image($img_file, 0, 0, 216, 35, '', '', 'T', false, 330, 'C', false, false, 0); 

		$html = ''; 
		$this->writeHTML($html, true, false, true, false, ''); 
}
	// Page footer
	public function Footer()
	{
		$img_file = base_url() . "public/img/formatos/footerm.png";
    $this->Image($img_file, 0, 244, 216, 36, '', '', 'B', false, 330, 'C', false, false, 0); 
        
    $html='';   
    $this->writeHTML($html, true, false, true, false, ''); 
	}
}
$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Minutti');
$pdf->SetTitle('Cotizaciones');
$pdf->SetSubject('Cotizaciones');
$pdf->SetKeywords('Cotizaciones');

// set default header data
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

// set header and footer fonts
$pdf->setHeaderFont(array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins('6', '10', '6', '15');
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
//$pdf->SetFooterMargin(PDF_MARGIN_FOOTER); 
$pdf->SetFooterMargin('8');
// set auto page breaks
$pdf->SetAutoPageBreak(true, 8);
// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

$pdf->SetFont('dejavusans', '', 13);
// add a page
$pdf->AddPage('P', 'LETTER');
$html = '
			<table width="100%" border="0" RULES="rows" style="padding: 1.5px;" class="table table-striped">
			<tr> 
					<td width="100%"></td>
				</tr>
				<tr> 
					<td width="65%" style="font-size: 12px;"></td>
					<td width="35%" style="font-size: 12px; text-align: rigth;">
						<b>COT: </b> ' .$noCotizacion. '
					</td>
				</tr>
				<tr>
					<td width="100%"></td>
				</tr>
				<tr> 
					<td width="45%" style="font-size: 12px;">
						<b>Diagonal San Pablo Xochimehuacan No. 170, Local A, San Felipe Hueyotlipan, 72030 Puebla, Pue.</b>
					</td>
          <td width="55%"></td>
        </tr>
				<tr> 
					<td width="45%" style="font-size: 11px;">
						<b>Correo electrónico:</b> ' .$correo. '<br>
						<b>Teléfono:</b> ' .$telefono. '
					</td>
					<td width="20%"></td>
          <td width="35%" style="font-size: 12px;">
						Usuario que emitió: <b>' .$usuario. '</b><br>
						Fecha de expedición: <b>' .$fecha. '</b>
					</td> 
        </tr>


        <tr> 
          <td width="100%" style="color:#002060; font-size: 10px; text-align: center;border-top:4px solid #DB030A;">
						<i>
						Por medio del presente documento le exponemos el siguiente plan de inversión acerca de los siguientes productos y/o servicios: 
						Cualquier duda y/o comentario(s) favor de comunicarse a nuestras oficinas con teléfono: 
						(222) 286 5661 o correo electrónico: domingo.minutti@hotmail.com en horario de atención a clientes de lunes a viernes de 9:00 am a 8:00pm.
						</i>
					</td> 
        </tr>
				<tr>
					<td width="100%"></td>
				</tr>
				<tr>
					<td width="100%"  style="border-bottom:4px solid #DB030A; font-size: 12px;">
						<b>Productos y/o Servicios</b>
					</td>
				</tr>
				<tr>
					<td width="8%" style="font-size: 10px; text-align: center;">Cantidad</td>
					<td width="15%" style="font-size: 10px; text-align: center;">Código</td>
					<td width="29%" style="font-size: 10px; text-align: center;">Nombre</td>
					<td width="30%" style="font-size: 10px; text-align: center;">Descripción</td>
					<td width="10%" style="font-size: 10px; text-align: center;">P. Unitario</td>
					<td width="8%" style="font-size: 10px; text-align: center;">Importe</td>
				</tr>
				<tr>
					<td width="100%" style="border-top:4px solid #DB030A;"></td>
				</tr>
				';

foreach ($productos->result() as $index => $data) {
	$html .= '
		<tr>
			<td width="8%" style="font-size: 10px; text-align: center;">'. $data->cantidad .'</td>
			<td width="15%" style="font-size: 10px; text-align: center;">'. $data->cod .'</td>
			<td width="29%" style="font-size: 10px; text-align: center;">'. $data->nombre .'</td>
			<td width="30%" style="font-size: 10px; text-align: center;">'. $data->descripcion .'</td>
			<td width="10%" style="font-size: 10px; text-align: center;">'. $data->p_unitario .'</td>
			<td width="8%" style="font-size: 10px; text-align: center;">'. $data->total .'</td>
		</tr>
	';
}

foreach ($servicios->result() as $index => $data) {
	$html .= '
		<tr>
			<td width="8%" style="font-size: 10px; text-align: center;">'. $data->cantidad .'</td>
			<td width="10%" style="font-size: 10px; text-align: center;">'. $data->cod .'</td>
			<td width="29%" style="font-size: 10px; text-align: center;">'. $data->nombre .'</td>
			<td width="35%" style="font-size: 10px; text-align: center;">'. $data->descripcion .'</td>
			<td width="10%" style="font-size: 10px; text-align: center;">'. $data->p_unitario .'</td>
			<td width="8%" style="font-size: 10px; text-align: center;">'. $data->total .'</td>
		</tr>
	';
}

$html .= '<tr>
					<td width="100%"></td> 
				</tr>
				<tr>
					<td width="70%"></td> 
					<td width="15%" style="text-align: right; font-size: 11px; border-top:2px solid #DB030A;">
						<b>Sub Total: </b>
					</td>
					<td width="15%" style="font-size: 11px; border-top:2px solid #DB030A;">
						$' .number_format($subtotal,2). '
					</td>
				</tr>
				<tr>
					<td width="70%"></td> 
					<td width="15%" style="text-align: right; font-size: 11px; border-top:2px solid #DB030A;">
						<b>IVA: </b>
					</td>
					<td width="15%" style="font-size: 11px; border-top:2px solid #DB030A;">
						$' .number_format($iva,2). '
					</td>
				</tr>
				<tr>
					<td width="70%"></td> 
					<td width="15%" style="text-align: right; font-size: 11px; background-color: #F6C0C2; border-top:2px solid #DB030A;">
						<b>Total: </b>
					</td>
					<td width="15%" style="font-size: 11px; background-color: #F6C0C2; border-top:2px solid #DB030A;">
						$' .number_format($total,2). '
					</td>
				</tr>

				<tr>
					<td width="100%" style="font-size: 12px;">
						<b>Clausulas / Observaciones</b>
					</td>
				</tr>
				<tr>
					<td width="100%" style="font-size: 10px;">' .$observaciones. '</td>
				</tr>
				';

$html .= '
      </table>';

$pdf->writeHTML($html, true, false, true, false, '');
$pdf->IncludeJS('print(true);');
$pdf->Output('documento.pdf', 'I');
//$pdf->Output('files/'.$GLOBALS["carpeta"].'/facturas/'.$GLOBALS["rrfc"].'_'.$GLOBALS["Folio"].'.pdf', 'F');
