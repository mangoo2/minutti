<?php

    //require_once('TCPDF/examples/tcpdf_include.php');
    require_once('TCPDF3/tcpdf.php');
    $this->load->helper('url');

    foreach ($det as $item){
        $fecha_solicitud=date('d/m/Y',strtotime($item->fecha_solicitud));
        $fecha_reg=date('d/m/Y H:i:s',strtotime($item->reg));
        $personal=$item->personal;
        /*$origen=$item->suc_org;
        $destino=$item->suc_dest;*/
    }


//=======================================================================================
class MYPDF extends TCPDF {
    //Page header
    public function Header() {
    
        $img_file = base_url().'public/img/formatos/header3.PNG'; 
        $this->Image($img_file, 5, 3, 200, 18, '', '', '', false, 330, '', false, false, 0); 

        $html = ''; 

        $this->writeHTML($html, true, false, true, false, ''); 
    }
    // Page footer
    public function Footer() {
        
        $img_file = base_url().'public/img/formatos/footer.PNG'; 
        $this->Image($img_file, 0, 250, 212, 45, '', '', '', false, 330, '', false, false, 0); 
        
        $html='';   
        $this->writeHTML($html, true, false, true, false, ''); 
    }
} 
$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Mapgroup');
$pdf->SetTitle('Traspaso de Productos');
$pdf->SetSubject('Formato');
$pdf->SetKeywords('Traspaso entre bodegas');

// set default header data
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins('10', '10', '10'); 
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER); 
//$pdf->SetFooterMargin(PDF_MARGIN_FOOTER); 
$pdf->SetFooterMargin('45'); 
// set auto page breaks
$pdf->SetAutoPageBreak(true, 45);
// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

$pdf->SetFont('dejavusans',13);
// add a page
$pdf->AddPage('P', 'A4');
  $html='
        <style type="text/css">
            .div_prin{
                border-bottom-color: #bd1620; border-bottom: 5px;
            }
            .th_prin{
                color:black; font-size: 11px; text-align=center;
            }
            .b_sty{
                color:black; font-size: 16px;
            }
            .th_seg{
               color:black; font-size: 11px; background-color: #f6f6f6;
            }
            .span_space{
               font-size: 20px; color:#f6f6f6;
            }
            .space{
                height: 1px; font-size: 7px;
            }
        </style>
        <table width="100%" border="0" RULES="rows"> 
            <tr style="border:10px"> 
                <td width="0.8%">
                </td> 
                <td width="98.5%">
                    <div class="div_prin"></div>
                </td> 
            </tr>
            <tr>
                <td HEIGHT="18" align="center" width="100%"  class="th_prin"> 
                    <b class="b_sty">
                    TRASLADO DE PRODUCTO(S)
                    </b>
                </td> 
            </tr>
            <tr>
                <td HEIGHT="18" width="50%" class="th_seg"> 
                    <b class="th_seg">
                    FECHA SOLICITUD: '.$fecha_solicitud.' 
                    </b>
                    <span class="span_space">l</span>
                </td> 
                <td HEIGHT="18" width="50%" class="th_seg"> 
                    <b class="span_space">
                    PERSONA SOLICITA: '.$personal.'
                    </b>
                    <span class="span_space">l</span>
                </td> 
            </tr>
            <small HEIGHT="1" class="space"><br></small>
            <tr> 
                <td HEIGHT="18" width="100%" class="th_seg"> 
                    <b style="font-size: 11px;">
                    FECHA / HORA REGISTRO: '.$fecha_reg.'
                    </b>
                    <span class="span_space">l</span>
                </td> 
            </tr>
            <small HEIGHT="1" class="space"><br></small>

        </table>';
        $html.='<table width="100%">
                <tr><td><br></td></tr>
            </table>
            <style type="text/css">
                .styletd{
                    background-color: #f6f6f6;
                    color:black;
                    font-size: 8px;
                }
            </style>
            <table width="100%" border="0" RULES="rows"> 
                <thead>';
            $tot_prods=0;
            foreach ($result->result() as $pro){
                $tot_prods = $tot_prods + $pro->cantidad;
                $html.='
                <tr> 
                    <td width="50%" class="styletd"> 
                        <b class="styletd">
                        ORIGEN: '.$pro->suc_org.'
                        </b>
                    </td> 
                    <td width="50%" class="styletd"> 
                        <b style="font-size: 10px;">
                        DESTINO: '.$pro->suc_dest.'
                        </b>
                    </td> 
                </tr>
                <tr> 
                    <td width="30%" class="styletd"> 
                        <b>
                        CANTIDAD: '.$pro->cantidad.'
                        </b>
                    </td> 
                    <td width="70%" class="styletd"> 
                        <b>  
                        PRODUCTO: '.$pro->codigo.' - '.$pro->nombre.'
                        </b>
                    </td>
                </tr>
                 <tr> 
                    <td width="100%" class="styletd"> 
                        <hr>
                    </td> 
                </tr>';
            }
            $html.='</thead>
            <tfoot>
                <tr>
                    <td width="100%" style="color:black; font-size: 10px; background-color: #f6f6f6;">
                        <b>TOTAL DE PRODS. '.$tot_prods.'</b>
                    </td>
                </tr>
            </tfoot>';
            
  $html.='</table>';


$pdf->writeHTML($html, true, false, true, false, '');
$pdf->Output('documento.pdf', 'I');
//$pdf->Output('files/'.$GLOBALS["carpeta"].'/facturas/'.$GLOBALS["rrfc"].'_'.$GLOBALS["Folio"].'.pdf', 'F');
?>
