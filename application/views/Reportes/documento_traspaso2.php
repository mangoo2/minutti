<?php

    //require_once('TCPDF/examples/tcpdf_include.php');
    require_once('TCPDF3/tcpdf.php');
    $this->load->helper('url');

    foreach ($det as $item){
        $fecha_solicitud=date('d/m/Y',strtotime($item->fecha_solicitud));
        $fecha_reg=date('d/m/Y H:i:s',strtotime($item->reg));
        $personal=$item->personal;
        /*$origen=$item->suc_org;
        $destino=$item->suc_dest;*/
    }


//=======================================================================================
class MYPDF extends TCPDF {
    //Page header
    public function Header() {
    
        $img_file = base_url().'public/img/formatos/header3.PNG'; 
        $this->Image($img_file, 5, 3, 200, 18, '', '', '', false, 330, '', false, false, 0); 

        $html = ''; 

        $this->writeHTML($html, true, false, true, false, ''); 
    }
    // Page footer
    public function Footer() {
        
        $img_file = base_url().'public/img/formatos/footer.PNG'; 
        $this->Image($img_file, 0, 250, 212, 45, '', '', '', false, 330, '', false, false, 0); 
        
        $html='';   
        $this->writeHTML($html, true, false, true, false, ''); 
    }
} 
$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Mapgroup');
$pdf->SetTitle('Traspaso de Productos');
$pdf->SetSubject('Formato');
$pdf->SetKeywords('Traspaso entre bodegas');

// set default header data
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins('10', '15', '10'); 
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER); 
//$pdf->SetFooterMargin(PDF_MARGIN_FOOTER); 
$pdf->SetFooterMargin('145'); 
// set auto page breaks
$pdf->SetAutoPageBreak(true, 50);
// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

$pdf->SetFont('dejavusans',13);
// add a page
$pdf->AddPage('P', 'A4');
  $html='
        <table width="100%" border="0" RULES="rows"> 
            <tr style="border:10px"> 
                <td width="0.8%"></td> 
                <td width="98.5%">
                    <div style="border-bottom-color: #bd1620; border-bottom: 5px;"></div>
                </td> 
            </tr>
            <tr>
                <td HEIGHT="20" align="center" width="100%"  style="color:black; font-size: 11px; text-align=center;"> 
                    <b style="color:black; font-size: 16px;">
                    TRASLADO DE PRODUCTO(S)
                    </b>
                </td> 
            </tr>
            <tr>
                <td HEIGHT="20" width="50%" style="color:black; font-size: 11px; background-color: #f6f6f6;"> 
                    <b style="color:black; font-size: 11px; background-color: #f6f6f6;">
                    FECHA SOLICITUD: '.$fecha_solicitud.' 
                    </b>
                </td> 
                <td HEIGHT="20" width="50%" style="color:black; font-size: 11px; background-color: #f6f6f6;"> 
                    <b style="font-size: 20px; color:#f6f6f6;">
                    PERSONA SOLICITA: '.$personal.'
                    </b>
                </td> 
            </tr>
            <tr> 
                <td HEIGHT="20" width="100%" style="color:black; font-size: 11px; background-color: #f6f6f6;"> 
                    <b style="font-size: 11px;">
                    FECHA / HORA REGISTRO: '.$fecha_reg.'
                    </b>
                </td> 
            </tr>
        </table>';
        $html.='<table width="100%">
                <tr><td><br></td></tr>
            </table>
            <style type="text/css">
                .styletd{
                    background-color: #f6f6f6;
                    color:black;
                    font-size: 10px;
                }
            </style>
            <table width="100%" border="0" RULES="rows"> 
                <thead>';
            $tot_prods=0;
            foreach ($result->result() as $pro){
                $tot_prods = $tot_prods + $pro->cantidad;
                $html.='
                <tr> 
                    <td width="50%" style="background-color: #f6f6f6; color:black; font-size: 10px;"> 
                        <b style="background-color: #f6f6f6; color:black; font-size: 10px;">
                        ORIGEN: '.$pro->suc_org.'
                        </b>
                    </td> 
                    <td width="50%" style="background-color: #f6f6f6; color:black; font-size: 10px;"> 
                        <b style="font-size: 10px;">
                        DESTINO: '.$pro->suc_dest.'
                        </b>
                    </td> 
                </tr>
                <tr> 
                    <td width="30%" style="background-color: #f6f6f6; color:black; font-size: 10px;"> 
                        <b>
                        CANTIDAD: '.$pro->cantidad.'
                        </b>
                    </td> 
                    <td width="70%" style="background-color: #f6f6f6; color:black; font-size: 10px;"> 
                        <b>  
                        PRODUCTO: '.$pro->codigo.' - '.$pro->nombre.'
                        </b>
                    </td>
                </tr>
                 <tr> 
                    <td width="100%" style="background-color: #f6f6f6; color:black; font-size: 10px;"> 
                        <hr>
                    </td> 
                </tr>';
            }
            $html.='</thead>
            <tfoot>
                <tr>
                    <td width="100%" style="color:black; font-size: 10px; background-color: #f6f6f6;">
                        <b>TOTAL DE PRODS. '.$tot_prods.'</b>
                    </td>
                </tr>
            </tfoot>';
            
  $html.='</table>';


$pdf->writeHTML($html, true, false, true, false, '');
$pdf->Output('documento.pdf', 'I');
//$pdf->Output('files/'.$GLOBALS["carpeta"].'/facturas/'.$GLOBALS["rrfc"].'_'.$GLOBALS["Folio"].'.pdf', 'F');
?>
