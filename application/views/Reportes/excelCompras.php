<?php 
header("Content-Type: text/html;charset=UTF-8");
header("Pragma: public");
header("Expires:0");
header("Cache-Control:must-revalidate,post-check=0, pre-check=0");
header("Content-Type: application/force-download");
header("Content-Type: application/octet-stream");
header("Content-Type: application/download");
header("Content-Type: application/vnd.ms-excel;");
header("Content-Disposition: attachment; filename=reporte_compras.xls");
?>

<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
<style>
  .table-striped tbody tr:nth-of-type(odd) {
    background-color: rgb(255 18 18 / 5%);
    }
    .style_head{
        color:black; font-size: 10px; text-align: center; background-color: #BFBFBF;
    }
</style>
  <table width="100%"> 
      <thead>
        <tr>
          <td width="15%"></td>
          <td width="70%"><img style="width: 150px" src="<?php echo base_url(); ?>public/img/formatos/he3.png"></td>
          <td width="15%"></td>
        </tr>
      </thead>
    </table>

    <table width="100%" border="1"> 
      <thead>
        <tr>
          <td width="60%"><br><br><br><br></td>
          <td class="style_head" style="text-align=right" width="40%"> 
              FECHA CREACIÓN: <?php echo date("d-m-Y"); ?> 
          </td> 
        </tr>
      </thead>
    </table>

    <table width="100%" border="1" RULES="rows" style="padding: 5px;" class="table table-striped"> 
      <thead>
        <tr>
          <td rowspan="2" width="5%" class="style_head">#</td>  
          <td rowspan="2" width="15%" class="style_head">PROVEEDOR</td> 
          <td rowspan="2" width="5%" class="style_head">O.C</td>
          <td colspan="2" width="20%" class="style_head">FECHA DE</td>
          <td rowspan="2" width="10%" class="style_head">TIPO COMPRA</td> 
          <td rowspan="2" width="10%" class="style_head">MÉTODO</td> 
          <td rowspan="2" width="20%" class="style_head">PRODUCTO(S)</td> 
          <td rowspan="2" width="15%" class="style_head">TOTAL COMPRADO</td> 
        </tr>
        <tr>
          <td  class="style_head">COMPRA</td> 
          <td  class="style_head">SURTIMIENTO</td> 
        </tr>
      </thead>
      <?php $cont=1;
      $color=0; $subtotal=0; $iva=0; $total=0; $metodo="";
      foreach ($rep as $i){
        $c1='';
        if($color==0){
          $color=1;
          $c1='FCD5B4';
        }else{
          $color=0;
          $c1='FDE9D9';
        } 
        $subtotal=$subtotal+$i->total;
        $iva=$subtotal*.16;
        $total=$iva+$subtotal;
        if($i->metodo_pago==1)
          $metodo="Efectivo";
        else if($i->metodo_pago==2)
          $metodo="Tarjeta de crédito";
        else if($i->metodo_pago==3)
          $metodo="Tarjeta de débito";
        else if($i->metodo_pago==4)
          $metodo="Transferencia";
        else if($i->metodo_pago==5)
          $metodo="Cheque";
        echo '<tr> 
          <td width="5%" style="color:black; font-size: 10px; text-align: center; background-color: #'.$c1.';">'.$cont.'</td> 
          <td width="15%" style="color:black; font-size: 10px; background-color: #'.$c1.';">'.$i->nombre.'</td>
          <td width="5%" style="color:black; font-size: 10px; background-color: #'.$c1.';">'.$i->no_oc.'</td>
          <td width="10%" style="color:black; font-size: 10px; background-color: #'.$c1.';">'.$i->fecha_compra.'</td>  
          <td width="10%" style="color:black; font-size: 10px; background-color: #'.$c1.';">'.$i->fecha_surtimiento.'</td>  
          <td width="10%" style="color:black; font-size: 10px; background-color: #'.$c1.';">'.$i->tipo_compra.'</td>
          <td width="10%" style="color:black; font-size: 10px; background-color: #'.$c1.';">'.$metodo.'</td>  
          <td width="20%" style="color:black; font-size: 10px; background-color: #'.$c1.';">'.$i->producto_precio.'</td>  
          <td width="15%" style="color:black; font-size: 10px; background-color: #'.$c1.';">$'.number_format($i->total,2,".",",").'</td> 
        </tr>';
        $cont++;
      }  ?>
      <tfoot>
        <tr>
          <td colspan="8" style="color:black; font-size: 10px; text-align:right">SUBTOTAL</td>
          <td style="color:black; font-size: 10px; text-align:right"><b>$ <?php echo number_format($subtotal,2,".",","); ?></b></td>
        </tr>
        <tr>
          <td colspan="8" style="color:black; font-size: 10px; text-align:right">IVA</td>
          <td style="color:black; font-size: 10px; text-align:right"><b>$ <?php echo number_format($iva,2,".",","); ?></b></td>
        </tr>
        <tr>
          <td colspan="8" style="color:black; font-size: 10px; text-align:right">TOTAL</td>
          <td style="color:black; font-size: 10px; text-align:right"><b>$ <?php echo number_format($total,2,".",","); ?></b></td>
        </tr>
      </tfoot>
    </table>