<?php 
header("Content-Type: text/html;charset=UTF-8");
header("Pragma: public");
header("Expires:0");
header("Cache-Control:must-revalidate,post-check=0, pre-check=0");
header("Content-Type: application/force-download");
header("Content-Type: application/octet-stream");
header("Content-Type: application/download");
header("Content-Type: application/vnd.ms-excel;");
header("Content-Disposition: attachment; filename=reporte_productos.xls");
?>

<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
<style>
  .table-striped tbody tr:nth-of-type(odd) {
    background-color: rgb(255 18 18 / 5%);
    }
    .style_head{
        color:black; font-size: 10px; text-align: center; background-color: #BFBFBF;
    }
</style>
  <table width="100%"> 
      <thead>
        <tr>
          <td width="20%"></td>
          <td width="60%"><img style="width: 110px" src="<?php echo base_url(); ?>public/img/formatos/he1.png"></td>
          <td width="20%"></td>
        </tr>
      </thead>
    </table>

    <table width="100%" border="1"> 
      <thead>
        <tr>
          <td width="60%"><br><br><br><br></td>
          <td class="style_head" style="text-align=right" width="40%"> 
              FECHA CREACIÓN: <?php echo date("d-m-Y"); ?> 
          </td> 
        </tr>
      </thead>
    </table>

    <table width="100%" border="1" RULES="rows" style="padding: 5px;" class="table table-striped"> 
      <thead>
        <tr>
          <td width="10%" class="style_head">#</td>  
          <td width="25%" class="style_head">NAVE</td> 
          <td width="55%" class="style_head">PRODUCTO</td> 
          <td width="10%" class="style_head">STOCK</td> 
        </tr>
      </thead>
      <?php $cont=1;
      $color=0;
      foreach ($rep as $i){
          $c1='';
          if($color==0){
            $color=1;
            $c1='FCD5B4';
          }else{
            $color=0;
            $c1='FDE9D9';
          } 
          echo '<tr> 
            <td width="10%" style="color:black; font-size: 10px; text-align: center; background-color: #'.$c1.';">'.$cont.'</td> 
            <td width="25%" style="color:black; font-size: 10px; background-color: #'.$c1.';">'.$i->sucursal.'</td>
            <td width="55%" style="color:black; font-size: 10px; background-color: #'.$c1.';">'.$i->codigo.' '.$i->nombre.'</td>  
            <td width="10%" style="color:black; font-size: 10px; text-align: center; background-color: #'.$c1.';">'.$i->stock.'</td> 
          </tr>';
        $cont++;
      }  ?>
    </table>