<?php 
header("Content-Type: text/html;charset=UTF-8");
header("Pragma: public");
header("Expires:0");
header("Cache-Control:must-revalidate,post-check=0, pre-check=0");
header("Content-Type: application/force-download");
header("Content-Type: application/octet-stream");
header("Content-Type: application/download");
header("Content-Type: application/vnd.ms-excel;");
header("Content-Disposition: attachment; filename=reporte_proveedores.xls");
?>

<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
<style>
  .table-striped tbody tr:nth-of-type(odd) {
    background-color: rgb(255 18 18 / 5%);
    }
    .style_head{
        color:black; font-size: 10px; text-align: center; background-color: #BFBFBF;
    }
</style>
  <table width="100%"> 
      <thead>
        <tr>
          <td width="20%"></td>
          <td width="60%"><img style="width: 110px" src="<?php echo base_url(); ?>public/img/formatos/he2.png"></td>
          <td width="20%"></td>
        </tr>
      </thead>
    </table>

    <table width="100%" border="1"> 
      <thead>
        <tr>
          <td width="60%"><br><br><br><br></td>
          <td class="style_head" style="text-align=right" width="40%"> 
              FECHA CREACIÓN: <?php echo date("d-m-Y"); ?> 
          </td> 
        </tr>
      </thead>
    </table>

    <table width="100%" border="1" RULES="rows" style="padding: 5px;" class="table table-striped"> 
      <thead>
        <tr>
          <td width="5%" class="style_head">#</td>  
          <td width="18%" class="style_head">PROVEEDOR</td> 
          <td width="10%" class="style_head">BANCO</td> 
          <td width="13%" class="style_head">TITULAR</td> 
          <td width="12%" class="style_head">CUENTA</td> 
          <td width="15%" class="style_head">CLAVE</td> 
          <td width="12%" class="style_head">TARJETA</td> 
          <td width="15%" class="style_head">TOTAL COMPRADO</td> 
        </tr>
      </thead>
      <?php $cont=1;
      $color=0; $subtotal=0; $iva=0; $total=0;
      foreach ($rep as $i){
        $c1='';
        if($color==0){
          $color=1;
          $c1='FCD5B4';
        }else{
          $color=0;
          $c1='FDE9D9';
        } 
        $subtotal=$subtotal+$i->total_compra;
        $iva=($subtotal*.16);
        $total=$iva+$subtotal;
        echo '<tr> 
          <td width="5%" style="color:black; font-size: 10px; text-align: center; background-color: #'.$c1.';">'.$cont.'</td> 
          <td width="18%" style="color:black; font-size: 10px; background-color: #'.$c1.';">'.$i->nombre.'</td>
          <td width="10%" style="color:black; font-size: 10px; background-color: #'.$c1.';">'.$i->banco.'</td>  
          <td width="13%" style="color:black; font-size: 10px; background-color: #'.$c1.';">'.$i->titular.'</td>  
          <td width="12%" style="color:black; font-size: 10px; background-color: #'.$c1.';">'.$i->cuenta.'</td>
          <td width="15%" style="color:black; font-size: 10px; background-color: #'.$c1.';">'.$i->clave.'</td>  
          <td width="12%" style="color:black; font-size: 10px; background-color: #'.$c1.';">'.$i->tarjeta.'</td>  
          <td width="15%" style="color:black; font-size: 10px; background-color: #'.$c1.';">$'.number_format($i->total_compra,2,".",",").'</td> 
        </tr>
        ';
        $cont++;
      }  ?>
      <tfoot>
        <tr>
          <td colspan="7" style="color:black; font-size: 10px; text-align:right">SUBTOTAL</td>
          <td style="color:black; font-size: 10px; text-align:right"><b>$ <?php echo number_format($subtotal,2,".",","); ?></b></td>
        </tr>
        <tr>
          <td colspan="7" style="color:black; font-size: 10px; text-align:right">IVA</td>
          <td style="color:black; font-size: 10px; text-align:right"><b>$ <?php echo number_format($iva,2,".",","); ?></b></td>
        </tr>
        <tr>
          <td colspan="7" style="color:black; font-size: 10px; text-align:right">TOTAL</td>
          <td style="color:black; font-size: 10px; text-align:right"><b>$ <?php echo number_format($total,2,".",","); ?></b></td>
        </tr>
      </tfoot>
    </table>