<?php
    require_once('TCPDF3/tcpdf.php');
    $this->load->helper('url');

//=======================================================================================
class MYPDF extends TCPDF {
    //Page header
    public function Header() {
    
        $img_file = base_url().'public/img/formatos/h3.png'; 
        $this->Image($img_file, 15, 3, 180, 20, '', '', '', false, 330, '', false, false, 0); 

        $html = ''; 

        $this->writeHTML($html, true, false, true, false, ''); 
    }
    // Page footer
    public function Footer() {
        /*
        $img_file = base_url().'public/img/formatos/footer.PNG'; 
        $this->Image($img_file, 0, 250, 212, 45, '', '', '', false, 330, '', false, false, 0); 
        */
        $html='';   
        $this->writeHTML($html, true, false, true, false, ''); 
    }
} 
$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Mapgroup');
$pdf->SetTitle('Reporte de Compras');
$pdf->SetSubject('Compras');
$pdf->SetKeywords('Compras');

// set default header data
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins('8', '25', '8'); 
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER); 
//$pdf->SetFooterMargin(PDF_MARGIN_FOOTER); 
$pdf->SetFooterMargin('8'); 
// set auto page breaks
$pdf->SetAutoPageBreak(true, 8);
// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

$pdf->SetFont('dejavusans',13);
// add a page
$pdf->AddPage('P', 'A4');
  $html='<style>
  .table-striped tbody tr:nth-of-type(odd) {
    background-color: rgb(255 18 18 / 5%);
    }
    .style_head{
        color:black; font-size: 8px; text-align: center; background-color: #BFBFBF;
    }
    .td{
        color:black; font-size: 8px; text-align: center;
    }
    .td2{
        color:black; font-size: 8px;
    }
</style>
    <table width="100%" border="1"> 
      <thead>
        <tr>
          <td width="60%"></td>
          <td class="style_head" style="text-align=right" width="40%"> 
              FECHA CREACIÓN: '.date("d-m-Y").' 
          </td> 
        </tr>
      </thead>
    </table>

    <table width="100%" border="1" RULES="rows" style="padding: 5px;" class="table table-striped"> 
      <thead>
        <tr>
          <td rowspan="2" width="5%" class="style_head">#</td>  
          <td rowspan="2" width="15%" class="style_head">PROVEEDOR</td> 
          <td rowspan="2" width="4%" class="style_head">O.C</td>
          <td colspan="2" width="20%" class="style_head">FECHA DE</td>
          <td rowspan="2" width="10%" class="style_head">TIPO COMPRA</td> 
          <td rowspan="2" width="10%" class="style_head">MÉTODO</td> 
          <td rowspan="2" width="23%" class="style_head">PRODUCTO(S)</td> 
          <td rowspan="2" width="13%" class="style_head">TOTAL COMPRADO</td> 
        </tr>
        <tr>
          <td class="style_head">COMPRA</td> 
          <td class="style_head">SURTIMIENTO</td> 
        </tr>
      </thead>';
        $cont=1; $subtotal=0; $iva=0; $total=0; $metodo="";
        foreach ($rep as $i){
          $subtotal=$subtotal+$i->total;
          $iva=$subtotal*.16;
          $total=$iva+$subtotal;
          if($i->metodo_pago==1)
            $metodo="Efectivo";
          else if($i->metodo_pago==2)
            $metodo="Tarjeta de crédito";
          else if($i->metodo_pago==3)
            $metodo="Tarjeta de débito";
          else if($i->metodo_pago==4)
            $metodo="Transferencia";
          else if($i->metodo_pago==5)
            $metodo="Cheque";

          $html.='<tr> 
            <td width="5%" class="td">'.$cont.'</td> 
            <td width="15%" class="td2">'.$i->nombre.'</td>
            <td width="4%" class="td2">'.$i->no_oc.'</td>
            <td width="10%" class="td2">'.date("d-m-Y", strtotime($i->fecha_compra)).'</td>  
            <td width="10%" class="td2">'.date("d-m-Y", strtotime($i->fecha_surtimiento)).'</td>  
            <td width="10%" class="td2">'.$i->tipo_compra.'</td>
            <td width="10%" class="td2">'.$metodo.'</td>  
            <td width="23%" class="td2">'.$i->producto_precio.'</td>  
            <td width="13%" class="td">$'.number_format($i->total,2,".",",").'</td> 
          </tr>';
          $cont++;
        } 
    $html.='<tfoot>
              <tr>
                <td colspan="8" style="color:black; font-size: 8px; text-align:right">SUBTOTAL:</td>
                <td style="color:black; font-size: 8px; text-align:center"><b>$'.number_format($subtotal,2,".",",").'</b></td>
              </tr>
              <tr>
                <td colspan="8" style="color:black; font-size: 8px; text-align:right">IVA:</td>
                <td style="color:black; font-size: 8px; text-align:center"><b>$'.number_format($iva,2,".",",").'</b></td>
              </tr>
              <tr>
                <td colspan="8" style="color:black; font-size: 8px; text-align:right">TOTAL:</td>
                <td style="color:black; font-size: 8px; text-align:center"><b>$'.number_format($total,2,".",",").'</b></td>
              </tr>
            </tfoot>
          </table>';

$pdf->writeHTML($html, true, false, true, false, '');
$pdf->Output('reporte_compras.pdf', 'I');
//$pdf->Output('files/'.$GLOBALS["carpeta"].'/facturas/'.$GLOBALS["rrfc"].'_'.$GLOBALS["Folio"].'.pdf', 'F');
?>
