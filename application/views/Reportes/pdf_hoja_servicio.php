<?php
require_once('TCPDF/examples/tcpdf_include.php');
require_once('TCPDF/tcpdf.php');
$this->load->helper('url');
//=======================================================================================
class MYPDF extends TCPDF
{
	//Page header
	public function Header() {
    
		$img_file = base_url() . "public/img/formatos/headerm.png";
		$this->Image($img_file, 0, 0, 216, 35, '', '', 'T', false, 330, 'C', false, false, 0); 

		$html = ''; 
		$this->writeHTML($html, true, false, true, false, ''); 
}
	// Page footer
	public function Footer()
	{
		$img_file = base_url() . "public/img/formatos/footerm.png";
    $this->Image($img_file, 0, 244, 216, 36, '', '', 'B', false, 330, 'C', false, false, 0); 
        
    $html='';   
    $this->writeHTML($html, true, false, true, false, ''); 
	}
}
$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Minutti');
$pdf->SetTitle('Orden de servicio');
$pdf->SetSubject('Orden de servicio');
$pdf->SetKeywords('Orden de servicio');

// set default header data
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

// set header and footer fonts
$pdf->setHeaderFont(array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins('6', '25', '6', '15');
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
//$pdf->SetFooterMargin(PDF_MARGIN_FOOTER); 
$pdf->SetFooterMargin('8');
// set auto page breaks
$pdf->SetAutoPageBreak(true, PDF_MARGIN_BOTTOM);
//$pdf->SetAutoPageBreak(true, 8);
// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

$pdf->SetFont('dejavusans', '', 13);
// add a page
$pdf->AddPage('P', 'LETTER');
$html = '<table width="100%" border="0" RULES="rows" style="padding: 1.5px;" class="table table-striped">
		<tr> 
			<td width="65%" style="font-size: 17px;"><br>Orden de servicio</td>
			<td width="35%" style="font-size: 12px; text-align: rigth;">
				<b>Folio: '.$id_venta.'</b>
			</td>
		</tr>
    </table>';

$html.='<br><br><br><table width="100%" border="1" RULES="rows" style="padding: 1.5px;" class="table table-striped">
		<tr>
			<td width="25%" style="font-size: 11px;">HORA ENTRADA '.date('g:i a',strtotime($hora_entrada)).'</td>
			<td width="40%" style="font-size: 11px;">HORA SALIDA '.date('g:i a',strtotime($hora_salida)).'</td>
		</tr>
		<tr>
			<td width="65%" style="font-size: 11px;">NOMBRE '.$nombre.'</td>
			<td width="35%" style="font-size: 11px;">R.F.C. '.$rfc.'</td>
		</tr>
		<tr>
			<td width="65%" style="font-size: 11px;">DIRECCIÓN '.$direccion.'</td>
			<td width="35%" style="font-size: 11px;"></td>
		</tr>
		<tr>
			<td width="25%" style="font-size: 11px;">CIUDAD '.$estado.'</td>
			<td width="20%" style="font-size: 11px;">C.P '.$codigo_postal.'</td>
			<td width="20%" style="font-size: 11px;">TEL. '.$celular.'</td>
            <td width="35%" style="font-size: 11px;">RECIBE '.$recibe.'</td>
		</tr>
		<tr>
			<td width="25%" style="font-size: 11px;">FECHA '.date('d/m/Y',strtotime($fecha)).'</td>
			<td width="20%" style="font-size: 11px;">TIPO '.$tipo.'</td>
			<td width="20%" style="font-size: 11px;">PLACAS. '.$placas.'</td>
            <td width="35%" style="font-size: 11px;">IVA DESGLOSADO ';
            if($iva_desglosado==1){
                $html.='X';
            }else{
                $html.='';  
            }
            $html.='</td>
		</tr>
	</table>';

$html.='<br><br><table width="100%" border="1" RULES="rows" style="padding: 1.5px;" class="table table-striped">
		<tr>
			<td width="20%" style="font-size: 10px;">ALINEACION</td><td width="5%" style="font-size: 10px;">';
            if($aliniacion==1){
                $html.='X';
            }else{
                $html.='';  
            }
            $html.='</td>
			<td width="20%" style="font-size: 10px;">VÁLVULAS</td><td width="5%" style="font-size: 10px;">';
            if($valvulas==1){
                $html.='X';
            }else{
                $html.='';  
            }
            $html.='</td>
			<td width="20%" style="font-size: 10px;">BALANCEO</td><td width="5%" style="font-size: 10px;">';
            if($balance==1){
                $html.='X';
            }else{
                $html.='';  
            }
            $html.='</td>
            <td width="20%" style="font-size: 10px;">MONTAJES</td><td width="5%" style="font-size: 10px;">';
            if($montajes==1){
                $html.='X';
            }else{
                $html.='';  
            }
            $html.='</td>
		</tr>
		<tr>
			<td width="20%" style="font-size: 10px;"></td><td width="5%" style="font-size: 10px;">';
            $html.='</td>
			<td width="20%" style="font-size: 10px;">AFINACIÓN</td><td width="5%" style="font-size: 10px;">';
            if($afinacion==1){
                $html.='X';
            }else{
                $html.='';  
            }
            $html.='</td>
			<td width="20%" style="font-size: 10px;">ROTACIÓN</td><td width="5%" style="font-size: 10px;">';
            if($rotacion==1){
                $html.='X';
            }else{
                $html.='';  
            }
            $html.='</td>
            <td width="20%" style="font-size: 10px;">PARCHES</td><td width="5%" style="font-size: 10px;">';
            if($parches==1){
                $html.='X';
            }else{
                $html.='';  
            }
            $html.='</td>
		</tr>
	</table>';

	$html.='<br><br><table width="100%" border="1" RULES="rows" style="padding: 1.5px;" class="table table-striped">
		<tr>
			<td width="11%" style="font-size: 10px;">LLANTAS</td>
			<td width="13%" style="font-size: 10px;">CANTIDAD '.$l_cantida.'</td>
			<td width="47%" style="font-size: 10px;">MEDIDA '.$l_media.'</td>
            <td width="9%" style="font-size: 10px;">ACEITE</td>
            <td width="10%" style="font-size: 10px;">CANT. '.$a_cantidad.'</td>
            <td width="10%" style="font-size: 10px;">MEDIDA '.$a_media.'</td>
		</tr>
		<tr>
			<td width="11%" style="font-size: 10px;">CAMARAS</td>
			<td width="13%" style="font-size: 10px;">CANTIDAD '.$c_cadidad.'</td>
			<td width="11%" style="font-size: 10px;">MEDIDA '.$l_media.'</td>
            <td width="19%" style="font-size: 10px;">AMORTIGUARDOR '.$c_amortiguador.'</td>
            <td width="7%" style="font-size: 10px;">MARCA</td>
            <td width="5%" style="font-size: 10px;">D '.$c_d.'</td>
            <td width="5%" style="font-size: 10px;">T '.$c_t.'</td>
            <td width="9%" style="font-size: 10px;">FILTRO</td>
            <td width="10%" style="font-size: 10px;">CANT. '.$f_cantidad.'</td>
            <td width="10%" style="font-size: 10px;">TIPO '.$f_tipo.'</td>
		</tr>
		<tr>
			<td width="11%" style="font-size: 10px;">CORBATA</td>
			<td width="13%" style="font-size: 10px;">CANTIDAD '.$co_cantidad.'</td>
			<td width="11%" style="font-size: 10px;">TIPO '.$co_tipo.'</td>
            <td width="19%" style="font-size: 10px;">INS.AMORTIGUARDORES '.$co_ins_amortiguador.'</td>
            <td width="7%" style="font-size: 10px;">N '.$co_n.'</td>
            <td width="5%" style="font-size: 10px;">E '.$co_e.'</td>
            <td width="5%" style="font-size: 10px;"></td>
            <td width="9%" style="font-size: 10px;">BALATAS</td>
            <td width="10%" style="font-size: 10px;">D '.$ba_d.'</td>
            <td width="10%" style="font-size: 10px;">T '.$ba_t.'</td>
		</tr>
		<tr>
			<td width="100%" style="font-size: 10px;">E-MAIL '.$correo.'</td>
		</tr>
		<tr>
			<td width="100%" style="font-size: 10px;">MANO DE OBRA '.$mano_obra.'</td>
		</tr>
		<tr>
			<td width="100%" style="font-size: 10px;">OTROS '.$otros.'</td>
		</tr>
	</table>';
	$html.='<br><br><table width="100%" border="0" RULES="rows" style="padding: 1.5px;" class="table table-striped">
	    <tr>
		    <td width="60%" style="font-size: 10px;">
		        <table width="100%" border="0" RULES="rows" style="padding: 1.5px;" class="table table-striped">
			        <tr>
						<td width="40%" style="font-size: 10px;"><b>10 PUNTOS DE SEGURIDAD</b></td>
						<td width="60%" style="font-size: 13px; text-align:center">Estado 
						</td>
					</tr>
					<tr>
						<td width="40%" style="font-size: 10px;"></td>
						<td width="20%" style="font-size: 10px;">
						    <table width="100%" border="0" RULES="rows" style="padding: 1.5px;" class="table table-striped">
								<tr>
									<td width="100%" style="font-size: 10px; text-align:center">BUENO</td>
								</tr>
							</table>
						</td>
						<td width="20%" style="font-size: 10px;">
						    <table width="100%" border="0" RULES="rows" style="padding: 1.5px;" class="table table-striped">
								<tr>
									<td width="100%" style="font-size: 10px; text-align:center">REGULAR</td>
								</tr>
							</table>
						</td>
						<td width="20%" style="font-size: 10px;">
						    <table width="100%" border="0" RULES="rows" style="padding: 1.5px;" class="table table-striped">
								<tr>
									<td width="100%" style="font-size: 10px; text-align:center">MALO</td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td width="40%" style="font-size: 13px;">1. Llantas</td>
						<td width="20%" style="font-size: 10px;">
						    <table width="100%" border="1" RULES="rows" style="padding: 1.5px;" class="table table-striped">
								<tr>
									<td width="100%" style="font-size: 10px; text-align:center">';
							            if($b1==1){
							                $html.='X';
							            }else{
							                $html.='';  
							            }
							            $html.='
									</td>
								</tr>
							</table>
						</td>
						<td width="20%" style="font-size: 10px;">
						    <table width="100%" border="1" RULES="rows" style="padding: 1.5px;" class="table table-striped">
								<tr>
									<td width="100%" style="font-size: 10px; text-align:center">';
							            if($r1==1){
							                $html.='X';
							            }else{
							                $html.='';  
							            }
							            $html.='
							        </td>
								</tr>
							</table>
						</td>
						<td width="20%" style="font-size: 10px;">
						    <table width="100%" border="1" RULES="rows" style="padding: 1.5px;" class="table table-striped">
								<tr>
									<td width="100%" style="font-size: 10px; text-align:center">';
							            if($m1==1){
							                $html.='X';
							            }else{
							                $html.='';  
							            }
							            $html.='
							        </td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td width="40%" style="font-size: 13px;">2. Amortiguadores</td>
						<td width="20%" style="font-size: 10px;">
						    <table width="100%" border="1" RULES="rows" style="padding: 1.5px;" class="table table-striped">
								<tr>
									<td width="100%" style="font-size: 10px; text-align:center">';
							            if($b2==1){
							                $html.='X';
							            }else{
							                $html.='';  
							            }
							            $html.='
									</td>
								</tr>
							</table>
						</td>
						<td width="20%" style="font-size: 10px;">
						    <table width="100%" border="1" RULES="rows" style="padding: 1.5px;" class="table table-striped">
								<tr>
									<td width="100%" style="font-size: 10px; text-align:center">';
							            if($r2==1){
							                $html.='X';
							            }else{
							                $html.='';  
							            }
							            $html.='
							        </td>
								</tr>
							</table>
						</td>
						<td width="20%" style="font-size: 10px;">
						    <table width="100%" border="1" RULES="rows" style="padding: 1.5px;" class="table table-striped">
								<tr>
									<td width="100%" style="font-size: 10px; text-align:center">';
							            if($m2==1){
							                $html.='X';
							            }else{
							                $html.='';  
							            }
							            $html.='
							        </td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td width="40%" style="font-size: 13px;">3. Frenos</td>
						<td width="20%" style="font-size: 10px;">
						    <table width="100%" border="1" RULES="rows" style="padding: 1.5px;" class="table table-striped">
								<tr>
									<td width="100%" style="font-size: 10px; text-align:center">';
							            if($b3==1){
							                $html.='X';
							            }else{
							                $html.='';  
							            }
							            $html.='
									</td>
								</tr>
							</table>
						</td>
						<td width="20%" style="font-size: 10px;">
						    <table width="100%" border="1" RULES="rows" style="padding: 1.5px;" class="table table-striped">
								<tr>
									<td width="100%" style="font-size: 10px; text-align:center">';
							            if($r3==1){
							                $html.='X';
							            }else{
							                $html.='';  
							            }
							            $html.='
							        </td>
								</tr>
							</table>
						</td>
						<td width="20%" style="font-size: 10px;">
						    <table width="100%" border="1" RULES="rows" style="padding: 1.5px;" class="table table-striped">
								<tr>
									<td width="100%" style="font-size: 10px; text-align:center">';
							            if($m3==1){
							                $html.='X';
							            }else{
							                $html.='';  
							            }
							            $html.='
							        </td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td width="40%" style="font-size: 13px;">4. Alineación</td>
						<td width="20%" style="font-size: 10px;">
						    <table width="100%" border="1" RULES="rows" style="padding: 1.5px;" class="table table-striped">
								<tr>
									<td width="100%" style="font-size: 10px; text-align:center">';
							            if($b4==1){
							                $html.='X';
							            }else{
							                $html.='';  
							            }
							            $html.='
									</td>
								</tr>
							</table>
						</td>
						<td width="20%" style="font-size: 10px;">
						    <table width="100%" border="1" RULES="rows" style="padding: 1.5px;" class="table table-striped">
								<tr>
									<td width="100%" style="font-size: 10px; text-align:center">';
							            if($r4==1){
							                $html.='X';
							            }else{
							                $html.='';  
							            }
							            $html.='
							        </td>
								</tr>
							</table>
						</td>
						<td width="20%" style="font-size: 10px;">
						    <table width="100%" border="1" RULES="rows" style="padding: 1.5px;" class="table table-striped">
								<tr>
									<td width="100%" style="font-size: 10px; text-align:center">';
							            if($m4==1){
							                $html.='X';
							            }else{
							                $html.='';  
							            }
							            $html.='
							        </td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td width="40%" style="font-size: 13px;">5. Balanceo</td>
						<td width="20%" style="font-size: 10px;">
						    <table width="100%" border="1" RULES="rows" style="padding: 1.5px;" class="table table-striped">
								<tr>
									<td width="100%" style="font-size: 10px; text-align:center">';
							            if($b5==1){
							                $html.='X';
							            }else{
							                $html.='';  
							            }
							            $html.='
									</td>
								</tr>
							</table>
						</td>
						<td width="20%" style="font-size: 10px;">
						    <table width="100%" border="1" RULES="rows" style="padding: 1.5px;" class="table table-striped">
								<tr>
									<td width="100%" style="font-size: 10px; text-align:center">';
							            if($r5==1){
							                $html.='X';
							            }else{
							                $html.='';  
							            }
							            $html.='
							        </td>
								</tr>
							</table>
						</td>
						<td width="20%" style="font-size: 10px;">
						    <table width="100%" border="1" RULES="rows" style="padding: 1.5px;" class="table table-striped">
								<tr>
									<td width="100%" style="font-size: 10px; text-align:center">';
							            if($m5==1){
							                $html.='X';
							            }else{
							                $html.='';  
							            }
							            $html.='
							        </td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td width="40%" style="font-size: 13px;">6. Suspensión</td>
						<td width="20%" style="font-size: 10px;">
						    <table width="100%" border="1" RULES="rows" style="padding: 1.5px;" class="table table-striped">
								<tr>
									<td width="100%" style="font-size: 10px; text-align:center">';
							            if($b6==1){
							                $html.='X';
							            }else{
							                $html.='';  
							            }
							            $html.='
									</td>
								</tr>
							</table>
						</td>
						<td width="20%" style="font-size: 10px;">
						    <table width="100%" border="1" RULES="rows" style="padding: 1.5px;" class="table table-striped">
								<tr>
									<td width="100%" style="font-size: 10px; text-align:center">';
							            if($r6==1){
							                $html.='X';
							            }else{
							                $html.='';  
							            }
							            $html.='
							        </td>
								</tr>
							</table>
						</td>
						<td width="20%" style="font-size: 10px;">
						    <table width="100%" border="1" RULES="rows" style="padding: 1.5px;" class="table table-striped">
								<tr>
									<td width="100%" style="font-size: 10px; text-align:center">';
							            if($m6==1){
							                $html.='X';
							            }else{
							                $html.='';  
							            }
							            $html.='
							        </td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td width="40%" style="font-size: 13px;">7. Luces</td>
						<td width="20%" style="font-size: 10px;">
						    <table width="100%" border="1" RULES="rows" style="padding: 1.5px;" class="table table-striped">
								<tr>
									<td width="100%" style="font-size: 10px; text-align:center">';
							            if($b7==1){
							                $html.='X';
							            }else{
							                $html.='';  
							            }
							            $html.='
									</td>
								</tr>
							</table>
						</td>
						<td width="20%" style="font-size: 10px;">
						    <table width="100%" border="1" RULES="rows" style="padding: 1.5px;" class="table table-striped">
								<tr>
									<td width="100%" style="font-size: 10px; text-align:center">';
							            if($r7==1){
							                $html.='X';
							            }else{
							                $html.='';  
							            }
							            $html.='
							        </td>
								</tr>
							</table>
						</td>
						<td width="20%" style="font-size: 10px;">
						    <table width="100%" border="1" RULES="rows" style="padding: 1.5px;" class="table table-striped">
								<tr>
									<td width="100%" style="font-size: 10px; text-align:center">';
							            if($m7==1){
							                $html.='X';
							            }else{
							                $html.='';  
							            }
							            $html.='
							        </td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td width="40%" style="font-size: 13px;">8. Bandas,Mangueras</td>
						<td width="20%" style="font-size: 10px;">
						    <table width="100%" border="1" RULES="rows" style="padding: 1.5px;" class="table table-striped">
								<tr>
									<td width="100%" style="font-size: 10px; text-align:center">';
							            if($b8==1){
							                $html.='X';
							            }else{
							                $html.='';  
							            }
							            $html.='
									</td>
								</tr>
							</table>
						</td>
						<td width="20%" style="font-size: 10px;">
						    <table width="100%" border="1" RULES="rows" style="padding: 1.5px;" class="table table-striped">
								<tr>
									<td width="100%" style="font-size: 10px; text-align:center">';
							            if($r8==1){
							                $html.='X';
							            }else{
							                $html.='';  
							            }
							            $html.='
							        </td>
								</tr>
							</table>
						</td>
						<td width="20%" style="font-size: 10px;">
						    <table width="100%" border="1" RULES="rows" style="padding: 1.5px;" class="table table-striped">
								<tr>
									<td width="100%" style="font-size: 10px; text-align:center">';
							            if($m8==1){
							                $html.='X';
							            }else{
							                $html.='';  
							            }
							            $html.='
							        </td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td width="40%" style="font-size: 13px;">9. Limpiadores</td>
						<td width="20%" style="font-size: 10px;">
						    <table width="100%" border="1" RULES="rows" style="padding: 1.5px;" class="table table-striped">
								<tr>
									<td width="100%" style="font-size: 10px; text-align:center">';
							            if($b9==1){
							                $html.='X';
							            }else{
							                $html.='';  
							            }
							            $html.='
									</td>
								</tr>
							</table>
						</td>
						<td width="20%" style="font-size: 10px;">
						    <table width="100%" border="1" RULES="rows" style="padding: 1.5px;" class="table table-striped">
								<tr>
									<td width="100%" style="font-size: 10px; text-align:center">';
							            if($r9==1){
							                $html.='X';
							            }else{
							                $html.='';  
							            }
							            $html.='
							        </td>
								</tr>
							</table>
						</td>
						<td width="20%" style="font-size: 10px;">
						    <table width="100%" border="1" RULES="rows" style="padding: 1.5px;" class="table table-striped">
								<tr>
									<td width="100%" style="font-size: 10px; text-align:center">';
							            if($m9==1){
							                $html.='X';
							            }else{
							                $html.='';  
							            }
							            $html.='
							        </td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td width="40%" style="font-size: 13px;">10. Bateria</td>
						<td width="20%" style="font-size: 10px;">
						    <table width="100%" border="1" RULES="rows" style="padding: 1.5px;" class="table table-striped">
								<tr>
									<td width="100%" style="font-size: 10px; text-align:center">';
							            if($b10==1){
							                $html.='X';
							            }else{
							                $html.='';  
							            }
							            $html.='
									</td>
								</tr>
							</table>
						</td>
						<td width="20%" style="font-size: 10px;">
						    <table width="100%" border="1" RULES="rows" style="padding: 1.5px;" class="table table-striped">
								<tr>
									<td width="100%" style="font-size: 10px; text-align:center">';
							            if($r10==1){
							                $html.='X';
							            }else{
							                $html.='';  
							            }
							            $html.='
							        </td>
								</tr>
							</table>
						</td>
						<td width="20%" style="font-size: 10px;">
						    <table width="100%" border="1" RULES="rows" style="padding: 1.5px;" class="table table-striped">
								<tr>
									<td width="100%" style="font-size: 10px; text-align:center">';
							            if($m10==1){
							                $html.='X';
							            }else{
							                $html.='';  
							            }
							            $html.='
							        </td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
		    </td>
		    <td width="40%">
		        <table width="100%" border="0" RULES="rows" style="padding: 1.5px;" class="table table-striped">
					<tr>
						<td width="100%" style="font-size: 10px;">Km. ';
				            $html.=$km;
				            $html.='
						</td>
					</tr>
				</table>';

			    //if($file_carro!=''){ 
	                //$fh = fopen(base_url()."uploads/servicio_carro/".$file_carro, 'r') or die("Se produjo un error al abrir el archivo");
		            $linea = base_url()."uploads/servicio_carro/".$file_carro;//fgets($fh);
		            //fclose($fh);  
		            $html.='<div style="text-align:center;margin-top:0.05cm;padding:0.1cm;">
	                            <img style="width: 220px; height: 150px; border: 2px dashed rgb(29, 175, 147); background: url('.base_url().'public/img/cark.png);background-repeat:no-repeat; background-position:center;" src="'.$linea.'" width="220" height="150" style="border:dotted 1px black;">
	                        </div>';
                //}
		     $html.='</td>
		</tr>
	</table>';
    
    //$fhfp1 = fopen(base_url()."uploads/servicio_firma1/".$file_firma1, 'r') or die("Se produjo un error al abrir el archivo");
    $lineafp1 = base_url()."uploads/servicio_firma1/".$file_firma1;//fgets($fhfp1);
    //$fhfc1 = fopen(base_url()."uploads/servicio_firma2/".$file_firma2, 'r') or die("Se produjo un error al abrir el archivo");
    $lineafc1 = base_url()."uploads/servicio_firma2/".$file_firma2;//fgets($fhfc1);
	$html.='<table width="100%" border="0" RULES="rows" style="padding: 1.5px;" class="table table-striped">';
		$html.='<tr width="100%">
                        <td width="50%" align="center">
                            <div style="text-align:center;margin-top:0.05cm;padding:0.1cm;">
                                <img style="width: 150px; height: 90px; border: 2px dashed rgb(29, 175, 147); background-position:center;" src="'.$lineafp1.'" width="150" height="90" style="border:dotted 1px black;">
                            </div>
                            RECIBE: '.$recibe.'
                        </td>
                        <td width="50%" align="center">
                            <div style="text-align:center;margin-top:0.05cm;padding:0.1cm;">
                                <img style="width: 150px; height: 90px; border: 2px dashed rgb(29, 175, 147); background-position:center;" src="'.$lineafc1.'" width="150" height="90" style="border:dotted 1px black;">
                            </div>
                            CLIENTE: '.$nombre.'
                        </td>
                    </tr>';
	$html.='</table>';

	$html.='<table width="100%" border="0" RULES="rows" style="padding: 1.5px;" class="table table-striped">
		<tr> 
			<td width="100%" style="font-size: 14px;">NOMBRE  '.$nombre.'</td>
		</tr>
		<tr> 
			<td width="100%" style="font-size: 14px;">AUTO  '.$auto.'</td>
		</tr>
		<tr> 
			<td width="100%" style="font-size: 14px;">PLACAS  '.$placas2.'</td>
		</tr>
    </table>';
$pdf->writeHTML($html, true, false, true, false, '');
//$pdf->IncludeJS('print(true);');
$pdf->Output('documento.pdf', 'I');
//$pdf->Output('files/'.$GLOBALS["carpeta"].'/facturas/'.$GLOBALS["rrfc"].'_'.$GLOBALS["Folio"].'.pdf', 'F');
