<style type="text/css">
  a {
    color: #bd1620;
  }

  .page-link {
    position: relative;
    display: block;
    padding: 0.5rem 0.75rem;
    margin-left: -1px;
    line-height: 1.25;
    color: #bd1620;
    background-color: #FFFFFF;
    border: 1px solid #DDDDDD;
  }
  .btn-nefro.btn-flat {
    background-color: transparent !important;
    color: #000000;
    border: none;
  }
  .color_hover:hover{
    border: 3px solid #00000078;
    border-radius: 14px;
  }

  .color_hover:active{
    border: 3px solid #00000078;
    border-radius: 14px;
  } 

</style>
<div class="col-lg-12">
  <div class="card">
    <div class="card-header">
      <h4 class="card-title">Reportes</h4>
    </div>
    <div class="card-body">
      <div class="card-block">
        <ul class="nav nav-tabs">
          <li class="nav-item">
          <a class="nav-link active" id="baseIcon-tab1" data-toggle="tab" aria-controls="tabIcon1" href="#tabIcon1" aria-expanded="true"><i class="fa fa-gears"></i> Productos</a>
          </li>

          <li class="nav-item">
          <a class="nav-link" id="baseIcon-tab2" data-toggle="tab" aria-controls="tabIcon2" href="#tabIcon2" aria-expanded="true"><i class="fa fa-truck"></i> Proveedores</a>
          </li>

          <li class="nav-item">
          <a class="nav-link" id="baseIcon-tab3" data-toggle="tab" aria-controls="tabIcon3" href="#tabIcon3" aria-expanded="true"><i class="fa fa-usd"></i> Compras</a>
          </li>
        </ul>
        <div class="tab-content px-1 pt-1">
          <div role="tabpanel" class="tab-pane active" id="tabIcon1" aria-expanded="true" aria-labelledby="baseIcon-tab1">
            <div class="row">
              <div class="col-md-12">
                <br>
              </div>
              <div class="col-md-6">
                <select id="nave" class="form-control">
                  <option value="0">Seleccione una nave</option>
                    <?php foreach ($naves as $item) { ?>
                        <option value="<?php echo $item->sucursalId;?>"><?php echo $item->sucursal;?></option>
                    <?php } ?>
                </select>
              </div>
              <div class="col-md-6">
                <select id="opt_prods" class="form-control">
                  <option value="0">Seleccione una opción</option>
                  <option value="1">Productos con existencia</option>
                  <option value="2">Productos con existencia minima</option>
                  <option value="3">Productos sin stock</option>
                </select>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                <br>
              </div>
              <div class="col-md-12">
                <h1>Descargar Reporte de Productos</h1>
              </div>
                <div class="col-md-3">
                  <button class="btn btn-flat btn-dark btn-export" id="export_pdf" style="color:white"> <i class="fa fa-file-pdf-o" aria-hidden="true"></i> PDF</button>
                </div>

                <div class="col-md-3">
                  <button class="btn btn-flat btn-dark btn-export" id="export_excel" style="color:white"> <i class="fa fa-file-excel-o" aria-hidden="true"></i> EXCEL</button>
                </div>
              
            </div>
          </div>
        </div>

        <div class="tab-content2 px-1 pt-1" style="display: none;">
          <div role="tabpanel" class="tab-pane" id="tabIcon2" aria-expanded="true" aria-labelledby="baseIcon-tab2">
            <div class="row">
              <div class="col-md-12">
                <br>
              </div>
              <div class="col-md-6">
                <select id="provee" class="form-control">
                  <option value="0" selected disabled>Seleccione un proveedor</option>
                  <option value="t">Todos</option>
                    <?php foreach ($provee as $p) { ?>
                        <option value="<?php echo $p->idproveedor;?>"><?php echo $p->nombre;?></option>
                    <?php } ?>
                </select>
              </div>

            </div>
            <div class="row">
              <div class="col-md-12">
                <br>
              </div>
              <div class="col-md-12">
                <h1>Descargar Reporte de Proveedores</h1>
              </div>
                <div class="col-md-6">
                  <button class="btn btn-flat btn-dark btn-export2" id="export_pdf_pro" style="color:white"> <i class="fa fa-file-pdf-o" aria-hidden="true"></i> PDF</button>
                </div>

                <div class="col-md-6">
                  <button class="btn btn-flat btn-dark btn-export2" id="export_excel_pro" style="color:white"> <i class="fa fa-file-excel-o" aria-hidden="true"></i> EXCEL</button>
                </div>
              
            </div>
          </div>
        </div>

        <div class="tab-content3 px-1 pt-1" style="display: none;">
          <div role="tabpanel" class="tab-pane" id="tabIcon3" aria-expanded="true" aria-labelledby="baseIcon-tab3">
            <div class="row">
              <div class="col-md-12">
                <br>
              </div>
              <div class="col-md-6">
                <select id="provee_oc" class="form-control">
                  <option value="0" selected disabled>Seleccione un proveedor</option>
                  <option value="t">Todos</option>
                    <?php foreach ($provee as $p) { ?>
                        <option value="<?php echo $p->idproveedor;?>"><?php echo $p->nombre;?></option>
                    <?php } ?>
                </select>
              </div>
              <div class="col-md-6">
                <select class="form-control" id="metodo">
                  <option value="0" selected disabled>Selecciona un método</option>
                  <option value="6">Todos</option>
                  <option value="1">Efectivo</option>
                  <option value="2">Tarjeta de crédito</option>
                  <option value="3">Tarjeta de débito</option>
                  <option value="4">Transferencia</option>
                  <option value="5">Cheque</option>
                </select>
              </div>
              <div class="col-md-12"><br></div>
              <div class="col-md-6">
                <label class="col-md-2">Inicio:</label>
                <div class="col-md-6">
                  <input type="date" class="form-control" id="fi">
                </div>
              </div>
              <div class="col-md-6">
                <label class="col-md-2">Fin:</label>
                <div class="col-md-6">
                  <input type="date" class="form-control" id="ff">
                </div>
              </div>
              

            </div>
            <div class="row">
              <div class="col-md-12">
                <br>
              </div>
              <div class="col-md-12">
                <h1>Descargar Reporte de Compras</h1>
              </div>
                <div class="col-md-6">
                  <button class="btn btn-flat btn-dark btn-export3" id="export_pdf_oc" style="color:white"> <i class="fa fa-file-pdf-o" aria-hidden="true"></i> PDF</button>
                </div>

                <div class="col-md-6">
                  <button class="btn btn-flat btn-dark btn-export3" id="export_excel_oc" style="color:white"> <i class="fa fa-file-excel-o" aria-hidden="true"></i> EXCEL</button>
                </div>
              
            </div>
          </div>
        </div>

      </div>
    </div>
  </div>
</div>