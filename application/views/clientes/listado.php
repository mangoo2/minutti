<style type="text/css">
    .dataTables_wrapper{
        padding: 0px;
    }
    #table-lis td{
        font-size: 14px;
    }
</style>
<section class="basic-elements">
  <div class="row">
      <div class="col-sm-12">
          <div class="content-header">Clientes</div>
      </div>
  </div>
  <div class="row">
    <div class="col-md-12">
      <div class="card">
        <div class="card-body">
          <br>
          <div class="px-3">
            <div class="row form-group">
                  <div class="col-md-3">
                      <a type="button" class="btn btn-raised btn-danger btn-min-width mr-1 mb-1 button_save" href="<?php echo base_url()?>Clientes/registrar"><i class="fa fa-save"></i> Agregar nuevo</a>
                  </div>
            </div>
            <div class="row form-group">
              <div class="col-md-12">
                <!--------//////////////-------->
                <table class="table thead-inverse" id="table_datos" style="width: 100%">
                  
                  <thead >
                    <tr>
                      <th>Nombre</th>
                      <th>Correo</th>
                      <th>Tel.Celular</th>
                      <th>Tel.Ofcina</th>
                      <th>Contacto</th>
                      <th>Quejas o sugerencias</th>
                      <th>Acciones</th>
                    </tr>
                  </thead>
                  <tbody>
                  </tbody>
                </table>
                <!--------//////////////-------->
              </div>
            </div>    
          </div>
        </div>
      </div>
    </div>
</div>


<!----------------------------------------------------------->
<div class="modal fade text-left" id="modal_quejas" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" style="display: none;" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title" id="myModalLabel1">Quejas y sugerencias</h4>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">×</span>
				</button>
			</div>
			<div class="modal-body">
				<div class="row">
          <div class="col-md-12">
						<textarea class="form-control" id="quejas" rows="6" readonly><?php echo isset($quejas) ? $quejas : ''; ?></textarea>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
			</div>
		</div>
	</div>
</div>
<!----------------------------------------------------------->
