<style type="text/css">
    :root {
        --color-green: #05a8e1;
        --color-secondary: #b9b9b9;
        --color-button: white;
        --color-black: white;
    }


    .switch-button .switch-button__checkbox {
        display: none;
    }
    .switch-button .switch-button__label {
        background-color: var(--color-secondary);
        width: 3rem;
        height: 1.5rem;
        border-radius: 1rem;
        display: inline-block;
        position: relative;
    }
    .switch-button .switch-button__label:before {
        transition: .2s;
        display: block;
        position: absolute;
        width: 1.5rem;
        height: 1.5rem;
        background-color: var(--color-button);
        content: '';
        border-radius: 50%;
        box-shadow: inset 0px 0px 0px 1px var(--color-black);
    }
    .switch-button .switch-button__checkbox:checked + .switch-button__label {
        background-color: #bd1620;
    }
    .switch-button .switch-button__checkbox:checked + .switch-button__label:before {
        transform: translateX(1.5rem);
    }
</style>
<div class="row">
  <div class="col-sm-12">
    <div class="card">
      <div class="card-header">
        <h4 class="card-title">Nuevo cliente</h4>
        <hr>
        <form class="form" method="post"  role="form" id="form_registro">
          <input type="hidden" name="clienteId" id="clienteId" value="<?php echo $clienteId;?>">
          <div class="row">
            <div class="col-md-6">
              <div class="form-group">  
                <label>Nombre de empresa:</label>
                <input type="text" class="form-control" name="nombre" value="<?php echo $nombre;?>">
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label>Correo:</label>
                <input type="email" class="form-control" name="correo" value="<?php echo $correo;?>">
              </div>
            </div>
            <div class="col-md-3">
              <div class="form-group">
                <label>Teléfono celular:</label>
                <input type="text" maxlength="10" minlength="10" class="form-control" name="celular" value="<?php echo $celular;?>">
              </div>
            </div>
            <div class="col-md-3">
              <div class="form-group">
                <label>Teléfono de oficina:</label>
                <input type="text" class="form-control" name="tel_oficina" value="<?php echo $tel_oficina;?>">
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label>Nombre de contacto:</label>
                <input type="text" class="form-control" name="contacto" value="<?php echo $contacto;?>">
              </div>
            </div>
            <div class="col-md-12">
              <div class="form-group">
                <label>Dirección:</label>
                <input type="text" class="form-control" name="direccion" value="<?php echo $direccion;?>">
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12">
              <div class="form-group">
                <label>¿Desea agregar datos fiscales?:</label>
                <div class="switch-button">
                      <input type="checkbox" name="check_fiscales" id="datos_fiscales" class="switch-button__checkbox" onclick="btn_datos_fiscales()" <?php if($check_fiscales=='on') echo 'checked'?>>
                      <label for="datos_fiscales" class="switch-button__label"></label>
                  </div>
              </div>
            </div>
          </div>
          <div class="txt_datos_fiscales" <?php if($check_fiscales!='on') echo 'style="display: none;"'?>> 
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label>Razón social:</label>
                  <input type="text" class="form-control" id="razon_social" value="<?php echo $razon_social;?>">
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label>Calle:</label>
                  <input type="text" class="form-control" id="calle" value="<?php echo $calle;?>">
                </div>
              </div>
              <div class="col-md-2">
                <div class="form-group">
                  <label>No. ext:</label>
                  <input type="text" class="form-control" id="no_ext" value="<?php echo $no_ext;?>">
                </div>
              </div>
              <div class="col-md-2">
                <div class="form-group">
                  <label>No. int:</label>
                  <input type="text" class="form-control" name="no_int" value="<?php echo $no_int;?>">
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group">
                  <label>Colonia:</label>
                  <input type="text" class="form-control" id="colonia" value="<?php echo $colonia;?>">
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group">
                  <label>Localidad:</label>
                  <input type="text" class="form-control" id="localidad" value="<?php echo $localidad;?>">
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group">
                  <label>Municipio:</label>
                  <input type="text" class="form-control" id="municipio" value="<?php echo $municipio;?>">
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group">
                  <label>Estado:</label>
                  <input type="text" class="form-control" id="estado" value="<?php echo $estado;?>">
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group">
                  <label>País:</label>
                  <input type="text" class="form-control" name="pais" value="<?php echo $pais;?>">
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group">
                  <label>Código postal:</label>
                  <input type="text" class="form-control" id="codigo_postal" value="<?php echo $codigo_postal;?>">
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group">
                  <label>RFC:</label>
                  <input type="text" class="form-control" id="rfc" value="<?php echo $rfc;?>">
                </div>
              </div>

              <div class="col-md-3">
              </div>
              
              <div class="col-md-4">
                <div class="form-group">
                  <label>Uso de CFDI:</label>
                  <select id="uso_cfdi" class="form-control">
                    <option value="0" selected="" disabled="">Seleccionar una opción</option>
                    <?php foreach ($uso_cfdi_row->result() as $item) { ?>
                      <option value="<?php echo $item->id;?>" <?php if($uso_cfdi == $item->id){ echo 'selected';}?> > <?php  echo $item->descripcion;?></option>
                    <?php } ?> 
                  </select>
                </div>
              </div>

              <div class="col-md-4">
                <div class="form-group">
                  <label>Metodo de pago:</label>
                  <select id="metodo_pago" class="form-control">
                    <option value="0" selected="" disabled="">Seleccionar una opción</option>
                    <?php foreach ($metodo_pago_row->result() as $item) { ?>
                      <!--<option value="<?php /*echo $item->id;?>" <?php if($estado==$item->EstadoId){ echo 'selected';}?> ><?php echo $item->Nombre;*/?></option>-->
                      <option value="<?php echo $item->id;?>" <?php if($metodo_pago == $item->id){ echo 'selected';}?> > <?php echo $item->descripcion;?></option>
                    <?php } ?> 
                  </select>
                </div>
              </div>

              <div class="col-md-4">
                <div class="form-group">
                  <label>Forma de pago:</label>
                  <select id="forma_pago" class="form-control">
                    <option value="0" selected="" disabled="">Seleccionar una opción</option>
                    <?php foreach ($forma_pago_row->result() as $item) { ?>
                      <!--<option value="<?php /*echo $item->id;?>" <?php if($estado==$item->EstadoId){ echo 'selected';}?> ><?php echo $item->Nombre;*/?></option>-->
                      <option value="<?php echo $item->id;?>" <?php if($forma_pago == $item->id){ echo 'selected';}?> > <?php echo $item->descripcion;?></option>
                    <?php } ?> 
                  </select>
                </div>
              </div>

              <div class="col-md-4">
                <div class="form-group">
                  <label>Condiciones de pago:</label>
                  <input type="text" id="condicion_pago" class="form-control" value="<?php echo $condicion_pago;?>">
                </div>
              </div>

              <div class="col-md-8">
                <div class="form-group">
                  <label>Régimen fiscal:</label>
                  <select id="regimen_fiscal" class="form-control">
                    <option value="0" selected="" disabled="">Seleccionar una opción</option>
                    <?php foreach ($regimen_fiscal_row->result() as $item) { ?>
                      <!--<option value="<?php /*echo $item->id;?>" <?php if($estado==$item->EstadoId){ echo 'selected';}?> ><?php echo $item->Nombre;*/?></option>-->
                      <option value="<?php echo $item->id;?>" <?php if($regimen_fiscal == $item->id){ echo 'selected';}?> > <?php echo $item->descripcion;?></option>
                    <?php } ?> 
                  </select>
                </div>
              </div>

            </div>
          </div>
          <div class="row">
            <div class="col-md-12">
              <div class="form-group">
                <label>¿Desea agregar datos bancarios?:</label>
                <div class="switch-button">
                      <input type="checkbox" name="check_bancarios" id="check_bancarios" class="switch-button__checkbox" onclick="btn_datos_bancarios()" <?php if($check_bancarios=='on') echo 'checked'?>>
                      <label for="check_bancarios" class="switch-button__label"></label>
                  </div>
              </div>
            </div>
          </div>
          <div class="row txt_datos_bancarios" <?php if($check_bancarios!='on') echo 'style="display: none;"'?>>
            <div class="col-md-12">
              <table class="table thead-inverse" id="tabla_datos_bancarios">
                <thead>
                  <tr>
                    <th>Banco</th>
                    <th>Titular o empresa</th>
                    <th>Núm. cuenta</th>
                    <th>Clave interbancaria</th>
                    <th>Núm. tarjeta</th>
                    <th></th>
                  </tr>
                </thead>
                <tbody id="tabla_bancarios">
                </tbody>
              </table>
            </div>
          </div>


          <div class="row">
            <div class="col-md-12">
              <div class="form-group">
                <label>¿Deseas agregar una unidad?:</label>
                <div class="switch-button">
                      <input type="checkbox" name="check_unidades" id="check_unidades" class="switch-button__checkbox" onclick="btn_datos_unidades()" <?php if($check_unidades=='on') echo 'checked'?>>
                      <label for="check_unidades" class="switch-button__label"></label>
                  </div>
              </div>
            </div>
          </div>
          
          <div class="row txt_datos_unidades" <?php if($check_unidades!='on') echo 'style="display: none;"'?>>
            <div class="col-md-12">
              <table class="table thead-inverse" id="tabla_datos_unidades">
                <thead>
                  <tr>
                    <th>Placas</th>
                    <th>Modelo</th>
                    <th>Año</th>
                    <th></th>
                  </tr>
                </thead>
                <tbody id="tabla_unidades">
                </tbody>
              </table>
            </div>
          </div>

          
        <div class="row">
            <div class="col-md-12">
              <div class="form-group">
                <label>¿Deseas agregar usuario y contraseña?:</label>
                <div class="switch-button">
                      <input type="checkbox" name="check_usuario" id="check_usuario" class="switch-button__checkbox" onclick="btn_datos_usuario()" <?php if($check_usuario=='on') echo 'checked' ?>>
                      <label for="check_usuario" class="switch-button__label"></label>
                  </div>
              </div>
            </div>
          </div>

        </form>  
        
        <form method="post" id="form_usuario" role="form" style="margin-bottom: 10px;">
          <div class="txt_datos_usuario" <?php if($check_usuario!='on') echo 'style="display: none;"' ?>> 

            <div class="row">
              <input type="hidden" name="usuarioid" id="usuarioid" value="<?php echo $UsuarioID; ?>">
              <div class="col-md-4">
                <label>Usuario</label>
                <input type="text" id="usuario" class="form-control" value="<?php echo $Usuario; ?>"
                  pattern="[a-z]{1,15}" required>
              </div>
              <div class="col-md-4">
                <label>Contraseña</label>
                <input type="password" id="contrasena" minlength="6" class="form-control" value="<?php echo $contrasena; ?>"
                  required>
              </div>
              <div class="col-md-4">
                <label>Perfil</label>
                <select id="perfiles" class="form-control" required>
                  <?php foreach ($perfilesrow->result() as $key) { ?>
                  <option value="<?php echo $key->perfilId;?>" <?php if($key->perfilId==$perfilId){ echo 'selected';}?>>
                    <?php echo $key->nombre;?></option>
                  <?php } ?>
                </select>
              </div>
            </div>

          </div>
        </form>

        <hr>
        <div class="row">
          <div class="col-md-12" align="center">
            <button class="btn btn-raised btn-dark btn-min-width mr-1 mb-1 btn-lg btn_registro" onclick="guarda_registro()">
                  <i class="fa fa-check-square-o"></i> Guardar
                </button>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
