<style type="text/css">
    .dataTables_wrapper{
        padding: 0px;
    }
    #table-lis td{
        font-size: 14px;
    }
</style>
<section class="basic-elements">
    <div class="row">
        <div class="col-sm-12">
            <div class="content-header">Listado de Compras</div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header" style="padding:10px;">
                    <h4 class="card-title mb-0"></h4>
                </div>
                <div class="card-body">
                    <div class="px-3">
                    	<div class="row form-group">
                            <div class="col-md-3">
                                <a type="button" class="btn btn-raised btn-danger btn-min-width mr-1 mb-1 button_save" href="<?php echo base_url()?>Productos/add"><i class="fa fa-save"></i> Agregar nuevo</a>
                            </div>
                    	</div>
                        <div class="row form-group">
                            <div class="col-md-12">
                                <table class="table thead-inverse" id="table-lis" style="width: 100%">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>CÓDIGO</th>
                                            <th>PRODUCTO</th>
                                            <th>UNIDAD</th>
                                            <th>EXISTENCIA</th>
                                            <th>ACCIONES</th>
                                        </tr>
                                    </thead>
                                    <tbody></tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

