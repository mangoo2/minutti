<style type="text/css">
  a {
    color: #bd1620;
  }

  .page-link {
    position: relative;
    display: block;
    padding: 0.5rem 0.75rem;
    margin-left: -1px;
    line-height: 1.25;
    color: #bd1620;
    background-color: #FFFFFF;
    border: 1px solid #DDDDDD;
  }
  .btn-nefro.btn-flat {
    background-color: transparent !important;
    color: #000000;
    border: none;
  }
  .color_hover:hover{
    border: 3px solid #00000078;
    border-radius: 14px;
  }

  .color_hover:active{
    border: 3px solid #00000078;
    border-radius: 14px;
  } 

</style>
<div class="col-lg-12">
  <div class="card">
    <div class="card-header">
      <h4 class="card-title">Configuraciones</h4>
    </div>
    <div class="card-body">
      <div class="card-block">
        <ul class="nav nav-tabs">
          <li class="nav-item">
          <a class="nav-link active" id="baseIcon-tab1" data-toggle="tab" aria-controls="tabIcon1" href="#tabIcon1" aria-expanded="true"><i class="fa fa-truck"></i> Tipo de camioneta</a>
          </li>
        </ul>
        <div class="tab-content px-1 pt-1">
          <div role="tabpanel" class="tab-pane active" id="tabIcon1" aria-expanded="true" aria-labelledby="baseIcon-tab1">
            <div class="row">
                <div class="col-md-6">
                    <fieldset class="form-group">
                        <label for="basicInput">Nombre</label>
                        <div class="input-group">
                            <form class="form" method="post" id="form_tipo" style="width: 100%">
                                <input type="hidden" name="id" id="idconfig" value="0">
                                <input type="text" class="form-control" name="nombre" id="nombre" style="width: 100%">
                            </form>
                            <button type="button" class="btn btn-raised btn-dark btn-min-width mr-1 mb-1 btn_tipo" onclick="add_tipo()"><i class="fa fa-save"></i></button>
                        </div>
                    </fieldset>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <table class="table thead-inverse" id="table_tipo" style="width: 100%">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Nombre</th>
                                <th>Acciones</th>
                            </tr>
                        </thead>
                        <tbody></tbody>
                    </table>
                </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>