<style type="text/css">
    a {
        color: #bd1620;
    }
</style>
<section class="basic-elements">
    <div class="row">
        <div class="col-sm-12">
            <div class="content-header"><?php echo $title_heades;?></div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title mb-0">Producto</h4>
                </div>
                <div class="card-body">
                    <div class="px-3">
                        <form class="form" id="form-configuracion">
                            <div class="row form-group">
                                <label class="col-md-2">Código</label>
                                <div class="col-md-3">
                                    <input type="hidden" name="configId" id="configId" value="0">
                                    <input type="text" name="codigo" id="codigo" class="form-control">
                                </div>
                                <label class="col-md-2">Nombre</label>
                                <div class="col-md-5">
                                    <input type="text" name="nombre" id="nombre" class="form-control">
                                </div>
                                

                            </div>
                            <div class="row form-group">
                                <label class="col-md-2">Descripción</label>
                                <div class="col-md-10">
                                    <textarea name="descripcion" id="descripcion" class="form-control"></textarea>
                                </div>
                            </div> 
                        </form>
                        <div class="row form-group">
                            <div class="col-md-10">
                                
                            </div>
                            <div class="col-md-2">
                                <button type="button" class="btn btn-raised btn-danger btn-min-width mr-1 mb-1 button_save" style="float: right;" onclick="subconfig()"><i class="fa fa-save"></i> Agregar Subconfiguración</button>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 addsubconfiguracion">
                                <div class="card-block">
                                    
                                    <ul class="nav nav-tabs addsubconfiguracion_tabs">
                                      
                                    </ul>
                                    <div class="tab-content px-1 pt-1 addsubconfiguracion_body">
                                      
                                      
                                    </div>
                                  </div>
                            </div>
                        </div>
                        <div class="row" style="display:none;">
                            <div class="col-md-12">
                                <select id="selectedinsumos_hide">
                                    <?php foreach ($productosinsumos->result() as $item) { ?>
                                        <option value="<?php echo $item->productoId;?>" data-codigo="<?php echo $item->codigo;?>"><?php echo $item->nombre;?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                      
                    	
                        <div class="row form-group">
                            <div class="col-md-12">
                                <button type="button" class="btn btn-raised btn-dark btn-min-width mr-1 mb-1 btn-lg button_save" style="float: left;" onclick="save()"><i class="fa fa-save"></i> <?php echo $title_save;?></button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>