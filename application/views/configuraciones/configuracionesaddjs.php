<script type="text/javascript" src="<?php echo base_url(); ?>public/js/configuracion.js?v=<?php echo date('YmdGi');?>"></script> 
<script type="text/javascript">
	$(document).ready(function($) {
		<?php if($productoId>0){ ?>
				obtenerdatosproducto(<?php echo $productoId;?>);	
		<?php }else{ ?>
			subconfig();
		<?php } ?>
		<?php if($viewstatus==1){ ?>
			$('.button_save').remove();
			setTimeout(function(){ 
				$('.button_save').remove();
				$('.class_row_add_items').remove();
				$('input').prop('readonly', true);
				$('textarea').prop('readonly', true);
			}, 1000);
		<?php } ?>
	});
</script>