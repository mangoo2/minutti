<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>public/css/print.css" media="print">
<div class="row">
    <div class="col-md-12">
      <h2>Corte de caja </h2>
    </div>
</div>
<style type="text/css">
    :root {
        --color-green: #cf040c;
        --color-red: #b9b9b9;
        --color-button: #fdffff;
        --color-black: #000;
    }


    .switch-button .switch-button__checkbox {
        display: none;
    }
    .switch-button .switch-button__label {
        background-color: var(--color-red);
        width: 3rem;
        height: 1.5rem;
        border-radius: 1rem;
        display: inline-block;
        position: relative;
    }
    .switch-button .switch-button__label:before {
        transition: .2s;
        display: block;
        position: absolute;
        width: 1.5rem;
        height: 1.5rem;
        background-color: var(--color-button);
        content: '';
        border-radius: 50%;
        box-shadow: inset 0px 0px 0px 1px var(--color-black);
    }
    .switch-button .switch-button__checkbox:checked + .switch-button__label {
        background-color: var(--color-green);
    }
    .switch-button .switch-button__checkbox:checked + .switch-button__label:before {
        transform: translateX(1.5rem);
    }


</style>
<!--Statistics cards Ends-->
<!--Line with Area Chart 1 Starts-->
<div class="row">
<div class="col-sm-12">
    <div class="card">
        <div class="card-body">
            <div class="card-block">
                <!--------//////////////-------->
                <br>
                <div class="row">
                    <div class="col-md-3 btn-group">
                        <label>Desde:</label>&nbsp;&nbsp;&nbsp;&nbsp;
                        <input id="txtInicio" name="txtInicio" class="form-control date-picker" size="16" type="date" />
                    </div>
                    <div class="col-md-1"></div>
                    <div class="col-md-3 btn-group">
                        <label>Hasta:</label>&nbsp;&nbsp;&nbsp;&nbsp;
                        <input id="txtFin" name="txtFin" class="form-control date-picker" size="16" type="date"/>
                    </div>
                    <div class="col-md-1"></div>
                    <div class="col-md-2">                      
                        <div class="checkbox-list">
                            <label><input type="checkbox" id="chkFecha" value="1"> Fecha actual </label>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <a href="#" class="btn btn-raised btn-dark btn-min-width" id="btnBuscar">Buscar</a>
                        <a id="btnImprimir" onclick="imprimir();"><button type="button" class="btn btn-round btn_amarillo"   ><i class="fa fa-print"></i></button></a>
                    </div>
                </div>
                <br>
                <div class="row">
                    <div class="col-md-4 btn-group">
                        <label>Tipo de venta</label>&nbsp;&nbsp;&nbsp;&nbsp;
                        <fieldset class="form-group">
                          <select class="form-control" id="tipo_venta" onchange="">  
                            <option value="0">Todas</option>
                            <option value="1">Efectivo</option>
                            <option value="2">Tarjeta de crédito</option>
                            <option value="3">Tarjeta de débito</option>
                            <!-- <option value="4">Crédito</option> -->
                          </select>
                        </fieldset>
                    </div> 
                    <div class="col-md-4">
                        <div class="btn-group">
                            <label>PRODUCTOS MÁS VENDIDOS</label> &nbsp; &nbsp; &nbsp; 
                            <div class="switch-button">
                                <!-- Checkbox -->
                                <input type="checkbox" name="switch-button" id="checkproducto" class="switch-button__checkbox">
                                <!-- Botón -->
                                <label for="checkproducto" class="switch-button__label"> </label>
                            </div>
                        </div>    
                    </div>   
                </div>
                <br>
                <div class="row" id="imprimir">
                    <div class="col-md-12" id="tbCorte">
                    </div>
                    <div class="col-md-12" id="tbCorte2">
                    </div>
                    <div class="col-md-12" id="tbgastos">
                    </div>
                    <!-- <div class="col-md-6">
                        <p style="font-size: 20px">
                            <span class="col-md-6 text-warning">Subtotal:</span>
                            <span class="col-md-4" >
                                <span class="text-warning">$</span>
                                <span id="dSubtotal">0.00</span>
                            </span>
                        </p>
                        <p style="font-size: 20px">
                            <span class="col-md-6 text-warning">Gastos:</span>
                            <span class="col-md-4" >
                                <span class="text-warning">$</span>
                                <span id="dgastos">0.00</span>
                            </span>
                        </p>
                        <p style="font-size: 20px">
                            <span class="col-md-6 text-warning">Total:</span>
                            <span class="col-md-4" >
                                <span class="text-warning">$</span>
                                <span id="dTotal">0.00</span>
                            </span>
                        </p>
                        
                        
                        <p style="font-size: 20px">
                            <span class="col-md-6 text-warning">Total de ventas:</span>
                            <span class="col-md-4" >
                                <span class="text-warning"> </span>
                                <span id="rowventas">0</span>
                            </span>
                        </p>
                        <p style="font-size: 20px">
                            <span class="col-md-6 text-warning">Total de utilidad:</span>
                            <span class="col-md-4" >
                                <span class="text-warning">$</span>
                                <span id="totalutilidades">0</span>
                            </span>
                        </p>
                    </div> -->
                    <div class="col-sm-12">
                        <div class="mas_vendindos"></div>
                    </div>
                </div>
               
                
                <!--------//////////////-------->
            </div>
        </div>
    </div>
</div>
</div>

<script type="text/javascript">
    function imprimir1(){
      window.print();
    }
    function imprimir(){
    var mywindow = window.open();
    mywindow.document.write('<html><head>');
    mywindow.document.write('<style>.tabla{width:100%;border-collapse:collapse;margin:16px 0 16px 0;}.tabla th{border:1px solid #ddd;padding:4px;background-color:#d4eefd;text-align:left;font-size:15px;}.tabla td{border:1px solid #ddd;text-align:left;padding:6px;}</style>');
    mywindow.document.write('</head><body >');
    mywindow.document.write(document.getElementById('imprimir').innerHTML);
    mywindow.document.write('</body></html>');
    mywindow.document.close(); // necesario para IE >= 10
    mywindow.focus(); // necesario para IE >= 10
    mywindow.print();
    mywindow.close();
    return true;}
</script>  