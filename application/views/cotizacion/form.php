<style type="text/css">
	:root {
		--color-green: #05a8e1;
		--color-secondary: #b9b9b9;
		--color-button: white;
		--color-black: white;
	}


	.switch-button .switch-button__checkbox {
		display: none;
	}

	.switch-button .switch-button__label {
		background-color: var(--color-secondary);
		width: 3rem;
		height: 1.5rem;
		border-radius: 1rem;
		display: inline-block;
		position: relative;
	}

	.switch-button .switch-button__label:before {
		transition: .2s;
		display: block;
		position: absolute;
		width: 1.5rem;
		height: 1.5rem;
		background-color: var(--color-button);
		content: '';
		border-radius: 50%;
		box-shadow: inset 0px 0px 0px 1px var(--color-black);
	}

	.switch-button .switch-button__checkbox:checked+.switch-button__label {
		background-color: #bd1620;
	}

	.switch-button .switch-button__checkbox:checked+.switch-button__label:before {
		transform: translateX(1.5rem);
	}
</style>

<section class="basic-elements">
	<div class="row">
		<div class="col-sm-12">
			<div class="content-header"><?php echo $title_heades; ?></div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<div class="card">
				<div class="card-body mt-2">
					<div class="px-3">
						<form class="form" id="form_cotizaciones">

							<div class="row form-group">
							<?php /* if(isset($data)){ echo json_encode($data); } */?>
								<div class="col-md-12 mt-2">
									<input type="hidden" id="IDcotizaciones" name="IDcotizaciones" class="form-control" value="<?php echo $id; ?>">
									<input type="hidden" id="usuarioId" name="usuarioId" class="form-control" value="<?php echo $usuarioId; ?>">
								</div>
							</div>

							<div class="row form-group">
								<div class="col-md-3">
									<label class="control-label">No. de cotización</label>
								</div>
								<div class="col-md-2">
									<input type="number" id="noCotizacion" name="noCotizacion" value="<?php echo isset($noCotizacion) ? $noCotizacion : '';  ?>" class="form-control" style="text-align: center;" readonly>
								</div>
								<div class="col-md-2" style="text-align: right;">
									<label class="control-label">Fecha</label>
								</div>
								<div class="col-md-3">
									<input type="date" id="fecha" name="fecha" value="<?php echo isset($fecha) ? $fecha : '';  ?>" class="form-control" readonly>
								</div>
							</div>
							<div class="row form-group">
								<div class="col-md-12">
									<label>Buscar cliente</label>
								</div>
								<div class="col-md-12">
									<input type="hidden" id="Idcliente" class="form-control" value="<?php echo isset($clienteId) ? $clienteId : 0; ?>">
									<select class="form-control" name="clienteId" id="clienteId">
										<?php /* if ($clienteId != 0) {
											echo '<option value="' . $clienteId . '">' . $nombre . '</option>';
										}else { 
											echo '<option value=""></option>';
										} */ ?>;
										<option value="0" selected="" disabled="">Seleccionar una opción</option>
									</select>
								</div>
							</div>

							<div class="row form-group">
								<div class="col-md-2">
									<label class="control-label">Correo electrónico</label>
								</div>
								<div class="col-md-4">
									<input type="email" id="correo" name="correo" value="<?php echo isset($correo) ? $correo : '';  ?>" class="form-control" readonly>
								</div>

								<div class="col-md-2">
									<label class="control-label">Teléfono</label>
								</div>
								<div class="col-md-4">
									<input type="text" maxlength="10" minlength="10" id="telefono" name="telefono" value="<?php echo isset($telefono) ? $telefono : '';  ?>" class="form-control" readonly>
								</div>
							</div>

							<div class="row form-group">
								<div class="col-md-1 mr-3">
									<label class="control-label">Productos</label>
								</div>
								<div class="switch-button col-md-1">
									<input type="checkbox" name="check_productos" id="check_productos" class="switch-button__checkbox" onclick="btn_check_productos(true)" <?php if ($check_productos == 'on') echo 'checked' ?>>
									<label for="check_productos" class="switch-button__label"></label>
								</div>
							</div>

							<div class="row form-group form_check_productos" <?php echo 'style="display: none;"' ?>>
								<div class="col-md-12">
									<table class="table thead-inverse" id="tabla_productos">
										<thead>
											<tr>
												<th>Código</th>
												<th>Producto</th>
												<th>Descripción</th>
												<th>P. Unitario</th>
												<th>Cantidad</th>
												<th>
													Total
													<input type="hidden" id="total_producto" name="total_producto" class="form-control bg-success" value="<?php echo isset($total_productos) ?  $total_productos : 0; ?>">
												</th>
												<th></th>
											</tr>
										</thead>
										<tbody id="tabla_productos_datos">
										</tbody>
									</table>
								</div>
							</div>


							<div class="row form-group">
								<div class="col-md-1  mr-3">
									<label class="control-label">Servicios</label>
								</div>
								<div class="switch-button col-md-1">
									<input type="checkbox" name="check_servicios" id="check_servicios" class="switch-button__checkbox" onclick="btn_check_servicios(true)" <?php if ($check_servicios == 'on') echo 'checked' ?>>
									<label for="check_servicios" class="switch-button__label"></label>
								</div>
							</div>

							<div class="row form-group form_check_servicios" <?php echo 'style="display: none;"' ?>>
								<div class="col-md-12">
									<table class="table thead-inverse" id="tabla_servicios">
										<thead>
											<tr>
												<th>Código</th>
												<th>Servicio</th>
												<th>Descripción</th>
												<th>Costo Servicio</th>
												<th>Cantidad</th>
												<th>
													Total
													<input type="hidden" id="total_servicio" name="total_servicio" class="form-control bg-success" value="<?php echo isset($total_servicios) ?  $total_servicios : 0; ?>">
												</th>
												<th></th>
											</tr>
										</thead>
										<tbody id="tabla_servicios_datos">
										</tbody>
									</table>
								</div>
							</div>


							<div class="row form-group">
								<div class="col-md-8">
									<div class="col-md-12">
										<label class="control-label">Clausulas / Observaciones</label>
									</div>
									<div class="col-md-12">
										<table class="table" id="tabla_clausulas">
											<thead>
												<tr>
													<th><input type="text"  class="form-control" id="concepto_x" style="width: 100%"></th>
													<th><button type="button" class="btn btn-raised btn-dark round btn-min-width mr-1 mb-1 btn-sm btn_0" onclick="add_clausulas()">Agregar</button></th>
												</tr>
											</thead>
							                <tbody id="tbody_clausulas">
							                </tbody>
						                </table>
										<!-- <textarea class="form-control" name="observaciones" id="observaciones" rows="15"><?php // echo isset($observaciones) ? $observaciones : ''; ?></textarea> -->
									</div>
								</div>

								<div class="col-md-4">

									<div class="row form-group">
										<div class="col-md-2  mr-3">
											<label class="control-label">Descuento Efectivo</label>
										</div>
										<div class="switch-button col-md-2">
											<input type="checkbox" name="check_descuento_e" id="check_descuento_e" class="switch-button__checkbox" onclick="btn_check_descuentos_e(true)" <?php if ($check_descuento_e == 'on') echo 'checked' ?>>
											<label for="check_descuento_e" class="switch-button__label"></label>
										</div>

										<div class="col-md-2  mx-3">
											<label class="control-label">Descuento Porcentaje</label>
										</div>
										<div class="switch-button col-md-2">
											<input type="checkbox" name="check_descuento_p" id="check_descuento_p" class="switch-button__checkbox" onclick="btn_check_descuentos_p(true)" <?php if ($check_descuento_p == 'on') echo 'checked' ?>>
											<label for="check_descuento_p" class="switch-button__label"></label>
										</div>
									</div>


									<div class="row px-2">
										<div class="row form-group input_check_descuento_e" <?php echo 'style="display: none;"' ?>>
											<div class="col-md-3">
												<label class="control-label">Descuento Efectivo</label>
											</div>
											<div class="col-md-9">
												<input type="number" id="descuento_e" name="descuento_e" value="<?php echo isset($descuento_e) ? $descuento_e : 0; ?>" oninput="calcularSubtotal()" class="form-control" >
											</div>
										</div>

										<div class="row form-group input_check_descuento_p" <?php echo 'style="display: none;"' ?>>
											<div class="col-md-3">
												<label class="control-label">Descuento Porcentaje</label>
											</div>
											<div class="col-md-9">
												<input type="number" id="descuento_p" name="descuento_p" value="<?php echo isset($descuento_p) ? $descuento_p : 0; ?>" oninput="calcularSubtotal()" class="form-control" >
											</div>
										</div>
									</div>



									<div class="row form-group">
										<div class="col-md-3">
											<label class="control-label">Subtotal</label>
										</div>
										<div class="col-md-9">
											<input type="text" id="subtotal" name="subtotal" value="<?php echo isset($subtotal) ? $subtotal : ''; ?>" class="form-control" readonly>
										</div>
									</div>

									<div class="row form-group">
										<div class="col-md-3">
											<label class="control-label">IVA</label>
										</div>
										<div class="col-md-9">
											<input type="text" id="iva" name="iva" value="<?php echo isset($iva) ? $iva : ''; ?>" class="form-control" readonly>
										</div>
									</div>

									<div class="row form-group">
										<div class="col-md-3">
											<label class="control-label">Total</label>
										</div>
										<div class="col-md-9">
											<input type="text" id="total" name="total" value="<?php echo isset($total) ? $total : ''; ?>" class="form-control" readonly>
										</div>
									</div>

								</div>
							</div>

					</div>
				</div>
			</div>

			<div class="row form-group">
				<div class="col-md-12">
					<button type="button" class="btn btn-raised btn-dark btn-min-width mx-2 mb-1 btn-lg button_save" style="float: left;" onclick="save()"><i class="fa fa-save"></i> <?php echo $title_save; ?></button>
					<a href="<?php echo base_url(); ?>Cotizacion" type="button" class="btn btn-raised btn-dark btn-min-width mr-1 mb-1 btn-lg"><i class="fa fa-mail-reply"></i> Regresar</a>
				</div>
			</div>

			</form>

		</div>
	</div>
	</div>
	</div>
</section>