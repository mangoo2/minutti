<style type="text/css">
	:root {
		--color-green: #05a8e1;
		--color-secondary: #b9b9b9;
		--color-button: white;
		--color-black: white;
	}

	.dataTables_wrapper {
		padding: 0px;
	}

	#table-lis td {
		font-size: 14px;
	}


	.switch-button .switch-button__checkbox {
		display: none;
	}

	.switch-button .switch-button__label {
		background-color: var(--color-secondary);
		width: 3rem;
		height: 1.5rem;
		border-radius: 1rem;
		display: inline-block;
		position: relative;
	}

	.switch-button .switch-button__label:before {
		transition: .2s;
		display: block;
		position: absolute;
		width: 1.5rem;
		height: 1.5rem;
		background-color: var(--color-button);
		content: '';
		border-radius: 50%;
		box-shadow: inset 0px 0px 0px 1px var(--color-black);
	}

	.switch-button .switch-button__checkbox:checked+.switch-button__label {
		background-color: #bd1620;
	}

	.switch-button .switch-button__checkbox:checked+.switch-button__label:before {
		transform: translateX(1.5rem);
	}
</style>

<section class="basic-elements">
	<div class="row">
		<div class="col-sm-12">
			<div class="content-header">Listado de Cotizaciones</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<div class="card">
				<div class="card-header" style="padding:10px;">
					<h4 class="card-title mb-0"></h4>
				</div>
				<div class="card-body">
					<div class="px-3">
						<div class="row form-group">
							<div class="col-md-3">
								<a type="button" class="btn btn-raised btn-danger btn-min-width mr-1 mb-1 button_save" href="<?php echo base_url() ?>Cotizacion/add"><i class="fa fa-save"></i> Nueva cotización</a>
							</div>
						</div>
						<div class="row form-group">
							<div class="col-md-12">
								<table class="table thead-inverse" id="table-list" style="width: 100%">
									<thead>
										<tr>
											<th># COTIZACIÓN</th>
											<th>FECHA DE CREACIÓN</th>
											<th>USUARIO</th>
											<th>PRODUCTOS O SERVICIOS</th>
											<th>ESTATUS</th>
											<th>ACCIONES</th>
										</tr>
									</thead>
									<tbody></tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<!----------------------------------------------------------->
<div class="modal fade text-left" id="modal_ProcutoServicio" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" style="display: none;" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title" id="myModalLabel1">Productos o Servicios</h4>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">×</span>
				</button>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-md-12">
						<div class="table_data_product">
							<table class="table thead-inverse" id="tabla_ProcutoServicio" width="100%">
								<thead>
									<tr>
										<th>Codigo</th>
										<th>Producto o Servicio</th>
										<th>P. Unitario $</th>
										<th>Cantidad</th>
										<th>Total $</th>
									</tr>
								</thead>
								<tbody class="list_ProcutoServicio">
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn grey btn-outline-secondary" data-dismiss="modal">Cerrar</button>
			</div>
		</div>
	</div>
</div>


<!----------------------------------------------------------->
<div class="modal fade text-left" id="aceptar_registro" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title" id="myModalLabel1">Aceptación de cotización</h4>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-md-6">
						<fieldset class="form-group">
							<label>Fecha:</label>
							<input type="date" readonly class="form-control" id="fecha" value="<?php echo $fecha ?>">
						</fieldset>
					</div>

					<div class="col-md-6">
						<div class="form-group">
							<label class="control-label">Método de pago:</label>
							<select class="form-control" id="mpago">
								<option value="1">Efectivo</option>
								<option value="2">Tarjeta de crédito</option>
								<option value="3">Tarjeta de débito</option>
							</select>
						</div>
					</div>
				</div>

				<br>
				<h4>Productos o Servicios</h4>
				<div class="row">
					<div class="col-md-12">
						<div class="tabla_productos"></div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12 subtotal_p" align="right">
						<h5><b>SUBTOTAL: $<span class="subtotal_p_total"></span></b></h5>
					</div>
					<div class="col-md-12 iva_p" align="right">
						<h5><b>IVA: $<span class="iva_p_total"></span></b></h5>
					</div>
					<div class="col-md-12 total_p" align="right">
						<input type="hidden" id="descuento_c">
						<input type="hidden" id="descuento_p">
						<h5><b>TOTAL: $<span class="total_p_total"></span></b></h5>
						<span class="total_p_total2" style="display: none;">0</span>
						<span class="cantdescuento" style="display: none;">0</span>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12" align="right">
						<div class="form-group">
							<label>¿Desea facturar?</label>
							<div class="switch-button">
								<input type="checkbox" id="factura_x" class="switch-button__checkbox">
								<label for="factura_x" class="switch-button__label"></label>
							</div>
						</div>
					</div>
				</div>

				<!-- <div class="row">
					<div class="col-md-8" align="right">
						<label>Descuento %:</label>
					</div>
					<div class="col-md-4">
						<fieldset class="form-group">
							<input type="number" class="form-control" id="descuento" oninput="descuento_cotizacion()">
						</fieldset>
					</div>
				</div> -->

				<div class="row txt_servicio">
					<div class="col-md-12" align="right">
						<div class="form-group">
							<label>¿Desea agendar servicio?</label>
							<div class="switch-button">
								<input type="checkbox" name="check_servicio" id="check_servicio" class="switch-button__checkbox" onclick="btn_datos_servicio()" <?php if( isset($check_servicio) && $check_servicio == 'on') echo 'checked'?>>
								<label for="check_servicio" class="switch-button__label"></label>
							</div>
						</div>
					</div>
				</div>

				<div class="row txt_datos_servicio" <?php /* if ($check_servicio != 'on') echo 'style="display: none;"' */ ?>>
					<div class="col-md-6">
						<div class="form-group">
							<label>Fecha:</label>
							<input type="date" class="form-control" id="fecha_servicio" value="<?php echo isset($fecha_servicio) ? $fecha_servicio : ''; ?>">
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label>Hora:</label>
							<input type="time" class="form-control" id="hora_servicio">
						</div>
					</div>

					<div class="col-md-12">
						<div class="input-group">
							<label>Unidad:</label>
						</div>
					</div>

					<div class="col-md-12">
						<div class="input-group">
							<select class="form-control" id="unidadSelect">
								<option value="0" selected="" disabled="">Seleccionar una opción</option>
								<?php /* foreach ($unidadesrow->result() as $unidad) { ?>
									<option value="<?php echo $unidad->unidadId; ?>"><?php echo $unidad->unidad; ?></option>
								<?php } */ ?>
							</select>
							<button type="button" id="btn_select_unidad" class="btn btn-raised btn-danger btn-min-width mr-1 mb-1" onclick="modal_unidad()"><i class="fa fa-plus"></i></button>
						</div>
					</div>


				</div>


			</div>

			<div class="modal-footer">
				<button type="button" class="btn btn-success" onclick="aceptar_cotizacion()">Aceptar</button>
				<button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
			</div>
		</div>
	</div>
</div>


<!----------------------------------------------------------->
<div class="modal fade text-left" id="aceptar_cotizacion" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
	<div class="modal-dialog modal-sm" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title" id="myModalLabel1">Mensaje de confirmación</h4>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				¿Desea <b>aceptar</b> la cotización?
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-success" onclick="aceptar_registro()">Aceptar</button>
				<button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
			</div>
		</div>
	</div>
</div>

<!----------------------------------------------------------->

<div class="modal fade text-left" id="modal_unidad" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title" id="myModalLabel1">Unidad</h4>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">×</span>
				</button>
			</div>

			<div class="modal-body">
				<div class="row">
					<div class="col-md-12">
						<fieldset class="form-group">

							<form class="form" method="post" id="form_unidad" style="width: 100%">

								<div class="form-group col-md-3 p-0">
									<label>Placas</label>
									<input type="hidden" name="unidadId" id="unidad" value="0">
									<input type="text" class="form-control" name="placas" id="placas" style="width: 100%">
								</div>

								<div class="form-group col-md-3 p-0">
									<label>Modelo</label>
									<input type="text" class="form-control" name="modelo" id="modelo" style="width: 100%">
								</div>

								<div class="form-group col-md-3 p-0">
									<label>Año</label>
									<input type="number" class="form-control" name="ano" id="ano" style="width: 100%">
								</div>

							</form>
							<button type="button" class="btn btn-raised btn-dark btn-min-width ml-3 mt-3 btn_unidad" onclick="add_unidad()"><i class="fa fa-save"></i></button>

						</fieldset>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<table class="table thead-inverse" id="table_unidades" style="width: 100%">
							<thead>
								<tr>
									<th>Placas</th>
									<th>Modelo</th>
									<th>Año</th>
									<th>Acciones</th>
								</tr>
							</thead>
							<tbody></tbody>
						</table>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
			</div>
		</div>
	</div>
</div>