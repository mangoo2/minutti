<section class="basic-elements">
	<div class="row">
		<div class="col-sm-12">
			<div class="content-header"><?php echo $title_heades; ?></div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<div class="card">
				<div class="card-body mt-2">
					<div class="px-3">
						<form class="form" id="form-nueva-cotizacion">

							<div class="row form-group">
								<div class="col-md-4 mt-2">
									<input type="text" id="registro" class="form-control text-center" value="<?php echo $reg; ?>" disabled>
									<input type="hidden" id="cotizaId" class="form-control" value="<?php echo $id_n = isset($id_n) ? $id_n : "0"; ?>">
								</div>
								<div class="col-md-2"></div>
								<div class="col-md-6 text-right mt-2">
									<div class="col-md-6">
										<label>Seleccionar vehículo: </label>
									</div>
									<div class="col-md-6">
										<input type="hidden" id="auxvehiculo" class="form-control" value="<?php echo $vehiculo = isset($vehiculo) ? $vehiculo : "0"; ?>">
										<select id="vehiculo" class="form-control" onchange="get_vehicle();">
											<option value="0">Seleccionar una opción</option>
											<option value="1">Sprinter</option>
											<option value="2">Crafter</option>
										</select>
									</div>
								</div>

								<div class="col-md-2 mt-2">
									<div class="col-md-6 pl-0">
										<label>Usuario: </label>
									</div>
									<div class="col-md-6">
										<input type="hidden" id="userId" class="form-control" value="<?php echo $userId; ?>">
										<label><?php echo $user; ?></label>
									</div>
								</div>
								<div class="col-md-4"></div>
								<div class="col-md-6 text-right mt-2">
									<div class="col-md-6">
										<label>Seleccionar equipamiento: </label>
									</div>
									<div class="col-md-6">
										<input type="hidden" id="auxequipo" class="form-control" value="<?php echo $equipamiento = isset($equipamiento) ? $equipamiento : "0"; ?>">
										<select id="equipo" class="form-control" onchange="get_equip();">
											<option value="0">Seleccionar una opción</option>
											<!---
											<option value="1">Turismo alto</option>
											<option value="2">Turismo medio</option>
											<option value="3">Transporte de personal</option>
											<option value="4">Transporte publico</option>
-->
										</select>
									</div>
								</div>

								<div class="col-md-4 mt-2">
									<div class="col-md-2 pl-0">
										<label>Folio: </label>
									</div>
									<div class="col-md-10 p-0">
										<input type="text" id="folio" class="form-control text-center" value="<?php echo $folio ?>" disabled>
									</div>
								</div>
								<div class="col-md-8"></div>

								<div class="col-md-12 mt-2">
									<div class="col-md-2 pl-0">
										<label>Encabezado: </label>
									</div>
									<div class="col-md-10 p-0">
										<textarea class="form-control" id="encabezado" rows="2"><?php echo $encabezado = isset($encabezado) ? $encabezado : ""; ?></textarea>
									</div>
								</div>
							</div>
						</form>

						<div class="row form-group col-md-12 mt-3">


							<div class="col-md-12 display-none" id="div_select">
								<div class="col-md-11" >
									<select id="select_equip" class="form-control">
									</select>
								</div>

								<div class="col-md-1text-center">
									<button type="button" class="btn btn-raised btn-danger btn-min-width mb-1" onclick="add_config(0)"><i class="fa fa-plus"></i></button>
								</div>
							</div>

							<div class="col-md-12 mt-3 px-4">
								<table class="table thead-inverse" id="tabla_equipo" width="100%">
								</table>
							</div>
						</div>


						<div class="row form-group col-md-12 mt-3">
							<div class="col-md-12">
								<button type="button" class="btn btn-raised btn-dark btn-min-width ml-2 mb-1 btn-lg button_save" style="float: left;" onclick="save()"><i class="fa fa-save"></i> <?php echo $title_save; ?></button>
							</div>
						</div>

					</div>
				</div>
			</div>
		</div>
	</div>
</section>