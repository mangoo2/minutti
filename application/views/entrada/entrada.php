<section class="basic-elements">
    <div class="row">
        <div class="col-sm-12">
            <div class="content-header">Entrada de almacén</div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="">
                <div class="">
                    <div class="px-3">
                        <div class="row">
                            <div class="col-md-4 ">
                                <div class="card" style="background-image: linear-gradient( 45deg , #000000, #ff0404); height: 216px;">
                                    <a style="color:#ffffff" class="card-body card-block buttom_item" href="<?php echo base_url();?>Entrada/listadocompras">
                                        <div class="row">
                                            <div class="col-md-12 text-center" >
                                                <br>
                                                <i class="fa fa-truck" style="font-size: 48px;"></i>
                                                <br>
                                                <br>
                                                <h5>Listado de orden de Compra</h5>
                                                <br>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-4 ">
                                <div class="card" style="background-image: linear-gradient( 45deg , #000000, #ff0404); height: 216px;">
                                    <a style="color:#ffffff" class="card-body card-block buttom_item" href="<?php echo base_url();?>Entrada/requisicion">
                                        <div class="row">
                                            <div class="col-md-12 text-center" >
                                                <br>
                                                <i class="fa fa-truck" style="font-size: 48px;"></i>
                                                <br>
                                                <br>
                                                <h5>Ingresar Orden de Compra</h5>
                                                <br>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-4 ">
                                <div class="card" style="background-image: linear-gradient( 45deg , #000000, #ff0404); height: 216px;">
                                    <a style="color:#ffffff" class="card-body card-block buttom_item" href="<?php echo base_url();?>Entrada/retorno_material">
                                        <div class="row">
                                            <div class="col-md-12 text-center" style="min-height: 104px;">
                                                <br>
                                                <i class="fa fa-level-up" style="font-size: 48px;"></i>
                                                <br>
                                                <br>
                                                <h5>Retorno de material</h5>
                                                <br>
                                                <br>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>

                            
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</section>