<style type="text/css">
    :root {
        --color-green: #05a8e1;
        --color-secondary: #b9b9b9;
        --color-button: white;
        --color-black: white;
    }


    .switch-button .switch-button__checkbox {
        display: none;
    }
    .switch-button .switch-button__label {
        background-color: var(--color-secondary);
        width: 3rem;
        height: 1.5rem;
        border-radius: 1rem;
        display: inline-block;
        position: relative;
    }
    .switch-button .switch-button__label:before {
        transition: .2s;
        display: block;
        position: absolute;
        width: 1.5rem;
        height: 1.5rem;
        background-color: var(--color-button);
        content: '';
        border-radius: 50%;
        box-shadow: inset 0px 0px 0px 1px var(--color-black);
    }
    .switch-button .switch-button__checkbox:checked + .switch-button__label {
        background-color: #bd1620;
    }
    .switch-button .switch-button__checkbox:checked + .switch-button__label:before {
        transform: translateX(1.5rem);
    }
</style>
<section class="basic-elements">
    <div class="row">
        <div class="col-sm-12">
            <div class="content-header">Ingresar Orden de compra</div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title mb-0"></h4>
                </div>
                <div class="card-body">
                    <form method="post" id="form_data">
                        <div class="px-3">
                            <input type="hidden" name="idcompra" id="idcompra" value="<?php echo $idcompra ?>">
                            <div class="row form-group">
                                <label class="col-md-2">Proveedor</label>
                                <div class="col-md-4">
                                    <select class="form-control" name="idproveedor" id="idproveedor">
                                       <?php if($idcompra!=0){
                                                echo '<option value="'.$idproveedor.'">'.$supplir.'</option>';
                                            }
                                        ?>; 
                                    </select>
                                </div>
                                <label class="col-md-2">No. de O.C:</label>
                                <div class="col-md-4">
                                    <input type="text" class="form-control" value="<?php echo $no_oc?>" readonly>
                                </div>
                            </div>
                            <div class="row form-group">
                                <label class="col-md-2">Fecha de compra</label>
                                <div class="col-md-4">
                                    <input type="date" class="form-control" name="fecha_compra" value="<?php echo $fecha_compra ?>" readonly>
                                </div>
                                <label class="col-md-2">Fecha de surtimiento:</label>
                                <div class="col-md-4">
                                    <input type="date" class="form-control" name="fecha_surtimiento" value="<?php echo $fecha_surtimiento ?>">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <input type="hidden" id="idbancariox" value="<?php echo $idbancario ?>">
                                        <label>Datos bancarios de proveedor</label>
                                        <div class="datos_bancarios"></div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Compra a crédito:</label>
                                        <div class="switch-button">
                                            <input type="checkbox" name="tipo_compra" id="tipo_compra" class="switch-button__checkbox" <?php  if($tipo_compra=='on') echo 'checked'?>>
                                            <label for="tipo_compra" class="switch-button__label"></label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Método de pago:</label>
                                        <select class="form-control" name="metodo_pago" id="metodo_pago">
                                            <option value="0" selected="" disabled="">Selecciona una opción</option>
                                            <option value="1" <?php if($metodo_pago==1) echo 'selected' ?>>Efectivo</option>
                                            <option value="2" <?php if($metodo_pago==2) echo 'selected' ?>>Tarjeta de crédito</option>
                                            <option value="3" <?php if($metodo_pago==3) echo 'selected' ?>>Tarjeta de débito</option>
                                            <option value="4" <?php if($metodo_pago==4) echo 'selected' ?>>Transferencia</option>
                                            <option value="5" <?php if($metodo_pago==5) echo 'selected' ?>>Cheque</option>
                                        </select>
                                    </div>
                                </div>        
                            </div>           
                            <!--
                            <div class="row form-group">
                                <label class="col-md-2">Recibe</label>
                                <div class="col-md-4">
                                    <select class="form-control" name="personalId" id="personalId">
                                        <?php // if($idcompra!=0){
                                              //  echo '<option value="'.$personalId.'">'.$employee.'</option>';
                                            //}
                                        ?>; 
                                    </select>
                                </div>
                            </div>
                            -->
                            <hr>
                            <div class="row form-group">
                                <div class="col-md-12">
                                    Ingreso de productos de O.C:
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col-md-12">
                                    <table class="table thead-inverse" id="table_product">
                                        <thead>
                                            <tr>
                                                <th>Producto</th>
                                                <th style="width: 11%;">Cantidad</th>
                                                <th style="width: 11%;">Stock</th>
                                                <th style="width: 12%;">Costo Unitario</th>
                                                <th style="width: 12%;">Subtotal</th>
                                                <th style="width: 21%;">Código pieza</th>
                                                <th style="width: 18%;">Unidad</th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <tbody id="tbody_data">
                                        </tbody>
                                    </table>
                                    <div align="center" class="btn_agregar_pro"> 
                                    <button type="button" class="btn btn-dark" onclick="btn_agregar_pro()"><i class="fa fa-plus"></i> Agregar</button>
                                </div>
                                </div>
                            </div>  
                            <div class="row form-group">
                                <div class="col-md-2">
                                    <h3>Total: $</h3>
                                </div>    
                                <div class="col-md-3">                                    
                                    <input type="text" class="form-control" name="total" id="total" value="<?php echo $total ?>" readonly>
                                </div>
                            </div> 
                            <div class="row form-group">
                                <div class="col-md-12">
                                    <label>Comentarios:</label>
                                    <textarea id="comentarios" class="form-control"><?php echo $comentarios ?></textarea>
                                </div>
                            </div>

                            <div class="row form-group">
                                <div class="col-md-12">
                                    <button type="button" class="btn btn-dark btn-lg btn_registro" onclick="save_data()"><i class="fa fa-save"></i> Procesar compra</button>
                                    <a href="<?php echo base_url() ?>Entrada" type="button" class="btn btn-danger btn-lg" style="color: white"><i class="fa fa-angle-double-left"></i> Regresar </a>
                                </div>

                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>