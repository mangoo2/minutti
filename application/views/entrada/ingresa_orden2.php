<style type="text/css">
    :root {
        --color-green: #05a8e1;
        --color-secondary: #b9b9b9;
        --color-button: white;
        --color-black: white;
    }


    .switch-button .switch-button__checkbox {
        display: none;
    }
    .switch-button .switch-button__label {
        background-color: var(--color-secondary);
        width: 3rem;
        height: 1.5rem;
        border-radius: 1rem;
        display: inline-block;
        position: relative;
    }
    .switch-button .switch-button__label:before {
        transition: .2s;
        display: block;
        position: absolute;
        width: 1.5rem;
        height: 1.5rem;
        background-color: var(--color-button);
        content: '';
        border-radius: 50%;
        box-shadow: inset 0px 0px 0px 1px var(--color-black);
    }
    .switch-button .switch-button__checkbox:checked + .switch-button__label {
        background-color: #bd1620;
    }
    .switch-button .switch-button__checkbox:checked + .switch-button__label:before {
        transform: translateX(1.5rem);
    }
</style>
<section class="basic-elements">
    <div class="row">
        <div class="col-sm-12">
            <div class="content-header">Ingresar Orden de compra</div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title mb-0"></h4>
                </div>
                <div class="card-body">
                    <form method="post" id="form_data">
                        <div class="px-3">
                            <input type="hidden" name="idcompra" id="idcompra" value="<?php echo $idcompra ?>">
                            <input type="hidden" id="requsicionIdx" value="<?php echo $requsicionId ?>">
                            <input type="hidden" id="destinox" value="<?php echo $destino ?>">
                            <div class="row form-group">
                                <label class="col-md-2">Proveedor</label>
                                <div class="col-md-4">
                                    <select class="form-control" name="idproveedor" id="idproveedor">
                                       <?php if($idcompra!=0){
                                                echo '<option value="'.$idproveedor.'">'.$supplir.'</option>';
                                            }
                                        ?>; 
                                    </select>
                                </div>
                                <label class="col-md-2">No. de O.C:</label>
                                <div class="col-md-4">
                                    <input type="text" class="form-control" value="<?php echo $no_oc?>" readonly>
                                </div>
                            </div>
                            <div class="row form-group">
                                <label class="col-md-2">Fecha de compra</label>
                                <div class="col-md-4">
                                    <input type="date" class="form-control" name="fecha_compra" value="<?php echo $fecha_compra ?>" readonly>
                                </div>
                                <label class="col-md-2">Fecha de surtimiento:</label>
                                <div class="col-md-4">
                                    <input type="date" class="form-control" name="fecha_surtimiento" value="<?php echo $fecha_surtimiento ?>">
                                </div>
                            </div>   
                            <div class="row form-group">
                                <label class="col-md-2">Recibe</label>
                                <div class="col-md-4">
                                    <select class="form-control" name="personalId" id="personalId">
                                        <?php if($idcompra!=0){
                                                echo '<option value="'.$personalId.'">'.$employee.'</option>';
                                            }
                                        ?>; 
                                    </select>
                                </div>
                                <?php if($requsicionId!=0){ ?>
                                <label class="col-md-2">Destino:</label>
                                <div class="col-md-4">
                                    <select class="form-control" name="destino" id="destino" disabled="">
                                        <option value="0" disabled="" selected="">Seleccione una opción</option>
                                        <?php foreach ($sucursalrow->result() as $item) { ?>
                                            <option value="<?php echo $item->sucursalId;?>" <?php if($item->sucursalId==$destino) echo 'selected'?>><?php echo $item->sucursal;?></option>
                                        <?php } ?> 
                                    </select>
                                </div>
                                <?php } ?>
                            </div>
                            <hr>
                            <div class="row form-group">
                                <div class="col-md-12">
                                    Ingreso de productos de O.C:
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col-md-12">
                                    <table class="table thead-inverse" id="table_product">
                                        <thead>
                                            <tr>
                                                <th>Cantidad</th>
                                                <th>Nave</th>
                                                <th>Cantidad Recibida</th>
                                                <th>Diferencia</th>
                                                <th>Producto</th>
                                                <th>Código pieza</th>
                                                <th>Unidad</th>
                                                <th>Pieza</th>
                                                <th></th> 
                                            </tr>
                                        </thead>
                                        <tbody id="tbody_data">
                                        </tbody>
                                    </table>
                                </div>
                            </div>   
                            <div class="row form-group">
                                <div class="col-md-12">
                                    <label>Comentarios:</label>
                                    <textarea id="comentarios" class="form-control"><?php echo $comentarios ?></textarea>
                                </div>
                            </div>

                            <div class="row form-group">
                                <div class="col-md-12">
                                    <button type="button" class="btn btn-dark btn-lg btn_registro" onclick="save_data()"><i class="fa fa-save"></i> Ingresar a inventario</button>
                                    <a href="<?php echo base_url() ?>Entrada/listadocompras" type="button" class="btn btn-danger btn-lg" style="color: white"><i class="fa fa-angle-double-left"></i> Regresar </a>
                                </div>

                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- -->
<!--
<section class="basic-elements">
    <div class="row">
        <div class="col-sm-12">
            <div class="content-header">Ingresar Orden de compra</div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title mb-0"></h4>
                </div>
                <div class="card-body">
                    <div class="px-3">
                        <div class="row form-group">
                            <label class="col-md-2">Proveedor</label>
                            <div class="col-md-4">
                                <input type="text" class="form-control">
                            </div>
                            <label class="col-md-2">No. de O.C:</label>
                            <div class="col-md-4">
                                <input type="text" class="form-control">
                            </div>
                        </div>
                        <div class="row form-group">
                            <label class="col-md-2">Fecha de compra</label>
                            <div class="col-md-4">
                                <input type="date" class="form-control" readonly>
                            </div>
                            <label class="col-md-2">Fecha de surtimiento:</label>
                            <div class="col-md-4">
                                <input type="date" class="form-control">
                            </div>
                        </div>
                        <div class="row form-group">
                            <label class="col-md-2">Recibe</label>
                            <div class="col-md-4">
                                <select class="form-control"></select>
                            </div>
                            
                        </div>

                        <div class="row form-group">
                            <div class="col-md-12">
                                <hr>
                            </div>
                        </div>

                        
                        <div class="row form-group">
                            <div class="col-md-12">
                                Ingreso de productos de O.C:
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-md-12">
                                <table class="table thead-inverse">
                                    <thead>
                                        <tr>
                                            <th>Cantidad</th>
                                            <th>Nave</th>
                                            <th>Cantidad Recibida</th>
                                            <th>Diferencia</th>
                                            <th>Código pieza</th>
                                            <th>Unidad</th>
                                            <th>Pieza</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>25</td>
                                            <td>
                                                <input type="number" name="nombre" id="nombre" class="form-control" placeholder="Nave 1" style="width: 95px;"> 
                                                <input type="number" name="nombre" id="nombre" class="form-control" placeholder="Nave 2" style="width: 95px;">
                                                <input type="number" name="nombre" id="nombre" class="form-control" placeholder="Nave 3" style="width: 95px;">
                                                <input type="number" name="nombre" id="nombre" class="form-control" placeholder="Nave 4" style="width: 95px;">
                                            </td>
                                            <td>20</td>
                                            <td>5</td>
                                            <td>AB01</td>
                                            <td>Pieza</td>
                                            <td>Pijas 1/2 x 1/2</td>
                                        </tr>
                                        
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-md-12">
                                <label>Comentarios:</label>
                                <textarea class="form-control"></textarea>
                            </div>
                        </div>

                        <div class="row form-group">
                            <div class="col-md-12">
                                <button type="button" class="btn btn-dark btn-lg button_save" style="float: left;" onclick="save()"><i class="fa fa-save"></i> Procesar orden</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
-->