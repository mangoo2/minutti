<style type="text/css">
    #table-data-row td{
        font-size: 15px;
    }
</style>
<section class="basic-elements">
    <div class="row">
        <div class="col-sm-12">
            <div class="content-header">Requisiciones para compras</div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title mb-0"></h4>
                </div>
                <div class="card-body">
                    <div class="px-3">
                        <div class="row form-group">
                            <div class="col-md-3">
                                <a type="button" class="btn btn-raised btn-danger btn-min-width mr-1 mb-1 button_save" href="<?php echo base_url()?>Entrada/ingresa_orden"><i class="fa fa-save"></i> Realizar compra sin requisición</a>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <table class="table thead-inverse" id="table-data-row">
                                    <thead >
                                        <tr>
                                            <th>#</th>
                                            <th>Solicitante</th>
                                            <th>Destino</th>
                                            <th>Fecha</th>
                                            <th>PERSONAL QUE INGRESO</th>
                                            <th>Estatus</th>
                                            <th style="width: 84px;"></th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>