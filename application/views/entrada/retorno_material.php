<section class="basic-elements">
    <div class="row">
        <div class="col-sm-12">
            <div class="content-header">Retorno de material</div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title mb-0"></h4>
                </div>
                <div class="card-body">
                    <div class="px-3">
                        <form class="form" id="form-retorno">
                            <input type="hidden" id="retornoId" value="0">
                            <div class="row form-group">
                                <label class="col-md-2">Quien retorna:</label>
                                <div class="col-md-4">
                                    <select class="form-control" id="personalId" name="personalId">
                                        
                                    </select>
                                </div>
                                <label class="col-md-2">Fecha de retorno:</label>
                                <div class="col-md-4">
                                    <input type="date" class="form-control" id="fecha_retorno" name="fecha_retorno">
                                </div>
                            </div>
                        </form>
                        <div class="row form-group">
                            <label class="col-md-2">Cantidad:</label>
                            <div class="col-md-3">
                                <input type="number" class="form-control" id="s_cantidad">
                            </div>
                            <label class="col-md-2">Producto:</label>
                            <div class="col-md-3">
                                <select class="form-control" id="s_producto"></select>
                            </div>
                            <div class="col-md-2">
                                <button type="button" class="btn btn-raised btn-danger button_save" style="float: right;" onclick="addinfo()"><i class="fa fa-plus"></i> Agregar</button>
                            </div>
                        </div>
                        
                        <div class="row form-group">
                            <div class="col-md-12">
                                <table class="table thead-inverse" id="table-productos">
                                    <thead>
                                        <tr>
                                            <th>Cantidad</th>
                                            <th>Cógido pieza</th>
                                            <th>Pieza</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody class="table-productos-tb">
                                        
                                        
                                        
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        

                        <div class="row form-group">
                            <div class="col-md-12">
                                <button type="button" class="btn btn-raised btn-dark btn-lg button_save" style="float: right;" onclick="save()"><i class="fa fa-save"></i> Procesar orden</button>
                                <a href="<?php echo base_url();?>Entrada" type="button" class="btn btn-raised btn-danger btn-lg">Cancelar</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>