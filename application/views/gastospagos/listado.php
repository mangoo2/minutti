<div class="row">
  <div class="col-md-4">
    <h2>Listado de gastos</h2>
  </div>
  <div class="col-md-3">
    <fieldset class="form-group">
      <select class="form-control" id="tipo_gasto" onchange="reload_registro()">  
        <option value="0">Todas</option>
        <option value="1">Gastos</option>
        <option value="2">Pagos</option>
      </select>
    </fieldset>
  </div>
  <div class="col-md-3"></div>
  <div class="col-md-2" align="center">
    <a onclick="registro_modal(0)" class="btn btn-raised btn-danger btn-min-width mr-1 mb-1" style="color:white !important"> Registrar gasto</a>
  </div>
</div>

<div class="row">
  <div class="col-sm-12">
      <div class="card">
          <div class="card-body">
              <div class="card-block">
                <input type="password"  class="form-control" disabled="" style="background-color: #ff000000; border: 1px solid rgb(0 0 0 / 0%); font-size: 0rem;">
                <!--------//////////////-------->
                <table class="table table-striped" id="data_tables" style="width: 100%">
                  <thead>
                    <tr>
                      <th>Id</th>
                      <th>Tipo</th>
                      <th>Nombre</th>
                      <th>Monto</th>
                      <th>Fecha</th>
                      <th>Categoría</th>
                      <th>Usuario</th>
                      <th></th>
                    </tr>
                  </thead>
                  <tbody>
                  </tbody>
                </table>
                <!--------//////////////-------->
              </div>
          </div>
      </div>
  </div>
</div>
<!------------------------------------------------>
<div class="modal fade text-left" id="registro_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel1">Registrar gastos</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
              <form method="post"  role="form" id="formgastos">
                <input type="hidden" name="id" id="idgasto" value="0">
                <div class="row">
                  <div class="col-md-12">
                    <fieldset class="form-group">
                        <label for="basicInput">Tipo de cobro</label>
                        <select id="tipocobro" name="tipocobro" class="form-control" onchange="fun_tipo_cobro()">
                          <option value="0" selected="" disabled="">Seleccionar una opción</option>
                          <option value="1">Gasto</option>
                          <option value="2">Pago</option>
                        </select>
                    </fieldset>
                  </div>
                </div>
                <div class="text_cobro" style="display: none">
                  <div class="row">
                    <div class="col-md-12">
                      <fieldset class="form-group">
                          <label for="basicInput">Nombre del <span class="tipo_cobro"></span></label>
                          <input type="text" class="form-control" name="nombre" id="gasto">
                      </fieldset>
                    </div>
                  </div>
                  <hr style="border-color: #f2752c;">
                  <div class="row">
                    <div class="col-md-6">
                      <fieldset class="form-group">
                          <label for="basicInput">Monto</label>
                          <input type="number" class="form-control" name="monto" id="monto">
                      </fieldset>
                    </div>
                    <div class="col-md-6">
                      <fieldset class="form-group">
                          <label for="basicInput">Fecha</label>
                          <input type="date" class="form-control" name="fecha" id="fecha">
                      </fieldset>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-10">
                      <fieldset class="form-group">
                          <label for="basicInput">Categoria</label>
                          <div class="text_select_categoria"></div>
                      </fieldset>
                    </div>
                    <div class="col-md-2">
                      <label for="basicInput" style="color: white">..</label><br>
                      <button type="button" class="btn btn-raised btn-danger btn-min-width mr-1 mb-1" onclick="modal_categoria()"><i class="fa fa-plus"></i></button>
                    </div>  
                  </div>
                </div>
              </form>  
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-raised btn-dark btn-min-width btn_registro" onclick="registrar()">Guardar</button>
                <button type="button" class="btn grey btn-outline-secondary" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>
<!------------------------------------------------>
<div class="modal fade text-left" id="eliminar_registro" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel1">Mensaje de confirmación</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                ¿Desea <b>Eliminar</b> este gasto?
                <input type="hidden" id="id_aux">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-raised btn-dark btn-min-width" onclick="delete_registro()">Aceptar</button>
                <button type="button" class="btn btn-round btn_amarillo" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>


<div class="modal fade text-left" id="modal_categoria" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel1">Categoría</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <fieldset class="form-group">
                            <label for="basicInput">Nombre</label>
                            <div class="input-group">
                                <form class="form" method="post" id="form_categoria" style="width: 100%">
                                    <input type="hidden" name="categoriaId" id="idcategoria_categoria" value="0">
                                    <input type="text" class="form-control" name="nombre" id="nombre_categoria" style="width: 100%">
                                </form>
                                <button type="button" class="btn btn-raised btn-dark btn-min-width mr-1 mb-1 btn_categoria" onclick="add_categoria()"><i class="fa fa-save"></i></button>
                            </div>
                        </fieldset>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <table class="table thead-inverse" id="table_categoria" style="width: 100%">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Nombre</th>
                                    <th>Acciones</th>
                                </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn grey btn-outline-secondary" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>