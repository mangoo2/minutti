<section class="basic-elements">
	<div class="row">
		<div class="col-sm-12">
			<div class="content-header"><?php echo $title_heades; ?></div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<div class="card">
				<div class="card-body mt-2">
					<div class="px-3">
						<form class="form" id="form-herramientas">

							<div class="row form-group">
								<div class="col-12 mt-2">
									<input type="hidden" id="IDherramienta" name="IDherramienta" class="form-control" value="<?php echo $id; ?>">
								</div>

								<div class="col-md-6 mt-2">
									<label>Medida</label>
									<input type="text" id="medida" name="medida" class="form-control" value="<?php echo isset($medida) ? $medida : '';  ?>" >
								</div>

								<div class="col-md-6 mt-2">
									<label>Nombre</label>
									<input type="text" id="nombre" name="nombre" class="form-control" value="<?php echo isset($nombre) ? $nombre : '';  ?>" >
								</div>

								<div class="col-md-6 mt-2">
									<label>Stock</label>
									<input type="number" id="stock" name="stock" class="form-control" value="<?php echo isset($stock) ? $stock : '';  ?>" >
								</div>

								<div class="col-md-6 mt-2">
									<label>Ubicacion</label>
									<input type="text" id="ubicacion" name="ubicacion" class="form-control" value="<?php echo isset($ubicacion) ? $ubicacion : '';  ?>" >
								</div>

								<div class="row form-group col-md-12 mt-3">
									<div class="col-md-12">
										<button type="button" class="btn btn-raised btn-dark btn-min-width ml-2 mb-1 btn-lg button_save" style="float: left;" onclick="save()"><i class="fa fa-save"></i> <?php echo $title_save; ?></button>
									</div>
								</div>

							</div>
						</form>

					</div>
				</div>
			</div>
		</div>
</section>