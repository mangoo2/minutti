<style type="text/css">
    .dataTables_wrapper {
        padding: 0px;
    }

    #table-lis td {
        font-size: 14px;
    }
</style>
<section class="basic-elements">
    <div class="row">
        <div class="col-sm-12">
            <div class="content-header">Bienvenido de nuevo:</div>
            <div class="content-header font-italic"><?php echo $usuario ?></div>
            <input type="hidden" id="id_cliente" class="form-control" value="<?php echo $idCliente; ?>">
            <br>
        </div>
        
    </div>
    
    <div class="row">
        <div class="col-md-3 px-4">
            <div class="card" style="background-image: linear-gradient( 45deg , #000000, #ff0404);">
                <a style="color:#ffffff" class="card-body card-block buttom_item" onclick="loadtable_activos();">
                    <div class="row">
                        <div class="col-md-12 text-center ">
                            <br>
                            <br>
                            Servicios activos
                            <br>
                            <br>
                        </div>
                    </div>
                </a>
            </div>
        </div>
        <div class="col-md-3 px-4">
            <div class="card" style="background-image: linear-gradient( 45deg , #000000, #ff0404);">
                <a style="color:#ffffff" class="card-body card-block buttom_item" onclick="loadtable_historico();">
                    <div class="row">
                        <div class="col-md-12 text-center">
                            <br>
                            <br>
                            Histórico de servicios
                            <br>
                            <br>
                        </div>
                    </div>
                </a>
            </div>
        </div>
        <div class="col-md-3 px-4">
            <div class="card" style="background-image: linear-gradient( 45deg , #000000, #ff0404);">
                <a style="color:#ffffff" class="card-body card-block buttom_item" onclick="loadtable_fecha();" >
                    <div class="row">
                        <div class="col-md-12 text-center">
                            <br>
                            Próximo servicio 
                            <br>
                            <?php echo $fecha; ?>
                            <br>
                            <br>
                        </div>
                    </div>
                </a>
            </div>
        </div>
        <div class="col-md-3 my-auto">
            <div class="card" style="background-image: linear-gradient( 45deg , #000000, #ff0404);">
            <br>
                <a style="color:#ffffff" class="card-body card-block buttom_item" onclick="modal_quejas();" >
                    <div class="row">
                        <div class="col-md-12 text-center">
                            Quejas y sugerencias
                        </div>
                    </div>
                </a>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header" style="padding:10px;">
                    <h4 class="card-title mb-0"></h4>
                </div>
                <div class="card-body">
                    <div class="px-3">
                    
                        <div class="row form-group">
                            <div class="col-md-12">
                                <table class="table thead-inverse" id="table-list" style="width: 100%">
                                    <thead>
                                        <tr>
                                            <th>ID SERVICIO</th>
                                            <th>DESCRIPCIÓN</th>
                                            <th>COSTO DE SERVICIO</th>
                                            <th>FECHA Y HORA</th>
                                            <th>ESTATUS</th>
                                            <th>COTIZACIÓN</th>
                                            <th>HOJA DE SERVICIO</th>
                                            <th>VEHÍCULO</th>
                                            <th>REFACCIONES ADICIONALES</th>
                                        </tr>
                                    </thead>
                                    <tbody></tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>



<!----------------------------------------------------------->
<div class="modal fade text-left" id="modal_vehiculo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" style="display: none;" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title" id="myModalLabel1">Vehículo</h4>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">×</span>
				</button>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-md-12">
						<div class="table_data_product">
							<table class="table thead-inverse" id="tabla_vehiculo" width="100%">
								<thead>
									<tr>
										<th>PLACA</th>
										<th>UNIDAD</th>
										<th>AÑO</th>
									</tr>
								</thead>
								<tbody class="list_vehiculo">
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
			</div>
		</div>
	</div>
</div>
<!----------------------------------------------------------->

<!----------------------------------------------------------->
<div class="modal fade text-left" id="modal_quejas" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" style="display: none;" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title" id="myModalLabel1">Quejas y sugerencias</h4>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">×</span>
				</button>
			</div>
			<div class="modal-body">
				<div class="row">
                    <div class="col-md-12">
						<textarea class="form-control" id="quejas" rows="6"><?php echo isset($quejas) ? $quejas : ''; ?></textarea>
					</div>
				</div>
			</div>
			<div class="modal-footer">
                <button type="button" class="btn btn-success" data-dismiss="modal" onclick="insertQueja()">Aceptar</button>
				<button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
			</div>
		</div>
	</div>
</div>
<!----------------------------------------------------------->
