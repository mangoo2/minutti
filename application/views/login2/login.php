<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>Minutti</title>
    <!-- Favicon-->
    <link rel="shortcut icon" type="image/x-icon" href="<?php echo base_url(); ?>public/img/fav.png">
    <link rel="shortcut icon" type="image/jpeg" href="<?php echo base_url(); ?>public/img/fav.png">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

    <!-- Bootstrap Core Css -->
    <link href="<?php echo base_url();?>plugins/bootstrap/css/bootstrap.css" rel="stylesheet">

    <!-- Waves Effect Css -->
    <link href="<?php echo base_url();?>plugins/node-waves/waves.css" rel="stylesheet" />

    <!-- Animation Css -->
    <link href="<?php echo base_url();?>plugins/animate-css/animate.css" rel="stylesheet" />

    <!-- Custom Css -->
    <link href="<?php echo base_url();?>css/style.css" rel="stylesheet">
    <script>
        var base_url="<?php echo base_url(); ?>";
    </script>
</head>
<style type="text/css">
    .btn_sesion{
        color: white !important;
        background-color: #bd1723 !important;
        border-radius: 17px !important;
    }
    .login-page {
        background-color: #1c1c1a !important;
    }
    .cx3 {
        background-color: rgb(255 255 255 / 0%) !important;
    }
    .cx2 {
        background-color: rgb(255 255 255) !important;
    }
    .col_sesion {
        color: white  !important;
    }
    .card{
        background: #1c1c1a  !important;
        border-radius: 38px !important;
    }
    .input-group .input-group-addon .material-icons {
        font-size: -1px;
        color: #bd1723;
    }
    .form-control{
        border-radius: 17px !important;
        text-align: center !important;
    }
    .form-group .form-line {
        width: 100%;
        position: relative;
        border-bottom: 1px solid #ddd0;
    }

    .bg-pink {
        background-color: #bd1723 !important;
        color: #fff;
    }

    .login-page {
        background-color: #00A6E0;
        /* background-size: cover; */
        /* padding-left: 0; */
        max-width: 1300px;
        margin: 7% auto;
        overflow-x: hidden;
    }
    
    .btn_login{
        background-image: linear-gradient(177deg, #00913d, #df0109) !important;
        background-repeat: repeat-x !important;
        color: white !important;
        border-radius: 15px !important;
    }

    .input_login{
        background: #ff000000;
        border: 3px solid white !important;
        color: white !important;
    }

    input::placeholder {
      color: white !important;
    }

    body {
      background-image: url("<?php echo base_url(); ?>public/img/fondo.jpg");
    }

    @media screen and (max-width: 480px) {
        .margen_m{
            padding: 38px;
        }
        .img_logo_f{
            display: none !important;
        }
        .img_logo{
            display: block !important;
        }
        body {
            height: 700px;
            background-image: url("<?php echo base_url(); ?>public/img/fondo2.jpg");
            background-repeat:no-repeat;
            background-position-x:center;
            background-position-y:center;
            background-size: 10rem;
        }
    }
</style>
<body class="login-page">
    <div class="sesion_texto" style="width: 400px;
        height: 400px;
        position: absolute;
        top: 50%;
        left: 50%;
        margin-left: -200px;
        margin-top: -200px;
        z-index: 1;
        display: none;" align="center"> 
        <br>
        <br>
        <br>
        <br>
        <br>
        <br>
        <h2 style="color: white;">Iniciando Sesión</h2>
        <h3 style="color: white;">Conectado a la base de datos</h3>
    </div>
    <!--
    <div class="cx3"></div>
    <div class="cx2"></div>
    <div class="cx1"></div>
    -->
    <div class="login-box">
        <div class="logo">
            <br>
        </div>
        <div class="login_texto">
            <div class="body">
                <form id="login-form" action="#" role="form">
                    <div class="row align-center">
                        <div class="col-md-8">
                            <div class="img_logo_f">
                                <img style="top: 25px;position: fixed;left: 7px;" src="<?php echo base_url(); ?>public/img/logo4.png" width="280" >
                            </div>
                        </div>
                        <div class="col-md-3 margen_m">
                            <div class="img_logo" style="display: none">
                                <img src="<?php echo base_url(); ?>public/img/logo4.png" width="280" >
                            </div>
                            <i class="material-icons font-40 m-t-20" style="color: white;">lock</i>
                            <h2 class="col_sesion">Accede a tu cuenta</h2>
                            <h5 class="col_sesion">Estamos contentos de que regreses</h5>
                            <br>
                            <div class="form-group input-group">
                                <div class="form-line">
                                    <input type="text" class="form-control input_login" name="txtUsuario" id="txtUsuario" placeholder="Nombre de Usuario" required autofocus>
                                </div>
                            </div>
                            <div class="form-group input-group">
                                <div class="form-line">
                                    <input type="password" class="form-control input_login" name="txtPass" id="txtPass" placeholder="Contraseña" required>
                                </div>
                            </div>
                            <div class="row"> 
                                <div class="col-xs-12">
                                    <button class="btn btn-block btn_login waves-effect" type="submit">INICIAR SESIÓN</button>
                                </div>
                            </div>
                            <br>
                            <div class="alert bg-pink" id="error" style="display:none">
                                <i class="material-icons ">error</i> <strong>Error!</strong> El nombre de usuario y/o contraseña son incorrectos 
                            </div>
                        </div>
                        <div class="col-md-1">
                        </div>    
                    </div>
                </form>
            </div>
        <div>    
    </div>
    </div>
    <!--
    <div class="text-center" id="loader">
        <div class="lds-ripple"><div></div><div></div></div>
    </div>
-->
    <!--
    <div class="alert bg-light-green" id="success">
        <i class="material-icons ">done</i> <strong>Acceso Correcto!</strong> Será redirigido al sistema 
    </div>
    -->
    <!-- Jquery Core Js -->
    <script src="<?php echo base_url();?>plugins/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core Js -->
    <script src="<?php echo base_url();?>plugins/bootstrap/js/bootstrap.js"></script>

    <!-- Waves Effect Plugin Js -->
    <script src="<?php echo base_url();?>plugins/node-waves/waves.js"></script>

    <!-- Validation Plugin Js -->
    <script src="<?php echo base_url();?>plugins/jquery-validation/jquery.validate.js"></script>

    <!-- Custom Js -->
</body>

</html>