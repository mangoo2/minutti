<?php
header("Pragma: public");
header("Expires:0");
header("Cache-Control:must-revalidate,post-check=0, pre-check=0");
header("Content-Type: application/force-download");
header("Content-Type: application/octet-stream");
header("Content-Type: application/download");
header("Content-Type: application/vnd.ms-excel;");
header("Content-Disposition: attachment; filename=reporte_productos_".date('d-m-Y').".xls");

?>

<table  border="1">
    <thead>
        <tr>
            <th>#</th>
            <th><?php echo utf8_decode('Código') ?></th>
            <th>Producto</th>
            <th><?php echo utf8_decode('Descripción') ?></th>
            <th>Unidad</th>
            <th><?php echo utf8_decode('Categoría') ?></th>
            <th><?php echo utf8_decode('Configuración') ?></th>
            <th>Naves</th>
            <th>Precio de compra</th>
        </tr>
    </thead>
    <tbody>
    <?php  foreach ($productos_get as $x){
        $existencia_min = $x->existencia_min;
        $stock='';
        $stocks = $this->ModeloProductos->productos_sucu($x->productoId,$nave);
        foreach ($stocks as $stck){
            $stock = $stck->stock;
        }

        $backgroundcolor='';
        if($stock>$existencia_min){
            $backgroundcolor="green";
        }else if($stock>=1 && $stock<=$existencia_min){
            $backgroundcolor="orange";
        }else if($stock<=0){
            $backgroundcolor="red";
        }

        ?>
        
        <tr>
            <td><?php echo $x->productoId ?></td>
            <td><?php echo utf8_decode($x->codigo) ?></td>
            <td><?php echo utf8_decode($x->producto) ?></td>
            <td><?php echo utf8_decode($x->descripcion) ?></td>
            <td><?php echo utf8_decode($x->unidad) ?></td>
            <td><?php echo utf8_decode($x->categoria) ?></td>
            <td><?php echo utf8_decode($x->config) ?></td>
            <td style="background-color:<?php echo $backgroundcolor;?>"><?php   
                $sucu=''; 
                $result_sucu=$this->ModeloProductos->productos_sucu($x->productoId,$nave); 
                foreach ($result_sucu as $i){
                    $sucu.=utf8_decode($i->sucursal).': '.$i->stock.'<br>'; 
                }
                echo utf8_decode($sucu) ?></td>
            <td><?php echo $x->preciocompra ?></td>
        </tr>
    <?php } ?>
    </tbody>
</table>