<section class="basic-elements">
    <div class="row">
        <div class="col-sm-12">
            <div class="content-header"><?php echo $title_heades;?></div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <br>
                <div class="card-body">
                    <div class="px-3">
                        <form class="form" id="form-producto">
                            <div class="row form-group">
                                <label class="col-md-2">Código</label>
                                <div class="col-md-7">
                                    <input type="hidden" name="productoId" id="productoId" value="0">
                                    <input type="text" name="codigo" id="codigo" class="form-control">
                                </div>
                                <div class="col-md-3">
                                    <button type="button" class="btn btn-raised btn-danger btn-min-width mr-1 mb-1" onclick="generacodigo()"><i class="fa fa-barcode"></i> Generar Código</button>
                                </div>
                            </div>
                            <div class="row form-group">
                                <label class="col-md-2">Nombre</label>
                                <div class="col-md-10">
                                    <input type="text" name="nombre" id="nombre" class="form-control">
                                </div>
                            </div> 
                            <div class="row form-group">
                                <label class="col-md-2">Descripción</label>
                                <div class="col-md-10">
                                    <textarea name="descripcion" id="descripcion" class="form-control"></textarea>
                                </div>
                                
                            </div> 
                            <div class="row form-group">
                                <?php /*
                                <label class="col-md-2">Unidad</label>
                                <div class="col-md-4">
                                    <div class="input-group">
                                        <select class="form-control" name="unidadId" id="unidadId">
                                            <option value="0" selected="" disabled="">Seleccionar una opción</option>
                                            <?php foreach ($unidadrow->result() as $item) { ?>
                                                <option value="<?php echo $item->unidadId;?>"><?php echo $item->unidad;?></option>
                                            <?php } ?>
                                        </select>
                                        <button type="button" class="btn btn-raised btn-danger btn-min-width mr-1 mb-1" onclick="modal_unidad()"><i class="fa fa-plus"></i></button>
                                    </div>
                                </div>
                                */ ?>
                                <label class="col-md-2">Categoría</label>
                                <div class="col-md-4">
                                    <div class="input-group">
                                        <select class="form-control" name="categoriaId" id="categoriaId">
                                            <option value="0" selected="" disabled="">Seleccionar una opción</option>
                                            <?php foreach ($categoriarow->result() as $item) { ?>
                                                <option value="<?php echo $item->categoriaId;?>"><?php echo $item->categoria;?></option>
                                            <?php } ?>
                                        </select>
                                        <button type="button" class="btn btn-raised btn-danger btn-min-width mr-1 mb-1" onclick="modal_categoria()"><i class="fa fa-plus"></i></button>
                                    </div>
                                </div>


                                <label class="col-md-2">Marca</label>
                                <div class="col-md-4">
                                    <div class="input-group">
                                        <select class="form-control" name="marcaId" id="marcaId">
                                            <option value="0" selected="" disabled="">Seleccionar una opción</option>
                                            <?php foreach ($marcarow->result() as $item) { ?>
                                                <option value="<?php echo $item->marcaId;?>"><?php echo $item->marca;?></option>
                                            <?php } ?>
                                        </select>
                                        <button type="button" class="btn btn-raised btn-danger btn-min-width mr-1 mb-1" onclick="modal_marca()"><i class="fa fa-plus"></i></button>
                                    </div>
                                </div>
                            </div>   
                            
                            <div class="row form-group">
                                <!--
                                <label class="col-md-2">Configuración familia</label>
                                <div class="col-md-4">
                                    <div class="input-group">
                                        <select class="form-control" name="configuracion" id="configuracion">
                                            <option value="0" selected="" disabled="">Seleccionar una opción</option>
                                            <?php /* foreach ($config_productorow->result() as $item) { ?>
                                                <option value="<?php echo $item->id;?>"><?php echo $item->nombre;?></option>
                                            <?php } */?>
                                        </select>
                                        <button type="button" class="btn btn-raised btn-danger btn-min-width mr-1 mb-1" onclick="modal_configuracion()"><i class="fa fa-plus"></i></button>
                                    </div>
                                </div>
                                -->
                                <div class="col-md-4">
                                    Cantidad mínima de producto en existencia
                                </div>    
                                <div class="col-md-2">
                                    <div class="input-group">
                                        <input type="number" name="cantidad_minima_existencia" id="cantidad_minima_existencia" class="form-control">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    Cantidad máxima de producto en existencia
                                </div>    
                                <div class="col-md-2">
                                    <div class="input-group">
                                        <input type="number" name="cantidad_maxima_existencia" id="cantidad_maxima_existencia" class="form-control">
                                    </div>
                                </div>
                            </div>   
                            <div class="row form-group">
                                <label class="col-md-2">Número de serie</label>
                                <div class="col-md-4">
                                    <div class="input-group">
                                        <input type="text" name="num_serie" id="num_serie" class="form-control">
                                    </div>
                                </div>
                                
                            </div>  
                            <div class="row">
                                <div class="col-md-12">
                                    <h4 style="border-bottom: solid #bd1620;"><b>Existencias</b></h4>
                                </div>
                                <label class="col-md-2">Stock disponible </label>
                                <div class="col-md-4">
                                    <div class="input-group">
                                        <input type="number" name="stock_disponible" id="stock_disponible" class="form-control">
                                    </div>
                                </div>
                            </div>

                            
                            <div class="row">
                                <div class="col-md-12">
                                    <h4 style="border-bottom: solid #bd1620;"><b>Costos</b></h4>
                                </div>
                                <label class="col-md-2">Precio venta</label>
                                <div class="col-md-4">
                                    <div class="input-group">
                                        <input type="number" name="precio_venta" id="precio_venta" class="form-control">
                                    </div>
                                </div>

                                <label class="col-md-2">Costo compra</label>
                                <div class="col-md-4">
                                    <div class="input-group">
                                        <input type="number" name="costo_compra" id="costo_compra" class="form-control">
                                    </div>
                                </div>
                            </div>
                            
                        </form>
                        
                        <!--
                    	<table id="table-stock-sucursales">
                            <tbody>
                                <?php /* foreach ($sucursalrow->result() as $item) { ?>
                                    <tr>
                                        <td>
                                            <?php echo $item->sucursal;?>
                                        </td>
                                        <td>
                                            <input type="hidden" id="productosId" class="productosId_<?php echo $item->sucursalId;?>" value="0">
                                            <input type="hidden" id="sucursalId" class="sucursalId_<?php echo $item->sucursalId;?>" value="<?php echo $item->sucursalId;?>">
                                            <input type="number" id="stock" class="stock_<?php echo $item->sucursalId;?> form-control totalstock" value="" onchange="totalstock()">
                                            
                                        </td>
                                        <td>
                                            <input type="checkbox" id="pertenece" class="pertenece_<?php echo $item->sucursalId;?>"  value="0">
                                        </td>
                                    </tr>
                                <?php } */ ?> 
                            </tbody>
                            <tfoot>
                                <tr>
                                    <td><h5><b>Total de existencias</b></h5></td>
                                    <td class="totalstockt" align="center">0</td>
                                </tr>
                            </tfoot>
                            
                        </table>
                        -->
                        <?php /*
                        <br>
                        <div class="row">
                            <div class="col-md-12">
                                <h4 style="border-bottom: solid #bd1620;"><b>Precios y proveedores</b></h4>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="row form-group">
                                    <label class="col-md-2">Precio de compra</label>
                                    <div class="col-md-3">
                                        <input type="text" name="preciocompra" id="preciocompra" class="form-control">
                                    </div>
                                </div> 
                            </div>
                        </div>
                        */ ?>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="table_ultimo_costo"></div>
                            </div>
                        </div>        
                        <br>
                        <div class="row form-group">
                            <div class="col-md-12">
                                <button type="button" class="btn btn-raised btn-dark btn-min-width mr-1 mb-1 btn-lg button_save" style="float: left;" onclick="save()"><i class="fa fa-save"></i> <?php echo $title_save;?></button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<div class="modal fade text-left" id="modal_unidad" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel1">Unidad</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <fieldset class="form-group">
                            <label for="basicInput">Nombre</label>
                            <div class="input-group">
                                <form class="form" method="post" id="form_unidad" style="width: 100%">
                                    <input type="hidden" name="unidadId" id="idunidad_unidad" value="0">
                                    <input type="text" class="form-control" name="unidad" id="nombre_unidad" style="width: 100%">
                                </form>
                                <button type="button" class="btn btn-raised btn-dark btn-min-width mr-1 mb-1 btn_unidad" onclick="add_unidad()"><i class="fa fa-save"></i></button>
                            </div>
                        </fieldset>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <table class="table thead-inverse" id="table_unidad" style="width: 100%">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Nombre</th>
                                    <th>Acciones</th>
                                </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn grey btn-outline-secondary" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade text-left" id="modal_categoria" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel1">Categoría</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <fieldset class="form-group">
                            <label for="basicInput">Nombre</label>
                            <div class="input-group">
                                <form class="form" method="post" id="form_categoria" style="width: 100%">
                                    <input type="hidden" name="categoriaId" id="idcategoria_categoria" value="0">
                                    <input type="text" class="form-control" name="categoria" id="nombre_categoria" style="width: 100%">
                                </form>
                                <button type="button" class="btn btn-raised btn-dark btn-min-width mr-1 mb-1 btn_categoria" onclick="add_categoria()"><i class="fa fa-save"></i></button>
                            </div>
                        </fieldset>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <table class="table thead-inverse" id="table_categoria" style="width: 100%">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Nombre</th>
                                    <th>Acciones</th>
                                </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn grey btn-outline-secondary" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade text-left" id="modal_configuracion" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel1">Configuraciones</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <fieldset class="form-group">
                            <label for="basicInput">Nombre</label>
                            <div class="input-group">
                                <form class="form" method="post" id="form_configuracion" style="width: 100%">
                                    <input type="hidden" name="id" id="idconfig" value="0">
                                    <input type="text" class="form-control" name="nombre" id="nombre_config" style="width: 100%">
                                </form>
                                <button type="button" class="btn btn-raised btn-dark btn-min-width mr-1 mb-1 btn_configuracion" onclick="add_configuracion()"><i class="fa fa-save"></i></button>
                            </div>
                        </fieldset>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <table class="table thead-inverse" id="table_configuracion" style="width: 100%">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Nombre</th>
                                    <th>Acciones</th>
                                </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn grey btn-outline-secondary" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>


<!--Marca-->
<div class="modal fade text-left" id="modal_marca" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel1">Marca</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <fieldset class="form-group">
                            <label for="basicInput">Nombre</label>
                            <div class="input-group">
                                <form class="form" method="post" id="form_marca" style="width: 100%">
                                    <input type="hidden" name="marcaId" id="idmarca_marca" value="0">
                                    <input type="text" class="form-control" name="marca" id="nombre_marca" style="width: 100%">
                                </form>
                                <button type="button" class="btn btn-raised btn-dark btn-min-width mr-1 mb-1 btn_marca" onclick="add_marca()"><i class="fa fa-save"></i></button>
                            </div>
                        </fieldset>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <table class="table thead-inverse" id="table_marca" style="width: 100%">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Nombre</th>
                                    <th>Acciones</th>
                                </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn grey btn-outline-secondary" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>