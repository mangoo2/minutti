<style type="text/css">
	.dataTables_wrapper {
		padding: 0px;
	}

	#table-lis td {
		font-size: 14px;
	}
</style>
<section class="basic-elements">
	<div class="row">
		<div class="col-sm-12">
			<div class="content-header">Productos - Piezas - Materiales</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<div class="card">
				<div class="card-header" style="padding:10px;">
					<h4 class="card-title mb-0"></h4>
				</div>
				<div class="card-body">
					<div class="px-3">
						<div class="row form-group">
							<div class="col-md-2">
								<a type="button" class="btn btn-raised btn-danger btn-min-width mr-1 mb-1 button_save" href="<?php echo base_url() ?>Productos/add"><i class="fa fa-save"></i> Agregar nuevo</a>
							</div>
							<div class="col-md-3">
								<select class="form-control" id="categoriaselect" onchange="loadtable()">
									<option value="0">Todo</option>
									<?php foreach ($categoriarow->result() as $item) { ?>
										<option value="<?php echo $item->categoriaId; ?>"><?php echo $item->categoria; ?></option>
									<?php } ?>
								</select>
							</div>
							<div class="col-md-3">
                                <!--
								<div class="row" style="display:<?php /* echo $selectedsucursal_view; ?>; ">
									<select class="form-control" id="sucursalselect" onchange="loadtable()">
										<option value="0">Todo</option>
										<?php foreach ($sucursalrow->result() as $items) {
											if ($items->sucursalId == 1) {
												$nom_sucu = 'N1';
											} else if ($items->sucursalId == 2) {
												$nom_sucu = 'N2';
											} else if ($items->sucursalId == 3) {
												$nom_sucu = 'N3';
											} else if ($items->sucursalId == 4) {
												$nom_sucu = 'N4';
											}
										?>
											<option value="<?php echo $items->sucursalId; ?>" <?php if ($selectedsucursal == $items->sucursalId) {
											echo	} ?>><?p<?php } */ ?>
									</select>
								</div>
						    	-->
							</div>
							<!-- <div class="col-md-3" align="right">
								<a type="button" class="btn btn-raised btn-danger btn-min-width mr-1 mb-1" style="color: white" onclick="get_excel()">Descargar excel</a>
							</div> -->
						</div>
						<div class="row">
							<?php /* foreach ($sucursalrow->result() as $items) {
								if ($items->sucursalId == 1) {
									$nom_sucu = 'N1';
								} else if ($items->sucursalId == 2) {
									$nom_sucu = 'N2';
								} else if ($items->sucursalId == 3) {
									$nom_sucu = 'N3';
								} else if ($items->sucursalId == 4) {
									$nom_sucu = 'N4';
								}
								
								*/
								$totalporsucursal = $this->ModeloSession->totalporsucursal();
							?>
								<div class="col-md-3 ">
									<div class="card" style="background-image: linear-gradient( 45deg , #000000, #ff0404);">
										<a style="color:#ffffff" class="card-body card-block buttom_item">
											<div class="row">
												<div class="col-md-12 text-center">
													<br>
													Costo de almacen
													<br>
													<br>
													<h5>$ <?php echo number_format($totalporsucursal, 2, '.', ','); ?></h5>
													<br>
												</div>
											</div>
										</a>
									</div>
								</div>
							<?php // } ?>
						</div>
						<div class="row form-group">
							<div class="col-md-12">
								<table class="table thead-inverse" id="table-lis" style="width: 100%">
									<thead>
										<tr>
											<th>#</th>
											<th>CÓDIGO</th>
											<th>PRODUCTO</th>
											<th>CATEGORÍA</th>
											<th>EXISTENCIA</th>
											<th>ACCIONES</th>
										</tr>
									</thead>
									<tbody></tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>


<div class="modal fade text-left" id="modal_existencias" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" style="display: none;" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title" id="myModalLabel1">Existencias</h4>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">×</span>
				</button>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-md-12">
						<table class="table thead-inverse">
							<thead>
								<tr>
									<th>CÓDIGO</th>
									<th>PRODUCTO</th>
									<th>UNIDAD</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td class="class_codigo"></td>
									<td class="class_nombre"></td>
									<td>
										<select class="form-control unidadId" disabled style="background: transparent;border: 0px;">
											<?php foreach ($unidadrow->result() as $item) { ?>
												<option value="<?php echo $item->unidadId; ?>"><?php echo $item->unidad; ?></option>
											<?php } ?>
										</select>
									</td>
								</tr>
							</tbody>

						</table>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<table class="table thead-inverse">
							<thead>
								<tr>
									<?php foreach ($sucursalrow->result() as $item) { ?>
										<th><?php echo $item->sucursal; ?></th>
									<?php } ?>
									<th>Existencias totales</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<?php foreach ($sucursalrow->result() as $item) { ?>
										<td class="stock_<?php echo $item->sucursalId; ?> totalstock"></td>
									<?php } ?>
									<td class="totalstockt">0</td>
								</tr>
							</tbody>

						</table>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn grey btn-outline-secondary" data-dismiss="modal">Cerrar</button>
				<!--<button type="button" class="btn btn-outline-primary">Save changes</button>-->
			</div>
		</div>
	</div>
</div>

<div class="modal fade text-left" id="modal_traspaso" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" style="display: none;" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title" id="myModalLabel1">Existencias</h4>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">×</span>
				</button>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-md-12">
						<table class="table thead-inverse">
							<thead>
								<tr>
									<th>CÓDIGO</th>
									<th>PRODUCTO</th>
									<th>UNIDAD</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td class="class_codigo"></td>
									<td class="class_nombre"></td>
									<td>
										<select class="form-control unidadId" disabled style="background: transparent;border: 0px;">
											<?php foreach ($unidadrow->result() as $item) { ?>
												<option value="<?php echo $item->unidadId; ?>"><?php echo $item->unidad; ?></option>
											<?php } ?>
										</select>
									</td>
								</tr>
							</tbody>

						</table>
					</div>
				</div>
				<div class="row">
					<div class="col-md-4">
						<label>Salida</label>
						<select class="form-control" id="sucursalsalida" onchange="validar_nave()">
							<option value="0" selected="" disabled="">Selecciona una opción</option>
							<?php foreach ($sucursalrow->result() as $item) { ?>
								<option value="<?php echo $item->sucursalId; ?>" class="stock2_<?php echo $item->sucursalId; ?>" data-name="<?php echo $item->sucursal; ?>"><?php echo $item->sucursal; ?></option>
							<?php } ?>
						</select>
					</div>
					<div class="col-md-2">
						<label>Cantidad</label>
						<input type="number" id="cantidad_traspaso" class="form-control">
					</div>
					<label class="label col-md-2">Mover a:</label>
					<div class="col-md-4">
						<label>Entrada</label>
						<select class="form-control" id="sucursalentrada">
							<option value="0" selected="" disabled="">Selecciona una opción</option>
							<?php foreach ($sucursalrow->result() as $item) { ?>
								<option value="<?php echo $item->sucursalId; ?>" class="stock2_<?php echo $item->sucursalId; ?>" data-name="<?php echo $item->sucursal; ?>"><?php echo $item->sucursal; ?></option>
							<?php } ?>
						</select>
					</div>
					<div class="col-md-12 text-center">
						<button type="button" class="btn btn-raised btn-dark btn-min-width btn-lg mr-1 mb-1 button_save_transpaso" onclick="save_transpaso()" style="margin-top:10px;"><i class="fa fa-save"></i> Generar orden de traspaso</button>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn grey btn-outline-secondary" data-dismiss="modal">Cerrar</button>
				<!--<button type="button" class="btn btn-outline-primary">Save changes</button>-->
			</div>
		</div>
	</div>
</div>


<div class="modal fade text-left" id="modal_bitacora" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" style="display: none;" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title" id="myModalLabel1">Existencias</h4>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">×</span>
				</button>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-md-12">
						<input type="hidden" id="producto_bitacora">
						<table class="table thead-inverse">
							<thead>
								<tr>
									<th>CÓDIGO</th>
									<th>PRODUCTO</th>
									<th>UNIDAD</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td class="class_codigo"></td>
									<td class="class_nombre"></td>
									<td>
										<select class="form-control unidadId" disabled style="background: transparent;border: 0px;">
											<?php foreach ($unidadrow->result() as $item) { ?>
												<option value="<?php echo $item->unidadId; ?>"><?php echo $item->unidad; ?></option>
											<?php } ?>
										</select>
									</td>
								</tr>
							</tbody>

						</table>
					</div>
				</div>
				<div class="row">
					<div class="col-md-4">
						<label>Ver</label>
						<select class="form-control" id="sucursalbitacora" onchange="obtenerdatosproductobitacora()">
							<?php foreach ($sucursalrow->result() as $item) { ?>
								<option value="<?php echo $item->sucursalId; ?>" class="stock2_<?php echo $item->sucursalId; ?>" data-name="<?php echo $item->sucursal; ?>"><?php echo $item->sucursal; ?></option>
							<?php } ?>
						</select>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12 insertablabitacora">

					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn grey btn-outline-secondary" data-dismiss="modal">Cerrar</button>
				<!--<button type="button" class="btn btn-outline-primary">Save changes</button>-->
			</div>
		</div>
	</div>
</div>

<div class="modal fade text-left" id="modal_barcode" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title" id="myModalLabel1">Etiquetas</h4>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<div class="col-md-6">
					<p>Codigo de producto: <b><span class="class_codigo"></span></b> </p>
					<p>Producto: <b><span class="class_nombre"></span></b></p>
					<!--
                <p>Categoria: <b><span id="ecategoria"></span></b></p>
                <p>Precio: <b>$ <span id="eprecio"></span></b></p>
                --->
					<input type="hidden" name="idproetiqueta" id="idproetiqueta" readonly>
					<div class="col-md-12">
						<div class="form-group">
							<div class=" col-md-6">
								<input type="number" name="numprint" id="numprint" class="form-control" min="1" value="1">
							</div>
							<div class=" col-md-5">
								<a href="#" class="btn btn-raised gradient-purple-bliss white" id="imprimir_barcode">Imprimir</a>
							</div>
						</div>
					</div>

				</div>
				<div class="col-md-6" id="iframe_barcode">

				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn grey btn-outline-secondary" data-dismiss="modal">Cerrar</button>
			</div>
		</div>
	</div>
</div>