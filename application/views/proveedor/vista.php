<style type="text/css">
    .dataTables_wrapper{
        padding: 0px;
    }
    #table-lis td{
        font-size: 14px;
    }
</style>
<section class="basic-elements">
  <div class="row">
      <div class="col-sm-12">
          <div class="content-header">Proveedores</div>
      </div>
  </div>
  <div class="row">
    <div class="col-md-12">
      <div class="card">
        <div class="card-body">
          <br>
          <div class="px-3">
            <div class="row form-group">
                  <div class="col-md-3">
                      <a type="button" class="btn btn-raised btn-danger btn-min-width mr-1 mb-1" href="<?php echo base_url()?>Proveedores/registrar"><i class="fa fa-save"></i> Agregar nuevo</a>
                  </div>
            </div>
            <div class="row form-group">
              <div class="col-md-12">
                <!--------//////////////-------->
                <table class="table thead-inverse" id="table_datos" style="width: 100%">
                  
                  <thead >
                    <tr>
                      <th>Nombre</th>
                      <th>Correo</th>
                      <th>Tel.Celular</th>
                      <th>Tel.Ofcina</th>
                      <th>Contacto</th>
                      <th>Acciones</th>
                    </tr>
                  </thead>
                  <tbody>
                  </tbody>
                </table>
                <!--------//////////////-------->
              </div>
            </div>    
          </div>
        </div>
      </div>
    </div>
</div>
