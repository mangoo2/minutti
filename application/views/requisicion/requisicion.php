<section class="basic-elements">
    <div class="row">
        <div class="col-sm-12">
            <div class="content-header">Requisición de material</div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title mb-0"></h4>
                </div>
                <div class="card-body">
                    <div class="px-3">
                        <form class="form" id="form-requisicion">
                            <div class="row form-group">
                                <label class="col-md-2">Quien solicita:</label>
                                <div class="col-md-4">
                                    <input type="hidden" class="form-control" name="requsicionId" id="requsicionId" value="<?php echo $requsicionId ?>">
                                    <input type="text" name="personal" id="personal" class="form-control" value="<?php echo $personal_txt;?>">
                                    
                                </div>
                                <label class="col-md-2">Fecha de solicitud:</label>
                                <div class="col-md-4">
                                    <input type="date" class="form-control" id="fecha_solicitud" name="fecha_solicitud" value="<?php echo $fecha_solicitud ?>">
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col-md-6">
                                    
                                </div>
                                <label class="col-md-2">Destino:</label>
                                <div class="col-md-4">
                                    <select class="form-control" name="destino" id="destino">
                                        <option value="0" disabled="" selected="">Seleccione una opción</option>
                                        <?php foreach ($sucursalrow->result() as $item) { ?>
                                            <option value="<?php echo $item->sucursalId;?>" <?php if($item->sucursalId==$destino) echo 'selected'?>><?php echo $item->sucursal;?></option>
                                        <?php } ?> 
                                    </select>
                                </div>
                            </div>
                        </form>
                        <div class="row">
                            <label class="col-md-2">Cantidad:</label>
                            <div class="col-md-3">
                                <input type="number" class="form-control" id="s_cantidad">
                            </div>
                            <label class="col-md-2">Producto:</label>
                            <div class="col-md-3">
                                <select class="form-control" id="s_producto">
                                    
                                </select>
                            </div>
                            <div class="col-md-2">
                                <button type="button" class="btn btn-raised btn-danger" style="float: left;" onclick="add_datos()"><i class="fa fa-plus"></i> Agregar</button>
                            </div>
                        </div>
                        <br>
                        <div class="row form-group">
                            <div class="col-md-12">
                                <table class="table thead-inverse" id="table-productos">
                                    <thead>
                                        <tr>
                                            <th>Cantidad</th>
                                            <th>Cógido pieza</th>
                                            <th>Pieza</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody class="table-productos-tb">
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-md-12">
                                <button type="button" class="btn btn-dark btn-lg btn_registro" style="float: left;" onclick="save()"><i class="fa fa-save"></i> Procesar requisición</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>