<style type="text/css">
    #table-data-row td{
        font-size: 15px;
    }
</style>
<section class="basic-elements">
    <div class="row">
        <div class="col-sm-12">
            <div class="content-header">Requisiciones de material</div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title mb-0"></h4>
                </div>
                <div class="card-body">
                    <div class="px-3">
                        <div class="row">
                            <div class="col-md-12">
                                <a href="<?php echo base_url();?>Requisiciones/add" type="button" class="btn btn-raised btn-danger btn-min-width mr-1 mb-1 button_save" style="float: right;" ><i class="fa fa-plus"></i> Agregar</a>
                            </div>
                            <div class="col-md-12">
                                <table class="table thead-inverse" id="table-data-row">
                                    <thead >
                                        <tr>
                                            <th>#</th>
                                            <th>Solicitante</th>
                                            <th>Destino</th>
                                            <th>Fecha</th>
                                            <th>PERSONAL QUE INGRESO</th>
                                            <th>Estatus</th>
                                            <th style="width: 84px;"></th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                        
                        
                        
                        

                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>