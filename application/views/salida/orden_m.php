<style type="text/css">
    a {
        color: #b7171f;
    }
</style>
<section class="basic-elements">
    <div class="row">
        <div class="col-sm-12">
            <div class="content-header">Surtir orden de material</div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title mb-0"></h4>
                </div>
                <div class="card-body">
                    <div class="px-3">
                        <form class="form" id="form-configuracion">
                            <div class="row form-group">
                                <label class="col-md-2">Cliente</label>
                                <div class="col-md-4">
                                    <input type="hidden" class="form-control" name="ordenId" id="ordenId" value="<?php echo $ordenId ?>">
                                    <select class="form-control" name="clienteId" id="clienteId" style="width: 100%">
                                        <?php 
                                        if($clienteId!=0){
                                            echo '<option value="'.$clienteId.'">'.$cliente.'</option>';
                                        }?>
                                    </select>
                                </div>
                                <label class="col-md-2" style="padding-right: 0; padding-left: 5px;">Lider:</label>
                                <div class="col-md-4">
                                    <input type="text" class="form-control" name="lider" id="lider" value="<?php  echo !$lider ? $user_session : $lider ?>">
                                    <?php /*
                                    <select class="form-control" name="lider" id="lider" style="width: 100%">
                                        <?php 
                                        if($personalId!=0){
                                            echo '<option value="'.$personalId.'">'.$persona.'</option>';
                                        }?>
                                    </select>
                                    */ ?>
                                </div>
                            </div>
                            <div class="row form-group">
                                <label class="col-md-2">Número de serie</label>
                                <div class="col-md-4">
                                    <input type="text" class="form-control" name="serie" id="serie" value="<?php echo $serie ?>">
                                </div>
                                <label class="col-md-2" style="padding-right: 0; padding-left: 5px;">Fecha de solicitud:</label>
                                <div class="col-md-4">
                                    <input type="date" class="form-control" name="fecha_solicitud" id="fecha_solicitud" value="<?php echo $fecha_solicitud ?>">
                                </div>
                            </div>
                            <div class="row form-group">
                                <label class="col-md-2">Tipo de unidad</label>
                                <div class="col-md-4">
                                    <select class="form-control" name="unidad" id="unidad">
                                        <option value="0" disabled="" selected="">Seleccione una opción</option>
                                        <?php foreach ($unidadrow->result() as $item) { ?>
                                            <option value="<?php echo $item->id;?>" <?php if($item->id==$unidad) echo 'selected' ?>><?php echo $item->nombre;?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                                <label class="col-md-2" style="padding-right: 0; padding-left: 5px;">Fecha de entraga:</label>
                                <div class="col-md-4">
                                    <input type="date" class="form-control" name="fecha_entrega" id="fecha_entrega" value="<?php echo $fecha_entrega ?>">
                                </div>
                            </div>
                            <div class="row form-group">
                                <label class="col-md-3">Modelo de asiento</label>
                                <div class="col-md-9">
                                    <input type="text" class="form-control" name="modelo_asiento" id="modelo_asiento" value="<?php echo $modelo_asiento ?>">
                                </div>
                            </div>
                        </form>
                        <div class="row form-group">
                            <div class="col-md-12">
                                <hr>
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-md-12">
                                Indique unidades de subconfiguración
                            </div>
                        </div>
                        <!-------------------------------------------------->
                        <div class="row">
                            <div class="col-md-12 addsubconfiguracion">
                                <div class="card-block">
                                    
                                    <ul class="nav nav-tabs addsubconfiguracion_tabs">
                                      
                                    </ul>
                                    <div class="tab-content px-1 pt-1 addsubconfiguracion_body">
                                      
                                      
                                    </div>
                                  </div>
                            </div>
                        </div>
                        
                        <!-------------------------------------------------->
                        <div class="row form-group">
                            <div class="col-md-12">
                                <button type="button" class="btn btn-raised btn-danger btn-min-width mr-1 mb-1 button_save" onclick="subconfig()"><i class="fa fa-plus"></i> Agregar otra configuración</button>
                            </div>
                        </div>

                        <div class="row form-group">
                            <div class="col-md-12">
                                <button type="button" class="btn btn-dark btn-lg button_save" style="float: right;" onclick="save()"><i class="fa fa-save"></i> Procesar orden</button>
                                <a href="<?php echo base_url();?>Salida/orden_m" type="button" class="btn btn-raised btn-dark btn-min-width mr-1 mb-1 btn-lg"><i class="fa fa-mail-reply"></i> Regresar</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<div class="row" style="display:none">
    <div class="col-md-12">
        <select id="selecte_config">
                <option value="0" disabled selected>Seleccione una opción</option>
            <?php foreach ($configuracionrow->result() as $item) { ?>
                    <option value="<?php echo $item->configId;?>"><?php echo $item->nombre;?></option>
            <?php } ?>
        </select>
    </div>
</div>

<div class="modal fade text-left" id="modal_productos_sub" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" style="display: none;" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel1">Productos</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="tabla_productos"></div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn grey btn-outline-secondary" data-dismiss="modal">Cerrar</button>
                <!--<button type="button" class="btn btn-outline-primary">Save changes</button>-->
            </div>
        </div>
    </div>
</div>