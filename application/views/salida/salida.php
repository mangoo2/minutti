<section class="basic-elements">
    <div class="row">
        <div class="col-sm-12">
            <div class="content-header">Salida de almacén / Asignación de materiales</div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="">
                <div class="">
                    <div class="px-3">
                        <div class="row">
                            <div class="col-md-4 ">
                                <div class="card" style="background-image: linear-gradient( 45deg , #000000, #ff0404); height: 216px;">
                                    <a style="color:#ffffff" class="card-body card-block buttom_item" href="<?php echo base_url();?>Salida/orden_m">
                                        <div class="row">
                                            <div class="col-md-12 text-center" style="min-height: 104px;">
                                                <br>
                                                <i class="fa fa-bus" style="font-size: 48px;"></i>
                                                <br>
                                                <br>
                                                <h5>Surtir orden de material</h5>
                                                <br>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-4 ">
                                <div class="card" style="background-image: linear-gradient( 45deg , #000000, #ff0404); height: 216px;">
                                    <a style="color:#ffffff" class="card-body card-block buttom_item" href="<?php echo base_url();?>Salida/surtir_vale_material">
                                        <div class="row">
                                            <div class="col-md-12 text-center" style="min-height: 104px;">
                                                <br>
                                                <i class="fa fa-check-square" style="font-size: 48px;"></i>
                                                <br>
                                                <br>
                                                <h5>Surtir vale de material</h5>
                                                <br>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-4 ">
                                <div class="card" style="background-image: linear-gradient( 45deg , #000000, #ff0404); height: 216px;">
                                    <a style="color:#ffffff" class="card-body card-block buttom_item" href="<?php echo base_url();?>Salida/surtir_consumible_listado">
                                        <div class="row">
                                            <div class="col-md-12 text-center" style="min-height: 104px;">
                                                <br>
                                                <i class="fa fa-wrench" style="font-size: 48px;"></i>
                                                <br>
                                                <br>
                                                <h5>Surtir vale de consumibles</h5>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <!-- -->
                        <div class="row">
                            <div class="col-md-4 ">
                                <div class="card" style="background-image: linear-gradient( 45deg , #000000, #ff0404); height: 216px;">
                                    <a style="color:#ffffff" class="card-body card-block buttom_item" href="<?php echo base_url();?>Salida/surtir_papeleria_listado">
                                        <div class="row">
                                            <div class="col-md-12 text-center" style="min-height: 104px;">
                                                <br>
                                                <i class="fa fa-bus" style="font-size: 48px;"></i>
                                                <br>
                                                <br>
                                                <h5>Surtir vale de papelería</h5>
                                                <br>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-4 ">
                                <div class="card" style="background-image: linear-gradient( 45deg , #000000, #ff0404); height: 216px;">
                                    <a style="color:#ffffff" class="card-body card-block buttom_item" href="<?php echo base_url();?>Salida/surtir_herramienta_listado">
                                        <div class="row">
                                            <div class="col-md-12 text-center" style="min-height: 104px;">
                                                <br>
                                                <i class="fa fa-check-square" style="font-size: 48px;"></i>
                                                <br>
                                                <br>
                                                <h5>Surtir vale de herramienta</h5>
                                                <br>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-4 ">
                                <div class="card" style="background-image: linear-gradient( 45deg , #000000, #ff0404); height: 216px;">
                                    <a style="color:#ffffff" class="card-body card-block buttom_item" href="<?php echo base_url();?>Salida/surtir_otro_listado">
                                        <div class="row">
                                            <div class="col-md-12 text-center" style="min-height: 104px;">
                                                <br>
                                                <i class="fa fa-wrench" style="font-size: 48px;"></i>
                                                <br>
                                                <br>
                                                <h5>Surtir vale otros</h5>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>