<?php $perfil=$_SESSION['perfilid']; 
      $sucursal=$_SESSION['sucursal'];
?>
<section class="basic-elements">
    <div class="row">
        <div class="col-sm-12">
            <div class="content-header">Surtir vale de herramienta</div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title mb-0"></h4>
                </div>
                <div class="card-body">
                    <div class="px-3">
                        <form class="form" id="form-orden">
                            <div class="row form-group">
                                <label class="col-md-2">Quien solicita:</label>
                                <div class="col-md-4">
                                    <input type="hidden" name="Id" id="Idherramienta" class="form-control" value="<?php echo $Id ?>">
                                    <input type="text"  class="form-control" name="solicita" id="solicita" value="<?php echo !$solicita ? $user_session : $solicita ?>">
                                    <!--
                                    <select class="form-control" id="personalId" name="personalId">
                                        <?php /*
                                        if($personalId!=0){
                                            echo '<option value="'.$personalId.'">'.$personal.'</option>';
                                        } */
                                       ?>  
                                    </select> -->
                                </div>
                                <label class="col-md-2" >Fecha de solicitud:</label>
                                <div class="col-md-4">
                                    <input type="date" name="fecha_solicitud" id="fecha_solicitud" class="form-control" value="<?php echo $fecha_solicitud ?>">
                                </div>
                            </div>
                            <div class="row form-group">
                                <label class="col-md-2">Nave:</label>
                                <div class="col-md-4">
                                    <select class="form-control" id="area" name="area"> 
                                        <?php 
                                        foreach ($sucursalrow->result() as $x){ 
                                            if($x->sucursalId==$sucursal){ ?>
                                                <option value="<?php echo $x->sucursalId ?>" <?php if($x->sucursalId==$area) echo 'selected' ?> ><?php echo $x->sucursal ?></option>     
                                           <?php 
                                            }else{ 
                                                if($perfil==1){ ?>
                                                    <option value="<?php echo $x->sucursalId ?>" <?php if($x->sucursalId==$area) echo 'selected' ?> ><?php echo $x->sucursal ?></option>
                                                <?php }
                                            }
                                        }
                                        ?>  
                                    </select>
                                </div>
                            </div>
                        </form>
                        <div class="row form-group">
                            <div class="col-md-12">
                                <hr>
                            </div>
                        </div>
                        <div class="row form-group">
                            <label class="col-md-2">Cantidad:</label>
                            <div class="col-md-4">
                                <input type="number" id="s_cantidad" class="form-control" >
                            </div>
                            <label class="col-md-2">Producto:</label>
                            <div class="col-md-4">
                                <select class="form-control" id="s_producto" name="s_producto"></select>
                            </div>
                        </div>
                        <div class="row form-group">
                            <label class="col-md-2">Motivo:</label>
                            <div class="col-md-3">
                                <input type="radio" name="motivo" value="1" id="motivo1">
                                <label for="motivo1">Cambio por desgaste</label>
                            </div>
                            <div class="col-md-3">
                                <input type="radio" name="motivo" value="2" id="motivo2">
                                <label for="motivo2">Surtimiento de insumo</label>
                            </div>
                            <div class="col-md-1">
                                <input type="radio" name="motivo" value="3" id="motivo3">
                                <label for="motivo3">Otro</label>
                            </div>
                            <div class="col-md-3 s_motivo_otro" style="display:none;">
                                <input type="text" id="s_motivo_otro" class="form-control">
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-md-12">
                                <button type="button" class="btn btn-raised btn-danger button_save" style="float: right;" onclick="info_producto()"><i class="fa fa-plus"></i> Agregar</button>
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-md-12">
                                <table class="table thead-inverse" id="table_otros">
                                    <thead>
                                        <tr>
                                            <th>Cantidad</th>
                                            <th>Código pieza</th>
                                            <th>Producto</th>
                                            <th>Motivo</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody class="table_otros_tbody">
                                        
                                        
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        

                        <div class="row form-group">
                            <div class="col-md-12">
                                <button type="button" class="btn btn-dark btn-lg button_save" style="float: right;" onclick="save()"><i class="fa fa-save"></i> Procesar orden</button>
                                <a href="<?php echo base_url();?>Salida/surtir_herramienta_listado" type="button" class="btn btn-raised btn-dark btn-min-width mr-1 mb-1 btn-lg"><i class="fa fa-mail-reply"></i> Regresar</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>