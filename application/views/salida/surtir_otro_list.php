<style type="text/css">
    a {
        color: #b7171f;
    }
    #table-data-row td{
        font-size: 15px;
    }
</style>
<style type="text/css">
    .dataTables_wrapper{
        padding: 0px;
    }
    #table-lis td{
        font-size: 14px;
    }
</style>
<section class="basic-elements">
    <div class="row">
        <div class="col-sm-12">
            <div class="content-header">Surtir otro</div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <div class="px-3">
                        <br>
                        <div class="row">
                            <div class="col-md-6">
                                <a href="<?php echo base_url();?>Salida/surtir_otro" type="button" class="btn btn-raised btn-danger btn-min-width mr-1 mb-1 button_save" onclick="save()"><i class="fa fa-plus"></i> Agregar</a>
                            </div>
                            <div class="col-md-6" align="right">
                                <a href="<?php echo base_url();?>Salida" type="button" class="btn btn-raised btn-dark btn-min-width mr-1 mb-1"><i class="fa fa-mail-reply"></i> Regresar</a>
                            </div>
                            <div class="col-md-12">
                                <table class="table thead-inverse" id="table_datos">
                                    <thead >
                                        <tr>
                                            <th>#</th>
                                            <th>QUIÉN<span style="color: #ff000000">_</span>SOLICITA</th>
                                            <th>NAVE</th>
                                            <th>FECHA<span style="color: #ff000000">_</span>SOLICITUD</th>
                                            <th></th>
                                            <th><span style="color: #ff000000">_______________</span></th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<div class="modal fade text-left" id="modal_producto" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" style="display: none;" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel1">Productos</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <table class="table thead-inverse" id="table_otros">
                            <thead>
                                <tr>
                                    <th>Cantidad</th>
                                    <th>Código pieza</th>
                                    <th>Producto</th>
                                    <th>Motivo</th>
                                </tr>
                            </thead>
                            <tbody class="table_otros_tbody">
        
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn grey btn-outline-secondary" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>