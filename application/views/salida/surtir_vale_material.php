<?php $perfil=$_SESSION['perfilid']; 
      $sucursal=$_SESSION['sucursal'];
?>
<section class="basic-elements">
    <div class="row">
        <div class="col-sm-12">
            <div class="content-header">Surtir vale de material</div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title mb-0"></h4>
                </div>
                <div class="card-body">
                    <div class="px-3">
                        <form class="form" id="form-orden">
                            <div class="row form-group">
                                <label class="col-md-2">Quien Solicita:</label>
                                <input type="hidden" name="valeId" id="valeId" value="<?php echo $valeId ?>">
                                <div class="col-md-4">
                                    <input type="text"  class="form-control" name="solicita" id="solicita" value="<?php echo !$solicita ? $user_session : $solicita  ?>">
                                    <!--
                                    <select class="form-control" id="personal_solicitante" name="personal_solicitante">
                                    <?php /*
                                    if($personal_solicitante!=0){
                                        echo '<option value="'.$personal_solicitante.'">'.$persona.'</option>';
                                    } */?>
                                    </select>
                                     -->
                                </div>
                                <label class="col-md-2">Fecha de solicitud:</label>
                                <div class="col-md-4">
                                    <input type="date"  class="form-control" name="fecha_solicitud" id="fecha_solicitud" value="<?php echo $fecha_solicitud ?>">
                                </div>
                            </div>
                            <div class="row form-group">
                                <label class="col-md-2">Nave origen:</label>
                                <div class="col-md-4">
                                    <select class="form-control" id="origen" name="origen">
                                        <?php foreach ($sucursalrow->result() as $x){ 
                                            if($x->sucursalId==$sucursal){ ?>
                                                <option value="<?php echo $x->sucursalId ?>" <?php if($x->sucursalId==$origen) echo 'selected' ?> ><?php echo $x->sucursal ?></option>     
                                            <?php 
                                            }else{ 
                                                if($perfil==1){ ?>
                                                    <option value="<?php echo $x->sucursalId ?>" <?php if($x->sucursalId==$origen) echo 'selected' ?> ><?php echo $x->sucursal ?></option>     
                                                <?php }
                                            }
                                        } ?> 
                                    </select>
                                </div>
                                <label class="col-md-2">Nave destino:</label>
                                <div class="col-md-4">
                                    <select class="form-control" name="destino" id="destino">
                                        <option selected="" disabled="">Selecciona una opción</option>
                                        <?php foreach ($sucursalrow->result() as $item) { ?>
                                            <option value="<?php echo $item->sucursalId;?>" <?php if($item->sucursalId==$destino) echo 'selected' ?>><?php echo $item->sucursal;?></option>
                                        <?php } ?> 
                                    </select>
                                </div>
                            </div>
                        </form>
                        <div class="row">
                            <div class="col-md-12"><hr></div>
                        </div>
                        <div class="row">
                            <label class="col-md-2">Cantidad</label>
                            <div class="col-md-3">
                                <input type="number"  class="form-control" id="cantidad_select" name="cantidad_select">
                            </div>
                            <label class="col-md-2">Producto</label>
                            <div class="col-md-3">
                                <select class="form-control" id="productoId_select" name="productoId_select">
                                    
                                </select>
                            </div>
                            <div class="col-md-2">
                                <button type="button" class="btn btn-raised btn-danger" style="float: right;" onclick="addproducto_dato()"><i class="fa fa-plus"></i> Agregar</button>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-md-12">
                                <table class="table thead-inverse" id="table-produtos">
                                    <thead>
                                        <tr>
                                            <th>Cantidad</th>
                                            <th>Código pieza</th>
                                            <th>Pieza</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody class="table-produtos_tbody">
                                        
                                    </tbody>
                                    
                                </table>
                            </div>
                        </div>
                        

                        <div class="row form-group">
                            <div class="col-md-12">
                                <button type="button" class="btn btn-dark btn-lg button_save" style="float: right;" onclick="save()"><i class="fa fa-save"></i> Procesar orden</button>
                                <a href="<?php echo base_url();?>Salida/surtir_vale_material" type="button" class="btn btn-raised btn-dark btn-min-width mr-1 mb-1 btn-lg"><i class="fa fa-mail-reply"></i> Regresar</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>