<style type="text/css">
    #table-data-row td{
        font-size: 15px;
    }
</style>
<section class="basic-elements">
    <div class="row">
        <div class="col-sm-12">
            <div class="content-header">Surtir vale de material</div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <div class="px-3">
                        <br>
                        <div class="row">
                            <div class="col-md-6">
                                <a href="<?php echo base_url();?>Salida/surtir_vale_material_add" type="button" class="btn btn-raised btn-danger btn-min-width mr-1 mb-1"><i class="fa fa-plus"></i> Agregar</a>
                            </div>
                            <div class="col-md-6" align="right">
                                <a href="<?php echo base_url();?>Salida" type="button" class="btn btn-raised btn-dark btn-min-width mr-1 mb-1"><i class="fa fa-mail-reply"></i> Regresar</a>
                            </div>
                            <div class="col-md-12">
                                <table class="table thead-inverse" id="table-data-row">
                                    <thead >
                                        <tr>
                                            <th>#</th>
                                            <th>Solicitante</th>
                                            <th>Destino</th>
                                            <th>Fecha Solicitud</th>
                                            <th><span style="color: #ff000000">________</span></th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>