<style type="text/css">
  :root {
    --color-green: #05a8e1;
    --color-secondary: #b9b9b9;
    --color-button: white;
    --color-black: white;
  }


  .switch-button .switch-button__checkbox {
    display: none;
  }

  .switch-button .switch-button__label {
    background-color: var(--color-secondary);
    width: 3rem;
    height: 1.5rem;
    border-radius: 1rem;
    display: inline-block;
    position: relative;
  }

  .switch-button .switch-button__label:before {
    transition: .2s;
    display: block;
    position: absolute;
    width: 1.5rem;
    height: 1.5rem;
    background-color: var(--color-button);
    content: '';
    border-radius: 50%;
    box-shadow: inset 0px 0px 0px 1px var(--color-black);
  }

  .switch-button .switch-button__checkbox:checked+.switch-button__label {
    background-color: #bd1620;
  }

  .switch-button .switch-button__checkbox:checked+.switch-button__label:before {
    transform: translateX(1.5rem);
  }

  .formulario{
    display: block;
  }

  .mostrar_carro_btn{
    display: none;
  }
  
  .mostrar_carro{
    display: block;
  }
  
  .text_firma_1{
    display: block;
  }

  .text_firma_2{
    display: block;
  }
   
  .mostrar_firmas_btn1{
    display: none;
  }

  .mostrar_firmas_btn2{
    display: none;
  }
   
  @media all and (max-width: 750px){

    .formulario{
      display: block;
    }

    .mostrar_carro_btn{
      display: block;
    }

    .mostrar_carro{
      display: none;
    }
    
    .text_firma_1{
      display: none;
    }

    .text_firma_2{
      display: none;
    }

    .mostrar_firmas_btn1{
      display: block;
    }

    .mostrar_firmas_btn2{
      display: block;
    }
    
  }

</style>
<input type="hidden" id="idfile_carro" value="<?php echo $file_carro ?>">
<input type="hidden" id="file_firma1" value="<?php echo $file_firma1 ?>">
<input type="hidden" id="file_firma2" value="<?php echo $file_firma2 ?>">
<div class="row">
  <div class="col-sm-12">
    <div class="card">
      <div class="card-header">
        <h4 class="card-title formulario">Hoja de servicio</h4>
        <hr class="formulario">
        <form class="form" method="post"  role="form" id="form_data">
          <input type="hidden" name="id_venta" id="id_venta" value="<?php echo $id_venta ?>">
          <div class="formulario">
            <div class="row">
              <div class="col-md-12">
                <h5 style="text-align:right;">Folio:<?php echo $id_venta ?></h5>
              </div>
            </div>  
            <div class="row">
              <div class="col-md-3">
                <div class="form-group">
                  <label>Hora entrada:</label>
                  <input type="time" class="form-control" name="hora_entrada" value="<?php echo $hora_entrada ?>" oninput="update_registro()">
                </div>
              </div>
              <div class="col-md-3">
                <div class="form-group">
                  <label>Hora salida:</label>
                  <input type="time" class="form-control" name="hora_salida" value="<?php echo $hora_salida ?>" oninput="update_registro()">
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-8">
                <div class="form-group">
                  <label>Nombre:</label>
                  <input type="hidden" id="id_cliente" value="<?php echo $id_cliente ?>">
                  <input type="text" class="form-control" id="nombre"  value="<?php echo $nombre ?>" oninput="update_registro_cliente()">
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group">
                  <label>RFC:</label>
                  <input type="text" class="form-control" id="rfc" value="<?php echo $rfc ?>" oninput="update_registro_cliente()">
                </div>
              </div>
            </div>
            <div class="row">  
              <div class="col-md-12">
                <div class="form-group">
                  <label>Dirección:</label>
                  <input type="text" class="form-control" id="direccion" value="<?php echo $direccion ?>" oninput="update_registro_cliente()">
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-3">
                <div class="form-group">
                  <label>Ciudad:</label>
                  <input type="text" class="form-control" id="estado" value="<?php echo $estado ?>" oninput="update_registro_cliente()">
                </div>
              </div>
              <div class="col-md-3">
                <div class="form-group">
                  <label>C.p.:</label>
                  <input type="text" class="form-control" id="codigo_postal" value="<?php echo $codigo_postal ?>" oninput="update_registro_cliente()">
                </div>
              </div>
              <div class="col-md-3">
                <div class="form-group">
                  <label>Tel:</label>
                  <input type="text" class="form-control" id="celular" value="<?php echo $celular ?>" oninput="update_registro_cliente()">
                </div>
              </div>
              <div class="col-md-3">
                <div class="form-group">
                  <label>Recibe:</label>
                  <input type="text" class="form-control" name="recibe" value="<?php echo $recibe ?>" oninput="update_registro()">
                </div>
              </div>
            </div>  
            <div class="row">
              <div class="col-md-3">
                <div class="form-group">
                  <label>Fecha:</label>
                  <input type="date" class="form-control" name="fecha" value="<?php echo $fecha ?>" oninput="update_registro()">
                </div>
              </div>
              <div class="col-md-3">
                <div class="form-group">
                  <label>Tipo:</label>
                  <input type="text" class="form-control" name="tipo" value="<?php echo $tipo ?>" oninput="update_registro()">
                </div>
              </div>
              <div class="col-md-3">
                <div class="form-group">
                  <label>Placas:</label>
                  <input type="text" class="form-control" name="placas" readonly value="<?php echo $placas ?>" oninput="update_registro()">
                </div>
              </div>
              <div class="col-md-3">
                <div class="form-group">
                  <label>Iva desblosado:</label><br>
                  <div class="switch-button col-md-1">
                    <input type="checkbox" id="iva_desglosado" name="iva_desglosado" class="switch-button__checkbox" onclick="update_registro()" <?php if($iva_desglosado==1) echo 'checked' ?>>
                    <label for="iva_desglosado" class="switch-button__label"></label>
                  </div>
                </div>
              </div>
            </div>
            <hr>
            <div class="row">
              <div class="col-md-3">
                <div class="btn-group" role="group">
                  <label>Alineación</label>
                    <div class="form-group">
                      <div class="switch-button col-md-1">
                        <input type="checkbox" id="aliniacion" name="aliniacion" class="switch-button__checkbox" onclick="update_registro()" <?php if($aliniacion==1) echo 'checked' ?>>
                        <label for="aliniacion" class="switch-button__label"></label>
                      </div>
                    </div>
                </div>
              </div>
              <div class="col-md-3">
                <div class="btn-group" role="group">
                  <label>Válvulas</label>
                  <div class="form-group">
                    <div class="switch-button col-md-1">
                      <input type="checkbox" id="valvulas" name="valvulas" class="switch-button__checkbox" onclick="update_registro()" <?php if($valvulas==1) echo 'checked' ?>>
                      <label for="valvulas" class="switch-button__label"></label>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-md-3">
                <div class="btn-group" role="group">
                  <label>Balanceo</label>
                  <div class="form-group">
                    <div class="switch-button col-md-1">
                      <input type="checkbox" id="balance" name="balance" class="switch-button__checkbox" onclick="update_registro()" <?php if($balance==1) echo 'checked' ?>>
                      <label for="balance" class="switch-button__label"></label>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-md-3">
                <div class="btn-group" role="group">
                  <label>Montajes</label>
                    <div class="form-group">
                      <div class="switch-button col-md-1">
                        <input type="checkbox" id="montajes" name="montajes" class="switch-button__checkbox" onclick="update_registro()" <?php if($montajes==1) echo 'checked' ?>>
                        <label for="montajes" class="switch-button__label"></label>
                      </div>
                    </div>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-3">
                <div class="btn-group" role="group">
                  <label>Afinación</label>
                    <div class="form-group">
                      <div class="switch-button col-md-1">
                        <input type="checkbox" id="afinacion" name="afinacion" class="switch-button__checkbox" onclick="update_registro()" <?php if($afinacion==1) echo 'checked' ?>>
                        <label for="afinacion" class="switch-button__label"></label>
                      </div>
                    </div>
                </div>
              </div>
              <div class="col-md-3">
                <div class="btn-group" role="group">
                  <label>Rotación</label>
                    <div class="form-group">
                      <div class="switch-button col-md-1">
                        <input type="checkbox" id="rotacion" name="rotacion" class="switch-button__checkbox" onclick="update_registro()" <?php if($rotacion==1) echo 'checked' ?>>
                        <label for="rotacion" class="switch-button__label"></label>
                      </div>
                    </div>
                </div>
              </div>
              <div class="col-md-3">
                <div class="btn-group" role="group">
                  <label>Parches</label>
                    <div class="form-group">
                      <div class="switch-button col-md-1">
                        <input type="checkbox" id="parches" name="parches" class="switch-button__checkbox" onclick="update_registro()" <?php if($parches==1) echo 'checked' ?>>
                        <label for="parches" class="switch-button__label"></label>
                      </div>
                    </div>
                </div>
              </div>
            </div>
            <hr>
            <div class="row">
              <div class="col-md-2">
                <div class="form-group">
                  <h6><b>Llantas</b></h6>
                </div>
              </div>
              <div class="col-md-2">
                <div class="form-group">
                  <label>Cantidad:</label>
                  <input type="number" name="l_cantida" value="<?php echo $l_cantida ?>" class="form-control" oninput="update_registro()">
                </div>
              </div>
              <div class="col-md-3">
                <div class="form-group">
                  <label>Media:</label>
                  <input type="text" name="l_media" value="<?php echo $l_media ?>" class="form-control" oninput="update_registro()">
                </div>
              </div>
              <div class="col-md-1">
                <div class="form-group">
                  <h6><b>Aceite:</b></h6>
                </div>
              </div>
              <div class="col-md-2">
                <div class="form-group">
                  <label>Cant:</label>
                  <input type="number" class="form-control" name="a_cantidad" value="<?php echo $a_cantidad ?>" oninput="update_registro()">
                </div>
              </div>
              <div class="col-md-2">
                <div class="form-group">
                  <label>Media:</label>
                  <input type="text" class="form-control" name="a_media" value="<?php echo $a_media ?>" oninput="update_registro()">
                </div>
              </div>
            </div>
            <hr>
            <div class="row">
              <div class="col-md-2">
                <div class="form-group">
                  <h6><b>Camaras</b></h6>
                </div>
              </div>
              <div class="col-md-2">
                <div class="form-group">
                  <label>Cantidad:</label>
                  <input type="number" class="form-control" name="c_cadidad" value="<?php echo $c_cadidad ?>" oninput="update_registro()">
                </div>
              </div>
              <div class="col-md-3">
                <div class="form-group">
                  <label>Media:</label>
                  <input type="text" class="form-control" name="c_media" value="<?php echo $c_media ?>" oninput="update_registro()">
                </div>
              </div>
              <div class="col-md-5">
                <div class="form-group">
                  <label>Amortiguador:</label>
                  <input type="text" class="form-control" name="c_amortiguador" value="<?php echo $c_amortiguador ?>" oninput="update_registro()">
                </div>
              </div>
              <div class="col-md-1">
                <h6><b>Marca:</b></h6>
              </div>
              <div class="col-md-1">
                <div class="form-group">
                  <label>D:</label>
                  <input type="text" class="form-control" name="c_d" value="<?php echo $c_d ?>" oninput="update_registro()">
                </div>
              </div>
              <div class="col-md-1">
                <div class="form-group">
                  <label>T:</label>
                  <input type="text" class="form-control" name="c_t" value="<?php echo $c_t ?>" oninput="update_registro()">
                </div>
              </div>
              <div class="col-md-2">
                <div class="form-group">
                  <h6><b>Filtro:</b></h6>
                </div>
              </div>
              <div class="col-md-2">
                <div class="form-group">
                  <label>Cant:</label>
                  <input type="text" class="form-control" name="f_cantidad" value="<?php echo $f_cantidad ?>" oninput="update_registro()">
                </div>
              </div>
              <div class="col-md-2">
                <div class="form-group">
                  <label>Tipo:</label>
                  <input type="text" class="form-control" name="f_tipo" value="<?php echo $f_tipo ?>" oninput="update_registro()">
                </div>
              </div>
            </div>  
            <hr>
            <div class="row">
              <div class="col-md-2">
                <div class="form-group">
                  <h6><b>Corbata</b></h6>
                </div>
              </div>
              <div class="col-md-2">
                <div class="form-group">
                  <label>Cantidad:</label>
                  <input type="number" class="form-control" name="co_cantidad" value="<?php echo $co_cantidad ?>" oninput="update_registro()">
                </div>
              </div>
              <div class="col-md-3">
                <div class="form-group">
                  <label>Tipo:</label>
                  <input type="text" class="form-control" name="co_tipo" value="<?php echo $co_tipo ?>" oninput="update_registro()">
                </div>
              </div>
              <div class="col-md-3">
                <div class="form-group">
                  <label>Ins.Amortiguadores:</label>
                  <input type="text" class="form-control" name="co_ins_amortiguador" value="<?php echo $co_ins_amortiguador ?>" oninput="update_registro()">
                </div>
              </div>
              <div class="col-md-1">
                <div class="form-group">
                  <label>N:</label>
                  <input type="text" class="form-control" name="co_n" value="<?php echo $co_n ?>" oninput="update_registro()">
                </div>
              </div>
              <div class="col-md-1">
                <div class="form-group">
                  <label>E:</label>
                  <input type="text" class="form-control" name="co_e" value="<?php echo $co_e ?>" oninput="update_registro()">
                </div>
              </div>
              <div class="col-md-2">
                <div class="form-group">
                  <h6><b>Balatas:</b></h6>
                </div>
              </div>
              <div class="col-md-1">
                <div class="form-group">
                  <label>D:</label>
                  <input type="text" class="form-control" name="ba_d" value="<?php echo $ba_d ?>" oninput="update_registro()">
                </div>
              </div>
              <div class="col-md-2">
                <div class="form-group">
                  <label>T:</label>
                  <input type="text" class="form-control" name="ba_t" value="<?php echo $ba_t ?>" oninput="update_registro()">
                </div>
              </div>
            </div>
            <hr>
            <div class="row">  
              <div class="col-md-12">
                <div class="form-group">
                  <label>E-mail:</label>
                  <input type="email" class="form-control" id="correo" value="<?php echo $correo ?>" oninput="update_registro_cliente()">
                </div>
              </div>
            </div>
            <div class="row">  
              <div class="col-md-12">
                <div class="form-group">
                  <label>Mano de obra:</label>
                  <textarea class="form-control" rows="3" name="mano_obra" oninput="update_registro()"><?php echo $mano_obra ?></textarea>
                </div>
              </div>
            </div>
            <div class="row">  
              <div class="col-md-12">
                <div class="form-group">
                  <label>Otros:</label>
                  <input type="text" class="form-control" name="otros" value="<?php echo $otros ?>" oninput="update_registro()">
                </div>
              </div>
            </div>
            <hr>
          </div>  
          <div class="mostrar_carro_btn">
            <div class="row">  
              <div class="col-md-12">
                <a class="btn btn-raised btn-dark btn-min-width mr-1 mb-1 btn-lg title_carro" style="color: white" onclick="btn_mostrar_carro()">
                  Mostrar carro
                </a>
              </div>
            </div>
          </div>
            <div class="mostrar_carro">
            <?php if($file_carro!=''){ 
              //$fh = fopen(base_url()."uploads/servicio_carro/".$file_carro, 'r') or die("Se produjo un error al abrir el archivo");
              $linea = base_url()."uploads/servicio_carro/".$file_carro;//fgets($fh);
              ?>
              <div class="div_carro">
                <div class="row">
                  <div class="col-md-12" id="aceptance">
                    <canvas id="patientSignature" style="display: none"></canvas>
                    <div style="text-align:center;margin-top:0.05cm;padding:0.1cm;">
                        <img style="width: 975px; height: 627px; border: 2px dashed rgb(29, 175, 147); background: url(<?php echo base_url()?>public/img/cark.png);background-repeat:no-repeat; background-position:center;" src="<?php echo $linea ?>" width="975" height="627" style="border:dotted 1px black;">
                        <button type="button" class="btn btn-raised btn-dark btn-min-width mr-1 mb-1" onclick="editar_servicio_carro()">Editar</button>
                    </div>
                  </div>  
                </div>    
              </div>
            <?php }else{ ?>
              <div class="div_carro">
                <div class="row">
                  <div class="col-md-12" id="aceptance">
                      <div id="signature" class="signature">
                      <label for="patientSignature" class="sighiddeable hidden-xs hidden-sm marginTop text-md"></label>
                          <canvas id="patientSignature" class="sighiddeable hidden-xs hidden-sm" width="975" height="627" style="border: 2px dashed rgb(29, 175, 147); cursor: crosshair;"></canvas>
                          <img src="<?php echo base_url() ?>public/img/icon_borrar.png" class="sighiddeable hidden-xs hidden-sm clearSignature cargarcarro" data-signature="patientSignature">
                      </div>
                  </div>
                </div>
              </div> 
            <?php } ?>  
            </div>  
          <div class="formulario">
            <div class="row">  
              <div class="col-md-4">
                <div class="form-group">
                  <label>Km.</label>
                  <input type="text" class="form-control" name="km" value="<?php echo $km ?>" oninput="update_registro()">
                </div>
              </div>
            </div>
            <h6 style="text-transform: uppercase !important;">10 Puntos de seguridad</h6>
            <div class="row">
              <div class="col-md-3">
                1. Llantas
              </div>
              <div class="col-md-3">
                <div class="btn-group">
                  <label>Bueno</label>
                    <div class="form-group">
                      <div class="switch-button col-md-1">
                        <input type="checkbox" id="b1" name="b1" class="switch-button__checkbox" onclick="update_registro()" <?php if($b1==1) echo 'checked' ?>>
                        <label for="b1" class="switch-button__label"></label>
                      </div>
                    </div>
                </div>
              </div>  
              <div class="col-md-3">
                <div class="btn-group">
                  <label>Regular</label>
                    <div class="form-group">
                      <div class="switch-button col-md-1">
                        <input type="checkbox" id="r1" name="r1" class="switch-button__checkbox" onclick="update_registro()" <?php if($r1==1) echo 'checked' ?>>
                        <label for="r1" class="switch-button__label"></label>
                      </div>
                    </div>
                </div>
              </div>
              <div class="col-md-3">
                <div class="btn-group">
                  <label>Malo</label>
                    <div class="form-group">
                      <div class="switch-button col-md-1">
                        <input type="checkbox" id="m1" name="m1" class="switch-button__checkbox" onclick="update_registro()" <?php if($m1==1) echo 'checked' ?>>
                        <label for="m1" class="switch-button__label"></label>
                      </div>
                    </div>
                </div>
              </div>
            </div>  
            <div class="row">
              <div class="col-md-3">
                2. Amortiguadores
              </div>
              <div class="col-md-3">
                <div class="btn-group" role="group">
                  <label>Bueno</label>
                    <div class="form-group">
                      <div class="switch-button col-md-1">
                        <input type="checkbox" id="b2" name="b2" class="switch-button__checkbox" onclick="update_registro()" <?php if($b2==1) echo 'checked' ?>>
                        <label for="b2" class="switch-button__label"></label>
                      </div>
                    </div>
                </div>
              </div>  
              <div class="col-md-3">
                <div class="btn-group" role="group">
                  <label>Regular</label>
                    <div class="form-group">
                      <div class="switch-button col-md-1">
                        <input type="checkbox" id="r2" name="r2" class="switch-button__checkbox" onclick="update_registro()" <?php if($r2==1) echo 'checked' ?>>
                        <label for="r2" class="switch-button__label"></label>
                      </div>
                    </div>
                </div>
              </div>
              <div class="col-md-3">
                <div class="btn-group" role="group">
                  <label>Malo</label>
                    <div class="form-group">
                      <div class="switch-button col-md-1">
                        <input type="checkbox" id="m2" name="m2" class="switch-button__checkbox" onclick="update_registro()" <?php if($m2==1) echo 'checked' ?>>
                        <label for="m2" class="switch-button__label"></label>
                      </div>
                    </div>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-3">
                3. Frenos
              </div>
              <div class="col-md-3">
                <div class="btn-group" role="group">
                  <label>Bueno</label>
                    <div class="form-group">
                      <div class="switch-button col-md-1">
                        <input type="checkbox" id="b3" name="b3" class="switch-button__checkbox" onclick="update_registro()" <?php if($b3==1) echo 'checked' ?>>
                        <label for="b3" class="switch-button__label"></label>
                      </div>
                    </div>
                </div>
              </div>  
              <div class="col-md-3">
                <div class="btn-group" role="group">
                  <label>Regular</label>
                    <div class="form-group">
                      <div class="switch-button col-md-1">
                        <input type="checkbox" id="r3" name="r3" class="switch-button__checkbox" onclick="update_registro()" <?php if($r3==1) echo 'checked' ?>>
                        <label for="r3" class="switch-button__label"></label>
                      </div>
                    </div>
                </div>
              </div>
              <div class="col-md-3">
                <div class="btn-group" role="group">
                  <label>Malo</label>
                    <div class="form-group">
                      <div class="switch-button col-md-1">
                        <input type="checkbox" id="m3" name="m3" class="switch-button__checkbox" onclick="update_registro()" <?php if($m3==1) echo 'checked' ?>>
                        <label for="m3" class="switch-button__label"></label>
                      </div>
                    </div>
                </div>
              </div>
            </div>  
            <div class="row">
              <div class="col-md-3">
                4. Alineación
              </div>
              <div class="col-md-3">
                <div class="btn-group" role="group">
                  <label>Bueno</label>
                    <div class="form-group">
                      <div class="switch-button col-md-1">
                        <input type="checkbox" id="b4" name="b4" class="switch-button__checkbox" onclick="update_registro()" <?php if($b4==1) echo 'checked' ?>>
                        <label for="b4" class="switch-button__label"></label>
                      </div>
                    </div>
                </div>
              </div>  
              <div class="col-md-3">
                <div class="btn-group" role="group">
                  <label>Regular</label>
                    <div class="form-group">
                      <div class="switch-button col-md-1">
                        <input type="checkbox" id="r4" name="r4" class="switch-button__checkbox" onclick="update_registro()" <?php if($r4==1) echo 'checked' ?>>
                        <label for="r4" class="switch-button__label"></label>
                      </div>
                    </div>
                </div>
              </div>
              <div class="col-md-3">
                <div class="btn-group" role="group">
                  <label>Malo</label>
                    <div class="form-group">
                      <div class="switch-button col-md-1">
                        <input type="checkbox" id="m4" name="m4" class="switch-button__checkbox" onclick="update_registro()" <?php if($m4==1) echo 'checked' ?>>
                        <label for="m4" class="switch-button__label"></label>
                      </div>
                    </div>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-3">
                5. Balanceo
              </div>
              <div class="col-md-3">
                <div class="btn-group" role="group">
                  <label>Bueno</label>
                    <div class="form-group">
                      <div class="switch-button col-md-1">
                        <input type="checkbox" id="b5" name="b5" class="switch-button__checkbox" onclick="update_registro()" <?php if($b5==1) echo 'checked' ?>>
                        <label for="b5" class="switch-button__label"></label>
                      </div>
                    </div>
                </div>
              </div>  
              <div class="col-md-3">
                <div class="btn-group" role="group">
                  <label>Regular</label>
                    <div class="form-group">
                      <div class="switch-button col-md-1">
                        <input type="checkbox" id="r5" name="r5" class="switch-button__checkbox" onclick="update_registro()" <?php if($r5==1) echo 'checked' ?>>
                        <label for="r5" class="switch-button__label"></label>
                      </div>
                    </div>
                </div>
              </div>
              <div class="col-md-3">
                <div class="btn-group" role="group">
                  <label>Malo</label>
                    <div class="form-group">
                      <div class="switch-button col-md-1">
                        <input type="checkbox" id="m5" name="m5" class="switch-button__checkbox" onclick="update_registro()" <?php if($m5==1) echo 'checked' ?>>
                        <label for="m5" class="switch-button__label"></label>
                      </div>
                    </div>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-3">
                6. Suspensión
              </div>
              <div class="col-md-3">
                <div class="btn-group" role="group">
                  <label>Bueno</label>
                    <div class="form-group">
                      <div class="switch-button col-md-1">
                        <input type="checkbox" id="b6" name="b6" class="switch-button__checkbox" onclick="update_registro()" <?php if($b6==1) echo 'checked' ?>>
                        <label for="b6" class="switch-button__label"></label>
                      </div>
                    </div>
                </div>
              </div>  
              <div class="col-md-3">
                <div class="btn-group" role="group">
                  <label>Regular</label>
                    <div class="form-group">
                      <div class="switch-button col-md-1">
                        <input type="checkbox" id="r6" name="r6" class="switch-button__checkbox" onclick="update_registro()" <?php if($r6==1) echo 'checked' ?>>
                        <label for="r6" class="switch-button__label"></label>
                      </div>
                    </div>
                </div>
              </div>
              <div class="col-md-3">
                <div class="btn-group" role="group">
                  <label>Malo</label>
                    <div class="form-group">
                      <div class="switch-button col-md-1">
                        <input type="checkbox" id="m6" name="m6" class="switch-button__checkbox" onclick="update_registro()" <?php if($m6==1) echo 'checked' ?>>
                        <label for="m6" class="switch-button__label"></label>
                      </div>
                    </div>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-3">
                7. Luces
              </div>
              <div class="col-md-3">
                <div class="btn-group" role="group">
                  <label>Bueno</label>
                    <div class="form-group">
                      <div class="switch-button col-md-1">
                        <input type="checkbox" id="b7" name="b7" class="switch-button__checkbox" onclick="update_registro()" <?php if($b7==1) echo 'checked' ?>>
                        <label for="b7" class="switch-button__label"></label>
                      </div>
                    </div>
                </div>
              </div>  
              <div class="col-md-3">
                <div class="btn-group" role="group">
                  <label>Regular</label>
                    <div class="form-group">
                      <div class="switch-button col-md-1">
                        <input type="checkbox" id="r7" name="r7" class="switch-button__checkbox" onclick="update_registro()" <?php if($r7==1) echo 'checked' ?>>
                        <label for="r7" class="switch-button__label"></label>
                      </div>
                    </div>
                </div>
              </div>
              <div class="col-md-3">
                <div class="btn-group" role="group">
                  <label>Malo</label>
                    <div class="form-group">
                      <div class="switch-button col-md-1">
                        <input type="checkbox" id="m7" name="m7" class="switch-button__checkbox" onclick="update_registro()" <?php if($m7==1) echo 'checked' ?>>
                        <label for="m7" class="switch-button__label"></label>
                      </div>
                    </div>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-3">
                8. Bandas,Mangueras
              </div>
              <div class="col-md-3">
                <div class="btn-group" role="group">
                  <label>Bueno</label>
                    <div class="form-group">
                      <div class="switch-button col-md-1">
                        <input type="checkbox" id="b8" name="b8" class="switch-button__checkbox" onclick="update_registro()" <?php if($b8==1) echo 'checked' ?>>
                        <label for="b8" class="switch-button__label"></label>
                      </div>
                    </div>
                </div>
              </div>  
              <div class="col-md-3">
                <div class="btn-group" role="group">
                  <label>Regular</label>
                    <div class="form-group">
                      <div class="switch-button col-md-1">
                        <input type="checkbox" id="r8" name="r8" class="switch-button__checkbox" onclick="update_registro()" <?php if($r8==1) echo 'checked' ?>>
                        <label for="r8" class="switch-button__label"></label>
                      </div>
                    </div>
                </div>
              </div>
              <div class="col-md-3">
                <div class="btn-group" role="group">
                  <label>Malo</label>
                    <div class="form-group">
                      <div class="switch-button col-md-1">
                        <input type="checkbox" id="m8" name="m8" class="switch-button__checkbox" onclick="update_registro()" <?php if($m8==1) echo 'checked' ?>>
                        <label for="m8" class="switch-button__label"></label>
                      </div>
                    </div>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-3">
                9. Limpiadores
              </div>
              <div class="col-md-3">
                <div class="btn-group" role="group">
                  <label>Bueno</label>
                    <div class="form-group">
                      <div class="switch-button col-md-1">
                        <input type="checkbox" id="b9" name="b9" class="switch-button__checkbox" onclick="update_registro()" <?php if($b9==1) echo 'checked' ?>>
                        <label for="b9" class="switch-button__label"></label>
                      </div>
                    </div>
                </div>
              </div>  
              <div class="col-md-3">
                <div class="btn-group" role="group">
                  <label>Regular</label>
                    <div class="form-group">
                      <div class="switch-button col-md-1">
                        <input type="checkbox" id="r9" name="r9" class="switch-button__checkbox" onclick="update_registro()" <?php if($r9==1) echo 'checked' ?>>
                        <label for="r9" class="switch-button__label"></label>
                      </div>
                    </div>
                </div>
              </div>
              <div class="col-md-3">
                <div class="btn-group" role="group">
                  <label>Malo</label>
                    <div class="form-group">
                      <div class="switch-button col-md-1">
                        <input type="checkbox" id="m9" name="m9" class="switch-button__checkbox" onclick="update_registro()" <?php if($m9==1) echo 'checked' ?>>
                        <label for="m9" class="switch-button__label"></label>
                      </div>
                    </div>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-3">
                10. Bateria
              </div>
              <div class="col-md-3">
                <div class="btn-group" role="group">
                  <label>Bueno</label>
                    <div class="form-group">
                      <div class="switch-button col-md-1">
                        <input type="checkbox" id="b10" name="b10" class="switch-button__checkbox" onclick="update_registro()" <?php if($b10==1) echo 'checked' ?>>
                        <label for="b10" class="switch-button__label"></label>
                      </div>
                    </div>
                </div>
              </div>  
              <div class="col-md-3">
                <div class="btn-group" role="group">
                  <label>Regular</label>
                    <div class="form-group">
                      <div class="switch-button col-md-1">
                        <input type="checkbox" id="r10" name="r10" class="switch-button__checkbox" onclick="update_registro()" <?php if($r10==1) echo 'checked' ?>>
                        <label for="r10" class="switch-button__label"></label>
                      </div>
                    </div>
                </div>
              </div>
              <div class="col-md-3">
                <div class="btn-group" role="group">
                  <label>Malo</label>
                    <div class="form-group">
                      <div class="switch-button col-md-1">
                        <input type="checkbox" id="m10" name="m10" class="switch-button__checkbox" onclick="update_registro()" <?php if($m10==1) echo 'checked' ?>>
                        <label for="m10" class="switch-button__label"></label>
                      </div>
                    </div>
                </div>
              </div>
            </div>
            <div class="row">  
              <div class="col-md-12">
                <div class="form-group">
                  <label>Nombre:</label>
                  <input type="text" class="form-control" id="id_cliente2" readonly="" value="<?php echo $nombre ?>">
                </div>
              </div>
            </div>
            <div class="row">  
              <div class="col-md-12">
                <div class="form-group">
                  <label>Auto:</label>
                  <input type="text" class="form-control" name="auto" readonly value="<?php echo $modelo ?>" oninput="update_registro()">
                </div>
              </div>
            </div>
            <div class="row">  
              <div class="col-md-12">
                <div class="form-group">
                  <label>Placas:</label>
                  <input type="text" class="form-control" name="placas2" readonly value="<?php echo $placas2 ?>">
                </div>
              </div>
            </div>
          </div>  
          <div class="mostrar_firmas_btn1">
            <div class="row">  
              <div class="col-md-12">
                <a class="btn btn-raised btn-dark btn-min-width mr-1 mb-1 btn-lg title_firma" style="color: white" onclick="btn_mostrar_firma1()">
                  Mostrar firma 1
                </a>
              </div>
            </div>
          </div> 
          <div class="mostrar_firmas_btn2">
            <div class="row">  
              <div class="col-md-12">
                <a class="btn btn-raised btn-dark btn-min-width mr-1 mb-1 btn-lg title_firma" style="color: white" onclick="btn_mostrar_firma2()">
                  Mostrar firma 2
                </a>
              </div>
            </div>
          </div>
          
            <div class="row">
              <div class="col-md-5">
                <div class="text_firma_1">
                <?php if($file_firma1==''){ ?>
                  <div class="canvas_firmafepc1">
                    <div id="signaturepfc1" class="signaturepfc1 col-lg-12 col-md-12  col-sm-12 col-xs-12" style="text-align:center;margin-bottom:10px;border-radius:4px;">
                        <label for="patientSignaturepfc1" class="sighiddeable hidden-xs hidden-sm marginTop text-md arial_text"></label>
                        <canvas id="patientSignaturepfc1" class="sighiddeable hidden-xs hidden-sm" width="300" height="180" style="width: 300px; height: 180px; border: 2px dashed rgb(29, 175, 147); cursor: crosshair;"></canvas>
                        <img src="<?php echo base_url() ?>public/img/icon_borrar.png" class="sighiddeable hidden-xs hidden-sm clearSignaturepfc1" data-signaturepfc1="patientSignaturepfc1">              
                    </div>  
                  </div>
                <?php }else{ 
                  //$fhfc1 = fopen(base_url()."uploads/servicio_firma1/".$file_firma1, 'r') or die("Se produjo un error al abrir el archivo");
                  $lineafc1 = base_url()."uploads/servicio_firma1/".$file_firma1; //fgets($fhfc1);
                  ?>
                  
                  <div class="canvas_firmafepc1"><br>
                      <canvas id="patientSignaturepfc1" style="display: none"></canvas>
                      <div style="text-align:center;margin-top:0.05cm;padding:0.1cm;">
                          <img style="width: 300px; height: 180px; border: 2px dashed rgb(29, 175, 147); background-position:center;" src="<?php echo $lineafc1 ?>" width="300" height="180" style="border:dotted 1px black;"><br>
                          <button type="button" class="btn btn-raised btn-dark btn-min-width mr-1 mb-1" onclick="firma_editar1()">Editar</button>
                      </div>
                  </div>
                <?php } ?>
                </div>
              </div>
              <div class="col-md-2"></div>
              <div class="col-md-5">
                <div class="text_firma_2">
                <?php if($file_firma2==''){ ?>
                    <div class="canvas_firmafepc2">
                      <div id="signaturecfc1" class="signaturecfc1 col-lg-12 col-md-12  col-sm-12 col-xs-12" style="text-align:center;margin-bottom:10px;border-radius:4px;">
                          <label for="patientsignatureccfc1" class="sighiddeable hidden-xs hidden-sm marginTop text-md arial_text"></label>
                          <canvas id="patientsignatureccfc1" class="sighiddeable hidden-xs hidden-sm" width="300" height="180" style="width: 300px; height: 180px; border: 2px dashed rgb(29, 175, 147); cursor: crosshair;"></canvas>
                          <img src="<?php echo base_url() ?>public/img/icon_borrar.png" class="sighiddeable hidden-xs hidden-sm clearsignaturecfc1" data-signaturecfc1="patientsignatureccfc1">              
                      </div>  
                    </div>  
                <?php }else{ 
                  //$fhfc2 = fopen(base_url()."uploads/servicio_firma2/".$file_firma2, 'r') or die("Se produjo un error al abrir el archivo");
                  $lineafc2 = base_url()."uploads/servicio_firma2/".$file_firma2;//fgets($fhfc2);
                  ?>

                  <div class="canvas_firmafepc2"><br>
                    <canvas id="patientsignatureccfc1" style="display: none"></canvas>
                    <div style="text-align:center;margin-top:0.05cm;padding:0.1cm;">
                        <img style="width: 300px; height: 180px; border: 2px dashed rgb(29, 175, 147); background-position:center;" src="<?php echo $lineafc2 ?>" width="300" height="180" style="border:dotted 1px black;"><br>
                        <button type="button" class="btn btn-raised btn-dark btn-min-width mr-1 mb-1" onclick="firma_editar2()">Editar</button>
                    </div>
                  </div>
                <?php } ?>
                </div>
              </div>
            </div>
          </div>  
        </form>  
        <hr>
        <div class="formulario">
          <div class="row">
            <div class="col-md-12" align="center">
              <button class="btn btn-raised btn-dark btn-min-width mr-1 mb-1 btn-lg btn_registro" onclick="guarda_registro()">
                    <i class="fa fa-check-square-o"></i> Finalizar servicio
                  </button>
            </div>
          </div>
        </div>  
      </div>
    </div>
  </div>
</div>
