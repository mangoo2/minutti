<link rel="stylesheet" href="<?php echo base_url(); ?>public/fullcalendar/css/fullcalendar.min.css">
<style type="text/css">
	:root {
		--color-green: #05a8e1;
		--color-secondary: #b9b9b9;
		--color-button: white;
		--color-black: white;
	}


	.switch-button .switch-button__checkbox {
		display: none;
	}

	.switch-button .switch-button__label {
		background-color: var(--color-secondary);
		width: 3rem;
		height: 1.5rem;
		border-radius: 1rem;
		display: inline-block;
		position: relative;
	}

	.switch-button .switch-button__label:before {
		transition: .2s;
		display: block;
		position: absolute;
		width: 1.5rem;
		height: 1.5rem;
		background-color: var(--color-button);
		content: '';
		border-radius: 50%;
		box-shadow: inset 0px 0px 0px 1px var(--color-black);
	}

	.switch-button .switch-button__checkbox:checked+.switch-button__label {
		background-color: #bd1620;
	}

	.switch-button .switch-button__checkbox:checked+.switch-button__label:before {
		transform: translateX(1.5rem);
	}
</style>
<section class="basic-elements">
	<div class="row">
		<div class="col-sm-6">
			<div class="content-header">Agenda de Servicios</div>
		</div>
		<div class="col-sm-6">
			<div class="content-header" align="right"><button type="button" class="btn btn-raised btn-danger btn-min-width mr-1 mb-1 cita_servicio_nueva" onclick="alta_cita(1)">Nueva cita</button></div>
		</div>
	</div>
	<div class="cita_servicio" style="display: none">
		<div class="card">
			<div class="card-header" style="padding: 1rem;">
                <h5><b class="titulo_cita">Nueva cita</b></h5>
            </div>
            <div class="card-body">
            	<div class="px-3">	
            		<form class="form" method="post"  role="form" id="form_registro_cita">
            			<input type="hidden" name="id" id="idreg" value="0">
					    <div class="row">
							<div class="col-md-6">
								<label>Fecha:</label>
								<input type="date" name="fecha_servicio" class="form-control" id="fecha_servicio">
							</div>
							<div class="col-md-6">
								<label>Hora:</label>
								<input type="time" class="form-control" name="hora_servicio" id="hora_servicio">
							</div>

							<div class="col-md-6">
								<label>Cliente:</label>
								<select class="form-control" name="id_cliente" id="cliente_x" style="width: 100%">
								</select>
							</div>
							<div class="col-md-6">
								<label>Teléfono:</label>
								<input type="number" class="form-control" id="telefono_cliente_x" readonly="">
							</div>
                        </div>
                        <div class="row">
							<div class="col-md-4">
								<label>Placas de la unidad:</label>
								<div class="text_servicio">
									<select class="form-control" name="unidad_servicio" id="placas_unidad_x" style="width: 100%">
								    </select>
							    </div>
							</div>
							<div class="col-md-4">
								<label>Modelo de la unidad:</label>
								<input type="text" class="form-control" id="modelo_unidad_x" readonly="">
							</div>
							<div class="col-md-4">
								<label>Año de la unidad:</label>
								<input type="number" class="form-control" id="ano_unidad_X" readonly="">
							</div>
						</div>
						<br>
                        <div class="row">
                        	<div class="col-md-12">
								<table class="table thead-inverse" id="tabla_servicios">
									<thead>
										<tr>
											<th>Servicio</th>
											<th>Descripción</th>
											<th>Duración servicio</th>
											<th>Costo Servicio</th>
											<th>Cantidad</th>
											<th>Total</th>
											<th></th>
										</tr>
									</thead>
									<tbody id="tabla_servicios_datos">
									</tbody>
								</table>
							</div>
							<div class="col-md-12">
								<input type="hidden" class="form-control" name="precio" id="preciox">
								<h5>Total: <span class="preciox_x">$0.00</span></h5>
							</div>
							<div class="col-md-3">
								<div class="form-group">
									<label>Método de pago:</label>
	                                <select class="form-control" id="mpago" name="metodo">
	                                    <option value="1">Efectivo</option>
	                                    <option value="2">Tarjeta de crédito</option>
	                                    <option value="3">Tarjeta de débito</option>
	                                </select>
	                            </div>    	
	                        </div>    
                        </div>	
					</form>
					<hr>
					<div class="row">
						<div class="col-md-12" align="center">
							<button class="btn btn-raised btn-dark btn-min-width mr-1 mb-1 btn-lg btn_registro" onclick="guarda_registro_cita()">
			                  <i class="fa fa-check-square-o"></i> Guardar
			                </button>
						</div>
					</div> 	
				</div>	
			</div>	
		</div>
	</section>    
	<div class="card-content agenda1">
		<div class="card-body">
			<div id="calendar"></div>
		</div>
	</div>

</section>


<!----------------------------------------------------------->
<div class="modal fade text-left" id="modal_cita" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" style="display: none;" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title" id="myModalLabel1">Información de servicio</h4>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">×</span>
				</button>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-md-6">
						<label>Fecha:</label>
						<input type="date" readonly class="form-control" id="fecha_servicio_v">
					</div>
					<div class="col-md-6">
						<label>Hora:</label>
						<input type="time" readonly class="form-control" id="hora_servicio_v">
					</div>

					<div class="col-md-6">
						<label>Cliente:</label>
						<input type="text" readonly class="form-control" id="nombre_cliente">
					</div>
					<div class="col-md-6">
						<label>Teléfono:</label>
						<input type="number" readonly class="form-control" id="telefono_cliente">
					</div>
					<div class="col-md-4">
						<label>Placas de la unidad:</label>
						<input type="text" readonly class="form-control" id="placas_unidad">
					</div>
					<div class="col-md-4">
						<label>Modelo de la unidad:</label>
						<input type="text" readonly class="form-control" id="modelo_unidad">
					</div>
					<div class="col-md-4">
						<label>Año de la unidad:</label>
						<input type="number" readonly class="form-control" id="año_unidad">
					</div>
					<div class="col-md-12">
						<br>
						<div class="tabla_servicios_v"></div>
					</div>
				</div>
				<div class="row form-group">
					<div class="col-md-3">
						<div class="estatus_btn">
						
						</div>
					</div>
					<div class="col-md-1 estatus_btn_pdf" style="display: none">
						<div class="estatus_btn_pdf_txt"></div>
					</div>	
					<div class="col-md-3">
						<label class="control-label check_servicio_cita_text">Inicializar servicio</label>
					</div>
                    <div class="check_servicio_cita_modal">
						<div class="switch-button col-md-1">
							<input type="checkbox" id="check_servicio_cita" class="switch-button__checkbox" onclick="btn_check_servicio()">
							<label for="check_servicio_cita" class="switch-button__label"></label>
						</div>
					</div>
				</div>
			<div class="modal-footer">
				<div class="btn_modal_cita">
					<button type="button" class="btn btn-danger" onclick="btn_eliminar()">Eliminar</button>
					<button type="button" class="btn btn-danger" onclick="editar_servicio()">Editar</button>
					<button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
				</div>
			</div>
		</div>
	</div>
</div>


<!----------------------------------------------------------->