<style type="text/css">
  body.layout-dark.layout-transparent div  .clockpicker-dial .clockpicker-tick {
    color: #666 !important;
    
  }body.layout-dark.layout-transparent .clockpicker-popover .popover-title{
    color: #999 !important;
  }
  body.layout-dark.layout-transparent .clockpicker-popover .popover-title span{
    color: #999 !important;
  }
  body.layout-dark.layout-transparent div .text-primary{
    color: #009DA0 !important;

  }
</style>  
<div class="row">
  <div class="col-sm-12">
    <div class="card">
      <div class="card-header">
        <h4 class="card-title"><?php echo $tittle ?> servicio</h4>
        <hr>
        <form class="form" method="post"  role="form" id="form_data">
          <input type="hidden" name="id" value="<?php echo $id;?>">
          <div class="row">
            <div class="col-md-12">
              <div class="form-group">
                <label>Código:</label>
                <input type="text" class="form-control" name="codigo" value="<?php echo $codigo ?>">
              </div>
            </div>
            <div class="col-md-12">
              <div class="form-group">
                <label>Nombre del servicio:</label>
                <input type="text" class="form-control" name="servico" value="<?php echo $servico ?>">
              </div>
            </div>
            <div class="col-md-12">
              <div class="form-group">
                <label>Descripción:</label>
                <input type="text" class="form-control" name="descripcion" value="<?php echo $descripcion ?>">
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group">
                <label>Precio del servicio:</label>
                <input type="number" class="form-control" name="costo_servicio" value="<?php echo $costo_servicio ?>">
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group">
                <label>Precio especial del servicio:</label>
                <input type="number" class="form-control" name="costo_especial" value="<?php echo $costo_especial ?>">
              </div>
            </div>
            <div class="col-md-4">
              <!-- -->
              <div class="form-group">
                <label for="userinput2">Duración del servicio:</label>
                  <div class="input-group clockpicker">
                    <input type="text" class="form-control" name="duracion" id="duracion" value="<?php echo $duracion ?>">
                  </div>
              </div>
              <!-- -->
            </div>
            <div class="col-md-4">
              <div class="form-group">
                <label>Costo del servicio:</label>
                <input type="number" class="form-control" name="servicio_cuesta" value="<?php echo $servicio_cuesta ?>">
              </div>
            </div>
          </div>
        </form>  
        <hr>
        <div class="row">
          <div class="col-md-12" align="center">
            <button class="btn btn-raised btn-dark btn-min-width mr-1 mb-1 btn-lg btn_registro" onclick="guarda_registro()">
                  <i class="fa fa-check-square-o"></i> Guardar
                </button>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
