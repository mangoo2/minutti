<div class="row">
		<div class="col-sm-12">
			<div class="content-header">Listado de servicios</div>
		</div>
	</div>
<div class="row">
	<div class="col-md-12">
		<div class="card">
			<div class="card-header" style="padding:10px;">
				<h4 class="card-title mb-0"></h4>
			</div>
			<div class="card-body">
				<div class="px-3">
					<div class="row form-group">
						<div class="col-md-2">
							<a type="button" class="btn btn-raised btn-danger btn-min-width mr-1 mb-1 button_save" href="<?php echo base_url() ?>Servicios/registrar"><i class="fa fa-save"></i> Agregar nuevo</a>
						</div>
					</div>
					<div class="row form-group">
						<div class="col-md-12">
							<table class="table thead-inverse" id="table-data" style="width: 100%">
								<thead>
									<tr>
										<th>#</th>
										<th>CÓDIGO</th>
										<th>SERVICIO</th>
										<th>DESCRIPCIÓN</th>
										<th>PRECIO DEL SERVICIO</th>
										<th>PRECIO ESPECIAL DEL SERVICIO</th>
										<th>DURACIÓN DEL SERVICIO</th>
										<th>ACCIONES</th>
									</tr>
								</thead>
								<tbody></tbody>
							</table>
						</div>
					</div>
				</div>
			</div>		
	    </div>
    </div>
</div>				




<div class="modal fade text-left" id="modal_descripcionServ" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" style="display: none;" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title" id="myModalLabel1">Descripción</h4>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">×</span>
				</button>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-md-12">
						<div class="table_data_product">
							<table class="table thead-inverse" id="tabla_descripcionServ" width="100%">
								<thead>
									<tr>
										<th>DESCRIPCIÓN</th>
									</tr>
								</thead>
								<tbody class="list_descripcionServ">
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn grey btn-outline-secondary" data-dismiss="modal">Cerrar</button>
			</div>
		</div>
	</div>
</div>