    </div>
        </div>

        <footer class="footer footer-static footer-light">
          <p class="clearfix text-muted text-sm-center px-2"><span>Copyright  &copy; <?php echo date('Y') ?> <a href="http://www.mangoo.mx" id="pixinventLink" target="_blank" class="text-bold-800 primary darken-2"><img style="width: 123px;" src="<?php echo base_url() ?>public/img/mangoo.png"> </a> Todos los derechos reservados. </span></p>
        </footer>

      </div>
    </div>

    <script src="<?php echo base_url(); ?>app-assets/vendors/js/core/popper.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>app-assets/vendors/js/core/bootstrap.min.js" type="text/javascript"></script>

    <script src="<?php echo base_url(); ?>app-assets/vendors/js/perfect-scrollbar.jquery.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>app-assets/vendors/js/prism.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>app-assets/vendors/js/jquery.matchHeight-min.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>app-assets/vendors/js/screenfull.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>app-assets/vendors/js/pace/pace.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>app-assets/vendors/js/sweetalert2.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>app-assets/vendors/js/pGenerator.jquery.js" type="text/javascript"></script>
    <!-- BEGIN VENDOR JS-->
    <!-- BEGIN PAGE VENDOR JS-->
    <script src="<?php echo base_url(); ?>app-assets/vendors/js/jquery.steps.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>app-assets/vendors/js/pickadate/picker.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>app-assets/vendors/js/pickadate/picker.date.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>app-assets/vendors/js/pickadate/legacy.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>app-assets/vendors/js/toastr.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>app-assets/vendors/js/chosen.jquery.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>app-assets/vendors/js/formvalidation/formValidation.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>app-assets/vendors/js/formvalidation/framework/bootstrap.min.js"></script>
    <!-- END PAGE VENDOR JS-->
    <!-- BEGIN APEX JS-->
    <script src="<?php echo base_url(); ?>app-assets/js/app-sidebar.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>app-assets/js/notification-sidebar.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>app-assets/js/customizer.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>app-assets/js/tooltip.js" type="text/javascript"></script>
    <!-- END APEX JS-->
    <!-- BEGIN PAGE LEVEL JS-->
    <!--<script src="<?php echo base_url(); ?>app-assets/js/dashboard1.js" type="text/javascript"></script>-->

    <script src="<?php echo base_url(); ?>/app-assets/js/components-modal.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>/public/js/plugins.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>/public/js/select2.full.min.js" type="text/javascript"></script>

    <script src="<?php echo base_url(); ?>app-assets/vendors/js/chartist.min.js" type="text/javascript"></script>
    <link href="<?php echo base_url(); ?>app-assets/vendors/confirm/jquery-confirm.min.css" type="text/css" rel="stylesheet">
    <script src="<?php echo base_url(); ?>app-assets/vendors/confirm/jquery-confirm.min.js"></script>

    <script type="text/javascript" src='<?php echo base_url(); ?>public/plugins/ckeditor/ckeditor.js'></script>
    <script type="text/javascript" src='<?php echo base_url(); ?>public/plugins/ckeditor/adapters/jquery.js'></script>
    <script type="text/javascript">
        var aux_icon=1;
        $(document).ready(function() {
            icono_ps();
        });
        function icono_ps(){
            if(aux_icon==0){
                aux_icon=1;
                $('.logo-img').html('<img style="width: 36px;" src="<?php echo base_url(); ?>public/img/fav.png" >');
                $('.logo-img2').html('');  
            }else{
                aux_icon=0;
                $('.logo-img').html(''); 
                $('.logo-img2').html('<img src="<?php echo base_url(); ?>public/img/logo4.png" style="width: 170px; border-radius: 10px;">'); 

            }
        }
        function deletenotificacion(id){
            $.confirm({
                boxWidth: '30%',
                useBootstrap: false,
                icon: 'fa fa-warning',
                title: 'Atención!',
                content: '¿Desea eliminar la notificacion?',
                type: 'red',
                typeAnimated: true,
                buttons:{
                    confirmar: function (){
                        
                        //===================================================
                            
                            //console.log(aInfoa);
                                $.ajax({
                                    type:'POST',
                                    url: "<?php echo base_url(); ?>index.php/General/eliminarnotificaciontemporal",
                                    data: {
                                        idpros:id
                                    },
                                    success:function(response){  
                                        toastr["success"]("Notificacion eliminada", "Hecho!");
                                        setTimeout(function(){ 
                                            location.reload();
                                        }, 2000);
                                        
                                    }
                                });
                            
                        
                    },
                    cancelar: function () 
                    {
                        
                    }
                }
            });
        }
        toastr.options = {
          "closeButton": true,
          "debug": false,
          "newestOnTop": false,
          "progressBar": true,
          "positionClass": "toast-top-right",
          "preventDuplicates": false,
          "onclick": null,
          "showDuration": "300",
          "hideDuration": "4000",
          "timeOut": "5000",
          "extendedTimeOut": "1000",
          "showEasing": "swing",
          "hideEasing": "linear",
          "showMethod": "fadeIn",
          "hideMethod": "fadeOut"
        }
    </script>
    <!-- END PAGE LEVEL JS-->
  </body> 
</html> 