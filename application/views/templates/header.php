<!DOCTYPE html>
<html lang="es" class="loading">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
        <meta name="author" content="Mangoo">
        <title>Minutti</title>
        <link rel="icon" href="<?php echo base_url(); ?>public/img/fav.png" sizes="32x32" />
        <link rel="icon" href="<?php echo base_url(); ?>public/img/fav.png" sizes="192x192" />
        <link rel="apple-touch-icon-precomposed" href="<?php echo base_url(); ?>public/img/ico/ops.jpeg" />
        <meta name="msapplication-TileImage" content="<?php echo base_url(); ?>public/img/ico/ops.jpeg" />

        <meta name="apple-mobile-web-app-capable" content="yes">
        <meta name="apple-touch-fullscreen" content="yes">
        <meta name="apple-mobile-web-app-status-bar-style" content="default">
        <link href="https://fonts.googleapis.com/css?family=Rubik:300,400,500,700,900|Montserrat:300,400,500,600,700,800,900" rel="stylesheet">
        <!-- BEGIN VENDOR CSS-->
        <!-- font icons-->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/fonts/feather/style.min.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/fonts/simple-line-icons/style.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/fonts/font-awesome/css/font-awesome.min.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/vendors/css/perfect-scrollbar.min.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/vendors/css/prism.min.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/vendors/css/sweetalert2.min.css">
        <!--<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/vendors/css/chartist.min.css">-->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/vendors/css/tables/datatable/datatables.min.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/vendors/css/toastr.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/vendors/css/chosen.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/vendors/css/balloon.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/vendors/css/pickadate/pickadate.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/vendors/js/formvalidation/formValidation.min3f0d.css?v2.2.0">
        <!-- END VENDOR CSS-->
        <!-- BEGIN APEX CSS-->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/css/app.css">  
        <script src="<?php echo base_url(); ?>app-assets/vendors/js/core/jquery-3.2.1.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>app-assets/vendors/js/datatable/datatables.min.js" type="text/javascript"></script>
        <!--<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>public/css/theme.css">-->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>public/css/select2.min.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/vendors/css/wizard.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/vendors/css/chartist.min.css">
        <!-- END APEX CSS-->
        <!-- BEGIN Page Level CSS-->
        <!-- END Page Level CSS-->
        <!-- BEGIN Custom CSS-->
        <!-- END Custom CSS-->
        <input type="hidden" name="ssessius" id="ssessius" value="<?php echo $_SESSION['perfilid'];?>" readonly>
        <input type="hidden" name="base_url" id="base_url" value="<?php echo base_url(); ?>" readonly>
    </head>
    <style type="text/css">
        .vd_red{
            font-weight: bold;
            color: red;
        }
        .valid{
            color: #009688!important; 
        } 

        .card {
            border: 3px solid #1c1c1a;
            border-radius: 15px;
        }
        
        .btn-dark {
            color: #FFFFFF;
            background-color: #343A40;
            border-color: #343A40;
            border-radius: 12px;
        }

        .page-item.active .page-link {
            z-index: 2;
            color: #FFFFFF;
            background-color: #df0109;
            border-color: #df0109;
        }
        .page-item.disabled .page-link {
            color: #1c1c1a;
            pointer-events: none;
            background-color: #FFFFFF;
            border-color: #1c1c1a;
        }
        .icon_font{
            font-size: 23px;
        }
        .btn[class*='btn-'], .fc button[class*='btn-'] {
            margin-bottom: 0rem;
        }
        table.dataTable td, table.dataTable th {
            -webkit-box-sizing: content-box;
            box-sizing: content-box;
            vertical-align: revert;
        }
        .table th, .table td {
            padding: 0.50rem;
            /* vertical-align: top; */
            border-top: 1px solid #E9ECEF;
            vertical-align: revert;
        }
        .form-control:focus {
            border-color: #bd1620 !important;
        }
        form label {
            color: #1c1c1a;
            font-size: 0.75rem;
            text-transform: uppercase;
            letter-spacing: 2px;
            font-weight: 500;
        }
        .nav-link:hover {
            color: #FFFFFF;
            background-color: black;
            border-color: #FF253E;
        }
        form .form-control {
            border: 1px solid #1c1c1a;
            color: #1c1c1a;
            border-radius: 10px;
        }
        .form-control {
            border-radius: 10px !important;
            display: block;
            width: 100%;
            padding: 0.5rem 0.75rem;
            font-size: 1rem;
            line-height: 1.25;
            color: #1c1c1a;
            background-color: #FFFFFF;
            background-image: none;
            -webkit-background-clip: padding-box;
            background-clip: padding-box;
            border: 1px solid rgb(28 28 26);
            border-radius: 0.25rem;
            -webkit-transition: border-color ease-in-out 0.15s, -webkit-box-shadow ease-in-out 0.15s;
            transition: border-color ease-in-out 0.15s, -webkit-box-shadow ease-in-out 0.15s;
            -o-transition: border-color ease-in-out 0.15s, box-shadow ease-in-out 0.15s;
            -moz-transition: border-color ease-in-out 0.15s, box-shadow ease-in-out 0.15s;
            transition: border-color ease-in-out 0.15s, box-shadow ease-in-out 0.15s;
            transition: border-color ease-in-out 0.15s, box-shadow ease-in-out 0.15s, -webkit-box-shadow ease-in-out 0.15s;
        }
        .nav-tabs {
            border-bottom: 1px solid #1c1c1a;
        }
        .nav-tabs .nav-link.active, .nav-tabs .nav-item.show .nav-link {
            color: #495057;
            background-color: #ffffff;
            border-color: #1c1c1a #1c1c1a #ffffff;
        }

        
        @media screen and (max-width: 480px) {
            .btn_exit{
                display: block !important;
            }
        }
    </style>
    <body data-col="2-columns" class=" 2-columns ">
        <!-- ////////////////////////////////////////////////////////////////////////////-->
            <div class="wrapper">