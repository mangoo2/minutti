<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$this->load->helper('url');
if (!isset($_SESSION['perfilid'])) {
    $perfil=0;
}else{
    $perfil=$_SESSION['perfilid']; 
    $sucursal=$_SESSION['sucursal'];  
}
if(!isset($_SESSION['usuario'])){
    ?>
    <script>
        document.location="<?php echo base_url(); ?>index.php/Login"
    </script>
    <?php
}

$menu=$this->ModeloSession->menus($perfil);
$resultsuc = $this->ModeloSession->sucursales(); 
?>
<style type="text/css">
  .red_sesion:hover{
    background: #bd1620 !important;
    border-radius: 19px;
    color: white !important;
  }
  @font-face {
      font-family: "Helvetica";
      src: url("<?php echo base_url(); ?>public/Helvetica.ttf");
  }
</style>
<!-- main menu-->
<!--.main-menu(class="#{menuColor} #{menuOpenType}", class=(menuShadow == true ? 'menu-shadow' : ''))-->
<div data-active-color="white" data-background-color="purple-bliss" data-image="<?php echo base_url(); ?>public/img/1.png" class="app-sidebar">
<!-- main menu header-->
<!-- Sidebar Header starts-->
<div class="sidebar-header" >
  <div class="logo clearfix" style="background: white">
      <a href="Inicio" class="logo-text float-left">
        <div class="logo-img">
          
        </div>
        <span class="text pull-left">
          <div class="logo-img2"></div>
        </span>
      </a>
      <a id="sidebarToggle" href="javascript:;" class="nav-toggle d-none d-sm-none d-md-none d-lg-block" onclick="icono_ps()"><i data-toggle="expanded" class="ft-toggle-right toggle-icon"></i></a><a id="sidebarClose" href="javascript:;" class="nav-close d-block d-md-block d-lg-none d-xl-none"><i class="ft-x"></i></a></div>
</div>
<!-- Sidebar Header Ends-->
<!-- / main menu header-->
<!-- main menu content-->
<div class="sidebar-content">
  <div class="nav-container">
    <ul id="main-menu-navigation" data-menu="menu-navigation" class="navigation navigation-main">
        <?php foreach ($menu->result() as $item){ ?>
          <li class="has-sub nav-item"><a href="#"><i class="<?php echo $item->Icon; ?>"></i><span data-i18n="" class="menu-title"><?php echo $item->Nombre; ?></span></a>
            <ul class="menu-content">
                <?php 
                    $perfil=$perfil;
                    $menu =  $item->MenuId;
                    $menusub = $this->ModeloSession->submenus($perfil,$menu);
                    foreach ($menusub->result() as $datos) { ?>
                      <li>
                        <a href="<?php echo base_url(); ?><?php echo $datos->Pagina; ?>" class="menu-item">
                          <i class="<?php echo $datos->Icon; ?>"></i>
                          <?php echo $datos->Nombre; ?>
                        </a>
                      </li>
                <?php } ?>
              
            </ul>
          </li>
        <?php } ?>
        <?php 
          $menusub2 = $this->ModeloSession->submenus($perfil,0);
          foreach ($menusub2->result() as $item){ ?>
          <li class="nav-item"><a href="<?php echo base_url(); ?><?php echo $item->Pagina; ?>"><i class="<?php echo $item->Icon; ?>"><span data-i18n="" class="menu-title"></i><?php echo $item->Nombre; ?></span></a>
          </li>
        <?php } ?>

    </ul>
  </div>
</div>
<!-- main menu content-->
<div class="sidebar-background"></div>
<!-- main menu footer-->
<!-- include includes/menu-footer-->
<!-- main menu footer-->

</div>
<!-- / main menu-->

<!-- Navbar (Header) Starts-->
      <nav class="navbar navbar-expand-lg navbar-light bg-faded" >
        <div class="container-fluid">
          <div class="navbar-header">
            <button type="button" data-toggle="collapse" class="navbar-toggle d-lg-none float-left"><span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button>
            <div class="btn_exit" style="position: fixed;top: 12px;left: 205px; display: none;">
              <a class="btn white btn-danger btn_sesion" href="<?php echo base_url(); ?>index.php/Login/exitlogin">Cerrar sesión</a>
            </div>  
          </div>
          <div class="navbar-container">
            <div id="navbarSupportedContent" class="collapse navbar-collapse">
              <ul class="navbar-nav">
                <li class="nav-item mr-2">
                  <a class="btn white btn-danger btn_sesion" href="<?php echo base_url(); ?>index.php/Login/exitlogin">Cerrar sesión</a>
                </li>
                <?php
                /*
                foreach ($resultsuc as $y){ 
                  if($sucursal==$y->sucursalId or $perfil==1){
                  ?>
                  <li class="dropdown nav-item">
                    <a id="dropdownBasic2" href="#" data-toggle="dropdown" class="nav-link position-relative dropdown-toggle apptogglefullscreen red_sesion" style="color: black">
                      <?php 
                      $nom_sucu='';
                      if($y->sucursalId==1){
                          $nom_sucu='N1'; 
                      }else if($y->sucursalId==2){
                          $nom_sucu='N2'; 
                      }else if($y->sucursalId==3){
                          $nom_sucu='N3'; 
                      }else if($y->sucursalId==4){
                          $nom_sucu='N4'; 
                      }
                      ?>
                      <span ><b><?php echo $nom_sucu ?></b></span>
                      <i class="fa fa-truck font-medium-3 darken-4">
                        <span class="notification badge badge-pill badge-danger">
                          <?php $resultstockproductos=$this->ModeloSession->suma_producto_sucursal_datos($y->sucursalId);
                              echo $resultstockproductos->num_rows();
                          ?>
                        </span>
                      </i>
                      <p class="d-none">Notifications</p>
                    </a>
                    <div class="notification-dropdown dropdown-menu dropdown-menu-right">
                      <div class="noti-list">
                        <br>
                        <h4 align="center" style="border-bottom: solid #bd1620;"><?php echo $y->sucursal ?></h4>
                        <?php 
                          foreach ($resultstockproductos->result() as $item) { ?>
                          <a class="dropdown-item noti-container py-3 border-bottom border-bottom-blue-grey border-bottom-lighten-4" >
                            <span class="noti-wrapper">
                              <span class="noti-title d-block text-bold-400 danger" style="font-size: 19px;"><i class="fa fa-truck danger float-left d-block font-large-1 mt-1 mr-2" ></i> <?php echo $item->producto ?> <i class="fa fa-times" style="float: right;" onclick="deletenotificacion(<?php  echo $item->productosId ?>)"></i></span>
                              <span class="noti-text">Existencias: (<?php  echo $item->stock ?>)</span><br>
                            </span>
                          </a>
                        <?php } ?>   
                      </div>
                    </div>
                  </li>
                <?php 
                  }
                } 
                */
                ?>
                <!--<li class="nav-item mr-2"><a id="navbar-fullscreen" href="javascript:;" class="nav-link apptogglefullscreen red_sesion"><i class="ft-maximize font-medium-3 blue-grey darken-4"></i>-->
                    <p class="d-none">fullscreen</p></a></li>
                <li class="dropdown nav-item">
                  <a id="dropdownBasic3" href="#" data-toggle="dropdown" class="nav-link position-relative dropdown-toggle red_sesion" style="color:#263238"><i class="ft-user font-medium-3 darken-4"> <?php echo $_SESSION['usuario'];?></i>
                    <p class="d-none">User Settings</p></a>
                      <div ngbdropdownmenu="" aria-labelledby="dropdownBasic3" class="dropdown-menu dropdown-menu-right">
                        
                        <a href="<?php echo base_url(); ?>index.php/Login/exitlogin" class="dropdown-item"><i class="ft-power mr-2"></i><span>Cerrar</span></a>
                  </div>
                </li>
                
              </ul>
            </div>
          </div>
        </div>
      </nav>
      <!-- Navbar (Header) Ends-->
      <div class="main-panel">
        <div class="main-content">
          <div class="content-wrapper">