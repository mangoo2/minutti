<?php $perfil=$_SESSION['perfilid'];  ?>
<section class="basic-elements">
    <div class="row">
        <div class="col-sm-12">
            <div class="content-header">Traspasos</div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <br>
                <div class="card-body">
                    <div class="px-3">
                        <form class="form" method="post" id="form-traspasos">
                            <input type="hidden" class="form-control" name="traspasoId" id="traspasoId" value="0">
                            <div class="row form-group">
                                <label class="col-md-2">Quien solicita:</label>
                                <div class="col-md-4">
                                    <input type="text" name="personal" id="personal" class="form-control" value="">
                                </div>
                                <label class="col-md-2">Fecha de solicitud:</label>
                                <div class="col-md-4">
                                    <input type="date" class="form-control" name="fecha_solicitud" id="fecha_solicitud" value="<?php echo $fechaactual ?>">
                                </div>
                            </div>
                            <div class="row form-group">
                                <label class="col-md-2">Origen:</label>
                                <div class="col-md-4">
                                    <select class="form-control sucursal_origen" name="sucursal_origen" id="sucursal_origen" onchange="validar_nave()">
                                        <option value="0" disabled="" selected="">Seleccione una opción</option>
                                        <?php foreach ($sucursalrow->result() as $item) { 
                                            if($item->sucursalId==$sucursal){ 
                                            ?>
                                                <option value="<?php echo $item->sucursalId;?>"><?php echo $item->sucursal;?></option>
                                        <?php 
                                            }else{ 
                                                if($perfil==1){ ?>
                                                <option value="<?php echo $item->sucursalId;?>"><?php echo $item->sucursal;?></option>
                                        <?php } }
                                        } ?> 
                                    </select>
                                </div>
                                <label class="col-md-2">Destino:</label>
                                <div class="col-md-4">
                                    <select class="form-control sucursal_destino" id="sucursal_destino" name="sucursal_destino">
                                        <option value="0" disabled="" selected="">Seleccione una opción</option>
                                        <?php foreach ($sucursalrow->result() as $item) { ?>
                                            <option value="<?php echo $item->sucursalId;?>"><?php echo $item->sucursal;?></option>
                                        <?php } ?> 
                                    </select>
                                </div>
                            </div>
                        </form>
                        <div class="row">
                            <label class="col-md-2">Cantidad:</label>
                            <div class="col-md-3">
                                <input type="number" class="form-control" id="s_cantidad">
                            </div>
                            <label class="col-md-2">Producto:</label>
                            <div class="col-md-3">
                                <select class="form-control" id="s_producto">
                                    
                                </select>
                                <br>
                                <div class="existencia_total"></div>
                            </div>
                            <div class="col-md-2">
                                <button type="button" class="btn btn-raised btn-danger button_save" style="float: right;" onclick="addinfo()"><i class="fa fa-plus"></i> Agregar</button>
                            </div>
                        </div>
                        <br>
                        <div class="row form-group">
                            <div class="col-md-12">
                                <table class="table thead-inverse" id="table-productos">
                                    <thead>
                                        <tr>
                                            <th>Cantidad</th>
                                            <th>Código  pieza</th>
                                            <th>Pieza</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody class="table-productos-tb">
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        

                        <div class="row form-group">
                            <div class="col-md-12">
                                <button type="button" class="btn btn-raised btn-dark btn-lg button_save" style="float: right;" onclick="save()"><i class="fa fa-save"></i> Procesar orden</button>
                                <a href="<?php echo base_url();?>Traspasos" type="button" class="btn btn-raised btn-danger btn-lg">Cancelar</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>