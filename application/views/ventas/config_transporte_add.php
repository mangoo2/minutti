<section class="basic-elements">
	<div class="row">
		<div class="col-sm-12">
			<div class="content-header"><?php echo $title_heades; ?></div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<div class="card">
				<div class="card-body">
					<div class="px-3">
						<form class="form" id="form-configuracion">
							<div class="row form-group">
								<div class="col-md-12">
									<label>Nombre de la configuración</label>
									<input type="hidden" id="configId" class="form-control" value="<?php echo $id; ?>">
									<input type="text" id="nombre" class="form-control" oninput="nombre_sub();">
								</div>

								<div class="row form-group col-md-12 mt-3">
									<div class="col-md-6 p-0">
										<label class="col-md-12">Productos relacionados</label>
										<div class="col-md-12">
											<select id="produto" class="form-control">
												<option value="0">Seleccionar una opción</option>
												<?php foreach ($productosrow->result() as $item) { ?>
													<option value="<?php echo $item->productoId; ?>" data-codigo="<?php echo $item->codigo; ?>"><?php echo $item->nombre; ?></option>
												<?php } ?>
											</select>
										</div>
									</div>

									<div class="col-md-6 p-0">
										<label class="col-md-12">Cantidad</label>
										<div class="col-md-10 p-0">
											<input type="number" id="piezas" class="form-control" value="">
										</div>
										<div class="col-md-2">
											<button type="button" class="btn btn-raised btn-danger btn-min-width mb-1" onclick="add_config(0)"><i class="fa fa-plus"></i></button>
										</div>
									</div>
								</div>
						</form>

						<div class="row form-group col-md-12 mt-3">
							<div class="col-md-12 mt-3 px-4">
								<table class="table thead-inverse" id="tabla_det" width="100%">
									<thead>
										<tr>
											<th width="15%">Cantidad</th>
											<th width="15%">Código</th>
											<th width="60%">Producto</th>
											<th width="10%">Eliminar</th>
										</tr>
									</thead>
									<tbody class="list_productos">
									</tbody>
								</table>
							</div>
						</div>
					</div>

					<!--					
					<div class="row">
						<div class="col-md-12 addsubconfiguracion">
							<div class="card-block">
								<ul class="nav nav-tabs addsubconfiguracion_tabs">
								</ul>
								<div class="tab-content px-1 pt-1 addsubconfiguracion_body">
								</div>
							</div>
						</div>
					</div>
					-->

					<div class="row form-group">
						<div class="col-md-12">
							<button type="button" class="btn btn-raised btn-dark btn-min-width ml-2 mb-1 btn-lg button_save" style="float: left;" onclick="save()"><i class="fa fa-save"></i> <?php echo $title_save; ?></button>
						</div>
					</div>

				</div>
			</div>
		</div>
	</div>
	</div>
</section>