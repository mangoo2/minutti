<style type="text/css">
  .dataTables_wrapper {
    padding: 0px;
  }

  #table-lis td {
    font-size: 14px;
  }
</style>


<section class="basic-elements">
  <div class="row">
    <div class="col-sm-12">
      <div class="content-header">Listado de Configuraciones</div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-12">
      <div class="card">
        <div class="card-header" style="padding:10px;">
          <h4 class="card-title mb-0"></h4>
        </div>
        <div class="card-body">
          <div class="px-3">
            <div class="row form-group">
              <div class="col-md-3">
                <a type="button" class="btn btn-raised btn-danger btn-min-width mr-1 mb-1 button_save" href="<?php echo base_url() ?>Configuracion_transporte/add"><i class="fa fa-save"></i> Agregar nueva configuración</a>
              </div>
            </div>
            <div class="row form-group">
              <div class="col-md-12">
                <table class="table thead-inverse" id="table-lis" style="width: 100%">
                  <thead>
                    <tr>
                      <th>NOMBRE DE LA CONFIGURACIÓN</th>
                      <th>PRODUCTOS RELACIONADOS</th>
                      <th>ACCIONES</th>
                    </tr>
                  </thead>
                  <tbody></tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>


<div class="modal fade text-left" id="modal_details" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" style="display: none;" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="myModalLabel1">Productos relacionados</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-md-12">
            <div class="table_data_product">
              <table class="table thead-inverse" id="tabla_dets" width="100%">
                <thead>
                  <tr>
                    <th>Cantidad</th>
                    <th>Código</th>
                    <th>Producto</th>
                  </tr>
                </thead>
                <tbody class="list_productos">
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn grey btn-outline-secondary" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>