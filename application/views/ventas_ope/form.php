<style type="text/css">
    .custom-control-input:checked ~ .custom-control-indicator {
        color: #FFFFFF;
        background-color: #f6a169;
    }
    .custom-control-input:active ~ .custom-control-indicator {
        background-color: #f6a169;
    }
    .select2-container .select2-selection--single{
        border-radius: 19px;
    }
    /* inputo de busqueda */
    .h-60 {
      height: 60px;
    }
    .ai-center {
          align-items: center;
    }
    .lg\:ph-32 {
          padding-left: 32px;
          padding-right: 32px;
    }
    .bgc-moon, .hover\:bgc-moon:focus, .hover\:bgc-moon:hover {
          background-color: #f5f5fa;
    }
    .cursor-pointer {
          cursor: pointer;
    }
    .bdw-0 {
          border-width: 0;
    }
    .fxg-1 {
          flex-grow: 1;
    }
    .bgc-transparent {
          background-color: transparent;
    }
    .letra_t{
        font-size: 20px;
    } 
    .color_amarillo{
        color: #f2b327;
    }
    .borde_div{
        border-radius: 34px;
    }
    /* input de busqueda fin */

    :root {
        --color-green: #f2752c;
        --color-red: #cf040c;
        --color-button: #fdffff;
        --color-black: #000;
    }


    .switch-button .switch-button__checkbox {
        display: none;
    }
    .switch-button .switch-button__label {
        background-color: var(--color-red);
        width: 3rem;
        height: 1.5rem;
        border-radius: 1rem;
        display: inline-block;
        position: relative;
    }
    .switch-button .switch-button__label:before {
        transition: .2s;
        display: block;
        position: absolute;
        width: 1.5rem;
        height: 1.5rem;
        background-color: var(--color-button);
        content: '';
        border-radius: 50%;
        box-shadow: inset 0px 0px 0px 1px var(--color-black);
    }
    .switch-button .switch-button__checkbox:checked + .switch-button__label {
        background-color: var(--color-red);
    }
    .switch-button .switch-button__checkbox:checked + .switch-button__label:before {
        transform: translateX(1.5rem);
    }

</style>

<div class="row">
    <div class="col-md-12">
      <h2>Ventas </h2>
    </div>
</div>
<!--Statistics cards Ends-->
<!--Line with Area Chart 1 Starts-->
<div class="row">
    <div class="col-sm-12">
        <div class="card">
            <div class="card-body">
                <div class="card-block">
                    <!--------//////////////-------->
                    <div class="row">
                        <div class="col-md-4">
                            <div class="btn-group">
                            <h5>Imprimir Ticket</h5> &nbsp; &nbsp; &nbsp; 
                                <div class="switch-button">
                                    <!-- Checkbox -->
                                    <input type="checkbox" name="switch-button" id="checkimprimir" class="switch-button__checkbox" checked>
                                    <!-- Botón -->
                                    <label for="checkimprimir" class="switch-button__label"> </label>
                                </div>
                            </div>    
                        </div>
                        <div class="col-md-8" align="right">
                            <div class="btn-group">
                                <h5>¿Esta venta se factura?</h5> &nbsp; &nbsp; &nbsp; 
                                <div class="switch-button">
                                    <input type="checkbox" name="switch-button" id="factura" class="switch-button__checkbox">
                                    <label for="factura" class="switch-button__label"> </label>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <!-- <div class="temporal_texto"></div> -->
                        </div>    
                    </div> 
                    <br>  
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <select class="form-control" id="vcliente" style="width: 100%">
                                    <?php foreach ($clientedefault->result() as $item){ 
                                        echo '<option value="'.$item->clienteId.'">'.$item->nombre.'</option>';
                                    } 
                                    ?>
                                </select>
                            </div>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-3">   
                            <h4>Ingrese cantidad</h4>           
                        </div>
                    </div>    
                    <div class="row">
                        <div class="col-md-3">   
                            <div class="form-group">
                                <input type="number" id="cantidad_pro" value="1" class="form-control" style="border-radius: 27px; color: black; font-size: 34px; padding: 0.3rem  0.55rem;" autocorrect="off">
                            </div>              
                        </div>
                        <div class="col-md-9">  
                            <div class="recordableHolder">
                            <div class="borde_div h-60 lg:ph-32 bdr-4 d-flex ai-center bgc-moon">
                              <i class="fa fa-search" style="font-size: 25px; color:#cf040c"></i>
                              &nbsp;&nbsp;&nbsp;
                                <!-- autocapitalize="off" -->
                                <input class="letra_t bgc-transparent fxg-1 bdw-0 recordable rinited" id="productos" placeholder="Buscar producto" type="text" oninput="campo_vacio()">
                                <a class="rec"><i class="fa fa-microphone" data-category="VoiceRecord" data-event="click" data-label="undefined"></i></a>
                              
                              &nbsp;&nbsp;&nbsp;&nbsp;
                              <img style="width: 30px;"  src="<?php echo base_url() ?>img/mango.png">
                            </div>
                            </div>  
                        </div>  
                    </div>   
                    <!--------//////////////-------->
                    <div class="row">
                      <div class="col-md-12">
                        <div style="position: relative;">
                            <div class="producto_buscar_t" style="position: absolute; top: 25px; z-index: 3;">  
                            </div>
                        </div>    
                      </div>
                    </div>
                    <br> 
                    <div class="row">
                        <div class="col-md-12">
                            <table class="table table-hover" id="productosv">
                                <thead>
                                    <tr>
                                        <th>Clave</th>
                                        <th>Cantidad</th>
                                        <th>Producto</th>
                                        <th>Precio. U</th>
                                        <th>Total</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody id="class_productos">
                                    
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-5"></div>
                        <div class="col-md-4" align="right">
                            <h4>Método de pago:</h4>
                        </div>   
                        <div class="col-md-3"> 
                            <div class="texto_tipo_venta">  
                                <div class="form-group">
                                    <select class="form-control" id="mpago" name="mpago" onchange="tipo_pago()">
                                        <option value="1">Efectivo</option>
                                        <option value="2">Tarjeta de crédito</option>
                                        <option value="3">Tarjeta de débito</option>
                                    </select>
                                </div>     
                            </div>             
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-5">
                            <p>Guardar venta presionar: ALT + V </p>
                        </div>
                        <div class="col-md-4" align="right">
                            <h4>Total:</h4>
                        </div>   
                        <div class="col-md-3">  
                            <div class="form-group">
                                <input type="texto" id="total_p_total_texto" class="form-control" value="0" readonly  style="background-color: #cf040c;color: black; font-size: 22px; padding: 0.3rem 0.55rem;">
                            </div>     
                            <div class="form-group">
                                <input type="hidden" id="total_p_total" class="form-control" value="0" readonly  style="background-color: #cf040c;color: black;">
                                <input type="hidden" id="total_p_total2" class="form-control" value="0" readonly>
                            </div>              
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-5">
                            <p>Guardar venta temporal presionar: ALT + T </p>
                        </div>
                        <div class="col-md-4" align="right">
                            <h4>Descuento:</h4>
                        </div>   
                        <div class="col-md-3">   
                            <div class="form-group">
                                <input type="number" id="descuento" value="0" class="form-control" oninput="calculartotal()">
                                <input type="hidden" id="cantdescuento" readonly>
                                <!--calculartotal() -->
                            </div>              
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <p>Quitar último registro de venta de la tabla presionar: ALT + B </p>
                        </div>
                        <div class="col-md-3" align="right">
                            <h4>Ingreso:</h4>
                        </div>   
                        <div class="col-md-3">   
                            <div class="form-group">
                                <input type="number" id="vingreso" value="0" class="form-control" oninput="ingreso()">
                            </div>              
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-7">
                            <a href="#" class="btn btn-raised btn-dark btn-min-width mr-1 mb-1 btn-lg" id="ingresaventa">Ingresar venta</a>           
                        </div>
                        <div class="col-md-2" align="right">
                            <h4>Cambio:</h4>
                        </div>   
                        <div class="col-md-3">   
                            <div class="form-group">
                                <input type="number" id="vcambio" class="form-control" readonly style="background-color: #cf040c;color: black;">
                            </div>              
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<style type="text/css">
    #iframereporte{
        background: white;
    }
    iframe{
        height: 500px;
        border:0;
        width: 100%;
    }
</style>
<div class="modal fade text-left" id="iframeri" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel1">Ticket</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <!--<div class="modal-body">-->
            <!--<div id="iframereporte"></div>-->
            <!--</div>-->
            <div class="modal-body iframereporte">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn grey btn-outline-secondary" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade text-left" id="modalturno" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="false" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel1">Nuevo Turno</h4>
                <!--<button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>-->
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Abrir Turno:</label>
                            <div class="col-sm-8 controls">
                                <input type="number"class="input-border-btm form-control" id="cantidadt" name="cantidadt" style="text-transform:uppercase;"  placeholder="$"/>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="col-sm-3 control-label"> Nombre del Turno</label>
                            <div class="col-sm-8 controls">
                                <input type="text" class="input-border-btm form-control" id="nombredelturno" name="nombredelturno" style="text-transform:uppercase;" > 
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-round btn_orange" data-dismiss="modal" id="btnabrirt">Abrir turno</button>
            </div>
        </div>
    </div>
</div>
<?php /* if ($sturno=='cerrado') { ?>
    <script type="text/javascript">
        $(document).ready(function(){
            $('#modalturno').modal();
        });
    </script>
 <?php } */ ?>
