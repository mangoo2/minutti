<style type="text/css">
    #iframereporte{
        background: white;
    }
    iframe{
        height: 500px;
        border:0;
        width: 100%;
    }
</style>
<input type="hidden" id="personalId" readonly value="<?php echo $personalId ?>">
<input type="hidden" id="nombre_persona" readonly value="<?php echo $nombre_persona ?>">
<input type="hidden" id="fecha_abono" readonly value="<?php echo date('d/m/Y g:i:a',strtotime($fecha)) ?>">
<input type="hidden" id="fecha_abono2" readonly value="<?php echo $fecha ?>">
<div class="row">
  <div class="col-md-3">
    <h2>Lista Ventas</h2>
  </div>
  <div class="col-md-2">
      <input type="hidden" id="idsucursal_aux" value="0">
  </div>
  <div class="col-md-2">
    <fieldset class="form-group">
      <select class="form-control" id="tipo_venta" onchange="reload_registro()">  
        <option selected disabled="">Tipo de venta</option>
        <option value="0">Todas</option>
        <option value="1">Efectivo</option>
        <option value="2">Tarjeta de crédito</option>
        <option value="3">Tarjeta de débito</option>
        <!-- <option value="4">Crédito</option> -->
      </select>
    </fieldset>
  </div>
  <div class="col-md-2">
    <fieldset class="form-group">
      <select class="form-control" id="tipo_factura" onchange="reload_registro()">  
        <option selected disabled="">Factura</option>
        <option value="0">Todas</option>
        <option value="1">Se factura</option>
        <option value="2">No</option>
      </select>
    </fieldset>
  </div>

  <div class="col-md-3" align="right">
    <a href="<?php echo base_url(); ?>Ventas" class="btn white btn-round btn_amarillo"><i class="fa fa-plus"></i></span> Nueva Venta</a>
  </div>     
</div>
<!--Statistics cards Ends-->

<!--Line with Area Chart 1 Starts-->
<div class="row">
  <div class="col-sm-12">
      <div class="card">
          <div class="card-body">
              <div class="card-block">
                  <br>
                  <!--------//////////////-------->
                  <table class="table table-striped" id="data_tables" style="width: 100%">
                    <thead>
                      <tr>
                        <th>Folio</th>
                        <th>Fecha</th>
                        <th>Vendedor</th>
                        <th>Monto</th>
                        <th>Tipo<span style="color: #ff000000">_</span>de<span style="color: #ff000000">_</span>pago</th>
                        <th>Descuento</th>
                        <th>Cliente</th>
                        <th>Estatus</th>
                        <th>Factura</th>
                        <th></th>
                      </tr>
                    </thead>
                    <tbody id="tbodyresultadosvent">
                    </tbody>
                  </table>
          <!--------//////////////-------->
              </div>
          </div>
      </div>
  </div>
</div>
<!------------------------------------------------>

<div class="modal fade text-left" id="iframeri" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel1">Ticket</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <!--<div class="modal-body">-->
            <!--<div id="iframereporte"></div>-->
            <!--</div>-->
            <div class="modal-body iframereporte">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-round btn_amarillo" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade text-left" id="cancelar" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel1">Mensaje de confirmación</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                ¿Desea cancelar el ticket No. <span id="NoTicket"></span> por un total de <span id="CantidadTicket"></span>?
                      <input type="hidden" id="hddIdVenta">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-success" onclick="cancelar_venta()">Aceptar</button>
                <button type="button" class="btn grey btn-outline-secondary" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade text-left" id="modal_credito_registro" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel1">Venta a Crédito</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
              <div class="row">
                <div class="col-md-7">
                  <h5 class="cliente_txt"></h5>
                  <h5 class="venta_txt"></h5>
                  <h6 class="fechahora_txt"></h6>
                </div>
                <div class="col-md-5" align="right">
                  <h5 style="color:red" class="total_venta">Total: $0.00</h5>
                  <h5 class="resta">Restante: $0.00</h5>
                  <h5><b> Total abono: $<span class="total_abono">0</span> </b></h5>
                </div>
              </div>
              <hr>
              <div class="row">
                <div class="col-md-12">
                  <h5><b>Realizar abono</b></h5>
                </div>
                <div class="col-md-12">
                  <table class="table table-striped" style="width: 100%;">
                    <thead id="tabla_tr_abono1">
                    </thead>
                    <tbody id="tabla_tr_abono2" style="height: 200px;">
                    </tbody>
                  </table>
                </div>
                <div class="col-md-12">
                  
                </div>  
              </div>    
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-round btn_amarillo" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>


<div class="modal fade text-left" id="aceptar_registro" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="myModalLabel1">Aceptación de cotización</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">

        <div class="row">
          <div class="col-md-6">
            <div class="form-group">
              <label>Fecha:</label>
              <input type="date" class="form-control" id="fecha_servicio" value="">
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
              <label>Hora:</label>
              <input type="time" class="form-control" id="hora_servicio">
            </div>
          </div>

          <div class="col-md-12">
            <div class="input-group">
              <label>Unidad:</label>
            </div>
          </div>

          <div class="col-md-12">
            <div class="input-group">
              <select class="form-control" id="unidadSelect">
                <option value="0" selected="" disabled="">Seleccionar una opción</option>
              </select>
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-success" onclick="aceptar_registro_cita()">Aceptar</button>
        <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>


<script type="text/javascript">
  $(document).ready(function () {

    $('[data-toggle="tooltip"]').tooltip();

  });

  function ticket(id){
    $("#iframeri").modal();
    $('.iframereporte').html('<iframe src="' + base_url + 'Ticket/ticket_venta/'+id+'" class="iframeprintc1" id="iframeprintc1"></iframe>');
    //$('#iframereporte').html('<iframe src="Visorpdf?filex=Ticket&iden=id&id='+id+'"></iframe>');
  }
  
  function buscarventa(){
    var search=$('#buscarvent').val();
    if (search.length>2) {
        $.ajax({
            type:'POST',
            url: '<?php echo base_url(); ?>ListaVentas/buscarvent',
            data: {
                buscar: $('#buscarvent').val()
            },
            async: false,
            statusCode:{
                404: function(data){ toastr.error('Error!', 'No Se encuentra el archivo');},
                500: function(){ toastr.error('Error', '500');}
            },
            success:function(data){
                $('#tbodyresultadosvent2').html(data);
            }
        });
      $("#data-tables").css("display", "none");
        $("#data-tables2").css("display", "");
    }else{
        $("#data-tables2").css("display", "none");
        $("#data-tables").css("display", "");
    }
  }
  
</script>