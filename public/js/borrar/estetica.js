$(document).ready(function() {
	$("input[name=Implantes]").change(function() {
		var val = $( "input[type=radio][name=Implantes]:checked" ).val();
		if (val==1) {
			$(".implantescheck").css("display","block");
		}else{
			$(".implantescheck").css("display","none");
		}
	});
	$('#Menton').change(function(event) {
		if ($('#Menton').is(':checked')) {
			$(".Mentoncheck").css("display","block");
		}else{
			$(".Mentoncheck").css("display","none");
		}
	});
	$('#Pomulo').change(function(event) {
		if ($('#Pomulo').is(':checked')) {
			$(".Pomulocheck").css("display","block");
		}else{
			$(".Pomulocheck").css("display","none");
		}
	});
	$('#Nariz').change(function(event) {
		if ($('#Nariz').is(':checked')) {
			$(".Narizcheck").css("display","block");
		}else{
			$(".Narizcheck").css("display","none");
		}
	});
	$('#EOtro').change(function(event) {
		if ($('#EOtro').is(':checked')) {
			$(".EOtrocheck").css("display","block");
		}else{
			$(".EOtrocheck").css("display","none");
		}
	});
	//======================================
	$('#Beforoplastia').change(function(event) {
		if ($('#Beforoplastia').is(':checked')) {
			$(".Beforoplastia_check").css("display","block");
		}else{
			$(".Beforoplastia_check").css("display","none");
		}
	});
	$('#Bichectomia').change(function(event) {
		if ($('#Bichectomia').is(':checked')) {
			$(".Bichectomia_check").css("display","block");
		}else{
			$(".Bichectomia_check").css("display","none");
		}
	});
	$('#Hiluronico').change(function(event) {
		if ($('#Hiluronico').is(':checked')) {
			$(".Hiluronico_check").css("display","block");
		}else{
			$(".Hiluronico_check").css("display","none");
		}
	});
	$('#Botox').change(function(event) {
		if ($('#Botox').is(':checked')) {
			$(".Botox_check").css("display","block");
		}else{
			$(".Botox_check").css("display","none");
		}
	});
	$('#Rinoplastia').change(function(event) {
		if ($('#Rinoplastia').is(':checked')) {
			$(".Rinoplastia_check").css("display","block");
		}else{
			$(".Rinoplastia_check").css("display","none");
		}
	});
	$('#Lifting').change(function(event) {
		if ($('#Lifting').is(':checked')) {
			$(".Lifting_check").css("display","block");
		}else{
			$(".Lifting_check").css("display","none");
		}
	});
	$('#PRP').change(function(event) {
		if ($('#PRP').is(':checked')) {
			$(".PRP_check").css("display","block");
		}else{
			$(".PRP_check").css("display","none");
		}
	});
	$('#Peeling').change(function(event) {
		if ($('#Peeling').is(':checked')) {
			$(".Peeling_check").css("display","block");
		}else{
			$(".Peeling_check").css("display","none");
		}
	});
	$("input[name=ImplantesC]").change(function() {
		var val = $( "input[type=radio][name=ImplantesC]:checked" ).val();
		if (val==1) {
			$(".ImplantesCcheck").css("display","block");
		}else{
			$(".ImplantesCcheck").css("display","none");
		}
	});
	$("input[name=CirugiaC]").change(function() {
		var val = $( "input[type=radio][name=CirugiaC]:checked" ).val();
		if (val==1) {
			$(".CirugiaCcheck").css("display","block");
		}else{
			$(".CirugiaCcheck").css("display","none");
		}
	});
	$('#Marcapasos').change(function(event) {
		if ($('#Marcapasos').is(':checked')) {
			$(".Marcapasos_check").css("display","block");
		}else{
			$(".Marcapasos_check").css("display","none");
		}
	});
	$('#PM').change(function(event) {
		if ($('#PM').is(':checked')) {
			$(".PM_check").css("display","block");
		}else{
			$(".PM_check").css("display","none");
		}
	});
	$('#Lesiones').change(function(event) {
		if ($('#Lesiones').is(':checked')) {
			$(".Lesiones_check").css("display","block");
		}else{
			$(".Lesiones_check").css("display","none");
		}
	});
	$('#saveform').click(function(event) {
		var params ={};
			params.esteticaId = $('#esteticaId').val();
			params.clienteId = $('#clienteId').val();
			params.implantesfaciales = $("input[type=radio][name=Implantes]:checked").val();
			params.implantecual = $('#ImplanteV').val();
			params.implantefecha = $('#implateDesde').val();
			params.fmenton = $('#Menton').is(':checked')==true?1:0;
			params.fmenton_fecha = $('#MentonF').val();
			params.fpolulo = $('#Pomulo').is(':checked')==true?1:0;
			params.fpomulo_fecha = $('#PomuloF').val();
			params.fnariz = $('#Nariz').is(':checked')==true?1:0;
			params.fnariz_fecha = $('#NarizF').val();
			params.fotro = $('#EOtro').is(':checked')==true?1:0;
			params.fotro_cual = $('#OtroV').val();
			params.fotro_fecha = $('#OtroF').val();
			params.fblefarosplatia = $('#Beforoplastia').is(':checked')==true?1:0;
			params.fblefarosplatia_fecha = $('#BeforoplastiaF').val();
			params.fbichectomia = $('#Bichectomia').is(':checked')==true?1:0;
			params.fbichectomia_fecha = $('#BichectomiaF').val();
			params.fhialuronico = $('#Hiluronico').is(':checked')==true?1:0;
			params.fhialuronico_fecha = $('#HiluronicoF').val();
			params.fbotox = $('#Botox').is(':checked')==true?1:0;
			params.fbotox_fecha = $('#BotoxF').val();
			params.frinoplastia = $('#Rinoplastia').is(':checked')==true?1:0;
			params.frinoplastia_fecha = $('#RinoplastiaF').val();
			params.fliftingf = $('#Lifting').is(':checked')==true?1:0;
			params.fliftingf_fecha = $('#LiftingF').val();
			params.fprp = $('#PRP').is(':checked')==true?1:0;
			params.fprp_fecha = $('#PRPF').val();
			params.fpeelinq = $('#Peeling').is(':checked')==true?1:0;
			params.fpeelinq_fecha = $('#PeelingF').val();
			params.cimplante = $("input[type=radio][name=ImplantesC]:checked").val();
			params.cimplante_cual = $('#ImplanteCV').val();
			params.cimplante_fecha = $('#ImplanteCF').val();
			params.cproc_esteticos = $("input[type=radio][name=CirugiaC]:checked").val();
			params.cproc_esteticos_cual = $('#CirugiaCV').val();
			params.cproc_esteticos_fecha = $('#CirugiaCF').val();
			params.cmarcapaso = $('#Marcapasos').is(':checked')==true?1:0;
			params.cmarcapaso_fecha = $('#MarcapasosF').val();
			params.cprotesism = $('#PM').is(':checked')==true?1:0;
			params.cprotesism_fecha = $('#PMF').val();
			params.clesion = $('#Lesiones').is(':checked')==true?1:0;
			params.clesion_cual = $('#LesionesV').val();
			params.clesion_fecha = $('#LesionesF').val();
			params.obs_estitovida = $('#OvsEV').val();
			params.diagnostico = $('#Diagnostico').val();
			params.tratamiento = $('#Tratamiento').val();
			params.con_facila = $('#Facial').is(':checked')==true?1:0;
			params.con_corporal = $('#Corporal').is(':checked')==true?1:0;
			$.ajax({
				type:'POST',
				url:'addestetica',
				data:params,
				async:false,
                statusCode: {
                    404: function(data) {
                        toastr.error('Error', '404');
                    },
                    500: function(data) {
                        toastr.error('Error', data);
                    }
                },
				success:function(data){
					toastr.success( 'agregada','Hecho!');
				}
			});
	});
});
function mostrarestetica(id){
	params = {};
	params.clienteId = id;
	$.ajax({
		type:'POST',
		url:'viewestetica',
		data:params,
		async:false,
        statusCode: {
            404: function(data) {
                toastr.error('Error', '404');
            },
            500: function(data) {
                toastr.error('Error', data);
            }
        },
		success:function(data){
			var array = $.parseJSON(data);
			if (array.status==1) {
				$('#esteticaId').val(array.esteticaId);
				$("input[type=radio][name=Implantes][value='"+array.implantesfaciales+"']").prop("checked",true).change();
				$('#ImplanteV').val(array.implantecual);
				$('#implateDesde').val(array.implantefecha);
				if(array.fmenton==1){ $('#Menton').prop('checked', true).change();  }
				$('#MentonF').val(array.fmenton_fecha);
				if(array.fpolulo==1){ $('#Pomulo').prop('checked', true).change();  }
				$('#PomuloF').val(array.fpomulo_fecha);
				if(array.fnariz==1){ $('#Nariz').prop('checked', true).change();  }
				$('#NarizF').val(array.fnariz_fecha);
				if(array.fotro==1){ $('#EOtro').prop('checked', true).change();  }
				$('#OtroV').val(array.fotro_cual);
				$('#OtroF').val(array.fotro_fecha);
				if(array.fblefarosplatia==1){ $('#Beforoplastia').prop('checked', true).change();  }
				$('#BeforoplastiaF').val(array.fblefarosplatia_fecha);
				if(array.fbichectomia==1){ $('#Bichectomia').prop('checked', true).change();  }
				$('#BichectomiaF').val(array.fbichectomia_fecha);
				if(array.fhialuronico==1){ $('#Hiluronico').prop('checked', true).change();  }
				$('#HiluronicoF').val(array.fhialuronico_fecha);
				if(array.fbotox==1){ $('#Botox').prop('checked', true).change();  }
				$('#BotoxF').val(array.fbotox_fecha);
				if(array.frinoplastia==1){ $('#Rinoplastia').prop('checked', true).change();  }
				$('#RinoplastiaF').val(array.frinoplastia_fecha);
				if(array.fliftingf==1){ $('#Lifting').prop('checked', true).change();  }
				$('#LiftingF').val(array.fliftingf_fecha);
				if(array.fprp==1){ $('#PRP').prop('checked', true).change();  }
				$('#PRPF').val(array.fprp_fecha);
				if(array.fpeelinq==1){ $('#Peeling').prop('checked', true).change();  }
				$('#PeelingF').val(array.fpeelinq_fecha);
				$("input[type=radio][name=ImplantesC][value='"+array.cimplante+"']").prop("checked",true).change();
				$('#ImplanteCV').val(array.cimplante_cual);
				$('#ImplanteCF').val(array.cimplante_fecha);
				$("input[type=radio][name=CirugiaC][value='"+array.cproc_esteticos+"']").prop("checked",true).change();
				$('#CirugiaCV').val(array.cproc_esteticos_cual);
				$('#CirugiaCF').val(array.cproc_esteticos_fecha);

				if(array.cmarcapaso==1){ $('#Marcapasos').prop('checked', true).change();  }
				$('#MarcapasosF').val(array.cmarcapaso_fecha);
				if(array.cprotesism==1){ $('#PM').prop('checked', true).change();  }
				$('#PMF').val(array.cprotesism_fecha);
				if(array.clesion==1){ $('#Lesiones').prop('checked', true).change();  }
				$('#LesionesV').val(array.clesion_cual);
				$('#LesionesF').val(array.clesion_fecha);
				$('#OvsEV').val(array.obs_estitovida);
				$('#Diagnostico').val(array.diagnostico);
				$('#Tratamiento').val(array.tratamiento);
				if(array.con_facila==1){ $('#Facial').prop('checked', true).change();  }
				if(array.con_corporal==1){ $('#Corporal').prop('checked', true).change();  }

			}
		}
	});
}
//$( "input[type=radio][name=Implantes][value='0']").prop("checked",true).change();
//$('#Menton').prop('checked', true).change();