$(document).ready(function() {
	$("#saveform").click(function(event) {
		var params = {};
		 	params.clinicosId = $('#clinicosId').val();
		 	params.clienteId = $('#clienteId').val();
		 	params.pdiarrea = $('#Diarrea').is(':checked')==true?1:0;
		 	params.pestrenimiento = $('#Estrenimiento').is(':checked')==true?1:0;
		 	params.pgastritis = $('#Gastritis').is(':checked')==true?1:0;
		 	params.pulcera = $('#Ulcera').is(':checked')==true?1:0;
		 	params.pnauseas = $('#Nausea').is(':checked')==true?1:0;
		 	params.ppirosis = $('#Pirosis').is(':checked')==true?1:0;
		 	params.pvomito = $('#Vomito').is(':checked')==true?1:0;
		 	params.pcolitis = $('#Colitis').is(':checked')==true?1:0;
		 	params.pdentadura = $('#Dentadura').is(':checked')==true?1:0;
		 	params.potros = $('#Otroic').is(':checked')==true?1:0;
		 	params.potrosd = $('#OtroD').val();
		 	params.observacion = $('#Observacionesic').val();
		 	params.enferm_gastrico = $("input[type=radio][name=Enfermedadgd]:checked").val();
		 	params.enferm_gastrico_cual = $('#EnfermedadV').val();
		 	params.henferm_gastrico = $("input[type=radio][name=EnfermedadP]:checked").val();
		 	params.henferm_gastrico_cual = $('#EnfermedadVp').val();
		 	params.medicamento = $("input[type=radio][name=medicamentoic]:checked").val();
		 	params.medicamento_cual = $('#MedicamentoVic').val();
		 	params.medicamento_dosis = $('#Dosisic').val();
		 	params.medicamento_inicio = $('#Desdeic').val();
		 	params.tlaxante = $('#Laxantes').is(':checked')==true?1:0;
		 	params.tdiureticos = $('#Diureticos').is(':checked')==true?1:0;
		 	params.tantiacidos = $('#Antiacidos').is(':checked')==true?1:0;
		 	params.tanalgesico = $('#Analgesicos').is(':checked')==true?1:0;
		 	params.cirugia = $('#Cirugian').is(':checked')==true?1:0;
		 	params.cirugia_cual = $('#MedicamentoVicc').val();
		 	$.ajax({
				type:'POST',
				url:'indicadoresclinicosadd',
				data:params,
				async:false,
                statusCode: {
                    404: function(data) {
                        toastr.error('Error', '404');
                    },
                    500: function(data) {
                        toastr.error('Error', data);
                    }
                },
				success:function(data){
					//toastr.success( 'Sucursal agregada','Hecho!');
				}
			});
	});
	$("#saveform").click(function(event) {
		var params = {};
			params.nutricionId = $('#nutricionId').val();
			params.clienteId = $('#clienteId').val();
			params.comidas_dia = $('#ComidasD option:selected').val();
			params.comida_dia_casa_entre = $('#ESC option:selected').val();
			params.comida_dia_casa_fin = $('#FSC option:selected').val();
			params.comida_dia_fuera_entre = $('#ESFC option:selected').val();
			params.comida_dia_fuera_fin = $('#FSFC option:selected').val();
			params.desayuno = $('#Desayuno').is(':checked')==true?1:0;
			params.desayuno_h = $('#HoraD').val();
			params.c_matutina = $('#ColacionM').is(':checked')==true?1:0;
			params.c_matutina_h = $('#HoraCM').val();
			params.comida = $('#Comida').is(':checked')==true?1:0;
			params.comida_h = $('#HoraC').val();
			params.c_vespertina = $('#ColacionV').is(':checked')==true?1:0;
			params.c_vespertina_h = $('#HoraCV').val();
			params.cena = $('#Cena').is(':checked')==true?1:0;
			params.cena_h = $('#HoraCN').val();
			params.q_prepara_alimentos = $('#QuienAlimentos').val();
			params.come_entre = $('input[type=radio][name=Com-com]:checked').val();
			params.come_entre_que = $('#Com-comV').val();
			params.modificacion_alimento = $('input[type=radio][name=ModA]:checked').val();
			params.modificacion_alimento_que = $('#ModARazon').val();
			params.modificacion_alimento_como = $('#ModAComo').val();
			params.compromiso = $('#CompromisoPA option:selected').val();
		 	$.ajax({
				type:'POST',
				url:'indicadoresdieteticosadd',
				data:params,
				async:false,
                statusCode: {
                    404: function(data) {
                        toastr.error('Error', '404');
                    },
                    500: function(data) {
                        toastr.error('Error', data);
                    }
                },
				success:function(data){
					//toastr.success( 'Sucursal agregada','Hecho!');
				}
			});
	});
	$("#saveform").click(function(event) {
		var params = {};
			params.clienteId = $('#clienteId').val();
			params.nutricionId = $('#nutricionIdex').val();
			params.cintura = $('#Cintura').val();
			params.abdomen = $('#Abdomen').val();
			params.cadera = $('#Cadera').val();
			params.gluteos = $('#Gluteos').val();
			params.diario_actividades = $('#Diarioev').val();
			params.observaciones_est = $('#ObservacionesEV').val();
			params.plan_dtx = $('input[type=radio][name=Plan]:checked').val();
			params.plan_dtx_otro = $('#OtroDTX').val();
			params.diagnostico = $('#Diagnostico').val();
			params.tratamiento = $('#Tratamiento').val();
		 	$.ajax({
				type:'POST',
				url:'nutricion_ext',
				data:params,
				async:false,
                statusCode: {
                    404: function(data) {
                        toastr.error('Error', '404');
                    },
                    500: function(data) {
                        toastr.error('Error', data);
                    }
                },
				success:function(data){
					toastr.success( 'agregada','Hecho!');
				}
			});
	});
});
function nutricionclinicos(id){
	params = {};
	params.clienteId = id;
	$.ajax({
		type:'POST',
		url:'mostrarnutricionclinicos',
		data:params,
		async:false,
        statusCode: {
            404: function(data) {
                toastr.error('Error', '404');
            },
            500: function(data) {
                toastr.error('Error', data);
            }
        },
		success:function(data){
			var array = $.parseJSON(data);
			if (array.status==1) {
				$('#clinicosId').val(array.clinicosId);
				if(array.pdiarrea==1){ $('#Diarrea').prop('checked', true);  }
				if(array.pestrenimiento==1){ $('#Estrenimiento').prop('checked', true);  }
				if(array.pgastritis==1){ $('#Gastritis').prop('checked', true);  }
				if(array.pulcera==1){ $('#Ulcera').prop('checked', true);  }
				if(array.pnauseas==1){ $('#Nausea').prop('checked', true);  }
				if(array.ppirosis==1){ $('#Pirosis').prop('checked', true);  }
				if(array.pvomito==1){ $('#Vomito').prop('checked', true);  }
				if(array.pcolitis==1){ $('#Colitis').prop('checked', true);  }
				if(array.pdentadura==1){ $('#Dentadura').prop('checked', true);  }
				if(array.potros==1){ $('#Otroic').prop('checked', true);  }			 	
			 	$('#OtroD').val(array.potrosd);
			 	$('#Observacionesic').val(array.observacion);
			 	$("input[type=radio][name=Enfermedadgd][value='"+array.enferm_gastrico+"']" ).prop("checked",true);
			 	$('#EnfermedadV').val(array.enferm_gastrico_cual);
			 	$("input[type=radio][name=EnfermedadP][value='"+array.henferm_gastrico+"']").prop("checked",true);
			 	$('#EnfermedadVp').val(array.henferm_gastrico_cual);
			 	$("input[type=radio][name=medicamentoic][value='"+array.medicamento+"']").prop("checked",true);
			 	$('#MedicamentoVic').val(array.medicamento_cual);
			 	$('#Dosisic').val(array.medicamento_dosis);
			 	$('#Desdeic').val(array.medicamento_inicio);
			 	if(array.tlaxante==1){ $('#Laxantes').prop('checked', true);  }	
			 	if(array.tdiureticos==1){ $('#Diureticos').prop('checked', true);  }	
			 	if(array.tantiacidos==1){ $('#Antiacidos').prop('checked', true);  }	
			 	if(array.tanalgesico==1){ $('#Analgesicos').prop('checked', true);  }	
			 	if(array.cirugia==1){ $('#Cirugian').prop('checked', true);  }	
			 	$('#MedicamentoVicc').val(array.cirugia_cual);
			}
		}
	});
}
function nutriciondieteticos(id){
	params = {};
	params.clienteId = id;
	$.ajax({
		type:'POST',
		url:'nutriciondieteticos',
		data:params,
		async:false,
        statusCode: {
            404: function(data) {
                toastr.error('Error', '404');
            },
            500: function(data) {
                toastr.error('Error', data);
            }
        },
		success:function(data){
			var array = $.parseJSON(data);
			if (array.status==1) {
				$('#nutricionId').val(array.nutricionId);
				$('#ComidasD option[value="'+array.comidas_dia+'"]').attr("selected", "selected");
				$('#ESC option[value="'+array.comida_dia_casa_entre+'"]').attr("selected", "selected");
				$('#FSC option[value="'+array.comida_dia_casa_fin+'"]').attr("selected", "selected");
				$('#ESFC option[value="'+array.comida_dia_fuera_entre+'"]').attr("selected", "selected");
				$('#FSFC option[value="'+array.comida_dia_fuera_fin+'"]').attr("selected", "selected");
				if(array.desayuno==1){ $('#Desayuno').prop('checked', true);  }
				$('#HoraD').val(array.desayuno_h);
				if(array.c_matutina==1){ $('#ColacionM').prop('checked', true);  }
				$('#HoraCM').val(array.c_matutina_h);
				if(array.comida==1){ $('#Comida').prop('checked', true);  }
				$('#HoraC').val(array.comida_h);
				if(array.c_vespertina==1){ $('#ColacionV').prop('checked', true);  }
				$('#HoraCV').val(array.c_vespertina_h);
				if(array.cena==1){ $('#Cena').prop('checked', true);  }
				$('#HoraCN').val(array.cena_h);
				$('#QuienAlimentos').val(array.q_prepara_alimentos);
				$("input[type=radio][name=Com-com][value='"+array.come_entre+"']").prop("checked",true);
				$('#Com-comV').val(array.come_entre_que);
				$("input[type=radio][name=ModA][value='"+array.modificacion_alimento+"']").prop("checked",true);
				$('#ModARazon').val(array.modificacion_alimento_que);
				$('#ModAComo').val(array.modificacion_alimento_como);
				$('#CompromisoPA option[value="'+array.compromiso+'"]').attr("selected", "selected");
			}
		}
	});
}
function nutricionext(id){
	params = {};
	params.clienteId = id;
	$.ajax({
		type:'POST',
		url:'nutricionext',
		data:params,
		async:false,
        statusCode: {
            404: function(data) {
                toastr.error('Error', '404');
            },
            500: function(data) {
                toastr.error('Error', data);
            }
        },
		success:function(data){
			var array = $.parseJSON(data);
			if (array.status==1) {
				$('#nutricionIdex').val(array.nutricionId);
				$('#Cintura').val(array.cintura);
				$('#Abdomen').val(array.abdomen);
				$('#Cadera').val(array.cadera);
				$('#Gluteos').val(array.gluteos);
				$('#Diarioev').val(array.diario_actividades);
				$('#ObservacionesEV').val(array.observaciones_est);
				$("input[type=radio][value='"+array.plan_dtx+"']").prop("checked",true);
				$('#OtroDTX').val(array.plan_dtx_otro);
				$('#Diagnostico').val(array.diagnostico);
				$('#Tratamiento').val(array.tratamiento);
			}
		}
	});
}