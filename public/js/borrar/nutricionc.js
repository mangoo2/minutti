$(document).ready(function() {
	$('#clearmedidas').click(function(event) {
		$('#hpeso').val('');
		$('#hcintura').val('');
		$('#habdomen').val('');
		$('#hcaderas').val('');
		$('#hgluteos').val('');
	});
	$('#savemedidas').click(function(event) {
		var $form = $("#formMedidas").valid();
		if ($form) {
			params = {};
			params.clienteId = $('#clienteId').val();
			params.peso = $('#hpeso').val();
			params.cintura = $('#hcintura').val();
			params.abdomen = $('#habdomen').val();
			params.caderas = $('#hcaderas').val();
			params.gluteos = $('#hgluteos').val();
			$.ajax({
				type:'POST',
				url:'addmedidas',
				data:params,
				async:false,
		        statusCode: {
		            404: function(data) {
		                toastr.error('Error', '404');
		            },
		            500: function(data) {
		                toastr.error('Error', data);
		            }
		        },
				success:function(data){
					toastr.success('Agregado','Hecho!');
					medidas($('#clienteId').val());
					
					$('#ultimopeso').val(params.peso);
					animateGauges();
				}
			});
		}
		
	});
	//graficapeso();
	$('#saveform').click(function(event) {
		
			params = {};
			params.clienteId = $('#clienteId').val();
			params.peso = $('#peso').val();
			params.estatura = $('#estatura').val();
			params.motivo = $('#motivo').val();
			params.observaciones = $('#observaciones').val();
			params.cita = $('#cita').val();
			$.ajax({
				type:'POST',
				url:'addmconsultanutri',
				data:params,
				async:false,
		        statusCode: {
		            404: function(data) {
		                toastr.error('Error', '404');
		            },
		            500: function(data) {
		                toastr.error('Error', data);
		            }
		        },
				success:function(data){
					toastr.success('Agregado','Hecho!');
				}
			});
		
		
	});
	$('#generaplan').click(function(event) {
		$('#modalplanalimenticio').modal();
		$.ajax({
			type:'POST',
			url:'generaciondeplan',
			async:false,
	        statusCode: {
	            404: function(data) {
	                toastr.error('Error', '404');
	            },
	            500: function(data) {
	                toastr.error('Error', data);
	            }
	        },
			success:function(data){
				$('.agregarplan').html(data);
				$('#table-plan-1').DataTable({
				    //"sPaginationType": "full_numbers",
				    "searching": false,
				    "bPaginate" : false,
				    "info":     false,
				    "paging":   false,
						dom: 'Bfrtip',        // Needs button container
				          select: 'single',
				          responsive: true,
				          altEditor: true,     // Enable altEditor
				          buttons: [{
				            text: 'Agregar',
				            name: 'add'        // do not change name
				          },
				          {
				            extend: 'selected', // Bind to Selected row
				            text: 'Editar',
				            name: 'edit'        // do not change name
				          },
				          {
				            extend: 'selected', // Bind to Selected row
				            text: 'Eliminar',
				            name: 'delete'      // do not change name
				         }]
				  });
				$('#table-plan-2').DataTable({
				    //"sPaginationType": "full_numbers",
				    "searching": false,
				    "bPaginate" : false,
				    "info":     false,
				    "paging":   false,
						dom: 'Bfrtip',        // Needs button container
				          select: 'single',
				          responsive: true,
				          altEditor: true,     // Enable altEditor
				           buttons: [{
				            text: 'Agregar',
				            name: 'add'        // do not change name
				          },
				          {
				            extend: 'selected', // Bind to Selected row
				            text: 'Editar',
				            name: 'edit'        // do not change name
				          },
				          {
				            extend: 'selected', // Bind to Selected row
				            text: 'Eliminar',
				            name: 'delete'      // do not change name
				         }]
				  });
				$('#table-plan-3').DataTable({
				    //"sPaginationType": "full_numbers",
				    "searching": false,
				    "bPaginate" : false,
				    "info":     false,
				    "paging":   false,
						dom: 'Bfrtip',        // Needs button container
				          select: 'single',
				          responsive: true,
				          altEditor: true,     // Enable altEditor
				           buttons: [{
				            text: 'Agregar',
				            name: 'add'        // do not change name
				          },
				          {
				            extend: 'selected', // Bind to Selected row
				            text: 'Editar',
				            name: 'edit'        // do not change name
				          },
				          {
				            extend: 'selected', // Bind to Selected row
				            text: 'Eliminar',
				            name: 'delete'      // do not change name
				         }]
				 });
				$('#table-plan-4').DataTable({
				    //"sPaginationType": "full_numbers",
				    "searching": false,
				    "bPaginate" : false,
				    "info":     false,
				    "paging":   false,
						dom: 'Bfrtip',        // Needs button container
				          select: 'single',
				          responsive: true,
				          altEditor: true,     // Enable altEditor
				           buttons: [{
				            text: 'Agregar',
				            name: 'add'        // do not change name
				          },
				          {
				            extend: 'selected', // Bind to Selected row
				            text: 'Editar',
				            name: 'edit'        // do not change name
				          },
				          {
				            extend: 'selected', // Bind to Selected row
				            text: 'Eliminar',
				            name: 'delete'      // do not change name
				         }]
				  });

			}
		});
	});
	$('#busquedaproducto').select2({
            width: '100%',
            minimumInputLength: 3,
            minimumResultsForSearch: 10,
            dropdownParent: $('#modalplanalimenticio'),
            placeholder: 'Buscar un producto',
            ajax: {
                url: 'searchproducto',
                dataType: "json",
                data: function (params) {
                    var query = {
                        search: params.term,
                        type: 'public'
                    }
                    return query;
                },
                processResults: function(data){
                    var itemspre = [];
                    data.forEach(function(element) {
                        itemspre.push({
                            id: element.IdProducto,
                            text: element.Producto+' '+element.tipo+' '+element.categoria+' '+element.marca+' '+element.Descripcion
                        });
                    });
                    return {
                        results: itemspre
                    };          
                },  
            }
        });
	$('#agregarprod').click(function(event) {
		var indi =$('#plan-observacion').val();
		var proid =$('#busquedaproducto option:selected').val();
		var prot =$('#busquedaproducto option:selected').text();
		var addpro='<tr class="pro_select_'+proid+'">';
               addpro+='<td><input type="" id="proid" style="width: 100px;background: transparent;border: 0px;" value="'+proid+'" readonly></td>';
               addpro+='<td><input type="" id="proidtext" style="width: 300px;background: transparent;border: 0px;" value="'+prot+'" readonly></td>';
               addpro+='<td>';
                    addpro+='<textarea id="proindicaciontext" style="width: 350px;background: transparent;border: 0px;" readonly>'+indi+'</textarea>';
               addpro+='</td>';
               addpro+='<td>';
                   addpro+='<button type="button" class="btn btn-raised btn-danger round btn-min-width mr-1 mb-1" onclick="deleteproselected('+proid+')"><i class="fa fa-times"></i></button>';
               addpro+='</td>';
              addpro+='</tr>';
		$('.add_pro_nut').append(addpro)
	});
	$('#agregar_planes').click(function(event) {
		var DATA1  = [];
		var TABLA   = $("#table-plan-1 tbody > tr");
		TABLA.each(function(){    
			item = {};     
		    item ["comida"] = $(this).text().replace('                            ','');
		    DATA1.push(item);
		});
	});
	
});
function nutricionext(id){
	params = {};
	params.clienteId = id;
	$.ajax({
		type:'POST',
		url:'nutricionext',
		data:params,
		async:false,
        statusCode: {
            404: function(data) {
                toastr.error('Error', '404');
            },
            500: function(data) {
                toastr.error('Error', data);
            }
        },
		success:function(data){
			var array = $.parseJSON(data);
			if (array.status==1) {
				/*
				$('#nutricionIdex').val(array.nutricionId);
				$('#Cintura').val(array.cintura);
				$('#Abdomen').val(array.abdomen);
				$('#Cadera').val(array.cadera);
				$('#Gluteos').val(array.gluteos);
				$('#Diarioev').val(array.diario_actividades);*/
				$('#ObservacionesEV').val(array.observaciones_est);
				$("input[type=radio][value='"+array.plan_dtx+"']").prop("checked",true);
				$('#OtroDTX').val(array.plan_dtx_otro);
				//$('#Diagnostico').val(array.diagnostico);
				//$('#Tratamiento').val(array.tratamiento);
			}
		}
	});
}
function medidas(id){
	params = {};
	params.clienteId = id;
	$.ajax({
		type:'POST',
		url:'medidas',
		data:params,
		async:false,
        statusCode: {
            404: function(data) {
                toastr.error('Error', '404');
            },
            500: function(data) {
                toastr.error('Error', data);
            }
        },
		success:function(data){
			$('#tablemedidas').html(data);
			
		}
	});
}
function deleteproselected(id){
	$('.pro_select_'+id).remove();
}