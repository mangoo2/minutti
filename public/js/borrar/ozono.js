$(document).ready(function() {
	$("input[name=Transfuciones]").change(function() {
		var val = $( "input[type=radio][name=Transfuciones]:checked" ).val();
		if (val==1) {
			$(".Transfucionescheck").css("display","block");
		}else{
			$(".Transfucionescheck").css("display","none");
		}
	});
	$("input[name=Trombocitepenia]").change(function() {
		var val = $( "input[type=radio][name=Trombocitepenia]:checked" ).val();
		if (val==1) {
			$(".Trombocitepeniacheck").css("display","block");
		}else{
			$(".Trombocitepeniacheck").css("display","none");
		}
	});
	$("input[name=Fabismo]").change(function() {
		var val = $( "input[type=radio][name=Fabismo]:checked" ).val();
		if (val==1) {
			$(".Fabismocheck").css("display","block");
		}else{
			$(".Fabismocheck").css("display","none");
		}
	});
	$('#saveform').click(function(event) {
		var params ={};
			params.ozonoterapiaId = $('#ozonoterapiaId').val();
			params.clienteId = $('#clienteId').val();
			params.trasfusiones = $("input[type=radio][name=Transfuciones]:checked").val();
			params.trasfusiones_des = $('#TansfucionesV').val();
			params.trombocitepenia = $("input[type=radio][name=Trombocitepenia]:checked").val();
			params.trombocitepenia_des = $('#TrombocitepeniaV').val();
			params.fabismo = $("input[type=radio][name=Fabismo]:checked").val();
			params.fabismo_des = $('#FabismoV').val();
			params.padecimiento = $('#Padecimiento').val();
			params.astenia = $('#Astenia').is(':checked')==true?1:0;
			params.adinamia = $('#Adinamia').is(':checked')==true?1:0;
			params.anorexia = $('#Anorexia').is(':checked')==true?1:0;
			params.fiebre = $('#Fiebre').is(':checked')==true?1:0;
			params.perdida_peso = $('#PerdidaP').is(':checked')==true?1:0;
			params.adigestivo = $('#AparatoDigestivo').val();
			params.acardiovascular = $('#AparatoCardiovascular').val();
			params.arespiratorio = $('#AparatoRespiratorio').val();
			params.aurinario = $('#AparatoUrinario').val();
			params.agenital = $('#AparatoGenital').val();
			params.ahematologico = $('#AparatoHematologico').val();
			params.sendocrino = $('#SistemaEndocrino').val();
			params.sosteomuscular = $('#SistemaOsteomuscular').val();
			params.snervioso = $('#SistemaNervioso').val();
			params.ssensorial = $('#SistemaSensorial').val();
			params.psicosomatico = $('#Psicosomatico').val();
			params.diagnostico = $('#ozDiagnostico').val();
			params.tratamiento = $('#ozTratamiento').val();
			$.ajax({
				type:'POST',
				url:'Ozonoterapiaadds',
				data:params,
				async:false,
                statusCode: {
                    404: function(data) {
                        toastr.error('Error', '404');
                    },
                    500: function(data) {
                        toastr.error('Error', data);
                    }
                },
				success:function(data){
					toastr.success( 'agregada','Hecho!');
				}
			});
	});

});
function ozonoview(id){
	params = {};
	params.clienteId = id;
	$.ajax({
		type:'POST',
		url:'viewozonoterapia',
		data:params,
		async:false,
        statusCode: {
            404: function(data) {
                toastr.error('Error', '404');
            },
            500: function(data) {
                toastr.error('Error', data);
            }
        },
		success:function(data){
			var array = $.parseJSON(data);
			if (array.status==1) {
				$('#ozonoterapiaId').val(array.ozonoterapiaId);
				$("input[type=radio][name=Transfuciones][value='"+array.trasfusiones+"']").prop("checked",true).change();
				$('#TansfucionesV').val(array.trasfusiones_des);
				$("input[type=radio][name=Trombocitepenia][value='"+array.trombocitepenia+"']").prop("checked",true).change();
				$('#TrombocitepeniaV').val(array.trombocitepenia_des);
				$("input[type=radio][name=Fabismo][value='"+array.fabismo+"']").prop("checked",true).change();
				$('#FabismoV').val(array.fabismo_des);
				$('#Padecimiento').val(array.padecimiento);
				if(array.astenia==1){ $('#Astenia').prop('checked', true).change();  }
				if(array.adinamia==1){ $('#Adinamia').prop('checked', true).change();  }
				if(array.anorexia==1){ $('#Anorexia').prop('checked', true).change();  }
				if(array.fiebre==1){ $('#Fiebre').prop('checked', true).change();  }
				if(array.perdida_peso==1){ $('#PerdidaP').prop('checked', true).change();  }
				$('#AparatoDigestivo').val(array.adigestivo);
				$('#AparatoCardiovascular').val(array.acardiovascular);
				$('#AparatoRespiratorio').val(array.arespiratorio);
				$('#AparatoUrinario').val(array.aurinario);
				$('#AparatoGenital').val(array.agenital);
				$('#AparatoHematologico').val(array.ahematologico);
				$('#SistemaEndocrino').val(array.sendocrino);
				$('#SistemaOsteomuscular').val(array.sosteomuscular);
				$('#SistemaNervioso').val(array.snervioso);
				$('#SistemaSensorial').val(array.ssensorial);
				$('#Psicosomatico').val(array.psicosomatico);
				$('#ozDiagnostico').val(array.diagnostico);
				$('#ozTratamiento').val(array.tratamiento);

			}
		}
	});
}