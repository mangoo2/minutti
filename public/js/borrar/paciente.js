$(document).ready(function() {
	$('#savepaciente').click(function() {
        var $form = $("#formPaciente").valid();
        if ($form) {
        	var estet = $('#Estetica').is(':checked')==true?1:0;
        	var nutr = $('#Nutricion').is(':checked')==true?1:0;
        	var ozono = $('#Ozonoterapia').is(':checked')==true?1:0;
        	var wp = $('#WP').is(':checked')==true?1:0;
            $.ajax({
                type: 'POST',
                url: 'addpaciente',
                data: {
                    id: $("#idpaciente").val(),
                    sucursal: $("#sucursal option:selected").val(),
                    nombre: $("#Nombre").val(),
                    fechanacimiento: $("#fecha_nacimiento").val(),
                    edad: $("#edad").val(),
                    sexo: $('input:radio[name=sexo]:checked').val(),
                    ecivil: $("#Estado_civil option:selected").val(),
                    ocupacion: $('#Ocupacion option:selected').val(),
                    calle: $('#Calle').val(),
                    num: $('#Num').val(),
                    int: $('#Int').val(),
                    col: $('#Colonia').val(),
                    muni: $('#Municipio').val(),
                    estado: $('#Estado option:selected').val(),
                    cp: $('#CP').val(),
                    lada: $('#Lada').val(),
                    tel: $('#Tel').val(),
                    cel: $('#Cel').val(),
                    whats: wp,
                    email: $('#Correo').val(),
                    facebook: $('#Facebook').val(),
                    motivo: $('#Motivo').val(),
                    nutri: nutr,
                    esteti:estet,
                    ozonot:ozono


                },
                async: false,
                statusCode: {
                    404: function(data) {
                        toastr.error('Error', '404');
                    },
                    500: function() {
                        toastr.error('Error', '500');
                    }
                },

                success: function(data) {
                	toastr.success( 'Paciente agregado','Hecho!');
                	
                	setTimeout(function(){ 
                		location.href =$('#base_url_pac').val()+'?cli='+data; 
                	}, 3000);
                    
                }
            });            
        } 
    });
});