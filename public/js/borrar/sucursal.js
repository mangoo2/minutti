$(document).ready(function() {
	$('#savesucursal').click(function() {
        var $form = $("#formSucursales").valid();
        if ($form) {
        	var nutr = $('#nutricion').is(':checked')==true?1:0;
        	var ozono = $('#ozonoterapia').is(':checked')==true?1:0;
        	var este = $('#estetica').is(':checked')==true?1:0;
            $.ajax({
                type: 'POST',
                url: 'addsucursal',
                data: {
                    id: $("#idform").val(),
                    nombre: $("#sucursal").val(),
                    dire: $("#direccion").val(),
                    referencia: $("#referencia").val(),
                    lat: $("#latitud").val(),
                    lon: $("#longitud").val(),
                    nutri: nutr,
                    ozonot: ozono,
                    estet: este
                },
                async: false,
                statusCode: {
                    404: function(data) {
                        toastr.error('Error', '404');
                    },
                    500: function() {
                        toastr.error('Error', '500');
                    }
                },

                success: function(data) {
                	toastr.success( 'Sucursal agregada','Hecho!');
                	
                	setTimeout(function(){ 
                		location.href =$('#base_url_suc').val(); 
                	}, 3000);
                    
                }
            });            
        } 
    });
});