$(document).ready(function () {
    $("#savecycle").click(function () {
        var cicli =$('#newcliclo').val();
        if(cicli!=''){
            $.ajax({
                type:'POST',
                url:'Catalogos/nuevociclos',
                data:{ciclo:$('#newcliclo').val()},
                async:false,
                success:function(data){
                    viewciclo();
                    $('#modalCiclo').modal('toggle');
                    $('#newcliclo').val('');
                    notification("topright","success","fa fa-check-circle vd_green","¡Hecho!","Ciclo Guardado Correctamente");
                }
            }); 
        }else{
            notification("topright","notify","fa fa-exclamation-triangle vd_yellow","Notificación","No se permite campos vacios");

        }
         
    });
        
});

/* muestra los ciclos escolares cargados*/
function viewciclo(){
       $.ajax({
            type:'POST',
            url:'Catalogos/mostrarciclos',
            //data:{usua:usu},
            async:false,
            success:function(data){
                $('#viewtablecliclo').html(data)
            }
        });
}
function notificacionasig(){
    var notifi = $('#notiasig').is(':checked');
    var notifiv = notifi==true?1:0;
    console.log(notifiv);
    $.ajax({
        type:'POST',
        url:'Catalogos/notificacionasig',
        data:{not:notifiv},
        async:false,
        success:function(data){
        }
    }); 
}
 
function viewconfig(){
    $.ajax({
        type:'POST',
        url:'Catalogos/viewconfig',
        async:false,
        success:function(data){
            console.log(data);
            if (data==1) {
                $('#notiasig').bootstrapToggle('on')
            }else{
                $('#notiasig').bootstrapToggle('off')
            }
        }
    });
}