var base_url=$('#base_url').val();
var idpro=$('#clienteId').val();
var cant=0;
var idban_aux=0;
var idcont=0;


var cantU=0;
var idUni_aux=0;
var idcontU=0;

$(document).ready(function() {
    btn_datos_fiscales();
    btn_datos_bancarios();
    get_tabla_bancarios(idpro);

    btn_datos_unidades();
    get_tabla_unidades(idpro);

    btn_datos_usuario();
});

function guarda_registro(){
    var form_register = $('#form_registro');
    var error_register = $('.alert-danger', form_register);
    var success_register = $('.alert-success', form_register);
    var $validator1=form_register.validate({
        errorElement: 'div', //default input error message container
        errorClass: 'vd_red', // default input error message class
        focusInvalid: false, // do not focus the last invalid input
        ignore: "",
        rules: {
            nombre:{
              required: true
            },
            celular:{
              minlength: 10,
              maxlength: 10,
              required: true
            },
            correo:{
              email: true
            },
            

            razon_social:{
              required: true
            },
            calle:{
              required: true
            },
            no_ext:{
              required: true
            },
            colonia:{
              required: true
            },
            localidad:{
              required: true
            },
            municipio:{
              required: true
            },
            estado:{
              required: true
            },
            codigo_postal:{
              required: true
            },
            rfc:{
              required: true
            },
            uso_cfdi:{
              required: true
            },
            metodo_pago:{
              required: true
            },
            forma_pago:{
              required: true
            },
            condicion_pago:{
              required: true
            },
            regimen_fiscal:{
              required: true
            },

            bancox:{
              required: true
            },
            titularx:{
              required: true
            },
            cuentax:{
              required: true
            },
            clavex:{
              required: true
            },
            tarjetax:{
              required: true
            },

            usuario:{
              required: true
            },
            contrasena:{
              required: true
            },
            perfiles:{
              required: true
            },
        },
        
        errorPlacement: function(error, element) {
            if (element.parent().hasClass("vd_checkbox") || element.parent().hasClass("vd_radio")){
                element.parent().append(error);
            } else if (element.parent().hasClass("vd_input-wrapper")){
                error.insertAfter(element.parent());
            }else {
                error.insertAfter(element);
            }
        }, 
        
        invalidHandler: function (event, validator) { //display error alert on form submit              
                success_register.fadeOut(500);
                error_register.fadeIn(500);
                scrollTo(form_register,-100);

        },

        highlight: function (element) { // hightlight error inputs
    
            $(element).addClass('vd_bd-red');
            $(element).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');

        },

        unhighlight: function (element) { // revert the change dony by hightlight
            $(element)
                .closest('.control-group').removeClass('error'); // set error class to the control group
        },

        success: function (label, element) {
            label
                .addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
                .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
            $(element).removeClass('vd_bd-red');
        }
    });
    //////////Registro///////////
    var $valid = $("#form_registro").valid();
    var validu = $("#form_usuario").valid();
    if($valid && validu) {
        $('.btn_registro').attr('disabled',true);
        var datos = form_register.serialize()+'&'+$("#form_usuario").serialize();
        console.log(datos);
        $.ajax({
            type:'POST',
            url: base_url+'Clientes/registro',
            data: datos,
            statusCode:{
              404: function(data){
                  swal("Error!", "Error 404", "error");
              },
              500: function(){
                  swal("Error!", "Algo salió mal, intente de nuevo o contacte al administrador del sistema", "error");
              }
            },
            success:function(data){
                console.log(data);
                var id = data;
                guardar_bancarios(id);

                guardar_unidades(id);

                swal("Éxito!", "Guardado correctamente", "success");
                setTimeout(function(){ 
                    window.location = base_url+'Clientes';
                }, 1500);
                
            }
        });
    }   
}


function btn_datos_fiscales(){
	if($('#datos_fiscales').is(':checked')){
        $('.txt_datos_fiscales').css('display','block');
        $('#razon_social').attr("name", "razon_social");
        $('#calle').attr("name", "calle");
        $('#no_ext').attr("name", "no_ext");
        $('#colonia').attr("name", "colonia");
        $('#localidad').attr("name", "localidad");
        $('#municipio').attr("name", "municipio");
        $('#estado').attr("name", "estado");
        $('#codigo_postal').attr("name", "codigo_postal");
        $('#rfc').attr("name", "rfc");
        $('#uso_cfdi').attr("name", "uso_cfdi");
        $('#metodo_pago').attr("name", "metodo_pago");
        $('#forma_pago').attr("name", "forma_pago");
        $('#condicion_pago').attr("name", "condicion_pago");
        $('#regimen_fiscal').attr("name", "regimen_fiscal");
        

	}else{
        $('.txt_datos_fiscales').css('display','none');
        $('#razon_social').removeAttr("name");
        $('#calle').removeAttr("name");
        $('#no_ext').removeAttr("name");
        $('#colonia').removeAttr("name");
        $('#localidad').removeAttr("name");
        $('#municipio').removeAttr("name");
        $('#estado').removeAttr("name");
        $('#codigo_postal').removeAttr("name");
        $('#rfc').removeAttr("name");
        $('#uso_cfdi').removeAttr("name");
        $('#metodo_pago').removeAttr("name");
        $('#forma_pago').removeAttr("name");
        $('#condicion_pago').removeAttr("name");
        $('#regimen_fiscal').removeAttr("name");
	}
}


function btn_datos_usuario() {
	if ($('#check_usuario').is(':checked')) {
		$('.txt_datos_usuario').css('display', 'block');

    $('#usuario').attr("name", "usuario");
    $('#contrasena').attr("name", "contrasena");
    $('#perfiles').attr("name", "perfiles");
	} else {
		$('.txt_datos_usuario').css('display', 'none');

    $('#usuario').removeAttr("name");
    $('#contrasena').removeAttr("name");
    $('#perfiles').removeAttr("name");
	}
}


function btn_datos_bancarios(){
    if($('#check_bancarios').is(':checked')){
        $('.txt_datos_bancarios').css('display','block');

        $('#bancox').attr("name", "bancox");
        $('#titularx').attr("name", "titularx");
        $('#cuentax').attr("name", "cuentax");
        $('#clavex').attr("name", "clavex");
        $('#tarjetax').attr("name", "tarjetax");
    }else{
        $('.txt_datos_bancarios').css('display','none');

        $('#bancox').removeAttr("name");
        $('#titularx').removeAttr("name");
        $('#cuentax').removeAttr("name");
        $('#clavex').removeAttr("name");
        $('#tarjetax').removeAttr("name");
    }
}

function datos_bacarios(){
    tabla_bancario(0,'','','','','');
}

function tabla_bancario(id,banco,titular,cuenta,clave,tarjeta){
    var btn='';
    if(cant==0){
        btn='<button type="button" class="btn btn-raised btn-dark round btn-min-width mr-1 mb-1 btn-sm btn_'+cant+'" onclick="datos_bacarios('+id+','+cant+')"><i class="fa fa-plus"></i></button>'
    }else{
        btn='<button type="button" class="btn btn-raised btn-danger round btn-min-width mr-1 mb-1 btn-sm" onclick="remover('+id+','+cant+')"><i class="fa fa-trash"></i></button>';
    }
    var html='<tr class="row_'+cant+'">\
                <td>\
                    <input hidden class="form-control" id="idx" value="'+id+'">\
                    <input class="form-control" id="bancox" value="'+banco+'">\
                </td>\
                <td>\
                    <input class="form-control" id="titularx" value="'+titular+'">\
                </td>\
                <td>\
                    <input class="form-control" id="cuentax" value="'+cuenta+'">\
                </td>\
                <td>\
                    <input class="form-control" id="clavex" value="'+clave+'">\
                </td>\
                <td>\
                    <input class="form-control" id="tarjetax" value="'+tarjeta+'">\
                </td>\
                <td>\
                    '+btn+'\
                </td>\
              </tr>';
    $('#tabla_bancarios').append(html);
    cant++;
    total_bancarios();
}

function remover(id,cant){
    if(id==0){
        $('.row_'+cant).remove();
        total_bancarios();
    }else{
        idban_aux=id;
        idcont=cant;
        delete_bancario(id);
    }
}

var cont=0;
function total_bancarios(){
    //var TABLA = $("#tabla_datos_bancarios tbody > tr");
    var oRows = document.getElementById('tabla_datos_bancarios').getElementsByTagName('tr');
    var iRowCount = oRows.length-1;
    if(iRowCount>=2){
        $('.btn_0').attr('disabled',true);
    }else{
        $('.btn_0').attr('disabled',false);
    }
}

var contx=0;
function guardar_bancarios(id){
    var DATA  = [];
    var TABLA   = $("#tabla_datos_bancarios tbody > tr");
    TABLA.each(function(){ 
        contx=1;
        item = {};
        item ["clienteId"] = parseFloat(id) ;
        item ["id"] = $(this).find("input[id*='idx']").val();
        item ["banco"] = $(this).find("input[id*='bancox']").val();
        item ["titular"] = $(this).find("input[id*='titularx']").val();
        item ["cuenta"] = $(this).find("input[id*='cuentax']").val();
        item ["clave"] = $(this).find("input[id*='clavex']").val();
        item ["tarjeta"] = $(this).find("input[id*='tarjetax']").val();
        DATA.push(item);
    });     
    if(contx==1){
        INFO  = new FormData();
        aInfo   = JSON.stringify(DATA);
        INFO.append('data', aInfo);
        $.ajax({
            data: INFO, 
            type: 'POST',
            url : base_url + 'Clientes/registro_bancarios',
            processData: false, 
            contentType: false,
            async: false,
            statusCode:{
                404: function(data){
                    toastr.error('Error!', 'No Se encuentra el archivo');
                },
                500: function(){
                    toastr.error('Error', '500');
                }
            },
            success: function(data){
            }
        });  
    }
}

function get_tabla_bancarios(id){
    $.ajax({
        type:'POST',
        url: base_url+"Clientes/get_tabla_bancarios_all",
        data: {id:id},
        success: function (response){
            var array = $.parseJSON(response);
            if(array.length>0) {
                array.forEach(function(element){
                    tabla_bancario(element.id,element.banco,element.titular,element.cuenta,element.clave,element.tarjeta)
                });
            }else{
                datos_bacarios();
            } 
        },
        error: function(response){
            toastr.error('Error', 'Algo salió mal, intente de nuevo o contacte al administrador del sistema');
        }
    });
}

function delete_bancario(id){
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: '¿Deseas eliminar esta cuenta bacanria?',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                $.ajax({
                    type:'POST',
                    url: base_url+"Clientes/deleteregistro_bancaria",
                    data: {id:id},
                    statusCode: {
                        404: function(data) {
                            toastr.error('404','Error');
                        },
                        500: function(data) {
                            toastr.error('Algo salió mal, intente de nuevo o contacte al administrador del sistema','Error');
                        }
                    },
                    success:function(response){  
                        toastr.success('Elimando Correctamente','Hecho!');
                        $('.row_'+idcont).remove();
                        total_bancarios();
                    }
                });
            },
            cancelar: function () 
            {
                
            }
        }
    });
}


function btn_datos_unidades() {
    if ($('#check_unidades').is(':checked')) {
        $('.txt_datos_unidades').css('display', 'block');
        if (cantU == 0) {}
    } else {
        $('.txt_datos_unidades').css('display', 'none');
    }
  }
  
  function datos_unidades() {
    tabla_unidades(0, '', '', '');
  }
  
  function tabla_unidades(id, placas, modelo, ano) {
    var btn = '';
    if (cantU == 0) {
      btn = '<button type="button" class="btn btn-raised btn-dark round btn-min-width mr-1 mb-1 btn-sm btn_' + cantU + '" onclick="datos_unidades(' + id + ',' + cantU + ')"><i class="fa fa-plus"></i></button>'
    } else {
      btn = '<button type="button" class="btn btn-raised btn-danger round btn-min-width mr-1 mb-1 btn-sm" onclick="quitar_unidad(' + id + ',' + cantU + ')"><i class="fa fa-trash"></i></button>';
    }
    var html = '<tr class="row_' + cantU + '">\
                <td>\
                    <input hidden class="form-control" id="idU" value="' + id + '">\
                    <input class="form-control" id="placasU" value="' + placas + '">\
                </td>\
                <td>\
                    <input class="form-control" id="modeloU" value="' + modelo + '">\
                </td>\
                <td>\
                    <input class="form-control" id="anoU" value="' + ano + '">\
                </td>\
                <td>\
                    ' + btn + '\
                </td>\
              </tr>';
    $('#tabla_unidades').append(html);
    cantU++;
  }
  
  function quitar_unidad(id, cantU) {
    if (id == 0) {
      $('.row_' + cantU).remove();
    } else {
      idUni_aux = id;
      idcontU = cantU;
      delete_unidades(id);
    }
  }
  
  
  var cont = 0;
  
  var conty=0;
  function guardar_unidades(id) {
    var DATA = [];
    var TABLA = $("#tabla_datos_unidades tbody > tr");
    TABLA.each(function () {
      conty = 1;
      item = {};
      item["clienteId"] = parseFloat(id);
      item["id"] = $(this).find("input[id*='idU']").val();
      item["placas"] = $(this).find("input[id*='placasU']").val();
      item["modelo"] = $(this).find("input[id*='modeloU']").val();
      item["ano"] = $(this).find("input[id*='anoU']").val();
      
      DATA.push(item);
    });
    if (conty == 1) {
      INFO = new FormData();
      aInfo = JSON.stringify(DATA);
      INFO.append('data', aInfo);
      $.ajax({
        data: INFO,
        type: 'POST',
        url: base_url + 'Clientes/registro_unidades',
        processData: false,
        contentType: false,
        async: false,
        statusCode: {
          404: function (data) {
            toastr.error('Error!', 'No Se encuentra el archivo');
          },
          500: function () {
            toastr.error('Error', '500');
          }
        },
        success: function (data) {
        }
      });
    }
  }
  
  
  function get_tabla_unidades(id) {
    $.ajax({
      type: 'POST',
      url: base_url + "Clientes/get_tabla_unidades_all",
      data: {
        id: id
      },
      success: function (response) {
        var array = $.parseJSON(response);
        if (array.length > 0) {
          array.forEach(function (element) {
            tabla_unidades(element.unidadId, element.placas, element.modelo, element.ano)
          });
        } else {
          datos_unidades();
        }
      },
      error: function (response) {
        toastr.error('Error', 'Algo salió mal, intente de nuevo o contacte al administrador del sistema');
      }
    });
  }
  
  function delete_unidades(id) {
    $.confirm({
      boxWidth: '30%',
      useBootstrap: false,
      icon: 'fa fa-warning',
      title: 'Atención!',
      content: '¿Deseas eliminar esta unidad?',
      type: 'red',
      typeAnimated: true,
      buttons: {
        confirmar: function () {
          $.ajax({
            type: 'POST',
            url: base_url + "Clientes/deleteregistro_unidad",
            data: {
              id: id
            },
            statusCode: {
              404: function (data) {
                toastr.error('404', 'Error');
              },
              500: function (data) {
                toastr.error('Algo salió mal, intente de nuevo o contacte al administrador del sistema', 'Error');
              }
            },
            success: function (response) {
              toastr.success('Elimando Correctamente', 'Hecho!');
              $('.row_' + idcontU).remove();
            }
          });
        },
        cancelar: function () {
  
        }
      }
    });
  }
