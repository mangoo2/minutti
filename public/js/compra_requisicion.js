var base_url = $('#base_url').val();
var idpro_aux=0;
var cont_aux=0;
var requsicionId = $('#requsicionId').val();
$(document).ready(function() {
	$('#idproveedor').select2({
        width: 'resolve',
        minimumInputLength: 3,
        minimumResultsForSearch: 10,
        placeholder: 'Buscar un proveedor',
        ajax: {
            url: base_url + 'Entrada/search_supplier',
            dataType: "json",
            data: function(params) {
                var query = {
                    search: params.term,
                    type: 'public'
                }
                return query;
            },
            processResults: function(data) {
                var itemscli = [];
                data.forEach(function(element) {
                    itemscli.push({
                        id: element.idproveedor,
                        text: element.nombre
                    });
                });
                return {
                    results: itemscli
                };
            },
        }
    }).on('select2:select', function(e) {
        var data = e.params.data;
        get_bancario_proveedor(data.id);
    });

    $('#personalId').select2({
        width: 'resolve',
        minimumInputLength: 3,
        minimumResultsForSearch: 10,
        placeholder: 'Buscar un empleado',
        ajax: {
            url: base_url + 'Entrada/search_employee',
            dataType: "json",
            data: function(params) {
                var query = {
                    search: params.term,
                    type: 'public'
                }
                return query;
            },
            processResults: function(data) {
                var itemscli = [];
                data.forEach(function(element) {
                    itemscli.push({
                        id: element.personalId,
                        text: element.nombre+' '+element.apellido_paterno+' '+element.apellido_materno,
                    });
                });
                return {
                    results: itemscli
                };
            },
        }
    });
    tabla_detalles_product(requsicionId);
});


function save_data(){
        var form_register = $('#form_data');
        var error_register = $('.alert-danger', form_register);
        var success_register = $('.alert-success', form_register);
        form_register.validate({
            errorElement: 'div', //default input error message container
            errorClass: 'vd_red', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            ignore: "",
            rules: {
                idproveedor: {
                    required: true
                },
                personalId: {
                    required: true
                },
                fecha_surtimiento: {
                    required: true
                },
            },
            errorPlacement: function(error, element) {
                if (element.parent().hasClass("vd_checkbox") || element.parent().hasClass("vd_radio")){
                    element.parent().append(error);
                } else if (element.parent().hasClass("vd_input-wrapper")){
                    error.insertAfter(element.parent());
                }else {
                    error.insertAfter(element);
                }
            }, 
            
            invalidHandler: function (event, validator) { //display error alert on form submit              
                    success_register.fadeOut(500);
                    error_register.fadeIn(500);
                    scrollTo(form_register,-100);

            },

            highlight: function (element) { // hightlight error inputs
        
                $(element).addClass('vd_bd-red');
                $(element).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');

            },

            unhighlight: function (element) { // revert the change dony by hightlight
                $(element)
                    .closest('.control-group').removeClass('error'); // set error class to the control group
            },

            success: function (label, element) {
                label
                    .addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
                    .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
                $(element).removeClass('vd_bd-red');
            }
        });
        var $valid = $("#form_data").valid();
        if($valid) {
            var datos = form_register.serialize()+'&comentarios='+$('#comentarios').val();
            $('.btn_registro').attr('disabled',true);
            $.ajax({
                type:'POST',
                url: base_url+'Entrada/add_requisicion_compra',
                data: datos,
                statusCode:{
                    404: function(data){
                        toastr.error('Error!', 'No Se encuentra el archivo');
                    },
                    500: function(){
                        toastr.error('Error', '500');
                    }
                },
                success:function(data){
                    guardar_producto(parseFloat(data));
                    toastr.success('Hecho!', 'Guardado Correctamente');
                    setTimeout(function(){ 
                        location.href=base_url+'Entrada/listadocompras';
                    }, 2000);
                }
            });
        }
}

var cant = 0;
function table_data_product(id,cantidad,idproducto,producto,costo,codigo,unidad,unidadId,requsiciondId,total){
    var can=parseFloat(cantidad);
    var cos=parseFloat(costo);
    var mult=parseFloat(can*cos);
    var html='<tr class="row_'+cant+'">\
                <td>\
                   <input hidden class="form-control" id="idx" value="'+id+'">\
                   <input type="hidden" class="form-control" id="requsiciondIdx" value="'+requsiciondId+'" disabled>\
                   <input type="number" class="form-control cantidadx_'+cant+'" id="cantidadx" value="'+cantidad+'" disabled>\
                </td>\
                <td>\
                    <div class="stock_nave_'+cant+'"></div>\
                </td>\
                <td>\
                    <div class="btn-group">\
                        $<input  type="number" class="form-control costox_'+cant+'" oninput="total_costo('+cant+')" id="costox" value="'+costo+'">\
                    </div>\
                </td>\
                <td>\
                    <div class="btn-group">\
                        $<input  type="number" class="form-control total_'+cant+'" id="totalx" value="'+mult+'" disabled>\
                    </div>\
                </td>\
                <td>\
                    <input hidden class="form-control" id="idproductox" value="'+idproducto+'" disabled>\
                    <input class="form-control" value="'+producto+'" disabled>\
                </td>\
                <td>\
                   <input class="form-control" id="codigox" value="'+codigo+'" disabled>\
                </td>\
                <td>\
                   <input hidden class="form-control" id="unidadIdx" value="'+unidadId+'" disabled>\
                   <input class="form-control" value="'+unidad+'" disabled>\
                </td>\
              </tr>';
    $('#tbody_data').append(html);
    get_nave_stock(cant,idproducto);
    total_costo(cant);
    cant++;
}

function get_nave_stock(cant,idpro){
    $.ajax({
        type:'POST',
        url: base_url+'Entrada/get_detalle_nave_stock',
        data: {cant:cant,idprod:idpro},
        statusCode:{
            404: function(data){
                toastr.error('Error!', 'No Se encuentra el archivo');
            },
            500: function(){
                toastr.error('Error', '500');
            }
        },
        success:function(data){
            $('.stock_nave_'+cant).html(data);
        }
    });
}


function tabla_detalles_product(id){
    $.ajax({
        type:'POST',
        url: base_url+"Entrada/get_tabla_requisicion_compra",
        data: {id:id},
        success: function (response){
            var array = $.parseJSON(response);
            if(array.length>0) {
                array.forEach(function(element){
                    table_data_product(0,element.cantidad,element.productoId,element.nombre,element.preciocompra,element.codigo,element.unidad,element.unidadId,element.requsiciondId,0);
                });
            }   
        },
        error: function(response){
            swal("Error!", "Algo salió mal, intente de nuevo o contacte al administrador del sistema", "error");
        }
    });
}

function guardar_producto(id){
    var cont=0;
    var DATA  = [];
    var TABLA   = $("#table_product tbody > tr");
    TABLA.each(function(){ 
        cont=1;
        item = {};
        item ["idorden"] = id;
        item ["id"] = $(this).find("input[id*='idx']").val();
        item ["cantidad"] = $(this).find("input[id*='cantidadx']").val();
        item ["costo"] = $(this).find("input[id*='costox']").val();
        item ["idproducto"] = $(this).find("input[id*='idproductox']").val();
        item ["unidad"] = $(this).find("input[id*='unidadIdx']").val();
        item ["requsiciondId"] = $(this).find("input[id*='requsiciondIdx']").val();
        item ["total"] = $(this).find("input[id*='totalx']").val();
        item ["idproveedor"] = $('#idproveedor').val();
        DATA.push(item);
    });     
    if(cont==1){
        INFO  = new FormData();
        aInfo   = JSON.stringify(DATA);
        INFO.append('data', aInfo);
        $.ajax({
            data: INFO, 
            type: 'POST',
            url : base_url + 'Entrada/add_detalle_product',
            processData: false, 
            contentType: false,
            async: false,
            statusCode:{
                404: function(data){
                    toastr.error('Error!', 'No Se encuentra el archivo');
                },
                500: function(){
                    toastr.error('Error', '500');
                }
            },
            success: function(data){
            }
        });  
    }
}

function total_costo(cant){
    var cantidad = $('.cantidadx_'+cant).val(); 
    var costo = $('.costox_'+cant).val(); 
    var can=parseFloat(cantidad);
    var cos=parseFloat(costo);
    
    var mult=parseFloat(can*cos);
    $('.total_'+cant).val(mult);

    var total=0;
    var TABLA   = $("#table_product tbody > tr");
    TABLA.each(function(){ 
        var num=$(this).find("input[id*='totalx']").val();
        total+=parseFloat(num);

    });    
    $('#total').val(total); 
}


function get_bancario_proveedor(id){
    $.ajax({
        type:'POST',
        url: base_url+"Entrada/get_bacarios_proveedor",
        data: {id:id},
        statusCode: {
            404: function(data) {
                toastr.error('404','Error');
            },
            500: function(data) {
                console.log(data);
                toastr.error('Algo salió mal, intente de nuevo o contacte al administrador del sistema','Error');
            }
        },
        success:function(data){  
            $('.datos_bancarios').html(data);
        }
    });
}
