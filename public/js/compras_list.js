var base_url=$('#base_url').val();
var tabla;
$(document).ready(function() {
    table();
});

function reload_registro(){
    tabla.destroy();
    table();
}

function table(){
    tabla=$("#data_tables").DataTable({
        "bProcessing": true,
        "serverSide": true,
        "searching": true,
        responsive: !0,
        "ajax": {
            "url": base_url+"Entrada/getlistado_compras",
            type: "post",
        },
        "columns": [
            {"data":"no_oc"},
            {"data": null,
                "render": function ( data, type, row, meta ){
                    var html='';
                    if(row.requsicionId==0){
                        html='';
                    }else{
                        html=row.requsicionId;
                    }
                return html;
                }
            },
            {"data":"proveedor"},
            {"data":"fecha_compra"},
            {"data": null,
                "render": function ( data, type, row, meta ){
                var html='<a class="btn btn-flat btn-danger" onclick="modal_ver('+row.id+')"><i class="fa fa-eye icon_font"></i></a>';
                return html;
                }
            },
            {"data":"realizo"},
            {"data": null,
                "render": function ( data, type, row, meta ){
                    var html='';
                    if(row.estatus==2){
                        html='<span class="bg-info text-highlight white" style="border-radius: 10px;">Aceptada</span>\
                        <a href="'+base_url+'Entrada/documento_pdf_compra/'+row.id+'" target="_blank" class="btn btn-flat btn-dark" style="color:white">PDF</a>\
                        <a href="'+base_url+'Entrada/completar/'+row.id+'" class="btn btn-flat btn-dark">Editar</a>';  
                    }else{
                        html='<a href="'+base_url+'Entrada/ingresa_orden/'+row.id+'" class="btn btn-flat btn-danger"><i class="fa fa-edit icon_font"></i></a>\
                        <a href="'+base_url+'Entrada/documento_pdf_compra/'+row.id+'" target="_blank" class="btn btn-flat btn-dark" style="color:white">PDF</a>\
                        <a href="'+base_url+'Entrada/completar/'+row.id+'" class="btn btn-flat btn-dark">Ingresar inventario</a>\
                        <a onclick="compradelete('+row.id+')" class="btn btn-flat btn-danger"><i class="fa fa-trash-o icon_font"></i></a>';
                    }    
                

                return html;
                }
            },
        ],
        "order": [[ 0, "desc" ]],
        "lengthMenu": [[10, 25, 50], [10, 25, 50]],
        // Cambiamos lo principal a Español
        "language": {
            "lengthMenu": "Mostrar _MENU_ entradas",
            "zeroRecords": "Lo sentimos - No se han encontrado elementos",
            "sInfo":  "Mostrando registros del _START_ al _END_ de un total de _TOTAL_  registros",
            "info": "Mostrando página _PAGE_ de _PAGES_",
            "infoEmpty": "No hay registros disponibles",
            "infoFiltered": "(Filtrado de _MAX_ registros totales)",
            "search": "Buscar : _INPUT_",
            "paginate": {
                "previous": "Página previa",
                "next": "Siguiente página"
              }
        }
    
    });
}

function ver_pdf(id){
    
    alert(id);
}

function compradelete(id){
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: '¿Está seguro de eliminar esta compra?',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                $.ajax({
                    type:'POST',
                    url: base_url+"Entrada/delete_compras",
                    data: {id:id},
                    statusCode: {
                        404: function(data) {
                            toastr.error('404','Error');
                        },
                        500: function(data) {
                            console.log(data);
                            toastr.error('Algo salió mal, intente de nuevo o contacte al administrador del sistema','Error');
                        }
                    },
                    success:function(response){  
                        toastr.success( 'Eliminado correctamente','Hecho!');
                        tabla.ajax.reload();
                    }
                });
            },
            cancelar: function () 
            {
                
            }
        }
    });
}

function modal_ver(id){
	$('#modal_ver').modal();
    tabla_producto(id);
}

function tabla_producto(id){
    $.ajax({
        type:'POST',
        url: base_url+'Entrada/get_detalle_product',
        data: {id:id},
        statusCode:{
            404: function(data){
                toastr.error('Error!', 'No Se encuentra el archivo');
            },
            500: function(){
                toastr.error('Error', '500');
            }
        },
        success:function(data){
            $('.table_data_product').html(data);
        }
    });
}