var base_url = $('#base_url').val();
var idpro_aux=0;
var cont_aux=0;
var idcompra = $('#idcompra').val();
$(document).ready(function() {
	$('#idproveedor').select2({
        width: 'resolve',
        minimumInputLength: 3,
        minimumResultsForSearch: 10,
        placeholder: 'Buscar un proveedor',
        ajax: {
            url: base_url + 'Entrada/search_supplier',
            dataType: "json",
            data: function(params) {
                var query = {
                    search: params.term,
                    type: 'public'
                }
                return query;
            },
            processResults: function(data) {
                var itemscli = [];
                data.forEach(function(element) {
                    itemscli.push({
                        id: element.idproveedor,
                        text: element.nombre
                    });
                });
                return {
                    results: itemscli
                };
            },
        }
    }).on('select2:select', function(e) {
        var data = e.params.data;
        get_bancario_proveedor(data.id);
    });

    $('#personalId').select2({
        width: 'resolve',
        minimumInputLength: 3,
        minimumResultsForSearch: 10,
        placeholder: 'Buscar un empleado',
        ajax: {
            url: base_url + 'Entrada/search_employee',
            dataType: "json",
            data: function(params) {
                var query = {
                    search: params.term,
                    type: 'public'
                }
                return query;
            },
            processResults: function(data) {
                var itemscli = [];
                data.forEach(function(element) {
                    itemscli.push({
                        id: element.personalId,
                        text: element.nombre
                    });
                });
                return {
                    results: itemscli
                };
            },
        }
    });
    if(idcompra!=0){
        tabla_detalles_product(idcompra);
        $('.btn_agregar_pro').css('display','none');
        var idproveedorx = $('#idproveedor').val();
        if(idproveedorx!=null){
            get_bancario_proveedor(idproveedorx);
        }
    }

});

function save_data(){
    var form_register = $('#form_data');
    var error_register = $('.alert-danger', form_register);
    var success_register = $('.alert-success', form_register);
    form_register.validate({
        errorElement: 'div', //default input error message container
        errorClass: 'vd_red', // default input error message class
        focusInvalid: false, // do not focus the last invalid input
        ignore: "",
        rules: {
            idproveedor: {
                required: true
            },
            personalId: {
                required: true
            },
            fecha_surtimiento: {
                required: true
            },
        },
        errorPlacement: function(error, element) {
            if (element.parent().hasClass("vd_checkbox") || element.parent().hasClass("vd_radio")){
                element.parent().append(error);
            } else if (element.parent().hasClass("vd_input-wrapper")){
                error.insertAfter(element.parent());
            }else {
                error.insertAfter(element);
            }
        }, 
        
        invalidHandler: function (event, validator) { //display error alert on form submit              
                success_register.fadeOut(500);
                error_register.fadeIn(500);
                scrollTo(form_register,-100);

        },

        highlight: function (element) { // hightlight error inputs
    
            $(element).addClass('vd_bd-red');
            $(element).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');

        },

        unhighlight: function (element) { // revert the change dony by hightlight
            $(element)
                .closest('.control-group').removeClass('error'); // set error class to the control group
        },

        success: function (label, element) {
            label
                .addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
                .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
            $(element).removeClass('vd_bd-red');
        }
    });
    var $valid = $("#form_data").valid();
    if($valid) {
        var cantv=0;
        var TABLA   = $("#table_product tbody > tr");
        TABLA.each(function(){ 
            var cantidadv = $(this).find("input[id*='cantidadx']").val();
            var idproductov = $(this).find("select[id*='idproductox']").val();
            if(cantidadv==0 || cantidadv=='' || idproductov==0 || idproductov=='' || idproductov==null){
                cantv=2;
            }else{
                cantv=1;
            }
        }); 
        if(cantv==1){
            var datos = form_register.serialize()+'&comentarios='+$('#comentarios').val();
            $('.btn_registro').attr('disabled',true);
            $.ajax({
                type:'POST',
                url: base_url+'Entrada/add_orden_compra',
                data: datos,
                statusCode:{
                    404: function(data){
                        toastr.error('Error!', 'No Se encuentra el archivo');
                    },
                    500: function(){
                        toastr.error('Error', '500');
                    }
                },
                success:function(data){
                    guardar_producto(parseFloat(data));
                    toastr.success('Hecho!', 'Guardado Correctamente');
                    setTimeout(function(){ 
                        location.href=base_url+'Entrada/listadocompras';
                    }, 2000);
                }
            });
        }else if(cantv==2){
            toastr.error('Los campos de productos deben estar llenos','Atención!');
        }else{    
            toastr.error('Debes agregar almenos un producto','Atención!');
        }
    }

}

function btn_agregar_pro(){
    $('.btn_agregar_pro').css('display','none');
    data_product();
}


function data_product(){
    table_data_product(0,'','',0,'','','',0,"");
}
var cant = 0;
function table_data_product(id,cantidad,idproducto,producto,costo,codigo,unidad,unidadId,stock_suc){
    var btn='';
    if(cant==0){
        btn='<button type="button" class="btn btn-raised btn-dark round btn-min-width mr-1 mb-1 btn-sm btn_'+cant+'" onclick="data_product('+id+','+cant+')"><i class="fa fa-plus"></i></button>'
    }else{
        btn='<button type="button" class="btn btn-raised btn-danger round btn-min-width mr-1 mb-1 btn-sm" onclick="remover('+id+','+cant+')"><i class="fa fa-trash"></i></button>';
    }
    var can=parseFloat(cantidad);
    var cos=parseFloat(costo);
    var mult=parseFloat(can*cos);
    var html='<tr class="row_'+cant+'">\
                <td>\
                   <select class="form-control idproductox" id="idproductox_'+cant+'">';
                    if(idproducto!=0){ 
                 html+='<option value="'+idproducto+'">'+producto+'</option>';
                    }
                 html+='</select>\
                </td>\
                <td>\
                   <input hidden class="form-control" id="idx" value="'+id+'">\
                   <input type="number" class="form-control cantidadx_'+cant+'" oninput="total_costo('+cant+')" id="cantidadx" value="'+cantidad+'">\
                </td>\
                <td>\
                    <div class="stock_nave_'+cant+'">\
                        <span class="row_st_'+cant+'">\
                            '+stock_suc+'\
                        </span>\
                    </div>\
                </td>\
                <td>\
                    <div class="btn-group">\
                        $<input  type="number" class="form-control costox_'+cant+'" oninput="total_costo('+cant+')" id="costox" value="'+costo+'">\
                    </div>\
                </td>\
                <td>\
                    <div class="btn-group">\
                        $<input  type="number" class="form-control total_'+cant+'" id="totalx" value="'+mult+'" disabled>\
                    </div>\
                </td>\
                <td>\
                   <input class="form-control codigox_'+cant+'" id="codigox" value="'+codigo+'" disabled>\
                </td>\
                <td>\
                   <input class="form-control unidadx_'+cant+'" id="unidadx" value="'+unidad+'" disabled>\
                   <input hidden class="unidadIdx_'+cant+'" id="unidadIdx" value="'+unidadId+'">\
                </td>\
                <td>\
                   '+btn+'\
                </td>\
              </tr>';
    $('#tbody_data').append(html);
    select_producto(cant);
    if(id!=0){
        //console.log("id desde tabe data: "+idproducto);
        //get_nave_stock(cant,idproducto);
    }
    cant++;
}

function select_producto(cant){
    $('#idproductox_'+cant).select2({
        width: '100%',
        minimumInputLength: 3,
        minimumResultsForSearch: 10,
        placeholder: 'Buscar un producto',
        ajax: {
            url: base_url+'Entrada/search_product',
            dataType: "json",
            data: function(params) {
                var query = {
                    search: params.term,
                    type: 'public'
                }
                return query;
            },
            processResults: function(data) {
                var itemscli = [];
                data.forEach(function(element) {
                    itemscli.push({
                        id: element.productoId,
                        text: element.nombre,
                        codigo: element.codigo,
                        unidad: element.unidad,
                        unidadId: element.unidadId,
                        preciocompra: element.preciocompra,
                    });
                });
                return {
                    results: itemscli
                };
            },
        }
    }).on('select2:select', function(e) {
        var data = e.params.data;
        $('.codigox_'+cant).val(data.codigo);
        $('.unidadx_'+cant).val(data.unidad);
        $('.unidadIdx_'+cant).val(data.unidadId);
        $('.costox_'+cant).val(data.preciocompra);
        get_nave_stock(cant,data.id);
    });
}

function get_nave_stock(cant,idpro){
    $.ajax({
        type:'POST',
        url: base_url+'Entrada/get_detalle_nave_stock',
        data: {cant:cant,idprod:idpro},
        //async:false,
        statusCode:{
            404: function(data){
                toastr.error('Error!', 'No Se encuentra el archivo');
            },
            500: function(){
                toastr.error('Error', '500');
            }
        },
        success:function(data){
            $('.stock_nave_'+cant).html(data);
        }
    });
}

function total_costo(cant){
    var cantidad = $('.cantidadx_'+cant).val(); 
    var costo = $('.costox_'+cant).val(); 
    var can=parseFloat(cantidad);
    var cos=parseFloat(costo);
    var mult=parseFloat(can*cos);
    $('.total_'+cant).val(mult);

    var total=0;
    var TABLA   = $("#table_product tbody > tr");
    TABLA.each(function(){ 
        var num=$(this).find("input[id*='totalx']").val();
        total+=parseFloat(num);

    });    
    $('#total').val(total); 
}


function remover(id,cant){
    if(id==0){
       $('.row_'+cant).remove();
    }else{
       idpro_aux=id;
       cont_aux=cant;
    }
}

function guardar_producto(id){
    var cont=0;
    var DATA  = [];
    var TABLA   = $("#table_product tbody > tr");
    TABLA.each(function(){ 
        cont=1;
        item = {};
        item ["idorden"] = id;
        item ["id"] = $(this).find("input[id*='idx']").val();
        item ["cantidad"] = $(this).find("input[id*='cantidadx']").val();
        item ["costo"] = $(this).find("input[id*='costox']").val();
        item ["idproducto"] = $(this).find("select[class*='idproductox']").val();
        item ["unidad"] = $(this).find("input[id*='unidadIdx']").val();
        item ["total"] = $(this).find("input[id*='totalx']").val();
        item ["idproveedor"] = $('#idproveedor').val();
        DATA.push(item);
    });     
    if(cont==1){
        INFO  = new FormData();
        aInfo   = JSON.stringify(DATA);
        INFO.append('data', aInfo);
        $.ajax({
            data: INFO, 
            type: 'POST',
            url : base_url + 'Entrada/add_detalle_product',
            processData: false, 
            contentType: false,
            async: false,
            statusCode:{
                404: function(data){
                    toastr.error('Error!', 'No Se encuentra el archivo');
                },
                500: function(){
                    toastr.error('Error', '500');
                }
            },
            success: function(data){
            }
        });  
    }
}


function tabla_detalles_product(id){
    $.ajax({
        type:'POST',
        url: base_url+"Entrada/get_tabla_detalles_product",
        data: {id:id},
        success: function (response){
            var array = $.parseJSON(response);
            if(array.length>0) {
                array.forEach(function(element){
                    //console.log("id desde table_detalles: "+element.id);
                    table_data_product(element.id,element.cantidad,element.idproducto,element.nombre,element.costo,element.codigo,element.unidad,element.unidadId,element.stock_suc);
                });
            }   
        },
        error: function(response){
            swal("Error!", "Algo salió mal, intente de nuevo o contacte al administrador del sistema", "error");
        }
    });
}

function get_bancario_proveedor(id){
    $.ajax({
        type:'POST',
        url: base_url+"Entrada/get_bacarios_proveedor",
        data: {id:id},
        statusCode: {
            404: function(data) {
                toastr.error('404','Error');
            },
            500: function(data) {
                console.log(data);
                toastr.error('Algo salió mal, intente de nuevo o contacte al administrador del sistema','Error');
            }
        },
        success:function(data){  
            $('.datos_bancarios').html(data);
            if(idcompra!=0){
                var idproveedorx = $('#idproveedor').val();
                if(idproveedorx!=null){
                    setTimeout(function(){ 
                        $("#idbancario option[value="+$('#idbancariox').val()+"]").attr("selected",true);
                    }, 1000);

                }
            }
        }
    });
}