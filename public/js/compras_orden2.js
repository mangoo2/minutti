var base_url = $('#base_url').val();
var idpro_aux=0;
var cont_aux=0;
var idcompra = $('#idcompra').val();
var requsicionIdx = $('#requsicionIdx').val();
var destinox = $('#destinox').val();
$(document).ready(function() {
	$('#idproveedor').select2({
        width: 'resolve',
        minimumInputLength: 3,
        minimumResultsForSearch: 10,
        placeholder: 'Buscar un proveedor',
        ajax: {
            url: base_url + 'Entrada/search_supplier',
            dataType: "json",
            data: function(params) {
                var query = {
                    search: params.term,
                    type: 'public'
                }
                return query;
            },
            processResults: function(data) {
                var itemscli = [];
                data.forEach(function(element) {
                    itemscli.push({
                        id: element.idproveedor,
                        text: element.nombre
                    });
                });
                return {
                    results: itemscli
                };
            },
        }
    }).on('select2:select', function(e) {
        var data = e.params.data;
        get_bancario_proveedor(data.id);
    });

    $('#personalId').select2({
        width: 'resolve',
        minimumInputLength: 3,
        minimumResultsForSearch: 10,
        placeholder: 'Buscar un empleado',
        ajax: {
            url: base_url + 'Entrada/search_employee',
            dataType: "json",
            data: function(params) {
                var query = {
                    search: params.term,
                    type: 'public'
                }
                return query;
            },
            processResults: function(data) {
                var itemscli = [];
                data.forEach(function(element) {
                    itemscli.push({
                        id: element.personalId,
                        text: element.nombre
                    });
                });
                return {
                    results: itemscli
                };
            },
        }
    });
    
    tabla_detalles_product(idcompra);
    $('.btn_agregar_pro').css('display','none');
    var idproveedorx = $('#idproveedor').val();
    if(idproveedorx!=null){
        get_bancario_proveedor(idproveedorx);
    }
});

function save_data(){
        var form_register = $('#form_data');
        var error_register = $('.alert-danger', form_register);
        var success_register = $('.alert-success', form_register);
        form_register.validate({
            errorElement: 'div', //default input error message container
            errorClass: 'vd_red', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            ignore: "",
            rules: {
                idproveedor: {
                    required: true
                },
                personalId: {
                    required: true
                },
                fecha_surtimiento: {
                    required: true
                },
            },
            errorPlacement: function(error, element) {
                if (element.parent().hasClass("vd_checkbox") || element.parent().hasClass("vd_radio")){
                    element.parent().append(error);
                } else if (element.parent().hasClass("vd_input-wrapper")){
                    error.insertAfter(element.parent());
                }else {
                    error.insertAfter(element);
                }
            }, 
            
            invalidHandler: function (event, validator) { //display error alert on form submit              
                    success_register.fadeOut(500);
                    error_register.fadeIn(500);
                    scrollTo(form_register,-100);

            },

            highlight: function (element) { // hightlight error inputs
        
                $(element).addClass('vd_bd-red');
                $(element).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');

            },

            unhighlight: function (element) { // revert the change dony by hightlight
                $(element)
                    .closest('.control-group').removeClass('error'); // set error class to the control group
            },

            success: function (label, element) {
                label
                    .addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
                    .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
                $(element).removeClass('vd_bd-red');
            }
        });
        var $valid = $("#form_data").valid();
        if($valid) {
            var datos = form_register.serialize()+'&comentarios='+$('#comentarios').val();
            $('.btn_registro').attr('disabled',true);
            $.ajax({
                type:'POST',
                url: base_url+'Entrada/add_orden_compra_aceptar',
                data: datos,
                statusCode:{
                    404: function(data){
                        toastr.error('Error!', 'No Se encuentra el archivo');
                    },
                    500: function(){
                        toastr.error('Error', '500');
                    }
                },
                success:function(data){
                    guardar_producto(parseFloat(data));
                    guardar_producto_nave(parseFloat(data));
                    toastr.success('Hecho!', 'Guardado Correctamente');
                    setTimeout(function(){ 
                        location.href=base_url+'Entrada/listadocompras';
                    }, 2000);
                }
            });
        }

}

function btn_agregar_pro(){
    $('.btn_agregar_pro').css('display','none');
    data_product();
}


function data_product(){
    table_data_product(0,'','','',0,'','','',0);
}
var cant = 0;
function table_data_product(id,cantidad,cantidad_recibida,diferencia,idproducto,producto,codigo,unidad,unidadId){
    var btn='';
    if(cant==0){
        btn='<button type="button" class="btn btn-raised btn-dark round btn-min-width mr-1 mb-1 btn-sm btn_'+cant+'" onclick="data_product('+id+','+cant+')"><i class="fa fa-plus"></i></button>'
    }else{
        btn='<button type="button" class="btn btn-raised btn-danger round btn-min-width mr-1 mb-1 btn-sm" onclick="remover('+id+','+cant+')"><i class="fa fa-trash"></i></button>';
    }
    var html='<tr class="row_'+cant+'">\
                <td>\
                   <input hidden class="form-control" id="idx" value="'+id+'">\
                   <input hidden class="form-control" id="cantidadx" value="'+cantidad+'">\
                   '+cantidad+'\
                </td>\
                <td>\
                    <div class="cantidad_nave_'+cant+'"></div>\
                </td>\
                <td>\
                   <input type="number" readonly class="form-control cantidad_recibidax_'+cant+'" id="cantidad_recibidax" style="width: 95px;height: 32px;" value="'+cantidad_recibida+'">\
                </td>\
                <td>\
                   <input type="number" readonly class="form-control" id="diferenciax" value="'+diferencia+'">\
                </td>\
                <td>\
                   <input hidden class="form-control" id="idproductox" value="'+idproducto+'">\
                   '+producto+'\
                </td>\
                <td>\
                   '+codigo+'\
                </td>\
                <td>\
                   '+unidad+'\
                </td>\
                <td>\
                   '+btn+'\
                </td>\
              </tr>';
    $('#tbody_data').append(html);
    select_producto(cant);
    get_nave(cant,id,idproducto,cantidad);
    cant++;
}

function select_producto(cant){
    $('.idproductox_'+cant).select2({
        width: '100%',
        minimumInputLength: 3,
        minimumResultsForSearch: 10,
        placeholder: 'Buscar un producto',
        ajax: {
            url: base_url+'Entrada/search_product',
            dataType: "json",
            data: function(params) {
                var query = {
                    search: params.term,
                    type: 'public'
                }
                return query;
            },
            processResults: function(data) {
                var itemscli = [];
                data.forEach(function(element) {
                    itemscli.push({
                        id: element.productoId,
                        text: element.nombre,
                        codigo: element.codigo,
                        unidad: element.unidad,
                        unidadId: element.unidadId,
                    });
                });
                return {
                    results: itemscli
                };
            },
        }
    }).on('select2:select', function(e) {
        var data = e.params.data;
        $('.codigox_'+cant).val(data.codigo);
        $('.unidadx_'+cant).val(data.unidad);
        $('.unidadIdx_'+cant).val(data.unidadId);
    });
}

function get_nave(cant,id,idpro,cantidad){
    $.ajax({
        type:'POST',
        url: base_url+'Entrada/get_detalle_nave',
        data: {cant:cant,id:id,idprod:idpro,idcompra_orden:idcompra,requsicionId:requsicionIdx,destino:destinox,cantidad:cantidad},
        statusCode:{
            404: function(data){
                toastr.error('Error!', 'No Se encuentra el archivo');
            },
            500: function(){
                toastr.error('Error', '500');
            }
        },
        success:function(data){
            $('.cantidad_nave_'+cant).html(data);
        }
    });
}
function remover(id,cant){
    if(id==0){
       $('.row_'+cant).remove();
    }else{
       idpro_aux=id;
       cont_aux=cant;
    }
}

function guardar_producto(id){
    var cont=0;
    var DATA  = [];
    var TABLA   = $("#table_product tbody > tr");
    TABLA.each(function(){ 
        cont=1;
        item = {};
        item ["id"] = $(this).find("input[id*='idx']").val();
        item ["cantidad_recibida"] = $(this).find("input[id*='cantidad_recibidax']").val();
        item ["diferencia"] = $(this).find("input[id*='diferenciax']").val();
        DATA.push(item);
    });     
    if(cont==1){
        INFO  = new FormData();
        aInfo   = JSON.stringify(DATA);
        INFO.append('data', aInfo);
        $.ajax({
            data: INFO, 
            type: 'POST',
            url : base_url + 'Entrada/add_detalle_product_update',
            processData: false, 
            contentType: false,
            async: false,
            statusCode:{
                404: function(data){
                    toastr.error('Error!', 'No Se encuentra el archivo');
                },
                500: function(){
                    toastr.error('Error', '500');
                }
            },
            success: function(data){
            }
        });  
    }
}

function guardar_producto_nave(id){
    var cont=0;
    var DATA  = [];
    var TABLA   = $("#table_product tbody > tr");
    TABLA.each(function(){ 
        cont=1;
        item = {};
        var DATAconf_d  = [];
        var TABLAconf_d = $(this).find('td div > span');
        TABLAconf_d.each(function(){ 
            item_d = {};
            item_d ["idorden"] = id;
            item_d ["idstock"] = $(this).find("input[id*='idstockx']").val();
            item_d ["idordendetalle"] = $(this).find("input[id*='idordendetallex']").val();
            item_d ["idproducto"] = $(this).find("input[id*='idproductodx']").val();
            item_d ["sucursalId"] = $(this).find("input[id*='sucursalIdx']").val();
            item_d ["stock"] = $(this).find("input[id*='stockxv']").val();
            DATAconf_d.push(item_d);
            
        });
        
        item ["productos_stock"]   = DATAconf_d;
        DATA.push(item);
    });     
    if(cont==1){
        INFO  = new FormData();
        aInfo   = JSON.stringify(DATA);
        INFO.append('data', aInfo);
        $.ajax({
            data: INFO, 
            type: 'POST',
            url : base_url + 'Entrada/add_detalle_product_nave',
            processData: false, 
            contentType: false,
            async: false,
            statusCode:{
                404: function(data){
                    toastr.error('Error!', 'No Se encuentra el archivo');
                },
                500: function(){
                    toastr.error('Error', '500');
                }
            },
            success: function(data){
            }
        });  
    }
}

function tabla_detalles_product(id){
    $.ajax({
        type:'POST',
        url: base_url+"Entrada/get_tabla_detalles_product",
        data: {id:id},
        success: function (response){
            var array = $.parseJSON(response);
            if(array.length>0) {
                array.forEach(function(element){
                    table_data_product(element.id,element.cantidad,element.cantidad_recibida,element.diferencia,element.idproducto,element.nombre,element.codigo,element.unidad,element.unidadId);
                });
            }   
        },
        error: function(response){
            swal("Error!", "Algo salió mal, intente de nuevo o contacte al administrador del sistema", "error");
        }
    });
}

function validar_suma(sucursalId,idprod,cant){
    var TABLA   = $("#table_product tbody > .row_"+cant);
    var suma=0;
    TABLA.each(function(){ 
        cantidadx = $(this).find("input[id*='cantidadx']").val();
        
        var can = parseFloat(cantidadx);   
        var DATAconf_d  = [];
        var TABLAconf_d = $(this).find('td div > .row_d_'+cant); 
        TABLAconf_d.each(function(){ 
            stock = $(this).find("input[id*='stockxv']").val();
            var stock_aux=0;
            if(stock==NaN || stock==''){
                stock_aux=0;
            }else{
                stock_aux=stock;
            }
            suma+=parseFloat(stock_aux);  
        });
       
        if(suma<=can){
            $('.cantidad_recibidax_'+cant).val(suma);
            var dife=parseFloat(can-suma);
            $(this).find("input[id*='diferenciax']").val(dife);
        }else{
            $('.idsucu_'+sucursalId+'_'+idprod).val('');
            toastr.error('La cantidad de nave es mayor a la de cantidad','Atención');
        }
    }); 
}

function get_bancario_proveedor(id){
    $.ajax({
        type:'POST',
        url: base_url+"Entrada/get_bacarios_proveedor",
        data: {id:id},
        statusCode: {
            404: function(data) {
                toastr.error('404','Error');
            },
            500: function(data) {
                console.log(data);
                toastr.error('Algo salió mal, intente de nuevo o contacte al administrador del sistema','Error');
            }
        },
        success:function(data){  
            $('.datos_bancarios').html(data);
            if(idcompra!=0){
                var idproveedorx = $('#idproveedor').val();
                if(idproveedorx!=null){
                    setTimeout(function(){ 
                        $("#idbancario option[value="+$('#idbancariox').val()+"]").attr("selected",true);
                    }, 1000);

                }
            }
        }
    });
}