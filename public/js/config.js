var base_url=$('#base_url').val();
var tabla_tipo;
$(document).ready(function() {
    reload_tipo()
});

function add_tipo(){
    var form_register = $('#form_tipo');
    var error_register = $('.alert-danger', form_register);
    var success_register = $('.alert-success', form_register);
    form_register.validate({
        errorElement: 'div', //default input error message container
        errorClass: 'vd_red', // default input error message class
        focusInvalid: false, // do not focus the last invalid input
        ignore: "",
        rules: {
            categoria: {
                required: true
            }
        },
        errorPlacement: function(error, element) {
            if (element.parent().hasClass("vd_checkbox") || element.parent().hasClass("vd_radio")){
                element.parent().append(error);
            } else if (element.parent().hasClass("vd_input-wrapper")){
                error.insertAfter(element.parent());
            }else {
                error.insertAfter(element);
            }
        }, 
        
        invalidHandler: function (event, validator) { //display error alert on form submit              
                success_register.fadeOut(500);
                error_register.fadeIn(500);
                scrollTo(form_register,-100);

        },

        highlight: function (element) { // hightlight error inputs
    
            $(element).addClass('vd_bd-red');
            $(element).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');

        },

        unhighlight: function (element) { // revert the change dony by hightlight
            $(element)
                .closest('.control-group').removeClass('error'); // set error class to the control group
        },

        success: function (label, element) {
            label
                .addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
                .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
            $(element).removeClass('vd_bd-red');
        }
    });
    var $valid = $("#form_tipo").valid();
    if($valid) {
        var datos = form_register.serialize();
        $('.btn_tipo').attr('disabled',true);
        $.ajax({
            type:'POST',
            url: base_url+'Config/addtipo',
            data: datos,
            statusCode:{
                404: function(data){
                    toastr.error('Error!', 'No Se encuentra el archivo');
                },
                500: function(){
                    toastr.error('Error', '500');
                }
            },
            success:function(data){
                toastr.success('Hecho!', 'Guardado Correctamente');
                setTimeout(function(){ 
                    $('.btn_tipo').attr('disabled',false);
                    $('#idconfig').val(0);
                    $('#nombre').val('');
                }, 3000);
                reload_tipo();
            }
        });
    }
}

function reload_tipo(){
    tabla_tipo=$("#table_tipo").DataTable();
    tabla_tipo.destroy();
    table_tipo();
}
function table_tipo(){
    tabla_tipo=$("#table_tipo").DataTable({
        "bProcessing": true,
        "serverSide": true,
        "searching": true,
        responsive: !0,
        //"info":     false,
        //"paging": false,
        "ajax": {
            "url": base_url+"Config/getlistado_tipo",
            type: "post",
            error: function(){
               $("#table_tipo").css("display","none");
            }
        },
        "columns": [
            {"data":"id"},
            {"data":"nombre"},
            {"data": null,
                "render": function ( data, type, row, meta ){
                var html='<button type="button"\
                            data-idtipo="'+row.id+'"\
                            data-camioneta="'+row.nombre+'"\
                            class="btn btn-flat btn-danger tipo_c_'+row.id+'" onclick="editar_tipo('+row.id+')"><i class="fa fa-edit icon_font"></i></button>\
                            <button type="button" class="btn btn-flat btn-nefro color_hover" onclick="delete_registro_tipo('+row.id+');"><i class="fa fa-trash-o icon_font"></i> </button>';
                return html;
                }
            },
        ],
        "order": [[ 0, "desc" ]],
        "lengthMenu": [[5, 10, 25], [5, 10, 25]],
        // Cambiamos lo principal a Español
        "language": {
            "lengthMenu": "Mostrar _MENU_ entradas",
            "zeroRecords": "Lo sentimos - No se han encontrado elementos",
            "sInfo":  "Mostrando registros del _START_ al _END_ de un total de _TOTAL_  registros",
            "info": "Mostrando página _PAGE_ de _PAGES_",
            "infoEmpty": "No hay registros disponibles",
            "infoFiltered": "(Filtrado de _MAX_ registros totales)",
            "search": "Buscar : _INPUT_",
            "paginate": {
                "previous": "Página previa",
                "next": "Siguiente página"
              }
        }
    
    });
}
function editar_tipo(id){
    $('#idconfig').val($('.tipo_c_'+id).data('idtipo'));
    $('#nombre').val($('.tipo_c_'+id).data('camioneta'));
}

function delete_registro_tipo(id){
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: '¿Está seguro de eliminar tipo de camioneta?',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                $.ajax({
                    type:'POST',
                    url: base_url+"index.php/Config/delete_tipo",
                    data: {
                        id:id
                    },
                    statusCode: {
                        404: function(data) {
                            toastr.error('404','Error');
                        },
                        500: function(data) {
                            console.log(data);
                            toastr.error('Algo salió mal, intente de nuevo o contacte al administrador del sistema','Error');
                        }
                    },
                    success:function(response){  
                        toastr.success( 'Eliminado correctamente','Hecho!');
                        reload_tipo();
                    }
                });
            },
            cancelar: function () 
            {
                
            }
        }
    });
}