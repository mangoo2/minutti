var base_url = $('#base_url').val();
var table;
$(document).ready(function ($) {
	table = $('#table-lis').DataTable();
	//showData();
	loadtable();
});

function showData() {
	$.ajax({
		url: base_url + "index.php/Configuracion_transporte/getTabla",
		success: function (result) {
			//console.log(result);
			$("#table-lis").html(result);
		}
	});
};

function loadtable() {
	table = $('#table-lis').DataTable({
		destroy:true,
		"ajax": {
			"url": base_url + "index.php/Configuracion_transporte/getTabla",
		},
		"columns": [{
				"data": "nombre"
			},
			{
				"data": null,
				render: function (data, type, row) {
					//console.log(data);
					var html = '';
					html += '<div class="text-center" style="min-width:252px;">';
					html += '<a onclick="modal_details(' + row.id + ')" class="btn btn-flat btn-light"><i class="fa fa-eye icon_font"></i></a>';
					html += '</div>';
					return html;
				}
			},
			{
				"data": null,
				render: function (data, type, row) {
					//console.log(data);
					var html = '';
					html += '<div class="text-center" style="min-width:252px;">';
					html += '<a href="' + base_url + 'Configuracion_transporte/add/' + row.id + '" class="btn btn-flat btn-light"><i class="fa fa-edit icon_font"></i></a>';
					html += '<a onclick="eliminarconfig(' + row.id + ')" class="btn btn-flat btn-light"><i class="fa fa-trash-o icon_font"></i></a>';
					html += '</div>';
					return html;
				}
			},
		],
		"order": [
			[1, "desc"]
		],
		"lengthMenu": [
			[25, 50, 100],
			[25, 50, 100]
		],
	});
}

function modal_details(id) {
	$('#modal_details').modal();
	$.ajax({
		url: base_url+"Configuracion_transporte/getDetalleTabla",
		type: "post",
		data: { id:id},
		success: function (result) {
			//console.log(result);
			$(".list_productos").html(result);
			$("#tabla_dets").DataTable();
		}
	});
}



function eliminarconfig(id) {
	$.confirm({
		boxWidth: '30%',
		useBootstrap: false,
		icon: 'fa fa-warning',
		title: 'Atención!',
		content: '¿Está seguro de Eliminar la configuración?',
		type: 'red',
		typeAnimated: true,
		buttons: {
			confirmar: function () {
				$.ajax({
					type: 'POST',
					url: base_url+"Configuracion_transporte/deleteg",
					data: {
						id: id
					},
					statusCode: {
						404: function (data) {
							toastr.error('404', 'Error');
						},
						500: function (data) {
							//console.log(data);
							toastr.error('Algo salió mal, intente de nuevo o contacte al administrador del sistema', 'Error');
						}
					},
					success: function (response) {
						toastr.success('Eliminado correctamente', 'Hecho!');
						loadtable();
					}
				});
			},
			cancelar: function () {

			}
		}
	});
}
