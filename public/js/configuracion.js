var base_url = $('#base_url').val();

function subconfig(){
	subconfig_data(0,'','','')
	
}
function subconfig_data_row(conrow,id){
    $.ajax({
            type:'POST',
            url: base_url+"index.php/Configuraciones/config_d_d",
            data: {id:id},
            success: function (data){
                //console.log(data)
                var array = $.parseJSON(data);
                array.forEach(function(element) {
                    //console.log(configrow+'-'+1);
                    //var configrow_num=parseInt(configrow)-parseInt(1);
                    //console.log(configrow_num);
                    //console.log('xx configddId '+configrow_num+'-'+element.configddId);
                    add_config(conrow,element.configddId,element.piezas,element.productoId,element.codigo,element.nombre);
                });
               
            },
            error: function(data){ 
                toastr.error('Error', 'Algo salió mal, intente de nuevo o contacte al administrador del sistema');
            }
        });
}
var configrow=0;
function subconfig_data(configdId,nombresub){
    var selectedinsumos_hide=$('#selectedinsumos_hide').html();
	if(configrow==0){
		var configrowactive=' active ';
	}else{
		var configrowactive='';
	}
    //console.log('configdId: '+configdId);
    if(configdId>0){
        //console.log('entra: '+configdId);
        subconfig_data_row(configrow,configdId);
        
        
    }
    nombre_menu='';
    if(nombresub!=''){
        nombre_menu='<span class="menu_row_'+configrow+'">'+nombresub+'</span>';
    }else{
        nombre_menu='<span class="menu_row_'+configrow+'">Conf</span>';
    }
	var tabs='<li class="nav-item">\
                <a class="nav-link '+configrowactive+'" \
                	id="base-tab'+configrow+'" \
                	data-toggle="tab" \
                	aria-controls="tab'+configrow+'" \
                	href="#tab'+configrow+'" \
                	aria-expanded="true">'+nombre_menu+'. '+configrow+'</a>\
              </li>';
    var tabsbody ='<div role="tabpanel" \
    					class="tab-pane '+configrowactive+'" \
    					id="tab'+configrow+'" \
    					aria-expanded="true" \
    					aria-labelledby="base-tab'+configrow+'">\
                        <div class="row">\
                        	<div class="col-md-3" style="padding-right: 0px;">Nombre de subconfiguración</div>\
                        	<div class="col-md-9">\
                        		<input type="hidden" id="configdId" class="form-control" value="'+configdId+'">\
                                <input type="text" id="nombresub" class="form-control nombre_sub_'+configrow+'" oninput="nombre_sub('+configrow+')" value="'+nombresub+'">\
                        	</div>\
                        </div>\
                        <br>\
                        <div class="row class_row_add_items">\
                        	<div class="col-md-3">Número de piezas</div>\
                        	<div class="col-md-2">\
                        		<input type="number" id="piezas_'+configrow+'" class="form-control" value="">\
                        	</div>\
                        	<div class="col-md-2">Producto</div>\
                        	<div class="col-md-4">\
                        		<select id="produto_'+configrow+'" class="form-control">\
                                    <option value="0">Seleccione una opción</option>\
                                    '+selectedinsumos_hide+' \
                        		</select>\
                        	</div>\
                        	<div class="col-md-1">\
                        		<button type="button" class="btn btn-raised btn-danger btn-min-width mr-1 mb-1 button_save" style="float: right;" onclick="add_config('+configrow+',0,0,0,0,0)"><i class="fa fa-plus"></i></button>\
                        	</div>\
                        </div>\
                        <div class="row">\
                            <div class="col-md-12">\
                                <table class="table thead-inverse tableinsumos_'+configrow+'">\
                                    <thead>\
                                        <tr>\
                                            <th>Cantidad</th>\
                                            <th>Código Pieza</th>\
                                            <th>piezas</th>\
                                            <th></th>\
                                        </tr>\
                                    </thead>\
                                    <tbody class="tbody_tableinsumos_'+configrow+'">\
                                    </tbody>\
                                </table>\
                            </div>\
                        </div>\
                      </div>';
    $('.addsubconfiguracion_tabs').append(tabs);
    $('.addsubconfiguracion_body').append(tabsbody);
    producto_select(configrow);
    configrow++;
}

function producto_select(cant){
    $('#produto_'+cant).select2({
        width: 'resolve',
        minimumInputLength: 3,
        minimumResultsForSearch: 10,
        placeholder: 'Buscar un producto',
        ajax: {
            url: base_url+'Configuraciones/search_producto',
            dataType: "json",
            data: function (params) {
            var query = {
                search: params.term,
                type: 'public'
            }
            return query;
        },
        processResults: function(data){
            var clientes=data;
            var itemscli = [];
            data.forEach(function(element) {
                $(element).data('codigo', element.codigo);
                itemscli.push({
                    id: element.productoId,
                    text: element.nombre,
                    codigo: element.codigo
                });
            });
            return {
                results: itemscli
            };          
        },  
      }
    });
}


var add_config_row=0;



function add_config(row,configddId,piezas_row,productoId_row,pro_codigo,pro_nombre){
    var piezas=$('#piezas_'+row).val();
    var codigo=$('#produto_'+row+' option:selected').data('codigo');
    var productotext=$('#produto_'+row+' option:selected').text();
    var producto=$('#produto_'+row+' option:selected').val();
    if(piezas_row>0){
        piezas=piezas_row;
    }
    console.log(productoId_row);
    if(productoId_row>0){
        codigo=pro_codigo;
        productotext=pro_nombre;
        producto=productoId_row;
    }
    if(piezas!=0 && piezas!='' && producto!=0){
        var add_config='<tr class="add_config_delete_'+add_config_row+'">\
                            <td>\
                                <input type="hidden" id="configddId" value="'+configddId+'">\
                                <input type="hidden" id="piezas" value="'+piezas+'">\
                                '+piezas+'\
                            </td>\
                            <td>'+codigo+'</td>\
                            <td>\
                                <input type="hidden" id="productoId" value="'+producto+'">\
                                '+productotext+'\
                            </td>\
                            <td>\
                                <button type="button" class="btn btn-raised btn-danger btn-min-width mr-1 mb-1 button_save" style="float: right;" onclick="add_config_delete('+add_config_row+','+configddId+')"><i class="fa fa-minus"></i></button>\
                            </td>\
                        </tr>';
        $('.tbody_tableinsumos_'+row).append(add_config);
        add_config_row++;
        var piezas=$('#piezas_'+row).val('');
    }else{
        toastr.error('Los dos campos deben estar llenos o seleccionados','Atención!');
    }
}
function add_config_delete(row,id){
    if(id>0){
        $.confirm({
            boxWidth: '30%',
            useBootstrap: false,
            icon: 'fa fa-warning',
            title: 'Atención!',
            content: '¿Está seguro de Eliminar el producto?',
            type: 'red',
            typeAnimated: true,
            buttons:{
                confirmar: function (){
                    $.ajax({
                        type:'POST',
                        url: base_url+"index.php/Configuraciones/deleteproduto",
                        data: {
                            producto:id
                        },
                        statusCode: {
                            404: function(data) {
                                toastr.error('404','Error');
                            },
                            500: function(data) {
                                console.log(data);
                                toastr.error('Algo salió mal, intente de nuevo o contacte al administrador del sistema','Error');
                            }
                        },
                        success:function(response){  
                            toastr.success( 'Eliminado correctamente','Hecho!');
                            $('.add_config_delete_'+row).remove(); 
                        }
                    });
                },
                cancelar: function () 
                {
                    
                }
            }
        });
    }else{
       $('.add_config_delete_'+row).remove(); 
    }
        
}
function save(){
    var DATAconf  = [];
    var TABLAconf   = $(".addsubconfiguracion .addsubconfiguracion_body > .tab-pane");
        TABLAconf.each(function(){         
        item = {};
        item ["configdId"] = $(this).find("input[id*='configdId']").val();
        item ["nombresub"]   = $(this).find("input[id*='nombresub']").val();
        //=======================================
            var DATAconf_d  = [];
            var TABLAconf_d = $(this).find('table tbody > tr');
            TABLAconf_d.each(function(){ 
                item_d = {};
                item_d ["configddId"] = $(this).find("input[id*='configddId']").val();
                item_d ["piezas"] = $(this).find("input[id*='piezas']").val();
                item_d ["productoId"]   = $(this).find("input[id*='productoId']").val();
                DATAconf_d.push(item_d);
            });
        //=======================================
        item ["productos"]   = DATAconf_d;
        DATAconf.push(item);
    });
    INFO  = new FormData();
    arrayconfiguracion   = JSON.stringify(DATAconf);
    console.log(arrayconfiguracion);
    console.log($.parseJSON(arrayconfiguracion)); 
    var datos = $('#form-configuracion').serialize()+'&arrayconfiguracion='+arrayconfiguracion;
    $.ajax({
        type:'POST',
        url: base_url+"index.php/Configuraciones/insertupdate",
        data: datos,
        statusCode: {
            404: function(data) {
                toastr.error('Error', '404');
            },
            500: function(data) {
                console.log(data);
                toastr.error('Error', 'Algo salió mal, intente de nuevo o contacte al administrador del sistema');
            }
        },
        success: function (response){
            toastr.success( 'Configuración agregado','Hecho!');
            
            
            setTimeout(function(){ 
                window.location.href = base_url+"index.php/Configuraciones"; 
            }, 2000);
            
        },
        error: function(response){
            toastr.error('Error', 'Algo salió mal, intente de nuevo o contacte al administrador del sistema');
        }
    });
}
function obtenerdatosproducto(idp){
    $.ajax({
        type:'POST',
        url: base_url+"index.php/Configuraciones/obtenerdatosproducto",
        data: {id:idp},
        success: function (data){
            var array = $.parseJSON(data);
            $('#configId').val(array.configId);
            $('#codigo').val(array.codigo);
            $('#nombre').val(array.nombre);
            $('#descripcion').val(array.descripcion );
            array.config_d.forEach(function(element) {
                subconfig_data(element.configdId,element.nombresub);
            });
           
        },
        error: function(data){ 
            toastr.error('Error', 'Algo salió mal, intente de nuevo o contacte al administrador del sistema');
        }
    });
}

function nombre_sub(num){
    var nombre = $('.nombre_sub_'+num).val();
    $('.menu_row_'+num).html(nombre);
}