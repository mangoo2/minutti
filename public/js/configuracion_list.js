var base_url = $('#base_url').val();
var table;
$(document).ready(function($) {
	table=$('#table-lis').DataTable();
	loadtable();
});
function loadtable(){

	table.destroy();
	table = $('#table-lis').DataTable({
        "bProcessing": true,
        "serverSide": true,
        "searching": true,
        responsive: !0,
        "ajax": {
            "url": base_url+"index.php/Configuraciones/getlistconfig",
            type: "post",
            "data": {
                'col':0
            },
        },
        "columns": [
            {"data": "configId"},
            {"data": "codigo"},
            {"data": "nombre"},
            {"data": null,
                render:function(data,type,row){
                    var html='';
                    	html+='<div style="min-width:252px;">';
                        html+='<a href="'+base_url+'Configuraciones/add/'+row.configId+'" class="btn btn-flat btn-danger"><i class="fa fa-edit icon_font"></i></a>';
                        html+='<a href="'+base_url+'Configuraciones/add/'+row.configId+'/1" class="btn btn-flat btn-danger"><i class="fa fa-eye icon_font"></i></a>';
                        html+='<a onclick="eliminarconfig('+row.configId+')" class="btn btn-flat btn-danger"><i class="fa fa-trash-o icon_font"></i></a>';
                    	html+='</div>';
                    return html;
                }
            },
            
        ],
        "order": [[ 1, "desc" ]],
        "lengthMenu": [[25, 50, 100], [25, 50,100]],
    });
}
function eliminarconfig(id){
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: '¿Está seguro de Eliminar la configuración?',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                $.ajax({
                    type:'POST',
                    url: base_url+"index.php/Configuraciones/deleteg",
                    data: {
                        id:id
                    },
                    statusCode: {
                        404: function(data) {
                            toastr.error('404','Error');
                        },
                        500: function(data) {
                            console.log(data);
                            toastr.error('Algo salió mal, intente de nuevo o contacte al administrador del sistema','Error');
                        }
                    },
                    success:function(response){  
                        toastr.success( 'Eliminado correctamente','Hecho!');
                        loadtable();
                    }
                });
            },
            cancelar: function () 
            {
                
            }
        }
    });
}