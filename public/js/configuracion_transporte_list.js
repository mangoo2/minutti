var base_url = $('#base_url').val();
var configrow = 0;
var add_config_row = 0;
producto_select(configrow);

var codigo="";
function producto_select(cant) {
	$('#produto').select2({
		width: '90%',
		minimumInputLength: 2,
		minimumResultsForSearch: 10,
		placeholder: 'Buscar un producto',
		ajax: {
			url: base_url + 'Configuraciones/search_producto',
			dataType: "json",
			data: function (params) {
				var query = {
					search: params.term,
					type: 'public'
				}
				return query;
			},
			processResults: function (data) {
				//var clientes = data;
				var itemscli = [];
				data.forEach(function (element) {
					$(element).data('codigo', element.codigo);
					itemscli.push({
						id: element.productoId,
						text: element.nombre,
						codigo: element.codigo
					});
				});
				return {
					results: itemscli
				};
			},
		}
	}).on('select2:select', function (e) {
		var data = e.params.data;
        codigo = data.codigo;
        //console.log("codigo: "+codigo);

    });
    if($("#configId").val()>0){
    	getDetalles();
    }
}

function add_config(configddId) {
	var piezas = $('#piezas').val();
	var codigo2 = codigo;
	var productotext = $('#produto' + ' option:selected').text();
	var producto = $('#produto' + ' option:selected').val();
	/*console.log("piezas: " + piezas);
	console.log("codigo2: " + codigo2);
	console.log("productotext: " + productotext);
	console.log("producto: " + producto);*/
	if(!codigo2){
		codigo2 = " ";
	}

	if (piezas != 0 && piezas != '' && producto != 0) {
		var add_config = '<tr class="add_config_delete_' + add_config_row + '">\
						<td>\
								<input type="hidden" id="configddId" value="' + configddId + '">\
								<input type="hidden" id="piezas" value="' + piezas + '">\
								' + piezas + '\
						</td>\
						<td>' + codigo2 + '</td>\
						<td>\
								<input type="hidden" id="productoId" value="' + producto + '">\
								' + productotext + '\
						</td>\
						<td>\
								<button type="button" class="btn btn-raised btn-danger btn-min-width mr-1 mb-1 button_save" style="float: right;" onclick="add_config_delete(' + add_config_row + ',' + configddId + ')"><i class="fa fa-minus"></i></button>\
						</td>\
				</tr>';
		$('.list_productos').append(add_config);
		add_config_row++;
		var piezas = $('#piezas').val('');
		$("#produto").val("").change();
	} else {
		toastr.error('Los dos campos deben estar llenos o seleccionados', 'Atención!');
	}
}

function getDetalles(){
    $.ajax({
        type:'POST',
        url: base_url+"Configuracion_transporte/getDetalles",
        data: {
            id:$("#configId").val()
        },
        success: function (data){
            var array = $.parseJSON(data);
            //console.log("length: "+array.length);
            //console.log("nombre: "+array.nombre);
            //if(array.length>0){
                $('#nombre').val(array.nombre);
                $.each(array.detalle, function(index, i) {
                	var dets = '<tr class="add_config_deleteexi_' + i.id + '">\
						<td>\
								<input type="hidden" id="configddId" value="' + i.id + '">\
								<input type="hidden" id="piezas" value="' + i.cantidad + '">\
								' + i.cantidad + '\
						</td>\
						<td>' +i.codigo+ '</td>\
						<td>\
								<input type="hidden" id="productoId" value="' + i.productoId + '">\
								' + i.nombre + '\
						</td>\
						<td>\
								<button type="button" class="btn btn-raised btn-danger btn-min-width mr-1 mb-1 button_save" style="float: right;" onclick="add_config_delete(' + i.id + ',' + i.id + ')"><i class="fa fa-minus"></i></button>\
						</td>\
					</tr>';
					$('.list_productos').append(dets);
                }); 
            //}
        },
        error: function(response){
            swal("Error!", "Se produjo un error, intente de nuevamente o contacte al administrador del sistema", "error") 
        }
    });
}

function add_config_delete(row, id) {
	if (id > 0) {
		$.confirm({
			boxWidth: '30%',
			useBootstrap: false,
			icon: 'fa fa-warning',
			title: 'Atención!',
			content: '¿Está seguro de Eliminar el producto?',
			type: 'red',
			typeAnimated: true,
			buttons: {
				confirmar: function () {
					$.ajax({
						type: 'POST',
						url: base_url+"Configuracion_transporte/deleteDetalle",
						data: {
							id: id
						},
						statusCode: {
							404: function (data) {
								toastr.error('404', 'Error');
							},
							500: function (data) {
								//console.log(data);
								toastr.error('Algo salió mal, intente de nuevo o contacte al administrador del sistema', 'Error');
							}
						},
						success: function (response) {
							toastr.success('Eliminado correctamente', 'Hecho!');
							$('.add_config_deleteexi_' + row).remove();
						}
					});
				},
				cancelar: function () {}
			}
		});
	} else {
		$('.add_config_delete_' + row).remove();
	}

}

function save() {
	//=======================================
	var DATAconf_d = [];
	var TABLAconf_d = $('#tabla_det tbody > tr');
	TABLAconf_d.each(function () {
		item_d = {};
		item_d["configId"] = $("#configId").val();
		item_d["nombre"] = $("#nombre").val();
		item_d["configddId"] = $(this).find("input[id*='configddId']").val();
		item_d["piezas"] = $(this).find("input[id*='piezas']").val();
		item_d["productoId"] = $(this).find("input[id*='productoId']").val();
		DATAconf_d.push(item_d);
	});
	//=======================================
	//productos = JSON.stringify(DATAconf_d);
	INFO  = new FormData();
    aInfo   = JSON.stringify(DATAconf_d);
    INFO.append('data', aInfo);
    //console.log("INFO: "+INFO);
	//console.log("DATAconf_d: "+DATAconf_d);
	$.ajax({
		type: 'POST',
		url: base_url+"Configuracion_transporte/insertupdate",
		data: INFO,
		processData: false, 
        contentType: false,
        async: false,
        beforeSend: function(){
            $(".button_save").attr("disabled",true);
        },
		success: function (data) {
			toastr.success('Configuración agregado', 'Hecho!');
			//console.log(data);
			setTimeout(function () {
				window.location.href = base_url+"Configuracion_transporte";
			}, 2000);
		},
		error: function (response) {
			toastr.error('Error', 'Algo salió mal, intente de nuevo o contacte al administrador del sistema');
		}
	});
}

/*function save() {
	var DATAconf = [];
	var TABLAconf = $(".addsubconfiguracion .addsubconfiguracion_body > .tab-pane");
	TABLAconf.each(function () {
		item = {};
		item["configdId"] = $(this).find("input[id*='configdId']").val();
		item["nombresub"] = $(this).find("input[id*='nombresub']").val();
		//=======================================
		var DATAconf_d = [];
		var TABLAconf_d = $(this).find('table tbody > tr');
		TABLAconf_d.each(function () {
			item_d = {};
			item_d["configddId"] = $(this).find("input[id*='configddId']").val();
			item_d["piezas"] = $(this).find("input[id*='piezas']").val();
			item_d["productoId"] = $(this).find("input[id*='productoId']").val();
			DATAconf_d.push(item_d);
		});
		//=======================================
		item["productos"] = DATAconf_d;
		DATAconf.push(item);
	});
	INFO = new FormData();
	arrayconfiguracion = JSON.stringify(DATAconf);
	console.log(arrayconfiguracion);
	console.log($.parseJSON(arrayconfiguracion));
	//	var datos = arrayconfiguracion;
	var datos = 'configId='+item_d["configddId"] +'&arrayconfiguracion='+arrayconfiguracion;

	console.log("DATAconf: " + JSON.stringify(DATAconf));
	console.log("DATA: " + datos);
	$.ajax({
		type: 'POST',
		url: base_url + "index.php/Configuracion_transporte/insertupdate",
		data: datos,
		statusCode: {
			404: function (data) {
				toastr.error('Error', '404');
			},
			500: function (data) {
				toastr.error('Error', 'Algo salió mal, intente de nuevo o contacte al administrador del sistema');
			}
		},
		success: function (data) {
			toastr.success('Configuración agregado', 'Hecho!');
			console.log(data);
		},
		error: function (response) {
			toastr.error('Error', 'Algo salió mal, intente de nuevo o contacte al administrador del sistema');
		}
	});
}
*/

function nombre_sub(num) {
	var nombre = $('.nombre_sub_' + num).val();
	$('.menu_row_' + num).html(nombre);
}
