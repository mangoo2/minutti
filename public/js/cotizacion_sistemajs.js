var base_url = $('#base_url').val();
var table;
$(document).ready(function () {
	loadtable();
});

function loadtable() {
	table = $('#table-list').DataTable({
		"bProcessing": true,
		"serverSide": true,
		"searching": true,
		destroy: true,
		"ajax": {
			"url": base_url + "Nueva_cotizacion/getlist",
			type: "POST",
		},
		"columns": [{
				"data": "folio"
			},
			{
				"data": "vehiculo",
				render: function (data, type, row) {
					let option;
					switch (row.vehiculo) {
						case "1":
							option = "Sprinter";
							break;
						case "2":
							option = "Crafter";
							break;
						default:
							option = " ";
							break;
					}
					return option;
				}
			},
			{
				"data": "equipamiento",
				render: function (data, type, row) {
					let option;
					if (row.vehiculo == 1) {
						switch (row.equipamiento) {
							case "1":
								option = "Turismo alto";
								break;
							case "2":
								option = "Turismo medio";
								break;
							case "3":
								option = "Transporte de personal";
								break;
							case "4":
								option = "Transporte publico";
								break;
							default:
								option = " ";
								break;
						}
					} else if (row.vehiculo == 2) {
						switch (row.equipamiento) {
							case "1":
								option = "Turismo alto";
								break;
							case "2":
								option = "Turismo medio";
								break;
							case "3":
								option = "Urbana";
								break;
							default:
								option = " ";
								break;
						}
					}
					return option;
				}
			},
			{
				"data": "registro"
			},
			{
				"data": "estatus",
				render: function (data, type, row) {
					var html = '';
					if (row.estatus == 1) {
						html += '<div class="text-center rounded bg-danger text-white">';
						html += 'Pendiente';
					} else {
						html += '<div class="text-center rounded bg-success text-white">';
						html += 'Completo';
					}
					html += '</div>';
					return html;
				}
			},
			{
				"data": "usuario"
			},
			{
				"data": null,
				render: function (data, type, row) {
					var html = '';
					html += '<div class="text-center">';
					html += '<a target="_blank" title="Generar PDF" href=" ' + base_url + 'Nueva_cotizacion/formatocotizacion/' + row.id_ncotizacion + '" class="btn btn-flat btn-light"><i class="fa fa-file-pdf-o icon_font"></i></a>';
					html += '<a title="Editar cotización" href="' + base_url + 'Nueva_cotizacion/add/' + row.id_ncotizacion + '" class="btn btn-flat btn-light"><i class="fa fa-edit icon_font"></i></a>';
					html += '<a title="Eliminar cotización" onclick="eliminarconfig(' + row.id_ncotizacion + ')" class="btn btn-flat btn-danger"><i class="fa fa-trash-o icon_font"></i></a>';
					html += '</div>';
					return html;
				}
			},

		],
		"order": [
			[0, "desc"]
		],
		"lengthMenu": [
			[25, 50, 100],
			[25, 50, 100]
		],
	});
}

function eliminarconfig(id) {
	$.confirm({
		boxWidth: '30%',
		useBootstrap: false,
		icon: 'fa fa-warning',
		title: 'Atención!',
		content: '¿Está seguro de Eliminar la configuración?',
		type: 'red',
		typeAnimated: true,
		buttons: {
			confirmar: function () {
				$.ajax({
					type: 'POST',
					url: base_url + "Nueva_cotizacion/delete",
					data: {
						id: id
					},
					statusCode: {
						404: function (data) {
							toastr.error('404', 'Error');
						},
						500: function (data) {
							//console.log(data);
							toastr.error('Algo salió mal, intente de nuevo o contacte al administrador del sistema', 'Error');
						}
					},
					success: function (response) {
						toastr.success('Eliminado correctamente', 'Hecho!');
						loadtable();
					}
				});
			},
			cancelar: function () {

			}
		}
	});
}
