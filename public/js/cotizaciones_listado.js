var base_url = $('#base_url').val();
var table;
$(document).ready(function($) {
	loadtable();
});
function loadtable(){
	table = $('#table-lis').DataTable({
        "bProcessing": true,
        "serverSide": true,
        "searching": true,
        destroy:true,
        "ajax": {
            "url": base_url+"Cotizaciones/getlist",
            type: "post",
        },
        "columns": [
            {"data": "id"},
            {"data": "nombre"},
            {"data": "direccion"},
            {"data": "telefono",},
            {"data": "email"},
            {"data": "fecha_reg"},
            {"data": null,
                render:function(data,type,row){
                    var html='';
                        html+='<a onclick="detalles('+row.id+')" class="btn btn-flat btn-danger"><i class="fa fa-eye icon_font"></i></a>';
                    return html;
                }
            },
            
        ],
        "order": [[ 0, "desc" ]],
        "lengthMenu": [[25, 50, 100], [25, 50,100]],
    });
}

function detalles(id){
    $("#modal_ver").modal();
    $.ajax({
        type: 'POST',
        traditional: true,
        url : base_url+'Cotizaciones/detallesCotizacion',
        data: {id:id},
        success: function(data){
            $(".table_data_product").html(data);  
        }
    }); 
}