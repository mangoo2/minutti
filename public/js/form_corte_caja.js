$(document).ready(function(){
	$('#chkFecha').change(function(){
		if(document.getElementById("chkFecha").checked){
			f = new Date();
			if(f.getDate()<10){
				dia = "0"+f.getDate();
			}else{dia = f.getDate();
			}
			if(f.getMonth()<10){
				mes = "0"+(f.getMonth()+1);
			}else{
				mes = (f.getMonth+1);
			}
			fecha = f.getFullYear()+"-"+mes+"-"+dia;
			$('#txtInicio').val(fecha);
			$('#txtFin').val(fecha);
			$('#txtInicio').attr('disabled','disabled');
			$('#txtFin').attr('disabled','disabled');
			
			
		}
		else{
			$('#txtInicio').val('');
			$('#txtFin').val('');
			$('#txtInicio').removeAttr('disabled');
			$('#txtFin').removeAttr('disabled');
		}
	});
	$('#btnBuscar').click(function(){
		var pro_ven=0;
		if($('#checkproducto').is(':checked')){
			pro_ven=1;
        }else{
            pro_ven=0;
        }
		params = {};
		params.fecha1 = $('#txtInicio').val();
		params.fecha2 = $('#txtFin').val();
		params.idsucursal = $('#idsucursal option:selected').val();
		params.tipo_venta = $('#tipo_venta option:selected').val();
		params.pro_ven = pro_ven;
		if(params.fecha1 != '' && params.fecha2 != ''){
            var tipo = $('#tipo_venta option:selected').val();
            if(tipo!=4){
                $.ajax({
					type:'POST',
					url:'Corte_caja/corte',
					data:params,
					async:false,
					success:function(data){
						console.log(data);
						var array = $.parseJSON(data);
						$('#tbCorte').html(array.tabla);
						$('#tbCorte2').html(array.tabla2);
						$('#rowventas').html(array.totalventas);
						$('#totalutilidades').html(array.totalutilidad);
						$('#dgastos').html(''+array.dgastos);
						$('#dTotal').html(''+array.dTotal);
	                    $('#dSubtotal').html(''+array.dSubtotal);
	                    if($('#checkproducto').is(':checked')){ 
	                        $('.mas_vendindos').html(array.mas_vendindos);
	                    }else{
	                    	$('.mas_vendindos').html('');
	                    }   
					}
				});
				$.ajax({
					type:'POST',
					url:'Corte_caja/corte_gastos',
					data:params,
					async:false,
					success:function(data){
						console.log(data);
						var array = $.parseJSON(data);
						$('#tbgastos').html(array.tabla);
					}
				});
            }else{
                $.ajax({
					type:'POST',
					url:'Corte_caja/corte_credito',
					data:params,
					async:false,
					success:function(data){
						console.log(data);
						var array = $.parseJSON(data);
						$('#tbCorte').html(array.tabla);
						$('#tbCorte2').html(array.tabla2);
						$('#rowventas').html(array.totalventas);
						$('#totalutilidades').html(array.totalutilidad);
						$('#dTotal').html(''+array.dTotal);
	                    $('#dSubtotal').html(''+array.dSubtotal);
	                    if($('#checkproducto').is(':checked')){ 
	                        $('.mas_vendindos').html(array.mas_vendindos);
	                    }else{
	                    	$('.mas_vendindos').html('');
	                    }   
					}
				});
            }
				

		}
		else{
			toastr.error('No existen fechas validas', 'Error');
			
		}
	});
});