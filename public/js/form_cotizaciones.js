var base_url = $('#base_url').val();
var idCotizacion = $('#IDcotizaciones').val();
var cant_row_producto = 0;
var id_cont_producto = 0;

var cant_row_servicio = 0;
var id_cont_servicio = 0;
var can_actualizar_total = false;

$(document).ready(function ($) {
	$('#clienteId').select2({
		width: '100%',
		minimumInputLength: 3,
		minimumResultsForSearch: 10,
		placeholder: 'Buscar un cliente',
		ajax: {
			url: base_url + 'General/search_cliente',
			dataType: "json",
			data: function (params) {
				var query = {
					search: params.term,
					type: 'public'
				}
				return query;
			},
			processResults: function (data) {
				var itemscli = [];
				data.forEach(function (element) {
					itemscli.push({
						id: element.clienteId,
						text: element.nombre,
						correo: element.correo,
						telefono: element.celular
					});
				});
				return {
					results: itemscli
				};
			},
		}
	})
	.on('select2:select', function (e) {
		var data = e.params.data;
		//console.log(data);
		$('#correo').val(data.correo);
		$('#telefono').val(data.telefono);

	});

});


$(document).ready(function () {
	get_clientes();

	get_tabla_productos(idCotizacion);
	btn_check_productos();
	get_tabla_servicios(idCotizacion);
	btn_check_servicios();

	btn_check_descuentos_p();
	btn_check_descuentos_e()
});


function get_clientes() {
	let id = $("#Idcliente").val();
	$.ajax({
		type: 'POST',
		url: base_url + "Cotizacion/get_data_clientes",
		data: {
			id: id
		},
		success: function (response) {
			var array = $.parseJSON(response);
			array.forEach(function (element) {

				var newOption = new Option(element.nombre, element.clienteId, false, false);

				$('#clienteId').append(newOption).trigger('change');
				setTimeout(function () {
					$('#clienteId').val(element.clienteId).trigger('change.select2');
					
				}, 1000);
			});
		},
		error: function (response) {
			console.log('Error');
		}
	});
}

function save() {
	var form_register = $('#form_cotizaciones');
	var error_register = $('.alert-danger', form_register);
    var success_register = $('.alert-success', form_register);
	form_register.validate({
		errorElement: 'div', //default input error message container
		errorClass: 'vd_red', // default input error message class
		focusInvalid: false, // do not focus the last invalid input
		ignore: "",
		rules: {
			correo:{
				email: true
			},
			telefono:{
				minlength: 10,
				maxlength: 10,
            },
			clienteId:{
				required: true
			}
		},
		errorPlacement: function(error, element) {
			if (element.parent().hasClass("vd_checkbox") || element.parent().hasClass("vd_radio")){
				element.parent().append(error);
			} else if (element.parent().hasClass("vd_input-wrapper")){
				error.insertAfter(element.parent());
			}else {
				error.insertAfter(element);
			}
		}, 
		
		invalidHandler: function (event, validator) { //display error alert on form submit              
			success_register.fadeOut(500);
			error_register.fadeIn(500);
			scrollTo(form_register,-100);
		},

		highlight: function (element) { // hightlight error inputs
			$(element).addClass('vd_bd-red');
			$(element).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');
		},
		
		unhighlight: function (element) { // revert the change dony by hightlight
			$(element)
			.closest('.control-group').removeClass('error'); // set error class to the control group
		},

		success: function (label, element) {
			label
			.addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
			.closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
			$(element).removeClass('vd_bd-red');
		}
	});
	
	var $valid = $("#form_cotizaciones").valid();
	if($valid) {
		var val1=0;
		var val2=0;
		if($('#check_productos').is(':checked')){
            val1=1;
        }

        if($('#check_servicios').is(':checked')){
            val2=1;
        }
        if(val1==1 || val2==1){
				var cantP = $("#tabla_productos_datos").children().length;
				var cantS = $("#tabla_servicios_datos").children().length;
				var datos = $('#form_cotizaciones').serialize() + '&cantP=' + cantP + '&cantS=' + cantS;
				//console.log("datos: " + datos);
				
				$.ajax({
					type: 'POST',
					url: base_url + "Cotizacion/insert",
					data: datos,
					beforeSend: function () {
						$(".button_save").attr("disabled", true);
					},
					success: function (data) {
						var arrayct=$.parseJSON(data);
						var idcot=parseFloat(arrayct.idCotizaciones);
						alert(idcot);
						//guardar_clausulas(idcot);
						swal("Éxito!", "Guardado correctamente", "success");
		                setTimeout(function(){ 
		                    window.location.href = base_url+"index.php/Cotizacion"; 
		                }, 2000);
					
					},
					error: function (data) {
						swal("Error!", "Algo salió mal, intente de nuevo o contacte al administrador del sistema", "error");
					}
				});
		}else{
			swal("¡Atención!", "Falta seleccionar un prodcuto o servicio", "error");
		}

	}
}



function calcularTotalProducto(index) {
	let rowTotalP = 0;
	let indexN = 0;

	if(typeof index === 'string'){
		indexN = index.replace(/[^0-9]+/g, "");
	}else{
		indexN = index;
	}

	rowTotalP = parseFloat($("#pUnitarioP" + indexN).val() * $("#cantidadP" + indexN).val());
	$("#totalP" + (indexN)).val(rowTotalP.toFixed(2));

	calcularSubtotal();
}


function calcularSubtotal(){
	can_actualizar_total = true;
	let totalP = 0;
	let totalS = 0;

	$('.totales_prod').each(function(index){
    //console.log(index + " : " + $(this).val());
		totalP +=  parseFloat($(this).val());
		//console.log(index +": "+parseFloat($(this).val()));
		//console.log(totalP);
	});

	//console.log("TOTAL:" + totalP);
	$("#total_producto").val(totalP.toFixed(2));



	$('.totales_serv').each(function(index){
    //console.log(index + " : " + $(this).val());
		totalS +=  parseFloat($(this).val());
	});

	//console.log("TOTAL:" + totalP);
	$("#total_servicio").val(totalS.toFixed(2));

	calcularTotal();
}


function calcularTotalServicio(index) {
	let rowTotalS = 0;
	let indexN = 0;

	if(typeof index === 'string'){
		indexN = index.replace(/[^0-9]+/g, "");
	}else{
		indexN = index;
	}

	rowTotalS = parseFloat($("#pUnitarioS" + indexN).val() * $("#cantidadS" + indexN).val());
	$("#totalS" + (indexN)).val(rowTotalS.toFixed(2));

	calcularSubtotal();
}

function calcularTotal() {
	let totalP = $("#total_producto").val();
	let totalS = $("#total_servicio").val();
	let iva = 0.16;
	let ivaInverso = 1.16;
	let descuento = 0;
	let checkPorcentaje = false;
	let checkEfectivo = false;

	if ($("#total").val() > 0 && !can_actualizar_total) {
		return;
	}

	if (totalP === undefined || totalP == "" || $("#check_productos").is(':checked') === false) {
		totalP = 0;
		$("#totalP").val(0);
	}

	if (totalS === undefined || totalS == "" || $("#check_servicios").is(':checked') === false) {
		totalS = 0;
		$("#totalS").val(0);
	}

	
	if ($('#check_descuento_p').is(':checked')) {
		checkPorcentaje = true;
		descuento = $('#descuento_p').val();
		descuento = 1 - (descuento / 100);
	}else{
		checkPorcentaje = false;
		$('#descuento_p').val(0);
	}

	if ($('#check_descuento_e').is(':checked')) {
		checkEfectivo = true;
		descuento = $('#descuento_e').val();
	}else{
		checkEfectivo = false;
		$('#descuento_e').val(0);
	}


	let subtotal = parseFloat((parseFloat(totalS) + parseFloat(totalP)).toFixed(2));
	$("#subtotal").val(subtotal);

	let total = subtotal;


	if(checkEfectivo){
		total = parseFloat((parseFloat(subtotal) - parseFloat(descuento)).toFixed(2));
		$("#total").val(total);

	}else if(checkPorcentaje){
		total = parseFloat((parseFloat(subtotal) * parseFloat(descuento)).toFixed(2));
		$("#total").val(total);

	}else{
		$("#total").val(subtotal);
	}
	
	ivaInverso = parseFloat((total / ivaInverso).toFixed(2));
	iva = parseFloat((parseFloat(ivaInverso) * 0.16).toFixed(2));
	$("#iva").val(iva);

	var resta=subtotal-iva;
	$("#subtotal").val(resta);

	//console.log("subtotal: " +subtotal);
	//console.log("iva Inverso: " +ivaInverso);
	//console.log("iva: " +iva);
	//console.log("total: " +total);
	can_actualizar_total = false;
}

/*
function calcularTotal() {

	let totalP = $("#totalP").val();
	let totalS = $("#totalS").val();
	let iva = 0.16;

	if ($("#total").val() > 0 && !can_actualizar_total) {
		return;
	}

	if (totalP === undefined || totalP == "" || $("#check_productos").is(':checked') === false) {
		totalP = 0;
		$("#totalP").val(0);
	}

	if (totalS === undefined || totalS == "" || $("#check_servicios").is(':checked') === false) {
		totalS = 0;
		$("#totalS").val(0);
	}


	let subtotal = parseFloat((parseFloat(totalS) + parseFloat(totalP)).toFixed(2));
	$("#subtotal").val(subtotal);

	iva = parseFloat((subtotal * iva).toFixed(2));
	$("#iva").val(iva);

	let total = parseFloat((iva + subtotal).toFixed(2));
	$("#total").val(total);
}
*/

function btn_check_descuentos_e(click = false){
	if ($('#check_descuento_e').is(':checked')) {
		$('.input_check_descuento_e').css('display', 'block');

		$('#check_descuento_p').prop('checked', false);
		$('.input_check_descuento_p').css('display', 'none');
	} else {
		$('.input_check_descuento_e').css('display', 'none');
	}
	
	can_actualizar_total = click;
	calcularTotal();
}


function btn_check_descuentos_p(click = false){
	
	if ($('#check_descuento_p').is(':checked')) {
		$('.input_check_descuento_p').css('display', 'block');

		$('#check_descuento_e').prop('checked', false);
		$('.input_check_descuento_e').css('display', 'none');
	} else {
		$('.input_check_descuento_p').css('display', 'none');
	}

	can_actualizar_total = click;
	calcularTotal();
}



//------PRODUCTOS------------

function btn_check_productos(click = false) {
	if ($('#check_productos').is(':checked')) {
		$('.form_check_productos').css('display', 'block');
	} else {
		$('.form_check_productos').css('display', 'none');
	}
	can_actualizar_total = click;
	calcularTotal();
}

function get_tabla_productos(id) {
	$.ajax({
		type: 'POST',
		url: base_url + "Cotizacion/get_data_productos",
		data: {
			id: id
		},
		success: function (response) {;
			var array = $.parseJSON(response);
			if (array.length > 0) {
				let count = 0;
				array.forEach(function (element) {
					//$total_aux = count == 0 ? $('#total_producto').val() : 0; 
					row_con_productos(element.idProductos, element.codigo, element.producto, element.descripcion, element.p_unitario, element.cantidad, element.total);
					
					select2_codigo_producto(count);
					get_select2_codigo_producto(count, element.codigo);

					select2_nombre_producto(count);
					get_select2_nombre_producto(count, element.codigo);
					count++;
				});
			} else {
				row_sin_productos();
			}

		},
		error: function (response) {
			toastr.error('Error', 'Algo salió mal, intente de nuevo o contacte al administrador del sistema');
		}
	});
}

function row_sin_productos() {
	row_con_productos(0, '', '', '', '', '1','0.00');
	select2_codigo_producto(cant_row_producto - 1);
	//console.log(cant_row_producto - 1);
	select2_nombre_producto(cant_row_producto - 1);
	//console.log(cant_row_producto - 1);
}

function row_con_productos(id, codigoP, productoP, descripcionP, pUnitarioP, cantidadP, totalP) {
	var btn = '';
	if (cant_row_producto == 0) {
		btn = '<button type="button" class="btn btn-raised btn-dark round btn-min-width mr-1 mb-1 btn-sm btn_' + cant_row_producto + '" onclick="row_sin_productos(' + id + ',' + cant_row_producto + ')"><i class="fa fa-plus"></i></button>'
	} else {
		btn = '<button type="button" class="btn btn-raised btn-danger round btn-min-width mr-1 mb-1 btn-sm" onclick="remover_producto(' + id + ',' + cant_row_producto + ')"><i class="fa fa-trash"></i></button>';
	}
	var html = '<tr class="rowP_' + cant_row_producto + '">\
              <td>\
                <input type="hidden" class="form-control" name="idP' + cant_row_producto + '" id="idP' + cant_row_producto + '" value="' + id + '">\
                <!---<input type="text" class="form-control" id="codigoP' + cant_row_producto + '" name="codigoP' + cant_row_producto + '" value="' + codigoP + '">-->\
								<select class="form-control" name="codigoP' + cant_row_producto + '" id="codigoP' + cant_row_producto + '">\
									<option value="0" selected="" disabled="">Seleccionar una opción</option>\
								</select>\
              </td>\
              <td>\
                <!---<input type="text" class="form-control" id="productoP' + cant_row_producto + '" name="productoP' + cant_row_producto + '" value="' + productoP + '">-->\
								<select class="form-control" name="productoP' + cant_row_producto + '" id="productoP' + cant_row_producto + '">\
									<option value="0" selected="" disabled="">Seleccionar una opción</option>\
								</select>\
              </td>\
              <td>\
                <input type="text" class="form-control" id="descripcionP' + cant_row_producto + '" name="descripcionP' + cant_row_producto + '" value="' + descripcionP + '">\
              </td>\
              <td>\
                <input type="number" class="form-control" id="pUnitarioP' + cant_row_producto + '" name="pUnitarioP' + cant_row_producto + '" value="' + pUnitarioP + '" onchange="calcularTotalProducto(this.id);">\
              </td>\
							<td>\
                <input type="number" class="form-control" id="cantidadP' + cant_row_producto + '" name="cantidadP' + cant_row_producto + '" value="' + cantidadP + '" onchange="calcularTotalProducto(this.id);">\
              </td>\
              <td>\
                <input type="number" class="form-control totales_prod" name="totalP' + cant_row_producto + '" id="totalP' + cant_row_producto + '" value="'+totalP+'" readonly>\
              </td>\
              <td>\
                ' + btn + '\
              </td>\
            </tr>';
	$('#tabla_productos').append(html);
	cant_row_producto++;
}

function remover_producto(id, cant_row_producto) {
	if (id == 0) {
		$('.rowP_' + cant_row_producto).remove();
		calcularSubtotal();
	} else {
		idProductos_aux = id;
		id_cont_producto = cant_row_producto;
		delete_producto(id);
	}
}


//------SERVICIOS------------

function btn_check_servicios(click = false) {
	if ($('#check_servicios').is(':checked')) {
		$('.form_check_servicios').css('display', 'block');
	} else {
		$('.form_check_servicios').css('display', 'none');
	}
	can_actualizar_total = click;
	calcularTotal();
}

function get_tabla_servicios(id) {
	$.ajax({
		type: 'POST',
		url: base_url + "Cotizacion/get_data_servicios",
		data: {
			id: id
		},
		success: function (response) {
			var array = $.parseJSON(response);
			if (array.length > 0) {
				let count = 0;
				array.forEach(function (element) {
					//$total_aux = count == 0 ? $('#total_servicio').val() : 0;
					row_con_servicios(element.idServicios, element.codigo, element.servicio, element.descripcion, element.p_unitario, element.cantidad, element.total)

					select2_codigo_servicio(count);
					get_select2_codigo_servicio(count, element.codigo);

					select2_nombre_servicio(count);
					get_select2_nombre_servicio(count, element.codigo);

					count++;
				});
			} else {
				row_sin_servicios();
			}

		},
		error: function (response) {
			toastr.error('Error', 'Algo salió mal, intente de nuevo o contacte al administrador del sistema');
		}
	});
}

function row_sin_servicios() {
	row_con_servicios(0, '', '', '', '', '1','0.00');

	select2_codigo_servicio(cant_row_servicio - 1);
	//console.log(cant_row_servicio - 1);
	select2_nombre_servicio(cant_row_servicio - 1);
	//console.log(cant_row_servicio - 1);
}

function row_con_servicios(id, codigoS, servicioS, descripcionS, pUnitarioS, cantidadS, totalS) {
	var btn = '';
	if (cant_row_servicio == 0) {
		btn = '<button type="button" class="btn btn-raised btn-dark round btn-min-width mr-1 mb-1 btn-sm btn_' + cant_row_servicio + '" onclick="row_sin_servicios(' + id + ',' + cant_row_servicio + ')"><i class="fa fa-plus"></i></button>'
	} else {
		btn = '<button type="button" class="btn btn-raised btn-danger round btn-min-width mr-1 mb-1 btn-sm" onclick="remover_servicio(' + id + ',' + cant_row_servicio + ')"><i class="fa fa-trash"></i></button>';
	}
	var html = '<tr class="rowS_' + cant_row_servicio + '">\
              <td>\
                <input type="hidden" class="form-control" name="idS' + cant_row_servicio + '" id="idS' + cant_row_servicio + '" value="' + id + '">\
                <!---<input type="text" class="form-control" id="codigoS' + cant_row_servicio + '" name="codigoS' + cant_row_servicio + '" value="' + codigoS + '">-->\
								<select class="form-control" name="codigoS' + cant_row_servicio + '" id="codigoS' + cant_row_servicio + '">\
									<option value="0" selected="" disabled="">Seleccionar una opción</option>\
								</select>\
              </td>\
              <td>\
                <!---<input type="text" class="form-control" id="servicioS' + cant_row_servicio + '" name="servicioS' + cant_row_servicio + '" value="' + servicioS + '">-->\
								<select class="form-control" name="servicioS' + cant_row_servicio + '" id="servicioS' + cant_row_servicio + '">\
									<option value="0" selected="" disabled="">Seleccionar una opción</option>\
								</select>\
              </td>\
              <td>\
                <input type="text" class="form-control" id="descripcionS' + cant_row_servicio + '" name="descripcionS' + cant_row_servicio + '" value="' + descripcionS + '">\
              </td>\
              <td>\
                <input type="number" class="form-control" id="pUnitarioS' + cant_row_servicio + '" name="pUnitarioS' + cant_row_servicio + '" value="' + pUnitarioS + '" onchange="calcularTotalServicio(this.id);">\
              </td>\
							<td>\
                <input type="number" class="form-control" id="cantidadS' + cant_row_servicio + '" name="cantidadS' + cant_row_servicio + '" value="' + cantidadS + '" onchange="calcularTotalServicio(this.id);">\
              </td>\
              <td>\
                <input type="number" class="form-control totales_serv" name="totalS' + cant_row_servicio + '" id="totalS' + cant_row_servicio + '" name="totalS' + cant_row_servicio + '" value="' + totalS + '" readonly>\
              </td>\
              <td>\
                ' + btn + '\
              </td>\
            </tr>';
	$('#tabla_servicios').append(html);
	cant_row_servicio++;
}

function remover_servicio(id, cant_row_servicio) {
	if (id == 0) {
		$('.rowS_' + cant_row_servicio).remove();
		calcularSubtotal();
	} else {
		idservicios_aux = id;
		id_cont_servicio = cant_row_servicio;
		delete_servicio(id);
	}
}

//-----SELECT Productos------------------------------------

function select2_codigo_producto(i){
	$('#codigoP'+ i).select2({
		width: '100%',
		minimumInputLength: 3,
		minimumResultsForSearch: 10,
		placeholder: 'Buscar un producto',
		ajax: {
			url: base_url + 'Cotizacion/get_cotizacion_productos_cod',
			dataType: "json",
			data: function (params) {
				var query = {
					search: params.term,
					type: 'public'
				}
				return query;
			},
			processResults: function (data) {
				var itemsCodProd = [];
				data.forEach(function (element) {
					itemsCodProd.push({
						id: element.productoId,
						text: element.codigo,
						producto: element.nombre,
						descripcion: element.descripcion,
						punitario: element.precio_venta
					});
				});
				return {
					results: itemsCodProd
				};
			},
		}
	})

	.on('select2:select', function (e) {
		var data = e.params.data;
		//console.log(data);
		//$('#productoP' + i).val(data.producto);
		get_select2_nombre_producto(i, data.id);
		$('#descripcionP' + i).val(data.descripcion);
		$('#pUnitarioP' + i).val(data.punitario);
		calcularTotalProducto(i);
	});
}

function get_select2_codigo_producto(i,cod) {
	$.ajax({
		type: 'POST',
		url: base_url + "Cotizacion/get_data_cotizacion_producto",
		data: {
			id: cod
		},
		success: function (response) {
			var array = $.parseJSON(response);
			array.forEach(function (element) {
	
				var newOption = new Option(element.codigo, element.productoId, false, false);
				
				$('#codigoP' + i).append(newOption).trigger('change');
				setTimeout(function () {
					$('#codigoP' + i).val(element.productoId).trigger('change.select2');
					
				}, 1000);
				
			});
			//calcularTotalProducto(i);
		},
		error: function (response) {
			console.log('Error');
		}
	});
}


function select2_nombre_producto(i){
	$('#productoP'+ i).select2({
		width: '100%',
		minimumInputLength: 3,
		minimumResultsForSearch: 10,
		placeholder: 'Buscar un producto',
		ajax: {
			url: base_url + 'Cotizacion/get_cotizacion_productos_nom',
			dataType: "json",
			data: function (params) {
				var query = {
					search: params.term,
					type: 'public'
				}
				return query;
			},
			processResults: function (data) {
				var itemsNomProd = [];
				data.forEach(function (element) {
					itemsNomProd.push({
						id: element.productoId,
						codigo: element.codigo,
						text: element.nombre,
						descripcion: element.descripcion,
						punitario: element.precio_venta
					});
				});
				return {
					results: itemsNomProd
				};
			},
		}
	})

	.on('select2:select', function (e) {
		var data = e.params.data;
		//console.log(data);
		$('#codigoP' + i).val(data.codigo);
		get_select2_codigo_producto(i, data.id);
		$('#descripcionP' + i).val(data.descripcion);
		$('#pUnitarioP' + i).val(data.punitario);
		calcularTotalProducto(i);
	});
}

function get_select2_nombre_producto(i,cod) {
	$.ajax({
		type: 'POST',
		url: base_url + "Cotizacion/get_data_cotizacion_producto",
		data: {
			id: cod
		},
		success: function (response) {
			var array = $.parseJSON(response);
			array.forEach(function (element) {
	
				var newOption = new Option(element.nombre, element.productoId, false, false);
				
				$('#productoP' + i).append(newOption).trigger('change');
				setTimeout(function () {
					$('#productoP' + i).val(element.productoId).trigger('change.select2');
					
				}, 1000);
			});
			//calcularTotalProducto(i);
		},
		error: function (response) {
			console.log('Error');
		}
	});
}


//-----SELECT Servicios------------------------------------
function select2_codigo_servicio(i){
	$('#codigoS'+ i).select2({
		width: '100%',
		minimumInputLength: 3,
		minimumResultsForSearch: 10,
		placeholder: 'Buscar un Servicio',
		ajax: {
			url: base_url + 'Cotizacion/get_cotizacion_servicios_cod',
			dataType: "json",
			data: function (params) {
				var query = {
					search: params.term,
					type: 'public'
				}
				return query;
			},
			processResults: function (data) {
				var itemsCodServ = [];
				data.forEach(function (element) {
					itemsCodServ.push({
						id: element.id,
						text: element.codigo,
						servicio: element.servico,
						descripcion: element.descripcion,
						costoServicio: element.costo_servicio
					});
				});
				return {
					results: itemsCodServ
				};
			},
		}
	})

	.on('select2:select', function (e) {
		var data = e.params.data;
		//console.log(data);
		//$('#productoP' + i).val(data.producto);
		get_select2_nombre_servicio(i, data.id);
		$('#descripcionS' + i).val(data.descripcion);
		$('#pUnitarioS' + i).val(data.costoServicio);
		calcularTotalServicio(i);
	});
}

function get_select2_codigo_servicio(i,cod) {
	$.ajax({
		type: 'POST',
		url: base_url + "Cotizacion/get_data_cotizacion_servicio",
		data: {
			id: cod
		},
		success: function (response) {
			var array = $.parseJSON(response);
			array.forEach(function (element) {
	
				var newOption = new Option(element.codigo, element.id, false, false);
				
				$('#codigoS' + i).append(newOption).trigger('change');
				setTimeout(function () {
					$('#codigoS' + i).val(element.id).trigger('change.select2');
					
				}, 1000);
			});
			//calcularTotalServicio(i);
		},
		error: function (response) {
			console.log('Error');
		}
	});
}


function select2_nombre_servicio(i){
	$('#servicioS'+ i).select2({
		width: '100%',
		minimumInputLength: 3,
		minimumResultsForSearch: 10,
		placeholder: 'Buscar un servicio',
		ajax: {
			url: base_url + 'Cotizacion/get_cotizacion_servicios_nom',
			dataType: "json",
			data: function (params) {
				var query = {
					search: params.term,
					type: 'public'
				}
				return query;
			},
			processResults: function (data) {
				var itemsNomServ = [];
				data.forEach(function (element) {
					itemsNomServ.push({
						id: element.id,
						codigo: element.codigo,
						text: element.servico,
						descripcion: element.descripcion,
						costoServicio: element.costo_servicio
					});
				});
				return {
					results: itemsNomServ
				};
			},
		}
	})

	.on('select2:select', function (e) {
		var data = e.params.data;
		//console.log(data);
		//$('#codigoP' + i).val(data.codigo);
		get_select2_codigo_servicio(i, data.id);
		$('#descripcionS' + i).val(data.descripcion);
		$('#pUnitarioS' + i).val(data.costoServicio);
		calcularTotalServicio(i);
	});
}

function get_select2_nombre_servicio(i,cod) {
	$.ajax({
		type: 'POST',
		url: base_url + "Cotizacion/get_data_cotizacion_servicio",
		data: {
			id: cod
		},
		success: function (response) {
			var array = $.parseJSON(response);
			array.forEach(function (element) {
	
				var newOption = new Option(element.servico, element.id, false, false);
				
				$('#servicioS' + i).append(newOption).trigger('change');
				setTimeout(function () {
					$('#servicioS' + i).val(element.id).trigger('change.select2');
					
				}, 1000);
			});
			//calcularTotalServicio(i);
		},
		error: function (response) {
			console.log('Error');
		}
	});
}


function delete_producto(id){
	swal({
		title: "¿Desea eliminar el producto seleccionado?",
		text: "Se eliminará del listado",
		type: "warning",
		showCancelButton: true,
		confirmButtonColor: "#DD6B55",
		confirmButtonText: "Eliminar",
		cancelButtonText: "Cancelar",
	}).then(function (isConfirm) {
		if (isConfirm) {
			$.ajax({
				type:'POST',
				url: base_url + "Cotizacion/delete_registro_producto",
				data: {id:id},
				statusCode: {
					404: function(data) {
						swal("Error!", "Error 404", "error");
					},
					500: function(data) {
						//console.log(data);
						swal("Error!", "Algo salió mal, intente de nuevo o contacte al administrador del sistema", "error");
					}
				},
				success:function(response){  
					swal("Éxito!", "Eliminado correctamente", "success");
					$('.rowP_'+id_cont_producto).remove();
					calcularSubtotal();
				}
			});
		}
	});
}

function delete_servicio(id){
	swal({
		title: "¿Desea eliminar el servicio seleccionado?",
		text: "Se eliminará del listado",
		type: "warning",
		showCancelButton: true,
		confirmButtonColor: "#DD6B55",
		confirmButtonText: "Eliminar",
		cancelButtonText: "Cancelar",
	}).then(function (isConfirm) {
		if (isConfirm) {
			$.ajax({
				type:'POST',
				url: base_url + "Cotizacion/delete_registro_servicio",
				data: {id:id},
				statusCode: {
					404: function(data) {
						swal("Error!", "Error 404", "error");
					},
					500: function(data) {
						//console.log(data);
						swal("Error!", "Algo salió mal, intente de nuevo o contacte al administrador del sistema", "error");
					}
				},
				success:function(response){  
					swal("Éxito!", "Eliminado correctamente", "success");
					$('.rowS_'+id_cont_servicio).remove();
					calcularSubtotal();
				}
			});
		}
	});
}


function add_clausulas(){
	var concepto = $('#concepto_x').val();
	if(concepto!=''){
        tabla_clausulas(0,concepto);
        $('#concepto_x').val('');
	}else{
        swal("¡Atención!", "Falta agregar clausula", "error");
	}
	
}

var contcl=0;
function tabla_clausulas(id,concepto){
	var html='<tr class="row_cl_'+contcl+'"><td><input type="hidden" id="id_cl" value="'+id+'"><input type="text" class="form-control" id="concepto_cl" value="'+concepto+'" style="width: 100%"></td>\
	<td><button type="button" class="btn btn-raised btn-danger round btn-min-width mr-1 mb-1 btn-sm" onclick="delete_clausulas('+id+','+contcl+')"><i class="fa fa-trash"></i></button></td></tr>';
	$('#tbody_clausulas').append(html);
	contcl++;
}

function delete_clausulas(id,cont){
	if(id==0){
        $('.row_cl_'+cont).remove();
	}else{
		swal({
			title: "¿Desea eliminar el producto seleccionado?",
			text: "Se eliminará del listado",
			type: "warning",
			showCancelButton: true,
			confirmButtonColor: "#DD6B55",
			confirmButtonText: "Eliminar",
			cancelButtonText: "Cancelar",
		}).then(function (isConfirm) {
			if (isConfirm) {
				$.ajax({
					type:'POST',
					url: base_url + "Cotizacion/delete_registro_producto",
					data: {id:id},
					statusCode: {
						404: function(data) {
							swal("Error!", "Error 404", "error");
						},
						500: function(data) {
							//console.log(data);
							swal("Error!", "Algo salió mal, intente de nuevo o contacte al administrador del sistema", "error");
						}
					},
					success:function(response){  
						$('.row_cl_'+cont).remove();
					}
				});
			}
		});
	}
}


var contx=0;
function guardar_clausulas(id){
    var DATA  = [];
    var TABLA   = $("#tabla_clausulas tbody > tr");
    TABLA.each(function(){ 
        contx=1;
        item = {};
        item ["idCotizaciones"] = id;
        item ["id"] = $(this).find("input[id*='id_cl']").val();
        item ["clausulas"] = $(this).find("input[id*='concepto_cl']").val();
        DATA.push(item);
    });     
    if(contx==1){
        INFO  = new FormData();
        aInfo   = JSON.stringify(DATA);
        INFO.append('data', aInfo);
        $.ajax({
            data: INFO, 
            type: 'POST',
            url : base_url + 'Cotizacion/registro_clausulas',
            processData: false, 
            contentType: false,
            async: false,
            statusCode:{
                404: function(data){
                    toastr.error('Error!', 'No Se encuentra el archivo');
                },
                500: function(){
                    toastr.error('Error', '500');
                }
            },
            success: function(data){
            }
        });  
    }
}