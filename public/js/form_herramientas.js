var base_url = $('#base_url').val();

function save() {

	var datos = $('#form-herramientas').serialize();
	//console.log("datos: " +datos);

	$.ajax({
		type: 'POST',
		url: base_url + "Herramientas/insert",
		data: datos,
		beforeSend: function () {
			$(".button_save").attr("disabled", true);
		},
		success: function (data) {
			//console.log(data);
			swal("Éxito!", "Guardado correctamente", "success");
			setTimeout(function () {
				window.location.href = base_url + "Herramientas";
			}, 2000);
		},
		error: function (data) {
			swal("Error!", "Algo salió mal, intente de nuevo o contacte al administrador del sistema", "error");
		}
	});

}
