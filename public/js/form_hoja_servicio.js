var base_url = $('#base_url').val();
var idfile_carro = $('#idfile_carro').val();
var file_firma1 = $('#file_firma1').val();
var file_firma2 = $('#file_firma2').val();
$(document).ready(function() {
    if(idfile_carro==''){
        var canvas = document.getElementById('patientSignature');
        var ctx  = canvas.getContext("2d");
        var img = new Image();
        img.src = base_url+"public/img/cark.png";
        img.onload = function(){
            ctx.drawImage(img, 0, 0);
        }
    } 
    if(file_firma1==''){   
        createSignaturepfc1('patientSignaturepfc1');
    }
    if(file_firma2==''){    
        createSignaturecfc1('patientsignatureccfc1');
    }

    $('.cargarcarro').click(function(event) {
        var canvas = document.getElementById('patientSignature');
        var ctx  = canvas.getContext("2d");

        img.src = base_url+"public/img/cark.png";
        img.onload = function(){
            ctx.drawImage(img, 0, 0);
        }
    });
    
});
function guarda_registro(){
    var form_register = $('#form_data');
    var error_register = $('.alert-danger', form_register);
    var success_register = $('.alert-success', form_register);
    var $validator1=form_register.validate({
        errorElement: 'div', //default input error message container
        errorClass: 'vd_red', // default input error message class
        focusInvalid: false, // do not focus the last invalid input
        ignore: "",
        rules: {
            /*           
            fecha_servicio:{
              required: true
            },
            hora_servicio:{
              required: true
            },
            id_cliente:{
              required: true
            },
            unidad_servicio:{
              required: true
            },*/
        },
        errorPlacement: function(error, element) {
            if (element.parent().hasClass("vd_checkbox") || element.parent().hasClass("vd_radio")){
                element.parent().append(error);
            } else if (element.parent().hasClass("vd_input-wrapper")){
                error.insertAfter(element.parent());
            }else {
                error.insertAfter(element);
            }
        }, 
        
        invalidHandler: function (event, validator) { //display error alert on form submit              
                success_register.fadeOut(500);
                error_register.fadeIn(500);
                scrollTo(form_register,-100);

        },

        highlight: function (element) { // hightlight error inputs
    
            $(element).addClass('vd_bd-red');
            $(element).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');

        },

        unhighlight: function (element) { // revert the change dony by hightlight
            $(element)
                .closest('.control-group').removeClass('error'); // set error class to the control group
        },

        success: function (label, element) {
            label
                .addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
                .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
            $(element).removeClass('vd_bd-red');
        }
    });
    //////////Registro///////////
    var $valid = $("#form_data").valid();
    if($valid) {
        $('.btn_registro').attr('disabled',true);
        var datos = form_register.serialize()+'&estatus=1';
        $.ajax({
            type:'POST',
            url: base_url+'Servicio/update_registro_hojaservicio',
            data: datos,
            statusCode:{
              404: function(data){
                  swal("Error!", "Error 404", "error");
              },
              500: function(){
                  swal("Error!", "Algo salió mal, intente de nuevo o contacte al administrador del sistema", "error");
              }
            },
            success:function(data){
                var id = data;
                imagen_carro(id);
                imagen_firma1(id);
                imagen_firma2(id);
                swal("Éxito!", "Guardado correctamente", "success");
                setTimeout(function(){ 
                    window.location = base_url+'Servicio';
                }, 1500);
            }
        });
    }   
}


function update_registro(){
    var form_register = $('#form_data');
    var datos = form_register.serialize();
    $.ajax({
        type:'POST',
        url: base_url+'Servicio/update_registro_hojaservicio',
        data: datos,
        statusCode:{
          404: function(data){
              swal("Error!", "Error 404", "error");
          },
          500: function(){
              swal("Error!", "Algo salió mal, intente de nuevo o contacte al administrador del sistema", "error");
          }
        },
        success:function(data){
        }
    });
}

function update_registro_cliente(){
    $.ajax({
        type:'POST',
        url: base_url+'Servicio/update_registro_cliente',
        data: {
            id_cliente:$('#id_cliente').val(),
            nombre:$('#nombre').val(),
            rfc:$('#rfc').val(),
            direccion:$('#direccion').val(),
            estado:$('#estado').val(),
            codigo_postal:$('#codigo_postal').val(),
            celular:$('#celular').val(),
            correo:$('#correo').val(),
        },
        statusCode:{
          404: function(data){
              swal("Error!", "Error 404", "error");
          },
          500: function(){
              swal("Error!", "Algo salió mal, intente de nuevo o contacte al administrador del sistema", "error");
          }
        },
        success:function(data){
        }
    });
    $('#id_cliente2').val($('#nombre').val());
}



function imagen_carro(id) {
    var canvas = $("#patientSignature")[0];
    if (!isCanvasBlank(canvas)) {
        var canvas = document.getElementById('patientSignature');
        var dataURL = canvas.toDataURL();
        $.ajax({
            type:'POST',
            url: base_url+'Servicio/add_carro',
            data: {id:id,carro:dataURL},
            statusCode:{
                404: function(data){
                    $.toast({
                        heading: 'Error!',
                        text: '404',
                        position: 'top-right',
                        loaderBg:'#ff6849',
                        icon: 'error',
                        hideAfter: 3500
                    });
                },
                500: function(){
                    $.toast({
                        heading: 'Error!',
                        text: '500',
                        position: 'top-right',
                        loaderBg:'#ff6849',
                        icon: 'error',
                        hideAfter: 3500
                    });
                }
            },
            success:function(data){

            }
        });
    }
}

function editar_servicio_carro(){
    $('.div_carro').html('<div class="row">\
        <div class="col-md-12" id="aceptance">\
            <div id="signature" class="signature">\
            <label for="patientSignature" class="sighiddeable hidden-xs hidden-sm marginTop text-md "></label>\
                <canvas id="patientSignature" class="sighiddeable hidden-xs hidden-sm" width="975" height="627" style="width: 975px; height: 627px; border: 2px dashed rgb(29, 175, 147); cursor: crosshair;"></canvas>\
                <img src="'+base_url+'public/img/icon_borrar.png" class="sighiddeable hidden-xs hidden-sm clearSignature cargarcarro" data-signature="patientSignature">\
            </div>\
        </div>\
    </div>');
    createSignature('patientSignature');
    
    var canvas = document.getElementById('patientSignature');
    var ctx  = canvas.getContext("2d");
    var img = new Image();
    img.src = base_url+"public/img/cark.png";
    img.onload = function(){
        ctx.drawImage(img, 0, 0);
    }
    $('.cargarcarro').click(function(event) {
        var canvas = document.getElementById('patientSignature');
        var ctx  = canvas.getContext("2d");

        img.src = base_url+"public/img/cark.png";
        img.onload = function(){
            ctx.drawImage(img, 0, 0);
        }
    });
}

function imagen_firma1(id) {
    var canvas = $("#patientSignaturepfc1")[0];
    if (!isCanvasBlank(canvas)) {
        var canvas = document.getElementById('patientSignaturepfc1');
        var dataURL = canvas.toDataURL();
        $.ajax({
            type:'POST',
            url: base_url+'Servicio/add_firma1',
            data: {id:id,firma:dataURL},
            statusCode:{
                404: function(data){
                    $.toast({
                        heading: 'Error!',
                        text: '404',
                        position: 'top-right',
                        loaderBg:'#ff6849',
                        icon: 'error',
                        hideAfter: 3500
                    });
                },
                500: function(){
                    $.toast({
                        heading: 'Error!',
                        text: '500',
                        position: 'top-right',
                        loaderBg:'#ff6849',
                        icon: 'error',
                        hideAfter: 3500
                    });
                }
            },
            success:function(data){

            }
        });
    }
}

function firma_editar1(){
    $('.canvas_firmafepc1').html('<div id="signaturepfc1" class="signaturepfc1 col-lg-12 col-md-12  col-sm-12 col-xs-12" style="text-align:center;margin-bottom:10px;border-radius:4px;">\
          <label for="patientSignaturepfc1" class="sighiddeable hidden-xs hidden-sm marginTop text-md arial_text"></label>\
          <canvas id="patientSignaturepfc1" class="sighiddeable hidden-xs hidden-sm" width="300" height="180" style="width: 300px; height: 180px; border: 2px dashed rgb(29, 175, 147); cursor: crosshair;"></canvas>\
          <img src="'+base_url+'public/img/icon_borrar.png" class="sighiddeable hidden-xs hidden-sm clearSignaturepfc1" data-signaturepfc1="patientSignaturepfc1">\
    </div>');
    createSignaturepfc1('patientSignaturepfc1');    
    
}

function firma_editar2(){
    $('.canvas_firmafepc2').html('<div id="signaturecfc1" class="signaturecfc1 col-lg-12 col-md-12  col-sm-12 col-xs-12" style="text-align:center;margin-bottom:10px;border-radius:4px;">\
          <label for="patientsignatureccfc1" class="sighiddeable hidden-xs hidden-sm marginTop text-md arial_text"></label>\
          <canvas id="patientsignatureccfc1" class="sighiddeable hidden-xs hidden-sm" width="300" height="180" style="width: 300px; height: 180px; border: 2px dashed rgb(29, 175, 147); cursor: crosshair;"></canvas>\
          <img src="'+base_url+'public/img/icon_borrar.png" class="sighiddeable hidden-xs hidden-sm clearsignaturecfc1" data-signaturecfc1="patientsignatureccfc1">\
    </div>');
    createSignaturecfc1('patientsignatureccfc1');
}

function imagen_firma1(id) {
    var canvas = $("#patientSignaturepfc1")[0];
    if (!isCanvasBlank(canvas)) {
        var canvas = document.getElementById('patientSignaturepfc1');
        var dataURL = canvas.toDataURL();
        $.ajax({
            type:'POST',
            url: base_url+'Servicio/add_firma1',
            data: {id:id,firma:dataURL},
            statusCode:{
                404: function(data){
                    $.toast({
                        heading: 'Error!',
                        text: '404',
                        position: 'top-right',
                        loaderBg:'#ff6849',
                        icon: 'error',
                        hideAfter: 3500
                    });
                },
                500: function(){
                    $.toast({
                        heading: 'Error!',
                        text: '500',
                        position: 'top-right',
                        loaderBg:'#ff6849',
                        icon: 'error',
                        hideAfter: 3500
                    });
                }
            },
            success:function(data){

            }
        });
    }
}

function imagen_firma2(id) {
    var canvas = $("#patientsignatureccfc1")[0];
    if (!isCanvasBlank(canvas)) {
        var canvas = document.getElementById('patientsignatureccfc1');
        var dataURL = canvas.toDataURL();
        $.ajax({
            type:'POST',
            url: base_url+'Servicio/add_firma2',
            data: {id:id,firma:dataURL},
            statusCode:{
                404: function(data){
                    $.toast({
                        heading: 'Error!',
                        text: '404',
                        position: 'top-right',
                        loaderBg:'#ff6849',
                        icon: 'error',
                        hideAfter: 3500
                    });
                },
                500: function(){
                    $.toast({
                        heading: 'Error!',
                        text: '500',
                        position: 'top-right',
                        loaderBg:'#ff6849',
                        icon: 'error',
                        hideAfter: 3500
                    });
                }
            },
            success:function(data){

            }
        });
    }
}

var aux_carro=0;
function btn_mostrar_carro(){
    if(aux_carro==0){
        aux_carro=1;
        $('.formulario').css('display','none');
        $('.mostrar_carro').css('display','block');
        $('.title_carro').html('Ocultar carro');
        $('.mostrar_firmas_btn1').css('display','none');
        $('.mostrar_firmas_btn2').css('display','none');
    }else{
        aux_carro=0;
        $('.mostrar_firmas_btn1').css('display','block');
        $('.mostrar_firmas_btn2').css('display','block');
        $('.formulario').css('display','block');
        $('.mostrar_carro').css('display','none');
        $('.title_carro').html('Mostrar carro');
    }    
}

var aux_firma=0;
function btn_mostrar_firma1(){
    if(aux_firma==0){
        aux_firma=1;
        
        $('.mostrar_carro_btn').css('display','none');
        $('.formulario').css('display','none');
        $('.text_firma_1').css('display','block');
        $('.title_firma').html('Ocultar firma');
        $('.mostrar_firmas_btn2').css('display','none');
    }else{
        aux_firma=0;
        $('.mostrar_carro_btn').css('display','block');
        $('.mostrar_firmas_btn2').css('display','block');
        $('.formulario').css('display','block');
        $('.text_firma_1').css('display','none');
        $('.title_firma').html('Mostrar firma');
    }    
}

var aux_firma=0;
function btn_mostrar_firma2(){
    if(aux_firma==0){
        aux_firma=1;
        
        $('.mostrar_carro_btn').css('display','none');
        $('.formulario').css('display','none');
        $('.text_firma_2').css('display','block');
        $('.title_firma').html('Ocultar firma');
        $('.mostrar_firmas_btn1').css('display','none');
    }else{
        aux_firma=0;
        $('.mostrar_carro_btn').css('display','block');
        $('.mostrar_firmas_btn1').css('display','block');
        $('.formulario').css('display','block');
        $('.text_firma_2').css('display','none');
        $('.title_firma').html('Mostrar firma');
    }    
}

