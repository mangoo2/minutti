var base_url=$('#base_url').val();
var idpro=$('#clienteId').val();
var cant=0;
var idban_aux=0;
var idcont=0;
$(document).ready(function() {
    //btn_datos_fiscales();
    //btn_datos_bancarios();
    //get_tabla_bancarios(idpro);

    $('.clockpicker').clockpicker().find('input').change(function() {
        console.log(this.value);
    });
});
function guarda_registro(){
    var form_register = $('#form_data');
    var error_register = $('.alert-danger', form_register);
    var success_register = $('.alert-success', form_register);
    var $validator1=form_register.validate({
        errorElement: 'div', //default input error message container
        errorClass: 'vd_red', // default input error message class
        focusInvalid: false, // do not focus the last invalid input
        ignore: "",
        rules: {
            servico:{
                required: true
            },
            descripcion:{
                required: true
            },
            costo_servicio:{
                required: true
            },
        },
        
        errorPlacement: function(error, element) {
            if (element.parent().hasClass("vd_checkbox") || element.parent().hasClass("vd_radio")){
                element.parent().append(error);
            } else if (element.parent().hasClass("vd_input-wrapper")){
                error.insertAfter(element.parent());
            }else {
                error.insertAfter(element);
            }
        }, 
        
        invalidHandler: function (event, validator) { //display error alert on form submit              
                success_register.fadeOut(500);
                error_register.fadeIn(500);
                scrollTo(form_register,-100);

        },

        highlight: function (element) { // hightlight error inputs
    
            $(element).addClass('vd_bd-red');
            $(element).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');

        },

        unhighlight: function (element) { // revert the change dony by hightlight
            $(element)
                .closest('.control-group').removeClass('error'); // set error class to the control group
        },

        success: function (label, element) {
            label
                .addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
                .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
            $(element).removeClass('vd_bd-red');
        }
    });
    //////////Registro///////////
    var $valid = $("#form_data").valid();
    if($valid) {
        $('.btn_registro').attr('disabled',true);
        var datos = form_register.serialize();
        $.ajax({
            type:'POST',
            url: base_url+'Servicios/registro',
            data: datos,
            statusCode:{
                404: function(data){
                    swal("Error!", "Error 404", "error");
                },
                500: function(){
                    swal("Error!", "Algo salió mal, intente de nuevo o contacte al administrador del sistema", "error");
                }
            },
            success:function(data){                
                swal("Éxito!", "Guardado correctamente", "success");
                setTimeout(function(){ 
                    window.location = base_url+'Servicios';
                }, 1500);
        
            }
        });
    }   
}
