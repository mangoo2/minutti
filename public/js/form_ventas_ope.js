var base_url = $('#base_url').val();
var proselected_length=0;
var proselected=0;
var cliente_aux=0;
$(document).ready(function () {
    //=====================================================================================================
    $('#vcliente').select2({
        width: 'resolve',
        minimumInputLength: 3,
        minimumResultsForSearch: 10,
        placeholder: 'Buscar un cliente',
        ajax: {
            url: 'Ventas/searchcli',
            dataType: "json",
            data: function (params) {
            var query = {
                search: params.term,
                type: 'public'
            }
            return query;
        },
        processResults: function(data){
            var itemscli = [];
            data.forEach(function(element) {
                itemscli.push({
                    id: element.clienteId,
                    text: element.nombre,
                });
            });
            return {
                results: itemscli
            };          
        },  
      }
    });
    //=====================================================================================================
    //$("#productos").focus();
    $("#cantidad_pro").focus();
    $( "#productos" ).keypress(function(e) {
        if (e.which==13) {
            productos_buscar(1);
            setTimeout(function(){ 
                $('#productos').val('');
                $('.producto_buscar_t').html('');
            }, 500);
        }else{
            setTimeout(function(){ 
                productos_buscar(0);
            }, 900);
            
        }
    });
    $(document).keydown(function(e) {
        //console.log(e.which)
        switch(e.which) { 
            case 38:
                navigate('up');
            break;
            case 40:
                navigate('down');
            break;
            case 13:
                navigate('enter');
            break;             
        }
    });

    //=====================================================================================================
    // Guardar venta
    $('#ingresaventa').click(function(event) {
            addventas();
    });

    $( "#vingreso" ).keypress(function(e) {
        if(e.which == 13) {
            addventas();
        }
    });

    /*$('#ingresaventa_temporal').click(function(event) {
            addventas_temporal();
    });*/

    //temporal_get();
    limpiar();
    
    document.onkeyup = function(e) {
      if (e.altKey && e.which == 86){
        addventas();
        toastr.success('Creado venta','Hecho!');
        setTimeout (function(){ 
            $('#productos').val('');
        }, 1500);
      } else if (e.altKey && e.which == 84) {
        //addventas_temporal();
        setTimeout (function(){ 
            $('#productos').val('');
            $('.producto_buscar_t').html('');
        }, 1500);
      } else if (e.altKey && e.which == 66) {
        var pro = $('#borrar_producto').val();
        deletepro(pro);
      }
    };
});


/// Productos
function productos_buscar(tipo){
    var productos_b=$('#productos').val();
    if(productos_b!=''){
        $.ajax({
            type:'POST',
            url: base_url+'Ventas/buscar_producto',
            data:{
                producto:productos_b
                },
            statusCode:{
                404: function(data){
                    toastr.error('Error!', 'No Se encuentra el archivo');
                },
                500: function(){
                    toastr.error('Error', '500');
                }
            },
            success:function(data){ 
                    console.log(data); 
                    var array = $.parseJSON(data);
                    if (tipo==0) {
                        $('.producto_buscar_t').html('');
                        array.forEach(function(item) {
                            var html='<a data-nombre="'+item.nombre+'" \
                                        data-codigo="'+item.codigo+'" \
                                        data-precioventa="'+item.precio_venta+'" \
                                        data-tipo="'+item.tipo+'" \
                                        onclick="addproducto('+item.productoId+','+item.tipo+')" \
                                        class="btn btn-raised btn-dark btn-min-width mr-1 mb-1 btn-lg pro_'+item.productoId+' proselected" style="width: 100%; color:white">'+item.codigo+' / '+item.nombre+'</a><br>';
                            $('.producto_buscar_t').append(html);
                        });
                        proselected_length=$('.proselected').length;
                        proselected=-1;

                    }else{
                        if (array.length==1) {
                            array.forEach(function(item) {
                                addproducto(item.productoid,item.tipo);
                            });
                        }else{

                        }
                    }
            }
        });
    }else{
        setTimeout(function(){ 
            $('.producto_buscar_t').html('');
        }, 1000);
    }
}

function addproducto(pro,tipo){
    var cantidad_pro=$('#cantidad_pro').val();
    if(cantidad_pro>=0.1){
        $.ajax({
            type:'POST',
            url: base_url+'Ventas/addproducto_tabla',
            data: {prod: pro,cant:cantidad_pro,tipo:tipo},
            async: false,
            statusCode:{
                404: function(data){
                    toastr.error('Error!', 'No Se encuentra el archivo');
                },
                500: function(){
                    toastr.error('Error', '500');
                }
            },
            success:function(data){
                console.log(data);
                $('#class_productos').html(data);
            }
        });
        setTimeout(function(){ 
        $('#productos').val('');
        $('.producto_buscar_t').html('');
        }, 300);
        //$("#productos").focus();
        $("#cantidad_pro").focus();

        //$("#vproducto").val(0).change();
        //$('#vproducto').select2('open').on('focus');
        calculartotal();
        $('#cantidad_pro').val(1)
    }else{
        toastr.error('No as agregado una cantidad', 'Atención');
    }
}

function calculartotal(){
    var addtp = 0;
    $(".vstotal").each(function() {
        var vstotal = $(this).val();
        addtp += Number(vstotal);
    });
    var total=parseFloat(addtp);
    
    var descuento = $('#descuento').val();
    //var desc=parseFloat(descuento);
    //var entre=parseFloat(desc/100); 
    
    var ttl= parseFloat(descuento);//parseFloat(total*entre);
    var resta =parseFloat(total-ttl);
    $('#total_p_total').val(Number(resta).toFixed(2));
    $('#total_p_total2').val(Number(total).toFixed(2));
    $('#cantdescuento').val(Number(ttl).toFixed(2));
    $('#total_p_total_texto').val('$'+Number(resta).toFixed(2));

    ingreso();
}

function ingreso(){
    var ingresoe= $('#vingreso').val();
    var ingreso=parseFloat(ingresoe);
    var totals=$('#total_p_total').val();


    var cambio = parseFloat(ingreso)-parseFloat(totals);
    if (cambio<0) {
        cambio=0;
    }
    $('#vcambio').val(cambio.toFixed(2));
}

function deletepro(id){
    $.ajax({
        type:'POST',
        url: base_url+'Ventas/deleteproducto',
        data: {
            idd: id
            },
            async: false,
            statusCode:{
                404: function(data){
                    toastr.error('Error!', 'No Se encuentra el archivo');
                },
                500: function(){
                    toastr.error('Error', '500');
                }
            },
            success:function(data){
                //($('.producto_'+id).remove();
                $('#class_productos').html('');
                $.ajax({
                    type:'POST',
                    url: base_url+'Ventas/addproducto_tabla',
                    data: {prod: 0},
                    async: false,
                    statusCode:{
                        404: function(data){
                            toastr.error('Error!', 'No Se encuentra el archivo');
                        },
                        500: function(){
                            toastr.error('Error', '500');
                        }
                    },
                    success:function(data){
                        console.log(data);
                        $('#class_productos').html(data);
                    }
                });
            }
        });
}

function addventas(){
    if ($('#vcambio').val()>=0) {

                //var vingresot =$('#vingreso').val()==''?0:$('#vingreso').val();
                var vtotal = $('#total_p_total').val();
                //var efectivo = parseFloat(vtotal)-parseFloat(vingresot);
                var factura=0;
                var checkfact = document.getElementById("factura").checked;
                if (checkfact==true) {   
                    factura=1;      
                }else{
                    factura=0;
                }
                $('#ingresaventa').attr('disabled',true);
                setTimeout (function(){ 
                    $('#ingresaventa').attr('disabled',false);
                }, 2000);
                $.ajax({
                    type:'POST',
                    url: base_url+'Ventas/ingresarventa',
                    data: {
                        cli: $('#vcliente').val(),
                        mpago: $('#mpago option:selected').val(),
                        desc: $('#descuento').val(),
                        descu: $('#cantdescuento').val(),
                        sbtotal:$('#total_p_total2').val(),
                        total: vtotal,
                        efectivo: $('#vingreso').val(),
                        cambio:$('#vcambio').val(),
                        factura:factura,
                    },
                    async: false,
                    statusCode:{
                        404: function(data){
                            toastr.error('Error!', 'No Se encuentra el archivo');
                        },
                        500: function(){
                            toastr.error('Error', '500');
                        }
                    },
                    success:function(data){
                        $("#factura").prop('checked', false); 
                        var idventa=data;
                        var DATA = [];
                        var TABLA = $("#productosv tbody > tr");
                            TABLA.each(function(){         
                                item = {};
                                item ["idventa"] = idventa;
                                item ["producto"]   = $(this).find("input[id*='vsproid']").val();
                                item ["cantidad"]  = $(this).find("input[id*='vscanti']").val();
                                item ["precio"]  = $(this).find("input[id*='vsprecio']").val();
                                item ["tipo"]  = $(this).find("input[id*='vstipo']").val();
                                DATA.push(item);
                            });
                            INFO  = new FormData();
                            aInfo   = JSON.stringify(DATA);
                            INFO.append('data', aInfo);
                            $.ajax({
                                data: INFO,
                                type: 'POST',
                                url : base_url+'Ventas/ingresarventapro',
                                processData: false, 
                                contentType: false,
                                async: false,
                                statusCode:{
                                    404: function(data){
                                        toastr.error('Error!', 'No Se encuentra el archivo');
                                    },
                                    500: function(){
                                        toastr.error('Error', '500');
                                    }
                                },
                                success: function(data){
                                }
                            });
                            checkprint = document.getElementById("checkimprimir").checked;
                            if (checkprint==true) {
                                //$("#iframeri").modal();
                                //$('#iframereporte').html('<iframe src="'+base_url+'Ticket?id='+idventa+'"></iframe>');  

                                $('#iframeri').modal(); 
                                $('.iframereporte').html('<iframe src="' + base_url + 'Ticket/ticket_venta/'+idventa+'" class="iframeprintc1" id="iframeprintc1"></iframe>');
                            }else{
                                toastr.success( 'Venta Realizada','Hecho!');
                            }
                            limpiar();
                            //$("#productos").focus();
                            $("#cantidad_pro").focus();
                            if(cliente_aux!=0){
                                $("#vcliente option[value='"+cliente_aux+"']").remove();
                                setTimeout (function(){ 
                                
                                    $("#vcliente option[value="+1+"]").attr("selected",true);
                                    $('#vcliente').val(1).trigger('change.select2');
                                }, 1000);
                            }
                            //cambiar_estatus_temporal();
                            //temporal_get();
                            update_metodo();
                    }
                });
            }else{
                toastr.error('No se puede realizar la venta debido a que no ha ingresado el saldo para liquidar la venta','Error!');
            }
}


/*function addventas_temporal(){
    if ($('#vcambio').val()>=0) {

        //var vingresot =$('#vingreso').val()==''?0:$('#vingreso').val();
        var vtotal = $('#total_p_total').val();
        //var efectivo = parseFloat(vtotal)-parseFloat(vingresot);
        $.ajax({
            type:'POST',
            url: base_url+'Ventas/ingresarventa_temporal',
            data: {
                //uss: $('#ssessius').val(),
                cli: $('#vcliente').val(),
                mpago: $('#mpago option:selected').val(),
                desc: $('#descuento').val(),
                descu: $('#cantdescuento').val(),
                sbtotal:$('#total_p_total2').val(),
                total: vtotal,
                efectivo: $('#vingreso').val(),
                cambio:$('#vcambio').val()
            },
            async: false,
            statusCode:{
                404: function(data){
                    toastr.error('Error!', 'No Se encuentra el archivo');
                },
                500: function(){
                    toastr.error('Error', '500');
                }
            },
            success:function(data){
                var idventa=data;
                var DATA = [];
                var TABLA = $("#productosv tbody > tr");
                TABLA.each(function(){         
                    item = {};
                    item ["idventa"] = idventa;
                    item ["producto"]   = $(this).find("input[id*='vsproid']").val();
                    item ["cantidad"]  = $(this).find("input[id*='vscanti']").val();
                    item ["precio"]  = $(this).find("input[id*='vsprecio']").val();
                    DATA.push(item);
                });
                INFO  = new FormData();
                aInfo   = JSON.stringify(DATA);
                INFO.append('data', aInfo);
                $.ajax({
                    data: INFO,
                    type: 'POST',
                    url : base_url+'Ventas/ingresarventapro_temporal',
                    processData: false, 
                    contentType: false,
                    async: false,
                    statusCode:{
                        404: function(data){
                            toastr.error('Error!', 'No Se encuentra el archivo');
                        },
                        500: function(){
                            toastr.error('Error', '500');
                        }
                    },
                    success: function(data){
                    }
                });
                toastr.success( 'Venta Temporal','Hecho!');
                limpiar();
                //$("#productos").focus();
                $("#cantidad_pro").focus();
                if(cliente_aux!=0){
                    $("#vcliente option[value='"+cliente_aux+"']").remove();
                    setTimeout (function(){ 
                    
                        $("#vcliente option[value="+1+"]").attr("selected",true);
                        $('#vcliente').val(1).trigger('change.select2');
                    }, 1000);
                }
                cambiar_estatus_temporal();
                temporal_get();
                update_metodo();
            }
        });
    }else{
        toastr.error('No se puede realizar la venta debido a que no ha ingresado el saldo para liquidar la venta','Error!');
    }
}*/


function navigate(direction) {
    if(proselected_length>0){
        if(direction == 'up') {//subir
            $(".proselected").removeClass('selected');
            if (proselected==0 ||proselected==-1) {
                proselected=0;
            }else{
                proselected--;
            }
            
        
        } else if (direction == 'down') {//bajar
            $(".proselected").removeClass('selected');
            proselected++;
            if(proselected>=proselected_length){
                proselected=(proselected_length-1);
            }
        }else if (direction == 'enter') {
            $(".proselected.selected").click();
        }
    }
    
    //console.log(proselected);
    var valor=$(".proselected").eq(proselected).addClass('selected');
    //console.log(valor);
}

function campo_vacio(){
    var productos = $('#productos').val();
    if(productos==''){
        $('.producto_buscar_t').html('');   
    }
}

function limpiar(){
    $('#class_productos').html('');
    $.ajax({
            type:'POST',
            url: base_url+'Ventas/productoclear',
            async: false,
            statusCode:{
                404: function(data){
                    toastr.error('Error!', 'No Se encuentra el archivo');
                },
                500: function(){
                    toastr.error('Error', '500');
                }
            },
            success:function(data){
                    
            }
    });
    $("#vcliente").val(1).change();
    $('#total_p_total').val(0);
    $('#total_p_total2').val(0);
    $('#descuento').val(0);
    $('#cantdescuento').val(0);
    $('#vingreso').val(0);
    $('#vcambio').val(0);
    $('#total_p_total_texto').val('$'+Number(0).toFixed(2));
    $('.cancelar_temporal').html('');
}

/*function temporal_get(){
    $.ajax({
            type:'POST',
            url: base_url+'Ventas/temporal_get_sucursal',
            async: false,
            statusCode:{
                404: function(data){
                    toastr.error('Error!', 'No Se encuentra el archivo');
                },
                500: function(){
                    toastr.error('Error', '500');
                }
            },
            success:function(data){
                $('.temporal_texto').html(data);    
            }
    });
}*/

/*function ventas_temporal_get(){
    toastr.success( 'Cargando venta por favor espere','Hecho!');
    limpiar();
    //$("#productos").focus();
    $("#cantidad_pro").focus();
    var id = $('#idtemporal option:selected').val();
    setTimeout (function(){ 
        $.ajax({
                type:'POST',
                url: base_url+'Ventas/get_venta_temporal',
                data: {id:id},
                async: false,
                statusCode:{
                    404: function(data){
                        toastr.error('Error!', 'No Se encuentra el archivo');
                    },
                    500: function(){
                        toastr.error('Error', '500');
                    }
                },
                success:function(data){
                    var array = $.parseJSON(data);
                    var venta = array.venta;
                    var venta_detalle = array.venta_detalle;
                    //console.log("Cliente "+array.id_cliente+' / '+array.cliente);
                    if(cliente_aux!=0){
                        $("#vcliente option[value='"+cliente_aux+"']").remove();
                    }
                    if(array.id_cliente!=1){
                        $('#vcliente').prepend("<option value='"+array.id_cliente+"' selected>"+array.cliente+"</option>");
                        $('#vcliente').val(array.id_cliente).trigger('change.select2');
                        cliente_aux=array.id_cliente;
                    }
                    venta.forEach( function(e) {
                        console.log("Venta "+e.id_venta);
                        $('#total_p_total').val(e.monto_total);
                        $('#total_p_total2').val(e.subtotal);
                        $('#descuento').val(e.descuento);
                        $('#cantdescuento').val(e.descuentocant);
                        $('#vingreso').val(e.efectivo);
                        $('#vcambio').val(e.cambio);
                        $("#mpago option[value="+ e.metodo +"]").attr("selected",true);
                        $('#total_p_total_texto').val('$'+Number(e.monto_total).toFixed(2));
                    });
                    venta_detalle.forEach( function(e){
                        var cantidad = e.cantidad;
                        var i;
                        for (i=0;i<cantidad; i++){
                        addproducto(e.id_producto);
                        }
                        
                    });
                    if(id>0){
                        $('.cancelar_temporal').html('<a class="btn btn-round btn_amarillo white" onclick="cancelar_venta('+id+')">Cancelar venta temporal</a>  ');
                    }else{
                        $('.cancelar_temporal').html('');
                    }
                }
        });
    }, 1000);  
}*/

function cancelar_venta(id){
    $.ajax({
        type:'POST',
        url: base_url+'Ventas/cambio_activo_temporal',
        data: {id:id},
        async: false,
        statusCode:{
            404: function(data){
                toastr.error('Error!', 'No Se encuentra el archivo');
            },
            500: function(){
                toastr.error('Error', '500');
            }
        },
        success:function(data){
            toastr.success('Venta temporal cancelada', '¡Hecho!');
            //temporal_get();
            limpiar();
            //$("#productos").focus();
            $("#cantidad_pro").focus();
        }
    });

}


/*function cambiar_estatus_temporal(){
    var idtp=$('#idtemporal_aux').val();
    if(idtp==1){
        var id=$('#idtemporal option:selected').val();
        $.ajax({
            type:'POST',
            url: base_url+'Ventas/cambio_activo_temporal',
            data: {id:id},
            async: false,
            statusCode:{
                404: function(data){
                    toastr.error('Error!', 'No Se encuentra el archivo');
                },
                500: function(){
                    toastr.error('Error', '500');
                }
            },
            success:function(data){
            }
        });
    }
}*/

function calcular_cantidad_total(id){
    var idproducto = $('.precio_'+id).data('idproducto');
    var precioventa = $('.precio_'+id).data('precioventa');
    var cantidad = $('.vscanti_'+id).val();
    /*if(cantidad>=1){
    $.ajax({
        type:'POST',
        url: base_url+'Ventas/actulizar_producto_tabla',
        data: {prod:idproducto,cant:cantidad},
        async: false,
        statusCode:{
            404: function(data){
                toastr.error('Error!', 'No Se encuentra el archivo');
            },
            500: function(){
                toastr.error('Error', '500');
            }
        },
        success:function(data){
        }
    });    
    var precioventa = $('.precio_'+id).data('precioventa');
    var especial1_cantidad = $('.precio_'+id).data('especial1_cantidad');
    var especial1_precio = $('.precio_'+id).data('especial1_precio');
    var especial2_cantidad = $('.precio_'+id).data('especial2_cantidad');
    var especial2_precio = $('.precio_'+id).data('especial2_precio');
    var mayoreo = $('.precio_'+id).data('mayoreo');
    var canmayoreo = $('.precio_'+id).data('canmayoreo');
    var canmediomayoreo = $('.precio_'+id).data('canmediomayoreo');
    var mediomayoreo = $('.precio_'+id).data('mediomayoreo');
    var precio = 0;
    if (cantidad<=especial1_cantidad) {
        precio=especial1_precio;
    }else if(cantidad > especial1_cantidad && cantidad<=especial2_cantidad) {
        precio=especial2_precio;
    }else if(cantidad>=canmayoreo) {
        precio=mayoreo;
    }else if(cantidad>=canmediomayoreo && cantidad<canmayoreo) {
        precio=mediomayoreo;
    }else{
        precio=precioventa;
    }*/

    //$('.precio_'+id).val(precioventa);
    var cantotal=cantidad*precioventa;
    $('.vstotal_'+id).val(Number(cantotal).toFixed(2));
    calculartotal();

}

function tipo_pago(){
    var metodo = $('#mpago option:selected').val();
    if(metodo==4){
        $('#vingreso').attr('disabled',true);
        $('#vingreso').val(0);
        $('#vcambio').val(0);
    }else{
        $('#vingreso').attr('disabled',false);
    }
}

function update_metodo(){
    var html='<div class="form-group">\
                <select class="form-control" id="mpago" name="mpago" onchange="tipo_pago()">\
                    <option value="1">Efectivo</option>\
                    <option value="2">Tarjeta de crédito</option>\
                    <option value="3">Tarjeta de débito</option>\
                    <option value="4">Crédito</option>\
                </select>\
            </div>';
    $('.texto_tipo_venta').html(html);       
    $('#vingreso').attr('disabled',false); 
}