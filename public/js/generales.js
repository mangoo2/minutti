$(document).ready(function() {
	$("input[name=Ante]").change(function() {
		var val = $( "input[type=radio][name=Ante]:checked" ).val();
		if (val==1) {
			$(".descripantecedentes").css("display","block");
		}else{
			$(".descripantecedentes").css("display","none");
		}
	});
	$("input[name=alergia]").change(function() {
		var val = $( "input[type=radio][name=alergia]:checked" ).val();
		if (val==1) {
			$(".descripcionalergia").css("display","block");
		}else{
			$(".descripcionalergia").css("display","none");
		}
	});
	$("input[name=Med]").change(function() {
		var val = $( "input[type=radio][name=Med]:checked" ).val();
		if (val==1) {
			$(".descripcionmedicamento").css("display","block");
		}else{
			$(".descripcionmedicamento").css("display","none");
		}
	});
	$("input[name=AntOral]").change(function() {
		var val = $( "input[type=radio][name=AntOral]:checked" ).val();
		if (val==1) {
			$(".agregadoanticonceptivos").css("display","block");
		}else{
			$(".agregadoanticonceptivos").css("display","none");
		}
	});
	$("input[name=trh]").change(function() {
		var val = $( "input[type=radio][name=trh]:checked" ).val();
		if (val==1) {
			$(".terapiahormonal").css("display","block");
		}else{
			$(".terapiahormonal").css("display","none");
		}
	});
	$("input[name=analisis]").change(function() {
		var val = $( "input[type=radio][name=analisis]:checked" ).val();
		if (val==1) {
			$(".analisissoli").css("display","block");
		}else{
			$(".analisissoli").css("display","none");
		}
	});
	$("input[name=Ejercicior]").change(function() {
		var val = $( "input[type=radio][name=Ejercicior]:checked" ).val();
		if (val==1) {
			$(".siejercicio").css("display","block");
		}else{
			$(".siejercicio").css("display","none");
		}
	});
	$("input[name=Alcohol]").change(function() {
		var val = $( "input[type=radio][name=Alcohol]:checked" ).val();
		if (val==1) {
			$(".confirmalcohol").css("display","block");
		}else{
			$(".confirmalcohol").css("display","none");
		}
	});
	$("input[name=Tabaco]").change(function() {
		var val = $( "input[type=radio][name=Tabaco]:checked" ).val();
		if (val==1) {
			$(".confirmtabaco").css("display","block");
		}else{
			$(".confirmtabaco").css("display","none");
		}
	});
	$("input[name=Cafe]").change(function() {
		var val = $( "input[type=radio][name=Cafe]:checked" ).val();
		if (val==1) {
			$(".confirmcafe").css("display","block");
		}else{
			$(".confirmcafe").css("display","none");
		}
	});
	$("#saveform").click(function(event) {
		var params = {};
			params.generalId = $('#generalId').val();
			params.clienteId = $('#clienteId').val();
			params.motivo = $('#Motivo').val();
			params.peso = $('#Peso').val();
			params.estatura = $('#Estura').val();
			params.tratamientoprevio = $( "input[type=radio][name=Ante]:checked" ).val();
			params.tratamientodescrip = $('#Descripcionante').val();
			params.ahdia = $('#Diabetes').is(':checked')==true?1:0;
			params.ahdis = $('#Disilpidemias').is(':checked')==true?1:0;
			params.ahcar = $('#Cardiopatias').is(':checked')==true?1:0;
			params.ahobs = $('#Obesidad').is(':checked')==true?1:0;
			params.ahhgr = $('#HG').is(':checked')==true?1:0;
			params.ahhiperti = $('#HP').is(':checked')==true?1:0;
			params.ahhiperte = $('#Hipertencion').is(':checked')==true?1:0;
			params.ahinr = $('#IR').is(':checked')==true?1:0;
			params.ahca = $('#CA').is(':checked')==true?1:0;
			params.ahepi = $('#Epilepcia').is(':checked')==true?1:0;
			params.ahdes = $('#DH').is(':checked')==true?1:0;
			params.ahhipo = $('#HT').is(':checked')==true?1:0;
			params.ahotr = $('#otro').is(':checked')==true?1:0;
			params.ahotrd = $('#otrod').val();
			params.apdia = $('#Diabetes2').is(':checked')==true?1:0;
			params.apdis = $('#Disilpidemias2').is(':checked')==true?1:0;
			params.apcar = $('#Cardiopatias2').is(':checked')==true?1:0;
			params.apobs = $('#Obesidad2').is(':checked')==true?1:0;
			params.aphgr = $('#HG2').is(':checked')==true?1:0;
			params.aphiperti = $('#HP2').is(':checked')==true?1:0;
			params.aphiperte = $('#Hipertencion2').is(':checked')==true?1:0;
			params.apinr = $('#IR2').is(':checked')==true?1:0;
			params.apca = $('#CA2').is(':checked')==true?1:0;
			params.apepi = $('#Epilepcia2').is(':checked')==true?1:0;
			params.apdes = $('#DH2').is(':checked')==true?1:0;
			params.aphipo = $('#HT2').is(':checked')==true?1:0;
			params.apotr = $('#otro2').is(':checked')==true?1:0;
			params.apotrd = $('#otrod2').val();
			params.alergias = $( "input[type=radio][name=alergia]:checked" ).val();
			params.alergiasd = $('#descripcionalergia').val();
			params.medisuple = $( "input[type=radio][name=Med]:checked" ).val();
			params.medisupled = $('#descripcionmedicamento').val();
			params.embarazo = $( "input[type=radio][name=Emb]:checked" ).val();
			params.emb_sdg = $('#SDG option:selected').val();
			params.emb_fum = $('#FUM').val();
			params.anticonceptivo = $( "input[type=radio][name=AntOral]:checked" ).val();
			params.anticonceptivo_cual = $('#AntOralDes').val();
			params.anticonceptivo_dosis = $('#AntOralDosis').val();
			params.climaterio = $( "input[type=radio][name=AO]:checked" ).val();
			params.climaterio_fecha = $('#AOFc').val();
			params.trhormonal = $( "input[type=radio][name=trh]:checked" ).val();
			params.trhormonal_cual = $('#trhcual').val();
			params.trhormonal_dosis = $('#Dosistrh').val();
			params.datosquimicos = $('#Bioquimica').val();
			params.s_analisis = $( "input[type=radio][name=analisis]:checked" ).val();
			params.s_analisis_cual = $('#analisiscual').val();
			params.actividad_fisica = $( "input[type=radio][name=Actividad]:checked" ).val();
			params.actividad_fisica_fre = $('#ActividadFrecuencia').val();
			params.actividad_fisica_eje = $( "input[type=radio][name=Ejercicior]:checked" ).val();
			params.actividad_fisica_cual = $('#EjercicioA option:selected').val();
			params.actividad_fisica_eje_fre = $('#FrecuenciaE option:selected').val();
			params.actividad_fisica_eje_dura = $('#Duracioneje').val();
			params.actividad_fisica_eje_ini = $('#Inicioeje').val();
			params.alcohol = $( "input[type=radio][name=Alcohol]:checked" ).val();
			params.alcohol_cuanto = $('#CantA').val();
			params.alcohol_fre = $('#FrecuenciaA option:selected').val();
			params.tabaco = $( "input[type=radio][name=Tabaco]:checked" ).val();
			params.tabaco_cuanto = $('#CantT').val();
			params.tabaco_fre = $('#FrecuenciaT option:selected').val();
			params.cafe = $( "input[type=radio][name=Cafe]:checked" ).val();
			params.cafe_cuanto = $('#CantC').val();
			params.cafe_fre = $('#FrecuenciaC option:selected').val();
			$.ajax({
				type:'POST',
				url:'Nutricionadds',
				data:params,
				async:false,
                statusCode: {
                    404: function(data) {
                        toastr.error('Error', '404');
                    },
                    500: function(data) {
                        toastr.error('Error', data);
                    }
                },
				success:function(data){
					//toastr.success( 'Sucursal agregada','Hecho!');
				}
			});			
	});
});
function mostrargenerales(id){
	params = {};
	params.clienteId = id;
	$.ajax({
		type:'POST',
		url:'mostrargenerales',
		data:params,
		async:false,
        statusCode: {
            404: function(data) {
                toastr.error('Error', '404');
            },
            500: function(data) {
                toastr.error('Error', data);
            }
        },
		success:function(data){
			var array = $.parseJSON(data);
			if (array.status==1) {
				 $('#generalId').val(array.generalId);
				 $('#Motivo').val(array.motivo);
				 $('#Peso').val(array.peso);
				 $('#Estura').val(array.estatura);
				 $( "input[type=radio][name=Ante][value='"+array.tratamientoprevio+"']").prop("checked",true).change();
				 $('#Descripcionante').val(array.tratamientodescrip);
				if(array.ahdia==1){ $('#Diabetes').prop('checked', true);  }
				if(array.ahdis==1){ $('#Disilpidemias').prop('checked', true);  }
				if(array.ahcar==1){ $('#Cardiopatias').prop('checked', true);  }
				if(array.ahobs==1){ $('#Obesidad').prop('checked', true);  }
				if(array.ahhgr==1){ $('#HG').prop('checked', true);  }
				if(array.ahhiperti==1){ $('#HP').prop('checked', true);  }
				if(array.ahhiperte==1){ $('#Hipertencion').prop('checked', true);  }
				if(array.ahinr==1){ $('#IR').prop('checked', true);  }
				if(array.ahca==1){ $('#CA').prop('checked', true);  }
				if(array.ahepi==1){ $('#Epilepcia').prop('checked', true);  }
				if(array.ahdes==1){ $('#DH').prop('checked', true);  }
				if(array.ahhipo==1){ $('#HT').prop('checked', true);  }
				if(array.ahotr==1){ $('#otro').prop('checked', true);  }
				$('#otrod').val(array.ahotrd);
				if(array.apdia==1){ $('#Diabetes2').prop('checked', true); }
				if(array.apdis==1){ $('#Disilpidemias2').prop('checked', true); }
				if(array.apcar==1){ $('#Cardiopatias2').prop('checked', true); }
				if(array.apobs==1){ $('#Obesidad2').prop('checked', true); }
				if(array.aphgr==1){ $('#HG2').prop('checked', true); }
				if(array.aphiperti==1){ $('#HP2').prop('checked', true); }
				if(array.aphiperte==1){ $('#Hipertencion2').prop('checked', true); }
				if(array.apinr==1){ $('#IR2').prop('checked', true); }
				if(array.apca==1){ $('#CA2').prop('checked', true); }
				if(array.apepi==1){ $('#Epilepcia2').prop('checked', true); }
				if(array.apdes==1){ $('#DH2').prop('checked', true); }
				if(array.aphipo==1){ $('#HT2').prop('checked', true); }
				if(array.apotr==1){ $('#otro2').prop('checked', true); }
				 $('#otrod2').val(array.apotrd);
				 $( "input[type=radio][name=alergia][value='"+array.alergias+"']").prop("checked",true).change();
				 $('#descripcionalergia').val(array.alergiasd);
				 $( "input[type=radio][name=Med][value='"+array.medisuple+"']" ).prop("checked",true).change();
				 $('#descripcionmedicamento').val(array.medisupled);
				 $( "input[type=radio][name=Emb][value='"+array.embarazo+"']" ).prop("checked",true);
				 $('#SDG option[value="'+array.emb_sdg+'"]').attr("selected", "selected");
				 $('#FUM').val(array.emb_fum);
				 $( "input[type=radio][name=AntOral][value='"+array.anticonceptivo+"']" ).prop("checked",true).change();
				 $('#AntOralDes').val(array.anticonceptivo_cual);
				 $('#AntOralDosis').val(array.anticonceptivo_dosis);
				 $( "input[type=radio][name=AO][value='"+array.climaterio+"']" ).prop("checked",true);
				 $('#AOFc').val(array.climaterio_fecha);
				 $( "input[type=radio][name=trh][value='"+array.trhormonal+"']" ).prop("checked",true).change();
				 $('#trhcual').val(array.trhormonal_cual);
				 $('#Dosistrh').val(array.trhormonal_dosis);
				 $('#Bioquimica').val(array.datosquimicos);
				 $( "input[type=radio][name=analisis][value='"+array.s_analisis+"']" ).prop("checked",true).change();
				 $('#analisiscual').val(array.s_analisis_cual);
				 $( "input[type=radio][name=Actividad][value='"+array.actividad_fisica+"']" ).prop("checked",true);
				 $('#ActividadFrecuencia').val(array.actividad_fisica_fre);
				 $( "input[type=radio][name=Ejercicior][value='"+array.actividad_fisica_eje+"']" ).prop("checked",true).change();
				 $('#EjercicioA option[value="'+array.actividad_fisica_cual+'"]').attr("selected", "selected");
				 $('#FrecuenciaE option[value="'+array.actividad_fisica_eje_fre+'"]').attr("selected", "selected");
				 $('#Duracioneje').val(array.actividad_fisica_eje_dura);
				 $('#Inicioeje').val(array.actividad_fisica_eje_ini);
				 $( "input[type=radio][name=Alcohol][value='"+array.alcohol+"']" ).prop("checked",true).change();
				 $('#CantA').val(array.alcohol_cuanto);
				 $('#FrecuenciaA option[value="'+array.alcohol_fre+'"]').attr("selected", "selected");
				 $( "input[type=radio][name=Tabaco][value='"+array.tabaco+"']" ).prop("checked",true).change();
				 $('#CantT').val(array.tabaco_cuanto);
				 $('#FrecuenciaT option[value="'+array.tabaco_fre+'"]').attr("selected", "selected");
				 $( "input[type=radio][name=Cafe][value='"+array.cafe+"']" ).prop("checked",true).change();
				 $('#CantC').val(array.cafe_cuanto);
				 $('#FrecuenciaC option[value="'+array.cafe_fre+'"]').attr("selected", "selected");
			}
		}
	});
}