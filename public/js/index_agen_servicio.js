var base_url = $('#base_url').val();
var idreg_servicio=0;
$(document).ready(function () {
    calendario();
    $('#cliente_x').select2({
        width: 'resolve',
        minimumInputLength: 3,
        minimumResultsForSearch: 10,
        placeholder: 'Buscar un cliente',
        ajax: {
            url: 'Servicio/searchcli',
            dataType: "json",
            data: function (params) {
            var query = {
                search: params.term,
                type: 'public'
            }
            return query;
        },
        processResults: function(data){
            var itemscli = [];
            data.forEach(function(element) {
                itemscli.push({
                    id: element.clienteId,
                    text: element.nombre,
                    celular: element.celular,
                });
            });
            return {
                results: itemscli
            };          
        },  
      }
    }).on('select2:select', function(e) {
        var data = e.params.data;
        $('#telefono_cliente_x').val(data.celular);
        unidades_cliente_select(data.id);
    });

    row_sin_servicios();
});


function calendario() {
	$('#calendar').fullCalendar({
		defaultView: 'month',
		allDaySlot: false,
		header: {
			left: 'prev,next today',
			center: 'title',
			right: 'month,agendaWeek,agendaDay',
		},
		titleFormat: 'dddd, D MMMM , YYYY',
		events: function (start, end, timezone, callback, resources) {
			$.ajax({
				url: base_url + 'Servicio/agenda_servicios',
                type:'POST',
				dataType: 'json',
				data: {
					start: start.unix(),
					end: end.unix(),
				},
				success: function (doc) {
					var datos = doc;
					var events = [];
					datos.forEach(function (r) {
                        var color_estatus='';
                        if(r.estatus==null){
                            color_estatus='red';
                        }else{
                            color_estatus='grey';
                        }
                        var hfs_aux=r.hsalida2;
                        var hfs=hfs_aux.split(" ");
						events.push({
                        id_venta: r.id_venta,
						id_cotizacion: r.id_cotizacion,
						title: r.serviciott,
                        cliente: r.nombre,
                        telefono: r.celular,
                        servicio: r.servicio,
                        unidad_placas: r.placas,
                        unidad_modelo: r.modelo,
                        unidad_año:r.ano,
                        costo_total : r.monto_total,
						start: r.fecha_servicio + 'T' + r.hora_servicio,
					    end: hfs[0] + 'T' + hfs[1],
						hora: r.hora_servicio,
						fecha: r.fecha_servicio,
                        color: color_estatus,
                        textColor: '#ffffff',
                        check_servicio_cita:r.check_servicio_cita,
                        estatus:r.estatus,
						});
					});
					callback(events);
				}
			});
		},
		eventRender: function (event, element) {
			element.click(function () {
                idreg_servicio=event.id_venta;
				$('#modal_cita').modal();
				$('#fecha_servicio_v').val(event.fecha);
				$('#hora_servicio_v').val(event.hora);
		        $('#nombre_cliente').val(event.cliente);
			    $('#telefono_cliente').val(event.telefono);
		        $('#placas_unidad').val(event.unidad_placas);
		        $('#modelo_unidad').val(event.unidad_modelo);
		        $('#año_unidad').val(event.unidad_año);
				$('#monto_total').val(event.costo_total);
                if(event.check_servicio_cita==1){
                    $("#check_servicio_cita").prop("checked",true);
                }else{
                    $("#check_servicio_cita").prop("checked",false);    
                }
                if(event.estatus==1){
                    $('.estatus_btn').html('<button type="button" style="width: 100%" class="btn btn-success" onclick="href_hoja_servicio()">Hoja de servicio</button>');
                    $('.estatus_btn_pdf').css('display','block');
                    $('.estatus_btn_pdf_txt').html('<button onclick="pdf_hoja_servicio('+event.id_venta+')" title="Generar PDF" class="btn btn-flat btn-light"><i class="fa fa-file-pdf-o icon_font"></i></button>');

                    $('.check_servicio_cita_text').html('');
                    $('.check_servicio_cita_modal').html('');
                    $('.btn_modal_cita').html('<button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>');
                }else{
                    $('.estatus_btn').html('<button type="button" style="width: 100%" class="btn btn-danger" onclick="href_hoja_servicio()">Hoja de servicio</button>');
                    $('.estatus_btn_pdf').css('display','none');
                    $('.estatus_btn_pdf_txt').html('');
                    
                    $('.check_servicio_cita_text').html('Inicializar servicio');
                    $('.check_servicio_cita_modal').html('<div class="switch-button col-md-1">\
                            <input type="checkbox" id="check_servicio_cita" class="switch-button__checkbox" onclick="btn_check_servicio()">\
                            <label for="check_servicio_cita" class="switch-button__label"></label>\
                        </div>');

                    $('.btn_modal_cita').html('<button type="button" class="btn btn-danger" onclick="btn_eliminar()">Eliminar</button>\
                    <button type="button" class="btn btn-danger" onclick="editar_servicio()">Editar</button>\
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>');
                }
                
                get_servicios_modal(event.id_venta);

			});
		}

	});
}

function get_servicios_modal(id){
    $.ajax({
        type:'POST',
        url: base_url+'Servicio/get_servicios',
        data: {id_venta:id},
        statusCode:{
            404: function(data){
                toastr.error('Error!', 'No Se encuentra el archivo');
            },
            500: function(){
                toastr.error('Error', '500');
            }
        },
        success:function(data){
            $('.tabla_servicios_v').html(data);
        }
    });
}

var cita_add=0;
function alta_cita(tipo){
    if(tipo==1){
        $('.titulo_cita').html('Nueva cita');
        $('#idreg').val('');
        $('#fecha_servicio').val('');
        $('#hora_servicio').val('');
        $("#cliente_x").empty();
        $('#telefono_cliente_x').val('');
        $('.text_servicio').html('<select class="form-control" name="unidad_servicio" id="placas_unidad_x" style="width: 100%">\
                                    </select>');
        $('#modelo_unidad_x').val('');
        $('#ano_unidad_X').val('');
        $('#tabla_servicios_datos').html('');
        cant_row_servicio = 0;
        row_sin_servicios();
        $('#preciox').val('');
        $('.preciox_x').html('$0.00');
    }else{
        $('.titulo_cita').html('Editar cita');
    }
	if(cita_add==0){
        cita_add=1;
        $('.cita_servicio').css('display','block');
        $('.cita_servicio_nueva').html('Ocultar');
	}else{
		cita_add=0;
		$('.cita_servicio').css('display','none');
        $('.cita_servicio_nueva').html('Nueva cita');
	}	
}

function unidades_cliente_select(id){
	$.ajax({
        type:'POST',
        url: base_url+'Servicio/unidades_cliente',
        data: {idcliente:id},
        statusCode:{
            404: function(data){
                toastr.error('Error!', 'No Se encuentra el archivo');
            },
            500: function(){
                toastr.error('Error', '500');
            }
        },
        success:function(data){
            $('.text_servicio').html(data);
        }
    });
}

function get_unidad(){
	var modelo=$('#placas_unidad_x option:selected').data('modelo');
	var anio=$('#placas_unidad_x option:selected').data('ano');
    $('#modelo_unidad_x').val(modelo);
    $('#ano_unidad_X').val(anio);
}

function guarda_registro_cita(){
    var form_register = $('#form_registro_cita');
    var error_register = $('.alert-danger', form_register);
    var success_register = $('.alert-success', form_register);
    var $validator1=form_register.validate({
        errorElement: 'div', //default input error message container
        errorClass: 'vd_red', // default input error message class
        focusInvalid: false, // do not focus the last invalid input
        ignore: "",
        rules: {
            fecha_servicio:{
              required: true
            },
            hora_servicio:{
              required: true
            },
            id_cliente:{
              required: true
            },
            unidad_servicio:{
              required: true
            },
        },
        errorPlacement: function(error, element) {
            if (element.parent().hasClass("vd_checkbox") || element.parent().hasClass("vd_radio")){
                element.parent().append(error);
            } else if (element.parent().hasClass("vd_input-wrapper")){
                error.insertAfter(element.parent());
            }else {
                error.insertAfter(element);
            }
        }, 
        
        invalidHandler: function (event, validator) { //display error alert on form submit              
                success_register.fadeOut(500);
                error_register.fadeIn(500);
                scrollTo(form_register,-100);

        },

        highlight: function (element) { // hightlight error inputs
    
            $(element).addClass('vd_bd-red');
            $(element).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');

        },

        unhighlight: function (element) { // revert the change dony by hightlight
            $(element)
                .closest('.control-group').removeClass('error'); // set error class to the control group
        },

        success: function (label, element) {
            label
                .addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
                .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
            $(element).removeClass('vd_bd-red');
        }
    });
    //////////Registro///////////
    var $valid = $("#form_registro_cita").valid();
    if($valid) {
        $('.btn_registro').attr('disabled',true);
        ///
        var DATAconf  = [];
        var TABLAconf   = $("#tabla_servicios tbody > tr");
            TABLAconf.each(function(){         
            item = {};
            item ["idreg"] = $(this).find("input[id*='idregx']").val();
            item ["servicioS"] = $(this).find("select[class*='servicioSx']").val();
            item ["pUnitarioS"] = $(this).find("input[class*='pUnitarioSx']").val();
            item ["cantidadS"] = $(this).find("input[class*='cantidadSx']").val();
            DATAconf.push(item);
        });
        INFO  = new FormData();
        arrayconfiguracion   = JSON.stringify(DATAconf);
        ///
        var datos = form_register.serialize()+'&arrayproductos='+arrayconfiguracion;
        $.ajax({
            type:'POST',
            url: base_url+'Servicio/registro_cita',
            data: datos,
            statusCode:{
              404: function(data){
                  swal("Error!", "Error 404", "error");
              },
              500: function(){
                  swal("Error!", "Algo salió mal, intente de nuevo o contacte al administrador del sistema", "error");
              }
            },
            success:function(data){
                var id = data;
                swal("Éxito!", "Guardado correctamente", "success");
                setTimeout(function(){ 
                    window.location = base_url+'Servicio';
                }, 1500);
            }
        });
    }   
}

function editar_servicio(){
    $.ajax({
        type: 'POST',
        url: base_url + "Servicio/get_cita",
        data: {id: idreg_servicio},
        statusCode: {
                404: function(data) {
                        swal("Error!", "Error 404", "error");
                },
                500: function(data) {
                        //console.log(data);
                        swal("Error!", "Algo salió mal, intente de nuevo o contacte al administrador del sistema", "error");
                }
        },
        success:function(response){  
            $('#idreg').val(idreg_servicio);
            var array=$.parseJSON(response);
            $('#fecha_servicio').val(array.fecha_servicio);
            $('#hora_servicio').val(array.hora_servicio);
            var data = {
                id: array.id_cliente,
                text: array.nombre
            };
            var newOption = new Option(data.text, data.id, false, false);
            $('#cliente_x').append(newOption).trigger('change');
            $('#telefono_cliente_x').val(array.celular);
            unidades_cliente_select(array.id_cliente);
            setTimeout(function(){ 
                $("#placas_unidad_x option[value='"+array.unidad_servicio+"']").attr("selected", true);
            }, 2000);
            $('#modelo_unidad_x').val(array.modelo);
            $('#ano_unidad_X').val(array.ano);
            setTimeout(function(){ 
                $("#mpago option[value='"+array.metodo+"']").attr("selected", true);
            }, 2000);
            get_tabla_servicios(idreg_servicio);
        }
    });

    $('#modal_cita').modal('hide');
    alta_cita(2);
    $('body, html').animate({
        scrollTop: '0px'
    }, 300);


}

var cant_row_servicio = 0;
function row_sin_servicios() {
    row_con_servicios(0,'','','','',1,'0.00','');
    select2_nombre_servicio(cant_row_servicio - 1);
}

function row_con_servicios(id,servicioS,descripcionS,duracion,pUnitarioS,cantidadS,totalS,servicio){
    var btn = '';
    if (cant_row_servicio == 0) {
        btn = '<button type="button" class="btn btn-raised btn-dark round btn-min-width mr-1 mb-1 btn-sm btn_' + cant_row_servicio + '" onclick="row_sin_servicios(' + id + ',' + cant_row_servicio + ')"><i class="fa fa-plus"></i></button>'
    } else {
        btn = '<button type="button" class="btn btn-raised btn-danger round btn-min-width mr-1 mb-1 btn-sm" onclick="remover_servicio(' + id + ',' + cant_row_servicio + ')"><i class="fa fa-trash"></i></button>';
    }
    var html = '<tr class="rowS_'+cant_row_servicio+'">\
              <td>\
                <input type="hidden" class="form-control" id="idregx" value="'+id+'");">\
                <select class="form-control servicioSx" id="servicioS'+cant_row_servicio+'">\
                    <option value="0" selected="" disabled="">Seleccionar una opción</option>\
                </select>\
              </td>\
              <td>\
                <input type="text" readonly class="form-control descripcionSx" id="descripcionS'+cant_row_servicio+'" value="'+descripcionS+'">\
              </td>\
              <td>\
                <input type="text" readonly class="form-control duracion_serviciox" id="duracion_serviciox'+cant_row_servicio+'" value="'+duracion+'">\
              </td>\
              <td>\
                <input type="number" class="form-control pUnitarioSx" id="pUnitarioS'+cant_row_servicio+'" value="'+pUnitarioS+'" onchange="calcularTotalServicio('+cant_row_servicio+');">\
              </td>\
              <td>\
                <input type="number" class="form-control cantidadSx" id="cantidadS'+cant_row_servicio+'" value="'+cantidadS+'" onchange="calcularTotalServicio('+cant_row_servicio+');">\
              </td>\
              <td>\
                <input type="number" class="form-control totalSx" id="totalS'+cant_row_servicio+'" value="'+totalS+'" readonly>\
              </td>\
              <td>\
                '+btn+'\
              </td>\
            </tr>';
    $('#tabla_servicios').append(html);
    if(servicioS!=''){
        $('#servicioS'+cant_row_servicio).val(null).trigger('change');
        var data = {
            id: servicioS,
            text: servicio
        };
        var newOption = new Option(data.text, data.id, false, false);
        $('#servicioS'+cant_row_servicio).append(newOption).trigger('change.select2');
    }
    cant_row_servicio++;
}

function select2_nombre_servicio(i){
    $('#servicioS'+ i).select2({
        width: '100%',
        minimumInputLength: 3,
        minimumResultsForSearch: 10,
        placeholder: 'Buscar un servicio',
        ajax: {
            url: base_url + 'Cotizacion/get_cotizacion_servicios_nom',
            dataType: "json",
            data: function (params) {
                var query = {
                    search: params.term,
                    type: 'public'
                }
                return query;
            },
            processResults: function (data) {
                var itemsNomServ = [];
                data.forEach(function (element) {
                    itemsNomServ.push({
                        id: element.id,
                        codigo: element.codigo,
                        text: element.servico,
                        descripcion: element.descripcion,
                        costoServicio: element.costo_servicio,
                        duracion: element.duracion,
                    });
                });
                return {
                    results: itemsNomServ
                };
            },
        }
    }).on('select2:select', function (e) {
        var data = e.params.data;
        $('#descripcionS'+i).val(data.descripcion);
        $('#duracion_serviciox'+i).val(data.duracion);
        $('#pUnitarioS'+i).val(data.costoServicio);
        var cantidad = $('#cantidadS'+i).val();
        var mult = parseFloat(data.costoServicio)*parseFloat(cantidad);
        $('#totalS'+i).val(mult);
        calculototal();
    });
}

function calcularTotalServicio(i){
    var costoServicio = $('#pUnitarioS'+i).val();
    var cantidad = $('#cantidadS'+i).val();
    var mult = parseFloat(costoServicio)*parseFloat(cantidad);
    $('#totalS'+i).val(mult);
    calculototal();
}

function calculototal(){
    var TABLAconf   = $("#tabla_servicios tbody > tr");
    var suma=0;
    TABLAconf.each(function(){             
        var totalSx= $(this).find("input[class*='totalSx']").val();
        suma+=parseFloat(totalSx);
    });
    $('#preciox').val(suma);
    var costof=new Intl.NumberFormat('es-MX',{style: 'currency', currency: 'MXN'}).format(suma);
    $('.preciox_x').html(costof);
}


function get_tabla_servicios(id){
    cant_row_servicio=0;
    $('#tabla_servicios_datos').html('');
    $.ajax({
        type:'POST',
        url: base_url+"Servicio/get_tabla_servicios_venta",
        data: {id:id},
        success: function (response){
            var array = $.parseJSON(response);
            if(array.length>0) {
                var contx=0;
                var suma_total=0;
                array.forEach(function(element){

                    var multi=element.cantidad*element.precio;
                    row_con_servicios(element.id_detalle_venta,element.id_servicio,element.descripcion,element.duracion,element.precio,element.cantidad,multi,element.servicio); 
                    select2_nombre_servicio(contx);
                    suma_total+=multi;
                    contx++;
                });
                $('#preciox').val(suma_total);
                var costof=new Intl.NumberFormat('es-MX',{style: 'currency', currency: 'MXN'}).format(suma_total);
                $('.preciox_x').html(costof);
                

            }
        },
        error: function(response){
            toastr.error('Error', 'Algo salió mal, intente de nuevo o contacte al administrador del sistema');
        }
    });
}

function btn_eliminar(){
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: '¿Está seguro de cancelar este vale de papeleria, afectara al catalogo de productos?',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                $.ajax({
                    type:'POST',
                    url: base_url+"Servicio/deleteregistro_servicio",
                    data: {
                        id:idreg_servicio
                    },
                    statusCode: {
                        404: function(data) {
                            toastr.error('404','Error');
                        },
                        500: function(data) {
                            console.log(data);
                            toastr.error('Algo salió mal, intente de nuevo o contacte al administrador del sistema','Error');
                        }
                    },
                    success:function(response){  
                        swal("Éxito!", "Eliminado correctamente", "success");
                        setTimeout(function(){ 
                            $('#calendar').fullCalendar( 'refetchEvents' );
                            $('#modal_cita').modal('hide');
                        }, 1500);
                    }
                });
            },
            cancelar: function () 
            {
                
            }
        }
    });
}


function btn_check_servicio(){
    var valor=0;   
    var texto='';
    if($('#check_servicio_cita').is(':checked')){
        valor=1;
        texto='¿Está seguro que deseas inicializar servicio?';
    }else{
        valor=0;
        texto='¿Está seguro que deseas pausar servicio?';
    }

    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: texto,
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                $.ajax({
                    type:'POST',
                    url: base_url+"Servicio/checket_servicio",
                    data: {
                        id:idreg_servicio,valor:valor
                    },
                    statusCode: {
                        404: function(data) {
                            toastr.error('404','Error');
                        },
                        500: function(data) {
                            console.log(data);
                            toastr.error('Algo salió mal, intente de nuevo o contacte al administrador del sistema','Error');
                        }
                    },
                    success:function(response){  
                        var textx='';
                        if(valor==1){
                            textx='Inicializado correctamente';
                        }else{
                            textx='Pausado correctamente';
                        }
                        swal("Éxito!", textx, "success");
                        setTimeout(function(){ 
                            $('#calendar').fullCalendar( 'refetchEvents' );
                            //$('#modal_cita').modal('hide'); 
                        }, 1500);

                    }
                });
            },
            cancelar: function () 
            {
                
            }
        }
    });
}

function href_hoja_servicio(){
    $.ajax({
        type:'POST',
        url: base_url+"Servicio/registro_hoja_servicio",
        data: {
            id:idreg_servicio
        },
        statusCode: {
            404: function(data) {
                toastr.error('404','Error');
            },
            500: function(data) {
                console.log(data);
                toastr.error('Algo salió mal, intente de nuevo o contacte al administrador del sistema','Error');
            }
        },
        success:function(response){  
            window.location.href = base_url+'Servicio/hoja_servicio/'+idreg_servicio; 
        }
    });
    
}

function pdf_hoja_servicio(id){
    window.open(base_url+'Servicio/pdf_hoja_servicio/'+id,'_blank'); 
}

function remover_servicio(id,cant_row_servicio) {
    if(id==0){
        $('.rowS_' + cant_row_servicio).remove();
        calculototal();
    }else{
        $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: '¿Seguro que deseas eliminar este registro?',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                $.ajax({
                    type:'POST',
                    url: base_url+"Servicio/deleteregistro_servicio_venta",
                    data: {
                        id:id
                    },
                    statusCode: {
                        404: function(data) {
                            toastr.error('404','Error');
                        },
                        500: function(data) {
                            console.log(data);
                            toastr.error('Algo salió mal, intente de nuevo o contacte al administrador del sistema','Error');
                        }
                    },
                    success:function(response){  
                        $('.rowS_' + cant_row_servicio).remove();
                        calculototal();
                    }
                });
            },
            cancelar: function () 
            {
                
            }
        }
    });
    }    
}