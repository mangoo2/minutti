var base_url = $('#base_url').val();
var table;

$(document).ready(function () {
	loadtable();
	//loadData();

	btn_datos_servicio();

	//Restablece check del modal
	$('#aceptar_registro').on('hidden.bs.modal', function() {
		$('#check_servicio').prop('checked', false);
		btn_datos_servicio();
  });
});

function reload_tabla(){
    table.destroy();
    loadtable();
}

function loadData() {
	$.ajax({
		url: base_url + "Cotizacion/get_list",
		type: "POST",
		success: function (result) {
			console.log(result);
		}
	});
}

function loadtable() {
	table = $('#table-list').DataTable({
		"bProcessing": true,
		"serverSide": true,
		"searching": true,
		responsive: !0,
		//destroy: true,
		"ajax": {
			"url": base_url + "Cotizacion/get_list",
			type: "POST",
		},
		"columns": [
		    {"data": "idCotizaciones"},
			{"data": "fecha"},
			{"data": "usuario"},
			{"data": null,
				render: function (data, type, row) {
					//console.log(row);
					var html = '';
					html += '<div class="text-center" style="min-width:252px;">';
					html += '<a onclick="modal_ProcutoServicio(' + row.idCotizaciones + ')" class="btn btn-flat btn-light"><i class="fa fa-eye icon_font"></i></a>';
					html += '</div>';
					return html;
				}
			},
			{"data": "estatus",
				render: function (data, type, row) {
					var html = '';
					if (row.estatus == 0) {
						html += '<div class="text-center rounded bg-danger text-white">';
						html += 'Pendiente';
					} else {
						html += '<div class="text-center rounded bg-success text-white">';
						html += 'Completo';
					}
					html += '</div>';
					return html;
				}
			},
			{"data": null,
				render: function (data, type, row) {
					//console.log(row);
					var html = '';
					if (row.estatus == 0) {
						html += '<div class="text-center">';
						html += '<a target="_blank" title="Generar PDF" href=" ' + base_url + 'Cotizacion/formato_cotizacion/' + row.idCotizaciones + '" class="btn btn-flat btn-light"><i class="fa fa-file-pdf-o icon_font"></i></a>';
						if(row.estatus == 0){
							html += '<a title="Aceptar cotizacion" onclick="modal_aceptar(' + row.idCotizaciones + ')" class="btn btn-flat btn-light idcoti_'+row.idCotizaciones+'"\
							data-total="'+row.total+'" data-subtotal="'+row.subtotal+'" data-iva="'+row.iva+'" data-check_servicio="'+row.check_servicio+'" data-cliente="'+row.clienteId+'" data-descuento_e="'+row.descuento_e+'" data-descuento_p="'+row.descuento_p+'"\
							><i class="fa fa-check-square-o icon_font"></i></a>';
						}else{
							html += '<a title="Aceptar cotizacion" class="btn btn-flat btn-light idcoti_'+row.idCotizaciones+'"><i class="fa fa-check-square icon_font"></i></a>';
						}
						html += '<a title="Editar cotizacion" href="' + base_url + 'Cotizacion/add/' + row.idCotizaciones + '" class="btn btn-flat btn-light"><i class="fa fa-edit icon_font"></i></a>';
						html += '<a title="Eliminar cotizacion" onclick="deleteCotizacion(' + row.idCotizaciones + ')" class="btn btn-flat btn-danger"><i class="fa fa-trash-o icon_font"></i></a>';
						html += '</div>';
				    }else{
				    	html += '<a target="_blank" title="Generar PDF" href=" ' + base_url + 'Cotizacion/formato_cotizacion/' + row.idCotizaciones + '" class="btn btn-flat btn-light"><i class="fa fa-file-pdf-o icon_font"></i></a>';
				    }
					return html;
				}
			},
		],
		"order": [
			[0, "desc"]
		],
		"lengthMenu": [
			[25, 50, 100],
			[25, 50, 100]
		],
		// Cambiamos lo principal a Español
		"language": {
			"lengthMenu": "Mostrar _MENU_ entradas",
			"zeroRecords": "Lo sentimos - No se han encontrado elementos",
			"sInfo":  "Mostrando registros del _START_ al _END_ de un total de _TOTAL_  registros",
			"info": "Mostrando página _PAGE_ de _PAGES_",
			"infoEmpty": "No hay registros disponibles",
			"infoFiltered": "(Filtrado de _MAX_ registros totales)",
			"search": "Buscar : _INPUT_",
			"paginate": {
					"previous": "Página previa",
					"next": "Siguiente página"
				}
			}
	});
}

function deleteCotizacion(id) {
	swal({
		title: "¿Desea eliminar la cotización seleccionada?",
		text: "Se eliminará del listado",
		type: "warning",
		showCancelButton: true,
		confirmButtonColor: "#DD6B55",
		confirmButtonText: "Eliminar",
		cancelButtonText: "Cancelar",
}).then(function (isConfirm) {
		if (isConfirm) {
				$.ajax({
						type:'POST',
						url: base_url + "Cotizacion/delete",
						data: {id:id},
						statusCode: {
								404: function(data) {
										swal("Error!", "Error 404", "error");
								},
								500: function(data) {
										//console.log(data);
										swal("Error!", "Algo salió mal, intente de nuevo o contacte al administrador del sistema", "error");
								}
						},
						success:function(response){  
								swal("Éxito!", "Eliminado correctamente", "success");
								reload_tabla();
						}
				});
		}
});
}

function aceptarCotizacion(id) {
	swal({
		title: "¿Desea aceptar la cotización no. " + id + " ?",
		text: "Se actualizará su estatus",
		type: "info",
		showCancelButton: true,
		confirmButtonColor: "#DD6B55",
		confirmButtonText: "Aceptar",
		cancelButtonText: "Cancelar",
}).then(function (isConfirm) {
		if (isConfirm) {
				$.ajax({
						type:'POST',
						url: base_url + "Cotizacion/updateEstatus",
						data: {id:id},
						statusCode: {
								404: function(data) {
										swal("Error!", "Error 404", "error");
								},
								500: function(data) {
										//console.log(data);
										swal("Error!", "Algo salió mal, intente de nuevo o contacte al administrador del sistema", "error");
								}
						},
						success:function(response){  
								swal("Éxito!", "Aceptado correctamente", "success");
								loadtable();
						}
				});
		}
});
}

var id_aceptar=0;
function modal_aceptar(id) {
	$('#aceptar_registro').modal();//{backdrop: 'static', keyboard: false}
	tabla_prod_serv(id);
	id_aceptar = id;
	$('.subtotal_p_total').html($('.idcoti_' + id).data('subtotal'));
	$('.iva_p_total').html($('.idcoti_' + id).data('iva'));
	$('.total_p_total').html($('.idcoti_' + id).data('total'));
	$('.total_p_total2').html($('.idcoti_' + id).data('total'));
	$('#descuento_c').val($('.idcoti_' + id).data('descuento_e'));
	$('#descuento_p').val($('.idcoti_' + id).data('descuento_p'));
	if ($('.idcoti_' + id).data('check_servicio') == "on") {
		$('.txt_servicio').css('display', 'block');
	} else {
		$('.txt_servicio').css('display', 'none');
	}

	addOption($('.idcoti_' + id).data('cliente'));
}


function addOption(cliente) {
	$.ajax({
		type: 'POST',
		url: base_url + 'Cotizacion/get_unidades',
		data: {
			id: cliente
		},
		async: false,
		success: function (data) {
			$('#unidadSelect').empty();
			$('#unidadSelect').append(`<option value="0" selected="" disabled="">Seleccionar una opción</option>`);
			//console.log(data);

			var array = JSON.parse(data);
			array.forEach((element) => {
				let option = 'Modelo: '+ element.modelo +' - placas: '+ element.placas +' - año: '+ element.ano;
				//console.log(element);
				$('#unidadSelect').append(`<option value="${element.unidadId}">${option}</option>`);
			});
		}
	});
}

function selectOption(idOption){
	$("#unidadSelect option[value=" + idOption + "]").attr("selected", true);
}


function tabla_prod_serv(id) {
	var html = '<table class="table table-striped" id="data_tables_productos" style="width: 100%">\
	<thead>\
		<tr>\
			<th>Nombre</th>\
			<th>Stock</th>\
			<th>Cantidad</th>\
			<th>Precio</th>\
			<th>Subtotal</th>\
		</tr>\
	</thead>\
	<tbody id="data_tables_productos_tbody">\
	</tbody>\
</table>';
	$('.tabla_productos').html(html);

	$.ajax({
		type: 'POST',
		url: base_url + 'Cotizacion/tabla_prod_serv',
		data: {
			id: id
		},
		async: false,
		statusCode: {
			404: function(data) {
					swal("Error!", "Error 404", "error");
			},
			500: function(data) {
					//console.log(data);
					swal("Error!", "Algo salió mal, intente de nuevo o contacte al administrador del sistema", "error");
			}
		},
		success: function (data) {
			//console.log(data);
			$('#id_cliente_acep').val(id);
			$('#data_tables_productos_tbody').html(data);
		}
	});

}

/*
function descuento_cotizacion(){
	var descuento = $('#descuento').val() == '' ? 0 : $('#descuento').val();
	var desc = parseFloat(descuento);
	var entre = parseFloat(desc/100); 
	var total = $('.total_p_total2').text();
	var ttl = parseFloat(total*entre);
	var resta = parseFloat(total-ttl);
	$('.total_p_total').html(resta.toFixed(2));
	$('.cantdescuento').html(ttl.toFixed(2));    
}*/


function aceptar_cotizacion(){
	$('#aceptar_cotizacion').modal();
}


function aceptar_registro() {
	var valorfac=0;
	if($('#factura_x').is(':checked')){
        valorfac=1; 
	}else{
		valorfac=0;
	}
	$.ajax({
		type: 'POST',
		url: base_url + 'Cotizacion/registrar_cotizacion',
		data: {
			id: id_aceptar,
			mpago: $('#mpago option:selected').val(),
			total_p_total: $('.total_p_total').text(),
			descuento: $('#descuento_p').val(),
			cantdescuento: $('#descuento_c').val(),
			check_servicio: $('#check_servicio').is(':checked'),
			fecha_servicio: $('#fecha_servicio').val(),
			hora_servicio: $('#hora_servicio').val(),
			unidad_servicio: $('#unidadSelect option:selected').val(),
			factura:valorfac
		},
		async: false,
		statusCode: {
			404: function (data) {
				swal("Error!", "Error 404", "error");
			},
			500: function (data) {
				//console.log(data);
				swal("Error!", "Algo salió mal, intente de nuevo o contacte al administrador del sistema", "error");
			}
		},
		success: function (data) {
			//console.log(data);
			var array = $.parseJSON(data);
			if (array.validar == 0) {
				registrar_productos(array.id_venta, id_aceptar);
				swal("Éxito!", "Aceptado correctamente", "success");
				reload_tabla();
				$('#aceptar_registro').modal('hide');
				$('#aceptar_cotizacion').modal('hide');
			} else {
				swal("Error!", "Uno de los productos le faltan existencias", "info");
			}
		}
	});
}



function registrar_productos(id,idcot){
	$.ajax({
			type:'POST',
			url: base_url+'Cotizacion/ingresarventapro',
			data: {id:id,idcot:idcot},
			async: false,
			statusCode:{
				404: function(data) {
						swal("Error!", "Error 404", "error");
				},
				500: function(data) {
						//console.log(data);
						swal("Error!", "Algo salió mal, intente de nuevo o contacte al administrador del sistema", "error");
				}
			},
			success:function(data){
				//console.log(data);
			}
	});
}


/*
function deleteCotizacion(id) {
	$.confirm({
		boxWidth: '30%',
		useBootstrap: false,
		icon: 'fa fa-warning',
		title: 'Atención!',
		content: '¿Está seguro de Eliminar esta selección?',
		type: 'red',
		typeAnimated: true,
		buttons: {
			confirmar: function () {
				$.ajax({
					type: 'POST',
					url: base_url + "Cotizacion/delete",
					data: {
						id: id
					},
					statusCode: {
						404: function (data) {
							toastr.error('404', 'Error');
						},
						500: function (data) {
							//console.log(data);
							toastr.error('Algo salió mal, intente de nuevo o contacte al administrador del sistema', 'Error');
						}
					},
					success: function (response) {
						toastr.success('Eliminado correctamente', 'Hecho!');
						loadtable();
					}
				});
			},
			cancelar: function () {

			}
		}
	});
}
*/

function modal_ProcutoServicio(id) {
	$('#modal_ProcutoServicio').modal();
	$.ajax({
		url: base_url + "Cotizacion/getDataProductoServicio",
		type: "post",
		data: {
			id: id
		},
		success: function (result) {
			//console.log(result);
			$(".list_ProcutoServicio").html(result);
			$("#tabla_ProcutoServicio").DataTable();
		}
	});
}


function modal_unidad(){
	$('#modal_unidad').modal();
	reload_unidad();

	$('#unidad').val(0);
	$('#placas').val('');
	$('#modelo').val('');
	$('#ano').val('');
	$('.vd_red').remove();

}

function reload_unidad(){
	tabla_unidades = $("#table_unidades").DataTable();
	tabla_unidades.destroy();
	table_unidades();
}


function table_unidades(){
	tabla_unidades=$("#table_unidades").DataTable({
			"bProcessing": true,
			"serverSide": true,
			"searching": false,
			responsive: !0,

			"ajax": {
					"url": base_url+"Cotizacion/get_listado_unidades",
					type: "post",
					data: {
						cliente: $('#id_cliente_acep').val(),
					},
					error: function(){
							$("#table_unidades").css("display", "none");
					},
			},
			"columns": [
				{"data":"placas"},
				{"data":"modelo"},
				{"data":"ano"},
				{"data": null,
						"render": function ( data, type, row, meta ){
						//console.log(row);
						var html='<button type="button"\
												data-idunidad="'+row.unidadId+'"\
												data-modelo="'+row.modelo+'"\
												data-placas="'+row.placas+'"\
												data-ano="'+row.ano+'"\
												data-option="Modelo: '+ row.modelo +' - placas: '+ row.placas +' - año: '+ row.ano+'"\
												class="btn btn-flat btn-danger marc_'+row.unidadId+'" onclick="editar_unidad('+row.unidadId+')"><i class="fa fa-edit icon_font"></i></button>\
												<button type="button" class="btn btn-flat btn-danger" onclick="delete_registro_unidad('+row.unidadId+');"><i class="fa fa-trash-o icon_font"></i> </button>';
						return html;
						}
				},
		],
			"order": [[ 0, "desc" ]],
			"lengthMenu": [[5, 10, 25], [5, 10, 25]],
			// Cambiamos lo principal a Español
			"language": {
					"lengthMenu": "Mostrar _MENU_ entradas",
					"zeroRecords": "Lo sentimos - No se han encontrado elementos",
					"sInfo":  "Mostrando registros del _START_ al _END_ de un total de _TOTAL_  registros",
					"info": "Mostrando página _PAGE_ de _PAGES_",
					"infoEmpty": "No hay registros disponibles",
					"infoFiltered": "(Filtrado de _MAX_ registros totales)",
					"search": "Buscar : _INPUT_",
					"paginate": {
							"previous": "Página previa",
							"next": "Siguiente página"
					}
			}
	
	});
}

function editar_unidad(id){
	id_unidad=1;
	$('#unidad').val($('.marc_'+id).data('idunidad'));
	$('#placas').val($('.marc_'+id).data('placas'));
	$('#modelo').val($('.marc_'+id).data('modelo'));
	$('#ano').val($('.marc_'+id).data('ano'));
	
}

function delete_registro_unidad(id){
	$.confirm({
			boxWidth: '30%',
			useBootstrap: false,
			icon: 'fa fa-warning',
			title: 'Atención!',
			content: '¿Está seguro de Eliminar esta unidad?',
			type: 'red',
			typeAnimated: true,
			buttons:{
					confirmar: function (){
							$.ajax({
									type:'POST',
									url: base_url+"index.php/Cotizacion/delete_unidad",
									data: {
										unidadId:id
									},
									statusCode: {
											404: function(data) {
													toastr.error('404','Error');
											},
											500: function(data) {
													//console.log(data);
													toastr.error('Algo salió mal, intente de nuevo o contacte al administrador del sistema','Error');
											}
									},
									success:function(response){  
										//console.log(response);
											toastr.success( 'Eliminado correctamente','Hecho!');

											reload_unidad();
											addOption($('#id_cliente_acep').val());
									}
							});
					},
					cancelar: function () 
					{
							
					}
			}
	});
}


function add_unidad(){
	var form_register = $('#form_unidad');
	var error_register = $('.alert-danger', form_register);
	var success_register = $('.alert-success', form_register);
	form_register.validate({
			errorElement: 'div', //default input error message container
			errorClass: 'vd_red', // default input error message class
			focusInvalid: false, // do not focus the last invalid input
			ignore: "",
			rules: {
					placas: {
							required: true
					},
					modelo: {
						required: true
					},
					ano: {
							required: true
					}
			},
			errorPlacement: function(error, element) {
					if (element.parent().hasClass("vd_checkbox") || element.parent().hasClass("vd_radio")){
							element.parent().append(error);
					} else if (element.parent().hasClass("vd_input-wrapper")){
							error.insertAfter(element.parent());
					}else {
							error.insertAfter(element);
					}
			}, 
			
			invalidHandler: function (event, validator) { //display error alert on form submit              
							success_register.fadeOut(500);
							error_register.fadeIn(500);
							scrollTo(form_register,-100);

			},

			highlight: function (element) { // hightlight error inputs
	
					$(element).addClass('vd_bd-red');
					$(element).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');

			},

			unhighlight: function (element) { // revert the change dony by hightlight
					$(element)
							.closest('.control-group').removeClass('error'); // set error class to the control group
			},

			success: function (label, element) {
					label
							.addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
							.closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
					$(element).removeClass('vd_bd-red');
			}
	});
	var $valid = $("#form_unidad").valid();
	if($valid) {
			var datos = form_register.serialize()+'&clienteId='+$('#id_cliente_acep').val();
			//console.log("Datos: " + datos);
			$('.btn_unidad').attr('disabled',true);
			$.ajax({
					type:'POST',
					url: base_url+'Cotizacion/add_unidad',
					data: datos,
					statusCode:{
							404: function(data){
									toastr.error('Error!', 'No Se encuentra el archivo');
							},
							500: function(){
									toastr.error('Error', '500');
							}
					},
					success:function(data){
							toastr.success('Hecho!', 'Guardado Correctamente');
							setTimeout(function(){ 
									$('.btn_unidad').attr('disabled',false);
									$('#unidad').val(0);
									$('#placas').val('');
									$('#modelo').val('');
									$('#ano').val('');
									$('.vd_red').remove();
									
							}, 2000);
							
							var id = data;
							//console.log("id:"+id);

							addOption($('#id_cliente_acep').val());
							selectOption(id);

							reload_unidad();
					}
			});
	}
}

function btn_datos_servicio(){
	if($('#check_servicio').is(':checked')){
			$('.txt_datos_servicio').css('display','block');
	}else{
			$('.txt_datos_servicio').css('display','none');
	}
}
