var base_url = $('#base_url').val();
var table;

$(document).ready(function () {
	loadtable();
	//loadData();
});

function loadData() {
	$.ajax({
		url: base_url + "Comparativo_proveedores/getlist",
		type: "POST",
		success: function (result) {
			//console.log(result);
		}
	});
}

function loadtable() {
	table = $('#table-list').DataTable({
		"bProcessing": true,
		"serverSide": true,
		"searching": true,
		destroy: true,
		"ajax": {
			"url": base_url + "Herramientas/getlist",
			type: "POST",
		},
		"columns": [{
				"data": "medida"
			},
			{
				"data": "nombre"
			},
			{
				"data": "ubicacion"
			},
			{
				"data": "stock"
			},
			{
				"data": null,
				render: function (data, type, row) {
					//console.log(row);
					var html = '';
					html += '<div class="text-center">';
					html += '<a title="Editar herramienta" href="' + base_url + 'Herramientas/add/' + row.id_herramienta + '" class="btn btn-flat btn-light"><i class="fa fa-edit icon_font"></i></a>';
					html += '<a title="Eliminar herramienta" onclick="deleteHerramienta(' + row.id_herramienta + ')" class="btn btn-flat btn-danger"><i class="fa fa-trash-o icon_font"></i></a>';
					html += '</div>';
					return html;
				}
			},
		],
		"order": [
			[0, "desc"]
		],
		"lengthMenu": [
			[25, 50, 100],
			[25, 50, 100]
		],
	});
}

function deleteHerramienta(id) {
	swal({
		title: "¿Desea eliminar la herramienta seleccionada?",
		text: "Se eliminará del listado",
		type: "warning",
		showCancelButton: true,
		confirmButtonColor: "#DD6B55",
		confirmButtonText: "Eliminar",
		cancelButtonText: "Cancelar",
}).then(function (isConfirm) {
		if (isConfirm) {
				$.ajax({
						type: 'POST',
						url: base_url + "Herramientas/delete",
						data: {id: id},
						statusCode: {
								404: function(data) {
										swal("Error!", "Error 404", "error");
								},
								500: function(data) {
										//console.log(data);
										swal("Error!", "Algo salió mal, intente de nuevo o contacte al administrador del sistema", "error");
								}
						},
						success:function(response){  
								swal("Éxito!", "Eliminado correctamente", "success");
								loadtable();
						}
				});
		}
});
}

/*
function deleteHerramienta(id) {
	$.confirm({
		boxWidth: '30%',
		useBootstrap: false,
		icon: 'fa fa-warning',
		title: 'Atención!',
		content: '¿Está seguro de Eliminar esta selección?',
		type: 'red',
		typeAnimated: true,
		buttons: {
			confirmar: function () {
				$.ajax({
					type: 'POST',
					url: base_url + "Herramientas/delete",
					data: {
						id: id
					},
					statusCode: {
						404: function (data) {
							toastr.error('404', 'Error');
						},
						500: function (data) {
							//console.log(data);
							toastr.error('Algo salió mal, intente de nuevo o contacte al administrador del sistema', 'Error');
						}
					},
					success: function (response) {
						toastr.success('Eliminado correctamente', 'Hecho!');
						loadtable();
					}
				});
			},
			cancelar: function () {

			}
		}
	});
}
*/