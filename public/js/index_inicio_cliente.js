var base_url = $('#base_url').val();
var idCliente = $('#id_cliente').val();

$(document).ready(function () {
	//console.log("Carga JS Correcto");
  loadtable_fecha();
});

function reload_registro(){
    table.destroy();
    loadtable_fecha();
}

function loadtable_fecha() {
	table = $('#table-list').DataTable({
		"bProcessing": true,
		"serverSide": true,
		"searching": true,
		destroy: true,
		"ajax": {
			"url": base_url + "Inicio/get_servicios_fecha",
      data : {
        id: idCliente
      },
			type: "POST",
		},
		"columns": [{
				"data": "id_servicio"
			},
			{
				"data": "descripcion"
			},
			{
				"data": "costo_servicio"
			},
			{"data": null,
        render: function ( data, type, row ){
          var html = row.fecha_servicio+' '+row.hora_servicio;
          return html;
        }
			},
			{
				"data": "estatus",
				render: function (data, type, row) {
					var html = '';
					if (row.check_servicio_cita == 0) {
						html += '<div class="text-center rounded bg-danger text-white">';
						html += 'Pendiente';
					} else if (row.check_servicio_cita == 1) {
						html += '<div class="text-center rounded bg-warning text-white">';
						html += 'En ejecución';
					} else if (row.check_servicio_cita == 2) {
						html += '<div class="text-center rounded bg-success text-white">';
						html += 'Realizado';
					}
					html += '</div>';
					return html;
				}
			},
        {
				"data": null,
				render: function (data, type, row) {
					console.log(row);
					var html = '';
					html += '<div class="text-center" style="min-width:150px;">';
					html += '<a target="_blank" title="Generar PDF" href=" ' + base_url + 'Cotizacion/formato_cotizacion/' + row.id_cotizacion + '" class="btn btn-flat btn-light"><i class="fa fa-file-pdf-o icon_font"></i></a>';
					html += '</div>';
					return html;
				}
			},
        {
				"data": null,
				render: function (data, type, row) {
					//console.log(row);
					var html = '';
					html += '<div class="text-center" style="min-width:150px;">';
					html += '<a target="_blank" title="Generar PDF" href=" ' + base_url + 'Cotizacion/formato_cotizacion/' + row.id_cotizacion + '" class="btn btn-flat btn-light"><i class="fa fa-file-pdf-o icon_font"></i></a>';
					html += '</div>';
					return html;
				}
			},
        {
				"data": null,
				render: function (data, type, row) {
					//console.log(row);
					var html = '';
					html += '<div class="text-center" style="min-width:150px;">';
					html += '<a onclick="modal_vehiculo(' + row.id_venta + ')" class="btn btn-flat btn-light"><i class="fa fa-eye icon_font"></i></a>';
					html += '</div>';
					return html;
				}
			},
			{
				"data": null,
				render: function (data, type, row) {
					var html = '';
						html += '<div class="text-center  text-white" style="min-width:150px;">';
						html += '<a onclick="" class="btn btn-flat btn-dark">Ver</a>';
						html += '</div>';
					return html;
				}
			},
		],
		"order": [
			[0, "desc"]
		],
		"lengthMenu": [
			[25, 50, 100],
			[25, 50, 100]
		],
	});
}

function loadtable_historico() {
	table = $('#table-list').DataTable({
		"bProcessing": true,
		"serverSide": true,
		"searching": true,
		destroy: true,
		"ajax": {
			"url": base_url + "Inicio/get_servicios_historico",
      data : {
        id: idCliente
      },
			type: "POST",
		},
		"columns": [{
			"data": "id_servicio"
		},
		{
			"data": "descripcion"
		},
		{
			"data": "costo_servicio"
		},
		{"data": null,
			render: function ( data, type, row ){
				var html = row.fecha_servicio+' '+row.hora_servicio;
				return html;
			}
		},
		{
			"data": "estatus",
			render: function (data, type, row) {
				var html = '';
				if (row.estatus == 0) {
					html += '<div class="text-center rounded bg-danger text-white">';
					html += 'Pendiente';
				} else if (row.estatus == 1) {
					html += '<div class="text-center rounded bg-warning text-white">';
					html += 'En ejecución';
				} else if (row.estatus == 2) {
					html += '<div class="text-center rounded bg-success text-white">';
					html += 'Realizado';
				}
				html += '</div>';
				return html;
			}
		},
		{
			"data": null,
			render: function (data, type, row) {
				console.log(row);
				var html = '';
				html += '<div class="text-center" style="min-width:150px;">';
				html += '<a target="_blank" title="Generar PDF" href=" ' + base_url + 'Cotizacion/formato_cotizacion/' + row.id_cotizacion + '" class="btn btn-flat btn-light"><i class="fa fa-file-pdf-o icon_font"></i></a>';
				html += '</div>';
				return html;
			}
		},
		{
			"data": null,
			render: function (data, type, row) {
				//console.log(row);
				var html = '';
				html += '<div class="text-center" style="min-width:150px;">';
				html += '<a target="_blank" title="Generar PDF" href=" ' + base_url + 'Cotizacion/formato_cotizacion/' + row.id_cotizacion + '" class="btn btn-flat btn-light"><i class="fa fa-file-pdf-o icon_font"></i></a>';
				html += '</div>';
				return html;
			}
		},
		{
			"data": null,
			render: function (data, type, row) {
				//console.log(row);
				var html = '';
				html += '<div class="text-center" style="min-width:150px;">';
				html += '<a onclick="modal_vehiculo(' + row.id_venta + ')" class="btn btn-flat btn-light"><i class="fa fa-eye icon_font"></i></a>';
				html += '</div>';
				return html;
			}
		},
		{
			"data": null,
			render: function (data, type, row) {
				var html = '';
					html += '<div class="text-center  text-white" style="min-width:150px;">';
					html += '<a onclick="" class="btn btn-flat btn-dark">Ver</a>';
					html += '</div>';
				return html;
			}
		},
	],
		"order": [
			[0, "desc"]
		],
		"lengthMenu": [
			[25, 50, 100],
			[25, 50, 100]
		],
	});
}

function loadtable_activos() {
	table = $('#table-list').DataTable({
		"bProcessing": true,
		"serverSide": true,
		"searching": true,
		destroy: true,
		"ajax": {
			"url": base_url + "Inicio/get_servicios_activos",
      data : {
        id: idCliente
      },
			type: "POST",
		},
		"columns": [{
			"data": "id_servicio"
		},
		{
			"data": "descripcion"
		},
		{
			"data": "costo_servicio"
		},
		{"data": null,
			render: function ( data, type, row ){
				var html = row.fecha_servicio+' '+row.hora_servicio;
				return html;
			}
		},
		{
			"data": "estatus",
			render: function (data, type, row) {
				var html = '';
				if (row.estatus == 0) {
					html += '<div class="text-center rounded bg-danger text-white">';
					html += 'Pendiente';
				} else if (row.estatus == 1) {
					html += '<div class="text-center rounded bg-warning text-white">';
					html += 'En ejecución';
				} else if (row.estatus == 2) {
					html += '<div class="text-center rounded bg-success text-white">';
					html += 'Realizado';
				}
				html += '</div>';
				return html;
			}
		},
		{
			"data": null,
			render: function (data, type, row) {
				console.log(row);
				var html = '';
				html += '<div class="text-center" style="min-width:150px;">';
				html += '<a target="_blank" title="Generar PDF" href=" ' + base_url + 'Cotizacion/formato_cotizacion/' + row.id_cotizacion + '" class="btn btn-flat btn-light"><i class="fa fa-file-pdf-o icon_font"></i></a>';
				html += '</div>';
				return html;
			}
		},
		{
			"data": null,
			render: function (data, type, row) {
				//console.log(row);
				var html = '';
				html += '<div class="text-center" style="min-width:150px;">';
				html += '<a target="_blank" title="Generar PDF" href=" ' + base_url + 'Cotizacion/formato_cotizacion/' + row.id_cotizacion + '" class="btn btn-flat btn-light"><i class="fa fa-file-pdf-o icon_font"></i></a>';
				html += '</div>';
				return html;
			}
		},
		{
			"data": null,
			render: function (data, type, row) {
				//console.log(row);
				var html = '';
				html += '<div class="text-center" style="min-width:150px;">';
				html += '<a onclick="modal_vehiculo(' + row.id_venta + ')" class="btn btn-flat btn-light"><i class="fa fa-eye icon_font"></i></a>';
				html += '</div>';
				return html;
			}
		},
		{
			"data": null,
			render: function (data, type, row) {
				var html = '';
					html += '<div class="text-center  text-white" style="min-width:150px;">';
					html += '<a onclick="" class="btn btn-flat btn-dark">Ver</a>';
					html += '</div>';
				return html;
			}
		},
	],
		"order": [
			[0, "desc"]
		],
		"lengthMenu": [
			[25, 50, 100],
			[25, 50, 100]
		],
	});
}

function modal_vehiculo(id_venta) {
	let id_cliente = $('#id_cliente').val();
	$('#modal_vehiculo').modal();
	$.ajax({
		url: base_url + "Inicio/getDataVehiculo",
		type: "post",
		data: {
			id_cliente: id_cliente,
			id_venta: id_venta
		},
		success: function (result) {
			//console.log(result);
			$(".list_vehiculo").html(result);
			$("#tabla_vehiculo").DataTable();
		}
	});
}

function modal_quejas() {
	$('#modal_quejas').modal();
}

function insertQueja(){
	let id = $('#id_cliente').val();
	let quejas = $('#quejas').val();
	
		$.ajax({
			type:'POST',
			url: base_url + "Inicio/insertQueja",
			data: {
				clienteId: id,
				quejas: quejas
			},
			statusCode: {
					404: function(data) {
							toastr.error('404','Error');
					},
					500: function(data) {
							//console.log(data);
							toastr.error('Algo salió mal, intente de nuevo o contacte al administrador del sistema','Error');
					}
			},
			success:function(response){  
				console.log(response);
					toastr.success( 'Enviado correctamente','Hecho!');
			}
	});
	
}