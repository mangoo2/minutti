var base_url = $('#base_url').val();
var table;
$(document).ready(function($) {
	table=$('#table-data').DataTable();
	loadtable();
});
function loadtable(){
	table.destroy();
	table = $('#table-data').DataTable({
        stateSave: true,
        responsive: !0,
        "bProcessing": true,
        "serverSide": true,
        "ajax": {
            "url": base_url+"index.php/Servicios/getlist_row",
            type: "post",
            "data": {
                'personal':0
            },
        },
        "columns": [
            {"data": "id"},
            {"data": "codigo"},
            {"data": "servico"},
            //{"data": "descripcion"},
            {
                "data": null,
				render: function (data, type, row) {
					//console.log(row);
					var html = '';
					html += '<div class="text-center" style="min-width:252px;">';
					html += '<a onclick="modal_descripcionServ(' + row.id + ')" class="btn btn-flat btn-light"><i class="fa fa-eye icon_font"></i></a>';
					html += '</div>';
					return html;
				}
			},
            {"data": "costo_servicio"},
            {"data": "costo_especial"},
            {"data": "duracion"},
            {"data": null,
                render:function(data,type,row){
                    var html='';
                        var html='<a href="'+base_url+'Servicios/registrar/'+row.id+'" class="btn btn-flat btn-danger"><i class="fa fa-edit icon_font"></i></a>\
                          <a onclick="delete_registro('+row.id+')" class="btn btn-flat btn-danger"><i class="fa fa-trash-o icon_font"></i></a>';
                    return html;
                }
            },
        ],
        "order": [[ 0, "desc" ]],
        "lengthMenu": [[25, 50, 100], [25, 50,100]]
        
    });
}

function btn_cancelar(id){
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: '¿Está seguro de cancelar este traspaso, afectara al catalogo de productos?',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                $.ajax({
                    type:'POST',
                    url: base_url+"Traspasos/update_traspaso",
                    data: {
                        id:id
                    },
                    statusCode: {
                        404: function(data) {
                            toastr.error('404','Error');
                        },
                        500: function(data) {
                            //console.log(data);
                            toastr.error('Algo salió mal, intente de nuevo o contacte al administrador del sistema','Error');
                        }
                    },
                    success:function(response){  
                        toastr.success( 'Eliminado correctamente','Hecho!');
                        loadtable();
                    }
                });
            },
            cancelar: function () 
            {
                
            }
        }
    });
}

function delete_registro(id){
    swal({
        title: "¿Desea eliminar el servicio seleccionado?",
        text: "Se eliminará del listado",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Eliminar",
        cancelButtonText: "Cancelar",
    }).then(function (isConfirm) {
        if (isConfirm) {
            $.ajax({
                type:'POST',
                url: base_url+"Servicios/deleteregistro",
                data: {id:id},
                statusCode: {
                    404: function(data) {
                        swal("Error!", "Error 404", "error");
                    },
                    500: function(data) {
                        //console.log(data);
                        swal("Error!", "Algo salió mal, intente de nuevo o contacte al administrador del sistema", "error");
                    }
                },
                success:function(response){  
                    swal("Éxito!", "Eliminado correctamente", "success");
                    loadtable();
                }
            });
        }
    });
}
/*
function delete_registro(id){
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: '¿Está seguro de eliminar este registro?',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                $.ajax({
                    type:'POST',
                    url: base_url+"Servicios/deleteregistro",
                    data: {id:id},
                    statusCode: {
                        404: function(data) {
                            toastr.error('404','Error');
                        },
                        500: function(data) {
                            console.log(data);
                            toastr.error('Algo salió mal, intente de nuevo o contacte al administrador del sistema','Error');
                        }
                    },
                    success:function(response){  
                        toastr.success( 'Eliminado correctamente','Hecho!');
                        table.ajax.reload();
                    }
                });
            },
            cancelar: function () 
            {
                
            }
        }
    });
}
*/

function modal_descripcionServ(id) {
	$('#modal_descripcionServ').modal();
	$.ajax({
		url: base_url + "Servicios/getDescripcionServicio",
		type: "post",
		data: {
			id: id
		},
		success: function (result) {
			//console.log(result);
			$(".list_descripcionServ").html(result);
			$("#tabla_descripcionServ").DataTable();
		}
	});
}
