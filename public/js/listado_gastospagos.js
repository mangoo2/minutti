var base_url = $('#base_url').val();
var tabla;
var tipo_aux=0;
var id_aux_pass=0;
var tipo_cobro_aux=0;
$(document).ready(function() {
	table();
});

function reload_registro(){
    tabla.destroy();
    table();
}

function table(){
	tabla=$("#data_tables").DataTable({
        "bProcessing": true,
        "serverSide": true,
        "searching": true,
        responsive: !0,
        //"info":     false,
        //"paging": false,
        "ajax": {
            "url": base_url+"GastosPagos/getlistado",
            type: "post",
            "data":{tipo_gasto:$('#tipo_gasto').val()},
            error: function(){
               $("#data_tables").css("display","none");
            }
        },
        "columns": [
            {"data":"id"},
            {"data": null,
                "render": function ( data, type, row, meta ){
                    var html='';
                    if(row.tipocobro==1){
                        html='Gasto';
                    }else if(row.tipocobro==2){
                        html='Pago';
                    }
                    return  html;
                }
            },
            {"data":"nombre"},
            {"data": null,
                "render": function ( data, type, row, meta ){
                    return  '$'+ Number.parseFloat(row.monto).toFixed(2);
                }
            },
            {"data":"fecha_gas"},
            {"data": null,
                "render": function ( data, type, row, meta ){
                    return  row.categoria;
                }
            },
            {"data": null,
                "render": function ( data, type, row, meta ){
                    return  row.empleado+' '+row.apellido_paterno+' '+row.apellido_materno;
                }
            },
            {"data": null,
                "render": function ( data, type, row, meta ){
                var html='';
                var cm="'";
                html+='<a class="btn btn-flat btn-dark gas_'+row.id+'"\
                            data-idgasto="'+row.id+'"\
                            data-gasto="'+row. nombre+'"\
                            data-monto="'+row.monto+'"\
                            data-fecha="'+row.fecha+'"\
                            data-tipocobro="'+row.tipocobro+'"\
                            data-idcategoria="'+row.idcategoria+'"\
                           style="color:white !important" onclick="registro_modal('+row.id+')"><i class="ft-edit"></i></a>\
                        <button type="button" class="btn btn-flat btn-danger" onclick="eliminar_update('+row.id+');"><i class="ft-trash-2"></i></button>';
                return html;
                }
            },
        ],
        "order": [[ 0, "desc" ]],
        "lengthMenu": [[10, 25, 50], [10, 25, 50]],
        // Cambiamos lo principal a Español
        "language": {
            "lengthMenu": "Mostrar _MENU_ entradas",
            "zeroRecords": "Lo sentimos - No se han encontrado elementos",
            "sInfo":  "Mostrando registros del _START_ al _END_ de un total de _TOTAL_  registros",
            "info": "Mostrando página _PAGE_ de _PAGES_",
            "infoEmpty": "No hay registros disponibles",
            "infoFiltered": "(Filtrado de _MAX_ registros totales)",
            "search": "Buscar : _INPUT_",
            "paginate": {
                "previous": "Página previa",
                "next": "Siguiente página"
              }
        }
    
    });
}
function eliminar_update(id){
    $('#eliminar_registro').modal();
    $('#id_aux').val(id);
}

function delete_registro(){
    var id=$('#id_aux').val();
    $.ajax({
        type:'POST',
        url: base_url+'GastosPagos/deleteregistro',
        data: {id:id},
        async: false,
        statusCode:{
            404: function(data){
                toastr.error('Error!', 'No Se encuentra el archivo');
            },
            500: function(){
                toastr.error('Error', '500');
            }
        },
        success:function(data){
            toastr.success('eliminado Correctamente','Hecho!' );
            tabla.ajax.reload();
            $('#eliminar_registro').modal('hide');
        }
    });
}
function registro_modal(id){
	$('#registro_modal').modal();
    if(id==0){
        $('#idgasto').val(0);
        $('#tipocobro').val(0);
        $('#gasto').val('');
        $('#monto').val('');
        $('#fecha').val('');
    }else{
        var tipocobro = $('.gas_'+id).data('tipocobro');
        var idcategoria = $('.gas_'+id).data('idcategoria');
        fun_tipo_cobro_edit(tipocobro);
        $('#tipocobro option:selected').removeAttr('selected');
        $("#tipocobro option[value='"+tipocobro+"']").attr("selected", true);

        $('#idgasto').val($('.gas_'+id).data('idgasto'));
        $('#gasto').val($('.gas_'+id).data('gasto'));
        $('#monto').val($('.gas_'+id).data('monto'));
        $('#fecha').val($('.gas_'+id).data('fecha'));
        setTimeout(function(){ 
            $('#categoria option:selected').removeAttr('selected');
            $("#categoria option[value='"+idcategoria+"']").attr("selected", true);
        }, 1000); 
    }
}

function registrar(){
    var form_register = $('#formgastos');
    var error_register = $('.alert-danger', form_register);
    var success_register = $('.alert-success', form_register);
    var $validator1=form_register.validate({
        errorElement: 'div', //default input error message container
        errorClass: 'vd_red', // default input error message class
        focusInvalid: false, // do not focus the last invalid input
        ignore: "",
        rules: {
            nombre:{
              required: true
            },
            monto:{
              required: true
            },
            fecha:{
              required: true
            }
        },
        
        errorPlacement: function(error, element) {
            if (element.parent().hasClass("vd_checkbox") || element.parent().hasClass("vd_radio")){
                element.parent().append(error);
            } else if (element.parent().hasClass("vd_input-wrapper")){
                error.insertAfter(element.parent());
            }else {
                error.insertAfter(element);
            }
        }, 
        
        invalidHandler: function (event, validator) { //display error alert on form submit              
                success_register.fadeOut(500);
                error_register.fadeIn(500);
                scrollTo(form_register,-100);

        },

        highlight: function (element) { // hightlight error inputs
    
            $(element).addClass('vd_bd-red');
            $(element).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');

        },

        unhighlight: function (element) { // revert the change dony by hightlight
            $(element)
                .closest('.control-group').removeClass('error'); // set error class to the control group
        },

        success: function (label, element) {
            label
                .addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
                .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
            $(element).removeClass('vd_bd-red');
        }
    });
    //////////Registro///////////
    var $valid = $("#formgastos").valid();
    if($valid) {
        $('.btn_registro').attr('disabled',true);
        var datos = form_register.serialize();
        $.ajax({
            type:'POST',
            url: base_url+'GastosPagos/registro_gasto',
            data: datos,
            statusCode:{
                404: function(data){
                    toastr.error('Error!', 'No Se encuentra el archivo');
                },
                500: function(){
                    toastr.error('Error', '500');
                }
            },
            success:function(data){
                var id=data;
                toastr.success('Hecho!', 'Guardado Correctamente');
                setTimeout(function(){ 
                    reload_registro();
                    $('#idgasto').val(0);
                    $('#gasto').val('');
                    $('#monto').val('');
                    $('#fecha').val('');
                    $('.btn_registro').attr('disabled',false);
                }, 1000); 
                $('#registro_modal').modal('hide');
                $('.vd_red').remove();
            }
        });
    }   
}


function eliminar_update_pass(id,tipo){
    $('#modal_verificar').modal();  
    id_aux_pass=id; 
    tipo_aux=tipo;
}

function fun_tipo_cobro(){
    var tipocobro = $('#tipocobro option:selected').val();
    if(tipocobro==1){
        $('.tipo_cobro').html('Gasto');
        $('.text_cobro').css('display','block');
    }else if(tipocobro==2){
        $('.tipo_cobro').html('Pago');
        $('.text_cobro').css('display','block');
    }
    select_cobro(tipocobro);
    tipo_cobro_aux=tipocobro;
}

function fun_tipo_cobro_edit(id){
    var tipocobro = id;
    if(tipocobro==1){
        $('.tipo_cobro').html('Gasto');
        $('.text_cobro').css('display','block');
    }else if(tipocobro==2){
        $('.tipo_cobro').html('Pago');
        $('.text_cobro').css('display','block');
    }
    select_cobro(tipocobro);
    tipo_cobro_aux=tipocobro;
}

function select_cobro(id){
    $.ajax({
        type:'POST',
        url: base_url+"index.php/GastosPagos/get_categoria_tipo",
        data: {
            tipo:id
        },
        statusCode: {
            404: function(data) {
                toastr.error('404','Error');
            },
            500: function(data) {
                console.log(data);
                toastr.error('Algo salió mal, intente de nuevo o contacte al administrador del sistema','Error');
            }
        },
        success:function(response){  
            $('.text_select_categoria').html(response);
        }
    });
    
}


/// Categorias
function modal_categoria(){
    $('#modal_categoria').modal();
    reload_categoria();
    $('#idcategoria_categoria').val(0);
    $('#nombre_categoria').val('');
    $('.vd_red').remove();
}

function add_categoria(){
    var form_register = $('#form_categoria');
    var error_register = $('.alert-danger', form_register);
    var success_register = $('.alert-success', form_register);
    form_register.validate({
        errorElement: 'div', //default input error message container
        errorClass: 'vd_red', // default input error message class
        focusInvalid: false, // do not focus the last invalid input
        ignore: "",
        rules: {
            categoria: {
                required: true
            }
        },
        errorPlacement: function(error, element) {
            if (element.parent().hasClass("vd_checkbox") || element.parent().hasClass("vd_radio")){
                element.parent().append(error);
            } else if (element.parent().hasClass("vd_input-wrapper")){
                error.insertAfter(element.parent());
            }else {
                error.insertAfter(element);
            }
        }, 
        
        invalidHandler: function (event, validator) { //display error alert on form submit              
                success_register.fadeOut(500);
                error_register.fadeIn(500);
                scrollTo(form_register,-100);

        },

        highlight: function (element) { // hightlight error inputs
    
            $(element).addClass('vd_bd-red');
            $(element).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');

        },

        unhighlight: function (element) { // revert the change dony by hightlight
            $(element)
                .closest('.control-group').removeClass('error'); // set error class to the control group
        },

        success: function (label, element) {
            label
                .addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
                .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
            $(element).removeClass('vd_bd-red');
        }
    });
    var $valid = $("#form_categoria").valid();
    if($valid) {
        var datos = form_register.serialize()+'&tipo='+tipo_cobro_aux;
        $('.btn_categoria').attr('disabled',true);
        $.ajax({
            type:'POST',
            url: base_url+'GastosPagos/addcategoria',
            data: datos,
            statusCode:{
                404: function(data){
                    toastr.error('Error!', 'No Se encuentra el archivo');
                },
                500: function(){
                    toastr.error('Error', '500');
                }
            },
            success:function(data){
                var id_a=parseFloat(data);
                select_cobro(tipo_cobro_aux);
                toastr.success('Hecho!', 'Guardado Correctamente');
                setTimeout(function(){ 
                    $('.btn_categoria').attr('disabled',false);
                    $('#idcategoria_categoria').val(0);
                    $('#nombre_categoria').val('');
                    $('.vd_red').remove();
                    $("#categoria option[value='"+id_a+"']").attr("selected", true);
                }, 2000);
                reload_categoria();
            }
        });
    }
}

function reload_categoria(){
    tabla_categoria=$("#table_categoria").DataTable();
    tabla_categoria.destroy();
    table_categorias();
}
function table_categorias(){
    tabla_categoria=$("#table_categoria").DataTable({
        "bProcessing": true,
        "serverSide": true,
        "searching": false,
        responsive: !0,
        //"info":     false,
        //"paging": false,
        "ajax": {
            "url": base_url+"GastosPagos/getlistado_categoria",
            type: "post",
            data:{tipo:tipo_cobro_aux},
            error: function(){
               $("#table_categoria").css("display","none");
            }
        },
        "columns": [
            {"data":"id"},
            {"data":"nombre"},
            {"data": null,
                "render": function ( data, type, row, meta ){
                var html='<button type="button"\
                            data-idcategoria="'+row.id+'"\
                            data-categoria="'+row.nombre+'"\
                            class="btn btn-flat btn-danger catec_'+row.id+'" onclick="editar_categoria('+row.id+')"><i class="fa fa-edit icon_font"></i></button>\
                            <button type="button" class="btn btn-flat btn-danger" onclick="delete_registro_categoria('+row.id+');"><i class="fa fa-trash-o icon_font"></i> </button>';
                return html;
                }
            },
        ],
        "order": [[ 0, "desc" ]],
        "lengthMenu": [[5, 10, 25], [5, 10, 25]],
        // Cambiamos lo principal a Español
        "language": {
            "lengthMenu": "Mostrar _MENU_ entradas",
            "zeroRecords": "Lo sentimos - No se han encontrado elementos",
            "sInfo":  "Mostrando registros del _START_ al _END_ de un total de _TOTAL_  registros",
            "info": "Mostrando página _PAGE_ de _PAGES_",
            "infoEmpty": "No hay registros disponibles",
            "infoFiltered": "(Filtrado de _MAX_ registros totales)",
            "search": "Buscar : _INPUT_",
            "paginate": {
                "previous": "Página previa",
                "next": "Siguiente página"
              }
        }
    
    });
}
function editar_categoria(id){
    id_categoria=1;
    $('#idcategoria_categoria').val($('.catec_'+id).data('idcategoria'));
    $('#nombre_categoria').val($('.catec_'+id).data('categoria'));
}

function delete_registro_categoria(id){
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: '¿Está seguro de Eliminar esta categoria?',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                $.ajax({
                    type:'POST',
                    url: base_url+"index.php/GastosPagos/delete_categoria",
                    data: {
                        categoriaId:id
                    },
                    statusCode: {
                        404: function(data) {
                            toastr.error('404','Error');
                        },
                        500: function(data) {
                            console.log(data);
                            toastr.error('Algo salió mal, intente de nuevo o contacte al administrador del sistema','Error');
                        }
                    },
                    success:function(response){  
                        toastr.success( 'Eliminado correctamente','Hecho!');
                        reload_categoria();
                        $('#categoriaId option[value="'+id+'"]').remove();
                    }
                });
            },
            cancelar: function () 
            {
                
            }
        }
    });
}