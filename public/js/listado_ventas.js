var base_url = $('#base_url').val();
var tabla;
$(document).ready(function() {
	table();
});

function reload_registro(){
    tabla.destroy();
    table();
}

function table(){
    var tipo_venta = $('#tipo_venta option:selected').val();
    var tipo_factura = $('#tipo_factura option:selected').val();
	tabla=$("#data_tables").DataTable({
        "bProcessing": true,
        "serverSide": true,
        "searching": true,
        responsive: !0,
        //"info":     false,
        //"paging": false,
        "ajax": {
            "url": base_url+"ListaVentas/getlistado",
            type: "post",
            "data":{tipo_venta:tipo_venta,tipo_factura:tipo_factura},
            error: function(){
               $("#data_tables").css("display","none");
            }
        },
        "columns": [
            {"data":"id_venta"},
            {"data":"reg"},
            {"data":"vendedor"},
            {"data": null,
                "render": function ( data, type, row, meta ){
                    var cant=parseFloat(row.monto_total);
                    var html ='$'+cant.toFixed(2);
                return html;
                }
            },
            {"data": null,
                "render": function ( data, type, row, meta ){
                    var html = '';
                    if(row.metodo==1){
                        html = 'Efectivo';
                    }else if(row.metodo==2){
                        html = 'Tarjeta de crédito';
                    }else if(row.metodo==3){
                        html = 'Tarjeta de débito';
                    }else{ //if(row.metodo==4){
                        html = '';//'Crédito';
                    }
                return html;
                }
            },
            {"data": null,
                "render": function ( data, type, row, meta ){
                    var cant=parseFloat(row.descuentocant);
                    var html ='$'+cant.toFixed(2);
                return html;
                }
            },
            {"data":"cliente"},
            {"data": null,
                "render": function ( data, type, row, meta ){
                    var html = '';
                    if(row.cancelado==1){
                        html = '<span class="badge btn-danger border_eti">Cancelado</span>';
                    }else{
                        html = '<span class="badge btn-success border_eti">Activa</span>';
                    }
                return html;
                }
            },
            {"data": null,
                "render": function ( data, type, row, meta ){
                    var html = '';
                    if(row.factura==1){
                        html = '<span class="badge btn-danger border_eti">Se factura</span>';
                    }else{
                        html = '<span class="badge btn-success border_eti">No</span>';
                    }
                return html;
                }
            },
            
            {"data": null,
                "render": function ( data, type, row, meta ){
                var html='';
                var btn_cancelar='';
                    if(row.cancelado!=1){ 
                        btn_cancelar='<button type="button" class="btn btn-raised btn-icon btn_crema" onclick="cancelar('+row.id_venta+','+row.monto_total+')"><i class="fa fa-ban"></i></button>';
                    }
                var c='';
                    if(row.tipo_venta==1){
                        c='<a class="btn btn-raised btn-icon btn_sinfondo" disabled><i class="fa fa-copyright"></i></a>';    
                    }else{
                        c='';
                    } 

                var agendar='';
                if(row.fecha_servicio==null){
                    if(row.serviciott!=0){
                        agendar='<a class="btn btn-flat btn-dark" style="color:white" onclick="agendar_cita('+row.id_venta+','+row.id_cliente+')">Agendar</a>';    
                    }
                }
                //var btn_credito='';
                    /*if(row.metodo==4){
                        var color = '';
                        if(row.pagado_credito==0){
                            color = 'btn-danger'; 
                        }else{
                            color = 'btn-success';
                        }
                        btn_credito='<button type="button" class="btn btn-raised btn-icon '+color+' abono_'+row.id_venta+'"\
                                    data-total="'+row.monto_total+'"\
                                    data-cliente="'+row.cliente+'"\
                                    data-fecha="'+row.reg+'"\
                                    onclick="modal_credito('+row.id_venta+')"><i class="fa fa-money"></i></button>';
                    }else{
                        btn_credito='';
                    }*/
                    html+='<div class="form-group">\
                        <div class="btn-group" role="group" aria-label="Basic example">'+c+'\
                            <a class="btn btn-raised btn-icon" onclick="ticket('+row.id_venta+')" title="Ticket"><i class="fa fa-book"></i></a>\
                            '+btn_cancelar;
                            html+=agendar;
                            //html+=btn_credito;
                    html+='</div>\
                    </div>';
                return html;
                }
            },
        ],
        "order": [[ 0, "desc" ]],
        "lengthMenu": [[10, 25, 50], [10, 25, 50]],
        // Cambiamos lo principal a Español
        "language": {
            "lengthMenu": "Mostrar _MENU_ entradas",
            "zeroRecords": "Lo sentimos - No se han encontrado elementos",
            "sInfo":  "Mostrando registros del _START_ al _END_ de un total de _TOTAL_  registros",
            "info": "Mostrando página _PAGE_ de _PAGES_",
            "infoEmpty": "No hay registros disponibles",
            "infoFiltered": "(Filtrado de _MAX_ registros totales)",
            "search": "Buscar : _INPUT_",
            "paginate": {
                "previous": "Página previa",
                "next": "Siguiente página"
              }
        }
    
    });
}

function eliminar_update(id){
    $('#eliminar_registro').modal();
    $('#id_aux').val(id);
}

function delete_registro(){
    var id=$('#id_aux').val();
    $.ajax({
        type:'POST',
        url: base_url+'Traspasos/deleteregistro',
        data: {id:id},
        async: false,
        statusCode:{
            404: function(data){
                toastr.error('Error!', 'No Se encuentra el archivo');
            },
            500: function(){
                toastr.error('Error', '500');
            }
        },
        success:function(data){
            toastr.success('eliminado Correctamente','Hecho!' );
            tabla.ajax.reload();
            $('#eliminar_registro').modal('hide');
        }
    });
}

var id_venta_aux=0;
var resta=0;
function modal_credito(id){
    id_venta_aux=id;
    var cliente = $('.abono_'+id).data('cliente');
    $('.cliente_txt').html('<b>Cliente:'+cliente+'<b>');
    $('.venta_txt').html('<b>Venta:'+id+'<b>');
    var fecha = $('.abono_'+id).data('fecha');
    $('.fechahora_txt').html('<b>Fecha y hora de venta: '+fecha+'<b>');
    
    $('#modal_credito_registro').modal();
    calular_abono_restante(id);
    tabla_abonos(id);
}

function calular_abono_restante(id){
    var nombre_persona = $('#nombre_persona').val();
    var fecha = $('#fecha_abono').val();  
    $('.fecha_abono').html(fecha);
    $('.logueado').html(nombre_persona);
    $.ajax({
        type:'POST',
        url: base_url+'ListaVentas/get_abono_venta',
        data: {id_venta:id},
        async: false,
        statusCode:{
            404: function(data){
                toastr.error('Error!', 'No Se encuentra el archivo');
            },
            500: function(){
                toastr.error('Error', '500');
            }
        },
        success:function(data){
            var array = $.parseJSON(data);
            $('.total_venta').html('<b>Total:$'+array.total+'</b>');
            $('.resta').html('<b>Restante: $'+array.resta+'</b>');
            resta=array.resta;
        }
    });
}

function guardar_abono(){
    var personalId = $('#personalId').val();
    var fecha = $('#fecha_abono2').val();  
    var cantidad = $('#cantidad_abono').val();  
    var cantidad_aux=parseFloat(cantidad);
    var resta_aux=parseFloat(resta);
    if(cantidad!=0 || cantidad!=''){
        if(cantidad_aux<=resta_aux){
            $.ajax({
                type:'POST',
                url: base_url+'ListaVentas/registrar_abono',
                data: {id_venta:id_venta_aux,idpersona:personalId,cantidad:cantidad,fecha:fecha},
                async: false,
                statusCode:{
                    404: function(data){
                        toastr.error('Error!', 'No Se encuentra el archivo');
                    },
                    500: function(){
                        toastr.error('Error', '500');
                    }
                },
                success:function(data){
                    calular_abono_restante(id_venta_aux);
                    tabla_abonos(id_venta_aux);
                    $('#cantidad_abono').val('');  
                    tabla_abonos
                }
            });
        }else{
            toastr.error('La cantidad es mayor a lo que resta', '¡Atención!');
        }
    }else{
        toastr.error('El campo de cantidad esta vacio', '¡Atención!');
    }
}

function tabla_abonos(id){
    $.ajax({
        type:'POST',
        url: base_url+'ListaVentas/get_tabla_abono',
        data: {id_venta:id},
        async: false,
        statusCode:{
            404: function(data){
                toastr.error('Error!', 'No Se encuentra el archivo');
            },
            500: function(){
                toastr.error('Error', '500');
            }
        },
        success:function(data){
            var array = $.parseJSON(data);
            $('#tabla_tr_abono1').html(array.get_abono1);
            $('#tabla_tr_abono2').html(array.get_abono2);
            $('.total_abono').html(array.suma_cantidad);
            reload_registro();
        }
    });
}

function cancelar_abono(id){
    $.ajax({
        type:'POST',
        url: base_url+'ListaVentas/deleteregistro_abono',
        data: {id:id},
        async: false,
        statusCode:{
            404: function(data){
                toastr.error('Error!', 'No Se encuentra el archivo');
            },
            500: function(){
                toastr.error('Error', '500');
            }
        },
        success:function(data){
            toastr.success('Eliminado Correctamente','Hecho!' );
            tabla_abonos(id_venta_aux);
            calular_abono_restante(id_venta_aux);
        }
    });
}

function cancelar(ID,CantidadTicket){
    $('#cancelar').modal();
    $("#hddIdVenta").val(ID);
    $("#NoTicket").html(ID);
    $("#CantidadTicket").html(CantidadTicket);  
}
function cancelar_venta(){
    params = {};
    params.id = $('#hddIdVenta').val();
    $.ajax({
        type:'POST',
        url: base_url+'ListaVentas/cancalarventa',
        data:params,
        async:false,
        success:function(data){
          toastr.success('Cancelado Correctamente','Hecho!');
          reload_registro();
        }
    });
}

function agendar_cita(id,idcliente){
    id_venta_aux=id;
    addOption(idcliente);
    $('#aceptar_registro').modal();
}

function addOption(cliente) {
    $.ajax({
        type: 'POST',
        url: base_url + 'Cotizacion/get_unidades',
        data: {
            id: cliente
        },
        async: false,
        success: function (data) {
            $('#unidadSelect').empty();
            $('#unidadSelect').append(`<option value="0" selected="" disabled="">Seleccionar una opción</option>`);
            //console.log(data);

            var array = JSON.parse(data);
            array.forEach((element) => {
                let option = 'Modelo: '+ element.modelo +' - placas: '+ element.placas +' - año: '+ element.ano;
                //console.log(element);
                $('#unidadSelect').append(`<option value="${element.unidadId}">${option}</option>`);
            });
        }
    });
}

function aceptar_registro_cita() {
    $.ajax({
        type: 'POST',
        url: base_url + 'ListaVentas/registrar_agenda',
        data: {
            idventa:id_venta_aux,
            fecha_servicio: $('#fecha_servicio').val(),
            hora_servicio: $('#hora_servicio').val(),
            unidad_servicio: $('#unidadSelect option:selected').val(),
        },
        statusCode: {
            404: function (data) {
                swal("Error!", "Error 404", "error");
            },
            500: function (data) {
                //console.log(data);
                swal("Error!", "Algo salió mal, intente de nuevo o contacte al administrador del sistema", "error");
            }
        },
        success: function (data) {
            swal("Éxito!", "Aceptado correctamente", "success");
            reload_registro();
            $('#aceptar_registro').modal('hide');
        }
    });
}