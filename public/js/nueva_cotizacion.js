var base_url = $('#base_url').val();

$(document).ready(function () {
	getvehicles();
});
producto_select();
var aux = true;

var table = [];

function save() {
	item_d = {};
	item_d["id_n"] = $('#cotizaId').val();
	item_d["reg"] = $('#registro').val();
	item_d["user"] = $('#userId').val();
	item_d["folio"] = $('#folio').val();
	item_d["encabezado"] = $('#encabezado').val();
	item_d["vehiculo"] = $('select[id=vehiculo]').val();
	item_d["equipamiento"] = $('select[id=equipo]').val();

	$.ajax({
		type: 'POST',
		url: base_url + "Nueva_cotizacion/insert",
		data: {
			'array': JSON.stringify(item_d)
		},
		beforeSend: function () {
			$(".button_save").attr("disabled", true);
		},
		success: function (data) {
			var id = data;
			id = id.replace(/\s+/g, '');
			//console.log('id: ' + id);

			var DATA = [];
			var TABLA = $("#tabla_equipo tbody > tr");
			TABLA.each(function () {
				item = {};
				item["id"] = id;
				item["id_equipo"] = $(this).find("input[id*='id_equipo']").val();
				DATA.push(item);
			});

			INFO = new FormData();
			aInfo = JSON.stringify(DATA);
			INFO.append('data', aInfo);

			$.ajax({
				data: INFO,
				type: 'POST',
				url: base_url + "Nueva_cotizacion/insert2",
				processData: false,
				contentType: false,
				async: false,
				success: function (data) {
					//console.log(data);
				},
			});

			toastr.success('Cotizacion guardada', 'Hecho!');
			setTimeout(function () {
				window.location.href = base_url + "Nueva_cotizacion";
			}, 2000);
		},
		error: function (data) {
			toastr.error('Error', 'Algo salió mal, intente de nuevo o contacte al administrador del sistema');
		}
	});
}

function get_vehicle(option) {
	let type_vehicle;
	if (option != null) {
		type_vehicle = option;
	} else {
		type_vehicle = $('#vehiculo').val();
	}

	let sprinter = '<option value="0">Seleccionar una opción</option>\
									<option value="1">Turismo alto</option>\
                  <option value="2">Turismo medio</option>\
									<option value="3">Transporte de personal</option>\
                  <option value="4">Transporte publico</option>';

	let crafter =  '<option value="0">Seleccionar una opción</option>\
									<option value="1">Turismo alto</option>\
									<option value="2">Turismo medio</option>\
									<option value="3">Urbana</option>';

	if (type_vehicle == 1) {
		$('#equipo option').remove();
		$('#tabla_equipo').children().remove();
		$('#equipo').append(sprinter);
	} else if (type_vehicle == 2) {
		$('#equipo option').remove();
		$('#tabla_equipo').children().remove();
		$('#equipo').append(crafter);
	}
	validate_fields();
};

function get_equip() {
	let id_update = $('#cotizaId').val();
	let type_vehicle = $('#vehiculo').val();
	let type_equip = $('select[id=equipo]').val();

	if (id_update > 0 && aux) {
		aux = false;
		$.ajax({
			type: 'post',
			url: base_url + "Nueva_cotizacion/get_equip_up",
			data: {
				'id': id_update,
			},
			success: function (data) {
				table = JSON.parse(data);
				//console.log(table);
				list_equip();
			}
		});

	} else {
		$.ajax({
			type: 'post',
			url: base_url + "Nueva_cotizacion/get_equip",
			data: {
				'type': type_equip,
				'vehicle': type_vehicle
			},
			success: function (data) {
				table = JSON.parse(data);
				//console.log(table);
				list_equip();
			}
		});

	}
}

function list_equip() {
	let equip = $('#equipo').val();

	let tab_equipo = '<thead>\
											<tr>\
												<th width="5%">#</th>\
												<th width="65%">Descripción</th>\
												<th width="25%">Vista</th>\
												<th width="5%">Eliminar</th>\
											</tr>\
										</thead>\
										<tbody>';

	table.forEach((item, index) => {
		tab_equipo += '<tr>\
										<td class="text-center"><input type="hidden" value="' + item.id + '" id="id_equipo">' + (index + 1) + '</td>\
										<td><span class="font-weight-semi-bold">' + item.title + '</span>' + item.text + '</td>\
										<td><img src="' + base_url + 'public/img/equipamiento/' + item.img + '" class="d-block w-100" alt=' + item.title + '></td>\
										<td class="text-center"><a title="Eliminar elemento" onclick="delete_element(' + index + ')" class="btn btn-danger text-white"><i class="fa fa-minus icon_font"></i></a></td>\
									</tr>';
	});

	tab_equipo += '</tbody>';


	if (equip != 0) {
		$('#tabla_equipo').children().remove();
		$('#tabla_equipo').append(tab_equipo);
	} else {
		$('#tabla_equipo').children().remove();
	}
	validate_fields();
}

function delete_element(index) {
	$.confirm({
		boxWidth: '30%',
		useBootstrap: false,
		icon: 'fa fa-warning',
		title: 'Atención!',
		content: '¿Está seguro de Eliminar la configuración?',
		type: 'red',
		typeAnimated: true,
		buttons: {
			confirmar: function () {
				table.splice(index, 1);
				list_equip();
			},
			cancelar: function () {}
		}
	});
}

var option_text = "";
var option_img = "";

function producto_select() {
	$('#select_equip').select2({
		width: '100%',
		minimumInputLength: 2,
		minimumResultsForSearch: 10,
		placeholder: 'Agregar productos',
		ajax: {
			url: base_url + 'Nueva_cotizacion/search_equip',
			dataType: "json",
			data: function (params) {
				var query = {
					search: params.term,
				}
				return query;
			},
			processResults: function (data) {
				var items = [];
				data.forEach(function (element) {
					$(element).data('title', element.title);
					items.push({
						id: element.id,
						text: element.title,
						description: element.text,
						img: element.img
					});
				});
				return {
					results: items
				};
			},
		}
	}).on('select2:select', function (e) {
		var data = e.params.data;
		option_text = data.description;
		option_img = data.img;
	});
}

function add_config(configddId) {
	let equipotitle = $('#select_equip' + ' option:selected').text();
	let equiposelected = $('#select_equip' + ' option:selected').val();
	let equipotext = option_text;
	let equipoimg = option_img;
	table.push({
		id: equiposelected,
		title: equipotitle,
		text: equipotext,
		img: equipoimg
	});
	list_equip();
}


function validate_fields() {
	if ($('select[id=equipo]').val() == 0) {
		$(".button_save").attr("disabled", true);
		$("#div_select").hide();
	} else {
		$(".button_save").attr("disabled", false);
		$("#div_select").show();
	}
}

function getvehicles() {
	let vehicle = $('#auxvehiculo').val();
	let equipament = $('#auxequipo').val();
	if (vehicle != 0 && equipament != 0) {
		$('#vehiculo').val(vehicle);
		get_vehicle(vehicle);
		$('#equipo').val(equipament);
		get_equip();
	}

	validate_fields();
}
