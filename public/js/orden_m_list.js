var base_url = $('#base_url').val();
var table;
$(document).ready(function($) {
	table=$('#table-data-row').DataTable();
	loadtable();
});
function loadtable(){
	table.destroy();
	table = $('#table-data-row').DataTable({
        stateSave: true,
        responsive: !0,
        "bProcessing": true,
        "serverSide": true,
        "ajax": {
            "url": base_url+"index.php/Salida/getlist_orden_m_row",
            type: "post",
            "data": {
                'personal':0
            },
        },
        "columns": [
            {"data": "ordenId"},
            {"data": "cliente"},
            {"data": "lider"},
            /*
                render:function(data,type,row){
                    var html ='';
                        
                        html=row.nombre+' '+row.apellido_paterno+' '+row.apellido_materno;

                    return html;
                }
            },
            */
            {"data": "serie"},
            {"data": "fecha_solicitud"},
            {"data": "fecha_entrega",},
            {"data": null,
                render:function(data,type,row){
                    var html='';
                    if(row.activo==1){
                        html='<a href="'+base_url+'Salida/documento_pdf_basico/'+row.ordenId+'" target="_blank" class="btn btn-flat btn-danger" title="Imprimir formato básico"><i class="fa fa-file-text-o icon_font"></i></a>\
                            <a href="'+base_url+'Salida/documento_pdf_completo/'+row.ordenId+'/1" target="_blank" class="btn btn-flat btn-danger" title="Imprimir formato completo" ><i class="fa fa-file-text icon_font"></i></a>\
                            <a href="'+base_url+'Salida/orden_m_add/'+row.ordenId+'/1" class="btn btn-flat btn-danger"><i class="fa fa-edit icon_font"></i></a>\
                            <a class="btn btn-flat btn-danger" onclick="btn_cancelar('+row.ordenId+')"><i class="fa fa-times-circle-o icon_font"></i></a>';
                    }else{
                        html='<span class="bg-danger text-highlight white" style="border-radius: 10px;">Cancelada</span>';
                    }
                    return html;
                }
            },
        ],
        "order": [[ 0, "desc" ]],
        "lengthMenu": [[25, 50, 100], [25, 50,100]]
        
    });
}

function btn_cancelar(id){
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: '¿Está seguro de cancelar este traspaso, afectara al catalogo de productos?',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                $.ajax({
                    type:'POST',
                    url: base_url+"Salida/cancelar_orden",
                    data: {
                        id:id
                    },
                    statusCode: {
                        404: function(data) {
                            toastr.error('404','Error');
                        },
                        500: function(data) {
                            console.log(data);
                            toastr.error('Algo salió mal, intente de nuevo o contacte al administrador del sistema','Error');
                        }
                    },
                    success:function(response){  
                        toastr.success( 'Eliminado correctamente','Hecho!');
                        loadtable();
                    }
                });
            },
            cancelar: function () 
            {
                
            }
        }
    });
}