var base_url = $('#base_url').val();
$(document).ready(function($) {
    $('#clienteId').select2({
        width: 'resolve',
        minimumInputLength: 3,
        minimumResultsForSearch: 10,
        placeholder: 'Buscar un cliente',
        ajax: {
            url: base_url+'General/search_cliente',
            dataType: "json",
            data: function (params) {
            var query = {
                search: params.term,
                type: 'public'
            }
            return query;
        },
        processResults: function(data){
            var clientes=data;
            var itemscli = [];
            data.forEach(function(element) {
                itemscli.push({
                    id: element.clienteId,
                    text: element.nombre
                });
            });
            return {
                results: itemscli
            };          
        },  
      }
    });
    /*
    $('#lider').select2({
        width: 'resolve',
        minimumInputLength: 3,
        minimumResultsForSearch: 10,
        placeholder: 'Buscar un lider',
        ajax: {
            url: base_url+'General/search_personal',
            dataType: "json",
            data: function (params) {
            var query = {
                search: params.term,
                type: 'public'
            }
            return query;
        },
        processResults: function(data){
            var clientes=data;
            var itemscli = [];
            data.forEach(function(element) {
                itemscli.push({
                    id: element.personalId,
                    text: element.nombre+' '+element.apellido_paterno+' '+element.apellido_materno
                });
            });
            return {
                results: itemscli
            };          
        },  
      }
    });
    */
});

function subconfig(){
	subconfig_data(0,'','');
}
var configrow=0;
function subconfig_data(configdId,nombresub,configId){
    var config_select_hide=$('#selecte_config').html();
	if(configrow==0){
		var configrowactive=' active ';
	}else{
		var configrowactive='';
	}
    if(configdId>0){
        subconfig_data_row(configrow,configdId);
    }
	var tabs='<li class="nav-item">\
                <a class="nav-link '+configrowactive+'" \
                	id="base-tab'+configrow+'" \
                	data-toggle="tab" \
                	aria-controls="tab'+configrow+'" \
                	href="#tab'+configrow+'" \
                	aria-expanded="true">Conf. '+configrow+'</a>\
              </li>';
    var tabsbody ='<div role="tabpanel" \
    					class="tab-pane '+configrowactive+'" \
    					id="tab'+configrow+'" \
    					aria-expanded="true" \
    					aria-labelledby="base-tab'+configrow+'">\
                        <div class="row">\
                        	<div class="col-md-3" style="padding-right: 0px;">Seleccione configuración solicitada</div>\
                        	<div class="col-md-9">\
                        		<input type="hidden" id="ordenIdd" class="form-control" value="'+configdId+'">\
                        		<select id="configId" class="form-control configId_'+configrow+'" onchange="obtenersubconfig('+configrow+')">\
                        			'+config_select_hide+'\
                        		</select>\
                        	</div>\
                        </div>\
                        <div class="row class_row_add_items">\
                        	<div class="col-md-3">\
                                Número de configuraciones<br>\
                        		<input type="number" id="piezas_'+configrow+'" class="form-control" value="">\
                        	</div>\
                        	<div class="col-md-3">\
                                Nave<br>\
                                <select id="idnave_'+configrow+'" class="form-control">\
                                </select>\
                            </div>\
                        	<div class="col-md-5">\
                                 Subconfiguración<br>\
                        		<select id="configdId_'+configrow+'" class="form-control">\
                        		</select>\
                        	</div>\
                        	<div class="col-md-1">\
                                <br>\
                                <div class="btn-group mr-1 mb-1">\
                            		<button type="button" class="btn btn-raised btn-danger btn-min-width mr-1 mb-1 button_save" style="float: right;" onclick="add_config('+configrow+',0,0,0,0)"><i class="fa fa-plus"></i></button>\
                                    <button type="button" class="btn btn-raised btn-danger btn-min-width mr-1 mb-1 button_save" style="float: right;" onclick="add_modal_producto('+configrow+',0,0,0,0)"><i class="fa fa-eye"></i></button>\
                                </div>\
                        	</div>\
                        </div>\
                        <div class="row">\
                            <div class="col-md-12">\
                                <table class="table thead-inverse tableinsumos_'+configrow+'">\
                                    <thead>\
                                        <tr>\
                                            <th>Cantidad</th>\
                                            <th>Nave</th>\
                                            <th>Subconfiguración</th>\
                                            <th></th>\
                                        </tr>\
                                    </thead>\
                                    <tbody class="tbody_tableinsumos_'+configrow+'">\
                                    </tbody>\
                                </table>\
                            </div>\
                        </div>\
                      </div>';
    $('.addsubconfiguracion_tabs').append(tabs);
    $('.addsubconfiguracion_body').append(tabsbody);
    /*
    if(configdId>0){
        setTimeout(function(){ 
            $('.configId_'+configrow+' option[value='+configId+']').attr("selected", true);
        }, 1500);   
    }
    */
    obtener_nave(configrow);
    configrow++;
}

function add_modal_producto(row){
        var configdId=$('#configdId_'+row+' option:selected').val();
        var idnave=$('#idnave_'+row+' option:selected').val();
        console.log("nave: "+idnave);
        if(configdId!=undefined){
            $.ajax({
                type:'POST',
                url: base_url+"index.php/Salida/get_producto_subconfiguracion",
                data:{id:configdId,id_nave:idnave},
                statusCode:{
                    404: function(data){
                        swal("Error!", "No Se encuentra el archivo", "error");
                    },
                    500: function(){
                        swal("Error!", "500", "error");
                    }
                },
                success:function(data){
                    $('#modal_productos_sub').modal();
                    $('.tabla_productos').html(data);
                }
            });  
        }else{
            toastr.error('Falta seleccionar una subconfiguración','Atención!');
        }
}

function obtenersubconfig(row){
	var config=$('.configId_'+row+' option:selected').val();
	$.ajax({
        type:'POST',
        url: base_url+"index.php/Salida/obtenersubconfig",
        data: {
            config:config
        },
        statusCode: {
            404: function(data) {
                toastr.error('404','Error');
            },
            500: function(data) {
                toastr.error('Algo salió mal, intente de nuevo o contacte al administrador del sistema','Error');
            }
        },
        success:function(data){  
            $('#configdId_'+row).html(data);
        }
    });
}

function obtener_nave(row){
    $.ajax({
        type:'POST',
        url: base_url+"index.php/Salida/obtener_nave",
        statusCode: {
            404: function(data) {
                toastr.error('404','Error');
            },
            500: function(data) {
                toastr.error('Algo salió mal, intente de nuevo o contacte al administrador del sistema','Error');
            }
        },
        success:function(data){  
            $('#idnave_'+row).html(data);
        }
    });
}

var add_config_row=0;
function add_config(row,subconfigId,piezas_row,subconfigId_row,subconfig,navetext,idnave){
    if(subconfigId==0){
        var piezas=$('#piezas_'+row).val();
        var configdIdtext=$('#configdId_'+row+' option:selected').text();
        var configdId=$('#configdId_'+row+' option:selected').val();
        var navetext=$('#idnave_'+row+' option:selected').text();
        var idnave=$('#idnave_'+row+' option:selected').val();
        var numv=verificacion_existencia(configdId,piezas,idnave);
        var add_config='';
        if(piezas!=''){
            if(idnave!=0){
                if(configdId!=undefined && configdId!=0){
                    if(numv==0){
                        if(piezas_row>0){
                            piezas=piezas_row;
                        }
                        console.log(subconfigId_row);
                        if(subconfigId_row>0){
                            configdIdtext=subconfig;
                            configdId=subconfigId_row;
                        }
                        add_config+='<tr class="add_config_delete_'+add_config_row+'">\
                            <td>\
                                <input type="hidden" id="ordenIddd" value="'+subconfigId+'">\
                                <input type="hidden" id="piezas" value="'+piezas+'">\
                                '+piezas+'\
                            </td>\
                            <td>\
                                <input type="hidden" id="sucursalId" value="'+idnave+'">\
                                '+navetext+'\
                            </td>\
                            <td>\
                                <input type="hidden" id="configdId" value="'+configdId+'">\
                                '+configdIdtext+'\
                            </td>\
                            <td>\
                                <button type="button" class="btn btn-raised btn-danger btn-min-width mr-1 mb-1 button_save" style="float: right;" onclick="add_config_delete('+add_config_row+','+subconfigId+')"><i class="fa fa-minus"></i></button>\
                            </td>\
                        </tr>';
                    }else{
                        toastr.error('Uno de los productos no cuenta con suficiente existencia','Atención!');
                    }
                }else{
                    toastr.error('Falta seleccionar subconfiguracion','Atención!');
                }
            }else{
                toastr.error('Falta seleccionar nave','Atención!');
            }
        }else{
            toastr.error('Falta agregar número de configuraciones','Atención!');
        }
    }else{
        if(piezas_row>0){
            piezas=piezas_row;
        }
        console.log(subconfigId_row);
        if(subconfigId_row>0){
            configdIdtext=subconfig;
            configdId=subconfigId_row;
        }
        add_config+='<tr class="add_config_delete_'+add_config_row+'">\
            <td>\
                <input type="hidden" id="ordenIddd" value="'+subconfigId+'">\
                <input type="hidden" id="piezas" value="'+piezas+'">\
                '+piezas+'\
            </td>\
            <td>\
                <input type="hidden" id="sucursalId" value="'+idnave+'">\
                '+navetext+'\
            </td>\
            <td>\
                <input type="hidden" id="configdId" value="'+configdId+'">\
                '+configdIdtext+'\
            </td>\
            <td>\
                <button type="button" class="btn btn-raised btn-danger btn-min-width mr-1 mb-1 button_save" style="float: right;" onclick="add_config_delete('+add_config_row+','+subconfigId+')"><i class="fa fa-minus"></i></button>\
            </td>\
        </tr>';
    }
    $('.tbody_tableinsumos_'+row).append(add_config);
    add_config_row++;
}

function verificacion_existencia(id,piezas,idnave){
    var result=0;
    $.ajax({
        type:'POST',
        url: base_url+"index.php/Salida/get_verificacion_existencia",
        data:{id:id,piezas:piezas,sucursalId:idnave},
        async: false,
        statusCode:{
            404: function(data){
                swal("Error!", "No Se encuentra el archivo", "error");
            },
            500: function(){
                swal("Error!", "500", "error");
            }
        },
        success:function(data){
            result=data;
        }
    });
    return result;
}

function add_config_delete(row,id){
    if(id>0){
        $.confirm({
            boxWidth: '30%',
            useBootstrap: false,
            icon: 'fa fa-warning',
            title: 'Atención!',
            content: '¿Está seguro de eliminar esta subconfiguración, afectara al catalogo de productos?',
            type: 'red',
            typeAnimated: true,
            buttons:{
                confirmar: function (){
                    $.ajax({
                        type:'POST',
                        url: base_url+"Salida/cancelar_orden_detalle",
                        data: {
                            id:id
                        },
                        statusCode: {
                            404: function(data) {
                                toastr.error('404','Error');
                            },
                            500: function(data) {
                                console.log(data);
                                toastr.error('Algo salió mal, intente de nuevo o contacte al administrador del sistema','Error');
                            }
                        },
                        success:function(response){  
                            toastr.success( 'Eliminado correctamente','Hecho!');
                            $('.add_config_delete_'+row).remove(); 
                        }
                    });
                },
                cancelar: function () 
                {
                    
                }
            }
        });
    }else{
       $('.add_config_delete_'+row).remove(); 
    }
        
}
function save(){
    var form_register = $('#form-configuracion');
    var error_register = $('.alert-danger', form_register);
    var success_register = $('.alert-success', form_register);
    form_register.validate({
        errorElement: 'div', //default input error message container
        errorClass: 'vd_red', // default input error message class
        focusInvalid: false, // do not focus the last invalid input
        ignore: "",
        rules: {
            clienteId: {
                required: true
            },
            lider: {
                required: true
            },
            serie: {
                required: true
            },
            unidad: {
                required: true
            },
            fecha_entrega: {
                required: true
            },
        },
        errorPlacement: function(error, element) {
            if (element.parent().hasClass("vd_checkbox") || element.parent().hasClass("vd_radio")){
                element.parent().append(error);
            } else if (element.parent().hasClass("vd_input-wrapper")){
                error.insertAfter(element.parent());
            }else {
                error.insertAfter(element);
            }
        }, 
        
        invalidHandler: function (event, validator) { //display error alert on form submit              
                success_register.fadeOut(500);
                error_register.fadeIn(500);
                scrollTo(form_register,-100);

        },

        highlight: function (element) { // hightlight error inputs
    
            $(element).addClass('vd_bd-red');
            $(element).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');

        },

        unhighlight: function (element) { // revert the change dony by hightlight
            $(element)
                .closest('.control-group').removeClass('error'); // set error class to the control group
        },

        success: function (label, element) {
            label
                .addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
                .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
            $(element).removeClass('vd_bd-red');
        }
    });
    var $valid = $("#form-configuracion").valid();
    if($valid) {
        var vali_tabla=0;
        var DATAconf  = [];
        var TABLAconf   = $(".addsubconfiguracion .addsubconfiguracion_body > .tab-pane");
        TABLAconf.each(function(){         
            item = {};
            item ["ordenIdd"] = $(this).find("input[id*='ordenIdd']").val();
            item ["configId"]   = $(this).find("select[id*='configId'] option:selected").val();
           //=======================================
            var DATAconf_d  = [];
            var TABLAconf_d = $(this).find('table tbody > tr');
            TABLAconf_d.each(function(){ 
                vali_tabla=1;
                item_d = {};
                item_d ["ordenIddd"] = $(this).find("input[id*='ordenIddd']").val();
                item_d ["pieza"] = $(this).find("input[id*='piezas']").val();
                item_d ["configdId"]   = $(this).find("input[id*='configdId']").val();
                item_d ["sucursalId"]   = $(this).find("input[id*='sucursalId']").val();
                DATAconf_d.push(item_d);
            });
        //=======================================
            item ["productos"]   = DATAconf_d;
            DATAconf.push(item);
        });
        if(vali_tabla==1){ 
            INFO  = new FormData();
            arrayconfiguracion   = JSON.stringify(DATAconf);
            console.log(arrayconfiguracion);
            console.log($.parseJSON(arrayconfiguracion)); 
            $('.button_save').attr('disabled',true);
            var datos = $('#form-configuracion').serialize()+'&arrayconfiguracion='+arrayconfiguracion;
            $.ajax({
                type:'POST',
                url: base_url+"index.php/Salida/orden_m_insertupdate",
                data: datos,
                statusCode: {
                    404: function(data) {
                        toastr.error('Error', '404');
                    },
                    500: function(data) {
                        console.log(data);
                        toastr.error('Error', 'Algo salió mal, intente de nuevo o contacte al administrador del sistema');
                    }
                },
                success: function (response){
                    toastr.success( 'Configuración agregado','Hecho!');
                    setTimeout(function(){ 
                        window.location.href = base_url+"index.php/Salida/orden_m"; 
                    }, 2000);
                },
                error: function(response){
                    toastr.error('Error', 'Algo salió mal, intente de nuevo o contacte al administrador del sistema');
                }
            });
        }else{
            toastr.error('No tienes ninguna subconfiguracion agregada', 'Atención!');
        }
    }
}

function obtenerdatosproducto(idp){
    $.ajax({
        type:'POST',
        url: base_url+"index.php/Salida/obtenerdatosproducto",
        data: {id:idp},
        success: function (data){
            var array = $.parseJSON(data);
            array.config_d.forEach(function(element) {
                subconfig_data(element.ordenIdd,element.nombresub,element.configId);
            });
        },
        error: function(data){ 
            toastr.error('Error', 'Algo salió mal, intente de nuevo o contacte al administrador del sistema');
        }
    });
}

function subconfig_data_row(conrow,id){
    $.ajax({
        type:'POST',
        url: base_url+"index.php/Salida/config_d_d",
        data: {id:id},
        success: function (data){
            var array = $.parseJSON(data);
            array.forEach(function(element) {
                add_config(conrow,element.ordenIddd,element.pieza,element.configdId,element.nombresub,element.sucursal,element.sucursalId);
            });
        },
        error: function(data){ 
            toastr.error('Error', 'Algo salió mal, intente de nuevo o contacte al administrador del sistema');
        }
    });
}