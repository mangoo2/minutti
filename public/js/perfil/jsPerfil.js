$(document).ready(function(){
	
	$('#cmbPerfiles').change(function(){
		var id = $('#cmbPerfiles option:selected').val();
		$.ajax({
			type:'POST',
			url:'Perfil/submenusselect',
            data: {id:id},
			async:false,
			success:function(data){
				$('#list').html(data);
			}
		});
	});
	$('.rowlink').click(function(){
		$('li[class = rowlink]').each(function(){
			$(this).attr('style','cursor: pointer; border-bottom:1px solid #EEE');
		});
		var id = $(this).attr('data-id');
		$(this).attr('style','cursor: pointer; background-color:#EEE; border-radius:4px;');
		$('#txtIdMenu').val(id);
	});
	$('#add').click(function(){
		var menu = $('#txtIdMenu').val();
		var perfil = $('#cmbPerfiles option:selected').val();
		$.ajax({
			type:'POST',
			url:'Perfil/addsubmenus',
			data:{menu:menu,perfil:perfil},
			async:false,
			success:function(data){
				if(data == 'Seleccione un perfil' || data=='Seleccione un menu' || data=="El menu seleccionado ya se encuentra asígnado a este perfil"){
					notification("topright","notify","fa fa-exclamation-triangle vd_yellow","Notificaci&oacute;n",data);
				}
				else{
					$('#list').html(data);
                    notification("topright","success","fa fa-exclamation-triangle vd_yellow","¡Hecho!","Se agrego correctamente el modulo al perfil");

				}
			}
		});
	});
	
	$('#remove').click(function(){
		var menu = $('#txtRemove').val();
		var perfil = $('#cmbPerfiles option:selected').val();
		$.ajax({
			type:'POST',
			url:'Perfil/deletesubmenus',
			data:{menu:menu,perfil:perfil},
			async:false,
			success:function(data){
				console.log(data);
				if(data == 'Seleccione un perfil' || data=='Seleccione un menu' || data=="El menu seleccionado ya se encuentra asígnado a este perfil"){
					$('#txtRemove').val('');
					notification("topright","notify","fa fa-exclamation-triangle vd_yellow","Notificaci&oacute;n",data);
				}
				else{
					$('#list').html(data);
					notification("topright","success","fa fa-exclamation-triangle vd_yellow","¡Hecho!","Se removio correctamente el modulo al perfil");
				}
			}
		});
	});
	/*
	$('#new').click(function(){
		var params = {};
		params.action = 'new';
		params.perfil = $('#txtPerfil').val();
		params.id = $('#cmbPerfiles option:selected').val();
		if(params.perfil!=''){
			$.ajax({
				type:'POST',
				url:'../AJX/ajax_perfil.php',
				data:params,
				async:false,
				success:function(data){
					$('#txtPerfil').val('');
					$('#cmbPerfiles').html(data);
				}
			});
		}
		else{
			notification("topright","notify","fa fa-exclamation-triangle vd_yellow","Notificaci&oacute;n","El campo 'Nombre' no puede estar vacio");
		}
	});
	*/
});

function remover(id){
		$('li[class = linkrow]').each(function(){
			$(this).attr('style','cursor: pointer; border-bottom:1px solid #EEE');
		});
		$(this).attr('style','cursor: pointer; background-color:#EEE; border-radius:4px;');
		$('#txtRemove').val(id);
		console.log(id);
}
