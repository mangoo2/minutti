var base_url = $('#base_url').val();
$(document).ready(function () {
    //setTimeout(function(){ sucursalIdclass(); }, 1000);
    /*
        "use strict";   
        var form_register = $('#frmPersonal');
        var error_register = $('.alert-danger', form_register);
        var success_register = $('.alert-success', form_register);
        form_register.validate({
            errorElement: 'div', //default input error message container
            errorClass: 'vd_red', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            ignore: "",
            rules: {
                nombre: {
                    //minlength: 7,
                    required: true
                    //email: true
                },
                email:{
                    email: true
                }

               

            },
            errorPlacement: function(error, element) {
                        console.log('2');

                if (element.parent().hasClass("vd_checkbox") || element.parent().hasClass("vd_radio")){
                    element.parent().append(error);
                } else if (element.parent().hasClass("vd_input-wrapper")){
                    error.insertAfter(element.parent());
                }else {
                    error.insertAfter(element);
                }
            }, 
            
            invalidHandler: function (event, validator) { //display error alert on form submit   
                        console.log('3');
           
                    success_register.fadeOut(500);
                    error_register.fadeIn(500);
                    scrollTo(form_register,-100);

            },

            highlight: function (element) { // hightlight error inputs

                $(element).addClass('vd_bd-red');
                $(element).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');

            },

            unhighlight: function (element) { // revert the change dony by hightlight
                        console.log('5');

                $(element)
                    .closest('.control-group').removeClass('error'); // set error class to the control group
            },

            success: function (label, element) {
                        console.log('6');

                label
                    .addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
                    .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
                $(element).removeClass('vd_bd-red');
            },
            submitHandler: function (form) {

                var datosf=$('#frmPersonal').serialize();
                $.ajax({
                    type:'POST',
                    url: base_url+'Personal/addpersonal',
                    data: datosf,
                    async: false,
                    statusCode:{
                        404: function(data){
                            toastr.error('Error!', 'No Se encuentra el archivo');
                        },
                        500: function(){
                            toastr.error('Error', '500');
                        }
                    },
                    success:function(data){
                        console.log(data);
                        toastr.success('Hecho!', 'Guardado Correctamente');
                        setTimeout(function(){ 
                            location.href=base_url+'Personal';
                        }, 30000);
                        
                        //location.reload();
                        
                        
                    }
                });
                
                error1.hide();
            }
        });
    */
    $('#frmusuario').validate({
            errorElement: 'div', //default input error message container
            errorClass: 'vd_red', // default input error message class
        });
});

function guardar(){
        var form_register = $('#frmPersonal');
        var error_register = $('.alert-danger', form_register);
        var success_register = $('.alert-success', form_register);
        form_register.validate({
            errorElement: 'div', //default input error message container
            errorClass: 'vd_red', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            ignore: "",
            rules: {
                nombre: {
                    //minlength: 7,
                    required: true
                    //email: true
                },
                email:{
                    email: true
                },
                celular:{
                    minlength: 10,
                    maxlength: 10,
                }, 
                puesto:{
                    required: true
                },
                perfiles:{
                    required: true
                } 
            },
            errorPlacement: function(error, element) {
                if (element.parent().hasClass("vd_checkbox") || element.parent().hasClass("vd_radio")){
                    element.parent().append(error);
                } else if (element.parent().hasClass("vd_input-wrapper")){
                    error.insertAfter(element.parent());
                }else {
                    error.insertAfter(element);
                }
            }, 
            
            invalidHandler: function (event, validator) { //display error alert on form submit              
                    success_register.fadeOut(500);
                    error_register.fadeIn(500);
                    scrollTo(form_register,-100);

            },

            highlight: function (element) { // hightlight error inputs
        
                $(element).addClass('vd_bd-red');
                $(element).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');

            },

            unhighlight: function (element) { // revert the change dony by hightlight
                $(element)
                    .closest('.control-group').removeClass('error'); // set error class to the control group
            },

            success: function (label, element) {
                label
                    .addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
                    .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
                $(element).removeClass('vd_bd-red');
            }
        });
        var $valid = $("#frmPersonal").valid();
        var validu = $("#frmusuario").valid();
        if($valid && validu) {
            var datos = form_register.serialize()+'&'+$("#frmusuario").serialize();
            $('.btn_registro').attr('disabled',true);
            $.ajax({
                type:'POST',
                url: base_url+'Personal/addpersonal',
                data: datos,
                statusCode:{
                    404: function(data){
                        swal("Error!", "Error 404", "error");
                    },
                    500: function(){
                        swal("Error!", "Algo salió mal, intente de nuevo o contacte al administrador del sistema", "error");
                    }
                },
                success:function(data){
                    /*swal({
                        title: 'Éxito!',
                        text: 'Guardado correctamente',
                        type: 'success',
                        showCancelButton: false,
                        allowOutsideClick: false,
                    }).then(function(isConfirm) {
                        if (isConfirm) {
                            window.location.href = base_url+'Personal'; 
                        }
                    }).catch(swal.noop);*/

                    
                    swal("Éxito!", "Guardado correctamente", "success");
                    setTimeout(function(){ 
                        location.href=base_url+'Personal';
                    }, 3000);
                
                }
            });

        }

}

function personaldelete(id){
    $.ajax({
            type:'POST',
            url: 'Personal/deletepersonal',
            data: {id:id},
            async: false,
            statusCode:{
                404: function(data){
                    toastr.error('Error!', 'No Se encuentra el archivo');
                },
                500: function(){
                    toastr.error('Error', '500');
                }
            },
            success:function(data){
                console.log(data);
                //location.reload();
                toastr.success('Hecho!', 'Se Elimino correctamente');
                var row = document.getElementById('trper_'+id);
                            row.parentNode.removeChild(row);
                
            }
        });
}
/*
function sucursalIdclass(){
    var perfiles = $('#perfiles option:selected').val();
    if(perfiles==3){
        $('.sucursalIdclass').show('show');
    }else{
        $('.sucursalIdclass').hide('show');
    }
}
*/