var base_url = $('#base_url').val();
var id_unidad=0;
var id_categoria=0;
var id_configuracion=0;
var tabla_unidad;
var tabla_configuracion;
var id_producto=0;


var id_marca=0;
$(document).ready(function($) {
});

function save(){
    var form_register = $('#form-producto');
    var error_register = $('.alert-danger', form_register);
    var success_register = $('.alert-success', form_register);
    form_register.validate({
        errorElement: 'div', //default input error message container
        errorClass: 'vd_red', // default input error message class
        focusInvalid: false, // do not focus the last invalid input
        ignore: "",
        rules: {
            codigo: {
                required: true,
            },
            categoriaId: {
                required: true
            },
            marcaId: {
                required: true
            },
            cantidad_minima_existencia: {
                required: true
            },
            cantidad_maxima_existencia: {
                required: true
            },
            stock_disponible: {
                required: true
            },
            precio_venta: {
                required: true
            },
            costo_compra: {
                required: true
            }
        },
        errorPlacement: function(error, element) {
            if (element.parent().hasClass("vd_checkbox") || element.parent().hasClass("vd_radio")){
                element.parent().append(error);
            } else if (element.parent().hasClass("vd_input-wrapper")){
                error.insertAfter(element.parent());
            }else {
                error.insertAfter(element);
            }
        }, 
            
        invalidHandler: function (event, validator) { //display error alert on form submit              
            success_register.fadeOut(500);
            error_register.fadeIn(500);
            scrollTo(form_register,-100);
        },

        highlight: function (element) { // hightlight error inputs
            $(element).addClass('vd_bd-red');
            $(element).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');
        },

        unhighlight: function (element) { // revert the change dony by hightlight
            $(element)
                .closest('.control-group').removeClass('error'); // set error class to the control group
            },

            success: function (label, element) {
                label
                    .addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
                    .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
                $(element).removeClass('vd_bd-red');
            }
        });
        
        
        var $valid = $("#form-producto").valid();
        if($valid) {
            var datos = $('#form-producto').serialize()+'&preciocompra='+$('#preciocompra').val();//+'&arraystock='+arraystock;
            $('.btn_registro').attr('disabled',true);

        var codigo = $('#codigo').val();
        var num_serie = $('#num_serie').val();  
        $.ajax({
            type:'POST',
            url: base_url+"index.php/Productos/validar_codigo_numero_serie",
            data: {codigo:codigo,num_serie:num_serie,id_producto:id_producto},
            statusCode: {
                404: function(data) {
                    swal("Error!", "Error 404", "error");
                },
                500: function(data) {
                    console.log(data);
                    swal("Error!", "Algo salió mal, intente de nuevo o contacte al administrador del sistema", "error");
                }
            },
            success: function (response){
                if(response==1){
                    swal("!Atención¡", "El código o el número de serie ya existe en otro producto", "warning");
                    //toastr.error('El código o el número de serie ya existe en otro producto','!Atención¡'); 
                }else{
                    $( ".button_save" ).prop( "disabled", true );
                    setTimeout(function(){ 
                        $(".button_save" ).prop( "disabled", false );
                    }, 5000);
                    /*
                    var DATAr  = [];
                    var TABLA   = $("#table-stock-sucursales tbody > tr");
                        TABLA.each(function(){         
                        item = {};
                        item ["productosId"] = $(this).find("input[id*='productosId']").val();
                        item ["sucursalId"]   = $(this).find("input[id*='sucursalId']").val();
                        item ["stock"]  = $(this).find("input[id*='stock']").val();
                        item ["pertenece"]  = $(this).find("input[id*='pertenece']").is(':checked')==true?1:0;
                        DATAr.push(item);
                    });
                    INFO  = new FormData();
                    arraystock   = JSON.stringify(DATAr);
                    */
                    
                    //var datos = $('#form-producto').serialize()+'&preciocompra='+$('#preciocompra').val();//+'&arraystock='+arraystock;

                    console.log(datos);
                    $.ajax({
                        type:'POST',
                        url: base_url+"index.php/Productos/insertupdate",
                        data: datos,
                        statusCode: {
                            404: function(data) {
                                swal("Error!", "Error 404", "error");
                            },
                            500: function(data) {
                                console.log(data);
                                swal("Error!", "Algo salió mal, intente de nuevo o contacte al administrador del sistema", "error");
                            }
                        },
                        success: function (response){
                            swal("Éxito!", "Guardado correctamente", "success");
                            
                            setTimeout(function(){ 
                                window.location.href = base_url+"index.php/Productos"; 
                            }, 2000);
                        
                        }
                    });
                }
            
            }
        });
    }
}


function totalstock() {
	var addtp = 0;
	$(".totalstock").each(function() {
        var vstotal = $(this).val();
        addtp += Number(vstotal);
    });
    $('.totalstockt').html(addtp);
}
function obtenerdatosproducto(id){
	$.ajax({
        type:'POST',
        url: base_url+"index.php/Productos/obtenerdatosproducto",
        data: {
        	id:id
        },
        statusCode: {
            404: function(data) {
                toastr.error('Error', '404');
            },
            500: function(data) {
            	console.log(data);
                toastr.error('Error', 'Algo salió mal, intente de nuevo o contacte al administrador del sistema');
            }
        },
        success: function (data){
           var array = $.parseJSON(data);
           if(array.productoId>0){
                id_producto=array.productoId;
           		$('#productoId').val(array.productoId);
				$('#codigo').val(array.codigo);
				$('#nombre').val(array.nombre);
				$('#descripcion').val(array.descripcion);
				$('#unidadId').val(array.unidadId);
				$('#categoriaId').val(array.categoriaId);
                $('#marcaId').val(array.marcaId);
                $('#configuracion').val(array.configuracion);
                $('#preciocompra').val(array.preciocompra);
                $('#cantidad_minima_existencia').val(array.cantidad_minima_existencia);
                $('#cantidad_maxima_existencia').val(array.cantidad_maxima_existencia);
                $('#stock_disponible').val(array.stock_disponible);
                $('#num_serie').val(array.num_serie);    
                $('#precio_venta').val(array.precio_venta);   
                $('#costo_compra').val(array.costo_compra);   
				array.productosucursal.forEach(function(element) {
					$('.productosId_'+element.sucursalId).val(element.productosId);
					$('.stock_'+element.sucursalId).val(element.stock);
                    if(element.pertenece==1){
                       $('.pertenece_'+element.sucursalId).attr('checked',true);
                    }
				});
                //tabla_ultimo_costo(array.productoId);
           }
           totalstock();
        }
    });
}


/*function tabla_ultimo_costo(id){
    $.ajax({
        type:'POST',
        url: base_url+'Productos/get_detalle_ultimo_costo',
        data: {id:id},
        statusCode:{
            404: function(data){
                toastr.error('Error!', 'No Se encuentra el archivo');
            },
            500: function(){
                toastr.error('Error', '500');
            }
        },
        success:function(data){
            $('.table_ultimo_costo').html(data);
        }
    });
}*/

function generacodigo(){
    var d = new Date();
    var dia=d.getDate();//1 31
    var dias=d.getDay();//0 6
    var mes = d.getMonth();//0 11
    var yy = d.getFullYear();//9999
    var hr = d.getHours();//0 24
    var min = d.getMinutes();//0 59
    var seg = d.getSeconds();//0 59
    var yyy = 18;
    var ram = Math.floor((Math.random() * 99) + 1);
    var codigo=seg+''+min+''+hr+''+yyy+''+ram+''+mes+''+dia+''+dias;
    //var condigo0=condigo.substr(0,13);
    //console.log(codigo);
    //if(tipo==1){
        $('#codigo').val(codigo);    
    
    
    
}
/// Unidad
function modal_unidad(){
    $('#modal_unidad').modal();
    reload_unidad();
    $('#idunidad_unidad').val(0);
    $('#nombre_unidad').val('');
    $('.vd_red').remove();
}

function add_unidad(){
    var form_register = $('#form_unidad');
    var error_register = $('.alert-danger', form_register);
    var success_register = $('.alert-success', form_register);
    form_register.validate({
        errorElement: 'div', //default input error message container
        errorClass: 'vd_red', // default input error message class
        focusInvalid: false, // do not focus the last invalid input
        ignore: "",
        rules: {
            unidad: {
                required: true
            }
        },
        errorPlacement: function(error, element) {
            if (element.parent().hasClass("vd_checkbox") || element.parent().hasClass("vd_radio")){
                element.parent().append(error);
            } else if (element.parent().hasClass("vd_input-wrapper")){
                error.insertAfter(element.parent());
            }else {
                error.insertAfter(element);
            }
        }, 
        
        invalidHandler: function (event, validator) { //display error alert on form submit              
                success_register.fadeOut(500);
                error_register.fadeIn(500);
                scrollTo(form_register,-100);

        },

        highlight: function (element) { // hightlight error inputs
    
            $(element).addClass('vd_bd-red');
            $(element).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');

        },

        unhighlight: function (element) { // revert the change dony by hightlight
            $(element)
                .closest('.control-group').removeClass('error'); // set error class to the control group
        },

        success: function (label, element) {
            label
                .addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
                .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
            $(element).removeClass('vd_bd-red');
        }
    });
    var $valid = $("#form_unidad").valid();
    if($valid) {
        var datos = form_register.serialize();
        $('.btn_unidad').attr('disabled',true);
        $.ajax({
            type:'POST',
            url: base_url+'Productos/addunidad',
            data: datos,
            statusCode:{
                404: function(data){
                    toastr.error('Error!', 'No Se encuentra el archivo');
                },
                500: function(){
                    toastr.error('Error', '500');
                }
            },
            success:function(data){
                toastr.success('Hecho!', 'Guardado Correctamente');
                setTimeout(function(){ 
                    $('.btn_unidad').attr('disabled',false);
                    $('#idunidad_unidad').val(0);
                    $('#nombre_unidad').val('');
                    $('.vd_red').remove();
                }, 2000);
                var id=data;
                if(id_unidad==1){
                    $("#unidadId option[value='"+id+"']").attr("selected", true);
                    $("#unidadId option[value='"+id+"']").text($('#nombre_unidad').val());
                }else{
                    $('#unidadId').prepend("<option value='"+id+"' selected>"+$('#nombre_unidad').val()+"</option>");    
                }
                reload_unidad();
            }
        });
    }
}

function reload_unidad(){
    tabla_unidad=$("#table_unidad").DataTable();
    tabla_unidad.destroy();
    table_unidads();
}
function table_unidads(){
    tabla_unidad=$("#table_unidad").DataTable({
        "bProcessing": true,
        "serverSide": true,
        "searching": false,
        responsive: !0,
        //"info":     false,
        //"paging": false,
        "ajax": {
            "url": base_url+"Productos/getlistado_unidad",
            type: "post",
            error: function(){
               $("#table_unidad").css("display","none");
            }
        },
        "columns": [
            {"data":"unidadId"},
            {"data":"unidad"},
            {"data": null,
                "render": function ( data, type, row, meta ){
                var html='<button type="button"\
                            data-idunidad="'+row.unidadId+'"\
                            data-unidad="'+row.unidad+'"\
                            class="btn btn-flat btn-danger unidac_'+row.unidadId+'" onclick="editar_unidad('+row.unidadId+')"><i class="fa fa-edit icon_font"></i></button>\
                            <button type="button" class="btn btn-flat btn-danger" onclick="delete_registro_unidad('+row.unidadId+');"><i class="fa fa-trash-o icon_font"></i> </button>';
                return html;
                }
            },
        ],
        "order": [[ 0, "desc" ]],
        "lengthMenu": [[5, 10, 25], [5, 10, 25]],
        // Cambiamos lo principal a Español
        "language": {
            "lengthMenu": "Mostrar _MENU_ entradas",
            "zeroRecords": "Lo sentimos - No se han encontrado elementos",
            "sInfo":  "Mostrando registros del _START_ al _END_ de un total de _TOTAL_  registros",
            "info": "Mostrando página _PAGE_ de _PAGES_",
            "infoEmpty": "No hay registros disponibles",
            "infoFiltered": "(Filtrado de _MAX_ registros totales)",
            "search": "Buscar : _INPUT_",
            "paginate": {
                "previous": "Página previa",
                "next": "Siguiente página"
              }
        }
    
    });
}
function editar_unidad(id){
    id_unidad=1;
    $('#idunidad_unidad').val($('.unidac_'+id).data('idunidad'));
    $('#nombre_unidad').val($('.unidac_'+id).data('unidad'));
}

function delete_registro_unidad(id){
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: '¿Está seguro de Eliminar esta unidad?',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                $.ajax({
                    type:'POST',
                    url: base_url+"index.php/Productos/delete_unidad",
                    data: {
                        unidadId:id
                    },
                    statusCode: {
                        404: function(data) {
                            toastr.error('404','Error');
                        },
                        500: function(data) {
                            console.log(data);
                            toastr.error('Algo salió mal, intente de nuevo o contacte al administrador del sistema','Error');
                        }
                    },
                    success:function(response){  
                        toastr.success( 'Eliminado correctamente','Hecho!');
                        reload_unidad();
                        $('#unidadId option[value="'+id+'"]').remove();
                    }
                });
            },
            cancelar: function () 
            {
                
            }
        }
    });
}

/// Categorias
function modal_categoria(){
    $('#modal_categoria').modal();
    reload_categoria();
    $('#idcategoria_categoria').val(0);
    $('#nombre_categoria').val('');
    $('.vd_red').remove();
}

function add_categoria(){
    var form_register = $('#form_categoria');
    var error_register = $('.alert-danger', form_register);
    var success_register = $('.alert-success', form_register);
    form_register.validate({
        errorElement: 'div', //default input error message container
        errorClass: 'vd_red', // default input error message class
        focusInvalid: false, // do not focus the last invalid input
        ignore: "",
        rules: {
            categoria: {
                required: true
            }
        },
        errorPlacement: function(error, element) {
            if (element.parent().hasClass("vd_checkbox") || element.parent().hasClass("vd_radio")){
                element.parent().append(error);
            } else if (element.parent().hasClass("vd_input-wrapper")){
                error.insertAfter(element.parent());
            }else {
                error.insertAfter(element);
            }
        }, 
        
        invalidHandler: function (event, validator) { //display error alert on form submit              
                success_register.fadeOut(500);
                error_register.fadeIn(500);
                scrollTo(form_register,-100);

        },

        highlight: function (element) { // hightlight error inputs
    
            $(element).addClass('vd_bd-red');
            $(element).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');

        },

        unhighlight: function (element) { // revert the change dony by hightlight
            $(element)
                .closest('.control-group').removeClass('error'); // set error class to the control group
        },

        success: function (label, element) {
            label
                .addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
                .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
            $(element).removeClass('vd_bd-red');
        }
    });
    var $valid = $("#form_categoria").valid();
    if($valid) {
        var datos = form_register.serialize();
        $('.btn_categoria').attr('disabled',true);
        $.ajax({
            type:'POST',
            url: base_url+'Productos/addcategoria',
            data: datos,
            statusCode:{
                404: function(data){
                    toastr.error('Error!', 'No Se encuentra el archivo');
                },
                500: function(){
                    toastr.error('Error', '500');
                }
            },
            success:function(data){
                toastr.success('Hecho!', 'Guardado Correctamente');
                setTimeout(function(){ 
                    $('.btn_categoria').attr('disabled',false);
                    $('#idcategoria_categoria').val(0);
                    $('#nombre_categoria').val('');
                    $('.vd_red').remove();
                }, 2000);
                var id=data;
                if(id_categoria==1){
                    $("#categoriaId option[value='"+id+"']").attr("selected", true);
                    $("#categoriaId option[value='"+id+"']").text($('#nombre_categoria').val());
                }else{
                    $('#categoriaId').prepend("<option value='"+id+"' selected>"+$('#nombre_categoria').val()+"</option>");    
                }
                reload_categoria();
            }
        });
    }
}

function reload_categoria(){
    tabla_categoria=$("#table_categoria").DataTable();
    tabla_categoria.destroy();
    table_categorias();
}
function table_categorias(){
    tabla_categoria=$("#table_categoria").DataTable({
        "bProcessing": true,
        "serverSide": true,
        "searching": false,
        responsive: !0,
        //"info":     false,
        //"paging": false,
        "ajax": {
            "url": base_url+"Productos/getlistado_categoria",
            type: "post",
            error: function(){
               $("#table_categoria").css("display","none");
            }
        },
        "columns": [
            {"data":"categoriaId"},
            {"data":"categoria"},
            {"data": null,
                "render": function ( data, type, row, meta ){
                var html='<button type="button"\
                            data-idcategoria="'+row.categoriaId+'"\
                            data-categoria="'+row.categoria+'"\
                            class="btn btn-flat btn-danger catec_'+row.categoriaId+'" onclick="editar_categoria('+row.categoriaId+')"><i class="fa fa-edit icon_font"></i></button>\
                            <button type="button" class="btn btn-flat btn-danger" onclick="delete_registro_categoria('+row.categoriaId+');"><i class="fa fa-trash-o icon_font"></i> </button>';
                return html;
                }
            },
        ],
        "order": [[ 0, "desc" ]],
        "lengthMenu": [[5, 10, 25], [5, 10, 25]],
        // Cambiamos lo principal a Español
        "language": {
            "lengthMenu": "Mostrar _MENU_ entradas",
            "zeroRecords": "Lo sentimos - No se han encontrado elementos",
            "sInfo":  "Mostrando registros del _START_ al _END_ de un total de _TOTAL_  registros",
            "info": "Mostrando página _PAGE_ de _PAGES_",
            "infoEmpty": "No hay registros disponibles",
            "infoFiltered": "(Filtrado de _MAX_ registros totales)",
            "search": "Buscar : _INPUT_",
            "paginate": {
                "previous": "Página previa",
                "next": "Siguiente página"
              }
        }
    
    });
}
function editar_categoria(id){
    id_categoria=1;
    $('#idcategoria_categoria').val($('.catec_'+id).data('idcategoria'));
    $('#nombre_categoria').val($('.catec_'+id).data('categoria'));
}

function delete_registro_categoria(id){
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: '¿Está seguro de Eliminar esta categoria?',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                $.ajax({
                    type:'POST',
                    url: base_url+"index.php/Productos/delete_categoria",
                    data: {
                        categoriaId:id
                    },
                    statusCode: {
                        404: function(data) {
                            toastr.error('404','Error');
                        },
                        500: function(data) {
                            console.log(data);
                            toastr.error('Algo salió mal, intente de nuevo o contacte al administrador del sistema','Error');
                        }
                    },
                    success:function(response){  
                        toastr.success( 'Eliminado correctamente','Hecho!');
                        reload_categoria();
                        $('#categoriaId option[value="'+id+'"]').remove();
                    }
                });
            },
            cancelar: function () 
            {
                
            }
        }
    });
}
///////////////Y
/// Configuracion
function modal_configuracion(){
    $('#modal_configuracion').modal();
    reload_configuracion();
    $('#idconfiguracion_configuracion').val(0);
    $('#nombre_configuracion').val('');
    $('.vd_red').remove();
}

function add_configuracion(){
    var form_register = $('#form_configuracion');
    var error_register = $('.alert-danger', form_register);
    var success_register = $('.alert-success', form_register);
    form_register.validate({
        errorElement: 'div', //default input error message container
        errorClass: 'vd_red', // default input error message class
        focusInvalid: false, // do not focus the last invalid input
        ignore: "",
        rules: {
            nombre: {
                required: true
            }
        },
        errorPlacement: function(error, element) {
            if (element.parent().hasClass("vd_checkbox") || element.parent().hasClass("vd_radio")){
                element.parent().append(error);
            } else if (element.parent().hasClass("vd_input-wrapper")){
                error.insertAfter(element.parent());
            }else {
                error.insertAfter(element);
            }
        }, 
        
        invalidHandler: function (event, validator) { //display error alert on form submit              
                success_register.fadeOut(500);
                error_register.fadeIn(500);
                scrollTo(form_register,-100);

        },

        highlight: function (element) { // hightlight error inputs
    
            $(element).addClass('vd_bd-red');
            $(element).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');

        },

        unhighlight: function (element) { // revert the change dony by hightlight
            $(element)
                .closest('.control-group').removeClass('error'); // set error class to the control group
        },

        success: function (label, element) {
            label
                .addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
                .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
            $(element).removeClass('vd_bd-red');
        }
    });
    var $valid = $("#form_configuracion").valid();
    if($valid) {
        var datos = form_register.serialize();
        $('.btn_configuracion').attr('disabled',true);
        $.ajax({
            type:'POST',
            url: base_url+'Productos/addconfig',
            data: datos,
            statusCode:{
                404: function(data){
                    toastr.error('Error!', 'No Se encuentra el archivo');
                },
                500: function(){
                    toastr.error('Error', '500');
                }
            },
            success:function(data){
                toastr.success('Hecho!', 'Guardado Correctamente');
                setTimeout(function(){ 
                    $('.btn_configuracion').attr('disabled',false);
                    $('#idconfig').val(0);
                    $('#nombre_config').val('');
                    $('.vd_red').remove();
                }, 2000);
                var id=data;
                if(id_configuracion==1){
                    $("#configuracion option[value='"+id+"']").attr("selected", true);
                    $("#configuracion option[value='"+id+"']").text($('#nombre_config').val());
                }else{
                    $('#configuracion').prepend("<option value='"+id+"' selected>"+$('#nombre_config').val()+"</option>");    
                }
                reload_configuracion();
            }
        });
    }
}

function reload_configuracion(){
    tabla_configuracion=$("#table_configuracion").DataTable();
    tabla_configuracion.destroy();
    table_configuracions();
}
function table_configuracions(){
    tabla_configuracion=$("#table_configuracion").DataTable({
        "bProcessing": true,
        "serverSide": true,
        "searching": false,
        responsive: !0,
        //"info":     false,
        //"paging": false,
        "ajax": {
            "url": base_url+"Productos/getlistado_config",
            type: "post",
            error: function(){
               $("#table_configuracion").css("display","none");
            }
        },
        "columns": [
            {"data":"id"},
            {"data":"nombre"},
            {"data": null,
                "render": function ( data, type, row, meta ){
                var html='<button type="button"\
                            data-idconfiguracion="'+row.id+'"\
                            data-configuracion="'+row.nombre+'"\
                            class="btn btn-flat btn-danger config_'+row.id+'" onclick="editar_configuracion('+row.id+')"><i class="fa fa-edit icon_font"></i></button>\
                            <button type="button" class="btn btn-flat btn-danger" onclick="delete_registro_configuracion('+row.id+');"><i class="fa fa-trash-o icon_font"></i> </button>';
                return html;
                }
            },
        ],
        "order": [[ 0, "desc" ]],
        "lengthMenu": [[5, 10, 25], [5, 10, 25]],
        // Cambiamos lo principal a Español
        "language": {
            "lengthMenu": "Mostrar _MENU_ entradas",
            "zeroRecords": "Lo sentimos - No se han encontrado elementos",
            "sInfo":  "Mostrando registros del _START_ al _END_ de un total de _TOTAL_  registros",
            "info": "Mostrando página _PAGE_ de _PAGES_",
            "infoEmpty": "No hay registros disponibles",
            "infoFiltered": "(Filtrado de _MAX_ registros totales)",
            "search": "Buscar : _INPUT_",
            "paginate": {
                "previous": "Página previa",
                "next": "Siguiente página"
              }
        }
    
    });
}
function editar_configuracion(id){
    id_configuracion=1;
    $('#idconfig').val($('.config_'+id).data('idconfiguracion'));
    $('#nombre_config').val($('.config_'+id).data('configuracion'));
}

function delete_registro_configuracion(id){
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: '¿Está seguro de Eliminar esta configuración?',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                $.ajax({
                    type:'POST',
                    url: base_url+"index.php/Productos/delete_config",
                    data: {
                        id:id
                    },
                    statusCode: {
                        404: function(data) {
                            toastr.error('404','Error');
                        },
                        500: function(data) {
                            console.log(data);
                            toastr.error('Algo salió mal, intente de nuevo o contacte al administrador del sistema','Error');
                        }
                    },
                    success:function(response){  
                        toastr.success( 'Eliminado correctamente','Hecho!');
                        reload_configuracion();
                        $('#configuracion option[value="'+id+'"]').remove();
                    }
                });
            },
            cancelar: function () 
            {
                
            }
        }
    });
}



//// Marca
function modal_marca(){
    $('#modal_marca').modal();
    reload_marca();
    $('#idcategoria_categoria').val(0);
    $('#nombre_categoria').val('');
    $('.vd_red').remove();
}

function add_marca(){
    var form_register = $('#form_marca');
    var error_register = $('.alert-danger', form_register);
    var success_register = $('.alert-success', form_register);
    form_register.validate({
        errorElement: 'div', //default input error message container
        errorClass: 'vd_red', // default input error message class
        focusInvalid: false, // do not focus the last invalid input
        ignore: "",
        rules: {
            marca: {
                required: true
            }
        },
        errorPlacement: function(error, element) {
            if (element.parent().hasClass("vd_checkbox") || element.parent().hasClass("vd_radio")){
                element.parent().append(error);
            } else if (element.parent().hasClass("vd_input-wrapper")){
                error.insertAfter(element.parent());
            }else {
                error.insertAfter(element);
            }
        }, 
        
        invalidHandler: function (event, validator) { //display error alert on form submit              
                success_register.fadeOut(500);
                error_register.fadeIn(500);
                scrollTo(form_register,-100);

        },

        highlight: function (element) { // hightlight error inputs
    
            $(element).addClass('vd_bd-red');
            $(element).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');

        },

        unhighlight: function (element) { // revert the change dony by hightlight
            $(element)
                .closest('.control-group').removeClass('error'); // set error class to the control group
        },

        success: function (label, element) {
            label
                .addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
                .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
            $(element).removeClass('vd_bd-red');
        }
    });
    var $valid = $("#form_marca").valid();
    if($valid) {
        var datos = form_register.serialize();
        $('.btn_marca').attr('disabled',true);
        $.ajax({
            type:'POST',
            url: base_url+'Productos/addmarca',
            data: datos,
            statusCode:{
                404: function(data){
                    toastr.error('Error!', 'No Se encuentra el archivo');
                },
                500: function(){
                    toastr.error('Error', '500');
                }
            },
            success:function(data){
                toastr.success('Hecho!', 'Guardado Correctamente');
                setTimeout(function(){ 
                    $('.btn_marca').attr('disabled',false);
                    $('#idmarca_marca').val(0);
                    $('#nombre_marca').val('');
                    $('.vd_red').remove();
                }, 2000);
                var id=data;
                if(id_marca==1){
                    $("#marcaId option[value='"+id+"']").attr("selected", true);
                    $("#marcaId option[value='"+id+"']").text($('#nombre_marca').val());
                }else{
                    $('#marcaId').prepend("<option value='"+id+"' selected>"+$('#nombre_marca').val()+"</option>");    
                }
                reload_marca();
            }
        });
    }
}

function reload_marca(){
    tabla_marca = $("#table_marca").DataTable();
    tabla_marca.destroy();
    table_marcas();
}

function table_marcas(){
    tabla_marca=$("#table_marca").DataTable({
        "bProcessing": true,
        "serverSide": true,
        "searching": false,
        responsive: !0,
        //"info":     false,
        //"paging": false,
        "ajax": {
            "url": base_url+"Productos/getlistado_marca",
            type: "post",
            error: function(){
                $("#table_marca").css("display","none");
            }
        },
        "columns": [
            {"data":"marcaId"},
            {"data":"marca"},
            {"data": null,
                "render": function ( data, type, row, meta ){
                var html='<button type="button"\
                            data-idmarca="'+row.marcaId+'"\
                            data-marca="'+row.marca+'"\
                            class="btn btn-flat btn-danger marc_'+row.marcaId+'" onclick="editar_marca('+row.marcaId+')"><i class="fa fa-edit icon_font"></i></button>\
                            <button type="button" class="btn btn-flat btn-danger" onclick="delete_registro_marca('+row.marcaId+');"><i class="fa fa-trash-o icon_font"></i> </button>';
                return html;
                }
            },
        ],
        "order": [[ 0, "desc" ]],
        "lengthMenu": [[5, 10, 25], [5, 10, 25]],
        // Cambiamos lo principal a Español
        "language": {
            "lengthMenu": "Mostrar _MENU_ entradas",
            "zeroRecords": "Lo sentimos - No se han encontrado elementos",
            "sInfo":  "Mostrando registros del _START_ al _END_ de un total de _TOTAL_  registros",
            "info": "Mostrando página _PAGE_ de _PAGES_",
            "infoEmpty": "No hay registros disponibles",
            "infoFiltered": "(Filtrado de _MAX_ registros totales)",
            "search": "Buscar : _INPUT_",
            "paginate": {
                "previous": "Página previa",
                "next": "Siguiente página"
            }
        }
    
    });
}


function editar_marca(id){
    id_marca=1;
    $('#idmarca_marca').val($('.marc_'+id).data('idmarca'));
    $('#nombre_marca').val($('.marc_'+id).data('marca'));
}

function delete_registro_marca(id){
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: '¿Está seguro de Eliminar esta marca?',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                $.ajax({
                    type:'POST',
                    url: base_url+"index.php/Productos/delete_marca",
                    data: {
                        marcaId:id
                    },
                    statusCode: {
                        404: function(data) {
                            toastr.error('404','Error');
                        },
                        500: function(data) {
                            console.log(data);
                            toastr.error('Algo salió mal, intente de nuevo o contacte al administrador del sistema','Error');
                        }
                    },
                    success:function(response){  
                        toastr.success( 'Eliminado correctamente','Hecho!');
                        reload_marca();
                        $('#marcaId option[value="'+id+'"]').remove();
                    }
                });
            },
            cancelar: function () 
            {
                
            }
        }
    });
}