var base_url = $('#base_url').val();
var table;
$(document).ready(function($) {
	table=$('#table-lis').DataTable();
	loadtable();

    $('#imprimir_barcode').click(function(event) {
        var idp=$('#idproetiqueta').val();
        var nump=$('#numprint').val();
        $('#iframe_barcode').html("<iframe src=" +base_url +"Barcode?id="+ idp +"&page="+nump+"&print=true> </iframe>");
      });
});


function loadtable(){
    var categoria=$('#categoriaselect option:selected').val();
    var sucursal=$('#sucursalselect option:selected').val();

	table.destroy();
	table = $('#table-lis').DataTable({
        rowCallback:function(row,data){
            var stock=parseFloat(data.stock);
            var existencia=parseFloat(data.cantidad_minima_existencia);
            if(stock>existencia){
                $($(row).find("td")[4]).css("background-color","green"); 
            }else if(stock>=1 && stock<=existencia){
                $($(row).find("td")[4]).css("background-color","orange"); 
            }else if(stock<=0){
                $($(row).find("td")[4]).css("background-color","red"); 
            }
        },
        "bProcessing": true,
        "serverSide": true,
        "searching": true,
        responsive: !0,
        "ajax": {
            "url": base_url+"index.php/Productos/getlistproductos",
            type: "post",
            "data": {
                'categoria':categoria,
                'sucursal':sucursal
            },
        },
        "columns": [
            {"data": "productoId"},
            {"data": "codigo"},
            {"data": "nombre"},
            {"data": "categoria"},
            {"data": "stock"},
           
            {"data": null,
                render:function(data,type,row){
                    var html='';
                    	html+='<div style="min-width:252px;">';
                        html+='<a href="'+base_url+'Productos/add/'+row.productoId+'" class="btn btn-flat btn-danger"><i class="fa fa-edit icon_font"></i></a>';
                        //html+='<a title="Existencias" onclick="existencias('+row.productoId+')" class="btn btn-flat btn-danger"><i class="fa fa-dropbox icon_font"></i></a>';
                        //html+='<a onclick="traspaso('+row.productoId+')" class="btn btn-flat btn-danger"><i class="fa fa-exchange icon_font"></i></a>';
                        //html+='<a onclick="bitacora('+row.productoId+')" class="btn btn-flat btn-danger"><i class="fa fa-th-list icon_font"></i></a>';
                        
                        html+='<a onclick="barcode('+row.productoId+')" class="btn btn-flat btn-danger"><i class="fa fa-barcode icon_font"></i></a>';

                        html+='<a onclick="eliminarpro('+row.productoId+')" class="btn btn-flat btn-danger"><i class="fa fa-trash-o icon_font"></i></a>';
                        /*
                    	html+='<a href="'+base_url+'Productos/add/'+row.productoId+'" type="button" class="btn btn-raised btn-icon btn-secondary mr-1"><i class="fa fa-edit"></i></a> ';
                    	html+='<button type="button" class="btn btn-raised btn-icon btn-secondary mr-1" title="Existencias" onclick="existencias('+row.productoId+')"><i class="fa fa-dropbox"></i></button> ';
                    	html+='<button type="button" class="btn btn-raised btn-icon btn-secondary mr-1" onclick="traspaso('+row.productoId+')"><i class="fa fa-exchange"></i></button> ';
                    	html+='<button type="button" class="btn btn-raised btn-icon btn-secondary mr-1" onclick="bitacora('+row.productoId+')"><i class="fa fa-th-list"></i></button> ';
                    	html+='<button type="button" class="btn btn-raised btn-icon btn-secondary mr-1" onclick="eliminarpro('+row.productoId+')"><i class="fa fa-trash-o"></i></button> ';
                    	*/
                        html+='</div>';
                    return html;
                }
            },
            
        ],
        "order": [[ 0, "desc" ]],
        "lengthMenu": [[25, 50, 100], [25, 50,100]],
    });
}
function existencias(id){
	$('#modal_existencias').modal();
	obtenerdatosproducto(id);
}
var productotraspaso=0;
function traspaso(id){
    productotraspaso=id;
    $('#modal_traspaso').modal();
    obtenerdatosproducto(id);
    $('#cantidad_traspaso').val('');
    $('#sucursalsalida').prop('selectedIndex',0);
    $('#sucursalentrada').prop('selectedIndex',0);
    $("#sucursalentrada option:selected").attr('disabled','disabled') .siblings().removeAttr('disabled');
}
function totalstock() {
	var addtp = 0;
	$(".totalstock").each(function() {
        var vstotal = $(this).html();
        addtp += Number(vstotal);
    });
    $('.totalstockt').html(addtp);
}
function obtenerdatosproducto(id){
	$.ajax({
        type:'POST',
        url: base_url+"index.php/Productos/obtenerdatosproducto",
        data: {
        	id:id
        },
        statusCode: {
            404: function(data) {
                toastr.error('Error', '404');
            },
            500: function(data) {
            	console.log(data);
                toastr.error('Error', 'Algo salió mal, intente de nuevo o contacte al administrador del sistema');
            }
        },
        success: function (data){
           var array = $.parseJSON(data);
           if(array.productoId>0){
				$('.class_codigo').html(array.codigo);
				$('.class_nombre').html(array.nombre);
				$('.unidadId').val(array.unidadId);
				array.productosucursal.forEach(function(element) {
					$('.stock_'+element.sucursalId).html(element.stock);
                    //======================================================
                    var namesuc=$('.stock2_'+element.sucursalId).data('name');
                    $('.stock2_'+element.sucursalId).html(namesuc+' : '+element.stock);
                    $('.stock2_'+element.sucursalId).data('stock', element.stock);
				});
           }
           totalstock();
        }
    });
}

function save_transpaso(){
    var s_salida=$('#sucursalsalida option:selected').val();
    var s_salida_d=$('#sucursalsalida option:selected').data('stock');
    var s_entrada = $('#sucursalentrada option:selected').val();
    var cantidad = $('#cantidad_traspaso').val();
    if(s_salida_d>=cantidad){
        $.ajax({
            type:'POST',
            url: base_url+"index.php/Productos/traspasos",
            data: {
                id:productotraspaso,
                suc_salida:s_salida,
                suc_entrada:s_entrada,
                cantidad:cantidad
            },
            statusCode: {
                404: function(data) {
                    toastr.error('404','Error');
                },
                500: function(data) {
                    console.log(data);
                    toastr.error('Algo salió mal, intente de nuevo o contacte al administrador del sistema','Error');
                }
            },
            success: function (data){
                $('#modal_traspaso').modal('hide');
                toastr.success( 'agregada','Hecho!');
                loadtable();

            }
        });
    }else{
        toastr.error('La cantidad no puede exceder la existente en sucursal','Error');
    }
    
}
function bitacora(id){
    $('#modal_bitacora').modal();
    obtenerdatosproducto(id);
    $('#producto_bitacora').val(id);
    obtenerdatosproductobitacora();
}

function barcode(id){
    $('#idproetiqueta').val(id);
    $("#modal_barcode").modal();
    obtenerdatosproducto(id);
    $('#iframe_barcode').html("<iframe src=" +base_url +"Barcode?id="+ id +"&page=1> </iframe>");
}

function obtenerdatosproductobitacora(){
    var idp=$('#producto_bitacora').val();
    $.ajax({
        type:'POST',
        url: base_url+"index.php/Productos/obtenerdatosproductobitacora",
        data: {
            id:idp,
            sucursal:$('#sucursalbitacora option:selected').val()
        },
        statusCode: {
            404: function(data) {
                toastr.error('Error', '404');
            },
            500: function(data) {
                console.log(data);
                toastr.error('Error', 'Algo salió mal, intente de nuevo o contacte al administrador del sistema');
            }
        },
        success: function (data){
           $('.insertablabitacora').html('');
           $('.insertablabitacora').html(data);
           $('#table_bitacora').DataTable();
        }
    });
}

function eliminarpro(id) {
    swal({
        title: "¿Desea eliminar el producto seleccionado?",
        text: "Se eliminará del listado",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Eliminar",
        cancelButtonText: "Cancelar",
    }).then(function (isConfirm) {
        if (isConfirm) {
            $.ajax({
                type:'POST',
                url: base_url+"index.php/Productos/delete",
                data: {
                    producto:id
                },
                statusCode: {
                    404: function(data) {
                        swal("Error!", "Error 404", "error");
                    },
                    500: function(data) {
                        console.log(data);
                        swal("Error!", "Algo salió mal, intente de nuevo o contacte al administrador del sistema", "error");
                    }
                },
                success:function(response){  
                    swal("Éxito!", "Eliminado correctamente", "success");
                    loadtable();
                }
            });
        }
    });
}
/*swal("Éxito!", "Se ha eliminado correctamente", "success");
function eliminarpro(id){
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: '¿Está seguro de Eliminar el producto?',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                $.ajax({
                    type:'POST',
                    url: base_url+"index.php/Productos/delete",
                    data: {
                        producto:id
                    },
                    statusCode: {
                        404: function(data) {
                            toastr.error('404','Error');
                        },
                        500: function(data) {
                            console.log(data);
                            toastr.error('Algo salió mal, intente de nuevo o contacte al administrador del sistema','Error');
                        }
                    },
                    success:function(response){  
                        toastr.success( 'Eliminado correctamente','Hecho!');
                        loadtable();
                    }
                });
            },
            cancelar: function () 
            {
                
            }
        }
    });
}
*/

function validar_nave(){
    var id = $('#sucursalsalida option:selected').val();
    $('#sucursalentrada').prop('selectedIndex',0);
    $("#sucursalentrada option:selected").attr('disabled','disabled') .siblings().removeAttr('disabled');
    setTimeout(function(){ 
        ///$("#sucursalentrada option[value='"+id+"']").attr("disabled", "disabled");
        $("#sucursalentrada option[value="+ id +"]").attr("disabled",true);
    }, 1000);
}

function get_excel(){
    var categoria = $('#categoriaselect option:selected').val();
    var nave = $('#sucursalselect option:selected').val();
    toastr.success( 'Por favor espere, se está generando el archivo de excel','Hecho!');
    location.href=base_url+'Productos/excel_reporte/'+categoria+'/'+nave;
}
