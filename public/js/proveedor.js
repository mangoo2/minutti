var base_url=$('#base_url').val();
var idpro=$('#idproveedor').val();
var cant=0;
var idban_aux=0;
var idcont=0;
$(document).ready(function() {
    btn_datos_fiscales();
    btn_datos_bancarios();
    get_tabla_bancarios(idpro);
});
function guarda_registro(){
    var form_register = $('#form_registro');
    var error_register = $('.alert-danger', form_register);
    var success_register = $('.alert-success', form_register);
    var $validator1=form_register.validate({
        errorElement: 'div', //default input error message container
        errorClass: 'vd_red', // default input error message class
        focusInvalid: false, // do not focus the last invalid input
        ignore: "",
        rules: {
            nombre:{
                required: true
            },
            celular:{
                minlength: 10,
                maxlength: 10,
                required: true
            },
            correo:{
                email: true
            }
        },
        
        errorPlacement: function(error, element) {
            if (element.parent().hasClass("vd_checkbox") || element.parent().hasClass("vd_radio")){
                element.parent().append(error);
            } else if (element.parent().hasClass("vd_input-wrapper")){
                error.insertAfter(element.parent());
            }else {
                error.insertAfter(element);
            }
        }, 
        
        invalidHandler: function (event, validator) { //display error alert on form submit              
                success_register.fadeOut(500);
                error_register.fadeIn(500);
                scrollTo(form_register,-100);

        },

        highlight: function (element) { // hightlight error inputs
    
            $(element).addClass('vd_bd-red');
            $(element).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');

        },

        unhighlight: function (element) { // revert the change dony by hightlight
            $(element)
                .closest('.control-group').removeClass('error'); // set error class to the control group
        },

        success: function (label, element) {
            label
                .addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
                .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
            $(element).removeClass('vd_bd-red');
        }
    });
    //////////Registro///////////
    var $valid = $("#form_registro").valid();
    if($valid) {
        $('.btn_registro').attr('disabled',true);
        var datos = form_register.serialize();
        $.ajax({
            type:'POST',
            url: base_url+'Proveedores/registro',
            data: datos,
            statusCode:{
                404: function(data){
                    swal("Error!", "Error 404", "error");
                },
                500: function(){
                    swal("Error!", "Algo salió mal, intente de nuevo o contacte al administrador del sistema", "error");
                }
            },
            success:function(data){
                var id=data
                guardar_bancarios(id);
                swal("Éxito!", "Guardado correctamente", "success");
                setTimeout(function(){ 
                    window.location = base_url+'Proveedores';
                }, 1500);
    
            }
        });
    }   
}


function btn_datos_fiscales(){
	if($('#datos_fiscales').is(':checked')){
        $('.txt_datos_fiscales').css('display','block');
	}else{
        $('.txt_datos_fiscales').css('display','none');
	}
}

function btn_datos_bancarios(){
    if($('#check_bancarios').is(':checked')){
        $('.txt_datos_bancarios').css('display','block');
        if(cant==0){
        }
    }else{
        $('.txt_datos_bancarios').css('display','none');
    }
}

function datos_bacarios(){
    tabla_bancario(0,'','','','','');
}

function tabla_bancario(id,banco,titular,cuenta,clave,tarjeta){
    var btn='';
    if(cant==0){
        btn='<button type="button" class="btn btn-raised btn-dark round btn-min-width mr-1 mb-1 btn-sm btn_'+cant+'" onclick="datos_bacarios('+id+','+cant+')"><i class="fa fa-plus"></i></button>'
    }else{
        btn='<button type="button" class="btn btn-raised btn-danger round btn-min-width mr-1 mb-1 btn-sm" onclick="remover('+id+','+cant+')"><i class="fa fa-trash"></i></button>';
    }
    var html='<tr class="row_'+cant+'">\
                <td>\
                   <input hidden class="form-control" id="idx" value="'+id+'">\
                   <input class="form-control" id="bancox" value="'+banco+'">\
                </td>\
                <td>\
                   <input class="form-control" id="titularx" value="'+titular+'">\
                </td>\
                <td>\
                   <input class="form-control" id="cuentax" value="'+cuenta+'">\
                </td>\
                <td>\
                   <input class="form-control" id="clavex" value="'+clave+'">\
                </td>\
                <td>\
                   <input class="form-control" id="tarjetax" value="'+tarjeta+'">\
                </td>\
                <td>\
                   '+btn+'\
                </td>\
              </tr>';
    $('#tabla_bancarios').append(html);
    cant++;
    total_bancarios();
}

function remover(id,cant){
    if(id==0){
       $('.row_'+cant).remove();
       total_bancarios();
    }else{
       idban_aux=id;
       idcont=cant;
       delete_bancario(id);
    }
}

var cont=0;
function total_bancarios(){
    //var TABLA = $("#tabla_datos_bancarios tbody > tr");
    var oRows = document.getElementById('tabla_datos_bancarios').getElementsByTagName('tr');
    var iRowCount = oRows.length-1;
    if(iRowCount>=2){
        $('.btn_0').attr('disabled',true);
    }else{
        $('.btn_0').attr('disabled',false);
    }
}

var contx=0;
function guardar_bancarios(id){
    var DATA  = [];
    var TABLA   = $("#tabla_datos_bancarios tbody > tr");
    TABLA.each(function(){ 
        contx=1;
        item = {};
        item ["idproveedor"] = parseFloat(id) ;
        item ["id"] = $(this).find("input[id*='idx']").val();
        item ["banco"] = $(this).find("input[id*='bancox']").val();
        item ["titular"] = $(this).find("input[id*='titularx']").val();
        item ["cuenta"] = $(this).find("input[id*='cuentax']").val();
        item ["clave"] = $(this).find("input[id*='clavex']").val();
        item ["tarjeta"] = $(this).find("input[id*='tarjetax']").val();
        DATA.push(item);
    });     
    if(contx==1){
        INFO  = new FormData();
        aInfo   = JSON.stringify(DATA);
        INFO.append('data', aInfo);
        $.ajax({
            data: INFO, 
            type: 'POST',
            url : base_url + 'Proveedores/registro_bancarios',
            processData: false, 
            contentType: false,
            async: false,
            statusCode:{
                404: function(data){
                    toastr.error('Error!', 'No Se encuentra el archivo');
                },
                500: function(){
                    toastr.error('Error', '500');
                }
            },
            success: function(data){
            }
        });  
    }
}

function get_tabla_bancarios(id){
    $.ajax({
        type:'POST',
        url: base_url+"Proveedores/get_tabla_bancarios_all",
        data: {id:id},
        success: function (response){
            var array = $.parseJSON(response);
            if(array.length>0) {
                array.forEach(function(element){
                    tabla_bancario(element.id,element.banco,element.titular,element.cuenta,element.clave,element.tarjeta)
                });
            }else{
                datos_bacarios();
            } 
        },
        error: function(response){
            toastr.error('Error', 'Algo salió mal, intente de nuevo o contacte al administrador del sistema');
        }
    });
}

function delete_bancario(id){
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: '¿Deseas eliminar esta cuenta bacanria?',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                $.ajax({
                    type:'POST',
                    url: base_url+"Proveedores/deleteregistro_bancaria",
                    data: {id:id},
                    statusCode: {
                        404: function(data) {
                            toastr.error('404','Error');
                        },
                        500: function(data) {
                            console.log(data);
                            toastr.error('Algo salió mal, intente de nuevo o contacte al administrador del sistema','Error');
                        }
                    },
                    success:function(response){  
                        toastr.success('Elimando Correctamente','Hecho!');
                        $('.row_'+idcont).remove();
                        total_bancarios();
                    }
                });
            },
            cancelar: function () 
            {
                
            }
        }
    });
}