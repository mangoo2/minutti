var base_url=$('#base_url').val();
var idproveedor=0;
$(document).ready(function() {
    table();
});

function reload_registro(){
    tabla.destroy();
    table();
}

function table(){
	tabla=$("#table_datos").DataTable({
        "bProcessing": true,
        "serverSide": true,
        responsive: !0,
        //"info":     false,
        //"paging": false,
        "ajax": {
            "url": base_url+"Proveedores/getlistado",
            type: "post",
            error: function(){
               $("#table_datos").css("display","none");
            }
        },
        "columns": [
            {"data":"nombre"},
            {"data":"correo"},
            {"data":"celular"},
            {"data":"tel_oficina"},
            {"data":"contacto"},
            {"data": null,
                "render": function ( data, type, row, meta ){
                var html='<a href="'+base_url+'Proveedores/registrar/'+row.idproveedor+'" class="btn btn-flat btn-danger"><i class="fa fa-edit icon_font"></i></a>\
                          <a onclick="delete_registro('+row.idproveedor+')" class="btn btn-flat btn-danger"><i class="fa fa-trash-o icon_font"></i></a>';
                return html;
                }
            },
        ],
        "order": [[ 0, "desc" ]],
        "lengthMenu": [[10, 25, 50], [10, 25, 50]],
        // Cambiamos lo principal a Español
        "language": {
            "lengthMenu": "Mostrar _MENU_ entradas",
            "zeroRecords": "Lo sentimos - No se han encontrado elementos",
            "sInfo":  "Mostrando registros del _START_ al _END_ de un total de _TOTAL_  registros",
            "info": "Mostrando página _PAGE_ de _PAGES_",
            "infoEmpty": "No hay registros disponibles",
            "infoFiltered": "(Filtrado de _MAX_ registros totales)",
            "search": "Buscar : _INPUT_",
            "paginate": {
                "previous": "Página previa",
                "next": "Siguiente página"
              }
        }
    
    });
}


function delete_registro(id){
    swal({
        title: "¿Desea eliminar el proveedor seleccionado?",
        text: "Se eliminará del listado",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Eliminar",
        cancelButtonText: "Cancelar",
    }).then(function (isConfirm) {
        if (isConfirm) {
            $.ajax({
                type:'POST',
                url: base_url+"Proveedores/deleteregistro",
                data: {id:id},
                statusCode: {
                    404: function(data) {
                        swal("Error!", "Error 404", "error");
                    },
                    500: function(data) {
                        console.log(data);
                        swal("Error!", "Algo salió mal, intente de nuevo o contacte al administrador del sistema", "error");
                    }
                },
                success:function(response){  
                    swal("Éxito!", "Eliminado correctamente", "success");
                    tabla.ajax.reload();
                }
            });
        }
    });
}

/*
function delete_registro(id){
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: '¿Está seguro de eliminar este proveedor?',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                $.ajax({
                    type:'POST',
                    url: base_url+"Proveedores/deleteregistro",
                    data: {id:id},
                    statusCode: {
                        404: function(data) {
                            toastr.error('404','Error');
                        },
                        500: function(data) {
                            console.log(data);
                            toastr.error('Algo salió mal, intente de nuevo o contacte al administrador del sistema','Error');
                        }
                    },
                    success:function(response){  
                        toastr.success( 'Eliminado correctamente','Hecho!');
                        tabla.ajax.reload();
                    }
                });
            },
            cancelar: function () 
            {
                
            }
        }
    });
}
*/