var base_url=$('#base_url').val();
$(document).ready(function() {

	$("#provee").select2({width: '95%',});
	$("#provee_oc").select2({width: '95%',});
	$("#baseIcon-tab1").click(function() {
		$(".tab-content").show("slow");
		$(".tab-content2").hide("slow");
		$(".tab-content3").hide("slow");
		//$("#baseIcon-tab3").removeClass("active");
	});
	$("#baseIcon-tab2").click(function() {
		$(".tab-content2").show("slow");
		$(".tab-content").hide("slow");
		$(".tab-content3").hide("slow");
	});
	$("#baseIcon-tab3").click(function() {
		$(".tab-content3").show("slow");
		$(".tab-content").hide("slow");
		$(".tab-content2").hide("slow");
	});

	$("#nave, #opt_prods").change(function() { // para exportar reporte pdf de prods
		validaBtns();
	});
	validaBtns();
	$("#export_pdf").click(function() { //exporat reporte pdf de prods
		if($("#nave option:selected").val()!="0" && $("#opt_prods option:selected").val()!="0"){
			expotarPDFProds();
		}
	});
	$("#export_excel").click(function() { //exporat reporte pdf de prods
		if($("#nave option:selected").val()!="0" && $("#opt_prods option:selected").val()!="0"){
			expotarExcelProds();
		}
	});

	/////////////////////////////////////////////////
	$("#provee").change(function() { // para exportar reporte pdf de provees
		validaBtnsProvee();
	});
	validaBtnsProvee();
	$("#export_pdf_pro").click(function() { //exporat reporte pdf de provees
		if($("#provee option:selected").val()!="0"){
			expotarPDFProvees();
		}
	});
	$("#export_excel_pro").click(function() { //exporat reporte pdf de provees
		if($("#provee option:selected").val()!="0"){
			expotarExcelProvees();
		}
	});
	/////////////////////////////////////////////////
	$("#provee_oc,#metodo,#fi,#ff").change(function() { // para exportar reporte pdf de compras
		validaBtnsCompras();
	});
	validaBtnsCompras();
	$("#export_pdf_oc").click(function() { //exporat reporte pdf de compras
		if($("#provee_oc option:selected").val()!="0" && $("#metodo option:selected").val()!="0" && $("#fi").val()!="" && $("#ff").val()!=""){
			expotarPDFCompras();
		}
	});
	$("#export_excel_oc").click(function() { //exporat reporte pdf de compras
		if($("#provee_oc option:selected").val()!="0" && $("#metodo option:selected").val()!="0" && $("#fi").val()!="" && $("#ff").val()!=""){
			expotarExcelCompras();
		}
	});
});

function validaBtns(){
	if($("#nave option:selected").val()!="0" && $("#opt_prods option:selected").val()!="0")
		$(".btn-export").attr("disabled",false);
	else{
		$(".btn-export").attr("disabled",true);
	}	
}
function expotarPDFProds(){
	window.open(base_url+'Reportes/PDFProds/'+$("#nave option:selected").val()+"/"+$("#opt_prods option:selected").val());
}
function expotarExcelProds(){
	window.open(base_url+'Reportes/ExcelProds/'+$("#nave option:selected").val()+"/"+$("#opt_prods option:selected").val());
}

///////////////////////////////
function validaBtnsProvee(){
	if($("#provee option:selected").val()!="0" && $("#provee option:selected").val()!=undefined)
		$(".btn-export2").attr("disabled",false);
	else{
		$(".btn-export2").attr("disabled",true);
	}	
}
function expotarPDFProvees(){
	window.open(base_url+'Reportes/PDFProvees/'+$("#provee option:selected").val());
}
function expotarExcelProvees(){
	window.open(base_url+'Reportes/ExcelProvees/'+$("#provee option:selected").val());
}

///////////////////////////////
function validaBtnsCompras(){
	if($("#provee_oc option:selected").val()!="0" && $("#provee_oc option:selected").val()!=undefined && $("#metodo option:selected").val()!="0" && $("#fi").val()!="" && $("#ff").val()!="")
		$(".btn-export3").attr("disabled",false);
	else{
		$(".btn-export3").attr("disabled",true);
	}	
}
function expotarPDFCompras(){
	window.open(base_url+'Reportes/PDFCompras/'+$("#provee_oc option:selected").val()+"/"+$("#fi").val()+"/"+$("#ff").val()+"/"+$("#metodo option:selected").val());
}
function expotarExcelCompras(){
	window.open(base_url+'Reportes/ExcelCompras/'+$("#provee_oc option:selected").val()+"/"+$("#fi").val()+"/"+$("#ff").val()+"/"+$("#metodo option:selected").val());
}
