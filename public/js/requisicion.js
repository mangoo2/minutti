var base_url = $('#base_url').val();
$(document).ready(function($) {
	/*
    $('#personalId').select2({
        width: 'resolve',
        minimumInputLength: 3,
        minimumResultsForSearch: 10,
        placeholder: 'Buscar un personal',
        ajax: {
            url: base_url+'General/search_personal',
            dataType: "json",
            data: function (params) {
            var query = {
                search: params.term,
                type: 'public'
            }
            return query;
        },
        processResults: function(data){
            var clientes=data;
            var itemscli = [];
            data.forEach(function(element) {
                itemscli.push({
                    id: element.personalId,
                    text: element.nombre+' '+element.apellido_paterno+' '+element.apellido_materno
                });
            });
            return {
                results: itemscli
            };          
        },  
      }
    });
    */
    $('#s_producto').select2({
        width: 'resolve',
        minimumInputLength: 3,
        minimumResultsForSearch: 10,
        placeholder: 'Buscar un producto',
        ajax: {
            url: base_url+'General/search_producto',
            dataType: "json",
            data: function (params) {
            var query = {
                search: params.term,
                type: 'public'
            }
            return query;
        },
        processResults: function(data){
            var clientes=data;
            var itemscli = [];
            data.forEach(function(element) {
                $(element).data('codigo', element.codigo);
                itemscli.push({
                    id: element.productoId,
                    text: element.nombre,
                    codigo: element.codigo
                });
            });
            return {
                results: itemscli
            };          
        },  
      }
    }).on('select2:select', function (e) {
        var data = e.params.data
        $("#s_producto option:selected").attr('data-codigo',data.codigo);
    });
    var requsicionId = $('#requsicionId').val();
    if(requsicionId>0){
        get_tabla_requisicion(requsicionId);  
    }
});

function add_datos(){
    var s_cantidad = $('#s_cantidad').val();
    var codigo=$('#s_producto option:selected').data('codigo');
    var productotext=$('#s_producto option:selected').text();
    var productoId=$('#s_producto option:selected').val();
    if(s_cantidad!='' && s_cantidad!=0){
        if(productoId!=undefined){
            addinfo(0,s_cantidad,codigo,productotext,productoId);
            $('#s_producto').val(null).trigger('change');
        }else{
            toastr.error('Falta selecionar un producto','Ateción!');
        }
    }else{
        toastr.error('Falta agregar una cantidad','Ateción!');
    } 
}
var addproductorow=0;
function addinfo(idreg,s_cantidad,codigo,productotext,productoId){
    var html='<tr class="addproducto_'+addproductorow+'">\
                <td>\
                    <input type="hidden" id="requsiciondId" value="'+idreg+'">\
                    <input type="hidden" id="cantidad" value="'+s_cantidad+'">\
                    <input type="hidden" id="productoId" value="'+productoId+'">\
                    '+s_cantidad+'\
                </td>\
                <td>\
                    '+codigo+'\
                </td>\
                <td>\
                    '+productotext+'\
                </td>\
                <td>\
                    <button type="button" class="btn btn-raised btn-icon btn-danger mr-1" onclick="deletepro('+addproductorow+','+idreg+')"><i class="fa fa-trash-o"></i></button>\
                </td>\
            </tr>';
        $('.table-productos-tb').append(html);
    addproductorow++;
}
function deletepro(row,id){
    if(id==0){
        $('.addproducto_'+row).remove();
    }else{
        btn_cancelar_detalle(id,row);
    }
    
}
function save(){
    var form_register = $('#form-requisicion');
    var error_register = $('.alert-danger', form_register);
    var success_register = $('.alert-success', form_register);
    form_register.validate({
        errorElement: 'div', //default input error message container
        errorClass: 'vd_red', // default input error message class
        focusInvalid: false, // do not focus the last invalid input
        ignore: "",
        rules: {
            personalId: {
                required: true
            },
            fecha_solicitud: {
                required: true
            },
            destino: {
                required: true
            },
        },
        errorPlacement: function(error, element) {
            if (element.parent().hasClass("vd_checkbox") || element.parent().hasClass("vd_radio")){
                element.parent().append(error);
            } else if (element.parent().hasClass("vd_input-wrapper")){
                error.insertAfter(element.parent());
            }else {
                error.insertAfter(element);
            }
        }, 
        
        invalidHandler: function (event, validator) { //display error alert on form submit              
                success_register.fadeOut(500);
                error_register.fadeIn(500);
                scrollTo(form_register,-100);

        },

        highlight: function (element) { // hightlight error inputs
    
            $(element).addClass('vd_bd-red');
            $(element).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');

        },

        unhighlight: function (element) { // revert the change dony by hightlight
            $(element)
                .closest('.control-group').removeClass('error'); // set error class to the control group
        },

        success: function (label, element) {
            label
                .addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
                .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
            $(element).removeClass('vd_bd-red');
        }
    });
    var $valid = $("#form-requisicion").valid();
    if($valid) {
        var DATAconf  = [];
        var TABLAconf   = $("#table-productos tbody > tr");
            TABLAconf.each(function(){         
            item = {};
            item ["requsiciondId"] = $(this).find("input[id*='requsiciondId']").val();
            item ["cantidad"] = $(this).find("input[id*='cantidad']").val();
            item ["productoId"] = $(this).find("input[id*='productoId']").val();
            
            
            DATAconf.push(item);
        });
        INFO  = new FormData();
        arrayconfiguracion   = JSON.stringify(DATAconf);
        console.log(arrayconfiguracion);
        console.log($.parseJSON(arrayconfiguracion)); 
        var datos = $('#form-requisicion').serialize()+'&arrayproductos='+arrayconfiguracion;
        $('.btn_registro').attr('disabled',true);
        $.ajax({
            type:'POST',
            url: base_url+"index.php/Requisiciones/insertupdate",
            data: datos,
            statusCode: {
                404: function(data) {
                    toastr.error('Error', '404');
                },
                500: function(data) {
                    console.log(data);
                    toastr.error('Error', 'Algo salió mal, intente de nuevo o contacte al administrador del sistema');
                }
            },
            success: function (response){
                toastr.success( 'Configuración agregado','Hecho!');
                
                
                setTimeout(function(){ 
                    //location.reload();
                    window.location.href = base_url+"index.php/Requisiciones"; 
                }, 2000);
                
            },
            error: function(response){
                toastr.error('Error', 'Algo salió mal, intente de nuevo o contacte al administrador del sistema');
            }
        });
    }
}

function btn_cancelar_detalle(id,row){
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: '¿Está seguro de eliminar este registro?',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                $.ajax({
                    type:'POST',
                    url: base_url+"Requisiciones/cancelar_requisicion_material_detalle",
                    data: {
                        id:id
                    },
                    statusCode: {
                        404: function(data) {
                            toastr.error('404','Error');
                        },
                        500: function(data) {
                            console.log(data);
                            toastr.error('Algo salió mal, intente de nuevo o contacte al administrador del sistema','Error');
                        }
                    },
                    success:function(response){  
                        $('.addproducto_'+row).remove();
                        toastr.success( 'Eliminado correctamente','Hecho!');
                    }
                });
            },
            cancelar: function () 
            {
                
            }
        }
    });
}

function get_tabla_requisicion(id){
    $.ajax({
        type:'POST',
        url: base_url+"Requisiciones/get_tabla_requisicion_material_detalle",
        data: {id:id},
        success: function (response){
            var array = $.parseJSON(response);
            if(array.length>0) {
                array.forEach(function(element){
                    addinfo(element.requsiciondId,element.cantidad,element.codigo,element.producto,element.productoId);
                });
            }
        },
        error: function(response){
            toastr.error('Error', 'Algo salió mal, intente de nuevo o contacte al administrador del sistema');
        }
    });
}
