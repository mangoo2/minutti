var base_url = $('#base_url').val();
var table;
$(document).ready(function($) {
	table=$('#table-data-row').DataTable();
	loadtable();
});
function loadtable(){
	table.destroy();
	table = $('#table-data-row').DataTable({
        stateSave: true,
        responsive: !0,
        "bProcessing": true,
        "serverSide": true,
        "ajax": {
            "url": base_url+"index.php/Requisiciones/getlist_row",
            type: "post",
            "data": {
                'personal':0
            },
        },
        "columns": [
            {"data": "requsicionId"},
            {"data": "personal"},
            {"data": "sucursal"},
            {"data": "fecha_solicitud"},
            {"data": "realizo"},
            {"data": null,
                render:function(data,type,row){
                    var html='';
                    if(row.estatus==0){
                        html='<span class="bg-danger text-highlight white" style="border-radius: 10px;">Pendiente</span>  ';
                    }else{
                        html='<span class="bg-info text-highlight white" style="border-radius: 10px;">Aceptada</span>';
                    }
                    return html;
                }
            },
            {"data": null,
                render:function(data,type,row){
                    var html='';
                    if(row.estatus==0){
                        html='<a href="'+base_url+'Requisiciones/add/'+row.requsicionId+'" class="btn btn-flat btn-danger"><i class="fa fa-edit icon_font"></i></a>\
                            <a class="btn btn-flat btn-danger" onclick="btn_cancelar('+row.requsicionId+')"><i class="fa fa-times-circle-o icon_font"></i></a>';
                    }

                    return html;
                }
            },
        ],
        "order": [[ 0, "desc" ]],
        "lengthMenu": [[25, 50, 100], [25, 50,100]]
        
    });
}

function btn_cancelar(id){
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: '¿Está seguro de eliminar esta requisición?',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                $.ajax({
                    type:'POST',
                    url: base_url+"Requisiciones/cancelar_requisicion_material",
                    data: {
                        id:id
                    },
                    statusCode: {
                        404: function(data) {
                            toastr.error('404','Error');
                        },
                        500: function(data) {
                            console.log(data);
                            toastr.error('Algo salió mal, intente de nuevo o contacte al administrador del sistema','Error');
                        }
                    },
                    success:function(response){  
                        toastr.success( 'Eliminado correctamente','Hecho!');
                        loadtable();
                    }
                });
            },
            cancelar: function () 
            {
                
            }
        }
    });
}
