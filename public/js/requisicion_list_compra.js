var base_url = $('#base_url').val();
var table;
$(document).ready(function($) {
	table=$('#table-data-row').DataTable();
	loadtable();
});
function loadtable(){
	table.destroy();
	table = $('#table-data-row').DataTable({
        stateSave: true,
        responsive: !0,
        "bProcessing": true,
        "serverSide": true,
        "ajax": {
            "url": base_url+"index.php/Entrada/getlist_requision",
            type: "post",
            "data": {
                'personal':0
            },
        },
        "columns": [
            {"data": "requsicionId"},
            {"data": "personalId",
                render:function(data,type,row){
                    var html ='';
                        
                        html=row.nombre+' '+row.apellido_paterno+' '+row.apellido_materno;

                    return html;
                }
            },
            {"data": "sucursal"},
            {"data": "fecha_solicitud"},
            {"data": "realizo"},
            {"data": null,
                render:function(data,type,row){
                    var html='';
                    if(row.estatus==0){
                        html='<span class="bg-danger text-highlight white" style="border-radius: 10px;">Pendiente</span>  ';
                    }else if(row.estatus==1){
                        html='<span class="bg-info text-highlight white" style="border-radius: 10px;">Compra realizada</span>';
                    }else if(row.estatus==2){
                        html='<span class="bg-success text-highlight white" style="border-radius: 10px;">Compra aceptada</span>';    
                    }
                    return html;
                }
            },
            {"data": null,
                render:function(data,type,row){
                    var html='';
                    if(row.estatus==0){
                    var html='<a href="'+base_url+'Entrada/compra_requisicion/'+row.requsicionId+'" class="btn btn-flat btn-dark">Realizar compra</a>';
                    }else{
                        html='';
                    }
                    return html;
                }
            },
        ],
        "order": [[ 0, "desc" ]],
        "lengthMenu": [[25, 50, 100], [25, 50,100]]
        
    });
}
