var base_url = $('#base_url').val();
$(document).ready(function($) {
	$('#personalId').select2({
        width: 'resolve',
        minimumInputLength: 3,
        minimumResultsForSearch: 10,
        placeholder: 'Buscar un personal',
        ajax: {
            url: base_url+'General/search_personal',
            dataType: "json",
            data: function (params) {
            var query = {
                search: params.term,
                type: 'public'
            }
            return query;
        },
        processResults: function(data){
            var clientes=data;
            var itemscli = [];
            data.forEach(function(element) {
                itemscli.push({
                    id: element.personalId,
                    text: element.nombre+' '+element.apellido_paterno+' '+element.apellido_materno
                });
            });
            return {
                results: itemscli
            };          
        },  
      }
    });
    $('#s_producto').select2({
        width: 'resolve',
        minimumInputLength: 3,
        minimumResultsForSearch: 10,
        placeholder: 'Buscar un producto',
        ajax: {
            url: base_url+'General/search_producto',
            dataType: "json",
            data: function (params) {
            var query = {
                search: params.term,
                type: 'public'
            }
            return query;
        },
        processResults: function(data){
            var clientes=data;
            var itemscli = [];
            data.forEach(function(element) {
                $(element).data('codigo', element.codigo);
                itemscli.push({
                    id: element.productoId,
                    text: element.nombre,
                    codigo: element.codigo
                });
            });
            return {
                results: itemscli
            };          
        },  
      }
    }).on('select2:select', function (e) {
        var data = e.params.data
        $("#s_producto option:selected").attr('data-codigo',data.codigo);
    });
});
var addproductorow=0;
function addinfo(){
    var s_cantidad = $('#s_cantidad').val();
    var codigo=$('#s_producto option:selected').data('codigo');
    var productotext=$('#s_producto option:selected').text();
    var productoId=$('#s_producto option:selected').val();

    var html='<tr class="addproducto_'+addproductorow+'">\
                <td>\
                    <input type="hidden" id="retornodId" value="0">\
                    <input type="hidden" id="cantidad" value="'+s_cantidad+'">\
                    <input type="hidden" id="productoId" value="'+productoId+'">\
                    '+s_cantidad+'\
                </td>\
                <td>\
                    '+codigo+'\
                </td>\
                <td>\
                    '+productotext+'\
                </td>\
                <td>\
                    <button type="button" class="btn btn-raised btn-icon btn-danger mr-1" onclick="deletepro('+addproductorow+')"><i class="fa fa-trash-o"></i></button>\
                </td>\
            </tr>';
        $('.table-productos-tb').append(html);
    addproductorow++;
}
function deletepro(row){
    $('.addproducto_'+row).remove();
}
function save(){
    var DATAconf  = [];
    var TABLAconf   = $("#table-productos tbody > tr");
        TABLAconf.each(function(){         
        item = {};
        item ["retornodId"] = $(this).find("input[id*='retornodId']").val();
        item ["cantidad"] = $(this).find("input[id*='cantidad']").val();
        item ["productoId"] = $(this).find("input[id*='productoId']").val();
        
        
        DATAconf.push(item);
    });
    INFO  = new FormData();
    arrayconfiguracion   = JSON.stringify(DATAconf);
    console.log(arrayconfiguracion);
    console.log($.parseJSON(arrayconfiguracion)); 
    var datos = $('#form-retorno').serialize()+'&arrayproductos='+arrayconfiguracion;
    $.ajax({
        type:'POST',
        url: base_url+"index.php/Entrada/retorno_material_insertupdate",
        data: datos,
        statusCode: {
            404: function(data) {
                toastr.error('Error', '404');
            },
            500: function(data) {
                console.log(data);
                toastr.error('Error', 'Algo salió mal, intente de nuevo o contacte al administrador del sistema');
            }
        },
        success: function (response){
            toastr.success( 'Configuración agregado','Hecho!');
            
            
            setTimeout(function(){ 
                //location.reload();
                window.location.href = base_url+"index.php/Entrada/retorno_material"; 
            }, 2000);
            
        },
        error: function(response){
            toastr.error('Error', 'Algo salió mal, intente de nuevo o contacte al administrador del sistema');
        }
    });
}