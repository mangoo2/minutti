var base_url = $('#base_url').val();
var table;
$(document).ready(function($) {
	table=$('#table-data-row').DataTable();
	loadtable();
});
function loadtable(){
	table.destroy();
	table = $('#table-data-row').DataTable({
        stateSave: true,
        responsive: !0,
        "bProcessing": true,
        "serverSide": true,
        "ajax": {
            "url": base_url+"index.php/Entrada/getlist_rm_row",
            type: "post",
            "data": {
                'personal':0
            },
        },
        "columns": [
            {"data": "requsicionId"},
            {"data": "personalId",
                render:function(data,type,row){
                    var html ='';
                        
                        html=row.nombre+' '+row.apellido_paterno+' '+row.apellido_materno;

                    return html;
                }
            },
            {"data": "fecha_solicitud",},
            {"data": null,
                render:function(data,type,row){
                    var html='';
                        
                    return html;
                }
            },
        ],
        "order": [[ 0, "desc" ]],
        "lengthMenu": [[25, 50, 100], [25, 50,100]]
        
    });
}