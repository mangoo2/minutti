var base_url = $('#base_url').val();
var Idotro_aux = $('#Idotro').val();
$(document).ready(function($) {
    $('#personalId').select2({
        width: 'resolve',
        minimumInputLength: 3,
        minimumResultsForSearch: 10,
        placeholder: 'Buscar un Solicitante',
        ajax: {
            url: base_url+'Salida/search_personal_sucu',
            dataType: "json",
            data: function (params) {
            var query = {
                search: params.term,
                type: 'public'
            }
            return query;
        },
        processResults: function(data){
            var clientes=data;
            var itemscli = [];
            data.forEach(function(element) {
                itemscli.push({
                    id: element.personalId,
                    text: element.nombre+' '+element.apellido_paterno+' '+element.apellido_materno,
                    //puesto:element.puesto,
                    idsucu: element.sucursalId,
                    sucu: element.sucursal,
                });
            });
            return {
                results: itemscli
            };          
        },  
      }
    }).on('select2:select', function (e) {
        var data = e.params.data;
        $('#areatext').val(data.sucu);
        $('#area').val(data.idsucu);
    });
    $('#s_producto').select2({
        width: 'resolve',
        minimumInputLength: 3,
        minimumResultsForSearch: 10,
        placeholder: 'Buscar un producto',
        ajax: {
            url: base_url+'General/search_producto',
            dataType: "json",
            data: function (params) {
            var query = {
                search: params.term,
                type: 'public'
            }
            return query;
        },
        processResults: function(data){
            var clientes=data;
            var itemscli = [];
            data.forEach(function(element) {
                $(element).data('codigo', element.codigo);
                itemscli.push({
                    id: element.productoId,
                    text: element.nombre,
                    codigo: element.codigo
                });
            });
            return {
                results: itemscli
            };          
        },  
      }
    }).on('select2:select', function (e) {
        var data = e.params.data
        $("#s_producto option:selected").attr('data-codigo',data.codigo);
    });
    $( "input:radio[name=motivo]").change(function() {
        var motivo=$( "input:radio[name=motivo]:checked").val();
        if(motivo==3){
            $('.s_motivo_otro').show('show');
        }else{
            $('.s_motivo_otro').hide('show');
            $('#s_motivo_otro').val('');
        }
    });

    if(Idotro_aux!=0){
       obtener_datos_producto(Idotro_aux);
    }
});

function info_producto(){
    var s_cantidad = $('#s_cantidad').val();
    var codigo=$('#s_producto option:selected').data('codigo');
    var productotext=$('#s_producto option:selected').text();
    var productoId=$('#s_producto option:selected').val();
    var motivo=$( "input:radio[name=motivo]:checked").val();
    var motivotext=$( "#s_motivo_otro").val();
    var area=$( "#area").val();  
    if(s_cantidad!=''){
        if(productoId!=undefined && productoId!=0){
            if(area!=''){
                var numv=verificacion_existencia(area,productoId,s_cantidad);
                if(numv==0){
                    addinfo(0,s_cantidad,codigo,productotext,productoId,motivo,motivotext);
                    $('#s_cantidad').val(''); 
                    $('#s_motivo_otro').val(''); 
                    $('#s_producto').val(null).trigger('change');
                }else{
                    toastr.error('Este producto no cuenta con suficiente existencia','Atención!');
                }
            }else{
                toastr.error('Falta agregar un area','Atención!');    
            }
        }else{
            toastr.error('Falta agregar un producto','Atención!');
        }
    }else{
        toastr.error('Falta agregar una cantidad','Atención!');
    }
}

var addproductorow=0;
function addinfo(iddotro,s_cantidad,codigo,productotext,productoId,motivo,motivotext){
    /*
    if(motivo==1){
        var mottext='Cambio por desgaste';
    }else if(motivo==2){
        var mottext='Surtimiento de insumo';
    }else{
        var mottext=motivotext;
    }
    */
    var html='<tr class="addproducto_'+addproductorow+'">\
            <td>\
                <input type="hidden" id="sotrosdId" value="'+iddotro+'">\
                <input type="hidden" id="cantidad" value="'+s_cantidad+'">\
                <input type="hidden" id="producto" value="'+productoId+'">\
                <input type="hidden" id="motivo" value="'+motivo+'">\
                <input type="hidden" id="motivo_otro" value="'+motivotext+'">\
                '+s_cantidad+'\
            </td>\
            <td>\
                '+codigo+'\
            </td>\
            <td>\
                '+productotext+'\
            </td>\
            <td>\
                '+motivotext+'\
            </td>\
            <td>\
                <button type="button" class="btn btn-raised btn-icon btn-danger mr-1" onclick="deletepro('+addproductorow+','+iddotro+')"><i class="fa fa-trash-o"></i></button>\
            </td>\
        </tr>';
    $('.table_otros_tbody').append(html);
    addproductorow++;
}

function deletepro(row,iddotro){
    if(iddotro==0){
        $('.addproducto_'+row).remove();
    }else{
        $.confirm({
            boxWidth: '30%',
            useBootstrap: false,
            icon: 'fa fa-warning',
            title: 'Atención!',
            content: '¿Está seguro de eliminar este producto, afectara al catalogo de productos?',
            type: 'red',
            typeAnimated: true,
            buttons:{
                confirmar: function (){
                    $.ajax({
                        type:'POST',
                        url: base_url+"Salida/cancelar_orden_detalle_surtir_otro",
                        data: {
                            sotrosdId:iddotro
                        },
                        statusCode: {
                            404: function(data) {
                                toastr.error('404','Error');
                            },
                            500: function(data) {
                                console.log(data);
                                toastr.error('Algo salió mal, intente de nuevo o contacte al administrador del sistema','Error');
                            }
                        },
                        success:function(response){  
                            toastr.success( 'Eliminado correctamente','Hecho!');
                            $('.addproducto_'+row).remove(); 
                        }
                    });
                },
                cancelar: function () 
                {
                    
                }
            }
        });
    }
}

function save(){
    var form_register = $('#form-orden');
    var error_register = $('.alert-danger', form_register);
    var success_register = $('.alert-success', form_register);
    form_register.validate({
        errorElement: 'div', //default input error message container
        errorClass: 'vd_red', // default input error message class
        focusInvalid: false, // do not focus the last invalid input
        ignore: "",
        rules: {
            personalId: {
                required: true
            },
            fecha_solicitud: {
                required: true
            },
            area: {
                required: true
            }
        },
        errorPlacement: function(error, element) {
            if (element.parent().hasClass("vd_checkbox") || element.parent().hasClass("vd_radio")){
                element.parent().append(error);
            } else if (element.parent().hasClass("vd_input-wrapper")){
                error.insertAfter(element.parent());
            }else {
                error.insertAfter(element);
            }
        }, 
        
        invalidHandler: function (event, validator) { //display error alert on form submit              
                success_register.fadeOut(500);
                error_register.fadeIn(500);
                scrollTo(form_register,-100);

        },

        highlight: function (element) { // hightlight error inputs
    
            $(element).addClass('vd_bd-red');
            $(element).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');

        },

        unhighlight: function (element) { // revert the change dony by hightlight
            $(element)
                .closest('.control-group').removeClass('error'); // set error class to the control group
        },

        success: function (label, element) {
            label
                .addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
                .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
            $(element).removeClass('vd_bd-red');
        }
    });
    var $valid = $("#form-orden").valid();
    if($valid) {
        var vali_tabla=0;
        var DATAconf  = [];
        var TABLAconf   = $("#table_otros tbody > tr");
        TABLAconf.each(function(){   
            vali_tabla=1;           
            item = {};
            item ["sotrosdId"] = $(this).find("input[id*='sotrosdId']").val();
            item ["cantidad"] = $(this).find("input[id*='cantidad']").val();
            item ["producto"] = $(this).find("input[id*='producto']").val();
            item ["motivo"] = $(this).find("input[id*='motivo']").val();
            item ["motivo_otro"] = $(this).find("input[id*='motivo_otro']").val();
            DATAconf.push(item);
        });
        INFO  = new FormData();
        arrayconfiguracion   = JSON.stringify(DATAconf);
        console.log(arrayconfiguracion);
        console.log($.parseJSON(arrayconfiguracion)); 
        var datos = $('#form-orden').serialize()+'&arrayproductos='+arrayconfiguracion;
        if(vali_tabla==1){ 
            $('.button_save').attr('disabled',true);
            $.ajax({
                type:'POST',
                url: base_url+"index.php/Salida/surtir_otro_insertupdate",
                data: datos,
                statusCode: {
                    404: function(data) {
                        toastr.error('Error', '404');
                    },
                    500: function(data) {
                        console.log(data);
                        toastr.error('Error', 'Algo salió mal, intente de nuevo o contacte al administrador del sistema');
                    }
                },
                success: function (response){
                    toastr.success( 'Configuración agregado','Hecho!');
                    setTimeout(function(){ 
                        window.location.href = base_url+"index.php/Salida/surtir_otro_listado"; 
                    }, 2000);
                },
                error: function(response){
                    toastr.error('Error', 'Algo salió mal, intente de nuevo o contacte al administrador del sistema');
                }
            });
        }else{
            toastr.error('No tienes ningún producto agregada', 'Atención!');
        }
    }
}

function verificacion_existencia(idorigen,idproducto,cantidad){
    var result=0;
    $.ajax({
        type:'POST',
        url: base_url+"index.php/Salida/get_verificacion_existencia_producto",
        data:{idorigen:idorigen,idproducto:idproducto,cantidad:cantidad},
        async: false,
        statusCode:{
            404: function(data){
                swal("Error!", "No Se encuentra el archivo", "error");
            },
            500: function(){
                swal("Error!", "500", "error");
            }
        },
        success:function(data){
            result=data;
        }
    });
    return result;
}


function obtener_datos_producto(idp){
    $.ajax({
        type:'POST',
        url: base_url+"index.php/Salida/obtenerdatosproducto_surtir_otro",
        data: {id:idp},
        success: function (data){
            var array = $.parseJSON(data);
            array.get_datos.forEach(function(element) {
                addinfo(element.sotrosdId,element.cantidad,element.codigo,element.productotext,element.producto,element.motivo,element.motivo_otro);
            });
        },
        error: function(data){ 
            toastr.error('Error', 'Algo salió mal, intente de nuevo o contacte al administrador del sistema');
        }
    });
}