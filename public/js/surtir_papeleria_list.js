var base_url = $('#base_url').val();
var table;
$(document).ready(function($) {
	table=$('#table_datos').DataTable();
	loadtable();
});
function loadtable(){
	table.destroy();
	table = $('#table_datos').DataTable({
        stateSave: true,
        responsive: !0,
        "bProcessing": true,
        "serverSide": true,
        "ajax": {
            "url": base_url+"index.php/Salida/getlist_surtir_papeleria",
            type: "post",
        },
        "columns": [
            {"data": "Id"},
            {"data": "solicita"},
            {"data": "sucursal"},
            {"data": "fecha_solicitud"},
            {"data": null,
                render:function(data,type,row){
                    var html='';
                    html='<a class="btn btn-flat btn-danger" onclick="modalproducto('+row.Id+')"><i class="fa fa-eye icon_font"></i></a>';
                    return html;
                }
            },
            {"data": null,
                render:function(data,type,row){
                    var html='';
                    if(row.activo==1){
                        html='<a href="'+base_url+'Salida/documento_pdf_surtir_papeleria/'+row.Id+'" target="_blank" class="btn btn-flat btn-danger" title="Imprimir formato básico"><i class="fa fa-file-text-o icon_font"></i></a>\
                            <a href="'+base_url+'Salida/surtir_papeleria_add/'+row.Id+'" class="btn btn-flat btn-danger"><i class="fa fa-edit icon_font"></i></a>\
                            <a class="btn btn-flat btn-danger" onclick="btn_cancelar('+row.Id+')"><i class="fa fa-times-circle-o icon_font"></i></a>';
                    }else{
                        html='<span class="bg-danger text-highlight white" style="border-radius: 10px;">Cancelada</span>';
                    }
                    return html;
                }
            },
        ],
        "order": [[ 0, "desc" ]],
        "lengthMenu": [[25, 50, 100], [25, 50,100]]
        
    });
}

function btn_cancelar(id){
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: '¿Está seguro de cancelar este vale de papeleria, afectara al catalogo de productos?',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                $.ajax({
                    type:'POST',
                    url: base_url+"Salida/cancelar_surtir_papeleria",
                    data: {
                        id:id
                    },
                    statusCode: {
                        404: function(data) {
                            toastr.error('404','Error');
                        },
                        500: function(data) {
                            console.log(data);
                            toastr.error('Algo salió mal, intente de nuevo o contacte al administrador del sistema','Error');
                        }
                    },
                    success:function(response){  
                        toastr.success( 'Eliminado correctamente','Hecho!');
                        loadtable();
                    }
                });
            },
            cancelar: function () 
            {
                
            }
        }
    });
}

function modalproducto(id){
    $('#modal_producto').modal();
    $('.table_otros_tbody').html('');
    $.ajax({
        type:'POST',
        url: base_url+"index.php/Salida/obtenerdatosproducto_surtir_papeleria",
        data: {id:id},
        success: function (data){
            var array = $.parseJSON(data);
            array.get_datos.forEach(function(element){

                addinfo(element.Iddpapeleria,element.cantidad,element.codigo,element.productotext,element.producto,element.motivo,element.motivo_otro);
                //addinfo(iddconsimuble,s_cantidad,codigo,productotext,productoId,motivo,motivotext)
            });
        },
        error: function(data){ 
            toastr.error('Error', 'Algo salió mal, intente de nuevo o contacte al administrador del sistema');
        }
    });
}

var addproductorow=0;
function addinfo(iddpapeleria,s_cantidad,codigo,productotext,productoId,motivo,motivotext){
    if(motivo==1){
        var mottext='Cambio por desgaste';
    }else if(motivo==2){
        var mottext='Surtimiento de insumo';
    }else{
        var mottext=motivotext;
    }
    var html='<tr class="addproducto_'+addproductorow+'">\
            <td>\
                '+s_cantidad+'\
            </td>\
            <td>\
                '+codigo+'\
            </td>\
            <td>\
                '+productotext+'\
            </td>\
        </tr>';
    $('.table_otros_tbody').append(html);
    addproductorow++;
}