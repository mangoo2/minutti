var base_url = $('#base_url').val();
var cantidad_nave = 0;
$(document).ready(function($) {
    /*
	$('#personalId').select2({
        width: 'resolve',
        minimumInputLength: 3,
        minimumResultsForSearch: 10,
        placeholder: 'Buscar un personal',
        ajax: {
            url: base_url+'General/search_personal',
            dataType: "json",
            data: function (params) {
            var query = {
                search: params.term,
                type: 'public'
            }
            return query;
        },
        processResults: function(data){
            var clientes=data;
            var itemscli = [];
            data.forEach(function(element) {
                itemscli.push({
                    id: element.personalId,
                    text: element.nombre+' '+element.apellido_paterno+' '+element.apellido_materno
                });
            });
            return {
                results: itemscli
            };          
        },  
      }
    });
    */

    
    $('#s_producto').select2({
        width: 'resolve',
        minimumInputLength: 3,
        minimumResultsForSearch: 10,
        placeholder: 'Buscar un producto',
        ajax: {
            url: base_url+'General/search_producto',
            dataType: "json",
            data: function (params) {
            var query = {
                search: params.term,
                type: 'public'
            }
            return query;
        },
        processResults: function(data){
            var clientes=data;
            var itemscli = [];
            data.forEach(function(element) {
                $(element).data('codigo', element.codigo);
                itemscli.push({
                    id: element.productoId,
                    text: element.nombre,
                    codigo: element.codigo
                });
            });
            return {
                results: itemscli
            };          
        },  
      }
    }).on('select2:select', function (e) {
        var data = e.params.data
        $("#s_producto option:selected").attr('data-codigo',data.codigo);
        var ido = $('#sucursal_origen option:selected').val();
        if(ido==0){
            $('#s_producto').val(null).trigger('change');
            toastr.error('Por favor primero selecciona el lugar de origen', 'Atención!');
        }else{
            total_nave(ido,data.id);
        }
    });   
});

function total_nave(id,idp){
    $.ajax({
        type:'POST',
        url: base_url+'Traspasos/cantidad_nave_prodcuto',
        data: {id_nave:id,idproducto:idp},
        statusCode:{
            404: function(data){
                toastr.error('Error!', 'No Se encuentra el archivo');
            },
            500: function(){
                toastr.error('Error', '500');
            }
        },
        success:function(data){
            cantidad_nave=data;
            $('.existencia_total').html('Existencia:'+data);
        }
    });
}

var addproductorow=0;
function addinfo(){
    var sucursal_origen = $('#sucursal_origen option:selected').val();
    var sucursal_destino = $('#sucursal_destino option:selected').val();
    if(sucursal_destino==0){
        toastr.error('Hace falta seleccionar un destino', 'Atención!');
    }else{
        var s_cantidad = $('#s_cantidad').val();
        var productoId=$('#s_producto option:selected').val();
        if(productoId!=undefined && s_cantidad!='' || s_cantidad!=0){
            var num_cat=parseFloat(cantidad_nave);
            if(s_cantidad<=num_cat){
                var codigo=$('#s_producto option:selected').data('codigo');
                var productotext=$('#s_producto option:selected').text();
                

                var html='<tr class="addproducto_'+addproductorow+'">\
                            <td>\
                                <input type="hidden" id="traspasodId" value="0">\
                                <input type="hidden" id="cantidad" value="'+s_cantidad+'">\
                                <input type="hidden" id="productoId" value="'+productoId+'">\
                                <input type="hidden" id="sucursal_origenx" value="'+sucursal_origen+'">\
                                <input type="hidden" id="sucursal_destinox" value="'+sucursal_destino+'">\
                                '+s_cantidad+'\
                            </td>\
                            <td>\
                                '+codigo+'\
                            </td>\
                            <td>\
                                '+productotext+'\
                            </td>\
                            <td>\
                                <button type="button" class="btn btn-raised btn-icon btn-danger mr-1" onclick="deletepro('+addproductorow+')"><i class="fa fa-trash-o"></i></button>\
                            </td>\
                        </tr>';
                    $('.table-productos-tb').append(html);
                addproductorow++;
                setTimeout(function(){ 
                    //$('#sucursal_destino').prop('selectedIndex',0);
                    //$('#sucursal_origen').prop('selectedIndex',0);
                    $('#s_producto').val(null).trigger('change');
                    $('#s_cantidad').val('');
                    //$(".sucursal_destino option:selected").attr('disabled','disabled') .siblings().removeAttr('disabled');
                    //$(".sucursal_origen option:selected").attr('disabled','disabled') .siblings().removeAttr('disabled');
                    $('.existencia_total').html('');
                }, 1000);
            }else{
                toastr.error('La cantidad es mayor a la existencia del producto', 'Atención!');
            }
        }else{
            toastr.error('Falta agregar cantidad o producto', 'Atención!');
        }
    }
}

function deletepro(row){
    $('.addproducto_'+row).remove();
}
function save(){
    var form_register = $('#form-traspasos');
    var error_register = $('.alert-danger', form_register);
    var success_register = $('.alert-success', form_register);
    form_register.validate({
        errorElement: 'div', //default input error message container
        errorClass: 'vd_red', // default input error message class
        focusInvalid: false, // do not focus the last invalid input
        ignore: "",
        rules: {
            personalId: {
                required: true
            },
            fecha_solicitud: {
                required: true
            },
        },
        errorPlacement: function(error, element) {
            if (element.parent().hasClass("vd_checkbox") || element.parent().hasClass("vd_radio")){
                element.parent().append(error);
            } else if (element.parent().hasClass("vd_input-wrapper")){
                error.insertAfter(element.parent());
            }else {
                error.insertAfter(element);
            }
        }, 
        
        invalidHandler: function (event, validator) { //display error alert on form submit              
                success_register.fadeOut(500);
                error_register.fadeIn(500);
                scrollTo(form_register,-100);

        },

        highlight: function (element) { // hightlight error inputs
    
            $(element).addClass('vd_bd-red');
            $(element).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');

        },

        unhighlight: function (element) { // revert the change dony by hightlight
            $(element)
                .closest('.control-group').removeClass('error'); // set error class to the control group
        },

        success: function (label, element) {
            label
                .addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
                .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
            $(element).removeClass('vd_bd-red');
        }
    });
    var $valid = $("#form-traspasos").valid();
    if($valid) {
        var vali_tabla=0;
        var DATAconf  = [];
        var TABLAconf   = $("#table-productos tbody > tr");
        TABLAconf.each(function(){  
            vali_tabla=1;       
            item = {};
            item ["traspasodId"] = $(this).find("input[id*='traspasodId']").val();
            item ["cantidad"] = $(this).find("input[id*='cantidad']").val();
            item ["productoId"] = $(this).find("input[id*='productoId']").val();
            item ["sucursal_origen"] = $(this).find("input[id*='sucursal_origenx']").val();
            item ["sucursal_destino"] = $(this).find("input[id*='sucursal_destinox']").val();
            DATAconf.push(item);
        });
        if(vali_tabla==1){ 
            $('.button_save').attr('disabled',true);
            INFO  = new FormData();
            arrayconfiguracion   = JSON.stringify(DATAconf);
            console.log(arrayconfiguracion);
            console.log($.parseJSON(arrayconfiguracion)); 
            var datos = $('#form-traspasos').serialize()+'&arrayproductos='+arrayconfiguracion;
            $.ajax({
                type:'POST',
                url: base_url+"index.php/Traspasos/insertupdate",
                data: datos,
                statusCode: {
                    404: function(data) {
                        toastr.error('Error', '404');
                    },
                    500: function(data) {
                        console.log(data);
                        toastr.error('Error', 'Algo salió mal, intente de nuevo o contacte al administrador del sistema');
                    }
                },
                success: function (response){
                    toastr.success( 'Configuración agregado','Hecho!');
                    
                    
                    setTimeout(function(){ 
                        //location.reload();
                       window.location.href = base_url+"Traspasos"; 
                    }, 2000);
                    
                },
                error: function(response){
                    toastr.error('Error', 'Algo salió mal, intente de nuevo o contacte al administrador del sistema');
                }
            });
        }else{
            toastr.error('No tienes ninguna transferencia agregada', 'Atención!');
        }
    }
}

function validar_nave(){
    var id = $('#sucursal_origen option:selected').val();
    $('#s_producto').val(null).trigger('change');
    $('.existencia_total').html('');
    $('#sucursal_destino').prop('selectedIndex',0);
    $("#sucursal_destino option:selected").attr('disabled','disabled') .siblings().removeAttr('disabled');
    setTimeout(function(){ 
        $("#sucursal_destino option[value='"+id+"']").attr("disabled", "disabled");
    }, 1000);
}
/*
function validar_nave2(){
    var id = $('#sucursal_origen option:selected').val();
    if(id==0){
        toastr.error('Primero seleccionar un lugar de origen', 'Atención!');
        $('#sucursal_destino').prop('selectedIndex',0);
    }else{
        var id2 = $('#sucursal_destino option:selected').val();
        //$(".sucursal_destino option:selected").attr('disabled','disabled') .siblings().removeAttr('disabled');
        //$(".sucursal_origen option:selected").attr('disabled','disabled') .siblings().removeAttr('disabled');
        setTimeout(function(){ 
            $("#sucursal_destino option[value='"+id+"']").attr("disabled", "disabled");
        }, 1000);
        setTimeout(function(){ 
            $("#sucursal_origen option[value='"+id2+"']").attr("disabled", "disabled");
        }, 1000);
    }
    
}
*/