var base_url = $('#base_url').val();
var table;
$(document).ready(function($) {
	table=$('#table-data-row').DataTable();
	loadtable();
});
function loadtable(){
	table.destroy();
	table = $('#table-data-row').DataTable({
        stateSave: true,
        responsive: !0,
        "bProcessing": true,
        "serverSide": true,
        "ajax": {
            "url": base_url+"index.php/Traspasos/getlist_row",
            type: "post",
            "data": {
                'personal':0
            },
        },
        "columns": [
            {"data": "traspasoId"},
            {"data": "personal"},
            {"data": "sucursalo"},
            {"data": "sucursald"},
            {"data": "fecha_solicitud"},
            {"data": null,
                render:function(data,type,row){
                    var html='';
                    if(row.activo==1){
                        html='<a href="'+base_url+'Traspasos/generarPDF/'+row.traspasoId+'" target="_blank" class="btn btn-flat btn-danger" title="Generar formato"><i class="fa fa-file-text-o icon_font"></i></a>';
                        html+='<a class="btn btn-flat btn-danger" onclick="btn_cancelar('+row.traspasoId+')"><i class="fa fa-times-circle-o icon_font"></i></a>';
                    }else{
                        html='<span class="bg-danger text-highlight white" style="border-radius: 10px;">Cancelada</span>';
                    }
                    return html;
                }
            },
        ],
        "order": [[ 0, "desc" ]],
        "lengthMenu": [[25, 50, 100], [25, 50,100]]
        
    });
}

function btn_cancelar(id){
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: '¿Está seguro de cancelar este traspaso, afectara al catalogo de productos?',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                $.ajax({
                    type:'POST',
                    url: base_url+"Traspasos/update_traspaso",
                    data: {
                        id:id
                    },
                    statusCode: {
                        404: function(data) {
                            toastr.error('404','Error');
                        },
                        500: function(data) {
                            console.log(data);
                            toastr.error('Algo salió mal, intente de nuevo o contacte al administrador del sistema','Error');
                        }
                    },
                    success:function(response){  
                        toastr.success( 'Eliminado correctamente','Hecho!');
                        loadtable();
                    }
                });
            },
            cancelar: function () 
            {
                
            }
        }
    });
}