INSERT INTO `menu` (`MenuId`, `Nombre`, `Icon`) VALUES (NULL, 'Operaciones', 'fa fa-folder-open');
INSERT INTO `menu_sub` (`MenusubId`, `MenuId`, `Nombre`, `Pagina`, `Icon`) VALUES (NULL, '10', 'Ventas', 'Ventas', 'fa fa-shopping-cart');
INSERT INTO `perfiles_detalles` (`Perfil_detalleId`, `perfilId`, `MenusubId`) VALUES (NULL, '1', '27');
INSERT INTO `perfiles_detalles` (`Perfil_detalleId`, `perfilId`, `MenusubId`) VALUES (NULL, '1', '28');
INSERT INTO `menu_sub` (`MenusubId`, `MenuId`, `Nombre`, `Pagina`, `Icon`) VALUES (NULL, '10', 'Lista de ventas', 'ListaVentas', 'fa fa-cogs');

INSERT INTO `menu_sub` (`MenusubId`, `MenuId`, `Nombre`, `Pagina`, `Icon`) VALUES (NULL, '10', 'Gastos y pagos', 'GastosPagos', 'fa fa-dollar');
INSERT INTO `perfiles_detalles` (`Perfil_detalleId`, `perfilId`, `MenusubId`) VALUES (NULL, '1', '29');

INSERT INTO `menu_sub` (`MenusubId`, `MenuId`, `Nombre`, `Pagina`, `Icon`) VALUES (NULL, '10', 'Corte de caja', 'Corte_caja', 'fa fa-cogs');
INSERT INTO `perfiles_detalles` (`Perfil_detalleId`, `perfilId`, `MenusubId`) VALUES (NULL, '1', '30');

-----------------------------------------------------------------------------------------------------------------------------------

ALTER TABLE `ventas` ADD `check_servicio` VARCHAR(2) NOT NULL AFTER `monto_total`, ADD `fecha_servicio` DATE NULL DEFAULT NULL AFTER `check_servicio`, ADD `hora_servicio` TIME NULL DEFAULT NULL AFTER `fecha_servicio`, ADD `unidad_servicio` INT(11) NOT NULL AFTER `hora_servicio`;

ALTER TABLE `ventas` ADD `id_cotizacion` INT(11) NOT NULL AFTER `id_cliente`;

INSERT INTO `menu_sub` (`MenusubId`, `MenuId`, `Nombre`, `Pagina`, `Icon`) VALUES (NULL, '10', 'Servicios', 'Servicio', 'fa fa-cogs');
INSERT INTO `perfiles_detalles` (`Perfil_detalleId`, `perfilId`, `MenusubId`) VALUES (NULL, '1', '27');

<!-------------------?>
18-01-2023-->
ALTER TABLE `servicios` ADD `servicio_cuesta` FLOAT NOT NULL AFTER `reg`;
ALTER TABLE `ventas` ADD `hora_servicio_fin` TIME NOT NULL AFTER `reg`;
ALTER TABLE `ventas` ADD `duracion_servicio` TIME NOT NULL AFTER `hora_servicio_fin`;
ALTER TABLE `servicios` ADD `duracion` TIME NOT NULL AFTER `servicio_cuesta`;

19-01-2023
ALTER TABLE `venta_detalle` ADD `activo` INT(1) NOT NULL DEFAULT '1' AFTER `precio`;
ALTER TABLE `ventas` ADD `activo` INT(1) NOT NULL DEFAULT '1' AFTER `reg`;
ALTER TABLE `ventas` ADD `check_servicio_cita` INT(1) NOT NULL DEFAULT '0' AFTER `activo`;