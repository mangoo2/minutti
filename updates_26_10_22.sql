ALTER TABLE `personal` ADD `celular` VARCHAR(55) NOT NULL AFTER `puesto`;
ALTER TABLE `personal` ADD `nss` VARCHAR(55) NOT NULL AFTER `celular`;

INSERT INTO `menu_sub` (`MenusubId`, `MenuId`, `Nombre`, `Pagina`, `Icon`) VALUES (NULL, '1', 'Herramientas', 'Herramientas', 'fa fa-wrench');
INSERT INTO `perfiles_detalles` (`Perfil_detalleId`, `perfilId`, `MenusubId`) VALUES (NULL, '1', '25');

CREATE TABLE `sicoinet_minutti`.`herramientas` ( `id_herramienta` INT(11) NOT NULL AUTO_INCREMENT , `medida` VARCHAR(255) NOT NULL , `nombre` VARCHAR(255) NOT NULL , `ubicacion` VARCHAR(255) NOT NULL , `stock` INT(11) NOT NULL , `activo` TINYINT(1) NOT NULL DEFAULT '1' , PRIMARY KEY (`id_herramienta`)) ENGINE = InnoDB;

CREATE TABLE `sicoinet_minutti`.`marca` ( `marcaId` INT(11) NOT NULL AUTO_INCREMENT , `marca` VARCHAR(50) NOT NULL , `reg` TIMESTAMP NOT NULL , `activo` TINYINT(1) NOT NULL DEFAULT '1' , PRIMARY KEY (`marcaId`)) ENGINE = InnoDB;
ALTER TABLE `producto` ADD `marcaId` INT(11) NOT NULL AFTER `categoriaId`;

ALTER TABLE `producto` ADD `precio_venta` FLOAT NOT NULL AFTER `stock_disponible`, ADD `costo_compra` FLOAT NOT NULL AFTER `precio_venta`;


---------------------------------------------------
CREATE TABLE `sicoinet_minutti`.`unidades` ( `unidadId` INT(11) NOT NULL AUTO_INCREMENT , `clienteId` INT(11) NOT NULL , `placas` VARCHAR(10) NOT NULL , `modelo` VARCHAR(50) NOT NULL , `ano` INT(11) NOT NULL , `activo` TINYINT(1) NOT NULL DEFAULT '1' , PRIMARY KEY (`unidadId`)) ENGINE = InnoDB;

ALTER TABLE `unidades` ADD CONSTRAINT `clientes_clienteId_unidades` FOREIGN KEY (`clienteId`) REFERENCES `clientes`(`clienteId`) ON DELETE NO ACTION ON UPDATE NO ACTION;

ALTER TABLE `clientes` ADD `check_unidades` VARCHAR(2) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL AFTER `check_bancarios`;
---------------------------------------------------


CREATE TABLE `sicoinet_minutti`.`uso_cfdi` ( `id` INT(11) NOT NULL AUTO_INCREMENT , `clave` VARCHAR(11) NOT NULL, `descripcion` VARCHAR(255) NOT NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB;
CREATE TABLE `sicoinet_minutti`.`metodo_pago` ( `id` INT(11) NOT NULL AUTO_INCREMENT , `descripcion` VARCHAR(255) NOT NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB;
CREATE TABLE `sicoinet_minutti`.`forma_pago` ( `id` INT(11) NOT NULL AUTO_INCREMENT , `clave` INT(11) NOT NULL, `descripcion` VARCHAR(255) NOT NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB;
CREATE TABLE `sicoinet_minutti`.`regimen_fiscal` ( `id` INT(11) NOT NULL AUTO_INCREMENT , `clave` INT(11) NOT NULL, `descripcion` VARCHAR(255) NOT NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB;


INSERT INTO `uso_cfdi` (`id`, `clave`, `descripcion`) VALUES (NULL, 'G01', 'Adquisición de mercancías.'), (NULL, 'G02', 'Devoluciones, descuentos o bonificaciones.'), (NULL, 'G03', 'Gastos en general.'), (NULL, 'I01', 'Construcciones.'), (NULL, 'I02', 'Mobiliario y equipo de oficina por inversiones.'), (NULL, 'I03', 'Equipo de transporte.'), (NULL, 'I04', 'Equipo de cómputo y accesorios.'), (NULL, 'I05', 'Dados, troqueles, moldes, matrices y herramental.'), (NULL, 'I06', 'Comunicaciones telefónicas.'), (NULL, 'I07', 'Comunicaciones satelitales.'), (NULL, 'I08', 'Otra maquinaria y equipo.'), (NULL, 'D01', 'Honorarios médicos, dentales y gastos hospitalarios.'), (NULL, 'D02', 'Gastos médicos por incapacidad o discapacidad.'), (NULL, 'D03', 'Gastos funerales.'), (NULL, 'D04', 'Donativos.'), (NULL, 'D05', 'Intereses reales efectivamente pagados por créditos hipotecarios (casa habitación).'), (NULL, 'D06', 'Aportaciones voluntarias al SAR.'), (NULL, 'D07', 'Primas por seguros de gastos médicos.'), (NULL, 'D08', 'Gastos de transportación escolar obligatoria.'), (NULL, 'D09', 'Depósitos en cuentas para el ahorro, primas que tengan como base planes de pensiones.'), (NULL, 'D10', 'Pagos por servicios educativos (colegiaturas).'), (NULL, 'P01', 'Por definir.');
INSERT INTO `metodo_pago` (`id`, `descripcion`) VALUES (NULL, 'Pago en una sola exhibición.'), (NULL, 'Pago en parcialidades o diferido.');
INSERT INTO `forma_pago` (`id`, `clave`, `descripcion`) VALUES (NULL, '01', 'Efectivo'), (NULL, '02', 'Cheque nominativo'), (NULL, '03', 'Transferencia electrónica de fondos'), (NULL, '04', 'Tarjetas de Crédito'), (NULL, '05', 'Monedero Electrónico'), (NULL, '06', 'Dinero Electrónico'), (NULL, '08', 'Vales de despensa'), (NULL, '12', 'Dación en pago'), (NULL, '13', 'Pago por subrogación'), (NULL, '14', 'Pago por consignación'), (NULL, '15', 'Condonación'), (NULL, '17', 'Compensación'), (NULL, '23', 'Novación'), (NULL, '24', 'Confusión'), (NULL, '25', 'Remisión de deuda'), (NULL, '26', 'Prescripción o caducidad'), (NULL, '27', 'A satisfacción del acreedor'), (NULL, '28', 'Tarjeta de Débito'), (NULL, '29', 'Tarjeta de Servicio'), (NULL, '30', 'Aplicación de anticipos'), (NULL, '99', 'Por definir');
INSERT INTO `regimen_fiscal` (`id`, `clave`, `descripcion`) VALUES (NULL, '601', 'General de Ley Personas Morales'), (NULL, '603', 'Personas Morales con Fines no Lucrativos'), (NULL, '605', 'Sueldos y Salarios e Ingresos Asimilados a Salarios'), (NULL, '606', 'Arrendamiento'), (NULL, '607', 'Régimen de Enajenación o Adquisición de Bienes'), (NULL, '608', 'Demás ingresos'), (NULL, '609', 'Consolidación'), (NULL, '610', 'Residentes en el Extranjero sin Establecimiento Permanente en México'), (NULL, '611', 'Ingresos por Dividendos (socios y accionistas)'), (NULL, '612', 'Personas Físicas con Actividades Empresariales y Profesionales'), (NULL, '614', 'Ingresos por intereses'), (NULL, '615', 'Régimen de los ingresos por obtención de premios'), (NULL, '616', 'Sin obligaciones fiscales'), (NULL, '620', 'Sociedades Cooperativas de Producción que optan por diferir sus ingresos'), (NULL, '621', 'Incorporación Fiscal'), (NULL, '622', 'Actividades Agrícolas, Ganaderas, Silvícolas y Pesqueras'), (NULL, '623', 'Opcional para Grupos de Sociedades'), (NULL, '624', 'Coordinados'), (NULL, '628', 'Hidrocarburos'), (NULL, '629', 'De los Regímenes Fiscales Preferentes y de las Empresas Multinacionales'), (NULL, '630', 'Enajenación de acciones en bolsa de valores');


ALTER TABLE `clientes` ADD `uso_cfdi` INT(11) NOT NULL AFTER `rfc`;
ALTER TABLE `clientes` ADD `metodo_pago` INT(11) NOT NULL AFTER `uso_cfdi`;
ALTER TABLE `clientes` ADD `forma_pago` INT(11) NOT NULL AFTER `metodo_pago`;
ALTER TABLE `clientes` ADD `condicion_pago` VARCHAR(255) NOT NULL AFTER `forma_pago`;
ALTER TABLE `clientes` ADD `regimen_fiscal` INT(11) NOT NULL AFTER `condicion_pago`;

INSERT INTO `menu` (`MenuId`, `Nombre`, `Icon`) VALUES (NULL, 'Operaciones', 'fa fa-wrench');
INSERT INTO `menu_sub` (`MenusubId`, `MenuId`, `Nombre`, `Pagina`, `Icon`) VALUES (NULL, '10', 'Cotizaciones', 'Cotizacion', 'fa fa-file');
INSERT INTO `perfiles_detalles` (`Perfil_detalleId`, `perfilId`, `MenusubId`) VALUES (NULL, '1', '26');

CREATE TABLE `sicoinet_minutti`.`cotizaciones` ( `idCotizaciones` INT(11) NOT NULL AUTO_INCREMENT , `clienteId` INT(11) NOT NULL , `fecha` DATE NOT NULL , `correo` VARCHAR(255) NOT NULL , `telefono` VARCHAR(20) NOT NULL , `check_producto` VARCHAR(2) NOT NULL , `total_producto` FLOAT NOT NULL , `check_servicio` VARCHAR(2) NOT NULL , `total_servicio` FLOAT NOT NULL , `observaciones` TEXT NOT NULL , `estatus` INT(1) NOT NULL , `subtotal` FLOAT NOT NULL , `iva` FLOAT NOT NULL , `total` FLOAT NOT NULL,`usuarioId` INT(11) NOT NULL , `activo` INT(1) NOT NULL DEFAULT '1' , PRIMARY KEY (`idCotizaciones`)) ENGINE = InnoDB;
CREATE TABLE `sicoinet_minutti`.`cotizaciones_productos` ( `idProductos` INT(11) NOT NULL AUTO_INCREMENT , `cotizacionesId` INT(11) NOT NULL , `codigo` VARCHAR(250) NOT NULL , `producto` VARCHAR(250) NOT NULL , `descripcion` TEXT NOT NULL , `p_unitario` FLOAT NOT NULL , `activo` INT(1) NOT NULL DEFAULT '1' , PRIMARY KEY (`idProductos`)) ENGINE = InnoDB;
CREATE TABLE `sicoinet_minutti`.`cotizaciones_servicios` ( `idServicios` INT(11) NOT NULL AUTO_INCREMENT , `cotizacionesId` INT(11) NOT NULL , `codigo` VARCHAR(250) NOT NULL , `servicio` VARCHAR(250) NOT NULL , `descripcion` TEXT NOT NULL , `p_unitario` FLOAT NOT NULL , `activo` INT(1) NOT NULL DEFAULT '1' , PRIMARY KEY (`idServicios`)) ENGINE = InnoDB;